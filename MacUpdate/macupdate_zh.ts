<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>updateWidget</name>
    <message>
        <location filename="updatewidget.ui" line="19"/>
        <source>Update</source>
        <translation>升级</translation>
    </message>
    <message>
        <location filename="updatewidget.ui" line="52"/>
        <source>Update note</source>
        <translation>升级提示</translation>
    </message>
    <message>
        <location filename="updatewidget.ui" line="143"/>
        <source>Download</source>
        <translation>下载</translation>
    </message>
    <message>
        <location filename="updatewidget.ui" line="176"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="updatewidget.cpp" line="44"/>
        <source>Update content:</source>
        <translation>升级内容：</translation>
    </message>
</context>
</TS>
