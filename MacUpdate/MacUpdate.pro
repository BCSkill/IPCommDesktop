#-------------------------------------------------
#
# Project created by QtCreator 2018-08-20T14:59:14
#
#-------------------------------------------------

QT +=core gui widgets

SOURCES += \
    updatewidget.cpp

HEADERS += \
    updatewidget.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}

FORMS += \
    updatewidget.ui

include(shared.pri)
TRANSLATIONS += ./macupdate_zh.ts
TRANSLATIONS += ./macupdate_ru.ts
TRANSLATIONS += ./macupdate_hy.ts
TRANSLATIONS += ./macupdate_es.ts
TRANSLATIONS += ./macupdate_pt.ts

DISTFILES += \
    macupdate_zh.ts