<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hy_AM">
<context>
    <name>updateWidget</name>
    <message>
        <location filename="updatewidget.ui" line="19"/>
        <source>Update</source>
        <translation>Обновить</translation>
    </message>
    <message>
        <location filename="updatewidget.ui" line="52"/>
        <source>Update note</source>
        <translation>Обновить заметку</translation>
    </message>
    <message>
        <location filename="updatewidget.ui" line="143"/>
        <source>Download</source>
        <translation>скачать</translation>
    </message>
    <message>
        <location filename="updatewidget.ui" line="176"/>
        <source>cancel</source>
        <translation>отменить</translation>
    </message>
    <message>
        <location filename="updatewidget.cpp" line="44"/>
        <source>Update content:</source>
        <translation>Обновление контента:</translation>
    </message>
</context>
</TS>
