﻿#include "screencontrolwidget.h"
#include <QPoint>
#include <QDebug>
#include <QApplication>
#include <QClipboard>
#include "oescreenshot.h"

ScreenControlWidget::ScreenControlWidget(QWidget *parent)
: QWidget(parent), ui(new Ui::ScreenControlWidget)
{
	ui->setupUi(this);
	initUI();

	isDrawLine = false;
	isDrawArrow = false;;
	isDrawRound = false;;
	isDrawText = false;;
	isDrawRectang = false;;
  //  setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog);
    setWindowFlags( Qt::FramelessWindowHint);
	setStyleSheet("background-color:rgb(255,255,255)");

	connectSignalAndSlot();
}

ScreenControlWidget::~ScreenControlWidget()
{
	delete ui;
}

void ScreenControlWidget::initUI()
{
}

void ScreenControlWidget::connectSignalAndSlot()
{
	connect(ui->cancelBtn, SIGNAL(clicked()), this, SLOT(slotCancelBtnClicked()));
	connect(ui->finishBtn, SIGNAL(clicked()), this, SLOT(slotFinishBtnClicked()));
	connect(ui->rectangleBtn, SIGNAL(clicked()), this, SLOT(slotRectangleBtnClicked()));
	connect(ui->drawRoundBtn, SIGNAL(clicked()), this, SLOT(slotDrawRoundBtnClicked()));
	connect(ui->arrowBtn, SIGNAL(clicked()), this, SLOT(slotArrowBtnClicked()));
	connect(ui->drawLineBtn, SIGNAL(clicked()), this, SLOT(slotDrawLineBtnClicked()));
	connect(ui->textEditBtn, SIGNAL(clicked()), this, SLOT(slotEditBtnClicked()));

	connect(ui->revertBtn, SIGNAL(clicked()), this, SLOT(slotRevertBtnClicked()));
	connect(ui->saveBtn, SIGNAL(clicked()), this, SLOT(slotSaveBtnClicked()));
}

//保存Screen类的引用
void ScreenControlWidget::setScreenQuote(OEScreenshot* screen)
{
	this->screen = screen;
}

//取消按钮
void ScreenControlWidget::slotCancelBtnClicked()
{
	if (screen)
	{
        screen->CancleScreenCut();
	}
}

//完成按钮
void ScreenControlWidget::slotFinishBtnClicked()
{
	if (screen)
	{
		screen->SaveCutPicture();
	}
}

//添加字按钮
void ScreenControlWidget::slotEditBtnClicked()
{
	if (screen)
	{
		if (isDrawText)
		{
 			screen->DrawTextEnable(false);
			isDrawText = false;
			ui->textEditBtn->setChecked(false);
		}
		else
		{
 			screen->DrawTextEnable(true);
			isDrawText = true;
			ui->rectangleBtn->setChecked(false);
			ui->drawRoundBtn->setChecked(false);
			ui->arrowBtn->setChecked(false);
			ui->drawLineBtn->setChecked(false);
		}
	}
}

//绘制矩形
void ScreenControlWidget::slotRectangleBtnClicked()
{
	if (screen)
	{
		if (isDrawRectang)
		{
 			screen->DrawRectangEnable(false);
			isDrawRectang = false;
			ui->rectangleBtn->setChecked(false);
		}
		else
		{
 			screen->DrawRectangEnable(true);
			isDrawRectang = true;
			ui->drawRoundBtn->setChecked(false);
			ui->arrowBtn->setChecked(false);
			ui->drawLineBtn->setChecked(false);
			ui->textEditBtn->setChecked(false);
		}
	}
}

//绘制圆
void ScreenControlWidget::slotDrawRoundBtnClicked()
{
	if (screen)
	{
		if (isDrawRound)
		{
 			screen->DrawRoundEnable(false);
			isDrawRound = false;
			ui->drawRoundBtn->setChecked(false);
		}
		else
		{
 			screen->DrawRoundEnable(true);
			isDrawRound = true;
			ui->rectangleBtn->setChecked(false);
			ui->arrowBtn->setChecked(false);
			ui->drawLineBtn->setChecked(false);
			ui->textEditBtn->setChecked(false);
		}
	}
}

//绘制箭头
void ScreenControlWidget::slotArrowBtnClicked()
{
	if (screen)
	{
		if (isDrawArrow)
		{
 			screen->DrawArrowEnable(false);
			isDrawArrow = false;
			ui->arrowBtn->setChecked(false);
		}
		else
		{
 			screen->DrawArrowEnable(true);
			isDrawArrow = true;
			ui->rectangleBtn->setChecked(false);
			ui->drawRoundBtn->setChecked(false);
			ui->drawLineBtn->setChecked(false);
			ui->textEditBtn->setChecked(false);
		}
	}
}

//绘制线条
void ScreenControlWidget::slotDrawLineBtnClicked()
{
	if (screen)
	{
		if (isDrawLine)
		{
 			screen->DrawLineEnable(false);
			isDrawLine = false;
			ui->drawLineBtn->setChecked(false);
		}
		else
		{
 			screen->DrawLineEnable(true);
			isDrawLine = true;
			ui->rectangleBtn->setChecked(false);
			ui->drawRoundBtn->setChecked(false);
			ui->arrowBtn->setChecked(false);
			ui->textEditBtn->setChecked(false);
		}
	}
}

//撤销
void ScreenControlWidget::slotRevertBtnClicked()
{
	screen->RevertPaints();
}

//保存
void ScreenControlWidget::slotSaveBtnClicked()
{
	screen->SaveAsPic();
}

void ScreenControlWidget::paintEvent(QPaintEvent* event)
{
    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);

    QWidget::paintEvent(event);
}

