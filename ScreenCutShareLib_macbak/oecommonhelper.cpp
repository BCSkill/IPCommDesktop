﻿
#include "oecommonhelper.h"

#include <QFile>
#include <QTranslator>
#include <QApplication>
#include <QWidget>
#include <QDesktopWidget>

#ifdef Q_OS_WIN
#include <windows.h>
#endif
#ifndef QT_NO_DEBUG
#include <QDebug>
#endif


#define WINDOW_BASESIZE_WIDTH (1920)
#define WINDOW_BASESIZE_HEIGHT (1080)

float OECommonHelper::widthMultiplyingPower_ = 0;
float OECommonHelper::heightMultiplyingPower_ = 0;

void OECommonHelper::setStyle(const QString &style) 
{
    QFile qss(style);
    qss.open(QFile::ReadOnly);
    qApp->setStyleSheet(qss.readAll());
    qss.close();
}

void OECommonHelper::setLanguagePack(const QString &language) 
{
    // 加载中文包
    QTranslator translator;
    translator.load(language);
    qApp->installTranslator(&translator);
}

void OECommonHelper::moveCenter(QWidget *widget, QRect parentRect) 
{
    if (parentRect.isEmpty()) 
	{
        parentRect = QApplication::desktop()->rect();
    }
    widget->move (((parentRect.width() - widget->width()) >> 1),((parentRect.height() - widget->height()) >> 1));
}

const float &OECommonHelper::getWindowWidthMultiplyingPower() 
{
    if (widthMultiplyingPower_ == 0) 
	{
        upWindowSizeMultiplyingPower();
    }
    return widthMultiplyingPower_;
}

const float & OECommonHelper::getWindowHeightMultiplyingPower() 
{
    if (heightMultiplyingPower_ == 0) 
	{
        upWindowSizeMultiplyingPower();
    }
    return heightMultiplyingPower_;
}

void OECommonHelper::upWindowSizeMultiplyingPower() 
{
   QSize temp_size = QApplication::desktop()->size();
   widthMultiplyingPower_ = (float)temp_size.width() / (float)WINDOW_BASESIZE_WIDTH;
   heightMultiplyingPower_ = (float)temp_size.height() / (float)WINDOW_BASESIZE_HEIGHT;
}

bool OECommonHelper::getSmallestWindowFromCursor(QRect& out_rect) 
{
#ifdef Q_OS_WIN
	HWND hwnd;
    POINT pt;
    // 获得当前鼠标位置
    ::GetCursorPos(&pt);
    // 获得当前位置桌面上的子窗口
    hwnd = ::ChildWindowFromPointEx(::GetDesktopWindow(), pt, CWP_SKIPDISABLED | CWP_SKIPINVISIBLE);
    if (hwnd != NULL)
	{
        HWND temp_hwnd;
        temp_hwnd = hwnd;
        while (true)
		{
            ::GetCursorPos(&pt);
            ::ScreenToClient(temp_hwnd, &pt);
            temp_hwnd = ::ChildWindowFromPointEx(temp_hwnd, pt, CWP_SKIPINVISIBLE);
            if (temp_hwnd == NULL || temp_hwnd == hwnd)
                break;
            hwnd = temp_hwnd;
        }
        RECT temp_window;
        ::GetWindowRect(hwnd, &temp_window);
        out_rect.setRect(temp_window.left,temp_window.top,temp_window.right - temp_window.left, temp_window.bottom - temp_window.top);
        return true;
    }
#endif
    return false;
}

bool OECommonHelper::getCurrentWindowFromCursor(QRect &out_rect)
{
#ifdef Q_OS_WIN
    HWND hwnd;
    POINT pt;
    // 获得当前鼠标位置
    ::GetCursorPos(&pt);
    // 获得当前位置桌面上的子窗口
    hwnd = ::ChildWindowFromPointEx(::GetDesktopWindow(), pt,CWP_SKIPDISABLED | CWP_SKIPINVISIBLE);
    if (hwnd != NULL) 
	{
        RECT temp_window;
        ::GetWindowRect(hwnd, &temp_window);
        out_rect.setRect(temp_window.left, temp_window.top,temp_window.right - temp_window.left, temp_window.bottom - temp_window.top);
        return true;
    }
#endif
    return false;
}
