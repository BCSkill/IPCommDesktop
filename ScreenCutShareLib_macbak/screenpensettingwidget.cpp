﻿#include "screenpensettingwidget.h"
#include "ui_screenpensettingwidget.h"
#include <QButtonGroup>
#include <QPainter>

ScreenPenSettingWidget::ScreenPenSettingWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::ScreenPenSettingWidget();
	ui->setupUi(this);
	ui->btnColor->setStyleSheet("border:1px solid darkGrey;background-color:red");
	ui->btn15px->setChecked(true);
	m_btnColorGroup = new QButtonGroup();
	m_btnColorGroup->addButton(ui->btnBlack, 0);
	m_btnColorGroup->addButton(ui->btnWhite, 1);
	m_btnColorGroup->addButton(ui->btnGrey, 2);
	m_btnColorGroup->addButton(ui->btnLightGrey, 3);
	m_btnColorGroup->addButton(ui->btnDarkRed, 4);
	m_btnColorGroup->addButton(ui->btnRed, 5);
	m_btnColorGroup->addButton(ui->btnOrange, 6);
	m_btnColorGroup->addButton(ui->btnYellow, 7);
	m_btnColorGroup->addButton(ui->btnGreen, 8);
	m_btnColorGroup->addButton(ui->btnLightGreen, 9);
	m_btnColorGroup->addButton(ui->btnDarkBlue, 10);
	m_btnColorGroup->addButton(ui->btnBlue, 11);
	m_btnColorGroup->addButton(ui->btnDarkMagenta, 12);
	m_btnColorGroup->addButton(ui->btnMagenta, 13);
	m_btnColorGroup->addButton(ui->btnDarkCyan, 14);
	m_btnColorGroup->addButton(ui->btnCyan, 15);
	connect(m_btnColorGroup, SIGNAL(buttonClicked(int)), this, SLOT(slotSelectColor(int)));
	m_colorMap.insert(0, "black");
	m_colorMap.insert(1, "white");
	m_colorMap.insert(2, "grey");
	m_colorMap.insert(3, "lightGrey");
	m_colorMap.insert(4, "darkRed");
	m_colorMap.insert(5, "red");
	m_colorMap.insert(6, "orange");
	m_colorMap.insert(7, "yellow");
	m_colorMap.insert(8, "green");
	m_colorMap.insert(9, "lightGreen");
	m_colorMap.insert(10, "darkBlue");
	m_colorMap.insert(11, "blue");
	m_colorMap.insert(12, "darkMagenta");
	m_colorMap.insert(13, "magenta");
	m_colorMap.insert(14, "darkCyan");
	m_colorMap.insert(15, "cyan");

	m_btnLineGroup = new QButtonGroup();
	m_btnLineGroup->addButton(ui->btn9px,0);
	m_btnLineGroup->addButton(ui->btn15px, 1);
	m_btnLineGroup->addButton(ui->btn25px, 2);
	connect(m_btnLineGroup, SIGNAL(buttonClicked(int)), this, SLOT(slotSelectLine(int)));

	for (int i = 8; i < 23; i++)
	{
		ui->textCombobox->addItem(QString::number(i));
	}
	ui->textCombobox->setCurrentIndex(1);
	ui->textCombobox->setFocusPolicy(Qt::NoFocus); 
	connect(ui->textCombobox, SIGNAL(currentIndexChanged(QString)), this, SIGNAL(sigTextSizeChanged(QString)));
}

ScreenPenSettingWidget::~ScreenPenSettingWidget()
{
	delete ui;
}

void ScreenPenSettingWidget::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);

	QWidget::paintEvent(event);
}

void ScreenPenSettingWidget::setType(int iType)
{
	if (iType == 0)
	{
		ui->textLabel->hide();
		ui->textCombobox->hide();
		ui->btn9px->show();
		ui->btn15px->show();
		ui->btn25px->show();
		resize(240, 36);
	}
	else if (iType == 1)
	{
		ui->textLabel->show();
		ui->textCombobox->show();
		ui->btn9px->hide();
		ui->btn15px->hide();
		ui->btn25px->hide();
		resize(240, 36);
	}
}

void ScreenPenSettingWidget::slotSelectColor(int iValue)
{
	ui->btnColor->setStyleSheet("border:1px solid darkGrey;background-color:" + m_colorMap[iValue]);
	QColor qqq = Qt::red;
	switch (iValue)
	{
	case 0:
		qqq = Qt::black;
			break;
	case 1:
		qqq = Qt::white;
			break;
	case 2:
		qqq = Qt::gray;
		break;
	case 3:
		qqq = Qt::lightGray;
		break;
	case 4:
		qqq = Qt::darkRed;
		break;
	case 5:
		qqq = Qt::red;
		break;
	case 6:
		qqq = QColor(255, 127, 39);
		break;
	case 7:
		qqq = Qt::yellow;
		break;
	case 8:
		qqq = Qt::green;
		break;
	case 9:
		qqq = QColor(181, 230, 29);
		break;
	case 10:
		qqq = Qt::darkBlue;
		break;
	case 11:
		qqq = Qt::blue;
		break;
	case 12:
		qqq = Qt::darkMagenta;
		break;
	case 13:
		qqq = Qt::magenta;
		break;
	case 14:
		qqq = Qt::darkCyan;
		break;
	case 15:
		qqq = Qt::cyan;
		break;
	}
	sigSetColor(qqq);
}

void ScreenPenSettingWidget::slotSelectLine(int iValue)
{
	int i = 5;
	switch (iValue)
	{
	case 0:
			i = 1;
			break;
	case 1:
		i = 4;
		break;
	case 2:
		i = 9;
		break;
	}
	sigSetLine(i);
}
