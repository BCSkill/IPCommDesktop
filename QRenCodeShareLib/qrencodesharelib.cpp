#include "qrencodesharelib.h"
#include "qrencode.h"
#include <QPainter>

QRenCodeShareLib::QRenCodeShareLib()
{

}

QRenCodeShareLib::~QRenCodeShareLib()
{

}

QImage QRenCodeShareLib::GenerateQRcode(QString tempstr,int nWidth,int nHeight)
{
	QRcode *qrcode = NULL;
	QImage mainimg;
	qrcode = QRcode_encodeString(tempstr.toStdString().c_str(), 2, QR_ECLEVEL_L, QR_MODE_8, 1);
	if (qrcode)
	{
		qint32 qrcode_width = qrcode->width > 0 ? qrcode->width : 1;
		double scale_x = (double)nWidth / (double)qrcode_width; //二维码图片的缩放比例
		double scale_y = (double)nHeight / (double)qrcode_width;
		mainimg = QImage(nWidth, nHeight, QImage::Format_ARGB32);
		QPainter painter(&mainimg);
		QColor background(Qt::white);
		painter.setBrush(background);
		painter.setPen(Qt::NoPen);
		painter.drawRect(0, 0, nWidth, nHeight);
		QColor foreground(Qt::black);
		painter.setBrush(foreground);
		for (qint32 y = 0; y < qrcode_width; y++)
		{
			for (qint32 x = 0; x < qrcode_width; x++)
			{
				unsigned char b = qrcode->data[y * qrcode_width + x];
				if (b & 0x01)
				{
					QRectF r(x * scale_x, y * scale_y, scale_x, scale_y);
					painter.drawRects(&r, 1);
				}
			}
		}
		QRcode_free(qrcode);
	}
	return mainimg;
}
