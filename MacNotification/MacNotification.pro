#-------------------------------------------------
#
# Project created by QtCreator 2018-09-18T16:53:18
#
#-------------------------------------------------

QT       += core macextras

SOURCES += \
        macnotification.cpp \
    osx/osxplatform.cpp \
    osx/notification.mm \
    osx/notificationimpl.mm

HEADERS += \
        macnotification.h \
    osx/notification.h \
    osx/notificationimpl.h \
    osx/osxplatform.h

INCLUDEPATH += ../SharedLib_mac/OPMainWidget/include \
    ../SharedLib_mac/common

include(shared.pri)
