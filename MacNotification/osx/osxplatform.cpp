#include "osxplatform.h"
#include <QtMac>

OsxPlatform::OsxPlatform()
{

}

void OsxPlatform::showNotify(const QString &title, const QString &content, const QString & userID)
{
    mNotify.show(title, content, userID);
}

void OsxPlatform::hideAllNotify()
{
    mNotify.hideAll();
}

void OsxPlatform::setBadgeNumber(int number)
{
    if (number == 0)
        QtMac::setBadgeLabelText("");
    else
        QtMac::setBadgeLabelText(QString::number(number));
}

void OsxPlatform::setMainWnd(IMMainWidget *mainWnd)
{
    IMacNotification::setMainWnd(mainWnd);
    mNotify.setMainWnd(mainWnd);
}
