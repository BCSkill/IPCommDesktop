#include "notification.h"
#include "notificationimpl.h"
#include <QDebug>
#include "immainwidget.h"

static NotificationImpl* impl = [[NotificationImpl alloc] init];

static void clickCallback(void* arg, NSString* userID)
{
    IMMainWidget* mainWnd = (IMMainWidget*)arg;
    const char* id = [userID UTF8String];
    mainWnd->onNotifyClicked(id);
}

static void replyCallback(void* arg, NSString* userID,NSString* msg)
{
    const char* reply = [msg UTF8String];
    const char* id = [userID UTF8String];

    IMMainWidget* mainWnd = (IMMainWidget*)arg;
    mainWnd->onNotifyReplied(id,reply);
}

Notification::Notification()
{

}

void Notification::show(const QString &title, const QString &content, const QString& userID)
{
    NSString* objcTitle = title.toNSString();
    NSString* objcMessage = content.toNSString();
    [impl show: objcTitle message: objcMessage extra: userID.toNSString()];
}

void Notification::hideAll()
{
    [impl hideAll];
}

void Notification::setMainWnd(IMMainWidget *mainWnd)
{
    impl->clickCallback = clickCallback;
    impl->replyCallback = replyCallback;
    impl->callbackArg = mainWnd;
}
