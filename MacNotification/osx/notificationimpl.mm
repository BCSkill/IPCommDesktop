#include "notificationimpl.h"
#include <QDebug>

@interface NotificationImpl () <NSUserNotificationCenterDelegate>
@end

@implementation NotificationImpl

-(instancetype)init
{
    self = [super init];

    if(self)
    {
        [[NSUserNotificationCenter defaultUserNotificationCenter] setDelegate:self];
    }

    return self;
}

-(void)show:(NSString*)title message:(NSString*)msg extra:(NSString*)userID
{
    NSUserNotification *notification = [[NSUserNotification alloc] init];//创建通知中心
    notification.title = title;
    notification.informativeText = msg;
    notification.userInfo=@{@"extra":userID};

    notification.hasReplyButton = YES;
    notification.responsePlaceholder = @"回复";
   // [notification setValue:[NSImage imageNamed:@"logo.icns"] forKey:@"_identityImage"];

    [[NSUserNotificationCenter defaultUserNotificationCenter] scheduleNotification:notification];
          //设置通知的代理
    [[NSUserNotificationCenter defaultUserNotificationCenter] setDelegate:self];
}

-(void)hideAll
{
    [[NSUserNotificationCenter defaultUserNotificationCenter] removeAllDeliveredNotifications];
}

-(BOOL)userNotificationCenter:(NSUserNotificationCenter *)center shouldPresentNotification:(NSUserNotification *)notification
{
    return YES;
}
-(void)userNotificationCenter:(NSUserNotificationCenter *)center didActivateNotification:(NSUserNotification *)notification
{
    NSString* userID = [notification.userInfo objectForKey:@"extra"];
    if (notification.activationType == NSUserNotificationActivationTypeContentsClicked)
    {
        clickCallback(callbackArg, userID);
    }
    else if (notification.activationType == NSUserNotificationActivationTypeReplied)
    {
        replyCallback(callbackArg, userID, notification.response.string);
    }
}

@end
