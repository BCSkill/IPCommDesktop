#include "macnotification.h"
#include "osx/osxplatform.h"


MacNotification::MacNotification()
{
    mImpl = new OsxPlatform();
}

MacNotification::~MacNotification()
{
    delete mImpl;
}

MacNotification &MacNotification::instance()
{
    static MacNotification me;
    return me;
}

void MacNotification::showNotify(const QString &title, const QString &content, const QString &userID)
{
    mImpl->showNotify(title, content, userID);
}

void MacNotification::hideAllNotify()
{
    mImpl->hideAllNotify();
}

void MacNotification::setBadgeNumber(int number)
{
    mImpl->setBadgeNumber(number);
}

void MacNotification::setMainWnd(IMMainWidget *mainWnd)
{
    IMacNotification::setMainWnd(mainWnd);
    mImpl->setMainWnd(mainWnd);
}
