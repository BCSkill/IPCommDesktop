﻿#include "searchresultwidget.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QEventLoop>
#include "ui_searchresultwidget.h"
#include <qfile.h>

extern QString gThemeStyle;

SearchResultWidget::SearchResultWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::SearchResultWidget();
	ui->setupUi(this);
	m_strId = "";
	ui->headerLabel->setAttribute(Qt::WA_TransparentForMouseEvents, true);
	ui->iconLabel->setAttribute(Qt::WA_TransparentForMouseEvents, true);
	ui->noteLabel->setAttribute(Qt::WA_TransparentForMouseEvents, true);

	QFile style(":/QSS/Resources/QSS/BaseUIShareLib/searchresultwidget.qss");
	style.open(QFile::ReadOnly);
	QString sheet = QLatin1String(style.readAll());
	this->setStyleSheet(sheet);
	style.close();

	//ui->addBtn->setAttribute(Qt::WA_TransparentForMouseEvents, true);
	connect(ui->addBtn, SIGNAL(clicked()), this, SLOT(slotAddBtn()));
	connect(ui->nameBtn, SIGNAL(clicked()), this, SLOT(slotProFile()));
	m_iType = 0;

	m_strStyle = "QWidget#bakwidget{background-color:#eaeaea}";
	if (gThemeStyle == "Blue")
	{
		m_strStyle = "QWidget#bakwidget{background-color:#243C60}";
	}

}

SearchResultWidget::~SearchResultWidget()
{
	delete ui;
}

void SearchResultWidget::InitType(int iType)
{//0代表个人 需隐藏icon
	if (iType == 0)
	{
		ui->iconLabel->hide();
		ui->noteLabel->move(ui->iconLabel->pos());
	}
	//this->setStyleSheet("background-color:red;");
}

void SearchResultWidget::OnSetPicPathByHttp(QString strPath, QString strDefaultImage)
{
	if (ui->headerLabel)
	{
		QNetworkAccessManager manager;
		QEventLoop loop;
		QNetworkReply *reply = manager.get(QNetworkRequest(strPath));
		QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
		//开启子事件循环
		loop.exec();
		QByteArray jpegData = reply->readAll();
		if (!jpegData.isEmpty())
		{
			QPixmap pixmap;
			pixmap.loadFromData(jpegData);
			ui->headerLabel->setAutoFillBackground(true);
			ui->headerLabel->setPixmap(pixmap);
			ui->headerLabel->setScaledContents(true);
		}
		else
		{
			ui->headerLabel->setAutoFillBackground(true);
			ui->headerLabel->setPixmap(QPixmap(strDefaultImage));
			ui->headerLabel->setScaledContents(true);
		}
	}
}

void SearchResultWidget::OnSetNickNameText(QString strText, QString styleSheet)
{
	if (!strText.isEmpty())
	{
		//mStrNickName = strText;     // 保存原样字符串
		// 设置简略字符串(超长显示...)
		QString elide_text = getElideString(strText, (QWidget*)ui->nameBtn, ui->nameBtn->font());
		ui->nameBtn->setText(elide_text);
		ui->nameBtn->setToolTip(strText);
	}
	if (!styleSheet.isEmpty()) ui->nameBtn->setStyleSheet(styleSheet);
}

void SearchResultWidget::OnSetAddPersonButtonText(QString strText)
{
	if (ui->addBtn)
	{
		ui->addBtn->setText(strText);
	}
}

void SearchResultWidget::onSetNote(QString strText)
{
	if (ui->noteLabel)
	{
		ui->noteLabel->setText(strText);
	}
}

void SearchResultWidget::setIdInfo(QString strText)
{
	m_strId = strText;
}

QString SearchResultWidget::getIdInfo()
{
	return m_strId;
}

// 获取适应控件长度的字符串, 超长显示...
QString SearchResultWidget::getElideString(const QString& src_str, const QWidget* child_widget, const QFont& str_font)
{
	QFontMetrics fm(str_font);
	int str_width = fm.width(src_str);

	int widget_width = child_widget->width();
	QString elide_str = child_widget->fontMetrics().elidedText(src_str, Qt::ElideRight, widget_width);
	int str_width2 = fm.width(elide_str);

	return elide_str;
}

void SearchResultWidget::mousePressEvent(QMouseEvent *event)
{
	ui->bakwidget->setStyleSheet(m_strStyle);
	emit sigClickChange();
	QWidget::mousePressEvent(event);
}

void SearchResultWidget::slotAddBtn()
{
	ui->bakwidget->setStyleSheet(m_strStyle);
	emit sigClickChange();
	emit sigAddPerson(m_strId);
}
void SearchResultWidget::slotProFile()
{
	ui->bakwidget->setStyleSheet(m_strStyle);//243C60
	emit sigClickChange();
	emit sigOpenProfile(m_strId);
}
void SearchResultWidget::ResetBackStyle()
{
	ui->bakwidget->setStyleSheet("");
}