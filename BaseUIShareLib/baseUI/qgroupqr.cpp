﻿#include "qgroupqr.h"
#include "ui_qgroupqr.h"
#include <QMouseEvent>
#include <QDesktopWidget>
#include "QStringLiteralBak.h"
#include "define.h"

QGroupQR::QGroupQR(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::QGroupQR();
	ui->setupUi(this);

	setWindowIcon(QIcon(":/Login/Resources/login/system.ico"));

// 	m_pHeader = new QLabelHeader();
// 	m_pGroupName = new QLabel();
// 	m_QRLabel = new QLabel();
// 	m_pNote = m_pNo;
#ifdef Q_OS_WIN
	shadow = new Shadow();
#endif

	mMoveing = false;

	//label = new QLabel(this);
	myButton = new QPushButton(this);
	myButton->setFixedSize(20, 20);
	myButton->setStyleSheet("QPushButton{border-image:url(:/PerChat/Resources/person/zoom-normal.png) 0 0 0 0 stretch stretch;}"
		"QPushButton:hover{border-image:url(:/PerChat/Resources/person/zoom-hover.png) 0 0 0 0 stretch stretch;}");
	connect(myButton, SIGNAL(clicked()), this, SLOT(close()));
	setWindowFlags(Qt::FramelessWindowHint);     //也是去掉标题栏的语句
	setAttribute(Qt::WA_DeleteOnClose, true);

	ui->NoteLabel->setAlignment(Qt::AlignCenter);
	ui->NoteLabel->setText(tr("Scan the QR code to join\n the OpenPlanet group"));

	QFile style(":/QSS/Resources/QSS/BaseUIShareLib/qgroupqr.qss");
	style.open(QFile::ReadOnly);
	QString sheet = QLatin1String(style.readAll());
	this->setStyleSheet(sheet);
	style.close();

	myButton->move(this->x() + this->width() - 20, this->y());
	myButton->setCursor(QCursor(Qt::PointingHandCursor));
	myButton->show();

#ifdef Q_OS_LINUX
	setLinuxCenter();
#endif
}

QGroupQR::~QGroupQR()
{
#ifdef Q_OS_WIN
	if (shadow)
		delete shadow;
#endif
	if (myButton)
		delete myButton;
	delete ui;
}

void QGroupQR::mousePressEvent(QMouseEvent *event)
{
	mMoveing = true;
	mMovePosition = event->globalPos() - pos();
	return QWidget::mousePressEvent(event);
}

void QGroupQR::mouseMoveEvent(QMouseEvent *event)
{
	if (mMoveing && (event->buttons() && Qt::LeftButton)
		&& (event->globalPos() - mMovePosition).manhattanLength() > QApplication::startDragDistance())
	{
		move(event->globalPos() - mMovePosition);
		mMovePosition = event->globalPos() - pos();
	}
	return QWidget::mouseMoveEvent(event);
}

void QGroupQR::mouseReleaseEvent(QMouseEvent *event)
{
	mMoveing = false;
	return QWidget::mouseReleaseEvent(event);
}

void QGroupQR::OpenImg(QPixmap img)
{
	if (img.isNull())
		return;

	QDesktopWidget *pDesk = QApplication::desktop();
	int mainIndex = pDesk->primaryScreen();
	QRect screen = pDesk->screenGeometry(mainIndex);

	int nheight = screen.height(), nwidth = screen.width();
	if (img.width() > nwidth && img.height() > nheight)
	{
		nheight = screen.height();
		nwidth = screen.width();
	}
	else if (img.width() > nwidth && img.height() < nheight)
	{
		nheight = img.height();
		nwidth = screen.width();
	}
	else if (img.width() < nwidth && img.height() > nheight)
	{
		nheight = screen.height();
		nwidth = img.width();
	}
	else{
		nheight = img.height();
		nwidth = img.width();
	}
	if (nwidth == 0 || nheight == 0)
	{
		nwidth = 480;
		nheight = 600;
	}
	ui->QRLabel->resize(nwidth, nheight);
	//resize(nwidth, nheight);
	ui->QRLabel->setAutoFillBackground(true);
	ui->QRLabel->setPixmap(img);
	ui->QRLabel->setScaledContents(true);
}

void QGroupQR::OnSetPicPath(QPixmap pix, int nType /*= 0*/)
{
	if (ui->HeaderLabel)
	{
		ui->HeaderLabel->setAutoFillBackground(true);
		ui->HeaderLabel->setPixmap(pix);
		ui->HeaderLabel->setScaledContents(true);
		//ui->HeaderLabel->setObjectName(strPath);
	}
}
void QGroupQR::SetNickName(QString strValue)
{
	if (ui->NameLabel)
	{
		ui->NameLabel->setText(strValue);
	}
}

void QGroupQR::SetIDLabel(QString strValue)
{
	if (ui->IDLabel)
	{
		ui->IDLabel->setText(strValue);
	}
}

void QGroupQR::changeEvent(QEvent * event)
{
	if (event->type() == QEvent::WindowStateChange)
	{
#ifdef Q_OS_WIN
		if (this->windowState() == Qt::WindowMaximized)
			shadow->hide();
		if (this->windowState() == Qt::WindowMinimized)
			shadow->hide();

		if (this->windowState() == Qt::WindowNoState)
			shadow->show();
#endif
	}

	QWidget::changeEvent(event);
}


void QGroupQR::moveEvent(QMoveEvent *event)
{
#ifdef Q_OS_WIN
	shadow->move(event->pos().x() - 10, event->pos().y() - 10);
#endif
}

void QGroupQR::resizeEvent(QResizeEvent* event)
{
#ifdef Q_OS_WIN
	shadow->resize(event->size().width() + 20, event->size().height() + 20);
#endif
}

void QGroupQR::show()
{
	QWidget::show();
#ifdef Q_OS_WIN
	shadow->resize(width() + 20, height() + 20);
	shadow->move(x() - 10, y() - 10);
	shadow->show();
#endif
#ifdef Q_OS_WIN
	// 窗口被激活了，强制重绘  
	InvalidateRect((HWND)this->winId(), nullptr, FALSE);
#endif
}

void QGroupQR::setLinuxCenter()
{
    window()->setGeometry
    (
            QStyle::alignedRect
            (
                    Qt::LeftToRight,
                    Qt::AlignCenter,
                    window()->size(),
                    qApp->desktop()->availableGeometry()
            )
    );
}

void QGroupQR::setNoteText(QString strText)
{
	ui->NoteLabel->setText(strText);
}
