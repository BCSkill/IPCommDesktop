﻿#include "myScene.h"

#include <QDebug>
#include <QGraphicsSceneMouseEvent>
#include <QPointF>
#include <QGraphicsSceneDragDropEvent>
#include <QDrag>
#include <math.h>
#include "pixitem.h"

//构造函数初始化了变量pix
// myScene::myScene()
// {
// 	pix = *pixmap;
// 	setAcceptDrops(false);   //设置可拖拽
// 	m_scaleValue = 0;
// 
// 	m_isMove = false;
/*}*/

//实现自己的图元边界函数
// QRectF PixItem::boundingRect() const
// {
// 	return QRectF(-pix.width() / 2, -pix.height() / 2,
// 		pix.width(), pix.height());
// }

//只需QPainter的drawPixmap()函数将图元图片绘出即可
// void PixItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *,
// 	QWidget *)
// {
// 	painter->drawPixmap(-pix.width() / 2, -pix.height() / 2, pix);
// }

//鼠标点击事件  局部缩放
void myScene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
// 	m_startPos = event->pos();
// 	m_isMove = true;
//	QGraphicsScene::mousePressEvent(event);
}

void myScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
// 	if (m_isMove)
// 	{
// 		QPointF point = event->pos() - m_startPos;
// 		//moveBy(point.x(), point.y());
// 	}
//	QGraphicsScene::mouseMoveEvent(event);
}

void myScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	//m_isMove = false;
//	QGraphicsScene::mouseReleaseEvent(event);
}

//使用滚轮整体缩放
void myScene::wheelEvent(QGraphicsSceneWheelEvent *event)
{
	//QGraphicsScene::wheelEvent(event);
}

void myScene::SpWhellEvent(int iScale,int iX,int iY)
{
	QList<QGraphicsItem *> vecItem = items();
	if (vecItem.size() > 0)
	{
		PixItem * pItem = (PixItem *)(vecItem.at(0));
		pItem->SpWheelevent(iScale, iX, iY);
	}
}