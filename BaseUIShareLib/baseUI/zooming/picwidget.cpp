﻿#include "picwidget.h"
#include "ui_picwidget.h"

#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <QPolygon>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QMessageBox>
#include <QTextCodec>
#include <QMovie>
#include <QDesktopWidget>
#include <QMenu>
#include <QClipboard>
#include <QTimer>
#include "QStringLiteralBak.h"

PicWidget::PicWidget(QWidget *parent) :
QWidget(parent),
ui(new Ui::PicWidget)
{
	ui->setupUi(this);
    pixItem = NULL;
    pMovie = NULL;
    pLabel = NULL;
	m_dWidth = 0;
	m_dHeight = 0;

	this->setAttribute(Qt::WA_DeleteOnClose);
	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog); 
	//setWindowModality(Qt::WindowModal);
	this->setAttribute(Qt::WA_TranslucentBackground);
	setWindowIcon(QIcon(":/Login/Resources/login/system.ico"));
#ifdef Q_OS_WIN
	this->setContentsMargins(6, 6, 6, 6);
#endif
	QFile style(":/QSS/Resources/QSS/BaseUIShareLib/picwidget.qss");
	style.open(QFile::ReadOnly);
	QString sheet = QLatin1String(style.readAll());
	setStyleSheet(sheet);
	style.close();

	connect(ui->minBtn, SIGNAL(clicked()), this, SLOT(showMinimized()));
	connect(ui->maxBtn, SIGNAL(clicked()), this, SLOT(maxOrRstrWindow()));
    connect(ui->closeBtn, SIGNAL(clicked()), this, SLOT(close()));

	connect(ui->pbt_zoomIn, SIGNAL(clicked(bool)), this, SLOT(onIn()));
	connect(ui->pbt_zoomOut, SIGNAL(clicked(bool)), this, SLOT(onOut()));
	connect(ui->pbt_save, SIGNAL(clicked(bool)), this, SLOT(onSave()));
	connect(ui->pbt_rotation, SIGNAL(clicked(bool)), this, SLOT(onRotation()));
	//////////////////////////////////////////////////////////////////////////
	setMinimumSize(600, 400);
    m_graphicsView = new myView();
	m_graphicsView->setObjectName("m_graphicsView");
	ui->gLauout_graphicsView->addWidget(m_graphicsView);     //将自定义的组件添加到布局中
    m_graphicsScene = new myScene();  //new 一个新的场景对象
	//m_graphicsScene->setSceneRect(-370, -250, 740, 500);     //限定场景对象的显示区域
	m_graphicsView->setScene(m_graphicsScene);          //将视图对象于场景相连

	//m_graphicsView->setStyleSheet("background: #042439;border:0px");
	m_graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	m_graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	m_strName = ".jpg";
	bIsGif = false;

	m_Menu = new QMenu(this);
	//复制
	ActCopy = new QAction(tr("Copy"), this);
	ActCopy->setIcon(QIcon(":/zooming/Resources/zooming/zomming_copy_black.png"));
	connect(ActCopy, SIGNAL(triggered()), this, SLOT(onCopy()));
	m_Menu->addAction(ActCopy);
	m_Menu->addSeparator();
	//另存为
	ActDown = new QAction(tr("Save as"), this);
	ActDown->setIcon(QIcon(":/zooming/Resources/zooming/zomming_download_black.png"));
	connect(ActDown, SIGNAL(triggered()), this, SLOT(onSave()));
	m_Menu->addAction(ActDown);
	m_Menu->addSeparator();
	//放大缩小旋转
	ActIn = new QAction(tr("Enlarge"), this);
	ActIn->setIcon(QIcon(":/zooming/Resources/zooming/zomming_more_black.png"));
	connect(ActIn, SIGNAL(triggered()), this, SLOT(onIn()));
	m_Menu->addAction(ActIn);
	ActOut= new QAction(tr("Reduce"), this);
	ActOut->setIcon(QIcon(":/zooming/Resources/zooming/zomming_less_black.png"));
	connect(ActOut, SIGNAL(triggered()), this, SLOT(onOut()));
	m_Menu->addAction(ActOut);
	ActR = new QAction(tr("Rotate"), this);
	ActR->setIcon(QIcon(":/zooming/Resources/zooming/zomming_rotation_black.png"));
	connect(ActR, SIGNAL(triggered()), this, SLOT(onRotation()));
	m_Menu->addAction(ActR);

	m_pTipLabel = new QLabel(this);
	m_pTipLabel->resize(120, 30);
	m_pTipLabel->setStyleSheet("background-color:rgba(99,99,99,100);border-radius:5px;color:white;font: 75 16pt 微软雅黑;font-size:14px");
	m_pTipLabel->setAlignment(Qt::AlignCenter);
	m_pTipLabel->hide();

	m_pLeftBtn = new QPushButton(this);
	m_pLeftBtn->setFixedSize(40, 40);
	m_pLeftBtn->setCursor(QCursor(Qt::PointingHandCursor));
	m_pLeftBtn->setStyleSheet("QPushButton{background-color:rgba(198,198,198,75);border-radius:20px}");
	m_pLeftBtn->setIcon(QIcon(":/zooming/Resources/zooming/zomming_left.png"));
	m_pLeftBtn->setToolTip(tr("Previous"));
	m_pRightBtn = new QPushButton(this);
	m_pRightBtn->setFixedSize(40, 40);
	m_pRightBtn->setCursor(QCursor(Qt::PointingHandCursor));
	m_pRightBtn->setStyleSheet("QPushButton{background-color:rgba(198,198,198,75);border-radius:20px}");
	m_pRightBtn->setIcon(QIcon(":/zooming/Resources/zooming/zomming_right.png"));
	m_pRightBtn->setToolTip(tr("Next"));
	m_pRightBtn->hide();
	m_pLeftBtn->hide();

	connect(m_graphicsView, SIGNAL(sigShowBtn(int)), this, SLOT(slotShowBtn(int)));
	connect(m_pLeftBtn, SIGNAL(clicked()), this, SLOT(slotLeft()));
	connect(m_pRightBtn, SIGNAL(clicked()), this, SLOT(slotRight()));

	this->setWindowIcon(QIcon(":/Ico/Resources/logo/logo.ico"));
}
#ifdef Q_OS_WIN
void PicWidget::paintEvent(QPaintEvent * event)
{
	QPainterPath path;
	path.setFillRule(Qt::WindingFill);
	path.addRect(6, 6, this->width() - 12, this->height() - 12);

	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.fillPath(path, QBrush(Qt::white));

	QColor color(0, 0, 0, 50);//, 50);
	for (int i = 0; i < 6; i++)
	{
		QPainterPath path;
		path.setFillRule(Qt::WindingFill);
		path.addRect(6 - i, 6 - i, this->width() - (6 - i) * 2, this->height() - (6 - i) * 2);
		color.setAlpha(110 - qSqrt(i) * 40);
		painter.setPen(color);
		painter.drawPath(path);
	}
}
#endif
PicWidget::~PicWidget()
{
    if(pMovie)
    {
        pMovie->stop();
        delete pMovie;
        pMovie = NULL;
    }
    if(pLabel)
    {
        delete pLabel;
        pLabel = NULL;
    }
    if(pixItem)
    {
        delete pixItem;
        pixItem = NULL;
    }
    if(m_graphicsScene)
    {
        m_graphicsScene->clear();
        delete m_graphicsScene;
        m_graphicsScene = NULL;
    }
    if(m_graphicsView)
    {
        delete m_graphicsView;
        m_graphicsView = NULL;
    }
	if (ActR)
	{
		delete ActR;
		ActR = NULL;
	}
	if (ActIn)
	{
		delete ActIn;
		ActIn = NULL;
	}
	if (ActOut)
	{
		delete ActOut;
		ActOut = NULL;
	}
	if (ActDown)
	{
		delete ActDown;
		ActDown = NULL;
	}
	if (ActCopy)
	{
		delete ActCopy;
		ActCopy = NULL;
	}
	if (m_Menu)
	{
		delete m_Menu;
		m_Menu = NULL;
	}
	if (m_pLeftBtn)
	{
		delete m_pLeftBtn;
		m_pLeftBtn = NULL;
	}
		//delete pTimer;
    delete ui;
}
void PicWidget::onSave()
{
	if (bIsGif == true)
	{
		if (pMovie == NULL)
		{
			return;
		}
	}
	else
	{
		if (pixItem != NULL)
		{
			if (pixItem->GetPix().isNull())
			{
				return;
			}
		}
		else
		{
			return;
		}
	}
	QString filename = QFileDialog::getSaveFileName(this,
		tr("Save picture"),
		tr("Untitled") + m_strName,
		m_strName);
	if (filename.isEmpty())
	{
		return;
	}
	else
	{
		QString strSuf = QFileInfo(filename).suffix();
		if (strSuf.toLower()!="png")  //若后缀为空自动添加png后缀  
		{
			filename.append(m_strName);
		}
		if (bIsGif)
		{
			QFile::copy(m_strPath, filename);
		}
		else
		{
			pixItem->GetPix().save(filename);
		}
	}
}

//放大图片
void PicWidget::onIn()
{
	if (pixItem == NULL)
	{
		return;
	}
	pixItem->setScaleValue(pixItem->getScaleValue() + 2, 1);
	pixItem->setZoomState(NO_STATE);
}

//缩小图片
void PicWidget::onOut()
{
	if (pixItem == NULL)
	{
		return;
	}
	pixItem->setScaleValue(pixItem->getScaleValue() - 2,-1);
	pixItem->setZoomState(NO_STATE);
}

void PicWidget::onRotation()
{
	double iWidth = pixItem->GetPix().width();
	double iHeight = pixItem->GetPix().height();
	double dXScale = 1;
	double dYScale = 1;
	int iRo = pixItem->rotation();
	if (iRo % 180 == 0)
	{
		dXScale = m_dHeight / iHeight;
		dYScale = m_dWidth / iWidth;
	}
	else
	{
		dXScale = m_dWidth / iWidth;
		dYScale = m_dHeight / iHeight;
	}

	if (dXScale < 1 || dYScale < 1)
	{
		double dScale = dXScale < dYScale ? dXScale : dYScale;
		double dRealScal = 1;
		int i = 0;
		while (dRealScal>dScale)
		{
			dRealScal /= 1.1;
			i--;
		}
		pixItem->setScaleValue(i, 0);
		//pixItem->setScale(dRealScal);
	}
	else
	{
		pixItem->setScaleValue(0, 0);
	}
	QPointF centerPos = pixItem->boundingRect().center();
	pixItem->setTransformOriginPoint(centerPos);
	pixItem->setRotation(pixItem->rotation() + 90);
}

void PicWidget::onCopy()
{
	if (pixItem)
	{
		QClipboard*clipBoard = QApplication::clipboard();
		clipBoard->setPixmap(pixItem->GetPix());
	}

}

////复位图片
//void PicWidget::on_pbt_reset_clicked()
//{
//// 	pixItem->setScaleValue(0);
//// 	pixItem->setZoomState(RESET);
//// 	pixItem->setPos(0, 0);
//// 	m_graphicsView->resetMatrix(); //重置图片
//}
//
////切换图片
//void PicWidget::on_pbt_changeImage_clicked()
//{
//// 	QString fileName = QFileDialog::getOpenFileName(
//// 		this, tr("open image file"),
//// 		"./", tr("Image files(*.bmp *.jpg *.pbm *.pgm *.png *.ppm *.xbm *.xpm);;All files (*.*)"));
//// 	if (fileName.isEmpty())
//// 	{
//// 		//QMessageBox::warning(this, tr("警告"), tr("打开图片失败，请重新选择图片！"),
//// 		//	NULL, QMessageBox::Yes);
//// 		return;
//// 	}
//// 
//// 	m_graphicsScene->removeItem(pixItem);   //将上一个图元从场景中移除,重新添加新的图元
//// 	QPixmap *pixmap = new QPixmap(fileName);
//// 	pixItem = new PixItem(pixmap);          //实例化自定义类的一个对象,传入一个图片用于显示
//// 	m_graphicsScene->addItem(pixItem);      //将该图元对象添加到场景中，并设置此图元在场景中的位置为中心(0,0)
//// 	pixItem->setPos(0, 0);
//// 	on_pbt_reset_clicked();
//}
//
////点击处于【放大状态】后，点击图片才有放大的效果
//void PicWidget::on_pbt_setZoomInState_clicked()
//{
//	pixItem->setZoomState(ZOOM_IN);
//}
//
////点击处于【缩小状态】后，点击图片才有所缩小的效果
//void PicWidget::on_pbt_setZoomOutState_clicked()
//{
//	pixItem->setZoomState(ZOOM_OUT);
//}

void PicWidget::setPath(QString strPath,QWidget * pParentWidget,int iType)
{
	iShowBtn = iType;
	if (m_graphicsScene == NULL || m_graphicsView == NULL)
	{
		return;
	}
	//strPath = decodeURI(strPath);
	if (!QFileInfo(strPath).suffix().isEmpty())
	{
		if (QFileInfo(strPath).suffix().toLower() != "gif")
		{
			m_strName = ".png";
		}
		else
		{
			m_strName = ".gif";
		}
		//m_strName = "."+QFileInfo(strPath).suffix();
		m_strPath = strPath;
	}
	QPoint pos = QWidget::mapToGlobal(pParentWidget->pos());
	QRect deskRt = QApplication::desktop()->availableGeometry(pos);
	double g_nActScreenW = deskRt.width()*0.8;
	double g_nActScreenH = deskRt.height()*0.8;
	//gif不支持缩放
	if (m_strName == ".gif")
	{
        pLabel = new QLabel();
		pMovie = new QMovie(strPath,QByteArray(),this);
		if (pLabel == NULL || pMovie == NULL)
		{
			return;
		}
		pLabel->setMovie(pMovie);
		pMovie->start();

        m_graphicsScene->addWidget(pLabel);
		ui->pbt_zoomIn->hide();
		ui->pbt_zoomOut->hide();
		ui->pbt_rotation->hide();
		bIsGif = true;
		m_graphicsView->setGif(bIsGif);

		QRect rsfg = pMovie->frameRect();
		m_dHeight = rsfg.height();
		m_dWidth = rsfg.width();
		resize(rsfg.width() + 12, rsfg.height() + 92);
	}
	//静态图片在保持最小尺寸和不超过屏幕的前提下尽量100%大小展示
	else
	{
		QByteArray arr;
		QFile file(strPath);
		if (file.open(QIODevice::ReadOnly))
		{
			arr = file.readAll();
		}
		file.close();

		QImage Tmpimg;
		Tmpimg.loadFromData(arr);
		//ios角度问题
		QImageReader reader;
		reader.setDecideFormatFromContent(true);
		reader.setAutoTransform(true);
		reader.setFileName(strPath);
		Tmpimg = reader.read();
		QPixmap img = QPixmap::fromImage(Tmpimg);
		//将该图元对象添加到场景中，并设置此图元在场景中的位置为中心（0，0）
		pixItem = new PixItem(&img);
		if (pixItem == NULL)
		{
			return;
		}
		m_graphicsScene->addItem(pixItem);
		pixItem->setPos(0, 0);
		double dWidth = img.width();
		double dHeight = img.height();
		double dXScale = g_nActScreenW / dWidth;
		double dYScale = g_nActScreenH / dHeight;
		int iWidth = (dWidth + 1) / 1;
		int iHeight = (dHeight + 1) / 1;
		if (dXScale < 1 || dYScale < 1)
		{
			double dScale = dXScale < dYScale ? dXScale : dYScale;
			double dRealScal = 1;
			int i = 0;
			while (dRealScal>dScale)
			{
				dRealScal /= 1.1;
				i--;
			}
			dWidth *= dRealScal;
			dHeight *= dRealScal;
			iWidth = (dWidth + 1) / 1;
			iHeight = (dHeight + 1) / 1;
			pixItem->setScaleValue(i, 0);
			//pixItem->setScale(dRealScal);
		}
		resize(iWidth + 12, iHeight + 92);
		m_dHeight = iHeight;
		m_dWidth = iWidth;
		//最小为600*400
		if (iHeight < 400)iHeight = 400;
		if (iWidth < 600)iWidth = 600;
		//标题栏尺寸纳入计算
		m_pLeftBtn->move(26, iHeight / 2 -16);
		m_pRightBtn->move(iWidth - 66, iHeight / 2 - 16);
		bIsGif = false;
		m_graphicsView->setGif(bIsGif);
	}
	move(deskRt.x() + (deskRt.width() - this->width()) / 2, deskRt.y() + (deskRt.height() - this->height()) / 2);
}

//鼠标事件的处理。
void PicWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	//dateSelector->hide();   //隐藏日期选择器。

	if (event->button() == Qt::RightButton)
	{
		if (m_Menu->actions().count() > 0)
			m_Menu->exec(event->globalPos());
	}
	return QWidget::mousePressEvent(event);
}
void PicWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void PicWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);

	return QWidget::mouseMoveEvent(event);
}

void PicWidget::maxOrRstrWindow()
{
	if (this->isFullScreen())   //当前窗口已经最大化，进行还原。
	{
		this->showNormal();
		//将按钮图标变为最大化。
		ui->maxBtn->setStyleSheet("QPushButton#maxBtn{border-image:url(:/PerChat/Resources/person/maxsize04.png);}"
			"QPushButton#maxBtn:hover{border-image:url(:/PerChat/Resources/person/maxsize36.png);}");
	}
	else                        //当前尺寸为原始尺寸，因此执行最大化操作。                      
	{
		this->showFullScreen();
		//将按钮图标变为还原。
		ui->maxBtn->setStyleSheet("QPushButton#maxBtn{border-image:url(:/PerChat/Resources/person/rstrsize04.png);}"
			"QPushButton#maxBtn:hover{border-image:url(:/PerChat/Resources/person/rstrsize36.png);}");
	}
}

QString PicWidget::decodeURI(QString str)
{
	QByteArray array;
	for (int i = 0; i < str.length();) {
		if (0 == QString::compare(str.mid(i, 1), QString("%"))) {
			if ((i + 2) < str.length()) {
				array.append(str.mid(i + 1, 2).toShort(0, 16));
				i = i + 3;
			}
			else{
				array.append(str.mid(i, 1));
				i++;
			}
		}
		else{
			array.append(str.mid(i, 1));
			i++;
		}
	}
	QTextCodec *code = QTextCodec::codecForName("UTF-8");
	return code->toUnicode(array);
}
void PicWidget::paintgif()
{
	pixItem->paintgif();
}

void PicWidget::closeEvent(QCloseEvent * event)
{
	emit sigPicClose();
	QWidget::closeEvent(event);
}

void PicWidget::keyPressEvent(QKeyEvent *event)//Ctrl+C
{
	if (event->key() == Qt::Key_C  &&  event->modifiers() == (Qt::ControlModifier))
	{
		onCopy();
	}
}
void PicWidget::slotShowBtn(int iType)
{
	if (iShowBtn == 1)
	{
		return;
	}
	if (iType == 0)
	{
		m_pLeftBtn->hide();
		m_pRightBtn->hide();
	}
	else if (iType == 1)
	{
		m_pLeftBtn->show();
		m_pRightBtn->hide();
	}
	else if (iType == 2)
	{
		m_pRightBtn->show();
		m_pLeftBtn->hide();
	}
}

void PicWidget::resizeEvent(QResizeEvent * event)
{
	QRect rectW = this->rect();
	m_pLeftBtn->move(26, rectW.height() / 2 - 16);
	m_pRightBtn->move(rectW.width() - 66, rectW.height() / 2 - 16);
	m_pTipLabel->move(rectW.width()/2-65, rectW.height() / 2 - 16);
	QWidget::resizeEvent(event);
}

void PicWidget::resetPath(QString strPath)
{
	if (m_graphicsScene == NULL || m_graphicsView == NULL)
	{
		return;
	}
	if (pLabel)
	{
		delete pLabel;
		pLabel = NULL;
	}
	if (pMovie)
	{
		delete pMovie;
		pMovie = NULL;
	}
	if (pixItem)
	{
		delete pixItem;
		pixItem = NULL;
	}
	if (m_graphicsScene)
	{
		delete m_graphicsScene;
		m_graphicsScene = NULL;
	}
	m_graphicsScene = new myScene();
	m_graphicsView->setScene(m_graphicsScene);

	//strPath = decodeURI(strPath);
	if (!QFileInfo(strPath).suffix().isEmpty())
	{
		if (QFileInfo(strPath).suffix().toLower() != "gif")
		{
			m_strName = ".png";
		}
		else
		{
			m_strName = ".gif";
		}
		//m_strName = "."+QFileInfo(strPath).suffix();
		m_strPath = strPath;
	}
	if (m_strName == ".gif")
	{

		pLabel = new QLabel();
		pMovie = new QMovie(strPath, QByteArray(), this);
		if (pLabel == NULL || pMovie == NULL)
		{
			return;
		}
		pLabel->setMovie(pMovie);
		pMovie->start();

		m_graphicsScene->addWidget(pLabel);
		ui->pbt_zoomIn->hide();
		ui->pbt_zoomOut->hide();
		ui->pbt_rotation->hide();
		bIsGif = true;
		m_graphicsView->setGif(bIsGif);

		QRect rsfg = pMovie->frameRect();
		//resize(rsfg.width(), rsfg.height() + 80);
	}
	else
	{
		ui->pbt_zoomIn->show();
		ui->pbt_zoomOut->show();
		ui->pbt_rotation->show();
		QByteArray arr;
		QFile file(strPath);
		if (file.open(QIODevice::ReadOnly))
		{
			arr = file.readAll();
		}
		file.close();

		QImage Tmpimg;
		Tmpimg.loadFromData(arr);
		//ios角度问题
		QImageReader reader;
		reader.setDecideFormatFromContent(true);
		reader.setAutoTransform(true);
		reader.setFileName(strPath);
		Tmpimg = reader.read();
		QPixmap img = QPixmap::fromImage(Tmpimg);
		//将该图元对象添加到场景中，并设置此图元在场景中的位置为中心（0，0）
		pixItem = new PixItem(&img);
		if (pixItem == NULL)
		{
			return;
		}
		double dTmpW = m_dWidth > 588 ? m_dWidth : 588;
		double dTmpH = m_dHeight > 308 ? m_dHeight : 308;
		m_graphicsScene->addItem(pixItem);
		pixItem->setPos(0, 0);
		double dWidth = img.width();
		double dHeight = img.height();
		double dXScale = dTmpW / dWidth;
		double dYScale = dTmpH / dHeight;
		int iWidth = (dWidth + 1) / 1;
		int iHeight = (dHeight + 1) / 1;
		if (dXScale < 1 || dYScale < 1)
		{
			double dScale = dXScale < dYScale ? dXScale : dYScale;
			double dRealScal = 1;
			int i = 0;
			while (dRealScal>dScale)
			{
				dRealScal /= 1.1;
				i--;
			}
			dWidth *= dRealScal;
			dHeight *= dRealScal;
			iWidth = (dWidth + 1) / 1;
			iHeight = (dHeight + 1) / 1;
			pixItem->setScaleValue(i, 0);
			//pixItem->setScale(dRealScal);
		}
		bIsGif = false;
		m_graphicsView->setGif(bIsGif);
	}
}

void PicWidget::slotLeft()
{
	emit sigChangePic(0);
}

void PicWidget::slotRight()
{
	emit sigChangePic(1);
}

void PicWidget::setNoteTip(int iType)
{
	if (iType == 0)
	{
		//正常状态
		m_pLeftBtn->setToolTip(tr("Previous"));
		m_pRightBtn->setToolTip(tr("Next"));
	
	}
	else if (iType == 1)
	{
		//第一张
		m_pTipLabel->setText(tr("It's the first one"));
		m_pLeftBtn->setToolTip(tr("It's the first one"));
		m_pTipLabel->show();
		QTimer::singleShot(1000, this, SLOT(slotHideLabel()));
	}
	else if (iType == 2)
	{
		//最后一张
		m_pTipLabel->setText(tr("It's the last one"));
		m_pRightBtn->setToolTip(tr("It's the last one"));
		m_pTipLabel->show();
		QTimer::singleShot(1000, this, SLOT(slotHideLabel()));
	}
}

void PicWidget::slotHideLabel()
{
	m_pTipLabel->hide();
}
