﻿#include "myView.h"

#include <QDebug>
#include <QPointF>
#include <QGraphicsSceneDragDropEvent>
#include <QDrag>
#include "myScene.h"
#include <QGraphicsItem>

myView::myView(QWidget *parent)
: QGraphicsView(parent),
m_bMouseTranslate(false)
{
	this->setMouseTracking(true);
}
//鼠标点击事件  局部缩放
void myView::mousePressEvent(QMouseEvent *event)
{
	m_bMouseTranslate = true;
	m_lastMousePos = event->pos();

	QGraphicsView::mousePressEvent(event);
}

void myView::mouseMoveEvent(QMouseEvent *event)
{
	if (m_bMouseTranslate){
		QPointF mouseDelta = mapToScene(event->pos()) - mapToScene(m_lastMousePos);
		translate(mouseDelta);
	}
	
	m_lastMousePos = event->pos();
	if (m_lastMousePos.x() < 100)
	{
		emit sigShowBtn(1);
	}
	else if (m_lastMousePos.x() > viewport()->rect().width() - 100)
	{
		emit sigShowBtn(2);
	}
	else
	{
		emit sigShowBtn(0);
	}

	QGraphicsView::mouseMoveEvent(event);
}

void myView::mouseReleaseEvent(QMouseEvent *event)
{
	m_bMouseTranslate = false;

	QGraphicsView::mouseReleaseEvent(event);
}

//使用滚轮整体缩放
void myView::wheelEvent(QWheelEvent *event)
{
	if (bIsGif)
	{
		return;
	}
	//return QGraphicsView::wheelEvent(event);
	QRectF pRect = sceneRect();
	myScene* pScene = (myScene*)scene();
	int iScale = -1;
	if (event->delta() > 0)
	{
		iScale = 1;
	}
	int iX, iY;
	iX = event->pos().x();
	iY = event->pos().y();
	pScene->SpWhellEvent(iScale,iX,iY);
 	//pScene->wheelEvent((QGraphicsSceneWheelEvent)event);
// 	QGraphicsView::wheelEvent(event);
}
void myView::setGif(bool bValue)
{
	bIsGif = bValue;
}

void myView::translate(QPointF delta)
{
	QRectF tmpR = sceneRect();
	// view 根据鼠标下的点作为锚点来定位 scene
	setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
	QRect rTmp = viewport()->rect();
	QPoint newCenter(viewport()->rect().width()/2 - delta.x(), viewport()->rect().height()/2 - delta.y());
	centerOn(mapToScene(newCenter));
	// scene 在 view 的中心点作为锚点
	setTransformationAnchor(QGraphicsView::AnchorViewCenter);
}