﻿#include "floatbuttonwidget.h"
#include "ui_floatbuttonwidget.h"

FloatButtonWidget::FloatButtonWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::FloatButtonWidget();
	ui->setupUi(this);

	setWindowFlags(Qt::FramelessWindowHint);
	//this->setAttribute(Qt::WA_TranslucentBackground);
}

FloatButtonWidget::~FloatButtonWidget()
{
	delete ui;
}

void FloatButtonWidget::enterEvent(QEvent *)
{
	emit sigMoveIn();
}
void FloatButtonWidget::leaveEvent(QEvent *)
{
	emit sigMoveOut();
}

void FloatButtonWidget::setButtonStyle(QString strStyle,QString strIcon)
{
	ui->pushButton->setStyleSheet(strStyle);
	ui->pushButton->setIcon(QIcon(strIcon));
}