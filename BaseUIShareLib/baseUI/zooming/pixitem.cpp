﻿#include "pixitem.h"

#include <QDebug>
#include <QGraphicsSceneMouseEvent>
#include <QPointF>
#include <QGraphicsSceneDragDropEvent>
#include <QDrag>
#include <QDesktopWidget>
#include <QApplication>
#include <math.h>
#include <QGraphicsScene>


//构造函数初始化了变量pix
PixItem::PixItem(QPixmap *pixmap)
{
	pix = *pixmap;
	setAcceptDrops(true);   //设置可拖拽
	m_scaleValue = 0;

	m_isMove = false;
	m_bIsGif = false;
}

PixItem::PixItem(QMovie *pMovie)
{
	m_Movie = pMovie;
	setAcceptDrops(true);   //设置可拖拽
	m_scaleValue = 0;

	m_isMove = false;
	m_bIsGif = true;
}

//实现自己的图元边界函数
QRectF PixItem::boundingRect() const
{
	//return QGraphicsItem::boundingRect();
	if (m_bIsGif)
	{
		QRect tmpSize = m_Movie->frameRect();
		return QRectF(-tmpSize.width() / 2, tmpSize.height() / 2, tmpSize.width(), tmpSize.height());
	}
	else
	{
		int iw = pix.width();
		int ih = pix.height();
		return QRectF(-pix.width() / 2, -pix.height() / 2,
			pix.width(), pix.height());
	}
}

//只需QPainter的drawPixmap()函数将图元图片绘出即可
void PixItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *,
	QWidget *)
{
	if (m_bIsGif)
	{
		if (m_Movie !=  NULL && m_Movie->state() == QMovie::Running)
		{
			QRectF bound = boundingRect().adjusted(10, 10, -5, -5);
			painter->drawImage(bound, m_Movie->currentImage());
		}
	}
	else
	{
		painter->drawPixmap(-pix.width() / 2, -pix.height() / 2, pix);
	}
}

//鼠标点击事件  局部缩放
void PixItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	m_startPos = event->pos();
	m_isMove = true;

	int scaleValue = m_scaleValue;
	if (m_zoomState == ZOOM_IN)       //局部放大
	{
		scaleValue++;
	}
	else if (m_zoomState == ZOOM_OUT)        //局部缩小
	{
		scaleValue--;
	}

	if (scaleValue > ZOOM_IN_TIMES || scaleValue < ZOOM_OUT_TIMES)
		return;

	if (m_scaleValue != scaleValue)
	{
		setTransformOriginPoint(event->pos().x(), event->pos().y());
	}
	m_scaleValue = scaleValue;
	qreal s;
	//实现局部缩放
	if (m_scaleValue > 0)
	{
		s = pow(1.1, m_scaleValue);        //放大 计算x的y方次 参数都是double类型
	}
	else
	{
		s = pow(1 / 1.1, -m_scaleValue);      //缩小
	}
	setScale(s);
}

void PixItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
	if (m_isMove)
	{
		qreal s;
		if (m_scaleValue > 0)
		{
			s = pow(1.1, m_scaleValue);        //放大 计算x的y方次 参数都是double类型
		}
		else
		{
			s = pow(1 / 1.1, -m_scaleValue);      //缩小
		}
		QPointF point = event->pos() - m_startPos;
		moveBy(point.x(), point.y());
	}
}

void PixItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *)
{
	m_isMove = false;
}

//使用滚轮整体缩放
void PixItem::wheelEvent(QGraphicsSceneWheelEvent *event)
{
	int iWeight = pix.width();
	int iHeight = pix.height();

	QDesktopWidget* desktopWidget = QApplication::desktop();
	QRect screenRect = desktopWidget->screenGeometry();
	double g_nActScreenW = screenRect.width();
	double g_nActScreenH = screenRect.height();
	//setZoomState(NO_STATE);

	int scaleValue = m_scaleValue;
	if (event->delta() > 0)  //delta（）为正，滚轮向上滚
	{
		scaleValue++;
	}
	else
	{
		scaleValue--;
	}

// 	if (scaleValue > ZOOM_IN_TIMES || scaleValue < ZOOM_OUT_TIMES)
// 		return;


	qreal s;
	if (scaleValue > 0)
	{
		s = pow(1.1, scaleValue);        //放大 计算x的y方次 参数都是double类型
	}
	else
	{
		s = pow(1 / 1.1, -scaleValue);      //缩小
	}
	if (iWeight*s > g_nActScreenW * 2 || iWeight*s<g_nActScreenW / 8 || iHeight*s>g_nActScreenH * 2 || iHeight < g_nActScreenH / 8)
	{
		return;
	}
	m_scaleValue = scaleValue;
	setScale(s);
	int iX = event->pos().x();
	int iY = event->pos().y();
	setTransformOriginPoint(iX,iY);
}

//从widget获取的缩放值，用于同步滚轮和按键
void PixItem::setScaleValue(const int &scaleValue,int imove)
{
	int iWeight = pix.width();
	int iHeight = pix.height();

	QDesktopWidget* desktopWidget = QApplication::desktop();
	QRect screenRect = desktopWidget->screenGeometry();
	double g_nActScreenW = screenRect.width();
	double g_nActScreenH = screenRect.height();


// 	if (scaleValue > ZOOM_IN_TIMES || scaleValue < ZOOM_OUT_TIMES)
// 		return;

	qreal s;
	if (scaleValue > 0)
	{
		s = pow(1.1, scaleValue);        //放大 计算x的y方次 参数都是double类型
	}
	else
	{
		s = pow(1 / 1.1, -scaleValue);      //缩小
	}
	if (imove > 0 && scaleValue>20)
	{
		return;
	}
	if (imove < 0 && scaleValue<-20)
	{
		return;
	}

	m_scaleValue = scaleValue;
	setScale(s);

	QRectF tmpRect = scene()->sceneRect();
	//QRectF tmpRect2 = m_graphicsScene->sceneRect();
	QPointF tmpPoint = pos();
	int iw = GetPix().width()*s;
	int ih = GetPix().height()*s;
	scene()->setSceneRect(tmpPoint.rx()-iw/2, tmpPoint.ry()-ih/2, iw, ih);
}

void PixItem::setZoomState(const int &zoomState)
{
	m_zoomState = zoomState;
	if (m_zoomState == RESET)
	{
		m_scaleValue = 0;
		setScale(1);
		setTransformOriginPoint(0, 0);
	}
}

int PixItem::getScaleValue() const
{
	return m_scaleValue;
}

void PixItem::SpWheelevent(int iScale, int iX, int iY)
{
	int iWeight = pix.width();
	int iHeight = pix.height();

	QDesktopWidget* desktopWidget = QApplication::desktop();
	QRect screenRect = desktopWidget->screenGeometry();
	double g_nActScreenW = screenRect.width();
	double g_nActScreenH = screenRect.height();

	QPointF p1 = pos();
	QPointF p2 = scenePos();
// 	QGraphicsScene * pScene = scene();
// 	QRectF p3 = pScene->sceneRect();
	iX -= 370;
	iY -= 260;
// 	if (abs(iX) > pix.width() || abs(iY)>pix.height() / 2)
// 	{
		iX = 0;
		iY = 0;
//	}
	setZoomState(NO_STATE);

	int scaleValue = m_scaleValue;
	if (iScale > 0)  //delta（）为正，滚轮向上滚
	{
		scaleValue++;
	}
	else
	{
		scaleValue--;
	}

// 	if (scaleValue > ZOOM_IN_TIMES || scaleValue < ZOOM_OUT_TIMES)
// 		return;

	
	qreal s;
	if (scaleValue > 0)
	{
		s = pow(1.1, scaleValue);        //放大 计算x的y方次 参数都是double类型
	}
	else
	{
		s = pow(1 / 1.1, -scaleValue);      //缩小
	}
	if (iScale > 0 && scaleValue>20)
	{
		return;
	}
	if (iScale<0&& scaleValue<-20)
	{
		return;
	}
	m_scaleValue = scaleValue;
	setScale(s);
	setTransformOriginPoint(iX, iY);

	QRectF tmpRect = scene()->sceneRect();
	//QRectF tmpRect2 = m_graphicsScene->sceneRect();
	QPointF tmpPoint = pos();
	int iw = GetPix().width()*s;
	int ih = GetPix().height()*s;
	scene()->setSceneRect(tmpPoint.rx() - iw / 2, tmpPoint.ry() - ih / 2, iw, ih);
}

void PixItem::paintgif()
{
	update();
}
