﻿#include "inputbox.h"
#include "ui_inputbox.h"

InputBox::InputBox(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::InputBox();
	ui->setupUi(this);

	m_bEmpty = false;

	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog | Qt::WindowStaysOnTopHint);// | Qt::Tool);
	setWindowModality(Qt::WindowModal);
	setAttribute(Qt::WA_TranslucentBackground);
	setAttribute(Qt::WA_DeleteOnClose);
	setWindowIcon(QIcon(":/Login/Resources/login/system.ico"));

	QFile style(":/QSS/Resources/QSS/BaseUIShareLib/inputbox.qss");
	style.open(QFile::ReadOnly);
	QString sheet = QLatin1String(style.readAll());
	this->setStyleSheet(sheet);
	style.close();

	connect(ui->closeBtn, SIGNAL(clicked(bool)), this, SLOT(slotClose()));
	connect(ui->enterBtn, SIGNAL(clicked(bool)), this, SLOT(slotEnter()));
}

InputBox::~InputBox()
{
	delete ui;
}

//鼠标事件的处理。
void InputBox::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void InputBox::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void InputBox::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

//绘制阴影
void InputBox::paintEvent(QPaintEvent * event)
{
	QPainterPath path;
	path.setFillRule(Qt::WindingFill);
	path.addRect(6, 6, this->width() - 12, this->height() - 12);

	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.fillPath(path, QBrush(Qt::white));

	QColor color(0, 0, 0, 50);//, 50);
	for (int i = 0; i < 6; i++)
	{
		QPainterPath path;
		path.setFillRule(Qt::WindingFill);
		path.addRect(6 - i, 6 - i, this->width() - (6 - i) * 2, this->height() - (6 - i) * 2);
		color.setAlpha(110 - qSqrt(i) * 40);
		painter.setPen(color);
		painter.drawPath(path);
	}
}

void InputBox::tip(QWidget *parent, QString content, bool isNumber, QLineEdit::EchoMode mode)
{
	InputBox *box = new InputBox(parent);
	box->init(content, isNumber, mode);
}

void InputBox::init(QString content, bool isNumber, QLineEdit::EchoMode mode)
{
	setWindowTitle(QCoreApplication::applicationName());
	ui->contentLabel->setText(content);
	ui->lineEdit->setEchoMode(mode);

	if (isNumber)
	{
		ui->lineEdit->setValidator(new QDoubleValidator(0, 10000, 2, this));
	}
	this->show();
}

void InputBox::slotClose()
{
	emit sigClose();
	this->close();
}

void InputBox::slotEnter()
{
	QString text = ui->lineEdit->text();

	if (!text.isEmpty())
	{
		emit sigEnter(text);
		slotClose();
	}
	else if (m_bEmpty)
	{
		text = "";
		emit sigEnter(text);
		slotClose();
	}
}

void InputBox::setLineText(QString strValue,QString strBack)
{
	ui->lineEdit->setText(strValue);
	ui->lineEdit->setPlaceholderText(strBack);
}

void InputBox::setEmpty(bool bVal)
{
	m_bEmpty = bVal;
}

void InputBox::setEditFoucs()
{
	ui->lineEdit->setFocus();
}