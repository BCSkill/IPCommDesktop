﻿#include "messagebox.h"
#include "ui_messagebox.h"
#include "QStringLiteralBak.h"
#include <qdebug.h>
IMessageBox::IMessageBox(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::IMessageBox();
	ui->setupUi(this);

	QFile style(":/QSS/Resources/QSS/BaseUIShareLib/messagebox.qss");
	style.open(QFile::ReadOnly);
	QString sheet = QLatin1String(style.readAll());
	this->setStyleSheet(sheet);
	style.close();

	setContentsMargins(6, 6, 6, 6);
	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog | Qt::Tool | Qt::WindowStaysOnTopHint);
	setWindowModality(Qt::WindowModal);
	setAttribute(Qt::WA_TranslucentBackground);
	setAttribute(Qt::WA_DeleteOnClose);

	connect(ui->closeBtn, SIGNAL(clicked(bool)), this, SLOT(slotClose()));
	connect(ui->enterBtn, SIGNAL(clicked(bool)), this, SLOT(slotClose()));
	connect(ui->okBtn, SIGNAL(clicked(bool)), this, SLOT(slotOK()));
	ui->okBtn->hide();
}

IMessageBox::~IMessageBox()
{
	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

void IMessageBox::tip(QWidget *parent, QString title, QString text)
{
	IMessageBox *box = new IMessageBox(parent);
	box->init(title, text);
}

void IMessageBox::askTip(QWidget *parent, QString title, QString text)
{
	IMessageBox *box = new IMessageBox(parent);
	box->initAsk(title, text);
}

void IMessageBox::init(QString title, QString text)
{
	ui->titleLabel->setText(title);
	ui->tipLabel->setText(text);
	this->show();
}

void IMessageBox::initAsk(QString title, QString text)
{
	ui->titleLabel->setText(title);
	ui->tipLabel->setText(text);
	ui->okBtn->show();
	ui->okBtn->setText(tr("OK"));
	ui->enterBtn->setText(tr("Cancel"));
	this->show();
}

//鼠标事件的处理。
void IMessageBox::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void IMessageBox::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void IMessageBox::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

//绘制阴影
void IMessageBox::paintEvent(QPaintEvent * event)
{
	QPainterPath path;
	path.setFillRule(Qt::WindingFill);
	path.addRect(10, 10, this->width() - 20, this->height() - 20);

	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.fillPath(path, QBrush(Qt::white));

	QColor color(0, 0, 0, 50);//, 50);
	for (int i = 0; i < 10; i++)
	{
		QPainterPath path;
		path.setFillRule(Qt::WindingFill);
		path.addRect(10 - i, 10 - i, this->width() - (10 - i) * 2, this->height() - (10 - i) * 2);
		color.setAlpha(220 - qSqrt(i) * 67);
		painter.setPen(color);
		painter.drawPath(path);
	}
}

void IMessageBox::slotClose()
{
	emit sigClose();
	this->close();
	this->~IMessageBox();
}

void IMessageBox::slotOK()
{
	emit sigOK();
	this->close();
	this->~IMessageBox();
}
