﻿#include "qloadingwidget.h"
#include "ui_qloadingwidget.h"
#include <QMovie>
#include "QStringLiteralBak.h"

QLoadingWidget::QLoadingWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::QLoadingWidget();
	ui->setupUi(this);

	this->setAttribute(Qt::WA_DeleteOnClose);
	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog);//
	setWindowModality(Qt::WindowModal);
	this->setAttribute(Qt::WA_TranslucentBackground);

	m_pMovie = new QMovie(":/searchWidget/Resources/searchWidget/load.gif", QByteArray(), this);
	if (ui->GifLabel == NULL || m_pMovie == NULL)
	{
		return;
	}
	ui->GifLabel->setMovie(m_pMovie);
	m_pMovie->setScaledSize(QSize(62, 62));
	m_pMovie->start();
	ui->TextLable->setText(tr("Loading"));
	ui->TextLable->setAlignment(Qt::AlignCenter);
}

QLoadingWidget::~QLoadingWidget()
{
	delete ui;
}

