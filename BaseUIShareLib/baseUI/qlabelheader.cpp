﻿#include "qlabelheader.h"
#include <QPainter>
#include <QBitmap>

QLabelHeader::QLabelHeader(QWidget *parent)
	: QLabel(parent)
{
	shape = ellipse;
	smooth = true;
}

QLabelHeader::~QLabelHeader()
{

}

void QLabelHeader::setShape(SHAPE s)
{
	this->shape = s;
}

void QLabelHeader::paintEvent(QPaintEvent *e)
{
	if (NULL != pixmap())
	{
		QPainter painter(this);
		painter.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
		QPainterPath path;
		if (shape == ellipse)
		{
			int round = qMin(width(), height());
			path.addEllipse(0, 0, round, round);
		}
		if (shape == roundRect)
		{
			path.addRoundRect(0, 0, width(), height(), 20);
		}
		
		painter.setClipPath(path);

		if (smooth)
		{
			QPixmap map = this->pixmap()->scaled(this->size(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
			painter.drawPixmap(-1, -1, width() + 2, height() + 2, map);
		}
		else
		{
			painter.drawPixmap(-1, -1, width() + 2, height() + 2, *this->pixmap());
		}
		
	}
	else
	{
		QLabel::paintEvent(e);
	}
}

void QLabelHeader::setTransform(bool iSmooth)
{
	this->smooth = iSmooth;
}


