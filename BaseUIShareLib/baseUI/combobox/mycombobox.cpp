﻿#include "mycombobox.h"
#include "myBoxItem.h"

MyComboBox::MyComboBox(QWidget *parent)
	: QComboBox(parent)
{
	setEditable(true);
	//setFixedSize(390, 200);

	mpListWidget = new QListWidget();
	
	QFile style(":/QSS/Resources/QSS/BaseUIShareLib/messagebox.qss");
	style.open(QFile::ReadOnly);
	QString sheet = QLatin1String(style.readAll());
	mpListWidget->setStyleSheet(sheet);
	style.close();

	//mpListWidget->setStyleSheet("QListWidget{background-color:#15447c;border:none;outline:0px;}QListWidget::item:hover{background-color:#183457;}");
	setModel(mpListWidget->model());
	setView(mpListWidget);
}

MyComboBox::~MyComboBox()
{

}
 
void MyComboBox::AddAccount(QWidget* pAccountItem)
{
	connect(pAccountItem, SIGNAL(sigShowAccount(QString)), this, SLOT(OnShowAccount(QString)));
	connect(pAccountItem, SIGNAL(sigRemoveAccount(QString)), this, SLOT(OnRemoveAccount(QString)));
	QListWidgetItem* item = new QListWidgetItem(mpListWidget);
	mpListWidget->setItemWidget(item, pAccountItem);
	//mpListWidget->setFocusPolicy(Qt::NoFocus);
}

void MyComboBox::OnShowAccount(QString account)
{
	setEditText(account);
	hidePopup();
}

void MyComboBox::OnRemoveAccount(QString account)
{
// 	hidePopup();
// 	//msg_box->setInfo(tr("remove account"), tr("are you sure to remove account?"), QPixmap(":/loginDialog/attention"), false);
// 	//if(msg_box->exec() == QDialog::Accepted)
// 	{
// 		int list_count = mpListWidget->count();
// 		for (int i = 0; i < 3; i++)
// 		{
// 			QListWidgetItem *item = mpListWidget->item(i);
// 			myBoxItem *account_item = (CAccountItem *)(mpListWidget->itemWidget(item));
// 			QString account_number = account_item->GetAccountNumber();
// 			if (account == account_number)
// 			{
// 				mpListWidget->takeItem(i);
// 				delete item;
// 				break;
// 			}
// 		}
// 	}
}
