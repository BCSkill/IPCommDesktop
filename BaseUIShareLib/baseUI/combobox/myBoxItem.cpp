﻿#include "myBoxItem.h"
#include <QHBoxLayout>
#include "ui_myBoxItem.h"

MyBoxItem::MyBoxItem(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::MyBoxItem();
	ui->setupUi(this);
	mouse_press = false;

	QFile style(":/QSS/Resources/QSS/BaseUIShareLib/myBoxItem.qss");
	style.open(QFile::ReadOnly);
	QString sheet = QLatin1String(style.readAll());
	this->setStyleSheet(sheet);
	style.close();
}

MyBoxItem::MyBoxItem(const QPixmap& pix, const QString& title, const QString& name, QWidget *parent /*= 0*/)
	: QWidget(parent)
{
	ui = new Ui::MyBoxItem();
	ui->setupUi(this);

	QFile style(":/QSS/Resources/QSS/BaseUIShareLib/myBoxItem.qss");
	style.open(QFile::ReadOnly);
	QString sheet = QLatin1String(style.readAll());
	this->setStyleSheet(sheet);
	style.close();

	mouse_press = false;
	QPixmap pix1 = pix.scaled(40, 40, Qt::KeepAspectRatio, Qt::SmoothTransformation);
	ui->iconlabel->setPixmap(pix1);
	ui->titlelabel->setText(title);
	ui->namelabel->setText(name);
}

MyBoxItem::~MyBoxItem()
{
	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}


void MyBoxItem::OnRemoveAccount()
{
	emit sigRemoveAccount("");//ui.labNumber->text());
}

void MyBoxItem::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		mouse_press = true;
	}
}

void MyBoxItem::mouseReleaseEvent(QMouseEvent *event)
{
	if (mouse_press) {
		emit sigShowAccount(ui->namelabel->text());//ui.labNumber->text());
		mouse_press = false;
	}
}
