﻿#include "cfriendtablewidgetbase.h"
#include <QHeaderView>
#include "searchresultwidget.h"

CFriendTableWidgetBase::CFriendTableWidgetBase(QWidget *parent)
	: QTableWidget(parent)
{
	m_iNum = 0;
	this->setColumnCount(2);
	this->setRowCount(0);
	this->verticalHeader()->setHidden(true);
	this->horizontalHeader()->setHidden(true);
	this->setShowGrid(false);
	this->setColumnWidth(0, 290);
	this->setColumnWidth(1, 290);
	setFocusPolicy(Qt::NoFocus);
	setEditTriggers(QAbstractItemView::NoEditTriggers);
	setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	//setStyleSheet("CFriendTableWidgetBase::item:selected{background-color:white;}");
}

CFriendTableWidgetBase::~CFriendTableWidgetBase()
{

}

void CFriendTableWidgetBase::OnInsertSearchGroupList(QString strAddID, QString strPicPath, QString strAddName, int iType, int nsize)
{
	//CFrientStyleWidget *buddy = new CFrientStyleWidget();
	SearchResultWidget * buddy = new SearchResultWidget();
	buddy->setIdInfo(strAddID);
	buddy->InitType(1);
	buddy->OnSetPicPathByHttp(strPicPath, ":/GroupChat/Resources/groupchat/group.png");
	buddy->OnSetNickNameText(strAddName);
	buddy->OnSetAddPersonButtonText(tr("Join in"));
	buddy->onSetNote(QString::number(nsize));
	buddy->setProperty("Type", iType);
	this->setRowCount(m_iNum / 2 + 1);
	this->setRowHeight(m_iNum/2,96);
	this->setCellWidget(m_iNum / 2, m_iNum%2, buddy);
	connect(buddy, SIGNAL(sigAddPerson(QString)), this, SLOT(doAddPerson(QString)));
	connect(buddy, SIGNAL(sigClickChange()), this, SLOT(slotClickChange()));
	connect(buddy, SIGNAL(sigOpenProfile(QString)), this, SLOT(slotOpenProFile(QString)));
	m_iNum++;
}

void CFriendTableWidgetBase::OnInsertSearchBuddyList(QString strAddID, QString strPicPath, QString strAddName, int nHeight)
{
	//CFrientStyleWidget *buddy = new CFrientStyleWidget();
	SearchResultWidget * buddy = new SearchResultWidget();
	buddy->setIdInfo(strAddID);
	buddy->InitType(0);
	buddy->OnSetPicPathByHttp(strPicPath,":/PerChat/Resources/person/temp.png");
	buddy->OnSetNickNameText(strAddName);
	buddy->onSetNote(strAddID);
	buddy->OnSetAddPersonButtonText(tr("Add as friend"));
	this->setRowCount(m_iNum / 2 + 1);
	this->setRowHeight(m_iNum / 2, 96);
	this->setCellWidget(m_iNum / 2, m_iNum % 2, buddy); //将该newItem插入到后面
	connect(buddy, SIGNAL(sigAddPerson(QString)), this, SLOT(doAddPerson(QString)));
	connect(buddy, SIGNAL(sigClickChange()), this, SLOT(slotClickChange()));
	connect(buddy, SIGNAL(sigOpenProfile(QString)), this, SLOT(slotOpenProFile(QString)));
	m_iNum++;
}

void CFriendTableWidgetBase::doAddPerson(QString strBuddyID)
{
	SearchResultWidget * act = qobject_cast<SearchResultWidget *>(sender());
	int iType = act->property("Type").toInt();
	emit sigAddPerson(strBuddyID, iType);
}

void CFriendTableWidgetBase::mousePressEvent(QMouseEvent *event)
{
// 	for (int i = 0; i < m_iNum; i++)
// 	{
// 		QWidget* tmpWidget = this->cellWidget(i / 2, i % 2);
// 		if (tmpWidget)
// 		{
// 			tmpWidget->setStyleSheet("");
// 		}
// 	}
	QWidget::mousePressEvent(event);
}

void CFriendTableWidgetBase::slotClickChange()
{
	SearchResultWidget * sendWidget = qobject_cast<SearchResultWidget *>(sender());
 	for (int i = 0; i < m_iNum; i++)
 	{
		SearchResultWidget* tmpWidget = (SearchResultWidget*)this->cellWidget(i / 2, i % 2);
 		if (tmpWidget&&tmpWidget->getIdInfo()!= sendWidget->getIdInfo())
 		{
 			tmpWidget->ResetBackStyle();
 		}
 	}
}

void CFriendTableWidgetBase::slotOpenProFile(QString id)
{
	emit sigOpenProfile(id);
}