﻿#include "interplanetnoticewidget.h"
#include "ui_interplanetnoticewidget.h"
#include <QFile>
InterPlanetNoticeWidget::InterPlanetNoticeWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::InterPlanetNoticeWidget();
	ui->setupUi(this);

	QFile style(":/QSS/Resources/QSS/BaseUIShareLib/interplanetnoticewidget.qss");
	style.open(QFile::ReadOnly);
	QString sheet = QLatin1String(style.readAll());
	this->setStyleSheet(sheet);
	style.close();
}

InterPlanetNoticeWidget::~InterPlanetNoticeWidget()
{
	delete ui;
}

void InterPlanetNoticeWidget::OnSetNickNameText(QString strText)
{
	this->setToolTip(strText);
	ui->nameLabel->setText(strText);
	ui->nameLabel->setObjectName(strText);
}

QString InterPlanetNoticeWidget::getNickName()
{
	return ui->nameLabel->objectName();
}

void InterPlanetNoticeWidget::OnSetPicPath(QString strPath)
{
	QByteArray bytearray;
	QFile file(strPath);
	if (file.open(QIODevice::ReadOnly) && file.size() != 0)
	{
		bytearray = file.readAll();

		if (bytearray.endsWith("temp"))
			ui->headerLabel->setTransform(false);
	}
	file.close();
	QPixmap pix;
	pix.loadFromData(bytearray);
	ui->headerLabel->setShape(roundRect);
	ui->headerLabel->setAutoFillBackground(true);
	ui->headerLabel->setPixmap(pix);
	ui->headerLabel->setScaledContents(true);
	ui->headerLabel->setObjectName(strPath);
}

void InterPlanetNoticeWidget::OnSetMessageNum(QString strNum)
{
	ui->msgBtn->setText(strNum);
	if (ui->msgBtn->text().isEmpty())
		ui->msgBtn->hide();
	else
		ui->msgBtn->show();
}