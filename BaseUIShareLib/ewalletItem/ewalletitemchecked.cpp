﻿#include "ewalletitemchecked.h"
#include "ui_ewalletitemchecked.h"
#include <QDebug>

EWalletItemChecked::EWalletItemChecked(QWidget *parent)
: QWidget(parent)
{
	ui = new Ui::EWalletItemChecked();
	ui->setupUi(this);

	connect(ui->switchBtn, SIGNAL(toggled(bool)), this, SLOT(slotToggled(bool)));
}

EWalletItemChecked::~EWalletItemChecked()
{
	delete ui;
}

void EWalletItemChecked::setData(QString iconPath, QString title, QString fullName, QString address, bool checked)
{
	setIcon(iconPath);
	setData(title, fullName, address, checked);
}

void EWalletItemChecked::setData(QString title, QString fullName, QString address, bool checked)
{
	ui->titleLabel->setText(title);
	ui->fullNameLabel->setText(fullName);

	QString strAddress = address;
	if (address.count() > 10)
	{
		strAddress = address.left(6) + "****" + address.right(4);
	}
	ui->addressLabel->setText(strAddress);
	ui->addressLabel->setObjectName(address);

	setChecked(checked);
}

void EWalletItemChecked::setChecked(bool checked)
{
	disconnect(ui->switchBtn, SIGNAL(toggled(bool)), this, SLOT(slotToggled(bool)));
	ui->switchBtn->setChecked(checked);
	connect(ui->switchBtn, SIGNAL(toggled(bool)), this, SLOT(slotToggled(bool)));
}

void EWalletItemChecked::hideChecked()
{
	ui->switchBtn->hide();
}

void EWalletItemChecked::setIcon(QString iconPath)
{
	QByteArray bytearray;
	QFile file(iconPath);
	if (file.open(QIODevice::ReadOnly))
	{
		bytearray = file.readAll();
		file.close();
	}
	QPixmap pix;
	pix.loadFromData(bytearray);

	if (pix.isNull())
	{
		file.remove();
	}
	else
	{
		QPixmap mask(":/ewallet/Resources/ewallet/tokenMask.png");
		QPainter *painter = new QPainter;
		painter->begin(&mask);
		painter->setCompositionMode(QPainter::CompositionMode_DestinationOver);
		pix = pix.scaled(QSize(70, 70), Qt::IgnoreAspectRatio);
		painter->drawPixmap(15, 15, pix);
		painter->end();
		delete painter;

		ui->iconLabel->setPixmap(mask);
	}
}

QString EWalletItemChecked::getName()
{
	return ui->titleLabel->text();
}

void EWalletItemChecked::setTokenID(QString tokenID)
{
	this->tokenID = tokenID;
}

void EWalletItemChecked::slotToggled(bool checked)
{
	emit sigChecked(tokenID, checked);
}

QString EWalletItemChecked::getAddress()
{
	return ui->addressLabel->objectName();
}

QString EWalletItemChecked::getTokenID()
{
	return tokenID;
}
