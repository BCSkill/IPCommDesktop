﻿#include "ewalletitemrecord.h"
#include "ui_ewalletitemrecord.h"

EWalletItemRecord::EWalletItemRecord(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::EWalletItemRecord();
	ui->setupUi(this);
}

EWalletItemRecord::~EWalletItemRecord()
{
	delete ui;
}

void EWalletItemRecord::setData(QString address, QString time, QString data, bool isIn)
{
	QString strAddress;
	if (address.startsWith("0x") || address.startsWith("0X"))
		strAddress = address.left(6) + "****" + address.right(4);
	else
		strAddress = address;
	ui->addressLabel->setText(strAddress);
	ui->timeLabel->setText(time);

	if (data.length() > 10)
		data = data.left(10) + "...";
	ui->dataLabel->setText(data);

	if (isIn)
		ui->iconLabel->setPixmap(QPixmap(":/ewallet/Resources/ewallet/icon_turnin.png"));
	else
		ui->iconLabel->setPixmap(QPixmap(":/ewallet/Resources/ewallet/icon_turnout.png"));
}
