﻿#include "ewalletitemfollow.h"
#include "ui_ewalletitemfollow.h"

EWalletItemFollow::EWalletItemFollow(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::EWalletItemFollow();
	ui->setupUi(this);
}

EWalletItemFollow::~EWalletItemFollow()
{
	delete ui;
}

void EWalletItemFollow::setData(QString iconPath, QString title, QString balance, QString address, QString tokenID)
{
	setIcon(iconPath);
	setData(title, balance, address, tokenID);
}

void EWalletItemFollow::setData(QString title, QString balance, QString address, QString tokenID)
{
	ui->titleLabel->setText(title);
	ui->balanceLabel->setText(balance);

	this->address = address;
	this->tokenID = tokenID;
}

void EWalletItemFollow::setIcon(QString iconPath)
{
	QByteArray bytearray;
	QFile file(iconPath);
	if (file.open(QIODevice::ReadOnly))
	{
		bytearray = file.readAll();
		file.close();
	}
	QPixmap pix;
	pix.loadFromData(bytearray);

	if (pix.isNull())
	{
		file.remove();
	}
	else
	{
		QPixmap mask(":/ewallet/Resources/ewallet/tokenMask.png");
		QPainter *painter = new QPainter;
		painter->begin(&mask);
		painter->setCompositionMode(QPainter::CompositionMode_DestinationOver);
		pix = pix.scaled(QSize(70, 70), Qt::IgnoreAspectRatio);
		painter->drawPixmap(15, 15, pix);
		painter->end();
		delete painter;

		ui->iconLabel->setPixmap(mask);
	}
}

QString EWalletItemFollow::getName()
{
	return ui->titleLabel->text();
}

QString EWalletItemFollow::getAddress()
{
	return this->address;
}

QString EWalletItemFollow::getTokenID()
{
	return this->tokenID;
}
