﻿#include "ewalletitemasset.h"
#include "ui_ewalletitemasset.h"

EWalletItemAsset::EWalletItemAsset(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::EWalletItemAsset();
	ui->setupUi(this);
}

EWalletItemAsset::~EWalletItemAsset()
{
	delete ui;
}

void EWalletItemAsset::setData(QString iconPath, QString nameText, QString numberText, QString nameCNYText, QString numberCNYText)
{
	ui->iconLabel->setPixmap(QPixmap(iconPath));
	ui->nameLabel->setText(nameText);
	ui->nameCNYLabel->setText(nameCNYText);
	ui->numberLabel->setText(numberText);
	ui->numberCNYLabel->setText(numberCNYText);
}
