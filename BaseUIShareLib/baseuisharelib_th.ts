<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="th_TH">
<context>
    <name>CFriendListWidgetBase</name>
    <message>
        <location filename="baseUI/cfriendlistwidgetbase.cpp" line="214"/>
        <source>Add as friend</source>
        <translation>เพิ่มเป็นเพื่อน</translation>
    </message>
    <message>
        <location filename="baseUI/cfriendlistwidgetbase.cpp" line="337"/>
        <source>Join in</source>
        <translation>เข้าร่วมเผ่า</translation>
    </message>
</context>
<context>
    <name>CFriendTableWidgetBase</name>
    <message>
        <location filename="baseUI/cfriendtablewidgetbase.ui" line="16"/>
        <source>CFriendTableWidgetBase</source>
        <translation></translation>
    </message>
    <message>
        <location filename="baseUI/cfriendtablewidgetbase.cpp" line="35"/>
        <source>Join in</source>
        <translation>เข้าร่วมเผ่า</translation>
    </message>
    <message>
        <location filename="baseUI/cfriendtablewidgetbase.cpp" line="56"/>
        <source>Add as friend</source>
        <translation>เพิ่มเป็นเพื่อน</translation>
    </message>
</context>
<context>
    <name>CFrientItemMessage</name>
    <message>
        <source>InterPlanet Notice</source>
        <translation type="vanished">แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="baseUI/cfrientitemmessage.cpp" line="38"/>
        <location filename="baseUI/cfrientitemmessage.cpp" line="191"/>
        <location filename="baseUI/cfrientitemmessage.cpp" line="217"/>
        <location filename="baseUI/cfrientitemmessage.cpp" line="429"/>
        <source>OpenPlanet Notice</source>
        <oldsource>Telecomm Notice</oldsource>
        <translation type="unfinished">ประกาศโทรคมนาคม</translation>
    </message>
    <message>
        <location filename="baseUI/cfrientitemmessage.cpp" line="371"/>
        <source>Yesterday </source>
        <translation>เมื่อวาน</translation>
    </message>
</context>
<context>
    <name>CFrientStyleWidget</name>
    <message>
        <location filename="baseUI/cfrientstylewidget.cpp" line="244"/>
        <source>CFrientStyleWidget::getLoginUserID空指针</source>
        <translation></translation>
    </message>
    <message>
        <location filename="baseUI/cfrientstylewidget.cpp" line="470"/>
        <source>CFrientStyleWidget::doAddPerson空指针</source>
        <translation></translation>
    </message>
    <message>
        <location filename="baseUI/cfrientstylewidget.cpp" line="520"/>
        <source>CFrientStyleWidget::doCurrentChatClose空指针</source>
        <translation></translation>
    </message>
    <message>
        <location filename="baseUI/cfrientstylewidget.cpp" line="564"/>
        <source>CFrientStyleWidget::doLoginEditClose空指针</source>
        <translation></translation>
    </message>
    <message>
        <location filename="baseUI/cfrientstylewidget.cpp" line="675"/>
        <source>CFrientStyleWidget::OnGetHeaderImagePath空指针</source>
        <translation></translation>
    </message>
    <message>
        <location filename="baseUI/cfrientstylewidget.cpp" line="871"/>
        <source>Add as friend</source>
        <translation>เพิ่มเป็นเพื่อน</translation>
    </message>
    <message>
        <location filename="baseUI/cfrientstylewidget.cpp" line="890"/>
        <location filename="baseUI/cfrientstylewidget.cpp" line="1407"/>
        <source>Added</source>
        <translation>ที่เพิ่ม</translation>
    </message>
    <message>
        <location filename="baseUI/cfrientstylewidget.cpp" line="922"/>
        <location filename="baseUI/cfrientstylewidget.cpp" line="1408"/>
        <location filename="baseUI/cfrientstylewidget.cpp" line="1510"/>
        <source>Approve</source>
        <translation>อนุมัติ</translation>
    </message>
    <message>
        <location filename="baseUI/cfrientstylewidget.cpp" line="940"/>
        <location filename="baseUI/cfrientstylewidget.cpp" line="1409"/>
        <source>Approved</source>
        <translation>ได้รับการอนุมัติ</translation>
    </message>
    <message>
        <location filename="baseUI/cfrientstylewidget.cpp" line="1351"/>
        <source>CFrientStyleWidget::enterEvent mCloseBtn空指针</source>
        <translation></translation>
    </message>
    <message>
        <location filename="baseUI/cfrientstylewidget.cpp" line="1356"/>
        <source>CFrientStyleWidget::enterEvent mMessageNum空指针</source>
        <translation></translation>
    </message>
    <message>
        <location filename="baseUI/cfrientstylewidget.cpp" line="1375"/>
        <source>CFrientStyleWidget::leaveEvent mCloseBtn空指针</source>
        <translation></translation>
    </message>
    <message>
        <location filename="baseUI/cfrientstylewidget.cpp" line="1380"/>
        <source>CFrientStyleWidget::leaveEvent mMessageNum空指针</source>
        <translation></translation>
    </message>
    <message>
        <location filename="baseUI/cfrientstylewidget.cpp" line="1408"/>
        <source>已同意</source>
        <translation></translation>
    </message>
    <message>
        <location filename="baseUI/cfrientstylewidget.cpp" line="1516"/>
        <source>Refuse</source>
        <translation>ปฏิเสธ</translation>
    </message>
    <message>
        <location filename="baseUI/cfrientstylewidget.cpp" line="1525"/>
        <source>CFrientStyleWidget::doApplyFriend空指针</source>
        <translation></translation>
    </message>
    <message>
        <location filename="baseUI/cfrientstylewidget.cpp" line="1535"/>
        <source>CFrientStyleWidget::doRejectFriend空指针</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>EWalletItemAsset</name>
    <message>
        <location filename="ewalletItem/ewalletitemasset.ui" line="14"/>
        <source>EWalletItemAsset</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ewalletItem/ewalletitemasset.ui" line="84"/>
        <source>ETH</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ewalletItem/ewalletitemasset.ui" line="103"/>
        <source>≈223045.CNY</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ewalletItem/ewalletitemasset.ui" line="122"/>
        <source>0.0000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ewalletItem/ewalletitemasset.ui" line="138"/>
        <source>0.00CNY</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>EWalletItemChecked</name>
    <message>
        <location filename="ewalletItem/ewalletitemchecked.ui" line="32"/>
        <source>EWalletItemChecked</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ewalletItem/ewalletitemchecked.ui" line="119"/>
        <location filename="ewalletItem/ewalletitemchecked.ui" line="148"/>
        <source>ETH</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ewalletItem/ewalletitemchecked.ui" line="177"/>
        <source>0x</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>EWalletItemFollow</name>
    <message>
        <location filename="ewalletItem/ewalletitemfollow.ui" line="26"/>
        <source>EWalletItemFollow</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>EWalletItemRecord</name>
    <message>
        <location filename="ewalletItem/ewalletitemrecord.ui" line="38"/>
        <source>EWalletItemRecord</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FloatButtonWidget</name>
    <message>
        <location filename="baseUI/zooming/floatbuttonwidget.ui" line="14"/>
        <source>FloatButtonWidget</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>IMessageBox</name>
    <message>
        <location filename="baseUI/messagebox.ui" line="14"/>
        <source>MessageBox</source>
        <translation></translation>
    </message>
    <message>
        <location filename="baseUI/messagebox.ui" line="110"/>
        <source>OpenPlanet</source>
        <translation>การสื่อสารระหว่างดวงดาว</translation>
    </message>
    <message>
        <location filename="baseUI/messagebox.ui" line="187"/>
        <source>Incorrect Password!</source>
        <translation>รหัสผ่านผิดพลาด!</translation>
    </message>
    <message>
        <location filename="baseUI/messagebox.ui" line="292"/>
        <location filename="baseUI/messagebox.cpp" line="62"/>
        <source>OK</source>
        <translation>ตกลง</translation>
    </message>
    <message>
        <location filename="baseUI/messagebox.ui" line="295"/>
        <source>Return</source>
        <translation></translation>
    </message>
    <message>
        <location filename="baseUI/messagebox.cpp" line="63"/>
        <source>Cancel</source>
        <translation>ยกเลิก</translation>
    </message>
</context>
<context>
    <name>InputBox</name>
    <message>
        <location filename="baseUI/inputbox.ui" line="14"/>
        <source>InputBox</source>
        <translation></translation>
    </message>
    <message>
        <location filename="baseUI/inputbox.ui" line="110"/>
        <source>OpenPlanet</source>
        <translation>การสื่อสารระหว่างดวงดาว</translation>
    </message>
    <message>
        <location filename="baseUI/inputbox.ui" line="198"/>
        <source>Please enter your password:</source>
        <translation>กรุณาใส่รหัสผ่านของคุณ:</translation>
    </message>
    <message>
        <location filename="baseUI/inputbox.ui" line="287"/>
        <source>OK</source>
        <translation>ตกลง</translation>
    </message>
    <message>
        <location filename="baseUI/inputbox.ui" line="290"/>
        <source>Return</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>InterPlanetNoticeWidget</name>
    <message>
        <location filename="baseUI/interplanetnoticewidget.ui" line="14"/>
        <source>InterPlanetNoticeWidget</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MyBoxItem</name>
    <message>
        <location filename="baseUI/combobox/myBoxItem.ui" line="32"/>
        <source>MyBoxItem</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PicWidget</name>
    <message>
        <location filename="baseUI/zooming/picwidget.ui" line="14"/>
        <location filename="baseUI/zooming/picwidget.ui" line="143"/>
        <source>View Pictures</source>
        <translation>ดูรูปภาพ</translation>
    </message>
    <message>
        <location filename="baseUI/zooming/picwidget.ui" line="296"/>
        <location filename="baseUI/zooming/picwidget.cpp" line="81"/>
        <source>Enlarge</source>
        <translation>เพิ่ม</translation>
    </message>
    <message>
        <location filename="baseUI/zooming/picwidget.ui" line="340"/>
        <location filename="baseUI/zooming/picwidget.cpp" line="75"/>
        <source>Save as</source>
        <translation>บันทึกเป็น</translation>
    </message>
    <message>
        <location filename="baseUI/zooming/picwidget.ui" line="372"/>
        <location filename="baseUI/zooming/picwidget.cpp" line="85"/>
        <source>Reduce</source>
        <translation>ลด</translation>
    </message>
    <message>
        <location filename="baseUI/zooming/picwidget.ui" line="397"/>
        <location filename="baseUI/zooming/picwidget.cpp" line="89"/>
        <source>Rotate</source>
        <translation>หมุน</translation>
    </message>
    <message>
        <location filename="baseUI/zooming/picwidget.cpp" line="69"/>
        <source>Copy</source>
        <translation>สำเนา</translation>
    </message>
    <message>
        <location filename="baseUI/zooming/picwidget.cpp" line="105"/>
        <location filename="baseUI/zooming/picwidget.cpp" line="750"/>
        <source>Previous</source>
        <translation>ก่อน</translation>
    </message>
    <message>
        <location filename="baseUI/zooming/picwidget.cpp" line="111"/>
        <location filename="baseUI/zooming/picwidget.cpp" line="751"/>
        <source>Next</source>
        <translation>ต่อไป</translation>
    </message>
    <message>
        <location filename="baseUI/zooming/picwidget.cpp" line="235"/>
        <source>Save picture</source>
        <translation>บันทึกภาพ</translation>
    </message>
    <message>
        <location filename="baseUI/zooming/picwidget.cpp" line="236"/>
        <source>Untitled</source>
        <translation>ไม่ได้ตั้งชื่อ</translation>
    </message>
    <message>
        <location filename="baseUI/zooming/picwidget.cpp" line="757"/>
        <location filename="baseUI/zooming/picwidget.cpp" line="758"/>
        <source>It&apos;s the first one</source>
        <translation>มันเป็นคนแรก</translation>
    </message>
    <message>
        <location filename="baseUI/zooming/picwidget.cpp" line="765"/>
        <location filename="baseUI/zooming/picwidget.cpp" line="766"/>
        <source>It&apos;s the last one</source>
        <translation>มันเป็นอันสุดท้าย</translation>
    </message>
</context>
<context>
    <name>QGroupQR</name>
    <message>
        <location filename="baseUI/qgroupqr.ui" line="19"/>
        <source>GroupQR</source>
        <translation>กลุ่ม QR</translation>
    </message>
    <message>
        <location filename="baseUI/qgroupqr.ui" line="53"/>
        <location filename="baseUI/qgroupqr.ui" line="69"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="baseUI/qgroupqr.ui" line="92"/>
        <source>Scan QR code</source>
        <translation>สแกนรหัส QR</translation>
    </message>
    <message>
        <location filename="baseUI/qgroupqr.cpp" line="36"/>
        <source>Scan the QR code to join
 the OpenPlanet group</source>
        <oldsource>Scan the QR code to join
 the Telecomm group</oldsource>
        <translation type="unfinished">สแกนรหัส QR
เพื่อเข้าร่วมกลุ่ม</translation>
    </message>
</context>
<context>
    <name>QLoadingWidget</name>
    <message>
        <location filename="baseUI/qloadingwidget.ui" line="14"/>
        <source>QLoadingWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="baseUI/qloadingwidget.ui" line="26"/>
        <location filename="baseUI/qloadingwidget.ui" line="42"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="baseUI/qloadingwidget.cpp" line="25"/>
        <source>Loading</source>
        <translation>กำลังโหลด</translation>
    </message>
</context>
<context>
    <name>QuestionBox</name>
    <message>
        <location filename="baseUI/questionbox.ui" line="14"/>
        <source>QuestionBox</source>
        <translation></translation>
    </message>
    <message>
        <location filename="baseUI/questionbox.ui" line="101"/>
        <source>OpenPlanet</source>
        <translation>การสื่อสารระหว่างดวงดาว</translation>
    </message>
    <message>
        <location filename="baseUI/questionbox.ui" line="184"/>
        <source>Incorrect Password!</source>
        <translation>รหัสผ่านผิดพลาด!</translation>
    </message>
    <message>
        <location filename="baseUI/questionbox.ui" line="242"/>
        <source>OK</source>
        <translation>ตกลง</translation>
    </message>
    <message>
        <location filename="baseUI/questionbox.ui" line="245"/>
        <source>Return</source>
        <translation></translation>
    </message>
    <message>
        <location filename="baseUI/questionbox.ui" line="289"/>
        <source>Cancel</source>
        <translation>ยกเลิก</translation>
    </message>
    <message>
        <location filename="baseUI/questionbox.ui" line="292"/>
        <source>Esc</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SearchResultWidget</name>
    <message>
        <location filename="baseUI/searchresultwidget.ui" line="14"/>
        <source>SearchResultWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="baseUI/searchresultwidget.ui" line="63"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ZoomImg</name>
    <message>
        <location filename="baseUI/zoomimg.ui" line="14"/>
        <source>图片</source>
        <translation></translation>
    </message>
</context>
</TS>
