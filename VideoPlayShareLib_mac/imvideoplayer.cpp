﻿#include "imvideoplayer.h"
#include "ui_imvideoplayer.h"

IMVideoPlayer::IMVideoPlayer(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::IMVideoPlayer();
	ui->setupUi(this);

	initTimer = new QTimer();
	playTimer = new QTimer();

	setWindowFlags(Qt::FramelessWindowHint | Qt::Tool);
	setAttribute(Qt::WA_DeleteOnClose);
	mouse.setX(-1);
	isPlaying = false;

	closeImg.load(":/video/Resources/video/close.png");
	playImg.load(":/video/Resources/video/play.png");
	pauseImg.load(":/video/Resources/video/pause.png");
	ui->quitButton->setIcon(QIcon(closeImg));
	ui->playButton->setIcon(QIcon(pauseImg));

	connect(ui->quitButton, SIGNAL(clicked()), this, SIGNAL(sigClose()));
	connect(initTimer, SIGNAL(timeout()), this, SLOT(sizeInitSlot()));
	connect(ui->playButton, SIGNAL(clicked()), this, SLOT(playPauseSlot()));
	connect(playTimer, SIGNAL(timeout()), this, SLOT(stepSliderSlot()));
	connect(ui->slider, SIGNAL(valueChanged(int)), this, SLOT(setSliderSlot(int)));

	QFile file(":/qssWidget/Resources/qssWidget/imvideoplayer.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();
}

IMVideoPlayer::~IMVideoPlayer()
{
	ui->videoWidget->stop();
}

//鼠标事件的处理。
void IMVideoPlayer::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();
	return QWidget::mousePressEvent(event);
}
void IMVideoPlayer::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void IMVideoPlayer::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}
void IMVideoPlayer::mouseDoubleClickEvent(QMouseEvent *event)
{
	if (this->size() == this->originSize)
		this->showFullScreen();
	else
	{
		//获取屏幕分辨率。
		QSize screen = QApplication::desktop()->size();
		this->hide();
		this->resize(originSize);
		this->move((screen.width() - this->width()) / 2,
			(screen.height() - this->height()) / 2);
		this->show();
	}
	if (!isPlaying)
		playPauseSlot();
}

//载入视频并播放的方法。
void IMVideoPlayer::videoPlay(QString mediaPath)
{
	mediaPath.replace("%20", " ");
#ifdef Q_OS_WIN
	mediaPath.replace("file:///", "");
#else
    mediaPath.replace("file://", "");
#endif
	address = mediaPath;
	ui->videoWidget->setMedia(address);
	ui->videoWidget->play();
	isPlaying = true;
	initTimer->start(100);
	connect(ui->videoWidget, SIGNAL(sigPositionChanged(qint64)), this, SLOT(slotSetPositionChanged(qint64)));
}

void IMVideoPlayer::sizeInitSlot()
{
	//获取视频的分辨率。
	QSize size = ui->videoWidget->getVideoSize();

	//当分辨率有效时。
	if (size.width() > 0)
	{
		initTimer->stop();

		//获取屏幕分辨率。
		QSize screen = QApplication::desktop()->size();
		//如果视频分辨率小于屏幕，窗口大小与视频相同。
		//如果视频分辨率超出屏幕，就限制为屏幕大小。
		if (size.width() > screen.width()
			|| size.height() > screen.height())
			this->resize(screen);
		else
		    this->resize(size.width(), size.height()+80);

		this->originSize = this->size();
		this->move((screen.width() - this->width()) / 2,
			(screen.height() - this->height()) / 2);

		this->show();
	}
}

void IMVideoPlayer::slotSetPositionChanged(qint64 pos)
{
	//设置进度条。
	this->duration = pos / 1000;
	ui->slider->setMaximum(duration);
	playTimer->start(1000);
	//显示进度标签。
	setProgress();
}

void IMVideoPlayer::playPauseSlot()
{
	if (isPlaying)
	{
		ui->videoWidget->pause();
		ui->playButton->setIcon(QIcon(playImg));
		isPlaying = false;
		playTimer->stop();
	}
	else
	{
		ui->videoWidget->play();
		ui->playButton->setIcon(QIcon(pauseImg));
		isPlaying = true;
		playTimer->start(1000);
	}
}

void IMVideoPlayer::stepSliderSlot()
{
	disconnect(ui->slider, SIGNAL(valueChanged(int)), this, SLOT(setSliderSlot(int)));
	if (ui->slider->value() >= duration)
	{
		playTimer->stop();
		ui->slider->setValue(0);
		ui->videoWidget->setMedia(address);
		playPauseSlot();
	}
	else
		ui->slider->setValue(ui->slider->value() + 1);
	connect(ui->slider, SIGNAL(valueChanged(int)), this, SLOT(setSliderSlot(int)));
	setProgress();
}

void IMVideoPlayer::setSliderSlot(int value)
{
	float pos = (float)value / (float)duration * 1000;
	ui->videoWidget->setPosition(pos);
	setProgress();
}

//刷新进度标签的方法。
void IMVideoPlayer::setProgress()
{
	int p = ui->slider->value();
	int ch = p / 3600;
	int cm = (p % 3600) / 60;
	int cs = p % 60;
	QTime curTime;
	curTime.setHMS(ch, cm, cs);
	QString curString = curTime.toString("hh:mm:ss");

	int dh = duration / 3600;
	int dm = (duration % 3600) / 60;
	int ds = duration % 60;
	QTime durTime;
	durTime.setHMS(dh, dm, ds);
	QString durString = durTime.toString("hh:mm:ss");

	ui->placeLabel->setText(curString + "/" + durString);


}
