#include "VedioFrameOpera.h"
#include <QFile>
#include <QDir>
#include <QProcess>
#include <QDebug>

VedioFrameOpera::VedioFrameOpera(QObject *parent)
	: QObject(parent)
{
}

VedioFrameOpera::~VedioFrameOpera()
{
}

void VedioFrameOpera::CreateVedioPicture(QString vedioPath, QString imgPath)
{
    QDir dir;
    dir.remove(imgPath);
    QString appPath = QDir::currentPath() + "/ffmpeg";
    QStringList arguments;
    arguments << "-i" << vedioPath << "-r" <<"1"<<"-f"<<"image2"<<"-t"<<"0.0001"<<imgPath;
    QProcess process(this);
    process.start(appPath, arguments);
    process.waitForFinished();
    process.close();
}
