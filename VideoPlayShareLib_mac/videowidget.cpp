﻿#include "videowidget.h"

VideoWidget::VideoWidget(QObject *parent)
{
	media = new QMediaPlayer();
	media->setVideoOutput(this);
	connect(media, SIGNAL(durationChanged(qint64)), this, SLOT(positionChanged(qint64)));
}

VideoWidget::~VideoWidget()
{

}

void VideoWidget::setMedia(QString address)
{
	QUrl url = QUrl::fromLocalFile(address);
	media->setMedia(QMediaContent(url));
}

void VideoWidget::setVolume(int volume)
{
	media->setVolume(volume);
}

void VideoWidget::setPosition(qint64 pos)
{
	media->setPosition(pos);
}

QSize VideoWidget::getVideoSize()
{
	return this->size();
}

void VideoWidget::positionChanged(qint64 pos)
{
	emit sigPositionChanged(pos);
}

qint64 VideoWidget::getDuration()
{
	return media->duration();
}

void VideoWidget::play()
{
	media->play();
}

void VideoWidget::pause()
{
	media->pause();
}

void VideoWidget::stop()
{
	media->stop();
}


