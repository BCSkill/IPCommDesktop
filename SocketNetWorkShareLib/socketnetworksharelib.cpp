﻿#include "socketnetworksharelib.h"
#include "QStringLiteralBak.h"
#include <QHostInfo>

SocketNetWorkShareLib::SocketNetWorkShareLib(QObject *parent) : QObject(parent), mSocketClient(NULL)
{

}

SocketNetWorkShareLib::~SocketNetWorkShareLib()
{
	DisConnectSocket();
	if (mSocketClient != NULL)
	{
		mSocketClient->abort();
		mSocketClient->disconnectFromHost();
		mSocketClient->deleteLater();
	}
}

// Qualifier: 连接服务器
bool SocketNetWorkShareLib::ConnectServer(QString strIP, int nPort)
{
	QHostInfo info = QHostInfo::fromName(strIP);
	QString sIP = info.addresses().first().toString();

	if (mSocketClient == NULL)
	{
		mSocketClient = new QTcpSocket();
		connect(mSocketClient, SIGNAL(readyRead()), this, SLOT(slotReceiveServerData()), Qt::DirectConnection);
		connect(mSocketClient, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(slotSocketErro(QAbstractSocket::SocketError)));
	}
	mSocketClient->abort();
	mSocketClient->connectToHost(strIP, nPort);
	if (mSocketClient->waitForConnected(1000))
	{
		mSocketClient->socketDescriptor();
		return true;
	}
	else
	{
		mSocketClient->abort();
		mSocketClient->disconnectFromHost();
		return false;
	}
	return false;
}

// Qualifier: 断开socket连接
void SocketNetWorkShareLib::DisConnectSocket()
{
	if (mSocketClient != NULL)
	{
		mSocketClient->disconnectFromHost();
		slotSocketErro(QAbstractSocket::RemoteHostClosedError);
	}
}

// Qualifier: 发送消息
void SocketNetWorkShareLib::SendMsg(QByteArray byteArray)
{
	if (mSocketClient)
	{
		mSocketClient->write(byteArray);
		mSocketClient->flush();
	}
}

// Qualifier: 处理接受到数据
void SocketNetWorkShareLib::slotReceiveServerData()
{
	QByteArray buffer = mSocketClient->readAll();
	if (!buffer.isEmpty())
	{
		emit sigSendReadData(buffer);
	}
}

// Qualifier: socket错误
void SocketNetWorkShareLib::slotSocketErro(QAbstractSocket::SocketError socketError)
{
	switch (socketError)
	{
	case QAbstractSocket::RemoteHostClosedError:
		qDebug() << tr("RemoteHostClosedError");
		break;
	case QAbstractSocket::HostNotFoundError:
		qDebug() << tr( "HostNotFoundError");
		break;
	case QAbstractSocket::ConnectionRefusedError:
		qDebug() << tr("ConnectionRefusedError");
		break;
	default:
		qDebug() << tr("Erro");
		break;
	}
	emit sigDisConnectServer();
}
