
QT += core network gui widgets sql webenginewidgets xml multimedia multimediawidgets macextras quick quickwidgets quickcontrols2


CONFIG(debug,debug|release){
    mac{
        LIBS += -L$$PWD/../SharedLib_mac/AlphabeticalSortSharedLib/lib/mac/ -lAlphabeticalSortSharedLibD \
        -L$$PWD/../SharedLib_mac/BaseUI/lib/mac/ -lBaseUIShareLibD \
        -L$$PWD/../SharedLib_mac/botan/lib/mac/ -lbotan-2 \
        -L$$PWD/../SharedLib_mac/ChatWidget/lib/mac/ -lChatWidgetShareLibD \
        -L$$PWD/../SharedLib_mac/ContactsProfileShareLib/lib/mac/ -lContactsProfileShareLibD \
        -L$$PWD/../SharedLib_mac/ContactsWidget/lib/mac/ -lContactsWidgetShareLibD \
        -L$$PWD/../SharedLib_mac/createAddWidgetShareLib/lib/mac/ -lCreateAddWidgetShareLibD \
        -L$$PWD/../SharedLib_mac/ewalletShareLib/lib/mac/ -leWalletLibD \
        -L$$PWD/../SharedLib_mac/HttpNetWorkShareLib/lib/mac/ -lHttpNetWorkShareLibD \
        -L$$PWD/../SharedLib_mac/IMDataBaseOperaInfo/lib/mac/ -lIMDataBaseOperaInfoD \
        -L$$PWD/../SharedLib_mac/IMDataManagerShareLib/lib/mac/ -lIMDataManagerShareLibD \
        -L$$PWD/../SharedLib_mac/IMDownLoadHeaderImg/lib/mac/ -lIMDownLoadHeaderImgD \
        -L$$PWD/../SharedLib_mac/IMRequestBuddyInfo/lib/mac/ -lIMRequestBuddyInfoD \
        -L$$PWD/../SharedLib_mac/IMSocketDataBaseShareLib/lib/mac/ -lIMSocketDataBaseShareLibD \
        -L$$PWD/../SharedLib_mac/IMSocketNetWorkShareLib/lib/mac/ -lIMSocketNetWorkShareLibD \
        -L$$PWD/../SharedLib_mac/libqrencode/lib/mac/ -lqrencode \
        -L$$PWD/../SharedLib_mac/LoginDatabaseOperaShareLib/lib/mac/ -lLoginDatabaseOperaShareLibD \
        -L$$PWD/../SharedLib_mac/openssl/lib/mac/ -lssl \
        -L$$PWD/../SharedLib_mac/openssl/lib/mac/ -lcrypto \
        -L$$PWD/../SharedLib_mac/OPDataManager/lib/mac/ -lOPDataManagerD \
        -L$$PWD/../SharedLib_mac/OPDateBaseShareLib/lib/mac/ -lOPDatebaseShareLibD \
        -L$$PWD/../SharedLib_mac/OPMainMangerShareLib/lib/mac/ -lOPMainManagerShareLibD \
        -L$$PWD/../SharedLib_mac/OPMainWidget/lib/mac/ -lOPMainWidgetShareLibD \
        -L$$PWD/../SharedLib_mac/OPObjectManager/lib/mac/ -lOPObjectManagerD \
        -L$$PWD/../SharedLib_mac/OPRecoveryWalletShareLib/lib/mac/ -lOPRecoveryWalletShareLibD \
        -L$$PWD/../SharedLib_mac/OPRequestShareLib/lib/mac/ -lOPRequestShareLibD \
        -L$$PWD/../SharedLib_mac/OPWindowsManagerShareLib/lib/mac/ -lOPWindowsManagerShareLibD \
        -L$$PWD/../SharedLib_mac/QRenCodeShareLib/lib/mac/ -lQRenCodeShareLibD \
        -L$$PWD/../SharedLib_mac/ReadAppConfig/lib/mac/ -lReadAppConfigD \
        -L$$PWD/../SharedLib_mac/ScanQRLoginShareLib/lib/mac/ -lScanQRLoginShareLibD \
        -L$$PWD/../SharedLib_mac/SocketNetWorkShareLib/lib/mac/ -lSocketNetWorkShareLibD \
        -L$$PWD/../SharedLib_mac/SqlLiteShareLib/lib/mac/ -lSqlLiteShareLibD \
        -L$$PWD/../SharedLib_mac/UpdateShareLib/lib/mac/ -lUpdateShareLibD \
        -L$$PWD/../SharedLib_mac/VideoPlayShareLib/lib/mac/ -lVideoPlayShareLibD \
        -L$$PWD/../SharedLib_mac/WebObjectShareLib/lib/mac/ -lWebObjectShareLibD \
        -L$$PWD/../SharedLib_mac/ScreenCutShareLib/lib/mac/ -lScreenCutShareLibD \
        -L$$PWD/../SharedLib_mac/MacUpdate/lib/mac/ -lMacUpdateD \
        -L$$PWD/../SharedLib_mac/QxtGlobalShortCut/lib/mac/ -lQxtGlobalShortCutD \
        -L$$PWD/../SharedLib_mac/breakpad/lib/ -lgooglebreakpad \
        -L$$PWD/../SharedLib_mac/MacNotification/lib/mac/ -lMacNotificationD
    }
}else
{
        LIBS += -L$$PWD/../SharedLib_mac/AlphabeticalSortSharedLib/lib/mac/ -lAlphabeticalSortSharedLib \
        -L$$PWD/../SharedLib_mac/BaseUI/lib/mac/ -lBaseUIShareLib \
        -L$$PWD/../SharedLib_mac/botan/lib/mac/ -lbotan-2 \
        -L$$PWD/../SharedLib_mac/ChatWidget/lib/mac/ -lChatWidgetShareLib \
        -L$$PWD/../SharedLib_mac/ContactsProfileShareLib/lib/mac/ -lContactsProfileShareLib \
        -L$$PWD/../SharedLib_mac/ContactsWidget/lib/mac/ -lContactsWidgetShareLib \
        -L$$PWD/../SharedLib_mac/createAddWidgetShareLib/lib/mac/ -lCreateAddWidgetShareLib \
        -L$$PWD/../SharedLib_mac/ewalletShareLib/lib/mac/ -leWalletLib \
        -L$$PWD/../SharedLib_mac/HttpNetWorkShareLib/lib/mac/ -lHttpNetWorkShareLib \
        -L$$PWD/../SharedLib_mac/IMDataBaseOperaInfo/lib/mac/ -lIMDataBaseOperaInfo \
        -L$$PWD/../SharedLib_mac/IMDataManagerShareLib/lib/mac/ -lIMDataManagerShareLib \
        -L$$PWD/../SharedLib_mac/IMDownLoadHeaderImg/lib/mac/ -lIMDownLoadHeaderImg \
        -L$$PWD/../SharedLib_mac/IMRequestBuddyInfo/lib/mac/ -lIMRequestBuddyInfo \
        -L$$PWD/../SharedLib_mac/IMSocketDataBaseShareLib/lib/mac/ -lIMSocketDataBaseShareLib \
        -L$$PWD/../SharedLib_mac/IMSocketNetWorkShareLib/lib/mac/ -lIMSocketNetWorkShareLib \
        -L$$PWD/../SharedLib_mac/libqrencode/lib/mac/ -lqrencode \
        -L$$PWD/../SharedLib_mac/LoginDatabaseOperaShareLib/lib/mac/ -lLoginDatabaseOperaShareLib \
        -L$$PWD/../SharedLib_mac/openssl/lib/mac/ -lssl \
        -L$$PWD/../SharedLib_mac/openssl/lib/mac/ -lcrypto \
        -L$$PWD/../SharedLib_mac/OPDataManager/lib/mac/ -lOPDataManager \
        -L$$PWD/../SharedLib_mac/OPDateBaseShareLib/lib/mac/ -lOPDatebaseShareLib \
        -L$$PWD/../SharedLib_mac/OPMainMangerShareLib/lib/mac/ -lOPMainManagerShareLib \
        -L$$PWD/../SharedLib_mac/OPMainWidget/lib/mac/ -lOPMainWidgetShareLib \
        -L$$PWD/../SharedLib_mac/OPObjectManager/lib/mac/ -lOPObjectManager \
        -L$$PWD/../SharedLib_mac/OPRecoveryWalletShareLib/lib/mac/ -lOPRecoveryWalletShareLib \
        -L$$PWD/../SharedLib_mac/OPRequestShareLib/lib/mac/ -lOPRequestShareLib \
        -L$$PWD/../SharedLib_mac/OPWindowsManagerShareLib/lib/mac/ -lOPWindowsManagerShareLib \
        -L$$PWD/../SharedLib_mac/QRenCodeShareLib/lib/mac/ -lQRenCodeShareLib \
        -L$$PWD/../SharedLib_mac/ReadAppConfig/lib/mac/ -lReadAppConfig \
        -L$$PWD/../SharedLib_mac/ScanQRLoginShareLib/lib/mac/ -lScanQRLoginShareLib \
        -L$$PWD/../SharedLib_mac/SocketNetWorkShareLib/lib/mac/ -lSocketNetWorkShareLib \
        -L$$PWD/../SharedLib_mac/SqlLiteShareLib/lib/mac/ -lSqlLiteShareLib \
        -L$$PWD/../SharedLib_mac/UpdateShareLib/lib/mac/ -lUpdateShareLib \
        -L$$PWD/../SharedLib_mac/VideoPlayShareLib/lib/mac/ -lVideoPlayShareLib \
        -L$$PWD/../SharedLib_mac/WebObjectShareLib/lib/mac/ -lWebObjectShareLib \
        -L$$PWD/../SharedLib_mac/ScreenCutShareLib/lib/mac/ -lScreenCutShareLib \
        -L$$PWD/../SharedLib_mac/MacUpdate/lib/mac/ -lMacUpdate \
        -L$$PWD/../SharedLib_mac/QxtGlobalShortCut/lib/mac/ -lQxtGlobalShortCut \
        -L$$PWD/../SharedLib_mac/breakpad/lib/ -lgooglebreakpad \
        -L$$PWD/../SharedLib_mac/MacNotification/lib/mac/ -lMacNotification \
        -L$$PWD/../SharedLib_mac/SettingsManagerShareLib/lib/mac/ -lSettingsManagerShareLib
}

INCLUDEPATH += ../SharedLib_mac/openssl/include \
    ../SharedLib_mac/common \
    ../SharedLib_mac/libqrencode/include \
    ../SharedLib_mac/AlphabeticalSortSharedLib/include \
    ../SharedLib_mac/SqlLiteShareLib/include \
    ../SharedLib_mac/QRenCodeShareLib/include \
    ../SharedLib_mac/VideoPlayShareLib/include \
    ../SharedLib_mac/SocketNetWorkShareLib/include \
    ../SharedLib_mac/ReadAppConfig/include \
    ../SharedLib_mac/OPDataManager/include \
    ../SharedLib_mac/baseUI/include \
    ../SharedLib_mac/HttpNetWorkShareLib/include \
    ../SharedLib_mac/IMDataManagerShareLib/include \
    ../SharedLib_mac/IMDataBaseOperaInfo/include \
    ../SharedLib_mac/IMSocketDataBaseShareLib/include \
    ../SharedLib_mac/OPDatebaseShareLib/include \
    ../SharedLib_mac/LoginDatabaseOperaShareLib/include \
    ../SharedLib_mac/OPRecoveryWalletShareLib/include \
    ../SharedLib_mac/OPRequestShareLib/include \
    ../SharedLib_mac/IMDownLoadHeaderImg/include \
    ../SharedLib_mac/IMRequestBuddyInfo/include \
    ../SharedLib_mac/IMSocketNetWorkShareLib/include \
    ../SharedLib_mac/ContactsProfileShareLib/include \
    ../SharedLib_mac/ContactsWidget/include \
    ../SharedLib_mac/ChatWidget/include \
    ../SharedLib_mac/UpdateShareLib/include \
    ../SharedLib_mac/ewalletShareLib/include \
    ../SharedLib_mac/ScanQRLoginShareLib/include \
    ../SharedLib_mac/CreateAddWidgetShareLib/include \
    ../SharedLib_mac/OPMainWidget/include \
    ../SharedLib_mac/OPMainMangerShareLib/include \
    ../SharedLib_mac/OPWindowsManagerShareLib/include \
    ../SharedLib_mac/OPObjectManager/include \
    ../SharedLib_mac/botan/include \
    ../SharedLib_mac/OPObjectManager/include \
    ../SharedLib_mac/WebObjectShareLib/include \
    ../SharedLib_mac/ScreenCutShareLib/include \
    ../SharedLib_mac/MacUpdate/include \
    ../SharedLib_mac/breakpad/include \
    ../SharedLib_mac/SettingsManagerShareLib/include
include(OpenPlanet.pri)
include(shared.pri)

QMAKE_LFLAGS += -framework Carbon -framework CoreFoundation -framework Foundation -framework Cocoa

HEADERS += \
    myapplication.h

SOURCES += \
    myapplication.cpp
