﻿#include "sqllitesharelib.h"
#include <QFile>
#include <QDir>

SqlLiteShareLib::SqlLiteShareLib()
{

}

SqlLiteShareLib::~SqlLiteShareLib()
{
	if (m_db.isOpen())
	{
		m_db.close();
	}
}

bool SqlLiteShareLib::isDbDecrypted()
{
	QSqlQuery query(m_db);
	return query.exec("select name,sql from sqlite_master where type = 'table'");
}

bool SqlLiteShareLib::changePassword(const QString &newPassword)
{
	QSqlQuery query(m_db);
	return query.exec(QString("PRAGMA rekey='%1';").arg(newPassword));
}

bool SqlLiteShareLib::OnConnectDB(const QString& strConn, const QString& dbName, const QString& strPwd)
{
	if (!m_db.isOpen())
	{
		if (QSqlDatabase::contains(dbName)){
			m_db = QSqlDatabase::database(dbName);
		}
		else //否则打开这个数据库，注意带上数据库名
		{
			ZSQLiteCipherDriver *driver = new ZSQLiteCipherDriver();
			m_db = QSqlDatabase::addDatabase(driver, dbName);
		}
		m_db.setDatabaseName(strConn);
		if (!strPwd.isEmpty())
		{
			m_db.setPassword(strPwd);
		}
	}
	if (!m_db.open())
	{
		return false;
	}
	return isDbDecrypted();
}

bool SqlLiteShareLib::ExecuSql(const QString& strSql)
{
	if (m_db.isOpen())
	{
		QSqlQuery query(m_db);
		query.prepare(strSql);
		return query.exec();
	}
	return false;
}

QSqlQuery SqlLiteShareLib::ExecuQuery(QString& strSql)
{
	QSqlQuery query(m_db);
	if (m_db.isOpen())
	{
		query.exec(strSql);
	}
	return query;
}

void SqlLiteShareLib::CloseDataBase()
{
	if (m_db.isOpen())
	{
		m_db.close();
	}
	//QSqlDatabase::removeDatabase("QSQLITE");
}

bool SqlLiteShareLib::CreateDataFile(const QString &strFileName)
{
	if (!QFile::exists(strFileName))//文件不存在，则创建
	{
		QDir fileDir = QFileInfo(strFileName).absoluteDir();
		QString strFileDir = QFileInfo(strFileName).absolutePath();
		if (!fileDir.exists()) //路径不存在，创建路径
		{
			fileDir.mkpath(strFileDir);
		}
		QFile dbFile(strFileName);
		if (!dbFile.open(QIODevice::WriteOnly))//未成功打开
		{
			dbFile.close();
			return false;
		}
		dbFile.close();
	}
	return true;
}

bool SqlLiteShareLib::isExistTable(const QString &strTableName)
{
	QSqlQuery query(m_db);
	QString strSql = QString("SELECT 1 FROM sqlite_master where type = 'table' and  name = '%1'").arg(strTableName);
	query.exec(strSql);
	if (query.next())
	{
		int nResult = query.value(0).toInt();//有表时返回1，无表时返回null
		if (nResult)
		{
			return true;
		}
	}
	return false;
}

bool SqlLiteShareLib::IsOpen()
{
	return m_db.isOpen();
}