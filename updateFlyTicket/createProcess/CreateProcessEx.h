#ifndef _CREATE_PROCESS_EX_H_
#define _CREATE_PROCESS_EX_H_
#include <windows.h>
#include "tchar.h"
#pragma comment(lib, "shell32")
#pragma comment(lib, "user32")
#pragma comment(lib, "Advapi32.lib")

//以普通权限启动进程
BOOL CreateProcessLow(const wchar_t * lpApplicationName, 
											wchar_t * lpCommandLine = NULL, 
											wchar_t * lpDirectory = NULL,
											UINT nShow = SW_SHOWNORMAL);
//以管理员权限启动进程
BOOL CreateProcessHigh(TCHAR * strProcessName, 
											 TCHAR * strCommandLine = NULL, 
											 TCHAR * lpDirectory = NULL,
											 UINT nShow = SW_SHOWNORMAL);
#endif //_CREATE_PROCESS_EX_H_