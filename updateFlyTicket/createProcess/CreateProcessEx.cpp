#include "CreateProcessEx.h"

#include "shellapi.h"

#include <string>
using namespace std;

typedef BOOL (WINAPI *F_CreateProcessWithTokenW)(
	__in          HANDLE hToken,
	__in          DWORD dwLogonFlags,
	__in          LPCWSTR lpApplicationName,
	__in          LPWSTR lpCommandLine,
	__in          DWORD dwCreationFlags,
	__in          LPVOID lpEnvironment,
	__in          LPCWSTR lpCurrentDirectory,
	__in          LPSTARTUPINFOW lpStartupInfo,
	__out         LPPROCESS_INFORMATION lpProcessInfo
	);

HANDLE DupExplorerToken();
BOOL IsVistaOrLater();
BOOL IsAdminPrivilege();


//以普通权限启动进程
BOOL CreateProcessLow(const wchar_t * lpApplicationName, 
											wchar_t * lpCommandLine, 
											wchar_t * lpDirectory,
											UINT nShow)
{
	if (!IsVistaOrLater()
		|| !IsAdminPrivilege())
	{
		HINSTANCE hRet = ShellExecuteW(NULL, L"open", lpApplicationName, lpCommandLine, lpDirectory, nShow);
		return ((int)hRet > 32);
	}
	HANDLE hToken = DupExplorerToken();

	if (hToken == NULL)
		return FALSE;

	static HMODULE hDll = LoadLibrary(_T("ADVAPI32.dll"));
	if (!hDll)
	{
		CloseHandle(hToken);
		return FALSE;
	}
	F_CreateProcessWithTokenW pfn = (F_CreateProcessWithTokenW)GetProcAddress(hDll, "CreateProcessWithTokenW");
	if (!pfn)
	{
		CloseHandle(hToken);
		return FALSE;
	}

	STARTUPINFOW si = {sizeof(STARTUPINFO)};
	PROCESS_INFORMATION pi = {0};
	BOOL ret = pfn(hToken, LOGON_WITH_PROFILE, lpApplicationName, lpCommandLine, NORMAL_PRIORITY_CLASS, NULL, 
		                   lpDirectory, &si, &pi);

	if (ret)
	{
		CloseHandle(pi.hProcess);
		CloseHandle(pi.hThread);
	}

	CloseHandle(hToken);
	return ret;
}

//以管理员权限启动进程
BOOL CreateProcessHigh(TCHAR * lpApplicationName, 
											 TCHAR * lpCommandLine, 
											 TCHAR * lpDirectory,
											 UINT nShow)
{
#ifdef _UNICODE
	wstring command;
#else
	string command;
#endif

	if (lpCommandLine)
	{
		command = lpCommandLine;
	}
	if (IsVistaOrLater()
		&& !IsAdminPrivilege())
	{
		command += _T(" -Admin");
	}

	HINSTANCE hRet = ShellExecute(NULL, _T("runas"), lpApplicationName, command.c_str(), lpDirectory, nShow);
	return ((int)hRet > 32);
}


HANDLE DupExplorerToken()
{
	DWORD dwPid = 0;
	HWND hwnd = FindWindow(_T("Shell_TrayWnd"), NULL);
	if (NULL == hwnd)
		return NULL;

	GetWindowThreadProcessId(hwnd, &dwPid);
	if (dwPid == 0)
		return NULL;

	HANDLE hExplorer = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, dwPid);
	if (hExplorer == NULL)
		return NULL;

	HANDLE hToken = NULL;
	OpenProcessToken(hExplorer, TOKEN_DUPLICATE, &hToken);
	CloseHandle(hExplorer);

	HANDLE hNewToken = NULL;
	DuplicateTokenEx(hToken, TOKEN_ALL_ACCESS, NULL, SecurityImpersonation, TokenPrimary, &hNewToken);
	CloseHandle(hToken);

	return hNewToken;
}

BOOL IsVistaOrLater()
{
	OSVERSIONINFOEX version = {sizeof(OSVERSIONINFOEX)};
	if (!GetVersionEx((LPOSVERSIONINFO)&version))
	{
		version.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
		if (!GetVersionEx((LPOSVERSIONINFO)&version))
		{
			return FALSE;
		}
	}

	return (version.dwMajorVersion >= 6);
}

BOOL IsAdminPrivilege()
{
	BOOL bIsAdmin = FALSE;
	BOOL bRet = FALSE;
	SID_IDENTIFIER_AUTHORITY idetifier = SECURITY_NT_AUTHORITY;
	PSID pAdministratorGroup;
	if (AllocateAndInitializeSid(
		&idetifier,
		2,
		SECURITY_BUILTIN_DOMAIN_RID,
		DOMAIN_ALIAS_RID_ADMINS,
		0,0,0,0,0,0,
		&pAdministratorGroup))
	{
		if (!CheckTokenMembership(NULL, pAdministratorGroup, &bRet))
		{
			bIsAdmin = FALSE;
		}
		if (bRet)
		{
			bIsAdmin = TRUE;
		}
		FreeSid(pAdministratorGroup);
	}

	return bIsAdmin;
}