#pragma once 



enum IntegrityLevel {  
	INTEGRITY_LEVEL_BELOW_LOW,
	INTEGRITY_LEVEL_LOW,
	INTEGRITY_LEVEL_MEDIUM_LOW,
	INTEGRITY_LEVEL_MEDIUM,
	INTEGRITY_LEVEL_HIGH,
	INTEGRITY_LEVEL_SYSTEM,
	INTEGRITY_LEVEL_LAST  
};


// source code

#include <string>
#include <Sddl.h>
#include <Aclapi.h>

namespace Integrity 
{
	enum IntegrityLevel {  
		INTEGRITY_LEVEL_BELOW_LOW,
		INTEGRITY_LEVEL_LOW,
		INTEGRITY_LEVEL_MEDIUM_LOW,
		INTEGRITY_LEVEL_MEDIUM,
		INTEGRITY_LEVEL_HIGH,
		INTEGRITY_LEVEL_SYSTEM,
		INTEGRITY_LEVEL_LAST  
	};

	enum OS_VERSION
	{
		OS_BEFORE_2000,
		OS_WIN2000,
		OS_XP,
		OS_WIN2003,
		OS_VISTA,
		OS_WIN7,
		OS_WIN8ORLATER,
		OS_LAST
	};

	static OS_VERSION GetOSVersion()
	{
		OSVERSIONINFO osvi = {};    
		osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
		GetVersionEx(&osvi);
		if(osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 0)
		{
			return OS_WIN2000;
		}

		if(osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 1)
		{
			return OS_XP;
		}

		if(osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 2)
		{
			return OS_WIN2003;
		}

		if(osvi.dwMajorVersion == 6 && osvi.dwMinorVersion == 0)
		{
			return OS_VISTA;
		}

		if(osvi.dwMajorVersion == 6 && osvi.dwMinorVersion == 1)
		{
			return OS_WIN7;
		}

		if(osvi.dwMajorVersion >= 6)
			return OS_WIN8ORLATER;

		if(osvi.dwMajorVersion < 5)
			return OS_BEFORE_2000;

		return OS_LAST;

	}

	static  const wchar_t* GetIntegrityLevelString(IntegrityLevel integrity_level) {
		switch (integrity_level) {
	case INTEGRITY_LEVEL_SYSTEM:
		return L"S-1-16-16384";
	case INTEGRITY_LEVEL_HIGH:
		return L"S-1-16-12288";
	case INTEGRITY_LEVEL_MEDIUM:
		return L"S-1-16-8192";
	case INTEGRITY_LEVEL_MEDIUM_LOW:
		return L"S-1-16-6144";
	case INTEGRITY_LEVEL_LOW:
		return L"S-1-16-4096";
	case INTEGRITY_LEVEL_BELOW_LOW:
		return L"S-1-16-2048";
	case INTEGRITY_LEVEL_LAST:
		return NULL;
		}


		return NULL;
	}

	static IntegrityLevel GetIntegrityLevelFromRid(DWORD integrity)
	{
		switch(integrity) {
	case SECURITY_MANDATORY_PROTECTED_PROCESS_RID:
		//a way to mark protected processes are ˇ°untouchableˇ± by ˇ°normalˇ± processes
		return INTEGRITY_LEVEL_HIGH; 

	case SECURITY_MANDATORY_SYSTEM_RID:
		return INTEGRITY_LEVEL_SYSTEM;

	case SECURITY_MANDATORY_HIGH_RID:
		return INTEGRITY_LEVEL_HIGH; 

		//	case SECURITY_MANDATORY_MEDIUM_PLUS_RID: // Medium High Integrity
	case SECURITY_MANDATORY_MEDIUM_RID:
		return INTEGRITY_LEVEL_MEDIUM;

	case SECURITY_MANDATORY_UNTRUSTED_RID:
		return INTEGRITY_LEVEL_BELOW_LOW;

	case SECURITY_MANDATORY_LOW_RID:
		return INTEGRITY_LEVEL_LOW;       
		} 
		return INTEGRITY_LEVEL_LAST;
	}


	static DWORD GetProcessIntegrityLevel(IntegrityLevel* integrityLevel)
	{    
		DWORD err = ERROR_INVALID_PARAMETER; 

		if(GetOSVersion() < OS_VISTA)
		{
			*integrityLevel = INTEGRITY_LEVEL_MEDIUM;
			return ERROR_SUCCESS;
		}

		HANDLE hToken = NULL;
		BOOL fToken = ::OpenProcessToken(GetCurrentProcess(), TOKEN_READ, &hToken);
		if (!fToken || !hToken)
		{
			err = GetLastError();
			goto CLEANUP;
		}
		char buffer[100] = {};
		DWORD buf_size = sizeof(buffer);
		BOOL fTokenQuery = ::GetTokenInformation(hToken, TokenIntegrityLevel, reinterpret_cast<void *>(buffer), buf_size, &buf_size);
		if(!fTokenQuery)
		{
			err = GetLastError();
			goto CLEANUP;
		}

		TOKEN_MANDATORY_LABEL *label = reinterpret_cast<TOKEN_MANDATORY_LABEL *>(buffer);

		DWORD level = *GetSidSubAuthority(label->Label.Sid, 
			(DWORD)(UCHAR)(*GetSidSubAuthorityCount(label->Label.Sid)-1));
		*integrityLevel = GetIntegrityLevelFromRid(level);  

		err = ERROR_SUCCESS;

CLEANUP:
		if(hToken)
		{
			CloseHandle(hToken);
		}

		//
		return err;    
	}

	static  DWORD SetTokenIntegrityLevel(HANDLE token, IntegrityLevel integrity_level) {


		const wchar_t* integrity_level_str = GetIntegrityLevelString(integrity_level);
		if (!integrity_level_str) {
			// No mandatory level specified, we don't change it.
			return ERROR_SUCCESS;
		}

		PSID integrity_sid = NULL;
		if (!::ConvertStringSidToSid(integrity_level_str, &integrity_sid))
			return ::GetLastError();

		TOKEN_MANDATORY_LABEL label = {0};
		label.Label.Attributes = SE_GROUP_INTEGRITY;
		label.Label.Sid = integrity_sid;

		DWORD size = sizeof(TOKEN_MANDATORY_LABEL) + ::GetLengthSid(integrity_sid);
		BOOL result = ::SetTokenInformation(token, TokenIntegrityLevel, &label,
			size);
		::LocalFree(integrity_sid);

		return result ? ERROR_SUCCESS : ::GetLastError();
	}

	static DWORD SetObjectIntegrityLabel(HANDLE handle, SE_OBJECT_TYPE type,
		const wchar_t* ace_access,
		const wchar_t* integrity_level_sid) {
			// Build the SDDL string for the label.
			std::wstring sddl = L"S:(";     // SDDL for a SACL.
			sddl += SDDL_MANDATORY_LABEL;   // Ace Type is "Mandatory Label".
			sddl += L";;";                  // No Ace Flags.
			sddl += ace_access;             // Add the ACE access.
			sddl += L";;;";                 // No ObjectType and Inherited Object Type.
			sddl += integrity_level_sid;    // Trustee Sid.
			sddl += L")";

			DWORD error = ERROR_SUCCESS;
			PSECURITY_DESCRIPTOR sec_desc = NULL;

			PACL sacl = NULL;
			BOOL sacl_present = FALSE;
			BOOL sacl_defaulted = FALSE;

			if (::ConvertStringSecurityDescriptorToSecurityDescriptorW(sddl.c_str(),
				SDDL_REVISION,
				&sec_desc, NULL)) {
					if (::GetSecurityDescriptorSacl(sec_desc, &sacl_present, &sacl,
						&sacl_defaulted)) {
							error = ::SetSecurityInfo(handle, type,
								LABEL_SECURITY_INFORMATION, NULL, NULL, NULL,
								sacl);
					} else {
						error = ::GetLastError();
					}

					::LocalFree(sec_desc);
			} else {
				return::GetLastError();
			}

			return error;
	}

	static DWORD SetProcessIntegrityLevel(IntegrityLevel integrity_level) 
	{   
		if(GetOSVersion() < OS_VISTA)
		{        
			return ERROR_SUCCESS;
		}

		const wchar_t* integrity_level_str = GetIntegrityLevelString(integrity_level);
		if (!integrity_level_str) {
			// No mandatory level specified, we don't change it.
			return ERROR_SUCCESS;
		}

		// Before we can change the token, we need to change the security label on the
		// process so it is still possible to open the process with the new token.
		std::wstring ace_access = SDDL_NO_READ_UP;
		ace_access += SDDL_NO_WRITE_UP;

		DWORD error = SetObjectIntegrityLabel(::GetCurrentProcess(), SE_KERNEL_OBJECT,
			ace_access.c_str(),
			integrity_level_str);
		if (ERROR_SUCCESS != error)
			return error;

		HANDLE token_handle;
		if (!::OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_DEFAULT,
			&token_handle))
			return ::GetLastError();



		DWORD r = SetTokenIntegrityLevel(token_handle, integrity_level);
		CloseHandle(token_handle);
		return r;

	}

} // end namespace Integrity;
