﻿#include "downloadfromhttp.h"

DownLoadFromHttp::DownLoadFromHttp(QObject *parent)
	: QObject(parent)
{

}

DownLoadFromHttp::~DownLoadFromHttp()
{
	if (mReply != NULL)
	{
		if (!mReply->isFinished())
		{
			mReply->abort();
		}
		mReply->deleteLater();
	}
	if (file)
	{
		QString fileName = file->fileName();
		QFile::remove(fileName);//刪除文件
		file->flush();
		file->close();
		file = NULL;
		delete file;
	}
}

QString DownLoadFromHttp::GetExeDir()
{
	static wchar_t szbuf[MAX_PATH];
	::GetModuleFileNameW(NULL, szbuf, MAX_PATH);
	::PathRemoveFileSpecW(szbuf);

	std::wstring path;
	path.append(szbuf);
	if (path.at(path.size() - 1) != L'\\')
	{
		path.append(L"\\");
	}
	return QString::fromStdWString(path);
}

bool DownLoadFromHttp::StartDownLoadFile(QString strUrl)
{
	QUrl url(strUrl);
	QFileInfo fileInfo = url.path();
	QString fileName = fileInfo.fileName();
	if (fileName.isEmpty())
		fileName = "updateFlyTicket.zip";
	QString strFilePath = GetExeDir() + "\\" + fileName;
	file = new QFile(strFilePath);
	if (file->open(QIODevice::WriteOnly))
	{
		accessManager = new QNetworkAccessManager(this);
		connect(accessManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));

		mReply = accessManager->get(QNetworkRequest(url));
		connect(mReply, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
		connect(mReply, SIGNAL(downloadProgress(qint64, qint64)), this, SLOT(onDownloadProgress(qint64, qint64)));
	}
	else
	{
		return false;
	}
	return true;
}

void DownLoadFromHttp::replyFinished(QNetworkReply* reply)
{
	QString fileName;
	if (file)
	{
		file->flush();
		file->close();
		fileName = file->fileName();
		file = NULL;
		delete file;
	}
	if (reply->error() == QNetworkReply::NoError)
	{
		mReply->deleteLater();
		mReply = NULL;
		emit sigDownFinished(fileName);
	}
	else
	{
		reply->deleteLater();
		reply = NULL;
		QFile::remove(fileName);//刪除文件
		emit sigReplyErro();
	}
}

void DownLoadFromHttp::onDownloadProgress(qint64 bytesSent, qint64 bytesTotal)
{
	emit sigDownloadProgress(bytesSent, bytesTotal);
}

void DownLoadFromHttp::onReadyRead()
{
	if (file)
	{
		file->write(mReply->readAll());
		file->flush();
	}
}