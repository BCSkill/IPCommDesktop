<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS language="th_TH" version="2.1">
    <context>
        <name>MyMessageBox</name>
        <message>
            <location line="26" filename="mymessagebox.ui"/>
            <source>MyMessageBox</source>
            <translation></translation>
        </message>
        <message>
            <location line="99" filename="mymessagebox.ui"/>
            <location line="106" filename="mymessagebox.ui"/>
            <source>TextLabel</source>
            <translation></translation>
        </message>
        <message>
            <location line="99" filename="mymessagebox.cpp"/>
            <location line="105" filename="mymessagebox.cpp"/>
            <source>OK</source>
            <translation>กำหนด</translation>
        </message>
        <message>
            <location line="106" filename="mymessagebox.cpp"/>
            <source>cancel</source>
            <translation>ยกเลิก</translation>
        </message>
    </context>
    <context>
        <name>MyTitleBar</name>
        <message>
            <location line="54" filename="mytitlebar.cpp"/>
            <source>minimize</source>
            <translation>ลด</translation>
        </message>
        <message>
            <location line="55" filename="mytitlebar.cpp"/>
            <source>restore</source>
            <translation>การลดลง</translation>
        </message>
        <message>
            <location line="56" filename="mytitlebar.cpp"/>
            <source>maximize</source>
            <translation>เพิ่ม</translation>
        </message>
        <message>
            <location line="57" filename="mytitlebar.cpp"/>
            <source>close</source>
            <translation>ใกล้</translation>
        </message>
    </context>
    <context>
        <name>updateFlyTicket</name>
        <message>
            <location line="19" filename="updateflyticket.ui"/>
            <location line="153" filename="updateflyticket.ui"/>
            <source>upgrade</source>
            <translation>อัพเกรด</translation>
        </message>
        <message>
            <location line="62" filename="updateflyticket.ui"/>
            <source>Online upgrade</source>
            <translation>อัพเกรดออนไลน์</translation>
        </message>
        <message>
            <location line="186" filename="updateflyticket.ui"/>
            <source>cancel</source>
            <translation>ยกเลิก</translation>
        </message>
        <message>
            <location line="99" filename="updateflyticket.cpp"/>
            <location line="153" filename="updateflyticket.cpp"/>
            <location line="171" filename="updateflyticket.cpp"/>
            <location line="208" filename="updateflyticket.cpp"/>
            <source>error</source>
            <translation>ความผิดพลาด</translation>
        </message>
        <message>
            <location line="99" filename="updateflyticket.cpp"/>
            <location line="153" filename="updateflyticket.cpp"/>
            <source>File open error!</source>
            <translation>เกิดข้อผิดพลาดในการเปิดไฟล์!</translation>
        </message>
        <message>
            <location line="171" filename="updateflyticket.cpp"/>
            <source>The file is corrupt!</source>
            <translation>ไฟล์เสียหาย!</translation>
        </message>
        <message>
            <location line="208" filename="updateflyticket.cpp"/>
            <source>Network request error!</source>
            <translation>ข้อผิดพลาดคำขอเครือข่าย!</translation>
        </message>
        <message>
            <location line="216" filename="updateflyticket.cpp"/>
            <source>Notice</source>
            <translation>รวดเร็ว</translation>
        </message>
        <message>
            <location line="216" filename="updateflyticket.cpp"/>
            <source>The update is not completed. &#xd;
Is the update terminated?</source>
            <translation></translation>
        </message>
    </context>
</TS>
