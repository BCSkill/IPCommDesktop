﻿#ifndef UPDATEFLYTICKET_H
#define UPDATEFLYTICKET_H

#include <QWidget>
#include <QMouseEvent>
#include <QDomDocument>
#include <qsettings.h>
#include "mymessagebox.h"
#include "ui_updateflyticket.h"
#include "basewindow.h"
#include "downloadfromhttp.h"
#include "mymessagebox.h"
#include "include/zip_util.h"
#include <QtNetwork/QNetworkProxy>

#pragma execution_character_set("utf-8")

class updateFlyTicket : public QWidget
{
	Q_OBJECT

public:
	updateFlyTicket(QWidget *parent = 0);
	~updateFlyTicket();


	//************************************
	// Method:    SetUpdateDescribeInfo
	// FullName:  updateFlyTicket::SetUpdateDescribeInfo
	// Access:    public 
	// Returns:   void
	// Qualifier: 设置更新描述
	// Parameter: QString strDescribeInfo
	//************************************
	void SetUpdateDescribeInfo(QString strDescribeInfo);

	//************************************
	// Method:    SetUpdateUrl
	// FullName:  updateFlyTicket::SetUpdateUrl
	// Access:    public 
	// Returns:   void
	// Qualifier: 设置更新的URL
	// Parameter: QString strUrl
	//************************************
	void SetUpdateUrl(QString strUrl);

	void setExeName(QString strExeName);

	//设置代理
	void setProxy(int proxyModel, QString strUserName, QString strUserPwd, QString strAddress, QString strPort);

	//added by fanwenxing
	//设置参数
	void setArguments(QStringList);

	void StartMainExe(QString exeName);
private:
	void regedit();
	bool JudgeMainExeVersion();
	int VersionCompare(std::string v1, std::string v2);
	
protected:
	virtual void mousePressEvent(QMouseEvent *event);
	virtual void mouseMoveEvent(QMouseEvent *event);
	virtual void mouseReleaseEvent(QMouseEvent *event);
private slots:
	void doDownLoadFinish(QString strFileName);
	void downloadProgress(qint64 bytesSent, qint64 bytesTotal);
	void doReplyErro();
	void doExit();

	//added by fanwenxing
	void update();
private:
	Ui::updateFlyTicket ui;
	bool        mMoveing;
	QPoint      mMovePosition;
	DownLoadFromHttp *downLoad;
	QString m_strExeName;
	QString m_currentVersion;
	//added by fanwenxing
	QStringList arguments;
	bool mbDownLoadExe;
};

#endif // UPDATEFLYTICKET_H
