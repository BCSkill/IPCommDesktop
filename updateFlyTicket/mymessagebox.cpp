﻿#include "mymessagebox.h"
#include <QPainter>
#include <QCloseEvent>

MyMessageBox::MyMessageBox(QWidget *parent)
	: BaseWindow(parent)
	, m_eventLoop(NULL)
	, m_chooseResult(ID_CANCEL)
{
	ui.setupUi(this);
	this->setAttribute(Qt::WA_DeleteOnClose);
	initWindow();
}

MyMessageBox::~MyMessageBox()
{

}

void MyMessageBox::initWindow()
{
	initTitleBar();
	loadStyleSheet("MyMessageBox");
	Qt::WindowFlags flags = this->windowFlags();
	this->setWindowFlags(flags | Qt::Window);

	ui.inputContent->setVisible(false);

	connect(ui.pButtonOk, SIGNAL(clicked()), this, SLOT(onOkClicked()));
	connect(ui.pButtonCancel, SIGNAL(clicked()), this, SLOT(onCancelClicked()));
}

void MyMessageBox::initTitleBar()
{
	m_titleBar->move(0, 0);
	m_titleBar->setWindowBorderWidth(0);
	m_titleBar->setBackgroundColor(78, 103, 165);
	m_titleBar->setButtonType(ONLY_CLOSE_BUTTON);
	m_titleBar->setTitleWidth(this->width());
	m_titleBar->setTitleIcon(":/updateFlyTicket/Resources/logo.ico", QSize(16, 16));
}

void MyMessageBox::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);

	QPainterPath pathBack;
	pathBack.setFillRule(Qt::WindingFill);
	pathBack.addRoundedRect(QRect(0, 0, this->width(), this->height()) , 3 , 3);
	painter.setRenderHint(QPainter::SmoothPixmapTransform, true);
	painter.fillPath(pathBack, QBrush(QColor(255, 255, 255)));

	QPainterPath pathButtonBack;
	pathButtonBack.setFillRule(Qt::WindingFill);
	pathButtonBack.addRoundedRect(QRect(0, 116, this->width(), 48) , 3 , 3);
	painter.setRenderHint(QPainter::SmoothPixmapTransform, true);
	painter.fillPath(pathButtonBack, QBrush(QColor(245, 230, 233)));

	return QWidget::paintEvent(event);
}

void MyMessageBox::setWindowTitle(QString title , int titleFontSize)
{
	m_titleBar->setTitleContent(title, titleFontSize);
}

void MyMessageBox::setContentText(QString contentText)
{
	ui.MessageContent->setText(contentText);
}

void MyMessageBox::setMessageType(MessageType messageType)
{
	switch (messageType)
	{
	case MESSAGE_INFORMATION:
		ui.MessageIcon->setPixmap(QPixmap(":/updateFlyTicket/Resources/MyMessageBox/information.png"));
		break;
	case MESSAGE_WARNNING:
		ui.MessageIcon->setPixmap(QPixmap(":/updateFlyTicket/Resources/MyMessageBox/warnning.png"));
		break;
	case MESSAGE_QUESTION:
		ui.MessageIcon->setPixmap(QPixmap(":/updateFlyTicket/Resources/MyMessageBox/question.png"));
		break;
	case MESSAGE_INPUT:
		ui.MessageIcon->setVisible(false);
		ui.inputContent->setVisible(true);
	default:
		break;
	}
}

void MyMessageBox::setButtonType(MessageButtonType buttonType)
{
	switch (buttonType)
	{
	case BUTTON_OK:
	{
		ui.pButtonOk->setText(tr("OK"));
		ui.pButtonCancel->setVisible(false);
	}
		break;
	case BUTTON_OK_AND_CANCEL:
	{
		ui.pButtonOk->setText(tr("OK"));
		ui.pButtonCancel->setText(tr("cancel"));
	}
		break;
	default:
		break;
	}
}

void MyMessageBox::setMessageContent(QString messageContent)
{
	ui.MessageContent->setText(messageContent);
}

int MyMessageBox::showMyMessageBox(QWidget* parent, const QString &title, const QString &contentText, MessageType messageType, MessageButtonType messageButtonType, bool isModelWindow)
{
	MyMessageBox * myMessageBox = new MyMessageBox(parent);
	myMessageBox->setWindowTitle(title);
	myMessageBox->setContentText(contentText);
	myMessageBox->setMessageType(messageType);
	myMessageBox->setButtonType(messageButtonType);
	if (isModelWindow)
	{
		return myMessageBox->exec();
	}
	else
	{
		myMessageBox->show();
	}

	return -1;
}

int MyMessageBox::exec()
{
	this->setWindowModality(Qt::WindowModal); 
	show();
	m_eventLoop = new QEventLoop(this);
	m_eventLoop->exec();

	return m_chooseResult;
}

void MyMessageBox::onOkClicked()
{
	m_chooseResult = ID_OK;
	close();
}

void MyMessageBox::onCancelClicked()
{
	m_chooseResult = ID_CANCEL;
	close();
}

void MyMessageBox::closeEvent(QCloseEvent *event)
{
	if (m_eventLoop != NULL)
	{
		m_eventLoop->exit();
	}
	event->accept();
}