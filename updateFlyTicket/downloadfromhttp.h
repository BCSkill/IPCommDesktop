﻿#ifndef DOWNLOADFROMHTTP_H
#define DOWNLOADFROMHTTP_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QFile>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QFileInfo>
#include <shlwapi.h>


class DownLoadFromHttp : public QObject
{
	Q_OBJECT

public:
	DownLoadFromHttp(QObject *parent);
	~DownLoadFromHttp();

	//************************************
	// Method:    StartDownLoadFile
	// FullName:  DownLoadFromHttp::StartDownLoadFile
	// Access:    public 
	// Returns:   void
	// Qualifier: 开始下载文件
	// Parameter: QString strFilePath
	//************************************
	bool StartDownLoadFile(QString strUrl);

	//************************************
	// Method:    GetExeDir
	// FullName:  updateFlyTicket::GetExeDir
	// Access:    public 
	// Returns:   void
	// Qualifier: 获取exe路径
	//************************************
	QString GetExeDir();

public slots:
	void replyFinished(QNetworkReply*reply);
	void onDownloadProgress(qint64 bytesSent, qint64 bytesTotal);
	void onReadyRead();//

signals:
	void sigReplyErro();
	void sigDownFinished(QString fileName);
	void sigDownloadProgress(qint64 bytesSent, qint64 bytesTotal);
private:
	QNetworkAccessManager *accessManager;
	QNetworkRequest request;
	QNetworkReply *mReply;
	QFile *file;
};

#endif // DOWNLOADFROMHTTP_H
