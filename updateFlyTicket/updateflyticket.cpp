﻿#include "updateflyticket.h"
#include "createProcess/CreateProcessEx.h"
#include <QProcess>
#include <QDomDocument>
#include <QDomElement>
#include <QFile>
#include <QDir>

updateFlyTicket::updateFlyTicket(QWidget *parent)
{
	ui.setupUi(this);

	downLoad = NULL;
	mMoveing = false;

	ui.processbar->setDecimal(0);
	ui.processbar->setBarBackGroundColor(QColor(Qt::white));
	ui.processbar->setBarColor(QColor(100, 184, 255));
	ui.processbar->setShowBarSplitLine(false);
	ui.processbar->hide();
	ui.mLabelBK->setStyleSheet("background-color:#3692ef;");

	ui.labelOnline->setStyleSheet("color:white;");

	//ui.labelLogo->setStyleSheet("border-image:url(:/updateFlyTicket/Resources/IMChatClient.png) 0 0 0 0 ;");
	//不同版本的图标设定。
	ui.labelLogo->setStyleSheet("border-image:url(:/updateFlyTicket/Resources/OpenPlanet.png) 0 0 0 0 ;");

	ui.labelUpdateContent->setText("更新内容:");
	ui.labelUpdateContent->setStyleSheet("color:#ffa405;font: 87 14pt 微软雅黑;");

	ui.labelContent->setStyleSheet("color:white;font: 75 12pt 微软雅黑;");

	//加载样式
	QFile file(":/updateFlyTicket/Resources/flyticket.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();
	setWindowFlags(Qt::FramelessWindowHint);

	connect(ui.pushButton,SIGNAL(clicked()),this,SLOT(doExit()));
	connect(ui.updateButton, SIGNAL(clicked()), this, SLOT(update()));
	connect(ui.cancelButton, SIGNAL(clicked()), this, SLOT(doExit()));

	mbDownLoadExe = false;
}

updateFlyTicket::~updateFlyTicket()
{
	if (downLoad)
	{
		downLoad->deleteLater();
	}
}

void updateFlyTicket::mousePressEvent(QMouseEvent *event)
{
	mMoveing = true;
	mMovePosition = event->globalPos() - pos();
	return QWidget::mousePressEvent(event);
}

void updateFlyTicket::mouseMoveEvent(QMouseEvent *event)
{
	if (mMoveing && (event->buttons() && Qt::LeftButton)
		&& (event->globalPos() - mMovePosition).manhattanLength() > QApplication::startDragDistance())
	{
		move(event->globalPos() - mMovePosition);
		mMovePosition = event->globalPos() - pos();
	}
	return QWidget::mouseMoveEvent(event);
}

void updateFlyTicket::mouseReleaseEvent(QMouseEvent *event)
{
	mMoveing = false;
	return QWidget::mouseReleaseEvent(event);
}

void updateFlyTicket::SetUpdateDescribeInfo(QString strDescribeInfo)
{
	//qDebug() << strDescribeInfo;
	ui.labelContent->setText(strDescribeInfo);
	ui.labelContent->setWordWrap(true);
	ui.labelContent->setAlignment(Qt::AlignTop);
}

void updateFlyTicket::SetUpdateUrl(QString strUrl)
{
	downLoad = new DownLoadFromHttp(this);
	if (downLoad)
	{
		connect(downLoad, SIGNAL(sigDownFinished(QString)), this, SLOT(doDownLoadFinish(QString)));
		connect(downLoad, SIGNAL(sigDownloadProgress(qint64, qint64)), this, SLOT(downloadProgress(qint64, qint64)));
		if (!downLoad->StartDownLoadFile(strUrl))
		{
			if (ID_OK == MyMessageBox::showMyMessageBox(NULL, tr("error"), tr("File open error!"), MESSAGE_WARNNING, BUTTON_OK, true))
			{
				this->close();
			}
		}
	}
}

void updateFlyTicket::setExeName(QString strExeName)
{
	//qDebug() << strExeName;
	m_strExeName = strExeName;
}

void updateFlyTicket::setProxy(int proxyModel, QString strUserName, QString strUserPwd, QString strAddress, QString strPort)
{
	if (proxyModel != 0)  //使用代理
	{
		QNetworkProxy proxy;
		if (proxyModel == 1) //使用http代理
		{
			proxy.setType(QNetworkProxy::HttpProxy);
		}
		else if (proxyModel == 2) // 使用socket代理
		{
			proxy.setType(QNetworkProxy::Socks5Proxy);
		}
		if (!strAddress.isEmpty())
		{
			proxy.setHostName(strAddress);
		}
		if (!strPort.isEmpty())
		{
			proxy.setPort(strPort.toInt());
		}
		if (!strUserName.isEmpty())
		{
			proxy.setUser(strUserName);
		}
		if (!strUserPwd.isEmpty())
		{
			proxy.setPassword(strUserPwd);
		}
		QNetworkProxy::setApplicationProxy(proxy);
	}
}

void updateFlyTicket::doDownLoadFinish(QString strFileName)
{
	QFile file(strFileName);
	if (file.size() == 0)
	{
		if (!downLoad->StartDownLoadFile(arguments[2]))
		{
			if (ID_OK == MyMessageBox::showMyMessageBox(NULL, tr("error"), tr("File open error!"), MESSAGE_WARNNING, BUTTON_OK, true))
			{
				this->close();
			}
		}
	}
	else
	{
		if (!mbDownLoadExe)
		{
			QString strDir = downLoad->GetExeDir();
			if (ZipUtil::UnzipFile(strFileName.toStdWString(), strDir.toStdWString()))
			{
				CreateProcessLow(m_strExeName.toStdWString().c_str());
				QFile::remove(strFileName);//刪除文件
			}
			else
			{
				MyMessageBox::showMyMessageBox(NULL, tr("error"), tr("The file is corrupt!"), MESSAGE_WARNNING, BUTTON_OK, true);
			}
		}
		else
		{
			QString strExeName = QDir::currentPath() + "/OpenPlanet_update.exe";
			QProcess *myProcess = new QProcess(this);
			myProcess->startDetached(strExeName);
		}
		int ret = VersionCompare(m_currentVersion.toStdString(), "1.1.11");
		if (ret == -1)
		{
			
		}
		this->close();
	}
}

void updateFlyTicket::downloadProgress(qint64 bytesSent, qint64 bytesTotal)
{
	int nSize = 0;
	if (bytesTotal != 0)
	{
		nSize = (bytesSent * 100 / bytesTotal);
	}
	if (nSize < 10)
	{
		ui.processbar->setValue(0);
	}
	else
	{
		ui.processbar->setValue(nSize - 10);
	}
}

void updateFlyTicket::doReplyErro()
{
	if (ID_OK == MyMessageBox::showMyMessageBox(NULL, tr("error"), tr("Network request error!"), MESSAGE_WARNNING, BUTTON_OK, true))
	{
		this->close();
	}
}

void updateFlyTicket::doExit()
{
	if (ID_OK == MyMessageBox::showMyMessageBox(NULL, tr("Notice"), tr("The update is not completed. \nIs the update terminated?"), MESSAGE_WARNNING, BUTTON_OK_AND_CANCEL, true))
	{
		CreateProcessLow(m_strExeName.toStdWString().c_str(), TEXT("exeName ignoreUpdate"));
		this->close();
	}
	else
	{

	}
}

void updateFlyTicket::setArguments(QStringList stringList)
{
	this->arguments = stringList;
	this->SetUpdateDescribeInfo(arguments[3]);
	this->setExeName(arguments[4]);
}

void updateFlyTicket::regedit()
{
	//读写注册表的操作。
	QString appName = arguments[10];
	QDomDocument doc(QStringLiteral("xml"));
	QFile file("reg.xml");
	if (file.open(QIODevice::ReadOnly))
	{
		if (doc.setContent(&file))
		{
			QDomElement docElem = doc.documentElement();
			QDomNode n = docElem.firstChild();
			while (!n.isNull())
			{
				QDomElement e = n.toElement();
				if (!e.isNull())
				{
					//先进入版本节点。
					if (e.attributeNode("name").value() == appName)
					{
						//检索注册表目录。
						QDomNode i = e.firstChild();
						while (!i.isNull())
						{
							QDomElement f = i.toElement();
							//获取注册表目录。
							QString path = f.attributeNode("id").value();
							//定义QSettings对象并指定目录。
							QSettings *reg = new QSettings(path, QSettings::NativeFormat);
							//检索要写入的键值对。
							QDomNode j = f.firstChild();
							while (!j.isNull())
							{
								QDomElement g = j.toElement();
								//获取键名.
								QString key = g.attributeNode("key").value();
								//获取值。
								QString value = g.text();
								//根据标签类型作特殊处理。
								if (g.tagName() == "value")
								{
									//普通键值对。
								}
								if (g.tagName() == "path")
								{
									//路径键值对,根据不同的版本设定不同的名称。
									QString filePath = QApplication::applicationDirPath() + "/OpenPlanet.exe";
									value = filePath.replace("/", "\\") + value;
								}
								if (reg->value(key).toString().isEmpty())
								{
									//写入注册表。
									reg->setValue(key, value);
									reg->deleteLater();
								}

								j = j.nextSibling();
							}

							i = i.nextSibling();
						}
					}
				}

				n = n.nextSibling();
			}
		}
		file.close();
	}
}

void updateFlyTicket::update()
{
	ui.processbar->show();
	ui.updateButton->hide();
	ui.cancelButton->hide();

	regedit();
	this->setProxy(arguments[5].toInt(), arguments[6], arguments[7], arguments[8], arguments[9]);

	QString strUrl = arguments[2];
	/*if (JudgeMainExeVersion())
	{
		mbDownLoadExe = true;
		strUrl = arguments[2].replace("telecomm.zip", "telecomm_update.exe");
	}*/
	this->SetUpdateUrl(strUrl);
}

/*提供版本比对*/
int updateFlyTicket::VersionCompare(std::string v1, std::string v2)
{
	int vnum1 = 0, vnum2 = 0;
	for (int i = 0, j = 0; (i < v1.length() || j < v2.length());)
	{
		while (i < v1.length() && v1[i] != '.')
		{
			vnum1 = vnum1 * 10 + (v1[i] - '0');
			i++;
		}
		while (j < v2.length() && v2[j] != '.')
		{
			vnum2 = vnum2 * 10 + (v2[j] - '0');
			j++;
		}
		if (vnum1 > vnum2)
			return 1;
		if (vnum2 > vnum1)
			return -1;      //代表需要升级。
		vnum1 = vnum2 = 0;
		i++;
		j++;
	}
	return 0;
}

//判断主程序版本号
bool updateFlyTicket::JudgeMainExeVersion()
{
	QDomDocument doc(QStringLiteral("appConfig"));
	QFile file("config/config.xml");
	if (!file.open(QIODevice::ReadOnly))
	{
		return false;
	}
	if (!doc.setContent(&file))
	{
		file.close();
		return false;
	}
	file.close();
	QDomElement docElem = doc.documentElement();
	QDomNode n = docElem.firstChild();
	while (!n.isNull())
	{
		QDomElement e = n.toElement();
		if (!e.isNull())
		{
			if (e.tagName() == "version")
			{
				QDomNode nodeTemp = e.firstChild();
				while (!nodeTemp.isNull())
				{
					QDomElement elementTemp = nodeTemp.toElement();
					if (elementTemp.tagName() == "number")
					{
						m_currentVersion = elementTemp.text();
						int ret = VersionCompare(m_currentVersion.toStdString(), "1.1.0");
						if (ret == -1)
						{
							return true;
						}
						else
							return false;
					}
					nodeTemp = nodeTemp.nextSibling();
				}
			}
		}
		else
		{
			return false;
		}
		n = n.nextSibling();
	}
	return  false;
}

//启动程序
void updateFlyTicket::StartMainExe(QString exeName)
{
	QString strExeName = QDir::currentPath() + "/" + exeName;
	bool bResult = CreateProcessLow(strExeName.toStdWString().c_str());
	QFile::remove("openplanet_update.exe");//刪除文件
}