<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hy_AM">
<context>
    <name>MyMessageBox</name>
    <message>
        <location filename="mymessagebox.ui" line="26"/>
        <source>MyMessageBox</source>
        <translation>MyMessageBox</translation>
    </message>
    <message>
        <location filename="mymessagebox.ui" line="99"/>
        <location filename="mymessagebox.ui" line="106"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mymessagebox.cpp" line="99"/>
        <location filename="mymessagebox.cpp" line="105"/>
        <source>OK</source>
        <translation>լավ</translation>
    </message>
    <message>
        <location filename="mymessagebox.cpp" line="106"/>
        <source>cancel</source>
        <translation>չեղարկել</translation>
    </message>
</context>
<context>
    <name>MyTitleBar</name>
    <message>
        <location filename="mytitlebar.cpp" line="54"/>
        <source>minimize</source>
        <translation>փոքրացնել</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="55"/>
        <source>restore</source>
        <translation>վերականգնել</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="56"/>
        <source>maximize</source>
        <translation>առավելագույնի հասցնել</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="57"/>
        <source>close</source>
        <translation>փակել</translation>
    </message>
</context>
<context>
    <name>updateFlyTicket</name>
    <message>
        <location filename="updateflyticket.ui" line="19"/>
        <location filename="updateflyticket.ui" line="153"/>
        <source>upgrade</source>
        <translation>բարելավում</translation>
    </message>
    <message>
        <location filename="updateflyticket.ui" line="62"/>
        <source>Online upgrade</source>
        <translation>Առցանց բարելավում</translation>
    </message>
    <message>
        <location filename="updateflyticket.ui" line="186"/>
        <source>cancel</source>
        <translation>չեղարկել</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="99"/>
        <location filename="updateflyticket.cpp" line="153"/>
        <location filename="updateflyticket.cpp" line="171"/>
        <location filename="updateflyticket.cpp" line="208"/>
        <source>error</source>
        <translation>սխալը</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="99"/>
        <location filename="updateflyticket.cpp" line="153"/>
        <source>File open error!</source>
        <translation>Ֆայլի բաց սխալը:</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="171"/>
        <source>The file is corrupt!</source>
        <translation>Ֆայլը կոռումպացված է:</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="208"/>
        <source>Network request error!</source>
        <translation>Ցանցի պահանջի սխալ:</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="216"/>
        <source>Notice</source>
        <translation>Ծանուցում</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="216"/>
        <source>The update is not completed. 
Is the update terminated?</source>
        <translation>Թարմացումը չի ավարտվել:
Թարմացումը արդիականացվել է:</translation>
    </message>
</context>
</TS>
