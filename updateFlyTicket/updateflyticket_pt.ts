<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>MyMessageBox</name>
    <message>
        <location filename="mymessagebox.ui" line="26"/>
        <source>MyMessageBox</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mymessagebox.ui" line="99"/>
        <location filename="mymessagebox.ui" line="106"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mymessagebox.cpp" line="99"/>
        <location filename="mymessagebox.cpp" line="105"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="mymessagebox.cpp" line="106"/>
        <source>cancel</source>
        <translation>cancelar</translation>
    </message>
</context>
<context>
    <name>MyTitleBar</name>
    <message>
        <location filename="mytitlebar.cpp" line="54"/>
        <source>minimize</source>
        <translation>minimizar</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="55"/>
        <source>restore</source>
        <translation>restaurar</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="56"/>
        <source>maximize</source>
        <translation>maximizar</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="57"/>
        <source>close</source>
        <translation>perto</translation>
    </message>
</context>
<context>
    <name>updateFlyTicket</name>
    <message>
        <location filename="updateflyticket.ui" line="19"/>
        <location filename="updateflyticket.ui" line="153"/>
        <source>upgrade</source>
        <translation>atualizar</translation>
    </message>
    <message>
        <location filename="updateflyticket.ui" line="62"/>
        <source>Online upgrade</source>
        <translation>Atualização online</translation>
    </message>
    <message>
        <location filename="updateflyticket.ui" line="186"/>
        <source>cancel</source>
        <translation>cancelar</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="99"/>
        <location filename="updateflyticket.cpp" line="153"/>
        <location filename="updateflyticket.cpp" line="171"/>
        <location filename="updateflyticket.cpp" line="208"/>
        <source>error</source>
        <translation>erro</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="99"/>
        <location filename="updateflyticket.cpp" line="153"/>
        <source>File open error!</source>
        <translation>Erro de abertura de arquivo!</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="171"/>
        <source>The file is corrupt!</source>
        <translation>O arquivo está corrompido!</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="208"/>
        <source>Network request error!</source>
        <translation>Erro de solicitação de rede!</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="216"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="216"/>
        <source>The update is not completed. 
Is the update terminated?</source>
        <translation>A atualização não está concluída.
A atualização é finalizada?</translation>
    </message>
</context>
</TS>
