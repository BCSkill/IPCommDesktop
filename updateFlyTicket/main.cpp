﻿#include "updateflyticket.h"
#include <QtWidgets/QApplication>
#include <windows.h>
#include  "tlhelp32.h"
#include <QProcess>
#include <QDebug>
#include <QDir>
#include <qtranslator.h>

#define LOGFILEMAX 10000 


void LogInfo(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
	QString text;
	switch (type)
	{
	case QtDebugMsg:
		text = QString("Debug:");
		break;

	case QtWarningMsg:
		text = QString("Warning:");
		break;

	case QtCriticalMsg:
		text = QString("Critical:");
		break;

	case QtFatalMsg:
		text = QString("Fatal:");
	}

	QString context_info = QString("File:(%1) Line:(%2)").arg(QString(context.file)).arg(context.line);
	QString current_date_time = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss ddd");
	QString current_date = QString("(%1)").arg(current_date_time);
	QString message = QString("%1 %2 %3 %4").arg(text).arg(context_info).arg(msg).arg(current_date);
	QString fileDate = QDateTime::currentDateTime().toString("yyyy_MM_dd");
	QString strFileName = QString("updatelog/%1.txt").arg(fileDate);


	if (!QFile::exists(strFileName))//文件不存在，则创建
	{
		QDir fileDir = QFileInfo(strFileName).absoluteDir();
		QString strFileDir = QFileInfo(strFileName).absolutePath();
		if (!fileDir.exists()) //路径不存在，创建路径
		{
			fileDir.mkpath(strFileDir);
		}
	}
	QFile logFile(strFileName);
	if (!logFile.open(QIODevice::WriteOnly | QIODevice::Append))//未成功打开
	{
		return;
	}
	QTextStream text_stream(&logFile);
	text_stream << message << "\r\n";
	logFile.flush();
	logFile.close();
}

int main(int argc, char *argv[])
{
#if (QT_VERSION >= QT_VERSION_CHECK(5, 6, 0))
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif 

	QApplication a(argc, argv);
	QDir::setCurrent(QCoreApplication::applicationDirPath());
	//qInstallMessageHandler(LogInfo);  //日志模块
	QStringList arguments = QCoreApplication::arguments();
	
	if (arguments.size() == 11)
	{
		QLocale locale;
		switch (locale.language())
		{
			//系统语言为中文。
		case QLocale::Chinese:
			arguments.append(QString::fromWCharArray(L"简体中文"));
			break;
		case QLocale::Russian:
			arguments.append(QString::fromWCharArray(L"русский"));
			break;
		case QLocale::Armenian:
			arguments.append(QString::fromWCharArray(L"Հայերեն"));
			break;
		case QLocale::Spanish:
			arguments.append(QString::fromWCharArray(L"Español"));
			break;
		case QLocale::Portuguese:
			arguments.append(QString::fromWCharArray(L"Português"));
			break;
		case QLocale::Thai:
			arguments.append(QString::fromWCharArray(L"ไทย"));
			break;
		default:
			//其他所有情况设置为英文。
			arguments.append(QString::fromWCharArray(L"English"));
			break;
		}
	}
		
	if (arguments.size() == 12)
	{
		if (arguments[11] == QString::fromWCharArray(L"简体中文"))
		{
			QTranslator *trans = new QTranslator;
			trans->load(":/Simplified Chinese/Resources/updateflyticket_zh.qm");
			qApp->installTranslator(trans);
		}
		if (arguments[11] == QString::fromWCharArray(L"русский"))
		{
			QTranslator *trans = new QTranslator;
			trans->load(":/Russian/Resources/updateflyticket_ru.qm");
			qApp->installTranslator(trans);
		}
		if (arguments[11] == QString::fromWCharArray(L"Հայերեն"))
		{
			QTranslator *trans = new QTranslator;
			trans->load(":/Hayeren/Resources/updateflyticket_hy.qm");
			qApp->installTranslator(trans);
		}
		if (arguments[11] == QString::fromWCharArray(L"Español"))
		{
			QTranslator *trans = new QTranslator;
			trans->load(":/Spanish/Resources/updateflyticket_es.qm");
			qApp->installTranslator(trans);
		}
		if (arguments[11] == QString::fromWCharArray(L"Português"))
		{
			QTranslator *trans = new QTranslator;
			trans->load(":/Portuguese/Resources/updateflyticket_pt.qm");
			qApp->installTranslator(trans);
		}
		if (arguments[11] == QString::fromWCharArray(L"ไทย"))
		{
			QTranslator *trans = new QTranslator;
			trans->load(":/Thai/Resources/updateflyticket_th.qm");
			qApp->installTranslator(trans);
		}


		QProcess process;
		process.execute(QString("TASKKILL /IM %1 /F").arg(arguments[4]));
		process.close();
		updateFlyTicket w;
		w.setArguments(arguments);
		w.show();
		return a.exec();
	}
	else if (arguments.size() == 2)
	{
		if (arguments[1] == "ShowMainExe")
		{
			updateFlyTicket w;
			w.StartMainExe("OpenPlanet.exe");
		}
	}
	return 0;
}
