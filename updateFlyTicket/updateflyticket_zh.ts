<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>MyMessageBox</name>
    <message>
        <location filename="mymessagebox.ui" line="26"/>
        <source>MyMessageBox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mymessagebox.ui" line="99"/>
        <location filename="mymessagebox.ui" line="106"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mymessagebox.cpp" line="99"/>
        <location filename="mymessagebox.cpp" line="105"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="mymessagebox.cpp" line="106"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>MyTitleBar</name>
    <message>
        <location filename="mytitlebar.cpp" line="54"/>
        <source>minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="55"/>
        <source>restore</source>
        <translation>还原</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="56"/>
        <source>maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="57"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>updateFlyTicket</name>
    <message>
        <location filename="updateflyticket.ui" line="19"/>
        <location filename="updateflyticket.ui" line="153"/>
        <source>upgrade</source>
        <translation>升级</translation>
    </message>
    <message>
        <location filename="updateflyticket.ui" line="62"/>
        <source>Online upgrade</source>
        <translation>在线升级</translation>
    </message>
    <message>
        <location filename="updateflyticket.ui" line="186"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="99"/>
        <location filename="updateflyticket.cpp" line="153"/>
        <location filename="updateflyticket.cpp" line="171"/>
        <location filename="updateflyticket.cpp" line="208"/>
        <source>error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="99"/>
        <location filename="updateflyticket.cpp" line="153"/>
        <source>File open error!</source>
        <translation>文件打开错误!</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="171"/>
        <source>The file is corrupt!</source>
        <translation>文件已损坏!</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="208"/>
        <source>Network request error!</source>
        <translation>网络请求错误!</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="216"/>
        <source>Notice</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="216"/>
        <source>The update is not completed. 
Is the update terminated?</source>
        <translation>更新未完成,是否终止更新？</translation>
    </message>
    <message>
        <source>The update is not completed. Is the update terminated?</source>
        <translation type="vanished">更新未完成,是否终止更新？</translation>
    </message>
</context>
</TS>
