<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>MyMessageBox</name>
    <message>
        <location filename="mymessagebox.ui" line="26"/>
        <source>MyMessageBox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mymessagebox.ui" line="99"/>
        <location filename="mymessagebox.ui" line="106"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mymessagebox.cpp" line="99"/>
        <location filename="mymessagebox.cpp" line="105"/>
        <source>OK</source>
        <translation>Хорошо</translation>
    </message>
    <message>
        <location filename="mymessagebox.cpp" line="106"/>
        <source>cancel</source>
        <translation>отменить</translation>
    </message>
</context>
<context>
    <name>MyTitleBar</name>
    <message>
        <location filename="mytitlebar.cpp" line="54"/>
        <source>minimize</source>
        <translation>минимизировать</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="55"/>
        <source>restore</source>
        <translation>снижение</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="56"/>
        <source>maximize</source>
        <translation>максимизировать</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="57"/>
        <source>close</source>
        <translation>близко</translation>
    </message>
</context>
<context>
    <name>updateFlyTicket</name>
    <message>
        <location filename="updateflyticket.ui" line="19"/>
        <location filename="updateflyticket.ui" line="153"/>
        <source>upgrade</source>
        <translation>Обновить</translation>
    </message>
    <message>
        <location filename="updateflyticket.ui" line="62"/>
        <source>Online upgrade</source>
        <translation>Онлайн обновление</translation>
    </message>
    <message>
        <location filename="updateflyticket.ui" line="186"/>
        <source>cancel</source>
        <translation>отменить</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="99"/>
        <location filename="updateflyticket.cpp" line="153"/>
        <location filename="updateflyticket.cpp" line="171"/>
        <location filename="updateflyticket.cpp" line="208"/>
        <source>error</source>
        <translation>ошибка</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="99"/>
        <location filename="updateflyticket.cpp" line="153"/>
        <source>File open error!</source>
        <translation>Ошибка открытия файла!</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="171"/>
        <source>The file is corrupt!</source>
        <translation>Файл поврежден!</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="208"/>
        <source>Network request error!</source>
        <translation>Ошибка сетевого запроса!</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="216"/>
        <source>Notice</source>
        <translation>уведомление</translation>
    </message>
    <message>
        <location filename="updateflyticket.cpp" line="216"/>
        <source>The update is not completed. 
Is the update terminated?</source>
        <translation>Обновление не завершено.\nУстановлено ли обновление?</translation>
    </message>
</context>
</TS>
