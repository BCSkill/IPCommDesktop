﻿#include "scanqrloginsharelib.h"
#include <QDir>
#include <QDebug>
#include "QStringLiteralBak.h"
#include "globalmanager.h"
#ifndef Q_OS_WIN
#include "inline_mac.h"
#endif

ScanQRLoginShareLib::ScanQRLoginShareLib(QObject *parent)
: QObject(parent)
{
	scanWidget = NULL;
	pDataBase = NULL;
	ConnectLoginDB();
	OnInitWidget();
	LoadDatabase();
}

ScanQRLoginShareLib::~ScanQRLoginShareLib()
{
	if (scanWidget)
	{
		scanWidget->deleteLater();
		scanWidget = NULL;
	}
}

//连接登陆数据库
void ScanQRLoginShareLib::ConnectLoginDB()
{
	if (pDataBase == NULL)
		pDataBase = new LoginDatabaseOperaShareLib;
#ifdef Q_OS_WIN
    QString strDBPath = gSettingsManager->getUserPath()+"/database/common.db";
#else
    QString strDBPath = gSettingsManager->getUserPath() + "/database/common.db";
#endif
    if (pDataBase->ConnectLoginDB(strDBPath, "login"))
		qDebug() << tr("连接数据库成功!");
	else
		qDebug() << tr("连接数据库失败!");
}

//初始化窗口
void ScanQRLoginShareLib::OnInitWidget()
{
	scanWidget = new ScanQRWidget;
	connect(scanWidget, SIGNAL(sigCloseLoginWidget()), this, SIGNAL(sigCloseLoginWidget()));
	//connect(scanWidget, SIGNAL(sigClickedLogin()), this, SIGNAL(sigClickedLogin()));
	connect(scanWidget, SIGNAL(sigQRLoginSuccess(QVariantMap)), this, SIGNAL(sigQRLoginSuccess(QVariantMap)));
	connect(scanWidget, SIGNAL(sigProxy(NetWorkProxyInfo)), this, SLOT(slotInsertProxy(NetWorkProxyInfo)));

	scanWidget->show();
	scanWidget->showQrWidget();
}

//加载数据库信息
void ScanQRLoginShareLib::LoadDatabase()
{
	NetWorkProxyInfo info = pDataBase->GetNetWorkProxyInfoDB();

	if (info.nIndex == 1)
	  scanWidget->setProxy(info);
}

void ScanQRLoginShareLib::showErro(QString strMsg)
{
	if (scanWidget)
	{
		scanWidget->showErro(strMsg);
	}
}

void ScanQRLoginShareLib::slotInsertProxy(NetWorkProxyInfo info)
{
	if (pDataBase)
	{
		pDataBase->InsertProxyInfoDB(info);
	}
}
void ScanQRLoginShareLib::HideScanWidget()
{
	scanWidget->hide();
}
