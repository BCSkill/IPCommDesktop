<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>ScanLoginWidget</name>
    <message>
        <location filename="scanloginwidget.ui" line="14"/>
        <source>ScanLoginWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="scanloginwidget.ui" line="45"/>
        <source>阿玛尼好</source>
        <translation></translation>
    </message>
    <message>
        <location filename="scanloginwidget.ui" line="70"/>
        <source>切换账户</source>
        <translation></translation>
    </message>
    <message>
        <location filename="scanloginwidget.ui" line="95"/>
        <source>登录</source>
        <translation></translation>
    </message>
    <message>
        <location filename="scanloginwidget.cpp" line="46"/>
        <source>:/PerChat/Resources/person/temp.png</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ScanNetWorkSetWidget</name>
    <message>
        <location filename="scannetworksetwidget.ui" line="14"/>
        <source>ScanNetWorkSetWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="scannetworksetwidget.ui" line="35"/>
        <source>Network proxy settings</source>
        <translation>网络代理设置</translation>
    </message>
    <message>
        <location filename="scannetworksetwidget.ui" line="59"/>
        <source>Using proxy</source>
        <translation>使用代理</translation>
    </message>
    <message>
        <location filename="scannetworksetwidget.ui" line="81"/>
        <source>address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="scannetworksetwidget.ui" line="103"/>
        <source>port</source>
        <translation>端口</translation>
    </message>
    <message>
        <location filename="scannetworksetwidget.ui" line="125"/>
        <source>account</source>
        <translation>账户</translation>
    </message>
    <message>
        <location filename="scannetworksetwidget.ui" line="147"/>
        <source>password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="scannetworksetwidget.ui" line="169"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="scannetworksetwidget.ui" line="191"/>
        <source>open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="scannetworksetwidget.ui" line="277"/>
        <source>ok</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>ScanQRLoginShareLib</name>
    <message>
        <location filename="scanqrloginsharelib.cpp" line="40"/>
        <source>连接数据库成功!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="scanqrloginsharelib.cpp" line="42"/>
        <source>连接数据库失败!</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ScanQRWidget</name>
    <message>
        <location filename="scanqrwidget.cpp" line="221"/>
        <source>error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="scanqrwidget.cpp" line="221"/>
        <source>The network is unreachable. Please check the network status and try again~</source>
        <translation>网络不通，请查看网络状况后再试~</translation>
    </message>
    <message>
        <location filename="scanqrwidget.ui" line="19"/>
        <source>OpenPlanet</source>
        <translation>星际通讯</translation>
    </message>
    <message>
        <source>IPCom</source>
        <translation type="vanished">星际通讯</translation>
    </message>
</context>
<context>
    <name>ScanShowQRWidget</name>
    <message>
        <location filename="scanshowqrwidget.ui" line="14"/>
        <source>ScanShowQRWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="scanshowqrwidget.ui" line="45"/>
        <source>Please use your OpenPlanet App
 to scan the QR code to log in</source>
        <oldsource>Please use your Telecomm App
 to scan the QR code to log in</oldsource>
        <translation>请用星际通讯APP
扫描二维码登录</translation>
    </message>
</context>
</TS>
