<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="th_TH">
<context>
    <name>ScanLoginWidget</name>
    <message>
        <location filename="scanloginwidget.ui" line="14"/>
        <source>ScanLoginWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="scanloginwidget.ui" line="45"/>
        <source>阿玛尼好</source>
        <translation></translation>
    </message>
    <message>
        <location filename="scanloginwidget.ui" line="70"/>
        <source>切换账户</source>
        <translation></translation>
    </message>
    <message>
        <location filename="scanloginwidget.ui" line="95"/>
        <source>登录</source>
        <translation></translation>
    </message>
    <message>
        <location filename="scanloginwidget.cpp" line="46"/>
        <source>:/PerChat/Resources/person/temp.png</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ScanNetWorkSetWidget</name>
    <message>
        <location filename="scannetworksetwidget.ui" line="14"/>
        <source>ScanNetWorkSetWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="scannetworksetwidget.ui" line="35"/>
        <source>Network proxy settings</source>
        <translation>การตั้งค่าพร็อกซีเครือข่าย</translation>
    </message>
    <message>
        <location filename="scannetworksetwidget.ui" line="59"/>
        <source>Using proxy</source>
        <translation>ใช้พร็อกซี</translation>
    </message>
    <message>
        <location filename="scannetworksetwidget.ui" line="81"/>
        <source>address</source>
        <translation>ที่อยู่</translation>
    </message>
    <message>
        <location filename="scannetworksetwidget.ui" line="103"/>
        <source>port</source>
        <translation>พอร์ต</translation>
    </message>
    <message>
        <location filename="scannetworksetwidget.ui" line="125"/>
        <source>account</source>
        <translation>บัญชี</translation>
    </message>
    <message>
        <location filename="scannetworksetwidget.ui" line="147"/>
        <source>password</source>
        <translation>รหัสผ่าน</translation>
    </message>
    <message>
        <location filename="scannetworksetwidget.ui" line="169"/>
        <source>close</source>
        <translation>ใกล้</translation>
    </message>
    <message>
        <location filename="scannetworksetwidget.ui" line="191"/>
        <source>open</source>
        <translation>เปิด</translation>
    </message>
    <message>
        <location filename="scannetworksetwidget.ui" line="277"/>
        <source>ok</source>
        <translation>ตกลง</translation>
    </message>
</context>
<context>
    <name>ScanQRLoginShareLib</name>
    <message>
        <location filename="scanqrloginsharelib.cpp" line="40"/>
        <source>连接数据库成功!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="scanqrloginsharelib.cpp" line="42"/>
        <source>连接数据库失败!</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ScanQRWidget</name>
    <message>
        <source>IPCom</source>
        <translation type="vanished">การสื่อสารระหว่างดวงดาว</translation>
    </message>
    <message>
        <location filename="scanqrwidget.cpp" line="221"/>
        <source>error</source>
        <translation>ความผิดพลาด</translation>
    </message>
    <message>
        <location filename="scanqrwidget.cpp" line="221"/>
        <source>The network is unreachable. Please check the network status and try again~</source>
        <translation>เครือข่ายไม่สามารถเข้าถึงได้โปรดตรวจสอบสถานะเครือข่ายแล้วลองอีกครั้ง</translation>
    </message>
    <message>
        <location filename="scanqrwidget.ui" line="19"/>
        <source>OpenPlanet</source>
        <translation>星际通讯</translation>
    </message>
</context>
<context>
    <name>ScanShowQRWidget</name>
    <message>
        <location filename="scanshowqrwidget.ui" line="14"/>
        <source>ScanShowQRWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="scanshowqrwidget.ui" line="45"/>
        <source>Please use your OpenPlanet App
 to scan the QR code to log in</source>
        <oldsource>Please use your Telecomm App
 to scan the QR code to log in</oldsource>
        <translation>กรุณาใช้แอพของคุณ
  เพื่อสแกนรหัส QR เพื่อเข้าสู่ระบบ</translation>
    </message>
</context>
</TS>
