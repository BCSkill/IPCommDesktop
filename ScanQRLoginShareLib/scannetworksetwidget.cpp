﻿#include "scannetworksetwidget.h"
#include "ui_scannetworksetwidget.h"
#include <QFile>
#include <QDebug>

ScanNetWorkSetWidget::ScanNetWorkSetWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::ScanNetWorkSetWidget();
	ui->setupUi(this);

	//设置无边框
	this->setWindowFlags(Qt::FramelessWindowHint);
	this->setAttribute(Qt::WA_TranslucentBackground);    // 添加阴影时使用

	OnInitStyleSheet();

	connect(ui->pushButtonConfirm,SIGNAL(clicked()),this,SLOT(slotConfirm()));
	connect(ui->radioButtonOn, SIGNAL(clicked(bool)), this, SLOT(slotPressRadioButtonON(bool)));
	connect(ui->radioButtonOff, SIGNAL(clicked(bool)), this, SLOT(slotPressRadioButtonOFF(bool)));

	slotPressRadioButtonOFF(true);

}

ScanNetWorkSetWidget::~ScanNetWorkSetWidget()
{
	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

// Qualifier: 初始化样式表
void ScanNetWorkSetWidget::OnInitStyleSheet()
{
	//加载样式
	QFile file(":/QSS/Resources/QSS/ScanQRLoginShareLib/loginNetWorkSet.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();
}

void ScanNetWorkSetWidget::setProxy(NetWorkProxyInfo info)
{
	ui->lineEditAddress->setText(info.strAddress);
	ui->lineEditPort->setText(info.strPort);
	ui->lineEditAccount->setText(info.strUserName);
	ui->lineEditPsw->setText(info.strUserPwd);

	//proxyModel是0就不使用代理。
	if (info.proxyModel == 0)  
	{
		ui->radioButtonOff->setChecked(true);
		EnableEdit(false);
	}
	else
	{
		ui->radioButtonOn->setChecked(true);
		EnableEdit(true);
	}
}

void ScanNetWorkSetWidget::slotConfirm()
{
	QString strAddress = ui->lineEditAddress->text();
	QString strPort = ui->lineEditPort->text();
	QString strAccount = ui->lineEditAccount->text();
	QString strPsw = ui->lineEditPsw->text();

	NetWorkProxyInfo info;

	info.strAddress = strAddress;
	info.strPort = strPort;
	info.strUserName = strAccount;
	info.strUserPwd = strPsw;

	if (ui->radioButtonOn->isChecked())
	{
		info.proxyModel = 1;

		if (!info.strAddress.isEmpty() && info.strPort != "0")
		{
			QNetworkProxy proxy;
			proxy.setType(QNetworkProxy::HttpProxy);
			proxy.setHostName(info.strAddress);
			proxy.setPort(info.strPort.toInt());
			proxy.setUser(info.strUserName);
			proxy.setPassword(info.strUserPwd);
			QNetworkProxy::setApplicationProxy(proxy);
		}
	}
	if (ui->radioButtonOff->isChecked())
	{
		info.proxyModel = 0;
	}
	
	emit sigProxy(info);
}

void ScanNetWorkSetWidget::slotPressRadioButtonON(bool bCheck)
{
	EnableEdit(true);
}

void ScanNetWorkSetWidget::slotPressRadioButtonOFF(bool bCheck)
{
	EnableEdit(false);
}

void ScanNetWorkSetWidget::EnableEdit(bool bStatus)
{
	ui->lineEditAccount->setEnabled(bStatus);
	ui->lineEditAddress->setEnabled(bStatus);
	ui->lineEditPort->setEnabled(bStatus);
	ui->lineEditPsw->setEnabled(bStatus);
}
