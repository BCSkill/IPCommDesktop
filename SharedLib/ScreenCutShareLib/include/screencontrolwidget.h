﻿#ifndef SCREENCONTROLWIDGET_H
#define SCREENCONTROLWIDGET_H

#include <QWidget>
#include<QPainter>
#include<QImage>

namespace Ui {
	class ScreenControlWidget;
}

class OEScreenshot;
class ScreenControlWidget : public QWidget
{
	Q_OBJECT

public:
	 ScreenControlWidget(QWidget *parent = 0);
	~ScreenControlWidget();

	void connectSignalAndSlot();

	void setScreenQuote(OEScreenshot* screen);

	void initUI();

public slots:
	
	//取消按钮
	void slotCancelBtnClicked();

	//完成按钮
	void slotFinishBtnClicked();

	//添加字按钮
	void slotEditBtnClicked();

	//绘制矩形
	void slotRectangleBtnClicked();

	//绘制圆
	void slotDrawRoundBtnClicked();

	//绘制箭头
	void slotArrowBtnClicked();

	//绘制线条
	void slotDrawLineBtnClicked();

	//马赛克
	void slotMasicBtnClicked();

	//撤销
	void slotRevertBtnClicked();

	//保存
	void slotSaveBtnClicked();\
protected:
	virtual void paintEvent(QPaintEvent*);
private:
	Ui::ScreenControlWidget *ui;

	QPainter pt;
	QImage bg_gray;
	OEScreenshot *screen;

	bool isDrawLine;
	bool isDrawArrow;
	bool isDrawRound;
	bool isDrawText;
	bool isDrawRectang;
	bool isDrawMasic;
};

#endif // SCREENCONTROLWIDGET_H
