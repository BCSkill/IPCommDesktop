﻿#include "qaesutil.h"
#include <QDebug>
using namespace std;

QAesUtil::QAesUtil(QObject *parent)
: QObject(parent)
{

}

QAesUtil::~QAesUtil()
{

}

// Qualifier: AES加密数据 返回加密后base64
QByteArray QAesUtil::EncryptionData(QByteArray strData, QString passWord, bool base64)
{
	OctetString key = AESKeyForPassword(passWord);
	QByteArray byteArray = cryptoAES256(strData, key, ENCRYPTION);

	if (base64)
		return byteArray.toBase64();
	else
		return byteArray;
}

// Qualifier: AES解密数据 传入base64 返回字符串
QByteArray	QAesUtil::DecryptData(QByteArray strData, QString passWord, bool base64)
{
	OctetString key = AESKeyForPassword(passWord);
	//std::string string = key.as_string();
	QByteArray bytearry;
	if (base64)
		bytearry = QByteArray::fromBase64(strData);
	else
		bytearry = strData;

	return cryptoAES256(bytearry, key, DECRYPTION);
}

// Qualifier: 对key进行加密
OctetString QAesUtil::AESKeyForPassword(QString passWord)
{
	PBKDF* pbkdf = get_pbkdf("PBKDF2(SHA-1)");
	const byte salt[16] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF };
	return pbkdf->derive_key(32, passWord.toUtf8().toStdString(), salt, 16, 10000);
}

// Qualifier: AES256
QByteArray QAesUtil::cryptoAES256(QByteArray input, SymmetricKey passphrase, Cipher_Dir opt)
{
	const byte iv[16] = { 0xA, 1, 0xB, 5, 4, 0xF, 7, 9, 0x17, 3, 1, 6, 8, 0xC, 0xD, 91 };
	OctetString iv1(iv, 16);

	Pipe pipe(get_cipher("AES-256/CBC", passphrase, iv1, opt));

	try{
		pipe.process_msg(input.toStdString()); //encryption or decryption.   
	}
	catch (Botan::Decoding_Error &e)
	{
		std::cout << e.what() << std::endl;
		qDebug() << "erro " << e.what();

		return "";
	}
	string output = pipe.read_all_as_string();

	return QByteArray::fromStdString(output);
}