﻿#include "opdatamanager.h"

OPDataManager::OPDataManager()
{

}

OPDataManager::~OPDataManager()
{

}

void OPDataManager::encryption(QString text, QString key, QString path /*= "key"*/)
{
	//加密。
	TAesClass *aes = new TAesClass;
	aes->initKey(key);

	QByteArray byte = text.toLatin1();
	char *mingwen = byte.data();
	DWORD size = strlen(mingwen);
	char miwen[AesLength];
	aes->OnAesEncrypt((LPVOID)mingwen, size, (LPVOID)miwen); //进行加密

	QFile file(path);
	if (file.open(QIODevice::WriteOnly))
	{
		file.write(miwen, AesLength);
		file.close();
	}

	delete aes;
}

QString OPDataManager::Decryption(QString key, QString path /*= "key"*/)
{
	//解密。
	QString plainText;

	QFile file(path);
	if (file.open(QIODevice::ReadOnly))
	{
		char miwen[AesLength];
		file.read(miwen, AesLength);
		file.close();

		TAesClass *aes = new TAesClass;
		aes->initKey(key);
		plainText = aes->OnAesUncrypt((LPVOID)miwen, (DWORD)sizeof(miwen)); //进行解密

		delete aes;
	}

	return plainText;
}

QByteArray OPDataManager::encryptAES(QByteArray text, QString password, bool base64)
{
	QAesUtil aesUtil;
	QByteArray array = aesUtil.EncryptionData(text, password, base64);

	return array;
}

QByteArray OPDataManager::decryptAES(QByteArray letter, QString password, bool base64)
{
	QAesUtil aesUtil;
	QByteArray array = aesUtil.DecryptData(letter, password, base64);

	return array;
}

QString OPDataManager::encryptRSA(QString text, QString password)
{
	std::string clearText = text.toStdString();
	std::string pubKey = password.toStdString();

	RSAClass *rsa = new RSAClass;
	std::string miwen = rsa->rsa_pub_encrypt(clearText, pubKey);
	if (rsa)
	{
		delete rsa;
	}
	return QString::fromStdString(miwen);
}

QString OPDataManager::decryptRSA(QString letter, QString password)
{
	QByteArray array = QByteArray::fromBase64(letter.toUtf8());
	std::string cipherText = array.toStdString();
	std::string priKey = password.toStdString();

	RSAClass *rsa = new RSAClass;
	std::string mingwen = rsa->rsa_pri_decrypt(cipherText, priKey);
	if (rsa)
	{
		delete rsa;
	}
	return QString::fromStdString(mingwen);
}
