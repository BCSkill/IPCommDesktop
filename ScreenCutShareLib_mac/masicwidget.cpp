﻿#include "masicwidget.h"

masicWidget::masicWidget(QWidget *parent)
	: QWidget(parent)
{
	m_vecs.clear();
	m_vece.clear();
	m_vecSize.clear();
	m_iPenSize = 20;
}

masicWidget::~masicWidget()
{
}

void masicWidget::DrawRange(QPoint pS, QPoint pE)
{
	//printf("%d\n", m_vecs.size());
	m_vecs.push_back(pS);
	m_vece.push_back(pE);
	m_vecSize.push_back(m_iPenSize);
	update();
}
void masicWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);

	for (int i = 0; i < m_vecs.size(); i++)
	{
		QPen pen(QColor(0, 174, 255), m_vecSize[i]);
		painter.setPen(pen);
		painter.drawLine(m_vecs[i], m_vece[i]);
		//printf("%d\n", m_vecs.size());
	}
}

void masicWidget::revertMasic(int iNum)
{
	for (int i = 0; i < iNum; i++)
	{
		if (m_vecs.size() > 0)
		{
			m_vecs.pop_back();
			m_vece.pop_back();
			m_vecSize.pop_back();
		}
	}
}

void masicWidget::setMasicSize(int iNum)
{
	m_iPenSize = iNum;
	//printf("%d\n", iNum);
	update();
}