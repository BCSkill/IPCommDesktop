<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hy_AM">
<context>
    <name>ScreenControlWidget</name>
    <message>
        <location filename="screencontrolwidget.ui" line="35"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="80"/>
        <source>Rectangle</source>
        <translation>Ուղղանկյուն</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="147"/>
        <source>Circle</source>
        <translation>Շրջանակ</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="213"/>
        <source>Arrow</source>
        <translation>Սլաքը</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="279"/>
        <source>Brush</source>
        <translation>Խոզանակ</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="336"/>
        <source>Mosaic</source>
        <translation>Մոզաիկա</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="391"/>
        <source>Text</source>
        <translation>Տեքստ</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="496"/>
        <source>Revert</source>
        <translation>Վերադարձեք</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="546"/>
        <source>Save</source>
        <translation>Պահել</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="636"/>
        <source>Exit</source>
        <translation>Ելք</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="696"/>
        <source>Complete</source>
        <translation>Լրացրեք</translation>
    </message>
</context>
<context>
    <name>ScreenPenSettingWidget</name>
    <message>
        <location filename="screenpensettingwidget.ui" line="17"/>
        <source>ScreenPenSettingWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="screenpensettingwidget.ui" line="976"/>
        <source>Degree</source>
        <translation></translation>
    </message>
</context>
</TS>
