<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>ScreenControlWidget</name>
    <message>
        <location filename="screencontrolwidget.ui" line="35"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="80"/>
        <source>Rectangle</source>
        <translation>画矩形</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="147"/>
        <source>Circle</source>
        <translation>画圆</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="213"/>
        <source>Arrow</source>
        <translation>画箭头</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="279"/>
        <source>Brush</source>
        <translation>笔刷</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="336"/>
        <source>Mosaic</source>
        <translation>马赛克</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="391"/>
        <source>Text</source>
        <translation>添加文字</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="496"/>
        <source>Revert</source>
        <translation>撤销</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="546"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="636"/>
        <source>Exit</source>
        <translation>退出截图</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="696"/>
        <source>Complete</source>
        <translation>完成截图</translation>
    </message>
</context>
<context>
    <name>ScreenPenSettingWidget</name>
    <message>
        <location filename="screenpensettingwidget.ui" line="17"/>
        <source>ScreenPenSettingWidget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="screenpensettingwidget.ui" line="976"/>
        <source>Degree</source>
        <translation>模糊度</translation>
    </message>
</context>
</TS>
