<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>ScreenControlWidget</name>
    <message>
        <location filename="screencontrolwidget.ui" line="35"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="80"/>
        <source>Rectangle</source>
        <translation>Прямоугольник</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="147"/>
        <source>Circle</source>
        <translation>Круг</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="213"/>
        <source>Arrow</source>
        <translation>Стрела</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="279"/>
        <source>Brush</source>
        <translation>кисть</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="336"/>
        <source>Mosaic</source>
        <translation>мозаика</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="391"/>
        <source>Text</source>
        <translation>Текст</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="496"/>
        <source>Revert</source>
        <translation>возвращаться</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="546"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="636"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="696"/>
        <source>Complete</source>
        <translation>полный</translation>
    </message>
</context>
<context>
    <name>ScreenPenSettingWidget</name>
    <message>
        <location filename="screenpensettingwidget.ui" line="17"/>
        <source>ScreenPenSettingWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="screenpensettingwidget.ui" line="976"/>
        <source>Degree</source>
        <translation>степень</translation>
    </message>
</context>
</TS>
