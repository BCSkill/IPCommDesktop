﻿#include "userprofilewidget.h"
#include "ui_userprofilewidget.h"
#include <QDebug>
#include "QStringLiteralBak.h"
#include "QJsonDocument.h"
#include <QDesktopWidget>
#include "qgroupqr.h"
#include "qrencodesharelib.h"
UserProfileWidget::UserProfileWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::UserProfileWidget();
	ui->setupUi(this);
	
	//qDebug() << parent->objectName();
    //setWindowModality(Qt::WindowModal);
	this->setAttribute(Qt::WA_DeleteOnClose);
    this->setWindowTitle(tr("personal information"));
#ifdef Q_OS_WIN
    QPixmap mask(":/profile/Resources/profile/profileMask.png");
    this->setMask(mask.mask());
#endif
    this->setWindowFlags(Qt::FramelessWindowHint);// | Qt::WindowMinimizeButtonHint);
	closeBtn = new QPushButton(this);
	closeBtn->setStyleSheet("QPushButton{border-image: url(:/profile/Resources/profile/close.png);}");
	closeBtn->setCursor(Qt::PointingHandCursor);
	closeBtn->setToolTip(tr("close"));
	closeBtn->resize(24, 24);
	closeBtn->move(310, 10);
	headerBtn = new QPushButton(this);
	headerBtn->setStyleSheet("QPushButton{border-image: url(:/profile/Resources/profile/header.png);}");
	headerBtn->setCursor(Qt::PointingHandCursor);
	headerBtn->setToolTip(tr("Modify avatar"));
	headerBtn->resize(24, 24);
	headerBtn->move(280, 10);
	editBtn = new QPushButton(this);
	editBtn->setObjectName("editBtn");
	editBtn->setStyleSheet("QPushButton#editBtn{border-image: url(:/profile/Resources/profile/edit.png);}"
		"QPushButton#editBtn:hover{border-image: url(:/profile/Resources/profile/edit_hover.png);}");
	editBtn->setToolTip(tr("Modify information"));
	editBtn->resize(52, 52);
	editBtn->move(275, 313);
	connect(closeBtn, SIGNAL(clicked(bool)), this, SLOT(close()));
	connect(headerBtn, SIGNAL(clicked(bool)), this, SIGNAL(sigOpenAvatarWidget()));
	connect(editBtn, SIGNAL(clicked(bool)), this, SIGNAL(sigOpenUserInfoWidget()));
	connect(ui->strangersBtn, SIGNAL(clicked()), this, SLOT(slotClickStrangersBtn()));
	connect(ui->QRTitleBtn, SIGNAL(clicked()), this, SLOT(slotClickQRcodeBtn()));
	connect(ui->QRImgBtn, SIGNAL(clicked()), this, SLOT(slotClickQRcodeBtn()));
	ui->headerLabel->installEventFilter(this);

	QFile file(":/QSS/Resources/QSS/OPMainWidgetShareLib/userprofilewidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();
}

UserProfileWidget::~UserProfileWidget()
{
	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

bool UserProfileWidget::eventFilter(QObject *obj, QEvent *e)
{
	if (obj == ui->headerLabel)
	{
		if (e->type() == QEvent::MouseButtonPress)
		{
			QMouseEvent *event = (QMouseEvent *)e;
			mouse = event->pos();   //设置移动的原始位置。
		}
		if (e->type() == QEvent::MouseMove)
		{
			QMouseEvent *event = (QMouseEvent *)e;
			if (mouse.x() >= 0)
			{
				//首先通过做差值，获得鼠标位移的距离。
				int x = event->pos().x() - mouse.x();
				int y = event->pos().y() - mouse.y();
				//移动本窗体。
				this->move(this->x() + x, this->y() + y);
			}
		}
		if (e->type() == QEvent::MouseButtonRelease)
		{
			QMouseEvent *event = (QMouseEvent *)e;
			mouse.setX(-1);
		}
	}

	return QWidget::eventFilter(obj, e);
}

void UserProfileWidget::setUserInfo(UserInfo info)
{
	this->userInfo = info;

	QFile file(info.strUserAvatarLocal);
	QPixmap pix;
	if (file.open(QIODevice::ReadOnly))
	{
		QByteArray byteArray = file.readAll();
		file.close();
		pix.loadFromData(byteArray);
	}

	if (pix.isNull())
		pix.load(":/PerChat/Resources/person/temp.png");

	this->setHeaderImage(pix);
	ui->noteLabel->setText(info.strUserNickName);
	ui->signLabel->setText(info.strSign);
	ui->accountLabel->setText(info.strAccountName);
	ui->nameLabel->setText(info.strUserNickName);
	ui->lianxinLabel->setText(QString::number(info.nUserID));
	ui->mailLabel->setText(info.strEmil);

	if (info.disableStrangers == 0)
		ui->strangersBtn->setChecked(true);
	if (info.disableStrangers == 1)
		ui->strangersBtn->setChecked(false);
}

void UserProfileWidget::setHeaderImage(QPixmap pix)
{
	pix = pix.scaled(ui->headerLabel->size(), Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation);

	ui->headerLabel->setPixmap(pix);
}

void UserProfileWidget::slotClickStrangersBtn()
{
	emit sigDisableStrangers(ui->strangersBtn->isChecked());
}

void UserProfileWidget::slotClickQRcodeBtn()
{
	QVariantMap data;
	data.insert("type", "addFriend");
	data.insert("userID", this->userInfo.nUserID);

	QByteArray bytes = QJsonDocument::fromVariant(data).toJson();
	QString string = bytes;

	QRenCodeShareLib qr;
	QImage image = qr.GenerateQRcode(string);
	QPixmap pixmap = QPixmap::fromImage(image);

	QPixmap base(200, 200);
	base.fill(Qt::white);
	pixmap = pixmap.scaled(160, 160);
	QPainter *painter = new QPainter;
	painter->begin(&base);
	painter->drawPixmap(20, 20, pixmap);
	painter->end();
	delete painter;

	QPoint pos = QWidget::mapToGlobal(this->pos());
	QRect deskRt = QApplication::desktop()->availableGeometry(pos);

	//GroupInfo group = gDataBaseOpera->DBGetGroupFromID(groupID);
	QPixmap pix(this->userInfo.strUserAvatarLocal);
	if (pix.isNull())
	{
		QByteArray bytearray = "";
		QFile file(this->userInfo.strUserAvatarLocal);
		if (file.open(QIODevice::ReadOnly) && file.size() != 0)
		{
			bytearray = file.readAll();
		}
		file.close();
		if (!pix.loadFromData(bytearray) || bytearray == "")
		{
			pix.load(":/PerChat/Resources/person/temp.png");
		}
	}
	QGroupQR * pQRWidget = new QGroupQR();
	pQRWidget->OpenImg(base);
	pQRWidget->SetNickName(this->userInfo.strUserNickName);
	pQRWidget->setNoteText(tr("Scan the QR code on the top and add me as a friend."));
	pQRWidget->SetIDLabel(tr("ID:")+ QString::number(this->userInfo.nUserID));
	pQRWidget->OnSetPicPath(pix);
	pQRWidget->show();
}