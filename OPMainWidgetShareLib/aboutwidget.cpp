﻿#include "aboutwidget.h"
#include "QStringLiteralBak.h"
#include "ui_aboutwidget.h"

AboutWidget::AboutWidget(QWidget *parent)
: QWidget(parent)
{
	ui = new Ui::AboutWidget();

	ui->setupUi(this);

	setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
	setAttribute(Qt::WA_TranslucentBackground);

	connect(ui->closeBtn, SIGNAL(clicked(bool)), this, SLOT(close()));

	QFile file(":/QSS/Resources/QSS/OPMainWidgetShareLib/aboutwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	setContentsMargins(10, 10, 10, 10);

	ui->label_4->setOpenExternalLinks(true);
	ui->label_4->setText(QString::fromLocal8Bit("<style> a {text-decoration: none;color:rgb(106,130,166);} </style>  <a href=https://ipcom.io//>https://ipcom.io//</a>"));
}

AboutWidget::~AboutWidget()
{
	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

void AboutWidget::setName(QString name)
{
//	ui->nameLabel->setText(name);
}


void AboutWidget::setNumber(QString number)
{
	ui->numberLabel->setText(number);
}

//鼠标事件的处理。
void AboutWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void AboutWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void AboutWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}
void AboutWidget::paintEvent(QPaintEvent * event)
{
	QPainterPath path;
	path.setFillRule(Qt::WindingFill);
	path.addRect(10, 10, this->width() - 20, this->height() - 20);

	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing, true);
	//painter.fillPath(path, QBrush(Qt::white));

	QColor color(0, 0, 0, 50);
	for (int i = 0; i < 10; i++)
	{
		QPainterPath path;
		path.setFillRule(Qt::WindingFill);
		path.addRect(10 - i, 10 - i, this->width() - (10 - i) * 2, this->height() - (10 - i) * 2);
		color.setAlpha(150 - qSqrt(i) * 50);
		painter.setPen(color);
		painter.drawPath(path);
	}
}
