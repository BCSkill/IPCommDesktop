﻿#include "CSysTrayIcon.h"
#include <QtWidgets/QApplication>
#include <qwinfunctions.h>
#include "QStringLiteralBak.h"
#include <qdebug.h>
#include "immainwidget.h"

#define WM_TRAYNOTIFY WM_USER+300
//鼠标悬停显示消息盒子时间
#define SUSPEND_TIME_TOSHOW 10

CSysTrayIcon::CSysTrayIcon(QObject *parent)
	: QObject(parent)
{
	qApp->installNativeEventFilter(this);

	mTrayMenu = NULL;        //系统托盘菜单
	ActionCheckUpdate = NULL; //检查更新
	ActionExit = NULL;        //退出
	globalPos = QPoint(0, 0);
	localPos = QPoint(0, 0);

	suspendTimes = 0;
	CreateTrayMenu();
}

CSysTrayIcon::~CSysTrayIcon()
{
	Shell_NotifyIcon(NIM_DELETE, &nid);
	if (mTrayMenu)
	{
		mTrayMenu->deleteLater();
	}
	if (mLabel)
	{
		mLabel->deleteLater();
	}
}

void CSysTrayIcon::CreateTrayIcon(QIcon icon)
{
	mLabel = new QLabel;
	nid.cbSize = sizeof nid;

	nid.hIcon = QtWin::toHICON(icon.pixmap(16, 16));
	nid.hWnd = HWND(mLabel->winId());
	nid.uCallbackMessage = WM_TRAYNOTIFY;
	nid.uID = 1;
	nid.uFlags = NIF_ICON | NIF_MESSAGE;
	wcscpy(nid.szTip, tr("OpenPlanet").toStdWString().c_str());

	Shell_NotifyIcon(NIM_ADD, &nid);
}
void CSysTrayIcon::CreateTrayMenu()
{
	if (!mTrayMenu)
	{
		mTrayMenu = new QMenu;
	}

	if (!ActionCheckUpdate)
	{
		ActionCheckUpdate = new QAction(tr("about"), this);
		mTrayMenu->addAction(ActionCheckUpdate);
	}

	ActionCheckUpdate->setToolTip(tr("about"));
	ActionCheckUpdate->setIcon(QIcon(":/mainWidget/Resources/mainWidget/space.png"));
	connect(ActionCheckUpdate, SIGNAL(triggered()), this, SIGNAL(sigabout()));

	mTrayMenu->addSeparator();
	if (!ActionExit)
	{
		ActionExit = new QAction(tr("quit"), this);
		connect(ActionExit, SIGNAL(triggered()), this, SIGNAL(sigExit()));
		mTrayMenu->addAction(ActionExit);
	}

	ActionExit->setToolTip(tr("quit"));
	ActionExit->setIcon(QIcon(":/Login/Resources/login/title_close_hover.png"));

}
void CSysTrayIcon::CreateRightMenu(int x, int y)
{
	if (mTrayMenu)
	{
		mTrayMenu->move(QPoint(x, y));
		mTrayMenu->exec();
		connect(IMMainWidget::self(), SIGNAL(sigDestroyIcon()), this, SLOT(slotSysTrayExit()));
	}
}

//气泡通知 显示在托盘图标上的机制
//bool CSysTrayIcon::nativeEventFilter(const QByteArray & eventType, void * message, long * result)
//{
//	if (eventType == "windows_generic_MSG" || eventType == "windows_dispatcher_MSG")
//	{
//		//全局坐标
//		globalPos = QCursor::pos();
//		MSG * pMsg = reinterpret_cast<MSG *>(message);
//		if (pMsg->message == WM_TRAYNOTIFY)
//		{	
//			switch (pMsg->lParam)
//			{
//				case WM_RBUTTONDOWN:
//				{
//					QPoint point = QCursor::pos();
//					CreateRightMenu(point.x(), point.y());
//					break;
//				}
//				case WM_LBUTTONDOWN:
//				{
//					emit sigMouseLBttonDown();
//					break;
//				}
//				case WM_MOUSEMOVE:
//				{
//					isMouseInTray = true;
//					suspendTimes = 0;
//					//系统托盘内坐标
//					localPos = QCursor::pos();
//					break;
//				}
//			}
//
//		}
//
//		if (isMouseInTray == true)
//			suspendTimes++;
//		if (isMouseInTray == true && isMouseOutTray == true && suspendTimes == SUSPEND_TIME_TOSHOW)
//		{
//			//RECT *iconRect;			
//			//HRESULT iconResult = Shell_NotifyIconGetRect(nid., iconRect);
//
//			isMouseOutTray = false;
//			emit sigMouseSuspendIn(localPos);
//		}
//		if (globalPos != localPos)
//		{
//			if (boxRect.contains(globalPos))
//			{
//				//预留
//			}
//			else
//			{
//				isMouseInTray = false;
//				isMouseOutTray = true;
//				suspendTimes = 0;
//				emit sigMouseSuspendOut(localPos, 1);
//			}
//		}
//	}
//	return false;
//}

//弹窗通知 显示在任务栏上方的机制
bool CSysTrayIcon::nativeEventFilter(const QByteArray & eventType, void * message, long * result)
{
	if (eventType == "windows_generic_MSG" || eventType == "windows_dispatcher_MSG")
	{
		//全局坐标
		globalPos = QCursor::pos();
		MSG * pMsg = reinterpret_cast<MSG *>(message);
		if (pMsg->message == WM_TRAYNOTIFY)
		{
			switch (pMsg->lParam)
			{
			case WM_RBUTTONDOWN:
			{
				QPoint point = QCursor::pos();
				CreateRightMenu(point.x(), point.y());
				break;
			}
			case WM_LBUTTONDOWN:
			{
				emit sigMouseLBttonDown();
				break;
			}
			case WM_MOUSEMOVE:
			{
				isMouseInTray = true;
				outOfTrayFlag = true;
				suspendTimes = 0;
				//系统托盘内坐标
				localPos = QCursor::pos();
				break;
			}
			}
		}
		if (isFlash)
		{
			if (isMouseInTray == true)
			{
				suspendTimes++;
			}
			if (isMouseInTray == true && isMouseOutTray == true && suspendTimes == SUSPEND_TIME_TOSHOW)
			{
				isMouseOutTray = false;
				outOfTrayFlag = false;
				flag = false;
				emit sigMouseSuspendIn(localPos);
			}
			//坐标会有延迟 如果鼠标移速过快会有1-2像素的误差
			if (globalPos != localPos)
			{
				isMouseOutTray = true;

				//鼠标第一次移出托盘图标矩形框 延迟1s隐藏
				if (outOfTrayFlag == true)
				{
					suspendTimes = 0;
					isMouseInTray = false;
					outOfTrayFlag = false;
					emit sigMouseSuspendOut(localPos, 1);
				}
				//弹窗界面包含鼠标指针
				else if (boxRect.contains(globalPos) && (flag == false))
				{
					outOfWindowFlag = false;
					outOfTrayFlag = false;
					pointBuff = QPoint(0, 0);
					emit sigMouseSuspendIn(pointBuff);
				}
				//鼠标从菜单框移出 瞬间隐藏
				else if (outOfWindowFlag == false)
				{
					isMouseInTray = false;
					outOfWindowFlag = true;
					suspendTimes = 0;
					emit sigMouseSuspendOut(localPos, 0);
				}
			}
		}
		if (!isFlash)
		{
			 isMouseInTray = true;
			 isMouseOutTray = true;
			 outOfTrayFlag = true;
			 outOfWindowFlag = true;
			 flag = true;
		}
	}
	return false;
}


void CSysTrayIcon::setIcon(QIcon icon)
{
	if (nid.hIcon)
	{
		DestroyIcon(nid.hIcon);
		nid.hIcon = NULL;
		nid.hIcon = QtWin::toHICON(icon.pixmap(16, 16));
		Shell_NotifyIcon(NIM_MODIFY, &nid);//更改托盘图标
	}
}

void CSysTrayIcon::slotSysTrayExit()
{
	if (nid.hIcon)
	{
		DestroyIcon(nid.hIcon);
		nid.hIcon = NULL;
		Shell_NotifyIcon(NIM_DELETE, &nid);
	}
	//emit sigExit();
	/*if (mTrayMenu)
	{
		mTrayMenu->deleteLater();
	}
	if (mLabel)
	{
		mLabel->deleteLater();
	}*/
}


QPoint CSysTrayIcon::getTrayMenuPos()
{
	return sysTrayPos;
}

void CSysTrayIcon::slotReceiveBoxRect(QRect rect)
{
	boxRect = rect;
}

void CSysTrayIcon::slotGetFlashStatus(bool isflash)
{
	isFlash = isflash;
}