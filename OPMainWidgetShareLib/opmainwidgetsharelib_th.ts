<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="th_TH">
<context>
    <name>AboutWidget</name>
    <message>
        <location filename="aboutwidget.ui" line="20"/>
        <location filename="aboutwidget.ui" line="82"/>
        <source>about</source>
        <translation>บน</translation>
    </message>
</context>
<context>
    <name>CSysTrayIcon</name>
    <message>
        <location filename="CSysTrayIcon.cpp" line="50"/>
        <source>OpenPlanet</source>
        <translation>การสื่อสารระหว่างดวงดาว</translation>
    </message>
    <message>
        <location filename="CSysTrayIcon.cpp" line="63"/>
        <location filename="CSysTrayIcon.cpp" line="67"/>
        <source>about</source>
        <translation>บน</translation>
    </message>
    <message>
        <location filename="CSysTrayIcon.cpp" line="74"/>
        <location filename="CSysTrayIcon.cpp" line="79"/>
        <source>quit</source>
        <translation>เลิก</translation>
    </message>
</context>
<context>
    <name>ChangeAvatarWidget</name>
    <message>
        <location filename="userprofile/changeavatarwidget.ui" line="26"/>
        <location filename="userprofile/changeavatarwidget.ui" line="123"/>
        <source>Change avatar</source>
        <translation>เปลี่ยนภาพแทนตัว</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.ui" line="340"/>
        <source>Upload picture</source>
        <translation>อัพโหลดภาพ</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.ui" line="370"/>
        <source>OK</source>
        <translation>ตกลง</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.ui" line="400"/>
        <source>Cancel</source>
        <translation>ยกเลิก</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.cpp" line="249"/>
        <source>choose photo</source>
        <translation>เลือกรูปภาพ</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.cpp" line="249"/>
        <source>Image file(*.jpg *.png *.bmp)</source>
        <translation>ไฟล์รูปภาพ (*. jpg * .png * .bmp)</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.cpp" line="267"/>
        <source>Wait a moment</source>
        <translation>รอสักครู่</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.cpp" line="267"/>
        <source>The avatar is uploading!</source>
        <translation>อวตารกำลังอัพโหลด!</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.cpp" line="295"/>
        <source>Sorry</source>
        <translation>ขอโทษ</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.cpp" line="295"/>
        <source>Uploading avatar failed!</source>
        <translation>การอัปโหลดอวาตาร์ล้มเหลว!</translation>
    </message>
</context>
<context>
    <name>FunctionWidget</name>
    <message>
        <location filename="FunctionWidget.ui" line="14"/>
        <source>FunctionWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="FunctionWidget.ui" line="72"/>
        <source>Features</source>
        <translation>ฟังก์ชั่นการแนะนำ</translation>
    </message>
    <message>
        <location filename="FunctionWidget.ui" line="267"/>
        <location filename="FunctionWidget.cpp" line="53"/>
        <source>Full chain</source>
        <translation>โซ่เต็ม</translation>
    </message>
    <message>
        <location filename="FunctionWidget.cpp" line="66"/>
        <source>Full platform</source>
        <translation>แพลตฟอร์มเต็มรูปแบบ</translation>
    </message>
    <message>
        <location filename="FunctionWidget.cpp" line="79"/>
        <source>Mining</source>
        <translation>การทำเหมืองแร่</translation>
    </message>
    <message>
        <location filename="FunctionWidget.cpp" line="92"/>
        <source>Encrypted communication</source>
        <translation>การสื่อสารที่เข้ารหัส</translation>
    </message>
    <message>
        <location filename="FunctionWidget.cpp" line="105"/>
        <source>Digital assets</source>
        <translation>สินทรัพย์ดิจิทัล</translation>
    </message>
</context>
<context>
    <name>IMMainWidget</name>
    <message>
        <location filename="immainwidget.ui" line="179"/>
        <source>message</source>
        <translation>ข่าวสาร</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="204"/>
        <source>contact</source>
        <translation>ติดต่อ</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="226"/>
        <source>robot</source>
        <translation>หุ่นยนต์</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="248"/>
        <source>device</source>
        <translation>เครื่อง</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="343"/>
        <source>Modify Information</source>
        <translation>แก้ไขข้อมูล</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="368"/>
        <source>test</source>
        <translation>ทดสอบ</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="436"/>
        <location filename="immainwidget.ui" line="467"/>
        <source>minimize</source>
        <translation>ลด</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="498"/>
        <location filename="immainwidget.cpp" line="762"/>
        <source>maximize</source>
        <translation>เพิ่ม</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="529"/>
        <source>close</source>
        <translation>ใกล้</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="674"/>
        <source>Copyright © Pansoft</source>
        <translation>ลิขสิทธิ์© Pulian Software Co. , Ltd.</translation>
    </message>
    <message>
        <location filename="immainwidget.cpp" line="455"/>
        <source>space</source>
        <translation>ช่องว่าง</translation>
    </message>
    <message>
        <source>interPlanet</source>
        <translation type="vanished">ระหว่างดวงดาว</translation>
    </message>
    <message>
        <location filename="immainwidget.cpp" line="459"/>
        <source>base</source>
        <translation>ฐาน</translation>
    </message>
    <message>
        <location filename="immainwidget.cpp" line="472"/>
        <source>Setting</source>
        <translation>การตั้งค่า</translation>
    </message>
    <message>
        <location filename="immainwidget.cpp" line="768"/>
        <source>restore</source>
        <translation>ฟื้นฟู</translation>
    </message>
    <message>
        <location filename="immainwidget.cpp" line="457"/>
        <location filename="immainwidget.cpp" line="821"/>
        <source>OpenPlanet</source>
        <translation>การสื่อสารระหว่างดวงดาว</translation>
    </message>
</context>
<context>
    <name>NetWarningWidget</name>
    <message>
        <location filename="netwarningwidget.ui" line="14"/>
        <source>NetWarningWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="netwarningwidget.ui" line="121"/>
        <source>The network is unavailable, please check the network settings!</source>
        <translation>เครือข่ายใช้งานไม่ได้โปรดตรวจสอบการตั้งค่าเครือข่าย!</translation>
    </message>
</context>
<context>
    <name>QAboutWidget</name>
    <message>
        <location filename="settings/qaboutwidget.ui" line="14"/>
        <source>QAboutWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qaboutwidget.ui" line="189"/>
        <source>current version0.0.0.0</source>
        <translation>current version0.0.0.0</translation>
    </message>
    <message>
        <location filename="settings/qaboutwidget.ui" line="291"/>
        <source>Updates</source>
        <translation>อัพเดท</translation>
    </message>
    <message>
        <location filename="settings/qaboutwidget.ui" line="338"/>
        <source>Features</source>
        <translation>ฟังก์ชั่นการแนะนำ</translation>
    </message>
    <message>
        <location filename="settings/qaboutwidget.cpp" line="37"/>
        <source>Current Version:</source>
        <translation>รุ่นปัจจุบัน:</translation>
    </message>
</context>
<context>
    <name>QAccWidget</name>
    <message>
        <location filename="settings/qaccwidget.ui" line="26"/>
        <source>QAccWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qaccwidget.ui" line="308"/>
        <source>sign out</source>
        <translation>ออกจากระบบ</translation>
    </message>
    <message>
        <source>Telecomm ID：</source>
        <oldsource>interPlanet ID：</oldsource>
        <translation type="vanished">ID ดวงดาว：</translation>
    </message>
    <message>
        <location filename="settings/qaccwidget.cpp" line="56"/>
        <source>ID：</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QGeneralWidget</name>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="17"/>
        <source>QGeneralWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="106"/>
        <source>Language</source>
        <translation>ภาษา</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="138"/>
        <source>English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="143"/>
        <source>简体中文</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="148"/>
        <source>русский</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="153"/>
        <source>Հայերեն</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="158"/>
        <source>Español</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="163"/>
        <source>Português</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="168"/>
        <source>ไทย</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="236"/>
        <source>Document</source>
        <translation>เอกสาร</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="267"/>
        <source>Files received will be stored in this folder</source>
        <translation>ไฟล์ที่ได้รับจะถูกเก็บไว้ในโฟลเดอร์นี้</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="391"/>
        <source>Change</source>
        <translation>เปลี่ยนแปลง</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="432"/>
        <source>Open folder</source>
        <translation>เปิดโฟลเดอร์</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.cpp" line="54"/>
        <source>Select folder</source>
        <translation>เลือกโฟลเดอร์</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.cpp" line="61"/>
        <location filename="settings/qgeneralwidget.cpp" line="67"/>
        <source>Warning</source>
        <translation>การเตือน</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.cpp" line="61"/>
        <source>Do not select the same folder！</source>
        <translation>อย่าเลือกโฟลเดอร์เดียวกัน!</translation>
    </message>
    <message>
        <source>Restart Telecomm to take effect</source>
        <translation type="vanished">รีสตาร์ทเพื่อให้มีผล</translation>
    </message>
    <message>
        <source>  Telecomm will restart for the language  
settings to take effect</source>
        <translation type="vanished">มันจะรีสตาร์ทเพื่อให้การตั้งค่าภาษามีผล</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.cpp" line="67"/>
        <source>Restart OpenPlanet to take effect</source>
        <translation>รีสตาร์ทเพื่อให้มีผล</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.cpp" line="99"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.cpp" line="99"/>
        <source>  OpenPlanet will restart for the language  
settings to take effect</source>
        <translation>มันจะรีสตาร์ทเพื่อให้การตั้งค่าภาษามีผล</translation>
    </message>
</context>
<context>
    <name>QSetKeyWidget</name>
    <message>
        <location filename="settings/qsetkeywidget.ui" line="14"/>
        <source>QSetKeyWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qsetkeywidget.ui" line="36"/>
        <source>Please enter a new shortcut on the keyboard</source>
        <translation>โปรดป้อนทางลัดใหม่บนคีย์บอร์ด</translation>
    </message>
    <message>
        <location filename="settings/qsetkeywidget.ui" line="46"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qsetkeywidget.ui" line="83"/>
        <source>OK</source>
        <translation>ตกลง</translation>
    </message>
    <message>
        <location filename="settings/qsetkeywidget.ui" line="99"/>
        <source>Cancel</source>
        <translation>ยกเลิก</translation>
    </message>
    <message>
        <location filename="settings/qsetkeywidget.cpp" line="69"/>
        <source>No</source>
        <translation>ไม่</translation>
    </message>
</context>
<context>
    <name>QShortCutWidget</name>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="14"/>
        <source>QShortCutWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="106"/>
        <source>Send message</source>
        <translation>ส่งข้อความ</translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="137"/>
        <source>Enter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="142"/>
        <source>Ctrl+Enter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="204"/>
        <source>Screen capture</source>
        <translation>จับภาพหน้าจอ</translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="229"/>
        <source>Alt+A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="331"/>
        <source>Open hotkey</source>
        <translation>เปิดฮอตคีย์</translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="356"/>
        <source>Ctrl+Alt+W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="455"/>
        <source>Detect hotkey</source>
        <translation>ตรวจหาฮอตคีย์</translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="465"/>
        <source>Remind me when hotkey conflicts</source>
        <translation>เตือนฉันเมื่อฮอตคีย์ขัดแย้งกัน</translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="509"/>
        <source>Restore default</source>
        <translation>คืนค่าเริ่มต้น</translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.cpp" line="60"/>
        <location filename="settings/qshortcutwidget.cpp" line="87"/>
        <source>Type a new shortcut</source>
        <translation>พิมพ์ทางลัดใหม่</translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.cpp" line="145"/>
        <location filename="settings/qshortcutwidget.cpp" line="149"/>
        <location filename="settings/qshortcutwidget.cpp" line="204"/>
        <location filename="settings/qshortcutwidget.cpp" line="208"/>
        <source>none</source>
        <translation>ไม่</translation>
    </message>
</context>
<context>
    <name>QThemeSwitchWidget</name>
    <message>
        <location filename="settings/qthemeswitchwidget.ui" line="17"/>
        <source>QGeneralWidget</source>
        <translation></translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="obsolete">ภาษา</translation>
    </message>
    <message>
        <location filename="settings/qthemeswitchwidget.ui" line="106"/>
        <source>Theme Style</source>
        <translation>รูปแบบสไตล์</translation>
    </message>
    <message>
        <location filename="settings/qthemeswitchwidget.ui" line="138"/>
        <location filename="settings/qthemeswitchwidget.cpp" line="35"/>
        <location filename="settings/qthemeswitchwidget.cpp" line="37"/>
        <location filename="settings/qthemeswitchwidget.cpp" line="59"/>
        <location filename="settings/qthemeswitchwidget.cpp" line="71"/>
        <source>Light color</source>
        <translation>สีอ่อน</translation>
    </message>
    <message>
        <location filename="settings/qthemeswitchwidget.ui" line="143"/>
        <location filename="settings/qthemeswitchwidget.cpp" line="33"/>
        <location filename="settings/qthemeswitchwidget.cpp" line="57"/>
        <location filename="settings/qthemeswitchwidget.cpp" line="69"/>
        <source>Dark color</source>
        <translation>สีเข้ม</translation>
    </message>
    <message>
        <location filename="settings/qthemeswitchwidget.cpp" line="52"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="settings/qthemeswitchwidget.cpp" line="52"/>
        <source>The program will restart to switch theme, Y or N?</source>
        <translation>โปรแกรมรีสตาร์ทเพื่อให้มีผลใช้งานแล้ว</translation>
    </message>
</context>
<context>
    <name>ReadConfig</name>
    <message>
        <location filename="readconfig.cpp" line="18"/>
        <source>xml</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SettingsWidget</name>
    <message>
        <location filename="settings/settingswidget.ui" line="20"/>
        <location filename="settings/settingswidget.ui" line="157"/>
        <source>Setting</source>
        <translation>การตั้งค่า</translation>
    </message>
    <message>
        <location filename="settings/settingswidget.cpp" line="45"/>
        <source>Account settings</source>
        <translation>การตั้งค่าบัญชี</translation>
    </message>
    <message>
        <location filename="settings/settingswidget.cpp" line="46"/>
        <source>General settings</source>
        <translation>การตั้งค่าทั่วไป</translation>
    </message>
    <message>
        <location filename="settings/settingswidget.cpp" line="47"/>
        <source>Shortcut button</source>
        <translation>ปุ่มทางลัด</translation>
    </message>
    <message>
        <location filename="settings/settingswidget.cpp" line="48"/>
        <source>Theme swithing</source>
        <translation>การสลับธีม</translation>
    </message>
    <message>
        <location filename="settings/settingswidget.cpp" line="49"/>
        <source>About</source>
        <translation>เกี่ยวกับ</translation>
    </message>
</context>
<context>
    <name>UserInfoWidget</name>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="31"/>
        <location filename="userprofile/userinfowidget.ui" line="141"/>
        <source>Profile Edit</source>
        <translation>แก้ไขโปรไฟล์</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="234"/>
        <source>Nickname</source>
        <translation>ชื่อเล่น</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="279"/>
        <source>Signature</source>
        <translation>ลายเซ็น</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="349"/>
        <source>Gender</source>
        <translation>เพศ</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="363"/>
        <location filename="userprofile/userinfowidget.cpp" line="109"/>
        <location filename="userprofile/userinfowidget.cpp" line="122"/>
        <source>Male</source>
        <translation>ชาย</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="368"/>
        <location filename="userprofile/userinfowidget.cpp" line="111"/>
        <location filename="userprofile/userinfowidget.cpp" line="124"/>
        <source>Female</source>
        <translation>เพศหญิง</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="424"/>
        <source>Phone Number</source>
        <translation>เบอร์โทรศัพท์</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="469"/>
        <source>E-mail</source>
        <translation></translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="519"/>
        <source>Save</source>
        <translation>บันทึก</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="532"/>
        <source>Close</source>
        <translation>ใกล้</translation>
    </message>
</context>
<context>
    <name>UserProfileWidget</name>
    <message>
        <location filename="userprofile/userprofilewidget.ui" line="14"/>
        <source>UserProfileWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.ui" line="169"/>
        <source>Account</source>
        <translation>บัญชี</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.ui" line="217"/>
        <source>Nickname</source>
        <translation>ชื่อเล่น</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.ui" line="273"/>
        <source>ID</source>
        <translation></translation>
    </message>
    <message>
        <source>InterPlanet ID</source>
        <translation type="vanished">ID ดวงดาว</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.ui" line="323"/>
        <source>E-mail</source>
        <translation></translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.ui" line="377"/>
        <source>QR code card</source>
        <translation>รหัส QR</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.ui" line="430"/>
        <source>Receive stranger news</source>
        <translation>รับข่าวคนแปลกหน้า</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.cpp" line="18"/>
        <source>personal information</source>
        <translation>ข้อมูลส่วนบุคคล</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.cpp" line="27"/>
        <source>close</source>
        <translation>ใกล้</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.cpp" line="33"/>
        <source>Modify avatar</source>
        <translation>เปลี่ยนภาพแทนตัว</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.cpp" line="40"/>
        <source>Modify information</source>
        <translation>แก้ไขข้อมูล</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.cpp" line="184"/>
        <source>Scan the QR code on the top and add me as a friend.</source>
        <translation>สแกนรหัส QR ที่ด้านบนและเพิ่มฉันเป็นเพื่อน</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.cpp" line="185"/>
        <source>ID:</source>
        <translation></translation>
    </message>
    <message>
        <source>InterPlanet ID:</source>
        <translation type="vanished">ID ดวงดาว：</translation>
    </message>
</context>
</TS>
