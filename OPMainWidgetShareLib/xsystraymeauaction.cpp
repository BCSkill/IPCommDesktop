#include "xsystraymeauaction.h"
#include "ui_xsystraymeauaction.h"
#include <QMouseEvent>

xSysTrayMeauAction::xSysTrayMeauAction(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::xSysTrayMeauAction)
{
    ui->setupUi(this);

	QFile styleFile(":/QSS/Resources/QSS/OPMainWidgetShareLib/xsystraymeauaction.qss");
	styleFile.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(styleFile.readAll());
	setStyleSheet(styleSheet);
	styleFile.close();

    connect(ui->pushButton,SIGNAL(clicked()),this,SIGNAL(sigabout()));
    connect(ui->pushButton_2,SIGNAL(clicked()),this,SIGNAL(sigExit()));
}

xSysTrayMeauAction::~xSysTrayMeauAction()
{
    delete ui;
}
