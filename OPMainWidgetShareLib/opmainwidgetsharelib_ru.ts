<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AboutWidget</name>
    <message>
        <location filename="aboutwidget.ui" line="20"/>
        <location filename="aboutwidget.ui" line="82"/>
        <source>about</source>
        <translation>около</translation>
    </message>
</context>
<context>
    <name>CSysTrayIcon</name>
    <message>
        <location filename="CSysTrayIcon.cpp" line="50"/>
        <source>OpenPlanet</source>
        <translation>Открытая планета</translation>
    </message>
    <message>
        <location filename="CSysTrayIcon.cpp" line="63"/>
        <location filename="CSysTrayIcon.cpp" line="67"/>
        <source>about</source>
        <translation>около</translation>
    </message>
    <message>
        <location filename="CSysTrayIcon.cpp" line="74"/>
        <location filename="CSysTrayIcon.cpp" line="79"/>
        <source>quit</source>
        <translation>уволиться</translation>
    </message>
</context>
<context>
    <name>ChangeAvatarWidget</name>
    <message>
        <location filename="userprofile/changeavatarwidget.ui" line="26"/>
        <location filename="userprofile/changeavatarwidget.ui" line="123"/>
        <source>Change avatar</source>
        <translation>изменение</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.ui" line="340"/>
        <source>Upload picture</source>
        <translation>Загрузить</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.ui" line="370"/>
        <source>OK</source>
        <translation>Хорошо</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.ui" line="400"/>
        <source>Cancel</source>
        <translation>отменить</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.cpp" line="249"/>
        <source>choose photo</source>
        <translation>выбрать фото</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.cpp" line="249"/>
        <source>Image file(*.jpg *.png *.bmp)</source>
        <translation>Файл изображения (*. Jpg * .png * .bmp)</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.cpp" line="267"/>
        <source>Wait a moment</source>
        <translation>Подождите минутку</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.cpp" line="267"/>
        <source>The avatar is uploading!</source>
        <translation>Аватар загружается!</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.cpp" line="295"/>
        <source>Sorry</source>
        <translation>сожалею</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.cpp" line="295"/>
        <source>Uploading avatar failed!</source>
        <translation>Загрузка аватара не удалась!</translation>
    </message>
</context>
<context>
    <name>FunctionWidget</name>
    <message>
        <location filename="FunctionWidget.ui" line="14"/>
        <source>FunctionWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="FunctionWidget.ui" line="72"/>
        <source>Features</source>
        <translation>функция</translation>
    </message>
    <message>
        <location filename="FunctionWidget.ui" line="267"/>
        <location filename="FunctionWidget.cpp" line="53"/>
        <source>Full chain</source>
        <translation>Полная цепь</translation>
    </message>
    <message>
        <location filename="FunctionWidget.cpp" line="66"/>
        <source>Full platform</source>
        <translation>Полная платформа</translation>
    </message>
    <message>
        <location filename="FunctionWidget.cpp" line="79"/>
        <source>Mining</source>
        <translation>добыча</translation>
    </message>
    <message>
        <location filename="FunctionWidget.cpp" line="92"/>
        <source>Encrypted communication</source>
        <translation>Зашифрованная связь</translation>
    </message>
    <message>
        <location filename="FunctionWidget.cpp" line="105"/>
        <source>Digital assets</source>
        <translation>Цифровые активы</translation>
    </message>
</context>
<context>
    <name>IMMainWidget</name>
    <message>
        <location filename="immainwidget.ui" line="179"/>
        <source>message</source>
        <translatorcomment>message</translatorcomment>
        <translation>сообщение</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="204"/>
        <source>contact</source>
        <translation>контакт</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="226"/>
        <source>robot</source>
        <translation>робот</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="248"/>
        <source>device</source>
        <translation>устройство</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="343"/>
        <source>Modify Information</source>
        <translation>Изменить информацию</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="368"/>
        <source>test</source>
        <translation>тестовое задание</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="436"/>
        <location filename="immainwidget.ui" line="467"/>
        <source>minimize</source>
        <translation>минимизировать</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="498"/>
        <location filename="immainwidget.cpp" line="762"/>
        <source>maximize</source>
        <translation>максимизировать</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="529"/>
        <source>close</source>
        <translation>близко</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="674"/>
        <source>Copyright © Pansoft</source>
        <translation>Copyright © Pansoft</translation>
    </message>
    <message>
        <location filename="immainwidget.cpp" line="455"/>
        <source>space</source>
        <translation>пространство</translation>
    </message>
    <message>
        <source>interPlanet</source>
        <translation type="vanished">Интер Планета</translation>
    </message>
    <message>
        <location filename="immainwidget.cpp" line="459"/>
        <source>base</source>
        <translation>база</translation>
    </message>
    <message>
        <location filename="immainwidget.cpp" line="472"/>
        <source>Setting</source>
        <translation>настройка</translation>
    </message>
    <message>
        <location filename="immainwidget.cpp" line="768"/>
        <source>restore</source>
        <translation>восстановить</translation>
    </message>
    <message>
        <location filename="immainwidget.cpp" line="457"/>
        <location filename="immainwidget.cpp" line="821"/>
        <source>OpenPlanet</source>
        <translation>Открытая планета</translation>
    </message>
</context>
<context>
    <name>MenuWidget</name>
    <message>
        <source>wallet</source>
        <translation type="vanished">бумажник</translation>
    </message>
</context>
<context>
    <name>NetWarningWidget</name>
    <message>
        <location filename="netwarningwidget.ui" line="14"/>
        <source>NetWarningWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="netwarningwidget.ui" line="121"/>
        <source>The network is unavailable, please check the network settings!</source>
        <translation>Сеть недоступна, пожалуйста, проверьте это</translation>
    </message>
</context>
<context>
    <name>QAboutWidget</name>
    <message>
        <location filename="settings/qaboutwidget.ui" line="14"/>
        <source>QAboutWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qaboutwidget.ui" line="189"/>
        <source>current version0.0.0.0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qaboutwidget.ui" line="291"/>
        <source>Updates</source>
        <translation>апгрейд</translation>
    </message>
    <message>
        <location filename="settings/qaboutwidget.ui" line="338"/>
        <source>Features</source>
        <translation>Характеристики</translation>
    </message>
    <message>
        <location filename="settings/qaboutwidget.cpp" line="37"/>
        <source>Current Version:</source>
        <translation>Текущая версия:</translation>
    </message>
</context>
<context>
    <name>QAccWidget</name>
    <message>
        <location filename="settings/qaccwidget.ui" line="26"/>
        <source>QAccWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qaccwidget.ui" line="308"/>
        <source>sign out</source>
        <translation>выход</translation>
    </message>
    <message>
        <source>Telecomm ID：</source>
        <oldsource>interPlanet ID：</oldsource>
        <translation type="vanished">ИнтерПланет ID:</translation>
    </message>
    <message>
        <location filename="settings/qaccwidget.cpp" line="56"/>
        <source>ID：</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QGeneralWidget</name>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="17"/>
        <source>QGeneralWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="106"/>
        <source>Language</source>
        <translation>язык</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="138"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="143"/>
        <source>简体中文</source>
        <translation>简体中文</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="148"/>
        <source>русский</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="153"/>
        <source>Հայերեն</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="158"/>
        <source>Español</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="163"/>
        <source>Português</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="168"/>
        <source>ไทย</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="236"/>
        <source>Document</source>
        <translation>Документ</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="267"/>
        <source>Files received will be stored in this folder</source>
        <translation>Полученные файлы будут храниться в этой папке</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="391"/>
        <source>Change</source>
        <translation>перемена</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="432"/>
        <source>Open folder</source>
        <translation>Открыть папку</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.cpp" line="54"/>
        <source>Select folder</source>
        <translation>Выберите папку</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.cpp" line="61"/>
        <location filename="settings/qgeneralwidget.cpp" line="67"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.cpp" line="61"/>
        <source>Do not select the same folder！</source>
        <translation>Не выбирайте ту же папку папка</translation>
    </message>
    <message>
        <source>Restart Telecomm to take effect</source>
        <translation type="vanished">Перезапустите Telecomm для вступления в силу</translation>
    </message>
    <message>
        <source>  Telecomm will restart for the language  
settings to take effect</source>
        <translation type="vanished">Telecomm перезагрузится для языка
настройки вступят в силу</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.cpp" line="67"/>
        <source>Restart OpenPlanet to take effect</source>
        <translation>Перезапустите OpenPlanet для вступления в силу</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.cpp" line="99"/>
        <source>Notice</source>
        <translation>уведомление</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.cpp" line="99"/>
        <source>  OpenPlanet will restart for the language  
settings to take effect</source>
        <translation>Открытая планета перезапустится для языка
настройки вступят в силу</translation>
    </message>
</context>
<context>
    <name>QSetKeyWidget</name>
    <message>
        <location filename="settings/qsetkeywidget.ui" line="14"/>
        <source>QSetKeyWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qsetkeywidget.ui" line="36"/>
        <source>Please enter a new shortcut on the keyboard</source>
        <translation>Пожалуйста, введите новый ярлык на клавиатуре</translation>
    </message>
    <message>
        <location filename="settings/qsetkeywidget.ui" line="46"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qsetkeywidget.ui" line="83"/>
        <source>OK</source>
        <translation>Хорошо</translation>
    </message>
    <message>
        <location filename="settings/qsetkeywidget.ui" line="99"/>
        <source>Cancel</source>
        <translation>отменить</translation>
    </message>
    <message>
        <location filename="settings/qsetkeywidget.cpp" line="69"/>
        <source>No</source>
        <translation>нет</translation>
    </message>
</context>
<context>
    <name>QShortCutWidget</name>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="14"/>
        <source>QShortCutWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="106"/>
        <source>Send message</source>
        <translation>послать</translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="137"/>
        <source>Enter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="142"/>
        <source>Ctrl+Enter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="204"/>
        <source>Screen capture</source>
        <translation>Скриншот</translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="229"/>
        <source>Alt+A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="331"/>
        <source>Open hotkey</source>
        <translation>открыть</translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="356"/>
        <source>Ctrl+Alt+W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="455"/>
        <source>Detect hotkey</source>
        <translation>детектировать</translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="465"/>
        <source>Remind me when hotkey conflicts</source>
        <translation>Напомните мне, когда конфликты горячих клавиш</translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="509"/>
        <source>Restore default</source>
        <translation>Сброс настроек</translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.cpp" line="60"/>
        <location filename="settings/qshortcutwidget.cpp" line="87"/>
        <source>Type a new shortcut</source>
        <translation>Введите новый ярлык</translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.cpp" line="145"/>
        <location filename="settings/qshortcutwidget.cpp" line="149"/>
        <location filename="settings/qshortcutwidget.cpp" line="204"/>
        <location filename="settings/qshortcutwidget.cpp" line="208"/>
        <source>none</source>
        <oldsource>No</oldsource>
        <translation>никто</translation>
    </message>
</context>
<context>
    <name>QThemeSwitchWidget</name>
    <message>
        <location filename="settings/qthemeswitchwidget.ui" line="17"/>
        <source>QGeneralWidget</source>
        <translation></translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="obsolete">язык</translation>
    </message>
    <message>
        <location filename="settings/qthemeswitchwidget.ui" line="106"/>
        <source>Theme Style</source>
        <translation>тема</translation>
    </message>
    <message>
        <location filename="settings/qthemeswitchwidget.ui" line="138"/>
        <location filename="settings/qthemeswitchwidget.cpp" line="35"/>
        <location filename="settings/qthemeswitchwidget.cpp" line="37"/>
        <location filename="settings/qthemeswitchwidget.cpp" line="59"/>
        <location filename="settings/qthemeswitchwidget.cpp" line="71"/>
        <source>Light color</source>
        <translation>Свет</translation>
    </message>
    <message>
        <location filename="settings/qthemeswitchwidget.ui" line="143"/>
        <location filename="settings/qthemeswitchwidget.cpp" line="33"/>
        <location filename="settings/qthemeswitchwidget.cpp" line="57"/>
        <location filename="settings/qthemeswitchwidget.cpp" line="69"/>
        <source>Dark color</source>
        <translation>Темно</translation>
    </message>
    <message>
        <location filename="settings/qthemeswitchwidget.cpp" line="52"/>
        <source>Notice</source>
        <translation>уведомление</translation>
    </message>
    <message>
        <location filename="settings/qthemeswitchwidget.cpp" line="52"/>
        <source>The program will restart to switch theme, Y or N?</source>
        <translation>Программа перезапустится, чтобы вступить в силу, продолжается?</translation>
    </message>
</context>
<context>
    <name>ReadConfig</name>
    <message>
        <location filename="readconfig.cpp" line="18"/>
        <source>xml</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SettingsWidget</name>
    <message>
        <location filename="settings/settingswidget.ui" line="20"/>
        <location filename="settings/settingswidget.ui" line="157"/>
        <source>Setting</source>
        <translation>настройка</translation>
    </message>
    <message>
        <location filename="settings/settingswidget.cpp" line="45"/>
        <source>Account settings</source>
        <translation>счета</translation>
    </message>
    <message>
        <location filename="settings/settingswidget.cpp" line="46"/>
        <source>General settings</source>
        <translation>общий</translation>
    </message>
    <message>
        <location filename="settings/settingswidget.cpp" line="47"/>
        <source>Shortcut button</source>
        <translation>кратчайший путь</translation>
    </message>
    <message>
        <location filename="settings/settingswidget.cpp" line="48"/>
        <source>Theme swithing</source>
        <translation>Переключение тем</translation>
    </message>
    <message>
        <location filename="settings/settingswidget.cpp" line="49"/>
        <source>About</source>
        <translation>Около</translation>
    </message>
</context>
<context>
    <name>UserInfoWidget</name>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="31"/>
        <location filename="userprofile/userinfowidget.ui" line="141"/>
        <source>Profile Edit</source>
        <translation>Редактировать профиль</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="234"/>
        <source>Nickname</source>
        <translation>кличка</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="279"/>
        <source>Signature</source>
        <translation>Подпись</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="349"/>
        <source>Gender</source>
        <translation>Пол</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="363"/>
        <location filename="userprofile/userinfowidget.cpp" line="109"/>
        <location filename="userprofile/userinfowidget.cpp" line="122"/>
        <source>Male</source>
        <translation>мужчина</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="368"/>
        <location filename="userprofile/userinfowidget.cpp" line="111"/>
        <location filename="userprofile/userinfowidget.cpp" line="124"/>
        <source>Female</source>
        <translation>женский</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="424"/>
        <source>Phone Number</source>
        <translation>Номер телефона</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="469"/>
        <source>E-mail</source>
        <translation>Эл. почта</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="519"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="532"/>
        <source>Close</source>
        <translation>близко</translation>
    </message>
</context>
<context>
    <name>UserProfileWidget</name>
    <message>
        <location filename="userprofile/userprofilewidget.ui" line="14"/>
        <source>UserProfileWidget</source>
        <translation>Профиль пользователя</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.ui" line="169"/>
        <source>Account</source>
        <translation>учетная запись</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.ui" line="217"/>
        <source>Nickname</source>
        <translation>кличка</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.ui" line="273"/>
        <source>ID</source>
        <translation></translation>
    </message>
    <message>
        <source>InterPlanet ID</source>
        <translation type="vanished">ИнтерПланет ID</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.ui" line="323"/>
        <source>E-mail</source>
        <translation>Эл. почта</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.ui" line="377"/>
        <source>QR code card</source>
        <translation>QR код карты</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.ui" line="430"/>
        <source>Receive stranger news</source>
        <translation>Получать незнакомые новости</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.cpp" line="18"/>
        <source>personal information</source>
        <translation>Персональные данные</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.cpp" line="27"/>
        <source>close</source>
        <translation>близко</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.cpp" line="33"/>
        <source>Modify avatar</source>
        <translation>Изменить аватар</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.cpp" line="40"/>
        <source>Modify information</source>
        <translation>Изменить информацию</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.cpp" line="184"/>
        <source>Scan the QR code on the top and add me as a friend.</source>
        <translation>Отсканируйте QR-код сверху и добавьте меня в друзья.</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.cpp" line="185"/>
        <source>ID:</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>xSysTrayMeauAction</name>
    <message>
        <source>About</source>
        <translation type="vanished">около</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">уволиться</translation>
    </message>
</context>
</TS>
