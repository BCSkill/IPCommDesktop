﻿#include "FunctionWidget.h"
#include "ui_FunctionWidget.h"
#include "QStringLiteralBak.h"
#include <QDesktopWidget>

FunctionWidget::FunctionWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::FunctionWidget();
	ui->setupUi(this);

	QFile styleFile(":/QSS/Resources/QSS/OPMainWidgetShareLib/FunctionWidget.qss");
	styleFile.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(styleFile.readAll());
	setStyleSheet(styleSheet);
	styleFile.close();

	this->setWindowFlags(Qt::Tool | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
	this->setAttribute(Qt::WA_TranslucentBackground);
	this->setAttribute(Qt::WA_DeleteOnClose);
	
	QPixmap *pic = new QPixmap(":/functionDisplay/Resources/functionDisplay/page1_01.png");
	pic->scaled(ui->label_2->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
	ui->label_2->setPixmap(*pic);
	ui->label_2->setScaledContents(true);

	pic->load(":/functionDisplay/Resources/functionDisplay/page1_02.png");
	pic->scaled(ui->label_3->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
	ui->label_3->setPixmap(*pic);
	ui->label_3->setScaledContents(true);
	
	connect(ui->mPButtonClose, SIGNAL(clicked()), this, SLOT(slot_close()));
	connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(slot_firstPage()));
	connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(slot_secondPage()));
	connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(slot_thirdPage()));
	connect(ui->pushButton_4, SIGNAL(clicked()), this, SLOT(slot_fourthPage()));
	connect(ui->pushButton_5, SIGNAL(clicked()), this, SLOT(slot_fifthPage()));
#ifdef Q_OS_LINUX
setLinuxCenter();
#endif
}
void FunctionWidget::slot_firstPage() {
	QPixmap *pic = new QPixmap(":/functionDisplay/Resources/functionDisplay/page1_01.png");
	pic->scaled(ui->label_2->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
	ui->label_2->setPixmap(*pic);
	ui->label_2->setScaledContents(true);

	pic->load(":/functionDisplay/Resources/functionDisplay/page1_02.png");
	pic->scaled(ui->label_3->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
	ui->label_3->setPixmap(*pic);
	ui->label_3->setScaledContents(true);

	ui->label_6->setText(tr("Full chain"));
}
void FunctionWidget::slot_secondPage() {
	QPixmap *pic = new QPixmap(":/functionDisplay/Resources/functionDisplay/page2_01.png");
	pic->scaled(ui->label_2->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
	ui->label_2->setPixmap(*pic);
	ui->label_2->setScaledContents(true);

	pic->load(":/functionDisplay/Resources/functionDisplay/page2_02.png");
	pic->scaled(ui->label_3->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
	ui->label_3->setPixmap(*pic);
	ui->label_3->setScaledContents(true);

	ui->label_6->setText(tr("Full platform"));
}
void FunctionWidget::slot_thirdPage() {
	QPixmap *pic = new QPixmap(":/functionDisplay/Resources/functionDisplay/page3_01.png");
	pic->scaled(ui->label_2->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
	ui->label_2->setPixmap(*pic);
	ui->label_2->setScaledContents(true);

	pic->load(":/functionDisplay/Resources/functionDisplay/page3_02.png");
	pic->scaled(ui->label_3->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
	ui->label_3->setPixmap(*pic);
	ui->label_3->setScaledContents(true);

	ui->label_6->setText(tr("Mining"));
}
void FunctionWidget::slot_fourthPage() {
	QPixmap *pic = new QPixmap(":/functionDisplay/Resources/functionDisplay/page4_01.png");
	pic->scaled(ui->label_2->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
	ui->label_2->setPixmap(*pic);
	ui->label_2->setScaledContents(true);

	pic->load(":/functionDisplay/Resources/functionDisplay/page4_02.png");
	pic->scaled(ui->label_3->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
	ui->label_3->setPixmap(*pic);
	ui->label_3->setScaledContents(true);

	ui->label_6->setText(tr("Encrypted communication"));
}
void FunctionWidget::slot_fifthPage() {
	QPixmap *pic = new QPixmap(":/functionDisplay/Resources/functionDisplay/page5_01.png");
	pic->scaled(ui->label_2->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
	ui->label_2->setPixmap(*pic);
	ui->label_2->setScaledContents(true);

	pic->load(":/functionDisplay/Resources/functionDisplay/page5_02.png");
	pic->scaled(ui->label_3->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
	ui->label_3->setPixmap(*pic);
	ui->label_3->setScaledContents(true);

	ui->label_6->setText(tr("Digital assets"));
}
void FunctionWidget::slot_close() {
	this->hide();
}
FunctionWidget::~FunctionWidget()
{
	delete ui;
}
void FunctionWidget::paintEvent(QPaintEvent * event) {
	QPainterPath path;
	path.setFillRule(Qt::WindingFill);
	path.addRect(6, 6, this->width() - 12, this->height() - 12);

	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.fillPath(path, QBrush(Qt::white));

	QColor color(0, 0, 0, 50);//, 50);
	for (int i = 0; i < 6; i++)
	{
		QPainterPath path;
		path.setFillRule(Qt::WindingFill);
		path.addRect(6 - i, 6 - i, this->width() - (6 - i) * 2, this->height() - (6 - i) * 2);
		color.setAlpha(110 - qSqrt(i) * 40);
		painter.setPen(color);
		painter.drawPath(path);
	}
}
void FunctionWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		mMoveing = true;
		mMovePosition = event->globalPos() - pos();
	}
	return QWidget::mousePressEvent(event);
}

void FunctionWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mMoveing && (event->buttons() && Qt::LeftButton)
		&& (event->globalPos() - mMovePosition).manhattanLength() > QApplication::startDragDistance())
	{
		move(event->globalPos() - mMovePosition);
		mMovePosition = event->globalPos() - pos();
	}
	return QWidget::mouseMoveEvent(event);
}

void FunctionWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		mMoveing = false;
	}
}

void FunctionWidget::setLinuxCenter()
{
    window()->setGeometry
    (
            QStyle::alignedRect
            (
                    Qt::LeftToRight,
                    Qt::AlignCenter,
                    window()->size(),
                    qApp->desktop()->availableGeometry()
            )
    );
}
