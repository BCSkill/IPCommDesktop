﻿#include "qshortcutwidget.h"
#include "ui_qshortcutwidget.h"
#include "qsetkeywidget.h"
#include <QStyledItemDelegate>
#include "QStringLiteralBak.h"

//#include "CommonHelper.h"

QShortCutWidget::QShortCutWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::QShortCutWidget();
	ui->setupUi(this);

	QFile styleFile(":/QSS/Resources/QSS/OPMainWidgetShareLib/qshortcutwidget.qss");
	styleFile.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(styleFile.readAll());
	setStyleSheet(styleSheet);
	styleFile.close();

	connect(ui->screenCutBtn, SIGNAL(clicked()), this, SLOT(SLO_getShortCut()));
	connect(ui->openBtn, SIGNAL(clicked()), this, SLOT(SLO_getOpen()));
	connect(ui->setDefaultBtn, SIGNAL(clicked()), this, SLOT(SLO_setToDefault()));
	connect(ui->checkBox, SIGNAL(stateChanged(int)), this, SLOT(SLO_onStateChanged(int)));
	connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(SLO_onIndexChanged(int)));

	ui->setDefaultBtn->hide();
	QStyledItemDelegate* itemDelegate = new QStyledItemDelegate();
	ui->comboBox->setItemDelegate(itemDelegate);
	iBtnFlag = 0;//0:未点击 1:截屏 2:打开
	ui->cutNotelabel->setText("");
	ui->opNoteLabel->setText("");
 	ui->screenCutBtn->installEventFilter(this);
 	ui->openBtn->installEventFilter(this);
	this->installEventFilter(this);
#ifndef Q_OS_WIN
    ui->openBtn->setEnabled(false);
    ui->checkBox->setEnabled(false);
#endif
}

QShortCutWidget::~QShortCutWidget()
{
	delete ui;
}

void QShortCutWidget::SLO_getShortCut()
{   //创建用户自定义截图快捷键界面
// 	QSetKeyWidget *m_keyWidget = new QSetKeyWidget(this);
// 	m_keyWidget->setDistinct(0);
// 	m_keyWidget->setShortCut(m_strScreenCut);
// 	connect(m_keyWidget, SIGNAL(SIG_sendKey_1(QString)), this, SLOT(SLO_getShortCutKey(QString)));
// 	connect(this, SIGNAL(SIG_val(int)), m_keyWidget, SLOT(SLO_getVal(int)));
// 	emit(SIG_val(0));
// 	m_keyWidget->show();
	iBtnFlag = 1;
	ui->screenCutBtn->setStyleSheet("border: 1px solid #6a82a5;border-radius:4px;font: 75 16pt 微软雅黑;"\
		"font-size:15px;font-weight:100;color:white; background-color:#0066cb");
	ui->screenCutBtn->setFocus();
	ui->cutNotelabel->setText(tr("Type a new shortcut"));
	sigScreenCut("");
}

void QShortCutWidget::SLO_getShortCutKey(QString s)
{
    //得到用户自定义截图快捷键，并写入文件
	ui->screenCutBtn->setText(s);
	sigScreenCut(s);
	m_strScreenCut = s;
	//s = QStringLiteral(s);
	//CommonHelper::writeKeys(s, "keys.txt", 0);
}

void QShortCutWidget::SLO_getOpen()
{
// 	QSetKeyWidget *m_keyWidget = new QSetKeyWidget(this);
// 	m_keyWidget->setDistinct(1);
// 	m_keyWidget->setShortCut(m_strQuickOpen);
// 	connect(m_keyWidget, SIGNAL(SIG_sendKey_2(QString)), this, SLOT(SLO_getOpenKey(QString)));
// 	connect(this, SIGNAL(SIG_val(int)), m_keyWidget, SLOT(SLO_getVal(int)));
// 	emit(SIG_val(1));
// 	m_keyWidget->show();
	iBtnFlag = 2;
	ui->openBtn->setStyleSheet("border: 1px solid #6a82a5;border-radius:4px;font: 75 16pt 微软雅黑;"\
		"font-size:15px;font-weight:100;color:white; background-color:#0066cb");
	ui->openBtn->setFocus();
	ui->opNoteLabel->setText(tr("Type a new shortcut"));
	sigQuickOpen("");
}

void QShortCutWidget::SLO_getOpenKey(QString s)
{
	ui->openBtn->setText(s);
	sigQuickOpen(s);
	m_strQuickOpen = s;
}

void QShortCutWidget::SLO_setToDefault()
{//恢复默认设置,设置界面标签值，重写keys.txt

}

void QShortCutWidget::SLO_onStateChanged(int i)
{
    //复选框事件
	sigCheckKey(i);
}

void QShortCutWidget::SLO_onIndexChanged(int i)
{
    //下拉框事件
	m_iSendMsg = i;
	sigSendMsg(i);
}

void QShortCutWidget::setSendMsg(int iValue)
{
	ui->comboBox->setCurrentIndex(iValue);
	m_iSendMsg = iValue;
}

void QShortCutWidget::setScreenCut(QString strValue)
{
	ui->screenCutBtn->setText(strValue);
	m_strScreenCut = strValue;
}

void QShortCutWidget::setQuickOpen(QString strValue)
{
	ui->openBtn->setText(strValue);
	m_strQuickOpen = strValue;
}

void QShortCutWidget::setCheckKey(int bValue)
{
	bool bV = bValue > 0 ? true : false;
	ui->checkBox->setChecked(bV);
	m_bCheckKey= bValue;
}

void QShortCutWidget::keyPressEvent(QKeyEvent *e) {
 	if (e->key() == Qt::Key_Escape) {
		if (iBtnFlag == 1)
		{
			ui->screenCutBtn->setText(tr("none"));
		}
		else if (iBtnFlag == 2)
		{
			ui->openBtn->setText(tr("none"));
		}
		return;
 	}
	if (e->modifiers() != NULL&&e->key() == Qt::Key_Space)
	{
// 		if (iBtnFlag == 1)
// 		{
// 			ui->screenCutBtn->setText("SpaceBar");
// 		}
// 		else if(iBtnFlag == 2)
// 		{
// 			ui->openBtn->setText("SpaceBar");
// 		}
// 		return;
	}
	if (e->key() == Qt::Key_Space)
	{
// 		if (iBtnFlag == 1)
// 		{
// 			ui->screenCutBtn->setText("SpaceBar");
// 		}
// 		else if (iBtnFlag == 2)
// 		{
// 			ui->openBtn->setText("SpaceBar");
// 		}
// 		return;
	}
	if (e->key() == Qt::Key_Control || e->key() == Qt::Key_Shift || e->key() == Qt::Key_Alt || e->key() == Qt::Key_Meta)
	{
		m_bpressOrRelease = false;
		e->ignore();
		return;
	}
	if (e->key() == Qt::Key_Enter|| e->key() == Qt::Key_Return)
	{
		saveKey();
		return;
	}
	if (iBtnFlag == 1)
	{
		ui->screenCutBtn->setText(QKeySequence(e->modifiers() + e->key()).toString());
		m_bpressOrRelease = true;
	}
	else if (iBtnFlag == 2)
	{
		ui->openBtn->setText(QKeySequence(e->modifiers() + e->key()).toString());
		m_bpressOrRelease = true;
	}
}
void QShortCutWidget::keyReleaseEvent(QKeyEvent *e) {
	if (!m_bpressOrRelease) {
		if (e->key() == Qt::Key_Control || e->key() == Qt::Key_Shift || e->key() == Qt::Key_Alt || e->key() == Qt::Key_Meta) {
			if (iBtnFlag == 1)
			{
				ui->screenCutBtn->setText(tr("none"));
			}
			else if (iBtnFlag == 2)
			{
				ui->openBtn->setText(tr("none"));
			}
			return;
		}
	}
	return;
}

bool QShortCutWidget::eventFilter(QObject *obj, QEvent *event)
{
	if ((obj == ui->openBtn|| obj == ui->screenCutBtn)&&event->type() == QEvent::FocusOut)
	{
		saveKey();
	}
	if ((obj == ui->openBtn || obj == ui->screenCutBtn) && event->type() == QEvent::KeyPress)
	{
		QKeyEvent *e = static_cast<QKeyEvent*>(event);
		if (e->key() == Qt::Key_Space)
		{
			keyPressEvent(e);
			return false;
		}
	}
	if (obj == this&&event->type() == QEvent::MouseButtonPress)
	{
		saveKey();
	}
	return QWidget::eventFilter(obj, event);
}

void QShortCutWidget::saveKey()
{
	iBtnFlag = 0;
	ui->screenCutBtn->setStyleSheet("border: 1px solid #6a82a5;border-radius:4px;font: 75 16pt 微软雅黑;"\
		"font-size:15px;font-weight:100;color:#0066cb;");
	ui->openBtn->setStyleSheet("border: 1px solid #6a82a5;border-radius:4px;font: 75 16pt 微软雅黑;"\
		"font-size:15px;font-weight:100;color:#0066cb;");
	sigQuickOpen(ui->openBtn->text());
	sigScreenCut(ui->screenCutBtn->text());
	ui->cutNotelabel->setText("");
	ui->opNoteLabel->setText("");
}
