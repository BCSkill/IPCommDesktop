#include "qthemeswitchwidget.h"
#include "ui_qthemeswitchwidget.h"
#include "QStringLiteralBak.h"
#include "qrencodesharelib.h"
#include <QDesktopServices>
#include <QUrl>
#include <qsettings.h>
#include "stdafx.h"
#include "immainwidget.h"
#include <messagebox.h>
#include <qdebug.h>
#include "settingsmanager.h"


extern SettingsManager* gSettingsManager;

QThemeSwitchWidget::QThemeSwitchWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::QThemeSwitchWidget();
	ui->setupUi(this);

	QFile styleFile(":/QSS/Resources/QSS/OPMainWidgetShareLib/qthemeswitchwidget.qss");
	styleFile.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(styleFile.readAll());
	setStyleSheet(styleSheet);
	styleFile.close();


	QSettings settings("InterPlanet", "Qt");
	QString strTheme = settings.value("Theme").toString();
	if (strTheme == "Blue")
        ui->LanguageCombo->setCurrentText(tr("Light color"));
	else if (strTheme == "White")
		ui->LanguageCombo->setCurrentText(tr("Light color"));
	else
		ui->LanguageCombo->setCurrentText(tr("Light color"));

	connect(ui->LanguageCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(slotIndexSwitch(int)));
	connect(this, SIGNAL(sigThemeSwitch(QString)), gSettingsManager, SIGNAL(sigThemeSwitch(QString)));
}

QThemeSwitchWidget::~QThemeSwitchWidget()
{
	delete ui;
}
void QThemeSwitchWidget::slotIndexSwitch(int index)
{
	IMessageBox *tip = new IMessageBox();
	connect(tip, SIGNAL(sigOK()), this, SLOT(slotThemeChanged()));
	connect(tip, SIGNAL(sigClose()), this, SLOT(slotThemeCanceled()));
	tip->initAsk(tr("Notice"), tr("The program will restart to switch theme, Y or N?"));
}
void QThemeSwitchWidget::slotThemeChanged()
{
	QString strTheme = ui->LanguageCombo->currentText();
	if(strTheme == tr("Dark color"))
		emit sigThemeSwitch("Blue");
	if(strTheme == tr("Light color"))
		emit sigThemeSwitch("White");
}
void QThemeSwitchWidget::slotThemeCanceled()
{
	disconnect(ui->LanguageCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(slotIndexSwitch(int)));

	QSettings settings("InterPlanet", "Qt");
	QString strTheme = settings.value("Theme").toString();
	if(strTheme == "Blue")
		ui->LanguageCombo->setCurrentText(tr("Dark color"));
	else if(strTheme == "White")
		ui->LanguageCombo->setCurrentText(tr("Light color"));

	connect(ui->LanguageCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(slotIndexSwitch(int)));
}
