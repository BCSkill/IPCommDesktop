﻿#include "qsetkeywidget.h"
#include "ui_qsetkeywidget.h"
//#include "CommonHelper.h"
#include <qkeysequence.h>
#include <QDebug>
#include <QMouseEvent>
#include "QStringLiteralBak.h"
#include "shadow.h"

QSetKeyWidget::QSetKeyWidget(QWidget *parent)
	: QWidget(parent)
{
	//this->grabKeyboard();
	ui = new Ui::QSetKeyWidget();
	ui->setupUi(this);

	QFile styleFile(":/QSS/Resources/QSS/OPMainWidgetShareLib/qsetkeywidget.qss");
	styleFile.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(styleFile.readAll());
	setStyleSheet(styleSheet);
	styleFile.close();

#ifdef Q_OS_WIN
	shadow = new Shadow();
#endif
	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog);
	setWindowModality(Qt::WindowModal);
	//this->setAttribute(Qt::WA_TranslucentBackground);

	connect(ui->confirmBtn, SIGNAL(clicked()), this, SLOT(SLO_confirm()));
	connect(ui->cancelBtn, SIGNAL(clicked()), this, SLOT(SLO_cancel()));
	pressOrRelease = true;
	this->setFocusPolicy(Qt::NoFocus);
	ui->confirmBtn->setFocusPolicy(Qt::NoFocus);
	ui->cancelBtn->setFocusPolicy(Qt::NoFocus);
}

QSetKeyWidget::~QSetKeyWidget()
{
#ifdef Q_OS_WIN
	if (shadow)
		delete shadow;
#endif
	delete ui;
}
void QSetKeyWidget::keyPressEvent(QKeyEvent *e) {
	if (e->key() == Qt::Key_Escape) {
		this->close();
	}
	if (e->modifiers() != NULL&&e->key() == Qt::Key_Space) {
		//ui->label->setText("SpaceBar");
		return;
	}
	if (e->key() == Qt::Key_Space) {
		ui->label->setText("SpaceBar");
		return;
	}
	if (e->key() == Qt::Key_Control || e->key() == Qt::Key_Shift || e->key() == Qt::Key_Alt || e->key() == Qt::Key_Meta) {
		pressOrRelease = false;
		e->ignore();
		return;
	}
	ui->label->setText(QKeySequence(e->modifiers() + e->key()).toString());
	pressOrRelease = true;
}
void QSetKeyWidget::keyReleaseEvent(QKeyEvent *e) {
	if (!pressOrRelease) {
		if (e->key() == Qt::Key_Control || e->key() == Qt::Key_Shift || e->key() == Qt::Key_Alt || e->key() == Qt::Key_Meta) {
			ui->label->setText(tr("No"));
			return;
		}
	}
}
void QSetKeyWidget::SLO_confirm(){
	if (distinct == 0){
		emit SIG_sendKey_1(ui->label->text());
	}
	else{
		emit SIG_sendKey_2(ui->label->text());
	}
	this->close();
}
void QSetKeyWidget::SLO_cancel(){
	this->close();
}
//鼠标事件的处理。
void QSetKeyWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
							//dateSelector->hide();   //隐藏日期选择器。
	return QWidget::mousePressEvent(event);
}
void QSetKeyWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void QSetKeyWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);

	return QWidget::mouseMoveEvent(event);
}

void QSetKeyWidget::setDistinct(int iValue)
{
	distinct = iValue;
}

void QSetKeyWidget::setShortCut(QString strValue)
{
	ui->label->setText(strValue);
}
void QSetKeyWidget::changeEvent(QEvent * event)
{
	if (event->type() == QEvent::WindowStateChange)
	{
#ifdef Q_OS_WIN
		if (this->windowState() == Qt::WindowMaximized)
			shadow->hide();
		if (this->windowState() == Qt::WindowMinimized)
			shadow->hide();

		if (this->windowState() == Qt::WindowNoState)
			shadow->show();
#endif
	}

	QWidget::changeEvent(event);
}


void QSetKeyWidget::moveEvent(QMoveEvent *event)
{
#ifdef Q_OS_WIN
	shadow->move(event->pos().x() - 10, event->pos().y() - 10);
#endif
}

void QSetKeyWidget::resizeEvent(QResizeEvent* event)
{
#ifdef Q_OS_WIN
	shadow->resize(event->size().width() + 20, event->size().height() + 20);
#endif
}

void QSetKeyWidget::show()
{
	QWidget::show();
#ifdef Q_OS_WIN
	shadow->resize(width() + 20, height() + 20);
	shadow->move(x() - 10, y() - 10);
	shadow->show();
#endif
#ifdef Q_OS_WIN
	// 窗口被激活了，强制重绘  
	InvalidateRect((HWND)this->winId(), nullptr, FALSE);
#endif
}