﻿#include "qaboutwidget.h"
#include "ui_qaboutwidget.h"
#include "QStringLiteralBak.h"
#include "qrencodesharelib.h"
#include <QDesktopServices>
#include <QUrl>
#include "stdafx.h"
#include "immainwidget.h"

QAboutWidget::QAboutWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::QAboutWidget();
	ui->setupUi(this);

	QFile styleFile(":/QSS/Resources/QSS/OPMainWidgetShareLib/qaboutwidget.qss");
	styleFile.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(styleFile.readAll());
	setStyleSheet(styleSheet);
	styleFile.close();

	//生成填充二维码
	QRenCodeShareLib qr;
	QImage img = qr.GenerateQRcode("https://ipcom.io/");
	QPixmap pixTmp;
	pixTmp = QPixmap::fromImage(img);
	pixTmp = pixTmp.scaled(QSize(156, 156), Qt::KeepAspectRatio);
	ui->RenCodeLabel->setPixmap(pixTmp);
	//link
	connect(ui->LinkBtn, SIGNAL(clicked(bool)), this, SLOT(slotOpenLink()));
    if(IMMainWidget::self())
    {
       connect(ui->updateBtn, SIGNAL(clicked(bool)), IMMainWidget::self(), SIGNAL(sigCheckUpdate()));
    }
	//版本号
 	AppConfig appConf = gDataManager->getAppConfigInfo();
 	ui->verLabel->setText(tr("Current Version:")+appConf.appVersion.versionID);
	//this->show();
	ui->label_3->hide();
}

QAboutWidget::~QAboutWidget()
{
	delete ui;
}

void QAboutWidget::slotOpenLink()
{
   // QDesktopServices::openUrl(QUrl("https://telecomm.io/"));
    QDesktopServices::openUrl(QUrl::fromLocalFile("https://ipcom.io"));
}
