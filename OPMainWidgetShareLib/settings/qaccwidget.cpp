﻿#include "qaccwidget.h"
#include "ui_qaccwidget.h"
#include <QFile>
#include "QStringLiteralBak.h"

QAccWidget::QAccWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::QAccWidget();
	ui->setupUi(this);

	QFile styleFile(":/QSS/Resources/QSS/OPMainWidgetShareLib/qaccwidget.qss");
	styleFile.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(styleFile.readAll());
	setStyleSheet(styleSheet);
	styleFile.close();

	connect(ui->pushButton, SIGNAL(clicked()), this, SIGNAL(sigquitLogin()));
// 	QFile file(":/qssWidget/Resources/qssWidget/accwidget.qss");
// 	file.open(QFile::ReadOnly);
// 	QString styleSheet = QLatin1String(file.readAll());
// 	setStyleSheet(styleSheet);
// 	file.close();
}

QAccWidget::~QAccWidget()
{
	delete ui;
}
void QAccWidget::setAvatar(QString filePath){//设置头像

	QByteArray bytearray;
	QFile file(filePath);
	if (file.open(QIODevice::ReadOnly) && file.size() != 0)
	{
		bytearray = file.readAll();
	}
	file.close();
	QPixmap pix;
	if (!pix.loadFromData(bytearray) || bytearray == "")
	{
		pix.load(":/PerChat/Resources/person/temp.png");
	}
	ui->avatar->setAutoFillBackground(true);
	//		circleHeaderImage(pix);
	ui->avatar->setPixmap(pix);
	ui->avatar->setScaledContents(true);
	//mHeadImage->setObjectName(strPath);
}
void QAccWidget::setNickname(QString myNickname){//设置昵称
	
	ui->nickname->setText(myNickname);
	//ui->nickname->setContentsMargins(20, 10, 20, 30);
}
void QAccWidget::setId(QString myId){//设置用户ID
	QString fullId = tr("ID：") + myId;
	ui->id->setText(fullId);
}
//void QAccWidget::setBtnStyle(){//设置按钮样式，记得调用	
//	ui->pushButton->setStyleSheet("QPushButton:hover{background-color:#DBDBDB;}QPushButton{background-color:white;border-style:solid;border-color:#BFBFBF;border-width:1px;}");
//}