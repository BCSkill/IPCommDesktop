﻿#include "qgeneralwidget.h"
#include "ui_qgeneralwidget.h"
#include <qfiledialog.h>
#include <qprocess.h>
#include <qmessagebox.h>
#include <QListWidget>
#include "NoFocusFrameDelegate.h"
#include "QStringLiteralBak.h"
#include <QDesktopServices>
#include <messagebox.h>
#include <QStyledItemDelegate>
QGeneralWidget::QGeneralWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::QGeneralWidget();
	ui->setupUi(this);

	QFile styleFile(":/QSS/Resources/QSS/OPMainWidgetShareLib/qgeneralwidget.qss");
	styleFile.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(styleFile.readAll());
	setStyleSheet(styleSheet);
	styleFile.close();

	ui->LanguageCombo->setItemDelegate(new QStyledItemDelegate());
	ui->LanguageCombo->setItemDelegate(new NoFocusFrameDelegate(this));
	connect(ui->openFile, SIGNAL(clicked()), this, SLOT(SLO_openFile()));
	connect(ui->clear, SIGNAL(clicked()), this, SLOT(SLO_clear()));
	connect(ui->changePath, SIGNAL(clicked()), this, SLOT(SLO_changePath()));
	connect(ui->LanguageCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(slotLanguage(int)));
	QStyledItemDelegate* itemDelegate = new QStyledItemDelegate();
	ui->fileLocation->setCursorPosition(0);
	ui->fileLocation->setFocusPolicy(Qt::NoFocus);

#ifndef Q_OS_WIN
    ui->changePath->setEnabled(false);
#endif
}
QGeneralWidget::~QGeneralWidget()
{
	delete ui;
}
void QGeneralWidget::SLO_openFile(){//根据当前路径打开文件夹，目前仅有前端样式，后台待完善
// 	QString fileName;
// 	QProcess process;
// 	fileName = ui->fileLocation->text();
// // #ifdef WIN32
// // 	fileName.replace("/", "\\");//这句windows下必要
// // #endif
// 	process.startDetached("explorer /select," + fileName);
    QString strPath = ui->fileLocation->text();
    QDesktopServices::openUrl(QUrl::fromLocalFile(strPath));
}
void QGeneralWidget::SLO_changePath(){//改变文件存储路径，目前仅有前端样式，后台待完善
	QString dir = QFileDialog::getExistingDirectory(this, tr("Select folder"), "/home", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
	if (dir == ""){
		return;
	}
	dir += "/OpenPlanet";
	if (dir.indexOf(m_strPath) > -1)
	{
		IMessageBox::tip(this, tr("Warning"), tr("Do not select the same folder！"));
		return;
	}
	ui->fileLocation->setText(dir);
	ui->fileLocation->setCursorPosition(0);
	IMessageBox* pBox = new IMessageBox();
	pBox->initAsk(tr("Warning"), tr("Restart OpenPlanet to take effect"));
	connect(pBox, SIGNAL(sigOK()), SLOT(slotChangePath())); 
	connect(pBox, SIGNAL(sigClose()), SLOT(slotRevert()));
}
void QGeneralWidget::SLO_clear(){//清空聊天记录，后台待完善
	emit sigClearPath();
}
void QGeneralWidget::setClearText(QString strValue){
	ui->clear->setText(strValue);
}
void QGeneralWidget::setUserPath(QString strValue) {
	ui->fileLocation->setText(strValue);
	m_strPath = strValue;
	ui->fileLocation->setCursorPosition(0);
}
void QGeneralWidget::slotChangePath()
{
	this->close();
	m_strPath = ui->fileLocation->text();
	emit sigChangePath(m_strPath);
}

void QGeneralWidget::slotRevert()
{
	ui->fileLocation->setText(m_strPath);
}

void QGeneralWidget::slotLanguage(int index)
{
	IMessageBox *tip = new IMessageBox();
	connect(tip, SIGNAL(sigOK()), this, SLOT(slotSendLang()));
	connect(tip, SIGNAL(sigClose()), this, SLOT(slotCancelLang()));
	tip->initAsk(tr("Notice"), tr("  OpenPlanet will restart for the language  \nsettings to take effect"));
}

void QGeneralWidget::slotSendLang()
{
	QString strLan = ui->LanguageCombo->currentText();
	emit sigLanguage(strLan);
}

void QGeneralWidget::slotCancelLang()
{
	if (!this->currentLanguage.isEmpty())
		setLanguage(this->currentLanguage);
}

void QGeneralWidget::setLanguage(QString strValue)
{
	disconnect(ui->LanguageCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(slotLanguage(int)));
	
	int index = ui->LanguageCombo->findText(strValue);
	if (index < 0)
		index = 0;
	ui->LanguageCombo->setCurrentIndex(index);
	this->currentLanguage = ui->LanguageCombo->currentText();

	connect(ui->LanguageCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(slotLanguage(int)));
}