﻿#include "settingswidget.h"
#include "ui_settingswidget.h"
#include "shadow.h"
#include <QMouseEvent>
#include "qaccwidget.h"
#include "common.h"
#include "QStringLiteralBak.h"
#include "messagebox.h"
#include <QDebug>
#include <QPushButton>

extern QString gThemeStyle;

SettingsWidget::SettingsWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::SettingsWidget();
	ui->setupUi(this);

	QFile styleFile(":/QSS/Resources/QSS/OPMainWidgetShareLib/settingswidget.qss");
	styleFile.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(styleFile.readAll());
	setStyleSheet(styleSheet);
	styleFile.close();

	this->setAttribute(Qt::WA_DeleteOnClose);
	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog);
	setWindowIcon(QIcon(":/Login/Resources/login/system.ico"));

#ifdef Q_OS_WIN
	shadow = new Shadow();
#endif
	connect(ui->minBtn, SIGNAL(clicked()), this, SLOT(showMinimized()));
	connect(ui->closeBtn, SIGNAL(clicked()), this, SLOT(close()));
	connect(ui->accWidget, SIGNAL(sigquitLogin()), this, SIGNAL(sigCancel()));
	connect(ui->shortCutWidget, SIGNAL(sigScreenCut(QString)), this, SIGNAL(sigScreenCut(QString)));
	connect(ui->shortCutWidget, SIGNAL(sigQuickOpen(QString)), this, SIGNAL(sigQuickOpen(QString)));
	connect(ui->shortCutWidget, SIGNAL(sigSendMsg(int)), this, SIGNAL(sigSendMsg(int)));
	connect(ui->shortCutWidget, SIGNAL(sigCheckKey(int)), this, SIGNAL(sigCheckKey(int)));
	connect(ui->generalCutWidget, SIGNAL(sigChangePath(QString)), this, SIGNAL(sigChangePath(QString)));
	connect(ui->generalCutWidget, SIGNAL(sigClearPath()), this, SIGNAL(sigClearPath()));
	connect(ui->generalCutWidget, SIGNAL(sigLanguage(QString)), this, SIGNAL(sigLanguage(QString)));


	ui->pushButton_1->setText(tr("Account settings"));
	ui->pushButton_2->setText(tr("General settings"));
	ui->pushButton_3->setText(tr("Shortcut button"));
	ui->pushButton_5->setText(tr("Theme swithing"));
	ui->pushButton_4->setText(tr("About"));
	if (gThemeStyle == "Blue")
		ui->pushButton_1->setStyleSheet("QPushButton{background-color:#042439;border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:rgb(16, 142, 233);}");
	if (gThemeStyle == "White")
		ui->pushButton_1->setStyleSheet("QPushButton{background-color:#eceae8;border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:rgb(16, 142, 233);}");
	ui->pushButton_2->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");
	ui->pushButton_3->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");
	ui->pushButton_4->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");
	ui->pushButton_5->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");

	ui->stackedWidget->setCurrentIndex(0);

	Label1 = new QPushButton(this);
	Label1->resize(128, 40);
	Label1->move(0, 40);
	Label1->setStyleSheet("background-color:rgba(0,0,0,0)");
	Label1->setCursor(Qt::PointingHandCursor);

	Label2 = new QPushButton(this);
	Label2->resize(128, 40);
	Label2->move(0, 80);
	Label2->setStyleSheet("background-color:rgba(0,0,0,0)");
	Label2->setCursor(Qt::PointingHandCursor);

	Label3 = new QPushButton(this);
	Label3->resize(128, 40);
	Label3->move(0, 120);
	Label3->setStyleSheet("background-color:rgba(0,0,0,0)");
	Label3->setCursor(Qt::PointingHandCursor);

	Label5 = new QPushButton(this);
	Label5->resize(128, 40);
	Label5->move(0, 160);
	Label5->setStyleSheet("background-color:rgba(0,0,0,0)");
	Label5->setCursor(Qt::PointingHandCursor);

	Label4 = new QPushButton(this);
	Label4->resize(128, 40);
	Label4->move(0, 200);
	Label4->setStyleSheet("background-color:rgba(0,0,0,0)");
	Label4->setCursor(Qt::PointingHandCursor);

	connect(Label1, SIGNAL(clicked()), this, SLOT(slotAccount()));
	connect(Label2, SIGNAL(clicked()), this, SLOT(slotCurrency()));
	connect(Label3, SIGNAL(clicked()), this, SLOT(slotKey()));
	connect(Label4, SIGNAL(clicked()), this, SLOT(slotAbout()));
	connect(Label5, SIGNAL(clicked()), this, SLOT(slotThemeSwitch()));

}

SettingsWidget::~SettingsWidget()
{
#ifdef Q_OS_WIN
	if (shadow)
		delete shadow;
#endif
	delete Label1;
	delete Label2;
	delete Label3;
	delete Label4;
	Label1 = NULL;
	Label2 = NULL;
	Label3 = NULL;
	Label4 = NULL;
	delete ui;
}

void SettingsWidget::closeEvent(QCloseEvent * event)
{
	emit sigSettingsClose();
	QWidget::closeEvent(event);
}

void SettingsWidget::changeEvent(QEvent * event)
{
	if (event->type() == QEvent::WindowStateChange)
	{
#ifdef Q_OS_WIN
		if (this->windowState() == Qt::WindowMaximized)
			shadow->hide();
		if (this->windowState() == Qt::WindowMinimized)
			shadow->hide();

		if (this->windowState() == Qt::WindowNoState)
			shadow->show();
#endif
	}

	QWidget::changeEvent(event);
}


void SettingsWidget::moveEvent(QMoveEvent *event)
{
#ifdef Q_OS_WIN
	shadow->move(event->pos().x() - 10, event->pos().y() - 10);
#endif
}

void SettingsWidget::resizeEvent(QResizeEvent* event)
{
#ifdef Q_OS_WIN
	shadow->resize(event->size().width() + 20, event->size().height() + 20);
#endif
}

void SettingsWidget::show()
{
	QWidget::show();
#ifdef Q_OS_WIN
	shadow->resize(width() + 20, height() + 20);
	shadow->move(x() - 10, y() - 10);
	shadow->show();
#endif
#ifdef Q_OS_WIN
	// 窗口被激活了，强制重绘  
	InvalidateRect((HWND)this->winId(), nullptr, FALSE);
#else
    slotAccount();
#endif
}

void SettingsWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void SettingsWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void SettingsWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);

	return QWidget::mouseMoveEvent(event);
}

void SettingsWidget::slotAccount()
{
	if(gThemeStyle == "White")
		ui->pushButton_1->setStyleSheet("QPushButton{background-color:#eceae8;border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:rgb(16, 142, 233);}");
	if(gThemeStyle == "Blue")
		ui->pushButton_1->setStyleSheet("QPushButton{background-color:#042439;border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:rgb(16, 142, 233);}");
	ui->pushButton_2->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");
	ui->pushButton_3->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");
	ui->pushButton_4->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");
	ui->pushButton_5->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");
	ui->stackedWidget->setCurrentIndex(0);
}

void SettingsWidget::slotCurrency()
{
	if(gThemeStyle == "White")
		ui->pushButton_2->setStyleSheet("QPushButton{background-color:#eceae8;border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:rgb(16, 142, 233);}");
	if (gThemeStyle == "Blue")
		ui->pushButton_2->setStyleSheet("QPushButton{background-color:#042439;border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:rgb(16, 142, 233);}");
	ui->pushButton_1->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");
	ui->pushButton_3->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");
	ui->pushButton_4->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");
	ui->pushButton_5->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");
	ui->stackedWidget->setCurrentIndex(1);
}

void SettingsWidget::slotKey()
{
	if (gThemeStyle == "Blue")
		ui->pushButton_3->setStyleSheet("QPushButton{background-color:#042439;border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:rgb(16, 142, 233);}");
	if (gThemeStyle == "White")
		ui->pushButton_3->setStyleSheet("QPushButton{background-color:#eceae8;border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:rgb(16, 142, 233);}");
	ui->pushButton_1->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");
	ui->pushButton_2->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");
	ui->pushButton_4->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");
	ui->pushButton_5->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");
	ui->stackedWidget->setCurrentIndex(2);
}

void SettingsWidget::slotAbout()
{
	if (gThemeStyle == "Blue")
		ui->pushButton_4->setStyleSheet("QPushButton{background-color:#042439;border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:rgb(16, 142, 233);}");
	if (gThemeStyle == "White")
		ui->pushButton_4->setStyleSheet("QPushButton{background-color:#eceae8;border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:rgb(16, 142, 233);}");
	ui->pushButton_1->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");
	ui->pushButton_2->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");
	ui->pushButton_5->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");
	ui->pushButton_3->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");
	ui->stackedWidget->setCurrentIndex(4);
}

void SettingsWidget::slotThemeSwitch()
{
	if (gThemeStyle == "Blue")
		ui->pushButton_5->setStyleSheet("QPushButton{background-color:#042439;border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:rgb(16, 142, 233);}");
	if (gThemeStyle == "White")
		ui->pushButton_5->setStyleSheet("QPushButton{background-color:#eceae8;border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:rgb(16, 142, 233);}");
	ui->pushButton_1->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");
	ui->pushButton_2->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");
	ui->pushButton_4->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");
	ui->pushButton_3->setStyleSheet("QPushButton{background-color:rgba(0,0,0,0);border: none; border-right: 1px solid #eceae8; font: 75 16pt 微软雅黑; font-size:14px; color:#6a82a5;}");
	ui->stackedWidget->setCurrentIndex(3);
}

void SettingsWidget::setUserInfo(UserInfo info)
{
	m_userInfo = info;
	ui->accWidget->setAvatar(m_userInfo.strUserAvatarLocal);
	ui->accWidget->setNickname(m_userInfo.strUserNickName);
	ui->accWidget->setId(QString::number(m_userInfo.nUserID));
}

void SettingsWidget::setScreenCut(QString strValue)
{
	ui->shortCutWidget->setScreenCut(strValue);
}
void SettingsWidget::setQuickOpen(QString strValue)
{
	ui->shortCutWidget->setQuickOpen(strValue);
}
void SettingsWidget::setSendMsg(int iValue)
{
	ui->shortCutWidget->setSendMsg(iValue);
}

void SettingsWidget::setCheckKey(int iValue)
{
	ui->shortCutWidget->setCheckKey(iValue);
}

void SettingsWidget::setClear(QString strValue)
{
	ui->generalCutWidget->setClearText(strValue);
}

void SettingsWidget::setUserPath(QString strValue)
{
	ui->generalCutWidget->setUserPath(strValue);
}

void SettingsWidget::setLanguage(QString strValue)
{
	ui->generalCutWidget->setLanguage(strValue);
}






