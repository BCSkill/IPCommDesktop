﻿#include "immainwidget.h"
#include "imtranstype.h"
#include "shadow.h"
#include "readconfig.h"
#include "aboutwidget.h"

#include "ui_immainwidget.h"
#ifdef Q_OS_WIN
#define WM_TRAYNOTIFY WM_USER+300
#endif
#ifdef Q_OS_MAC
#include "MacNotification.h"
#include <QtMac>
#endif
#include "qxtglobalshortcut.h"
#include "QStringLiteralBak.h"
#include <QMetaType>

#include "imusermessage.h"
#include "contactsdatamanager.h"

#ifdef Q_OS_WIN
#include "NewMsgNotifyShareLib.h"
#include "MessageNotifyModel.h"
#endif


IMMainWidget* IMMainWidget::pThis = NULL;

#ifdef  Q_OS_WIN
NewMsgNotifyShareLib *newmsgnotifysharelib = NULL;
NewNotifyGui *newGui = NULL;
#endif //  Q_OS_WIN

#ifndef Q_OS_WIN
IMMainWidget* IMMainWidget::main_window_;
#ifdef Q_OS_MAC
io_connect_t root_port_;
void IMMainWidget::handleSleepCallBack(void* refCon, io_service_t service,natural_t messageType, void* messageArgument)
{
    Q_UNUSED(refCon);
    Q_UNUSED(service);
    Q_UNUSED(messageArgument);

    switch ( messageType )
    {
        case kIOMessageCanSystemSleep:
            break;
        case kIOMessageSystemWillSleep:
            main_window_->DealMacSystemSleep();
            break;
        case kIOMessageSystemWillPowerOn:
            main_window_->DealMacSystemPowerOn();
            break;
        case kIOMessageSystemHasPoweredOn:
            break;
        default:
            break;
    }
}

int IMMainWidget::registerForSystemSleep()
{
    void* refCon = nullptr;
    root_port_ = IORegisterForSystemPower(refCon, &notify_port_ref_, handleSleepCallBack, &notifier_object_);
    if (root_port_ == 0)
    {
        return -1;
    }

    CFRunLoopAddSource( CFRunLoopGetCurrent(),
            IONotificationPortGetRunLoopSource(notify_port_ref_), kCFRunLoopCommonModes );

    CFRunLoopRun();

    return 0;
}

void IMMainWidget::deregisterForSystemSleep()
{
    CFRunLoopRemoveSource( CFRunLoopGetCurrent(),
                               IONotificationPortGetRunLoopSource(notify_port_ref_),
                               kCFRunLoopCommonModes);

    IODeregisterForSystemPower(&notifier_object_);

    IOServiceClose(root_port_);
    IONotificationPortDestroy(notify_port_ref_);
}

void IMMainWidget::DealMacSystemSleep()
{
    emit sigDormancyState(true);
}

void IMMainWidget::DealMacSystemPowerOn()
{
    emit sigDormancyState(false);
}
#endif
void IMMainWidget::mouseMoveRect(const QPoint& p)
{
	if (!m_state.IsPressBorder)
	{
		if (p.x() > width() - m_border&&p.y()<height() - m_border&&p.y()>m_border)//right side
		{
			setCursor(Qt::SizeHorCursor);
			m_curPos = CursorPos::Right;
		}
		else if (p.x()<m_border&&p.y()<height() - m_border&&p.y()>m_border)//left side;
		{
			setCursor(Qt::SizeHorCursor);
			m_curPos = CursorPos::Left;
		}
		else if (p.y()>height() - m_border&&p.x() > m_border&&p.x() < width() - m_border)//bottom side;
		{
			setCursor(Qt::SizeVerCursor);
			m_curPos = CursorPos::Bottom;
		}
		else if (p.y()<m_border&&p.x()>m_border&&p.x() < width() - m_border)
		{
			setCursor(Qt::SizeVerCursor);
			m_curPos = CursorPos::Top;
		}
		else if (p.y()<m_border&&p.x()>width() - m_border)
		{
			setCursor(Qt::SizeBDiagCursor);
			m_curPos = CursorPos::TopRight;
		}

		else if (p.y()<m_border&&p.x()<m_border)
		{
			setCursor(Qt::SizeFDiagCursor);
			m_curPos = CursorPos::TopLeft;
		}

		else if (p.x()>m_border&&p.y()>height() - m_border)
		{
			setCursor(Qt::SizeFDiagCursor);
			m_curPos = CursorPos::BottomRight;
		}

		else if (p.x()<m_border&&p.y()>height() - m_border)
		{
			setCursor(Qt::SizeBDiagCursor);
			m_curPos = CursorPos::BottomLeft;
		}
		else
		{
			setCursor(Qt::ArrowCursor);
		}
	}
	else
	{
		switch (m_curPos) {
		case CursorPos::Right:
		{
								 int setW = QCursor::pos().x() - x();
								 if (minimumWidth() <= setW&&setW <= maximumWidth())
									 setGeometry(x(), y(), setW, height());
								 break;
		}
		case CursorPos::Left:
		{
								int setW = x() + width() - QCursor::pos().x();
								int setX = QCursor::pos().x();
								if (minimumWidth() <= setW&&setW <= maximumWidth())
									setGeometry(setX, y(), setW, height());
								break;
		}
		case CursorPos::Bottom:
		{
								  int setH = QCursor::pos().y() - y();
								  if (minimumHeight() <= setH&&setH <= maximumHeight())
									  setGeometry(x(), y(), width(), setH);
								  break;
		}
		case CursorPos::Top:
		{
							   int setH = y() - QCursor::pos().y() + height();
							   if (minimumHeight() <= setH&&setH <= maximumHeight())
								   setGeometry(x(), QCursor::pos().y(), width(), setH);
							   break;
		}
		case CursorPos::TopRight:
		{
									int setH = y() + height() - QCursor::pos().y();
									int setW = QCursor::pos().x() - x();
									int setY = QCursor::pos().y();
									if (setH >= maximumHeight())
									{
										setY = m_state.WindowPos.y() + m_state.PressedSize.height() - height();
										setH = maximumHeight();
									}
									if (setH <= minimumHeight())
									{
										setY = m_state.WindowPos.y() + m_state.PressedSize.height() - height();
										setH = minimumHeight();
									}
									setGeometry(m_state.WindowPos.x(), setY, setW, setH);
									break;
		}
		case CursorPos::TopLeft:
		{
								   int setY = QCursor::pos().y();
								   int setX = QCursor::pos().x();

								   int setW = pos().x() + width() - setX;
								   int setH = pos().y() + height() - setY;
								   int totalW = m_state.WindowPos.x() + m_state.PressedSize.width();
								   int totalH = m_state.WindowPos.y() + m_state.PressedSize.height();

								   if (totalW - setX >= maximumWidth())
								   {
									   setX = totalW - maximumWidth();
									   setW = maximumWidth();
								   }
								   if (totalW - setX <= minimumWidth())
								   {
									   setX = totalW - minimumWidth();
									   setW = minimumWidth();
								   }
								   if (totalH - setY >= maximumHeight())
								   {
									   setY = totalH - maximumHeight();
									   setH = maximumHeight();
								   }
								   if (totalH - setY <= minimumHeight())
								   {
									   setY = totalH - minimumHeight();
									   setH = minimumHeight();
								   }
								   setGeometry(setX, setY, setW, setH);
								   break;
		}
		case CursorPos::BottomRight:
		{
									   int setW = QCursor::pos().x() - x();
									   int setH = QCursor::pos().y() - y();
									   setGeometry(m_state.WindowPos.x(), m_state.WindowPos.y(), setW, setH);
									   break;
		}
		case CursorPos::BottomLeft:
		{
									  int setW = x() + width() - QCursor::pos().x();
									  int setH = QCursor::pos().y() - m_state.WindowPos.y();
									  int setX = QCursor::pos().x();
									  int totalW = m_state.WindowPos.x() + m_state.PressedSize.width();
									  if (totalW - setX >= maximumWidth())
									  {
										  setX = totalW - maximumWidth();
										  setW = maximumWidth();
									  }
									  if (totalW - setX <= minimumWidth())
									  {
										  setX = totalW - minimumWidth();
										  setW = minimumWidth();
									  }
									  setGeometry(setX, m_state.WindowPos.y(), setW, setH);
									  break;
		}
		default:
			setCursor(Qt::ArrowCursor);
			break;
		}
	}
}
void IMMainWidget::mousePressEvent(QMouseEvent *event)
{
    m_state.PressedSize = this->size();
    m_state.IsPressBorder = false;
    setFocus();
    if (event->button() == Qt::LeftButton)
    {
        m_state.WindowPos = this->pos(); //save the prssed position
        if (QRect(m_border + 1, m_border + 1, width() - (m_border + 1) * 2, height() - (m_border + 1) * 2).contains(event->pos()))
        {
            m_state.MousePos = event->globalPos();
            m_state.MousePressed = true;
        }
        else
            m_state.IsPressBorder = true;
    }
}

void IMMainWidget::mouseReleaseEvent(QMouseEvent *event)
{
    m_state.IsPressBorder = false;
    if (event->button() == Qt::LeftButton)
    {
        this->m_state.MousePressed = false;
    }
}

void IMMainWidget::enterEvent(QEvent *event)
{
    Q_UNUSED(event);
    #ifdef Q_OS_MAC
    MacNotification::instance().hideAllNotify();
    #endif
}
#ifdef Q_OS_MAC
void IMMainWidget::onNotifyClicked(const QString& userID)
{
    if(userID.contains("@"))
    {
        QString strUserID = userID;
        emit sigOpenChat(OpenGroup ,QVariant(strUserID.remove("@")));
    }
    else
    {
        emit sigOpenChat(OpenPer ,QVariant(userID));
    }
    raise();
    activateWindow();
    showNormal();
    StopMessageFlash();
}

void IMMainWidget::onNotifyReplied(const QString& userID, const QString &reply)
{
    emit sigNotifyReplied(userID,reply);
    StopMessageFlash();
}

void IMMainWidget::NotifyNotice(QString title,QString userID,QString strMsg)
{
   // if(this->isHidden() || !this->hasFocus())
    if(this->isHidden() || this->isMinimized())
    {
        MacNotification::instance().showNotify(title,strMsg,userID);
    }
}
#endif
#endif

IMMainWidget::IMMainWidget(QWidget *parent)
: QWidget(parent)
{
	ui = new Ui::IMMainWidget();
	ui->setupUi(this);
    pThis = this;
    //aboutWidget = NULL;
	functionWidget = NULL;

	mMessageRevFlashTime = NULL;
	mMessageRevFlashNum = 0;
    mFlashingFlag = false;

	m_pNetWarning = new NetWarningWidget(this);
	bNetWarn = false;
#ifdef Q_OS_WIN
    shadow = new Shadow();
#endif
#ifdef Q_OS_LINUX
    this->setWindowIcon(QIcon(":/Ico/Resources/logo/logo.ico"));
    this->setWindowFlags(Qt::FramelessWindowHint);
#else
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowMinimizeButtonHint);
#endif

    buttonGroup = new QButtonGroup(this);

    ui->titleBar->installEventFilter(this);
    ui->hearderLabel->installEventFilter(this);
    ui->nickNameLabel->installEventFilter(this);
    ui->contentStacked->installEventFilter(this);

    registerHandle();

	QFile file(":/QSS/Resources/QSS/OPMainWidgetShareLib/immainwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();


    //星际通讯。
    ui->statusBar->hide();
    ui->robotsBtn->hide();

#ifdef Q_OS_WIN
#else
#ifdef Q_OS_MAC
    is_registered_ = false;
#endif
    main_window_ = this;
    m_border = 4;
    m_state.MousePressed = false;
    setMouseTracking(true);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    ui->contentStacked->setMouseTracking(true);
#ifdef Q_OS_MAC
    MacNotification::instance().setMainWnd(this);
#endif
#endif
#ifdef Q_OS_WIN
	loadMsgNotifyBox();
#endif
    CreateTrayMenu();


	//激活到最前面
#ifdef Q_OS_WIN
	::SetForegroundWindow((HWND)this->winId());
#endif

}

IMMainWidget::~IMMainWidget()
{
	/*if (mSysTrayIcon)
	{
		delete mSysTrayIcon;
		mSysTrayIcon = NULL;
	}*/
#ifdef Q_OS_WIN
	if (shadow)
		shadow->deleteLater();
	if (newmsgnotifysharelib)
		newmsgnotifysharelib->deleteLater();
	if (newGui)
		newGui->deleteLater();
#endif
#ifdef Q_OS_MAC
    deregisterForSystemSleep();
#endif
	StopMessageFlash();

	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

IMMainWidget* IMMainWidget::self()
{
	return pThis;
}

void IMMainWidget::registerHandle()
{
    connect(ui->minBtn, SIGNAL(clicked(bool)), this, SLOT(showMinimized()));
	connect(ui->maxBtn, SIGNAL(clicked(bool)), this, SLOT(slotFullScreen()));
	connect(ui->closeBtn, SIGNAL(clicked(bool)), this, SLOT(close()));
#ifdef Q_OS_WIN
	connect(ui->closeBtn, SIGNAL(clicked(bool)), shadow, SLOT(hide()));
#endif
	connect(buttonGroup, SIGNAL(buttonToggled(int, bool)), this, SLOT(slotCheckWidget(int, bool)));

	QList<AppButton> appList = ReadConfig().read();

	ui->messageBtn->setFixedSize(50, 46);
	ui->messageBtn->setToolTip(tr("space"));
	ui->contactsBtn->setFixedSize(50, 46);
	ui->contactsBtn->setToolTip(tr("OpenPlanet"));
	ui->robotsBtn->setFixedSize(50, 46);
	ui->walletBtn->setToolTip(tr("base"));
	ui->walletBtn->setFixedSize(50, 46);
	
	buttonGroup->addButton(ui->messageBtn, 1);
	buttonGroup->addButton(ui->contactsBtn, 2);
	buttonGroup->addButton(ui->walletBtn, 3);

	/*aboutWidget = new AboutWidget();
	aboutWidget->hide();*/

	functionWidget= new FunctionWidget();
	functionWidget->hide();

	ui->settingsBtn->setToolTip(tr("Setting"));
	connect(ui->settingsBtn, SIGNAL(clicked()), this, SIGNAL(sigSettings()));
}

void IMMainWidget::setAppVersion(QString version)
{
	mAppVersion = version;
	/*if (aboutWidget)
	{
		aboutWidget->setNumber(mAppVersion);
	}*/
}

void IMMainWidget::setAppName(QString name)
{
	/*this->setWindowTitle(name);

	if (aboutWidget)
		aboutWidget->setName(name);*/
}

void IMMainWidget::show()
{
	QWidget::show();

#ifdef Q_OS_WIN
	shadow->resize(width() + 20, height() + 20);
	shadow->move(x() - 10, y() - 10);
	shadow->show();
#endif
}

void IMMainWidget::paintEvent(QPaintEvent * event)
{
    Q_UNUSED(event)
#ifdef Q_OS_WIN
// 	QPainter painter(this);
// 	QPen pen;
// 	pen.setColor(QColor(24, 50, 87));
//     pen.setWidth(3);
// 	painter.setPen(pen);
// 	painter.drawLine(1, 1, this->width(), 1);
// 	painter.drawLine(0, 0, 0, this->height());
// 	painter.drawLine(this->width()-1, 0, this->width()-1, this->height());
// 	painter.drawLine(0, this->height()-1, this->width(), this->height()-1);
// 	painter.end();
	//pen.setColor(QColor(10, 24, 45));
#endif
}

void IMMainWidget::moveEvent(QMoveEvent *event)
{
#ifdef Q_OS_WIN
	shadow->move(event->pos().x() - 10, event->pos().y() - 10);
#endif
	m_pNetWarning->move(event->pos().x() + (width()-m_pNetWarning->width())/2, event->pos().y()- m_pNetWarning->height()-10);
}

void IMMainWidget::resizeEvent(QResizeEvent* event)
{
#ifdef Q_OS_WIN
	shadow->resize(event->size().width() + 20, event->size().height() + 20);
#endif
}
void IMMainWidget::showEvent(QShowEvent * event)
{
	if (bNetWarn)
	{
		m_pNetWarning->show();
	}
}
void IMMainWidget::changeEvent(QEvent * event)
{
	if (event->type() == QEvent::WindowStateChange)
	{
#ifdef Q_OS_WIN
		if (this->windowState() == Qt::WindowMaximized)
			shadow->hide();
		if (this->windowState() == Qt::WindowMinimized)
		{
			m_pNetWarning->hide();
			shadow->hide();
		}
		if (this->windowState() == Qt::WindowNoState)
			shadow->show();
#endif
	}

	QWidget::changeEvent(event);
}

void IMMainWidget::closeEvent(QCloseEvent * event)
{
#ifdef Q_OS_WIN
	if (shadow)
	    shadow->hide();
#endif
	m_pNetWarning->hide();
	QWidget::closeEvent(event);
}

bool IMMainWidget::nativeEvent(const QByteArray &eventType, void *message, long *result)
{
#ifdef Q_OS_WIN
	PMSG msg = (PMSG)message;

	if (eventType == "windows_generic_MSG" || eventType == "windows_dispatcher_MSG")
	{
		PMSG msg = (PMSG)message;
		if (msg->message == WM_COPYDATA)
		{
			COPYDATASTRUCT * p = reinterpret_cast<COPYDATASTRUCT*>(msg->lParam);
			const char* strJson = static_cast<const char*>(p->lpData);
			QString strTemp = QString::fromLocal8Bit(strJson);
			QByteArray byteArray = strTemp.toUtf8();
			QString strJsonEx = QString::fromUtf8(byteArray);
			emit sigParseScreenCutMessage(strJsonEx);
		}
	}

	if (msg->message == WM_ACTIVATE && msg->wParam == WA_ACTIVE)
	{
		InvalidateRect((HWND)this->winId(), nullptr, FALSE);
		return false;
	}

	if (msg->message == WM_NCHITTEST)
	{
		if (shadow && shadow->isHidden())
			return false;
		*result = 0;
		const LONG border_width = 4; //in pixels
		RECT winrect;
		GetWindowRect((HWND)winId(), &winrect);
		long x = GET_X_LPARAM(msg->lParam);
		long y = GET_Y_LPARAM(msg->lParam);

		bool resizeWidth = minimumWidth() != maximumWidth();
		bool resizeHeight = minimumHeight() != maximumHeight();
		if (resizeWidth)
		{
			//left border
			if (x >= winrect.left && x < winrect.left + border_width)
			{
				*result = HTLEFT;
			}
			//right border
			if (x < winrect.right && x >= winrect.right - border_width)
			{
				*result = HTRIGHT;
			}
		}
		if (resizeHeight)
		{
			//bottom border
			if (y < winrect.bottom && y >= winrect.bottom - border_width)
			{
				*result = HTBOTTOM;
			}
			//top border
			if (y >= winrect.top && y < winrect.top + border_width)
			{
				*result = HTTOP;
			}
		}
		if (resizeWidth && resizeHeight)
		{
			//bottom left corner
			if (x >= winrect.left && x < winrect.left + border_width &&
				y < winrect.bottom && y >= winrect.bottom - border_width)
			{
				*result = HTBOTTOMLEFT;
			}
			//bottom right corner
			if (x < winrect.right && x >= winrect.right - border_width &&
				y < winrect.bottom && y >= winrect.bottom - border_width)
			{
				*result = HTBOTTOMRIGHT;
			}
			//top left corner
			if (x >= winrect.left && x < winrect.left + border_width &&
				y >= winrect.top && y < winrect.top + border_width)
			{
				*result = HTTOPLEFT;
			}
			//top right corner
			if (x < winrect.right && x >= winrect.right - border_width &&
				y >= winrect.top && y < winrect.top + border_width)
			{
				*result = HTTOPRIGHT;
			}
		}
		if (*result == 0)
			return QWidget::nativeEvent(eventType, message, result);
		return true;
	}
	else if (msg->message == WM_POWERBROADCAST)
	{
		if (msg->wParam == PBT_APMSUSPEND)         //系统休眠了
		{
			emit sigDormancyState(true);
		}
		else if (msg->wParam == PBT_APMRESUMESUSPEND) //系统唤醒了
		{
			emit sigDormancyState(false);
		}
		return false;
	}
	if (msg->message == WM_TRAYNOTIFY)
	{
		switch (msg->lParam)
		{
		case WM_RBUTTONDOWN:
			qDebug() << "WM_RBUTTONDOWN";
			//CreateRightMenu(pMsg->pt.x, pMsg->pt.y);
			break;
		}
	}
#endif
#ifdef Q_OS_MAC
    if (eventType == "NSEvent")
    {
        if (!is_registered_)
        {
            registerForSystemSleep();
            is_registered_ = true;
        }
    }

#endif
	return QWidget::nativeEvent(eventType, message, result);
}

bool IMMainWidget::eventFilter(QObject *obj, QEvent *e)
{
    if ((obj == ui->hearderLabel || obj == ui->nickNameLabel) && e->type() == QEvent::MouseButtonPress)
    {
        QMouseEvent *mouseEvent = (QMouseEvent *)e;
        if (mouseEvent->button() == Qt::LeftButton)
        {
            //QPoint globalPos = ui->hearderLabel->mapToGlobal(ui->hearderLabel->pos());
            emit sigUserProfile();
        }

    }
    if (obj == ui->contentStacked && e->type() == QEvent::MouseButtonPress)
    {
    }
    if (obj == ui->titleBar)
    {
        if (e->type() == QEvent::MouseButtonDblClick)
            this->slotFullScreen();
#ifdef Q_OS_WIN
        if (shadow && shadow->isVisible())
        {
#endif
            if (e->type() == QEvent::MouseButtonPress)
            {
                QMouseEvent *event = (QMouseEvent *)e;
                mouse = event->pos();   //设置移动的原始位置。
            }
            if (e->type() == QEvent::MouseMove)
            {
                QMouseEvent *event = (QMouseEvent *)e;
                if (mouse.x() >= 0)
                {
                    //首先通过做差值，获得鼠标位移的距离。
                    int x = event->pos().x() - mouse.x();
                    int y = event->pos().y() - mouse.y();
                    //移动本窗体。
                    this->move(this->x() + x, this->y() + y);
                }
            }
            if (e->type() == QEvent::MouseButtonRelease)
            {
                mouse.setX(-1);
            }
#ifdef Q_OS_WIN
        }
#endif
    }
    return QWidget::eventFilter(obj, e);
}

void IMMainWidget::slotFullScreen()
{
	if (this->isMaximized())
	{
		this->showNormal();
		ui->maxBtn->setStyleSheet("QPushButton#maxBtn{border-image:url(:/mainWidget/Resources/mainWidget/title_max.png);}");
		ui->maxBtn->setToolTip(tr("maximize"));
	}
	else
	{
		this->showMaximized();
		ui->maxBtn->setStyleSheet("QPushButton#maxBtn{border-image:url(:/mainWidget/Resources/mainWidget/title_recover.png);}");
		ui->maxBtn->setToolTip(tr("restore"));
	}
}

void IMMainWidget::slotCheckWidget(int id, bool checked)
{
	if (checked)
	{
		if (ui->contentStacked->widget(id))
			ui->contentStacked->setCurrentIndex(id);
	}
}

//设置昵称
void IMMainWidget::setNikeName(QString nickName)
{
	ui->nickNameLabel->setText(nickName);
}

//设置头像
void IMMainWidget::setHeaderImage(QPixmap headerImage)
{
	if (headerImage.isNull())
	{
		headerImage.load(":/PerChat/Resources/person/temp.png");
	}
	ui->hearderLabel->setPixmap(headerImage);
}

void IMMainWidget::addWidget(QWidget *widget)
{
	ui->contentStacked->addWidget(widget);
}

void IMMainWidget::CreateTrayMenu()
{
#ifdef Q_OS_WIN
    mSysTrayIcon.CreateTrayIcon(QIcon(":/Ico/Resources/logo/logo.ico"));
	connect(&mSysTrayIcon, SIGNAL(sigExit()), this, SIGNAL(sigAskExit()));
	connect(this, SIGNAL(sigAnswerExit()), this, SLOT(slotExit()));
	//connect(&mSysTrayIcon, SIGNAL(sigabout()), aboutWidget, SLOT(show()));
	connect(&mSysTrayIcon, SIGNAL(sigabout()), functionWidget, SLOT(show()));
	connect(&mSysTrayIcon, SIGNAL(sigMouseLBttonDown()), this, SLOT(slotSysTrayIconClicked()));
#else
    mWidgetAction = new QWidgetAction(this);
    mAction = new xSysTrayMeauAction(this);
    connect(mAction, SIGNAL(sigabout()), functionWidget, SLOT(show()));
    connect(mAction, SIGNAL(sigExit()), this, SIGNAL(sigAskExit()));
	connect(this, SIGNAL(sigAnswerExit()), this, SLOT(slotExit()));
    mWidgetAction->setDefaultWidget(mAction);
    mTrayIconMenu = new QMenu(this);
    mTrayIconMenu->addAction(mWidgetAction);
    mSysTrayIcon.setIcon(QIcon(":/Ico/Resources/logo/logo.ico"));
    mSysTrayIcon.setToolTip(tr("OpenPlanet"));
    mSysTrayIcon.setContextMenu(mTrayIconMenu);
    connect(&mSysTrayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(slotSysTrayIconClickedMac(QSystemTrayIcon::ActivationReason)));
    mSysTrayIcon.show();
#endif
}

void IMMainWidget::slotTriggered()
{
    qDebug()<<" clicked ";
}

#ifndef Q_OS_WIN
/*处理托盘点击事件*/

void IMMainWidget::slotSysTrayIconClickedMac(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
    case QSystemTrayIcon::Trigger:
     //   mTrayIconMenu->exec();
        slotSysTrayIconClicked();
        break;
    default:
        break;
    }
}
#endif

void IMMainWidget::slotSysTrayIconClicked()
{
	if (mMessageRevFlashTime != NULL)
	{
		if (mMessageRevFlashTime->isActive())
		{
			mMessageRevFlashTime->stop();
            mSysTrayIcon.setIcon(QIcon(":/Login/Resources/login/system.ico"));
			mMessageRevFlashNum = 0;
			mFlashingFlag = false;
			emit sigSendFlashStatus(false);

			QString fileName = mMessageRevHeaderImg.split("/").last();
			QString buddyID = fileName.split(".").first();
			if (mMessageRevType != NewApply)
			{
				//emit sigTrayOpenChat(mMessageRevType, QVariant(buddyID));
				if (ContactsDataManager::instance())
				{
					emit ContactsDataManager::instance()->sigOpenPerOrGroupChat(buddyID);
				}
				
			}
			else
			{
				slotCheckWidget(2, true);
				ui->contactsBtn->setChecked(true);
				emit sigNewApply();
			}
		}
#ifdef Q_OS_WIN
		newGui->slotClickButton();
#endif
	}
	if (this->isMinimized())
	{
		this->showNormal();
	}
    #ifdef Q_OS_WIN
	//设置窗口置顶
	::SetWindowPos(HWND(this->winId()), HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
	::SetWindowPos(HWND(this->winId()), HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
    #endif
    this->show();
	this->activateWindow();
    raise();

#ifdef Q_OS_WIN
	//此处使用hide会出现中断 具体原因不详
	if (newGui->isVisible())
		newGui->close();
#endif
}

#ifdef Q_OS_WIN
void IMMainWidget::slotItemOfMsgBoxClicked(QString id, int funcType)
{
	if (mMessageRevFlashTime != NULL)
	{
		if (mMessageRevFlashTime->isActive())
		{
			mMessageRevFlashTime->stop();
			mSysTrayIcon.setIcon(QIcon(":/Login/Resources/login/system.ico"));
			mMessageRevFlashNum = 0;
			mFlashingFlag = false;
			emit sigSendFlashStatus(false);
			if (funcType != NewApply)
			{
				//emit sigTrayOpenChat(funcType, QVariant(id));
				if (ContactsDataManager::instance())
				{
					emit ContactsDataManager::instance()->sigOpenPerOrGroupChat(id);
				}
				
			}
			else
			{
				slotCheckWidget(2, true);
				ui->contactsBtn->setChecked(true);
				emit sigNewApply();
			}
		}
	}
	if (this->isMinimized())
	{
		this->showNormal();
	}
	//设置窗口置顶
	::SetWindowPos(HWND(this->winId()), HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
	::SetWindowPos(HWND(this->winId()), HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
	this->show();
	this->activateWindow();
	raise();

#ifdef Q_OS_WIN
	//此处使用hide会出现中断 具体原因不详
	if(newGui->isVisible())
		newGui->close();
#endif

}
#endif

void IMMainWidget::slotExit()
{
#ifdef Q_OS_WIN
	if (shadow)
	{
		delete shadow;
		shadow = NULL;
	}
#endif
	emit sigDestroyIcon();
	StopMessageFlash();
	emit sigExit();
	this->close();
}

void IMMainWidget::setCurrentWidget(int type)
{
	if (type == OpenPer || type == OpenGroup)
	{
		ui->messageBtn->setChecked(true);
	}
	if (type == OpenWallet)
	{
		ui->walletBtn->setChecked(true);
	}
	ui->contentStacked->setCurrentIndex(buttonGroup->checkedId());
}

void IMMainWidget::slotSetMainWidgetStatusLabel(QString msg)
{
	ui->statusLabel->setText(msg);
}

void IMMainWidget::slotDealTrayIconFlash(QString buddyID)
{
	QString fileName = mMessageRevHeaderImg.split("/").last();
	QString name = fileName.split(".").first();

	if (buddyID == name || buddyID == "apply")   //apply.ico是验证消息的图标。
	{
		if (mMessageRevFlashTime)
		{
			if (mMessageRevFlashTime->isActive())
				mMessageRevFlashTime->stop();
            mSysTrayIcon.setIcon(QIcon(":/Login/Resources/login/system.ico"));
			mMessageRevFlashNum = 0;
			mFlashingFlag = false;//added by wangqingjie
			emit sigSendFlashStatus(false);
		}
	}
}

void IMMainWidget::slotMessageRevFlashTime()
{
	if (mMessageRevFlashNum % 2 == 0)
	{
		QPixmap map;
		QFile file(mMessageRevHeaderImg);
		QByteArray array;
		if (file.open(QIODevice::ReadOnly))
		{
			array = file.readAll();
		}
		map.loadFromData(array);
		if (map.isNull())
			map.load(":/GroupChat/Resources/groupchat/group.png");

		QIcon icon(map);
        mSysTrayIcon.setIcon(icon);
	}
	else
	{
        mSysTrayIcon.setIcon(QIcon(":/Login/Resources/login/system.ico"));
	}
	mMessageRevFlashNum++;
}

void IMMainWidget::slotShowAllClicked()
{

}

void IMMainWidget::StartMessageFlash(int type, QString imagePath, MessageInfo *messageInfo)
{
	this->mMessageRevType = type;
	this->mMessageRevHeaderImg = imagePath;

	mFlashingFlag = true;
	emit sigSendFlashStatus(true);
	if (mMessageRevFlashTime == NULL)
	{
		mMessageRevFlashTime = new QTimer(this);
		connect(mMessageRevFlashTime, SIGNAL(timeout()), this, SLOT(slotMessageRevFlashTime()));
	}
	if (!mMessageRevFlashTime->isActive())
	{
		mMessageRevFlashTime->start(500);
	}
	if (mMessageRevFlashTime->isActive())
	{
		//系统图标闪烁 进行消息窗口处理
#ifdef Q_OS_WIN
		newmsgnotifysharelib->dealMsgFlash(type, imagePath, messageInfo);
#endif
	}
}

void IMMainWidget::StopMessageFlash()
{
	if (mMessageRevFlashTime != NULL)
	{
		if (mMessageRevFlashTime->isActive())
		{
			mMessageRevFlashTime->stop();
		}
		delete mMessageRevFlashTime;
		mMessageRevFlashTime = NULL;
		mMessageRevFlashNum = 0;
        mSysTrayIcon.setIcon(QIcon(":/Login/Resources/login/system.ico"));
	}
}

void IMMainWidget::slotCountMessage(int num)
{
#ifdef Q_OS_MAC
    MacNotification::instance().setBadgeNumber(num);
#endif
	ui->messageBtn->slotSetNum(num);
}

void IMMainWidget::slotContactsMessageNum(int num)
{
	ui->contactsBtn->slotSetNum(num);
}

void IMMainWidget::slotClickedDock()
{
    if (this->isMinimized())
        this->showNormal();
    else
        this->show();
    StopMessageFlash();
    this->activateWindow();
    slotGlobalMouseMouse();
}

void IMMainWidget::slotGlobalMouseMouse()
{
#ifdef Q_OS_MAC
	mouseMoveRect(mapFromGlobal(QCursor::pos()));
	if (m_state.MousePressed)
	{
		//this->move(m_state.WindowPos + (event->globalPos() - m_state.MousePos));
	}
#endif
}

void IMMainWidget::slotQuickOpen()
{
	if (this->isMinimized())
	{
		this->showNormal();
#ifdef Q_OS_WIN
			//设置窗口置顶
			::SetWindowPos(HWND(this->winId()), HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
			::SetWindowPos(HWND(this->winId()), HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#endif
			this->activateWindow();
			raise();
	}
	else if (this->isHidden())
	{
#ifdef Q_OS_WIN
		//设置窗口置顶
		::SetWindowPos(HWND(this->winId()), HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
		::SetWindowPos(HWND(this->winId()), HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#endif
		this->show();
		this->activateWindow();
		raise();
	}
	else
	{
		this->close();
#ifdef Q_OS_WIN
		shadow->hide();
#endif
	}
}
#ifdef Q_OS_WIN
void IMMainWidget::loadMsgNotifyBox()
{
	newmsgnotifysharelib = new NewMsgNotifyShareLib();
	newGui = new NewNotifyGui();
	newGui->addQmlWidget(newmsgnotifysharelib);

	qRegisterMetaType<MessageNotifyModel * >("MessageNotifyModel *");
	connect(this, SIGNAL(sigSendFlashStatus(bool)), &mSysTrayIcon, SLOT(slotGetFlashStatus(bool)));
	connect(&mSysTrayIcon, SIGNAL(sigMouseSuspendIn(QPoint)), newGui, SLOT(slotMouseSuspendIn(QPoint)));
	connect(&mSysTrayIcon, SIGNAL(sigMouseSuspendOut(QPoint, int)), newGui, SLOT(slotMouseSuspendOut(QPoint, int)));
	connect(newGui, SIGNAL(sigSendBoxRect(QRect)), &mSysTrayIcon, SLOT(slotReceiveBoxRect(QRect)));
	connect(newGui, SIGNAL(sigIgnoreBtnClicked()), newmsgnotifysharelib, SLOT(slotIgnoreBtnClicked()));
	//connect(newGui, SIGNAL(sigCancleSuspendedColor()), newmsgnotifysharelib, SLOT(slotCancleSuspendedColor()));
	connect(newGui, SIGNAL(sigIgnoreBtnClicked()), this, SLOT(slotIgnoreBtnClicked()));
	connect(newmsgnotifysharelib, SIGNAL(sigItemOfMsgBoxClicked(QString, int)), this, SLOT(slotItemOfMsgBoxClicked(QString, int)));
	connect(newmsgnotifysharelib, SIGNAL(sigAdjustStyle(MessageNotifyModel *)), newGui, SLOT(slotAdjustStyle(MessageNotifyModel *)));
	//connect(newmsgnotifysharelib, SIGNAL(sigHideWidget()), newGui, SLOT(slotHideWidget()));
}
void IMMainWidget::slotIgnoreBtnClicked()
{
	this->StopMessageFlash();
}
#endif
void IMMainWidget::slotNetWarning(bool bV)
{
	bNetWarn = bV;
	if (this->isVisible()&&bV&&this->windowState() != Qt::WindowMinimized)
	{
		m_pNetWarning->show();
	}
	else
	{
		m_pNetWarning->hide();
	}
}