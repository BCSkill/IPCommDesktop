#include "netwarningwidget.h"
#include "ui_netwarningwidget.h"
#include "qfile.h"

NetWarningWidget::NetWarningWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::NetWarningWidget();
	this->setAttribute(Qt::WA_DeleteOnClose);
	this->setWindowFlags(Qt::FramelessWindowHint| Qt::Tool);
	this->setAttribute(Qt::WA_TranslucentBackground);
	ui->setupUi(this);

	QFile styleFile(":/QSS/Resources/QSS/OPMainWidgetShareLib/netwarningwidget.qss");
	styleFile.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(styleFile.readAll());
	setStyleSheet(styleSheet);
	styleFile.close();
}

NetWarningWidget::~NetWarningWidget()
{
	delete ui;
}
