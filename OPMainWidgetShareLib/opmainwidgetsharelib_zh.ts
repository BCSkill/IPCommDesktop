<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AboutWidget</name>
    <message>
        <location filename="aboutwidget.ui" line="20"/>
        <location filename="aboutwidget.ui" line="82"/>
        <source>about</source>
        <translation>关于</translation>
    </message>
</context>
<context>
    <name>CSysTrayIcon</name>
    <message>
        <location filename="CSysTrayIcon.cpp" line="50"/>
        <source>OpenPlanet</source>
        <translation>星际通讯</translation>
    </message>
    <message>
        <location filename="CSysTrayIcon.cpp" line="63"/>
        <location filename="CSysTrayIcon.cpp" line="67"/>
        <source>about</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="CSysTrayIcon.cpp" line="74"/>
        <location filename="CSysTrayIcon.cpp" line="79"/>
        <source>quit</source>
        <translation>退出</translation>
    </message>
</context>
<context>
    <name>ChangeAvatarWidget</name>
    <message>
        <location filename="userprofile/changeavatarwidget.ui" line="26"/>
        <location filename="userprofile/changeavatarwidget.ui" line="123"/>
        <source>Change avatar</source>
        <translation>更换头像</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.ui" line="340"/>
        <source>Upload picture</source>
        <oldsource>upload photo</oldsource>
        <translation>上传照片</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.ui" line="370"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.ui" line="400"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.cpp" line="249"/>
        <source>choose photo</source>
        <translation>选择照片</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.cpp" line="249"/>
        <source>Image file(*.jpg *.png *.bmp)</source>
        <translation>图片文件(*.jpg *.png *.bmp)</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.cpp" line="267"/>
        <source>Wait a moment</source>
        <translation>稍等</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.cpp" line="267"/>
        <source>The avatar is uploading!</source>
        <translation>头像正在上传中!</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.cpp" line="295"/>
        <source>Sorry</source>
        <translation>抱歉</translation>
    </message>
    <message>
        <location filename="userprofile/changeavatarwidget.cpp" line="295"/>
        <source>Uploading avatar failed!</source>
        <translation>上传头像失败!</translation>
    </message>
</context>
<context>
    <name>FunctionWidget</name>
    <message>
        <location filename="FunctionWidget.ui" line="14"/>
        <source>FunctionWidget</source>
        <translation>功能介绍</translation>
    </message>
    <message>
        <location filename="FunctionWidget.ui" line="72"/>
        <source>Features</source>
        <translation>功能介绍</translation>
    </message>
    <message>
        <location filename="FunctionWidget.ui" line="267"/>
        <location filename="FunctionWidget.cpp" line="53"/>
        <source>Full chain</source>
        <translation>全链</translation>
    </message>
    <message>
        <location filename="FunctionWidget.cpp" line="66"/>
        <source>Full platform</source>
        <translation>全平台</translation>
    </message>
    <message>
        <location filename="FunctionWidget.cpp" line="79"/>
        <source>Mining</source>
        <translation>挖矿</translation>
    </message>
    <message>
        <location filename="FunctionWidget.cpp" line="92"/>
        <source>Encrypted communication</source>
        <translation>加密通讯</translation>
    </message>
    <message>
        <location filename="FunctionWidget.cpp" line="105"/>
        <source>Digital assets</source>
        <translation>数字资产</translation>
    </message>
</context>
<context>
    <name>IMMainWidget</name>
    <message>
        <location filename="immainwidget.ui" line="179"/>
        <source>message</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="204"/>
        <source>contact</source>
        <translation>联系人</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="226"/>
        <source>robot</source>
        <translation>机器人</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="248"/>
        <source>device</source>
        <translation>设备</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="343"/>
        <source>Modify Information</source>
        <translation>修改资料</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="368"/>
        <source>test</source>
        <translation>测试</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="436"/>
        <location filename="immainwidget.ui" line="467"/>
        <source>minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="498"/>
        <location filename="immainwidget.cpp" line="762"/>
        <source>maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="529"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="immainwidget.ui" line="674"/>
        <source>Copyright © Pansoft</source>
        <translation>版权所有 © 普联软件股份有限公司</translation>
    </message>
    <message>
        <location filename="immainwidget.cpp" line="455"/>
        <source>space</source>
        <translation>空间</translation>
    </message>
    <message>
        <source>interPlanet</source>
        <oldsource>interstellar</oldsource>
        <translation type="vanished">星际</translation>
    </message>
    <message>
        <location filename="immainwidget.cpp" line="459"/>
        <source>base</source>
        <translation>基地</translation>
    </message>
    <message>
        <location filename="immainwidget.cpp" line="472"/>
        <source>Setting</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="immainwidget.cpp" line="768"/>
        <source>restore</source>
        <translation>恢复</translation>
    </message>
    <message>
        <location filename="immainwidget.cpp" line="457"/>
        <location filename="immainwidget.cpp" line="821"/>
        <source>OpenPlanet</source>
        <translation>星际通讯</translation>
    </message>
</context>
<context>
    <name>MenuWidget</name>
    <message>
        <source>wallet</source>
        <translation type="vanished">钱包</translation>
    </message>
</context>
<context>
    <name>NetWarningWidget</name>
    <message>
        <location filename="netwarningwidget.ui" line="14"/>
        <source>NetWarningWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="netwarningwidget.ui" line="121"/>
        <source>The network is unavailable, please check the network settings!</source>
        <translation>网络不可用，请检查网络设置！</translation>
    </message>
</context>
<context>
    <name>QAboutWidget</name>
    <message>
        <location filename="settings/qaboutwidget.ui" line="14"/>
        <source>QAboutWidget</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="settings/qaboutwidget.ui" line="189"/>
        <source>current version0.0.0.0</source>
        <translation>当前版本0.0.0.0</translation>
    </message>
    <message>
        <location filename="settings/qaboutwidget.ui" line="291"/>
        <source>Updates</source>
        <oldsource>Check for
 updates</oldsource>
        <translation>检查更新</translation>
    </message>
    <message>
        <location filename="settings/qaboutwidget.ui" line="338"/>
        <source>Features</source>
        <translation>功能介绍</translation>
    </message>
    <message>
        <source>当前版本:</source>
        <translation type="vanished">Current Version:</translation>
    </message>
    <message>
        <location filename="settings/qaboutwidget.cpp" line="37"/>
        <source>Current Version:</source>
        <translation>当前版本：</translation>
    </message>
</context>
<context>
    <name>QAccWidget</name>
    <message>
        <location filename="settings/qaccwidget.ui" line="26"/>
        <source>QAccWidget</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="settings/qaccwidget.ui" line="308"/>
        <source>sign out</source>
        <translation>退出登录</translation>
    </message>
    <message>
        <source>Telecomm ID：</source>
        <oldsource>interPlanet ID：</oldsource>
        <translation type="vanished">星际ID：</translation>
    </message>
    <message>
        <location filename="settings/qaccwidget.cpp" line="56"/>
        <source>ID：</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QGeneralWidget</name>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="17"/>
        <source>QGeneralWidget</source>
        <translation>一般设置</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="106"/>
        <source>Language</source>
        <translation>语言</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="138"/>
        <source>English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="148"/>
        <source>русский</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="153"/>
        <source>Հայերեն</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="158"/>
        <source>Español</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="163"/>
        <source>Português</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="168"/>
        <source>ไทย</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="236"/>
        <source>Document</source>
        <oldsource>Document
management</oldsource>
        <translation>文件管理</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="267"/>
        <source>Files received will be stored in this folder</source>
        <translation>星际通讯接收到的文件会被存放到此文件夹中</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="391"/>
        <source>Change</source>
        <translation>更改</translation>
    </message>
    <message>
        <source>Files received by OpenPlanet will be stored in
this folder</source>
        <translation type="vanished">星际通讯接收到的文件会被存放到此文件夹中</translation>
    </message>
    <message>
        <source>change</source>
        <oldsource>change</oldsource>
        <translation type="vanished">更改</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="432"/>
        <source>Open folder</source>
        <translation>打开文件夹</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.ui" line="143"/>
        <source>简体中文</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.cpp" line="54"/>
        <source>Select folder</source>
        <translation>选择文件夹</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.cpp" line="61"/>
        <location filename="settings/qgeneralwidget.cpp" line="67"/>
        <source>Warning</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.cpp" line="61"/>
        <source>Do not select the same folder！</source>
        <translation>请勿选择相同文件夹！</translation>
    </message>
    <message>
        <source>Restart Telecomm to take effect</source>
        <translation type="vanished">重启以生效</translation>
    </message>
    <message>
        <source>  Telecomm will restart for the language  
settings to take effect</source>
        <translation type="vanished">星际通讯将会重启 以使语言设置生效</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.cpp" line="67"/>
        <source>Restart OpenPlanet to take effect</source>
        <translation>重启星际通讯生效</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.cpp" line="99"/>
        <source>Notice</source>
        <translation>通知</translation>
    </message>
    <message>
        <location filename="settings/qgeneralwidget.cpp" line="99"/>
        <source>  OpenPlanet will restart for the language  
settings to take effect</source>
        <translation>星际通讯将会重启以使语言设置生效</translation>
    </message>
    <message>
        <source>Effective after restart!</source>
        <translation type="vanished">重启后生效！</translation>
    </message>
</context>
<context>
    <name>QSetKeyWidget</name>
    <message>
        <location filename="settings/qsetkeywidget.ui" line="14"/>
        <source>QSetKeyWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qsetkeywidget.ui" line="36"/>
        <source>Please enter a new shortcut on the keyboard</source>
        <translation>请直接在键盘上输入新的快捷键</translation>
    </message>
    <message>
        <location filename="settings/qsetkeywidget.ui" line="83"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="settings/qsetkeywidget.ui" line="99"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="settings/qsetkeywidget.ui" line="46"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qsetkeywidget.cpp" line="69"/>
        <source>No</source>
        <translation>无</translation>
    </message>
</context>
<context>
    <name>QShortCutWidget</name>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="14"/>
        <source>QShortCutWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="106"/>
        <source>Send message</source>
        <translation>发送消息</translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="137"/>
        <source>Enter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="142"/>
        <source>Ctrl+Enter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="204"/>
        <source>Screen capture</source>
        <translation>截取屏幕</translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="331"/>
        <source>Open hotkey</source>
        <translation>打开热键</translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="455"/>
        <source>Detect hotkey</source>
        <translation>检测热键</translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="465"/>
        <source>Remind me when hotkey conflicts</source>
        <oldsource>Reminder when hotkey conflicts</oldsource>
        <translation>热键与其他软件冲突时提醒</translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="509"/>
        <source>Restore default</source>
        <translation>恢复默认快捷键</translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="229"/>
        <source>Alt+A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.ui" line="356"/>
        <source>Ctrl+Alt+W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.cpp" line="60"/>
        <location filename="settings/qshortcutwidget.cpp" line="87"/>
        <source>Type a new shortcut</source>
        <translation>输入快捷键</translation>
    </message>
    <message>
        <location filename="settings/qshortcutwidget.cpp" line="145"/>
        <location filename="settings/qshortcutwidget.cpp" line="149"/>
        <location filename="settings/qshortcutwidget.cpp" line="204"/>
        <location filename="settings/qshortcutwidget.cpp" line="208"/>
        <source>none</source>
        <translation>无</translation>
    </message>
</context>
<context>
    <name>QThemeSwitchWidget</name>
    <message>
        <location filename="settings/qthemeswitchwidget.ui" line="17"/>
        <source>QGeneralWidget</source>
        <translation>一般设置</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="obsolete">语言</translation>
    </message>
    <message>
        <location filename="settings/qthemeswitchwidget.ui" line="106"/>
        <source>Theme Style</source>
        <translation>主题样式</translation>
    </message>
    <message>
        <location filename="settings/qthemeswitchwidget.ui" line="138"/>
        <location filename="settings/qthemeswitchwidget.cpp" line="35"/>
        <location filename="settings/qthemeswitchwidget.cpp" line="37"/>
        <location filename="settings/qthemeswitchwidget.cpp" line="59"/>
        <location filename="settings/qthemeswitchwidget.cpp" line="71"/>
        <source>Light color</source>
        <translation>浅色</translation>
    </message>
    <message>
        <location filename="settings/qthemeswitchwidget.ui" line="143"/>
        <location filename="settings/qthemeswitchwidget.cpp" line="33"/>
        <location filename="settings/qthemeswitchwidget.cpp" line="57"/>
        <location filename="settings/qthemeswitchwidget.cpp" line="69"/>
        <source>Dark color</source>
        <translation>深色</translation>
    </message>
    <message>
        <location filename="settings/qthemeswitchwidget.cpp" line="52"/>
        <source>Notice</source>
        <translation>通知</translation>
    </message>
    <message>
        <location filename="settings/qthemeswitchwidget.cpp" line="52"/>
        <source>The program will restart to switch theme, Y or N?</source>
        <translation>程序重启生效，是否继续？</translation>
    </message>
</context>
<context>
    <name>ReadConfig</name>
    <message>
        <location filename="readconfig.cpp" line="18"/>
        <source>xml</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SettingsWidget</name>
    <message>
        <location filename="settings/settingswidget.ui" line="20"/>
        <location filename="settings/settingswidget.ui" line="157"/>
        <source>Setting</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="settings/settingswidget.cpp" line="45"/>
        <source>Account settings</source>
        <translation>帐号设置</translation>
    </message>
    <message>
        <location filename="settings/settingswidget.cpp" line="46"/>
        <source>General settings</source>
        <translation>通用设置</translation>
    </message>
    <message>
        <location filename="settings/settingswidget.cpp" line="47"/>
        <source>Shortcut button</source>
        <translation>快捷按键</translation>
    </message>
    <message>
        <location filename="settings/settingswidget.cpp" line="48"/>
        <source>Theme swithing</source>
        <translation>主题切换</translation>
    </message>
    <message>
        <location filename="settings/settingswidget.cpp" line="49"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
</context>
<context>
    <name>UserInfoWidget</name>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="31"/>
        <location filename="userprofile/userinfowidget.ui" line="141"/>
        <source>Profile Edit</source>
        <oldsource>Profile Eit</oldsource>
        <translation>资料编辑</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="234"/>
        <source>Nickname</source>
        <translation>昵称</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="279"/>
        <source>Signature</source>
        <translation>签名</translation>
    </message>
    <message>
        <source>Sex</source>
        <translation type="vanished">性别</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="349"/>
        <source>Gender</source>
        <translation>性别</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="363"/>
        <location filename="userprofile/userinfowidget.cpp" line="109"/>
        <location filename="userprofile/userinfowidget.cpp" line="122"/>
        <source>Male</source>
        <translation>男</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="368"/>
        <location filename="userprofile/userinfowidget.cpp" line="111"/>
        <location filename="userprofile/userinfowidget.cpp" line="124"/>
        <source>Female</source>
        <translation>女</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="424"/>
        <source>Phone Number</source>
        <translation>电话号码</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="469"/>
        <source>E-mail</source>
        <oldsource>E_mail</oldsource>
        <translation>电子邮箱</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="519"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <source>edit information</source>
        <translation type="vanished">编辑资料</translation>
    </message>
    <message>
        <source>nickname</source>
        <translation type="vanished">昵  称</translation>
    </message>
    <message>
        <source>signature</source>
        <translation type="vanished">签  名</translation>
    </message>
    <message>
        <source>sex</source>
        <translation type="vanished">性  别</translation>
    </message>
    <message>
        <source>male</source>
        <translation type="vanished">男</translation>
    </message>
    <message>
        <source>female</source>
        <translation type="vanished">女</translation>
    </message>
    <message>
        <source>Mobile phone</source>
        <translation type="vanished">手  机</translation>
    </message>
    <message>
        <source>mailbox</source>
        <translation type="vanished">邮  箱</translation>
    </message>
    <message>
        <source>save</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <location filename="userprofile/userinfowidget.ui" line="532"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>UserProfileWidget</name>
    <message>
        <location filename="userprofile/userprofilewidget.ui" line="14"/>
        <source>UserProfileWidget</source>
        <translation>用户资料</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.ui" line="169"/>
        <source>Account</source>
        <oldsource>account</oldsource>
        <translation>账号</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.ui" line="217"/>
        <source>Nickname</source>
        <oldsource>nickname</oldsource>
        <translation>昵称</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.ui" line="273"/>
        <source>ID</source>
        <translation></translation>
    </message>
    <message>
        <source>InterPlanet ID</source>
        <translation type="vanished">星际ID</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.ui" line="323"/>
        <source>E-mail</source>
        <oldsource>mailbox</oldsource>
        <translation>邮箱</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.ui" line="377"/>
        <source>QR code card</source>
        <translation>二维码名片</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.ui" line="430"/>
        <source>Receive stranger news</source>
        <oldsource>stranger news</oldsource>
        <translation>接收陌生人消息</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.cpp" line="18"/>
        <source>personal information</source>
        <translation>个人资料</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.cpp" line="27"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.cpp" line="33"/>
        <source>Modify avatar</source>
        <translation>修改头像</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.cpp" line="40"/>
        <source>Modify information</source>
        <translation>修改资料</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.cpp" line="184"/>
        <source>Scan the QR code on the top and add me as a friend.</source>
        <translation>扫描上面的二维码图案，加我为好友。</translation>
    </message>
    <message>
        <location filename="userprofile/userprofilewidget.cpp" line="185"/>
        <source>ID:</source>
        <translation></translation>
    </message>
    <message>
        <source>InterPlanet ID:</source>
        <translation type="vanished">星际ID:</translation>
    </message>
</context>
<context>
    <name>xSysTrayMeauAction</name>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">退出</translation>
    </message>
</context>
</TS>
