﻿#include "hotkeymanager.h"
#include <QSettings>
#include "qxtglobalshortcut.h"
#include <QFileInfo>
#include <QDir>
#include <QStandardPaths>
#ifndef Q_OS_WIN
#include "inline_mac.h"
#endif

HotKeyManager::HotKeyManager(QObject *parent)
	: QObject(parent)
{
	m_shortcutPic = NULL;
	m_shortcutOpen = NULL;
	m_strScreenShot = "";
	m_iSend = 0;

#ifdef Q_OS_WIN
	QDir dir;
	QString currentPath = dir.currentPath();
#else
	QString currentPath = getResourcePath();
#endif
	currentPath += "/config/Settings.ini";
	QFileInfo file(currentPath);
	if (file.exists() == true)
	{
		QSettings * iniReader = new QSettings(currentPath, QSettings::IniFormat);
		m_strScreenShot = iniReader->value("/ScreenShot/Key").toString();
		m_iSend = iniReader->value("/SendMsg/Key").toInt();
		m_strUserPath = iniReader->value("/UserPath/Path").toString();
		m_strQuickOpen = iniReader->value("/QuickOpen/Key").toString();
		m_iCheckKey = iniReader->value("/CheckKey/Key").toInt();
		QFileInfo dir(m_strUserPath);
		if (!dir.isWritable())
		{
			m_strUserPath = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + "/OpenPlanet";
		}
	}
	else
	{
		QSettings * iniReader = new QSettings(currentPath, QSettings::IniFormat);
#ifdef Q_OS_WIN
		m_strScreenShot = "Ctrl+Shift+Z";
		m_strQuickOpen = "Ctrl+Shift+w";
#else
        m_strScreenShot = "Alt+A";
        m_strQuickOpen = "Ctrl+Shift+w";
#endif
		m_iSend = 2;
		m_strUserPath = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)+"/OpenPlanet";
		m_iSend = 0;
#ifdef Q_OS_WIN
		m_strUserPath = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)+"/OpenPlanet";
#else
        m_strUserPath = getResourcePath();
#endif
		iniReader->setValue("/ScreenShot/Key", m_strScreenShot);
		iniReader->setValue("/QucikOpen/Key", m_strQuickOpen);
		iniReader->setValue("/SendMsg/Key", m_iSend);
		iniReader->setValue("/UserPath/Path", m_strUserPath);
	}
}

HotKeyManager::~HotKeyManager()
{
	if (m_shortcutPic)
	{
		delete m_shortcutPic;
		m_shortcutPic = NULL;
	}
	if (m_shortcutOpen)
	{
		delete m_shortcutOpen;
		m_shortcutOpen = NULL;
	}
}

bool HotKeyManager::InitScreenShot()
{
#ifdef Q_OS_WIN
	m_shortcutPic = new QxtGlobalShortcut(this);
	connect(m_shortcutPic, SIGNAL(activated()), this, SIGNAL(sigGlobalScreenShot()));
	bool bEn = m_shortcutPic->setShortcut(QKeySequence(m_strScreenShot));
#else
	m_shortcutPic = new QxtGlobalShortcut(this);
    connect(m_shortcutPic, SIGNAL(activated()), this, SIGNAL(sigGlobalScreenShot()));
	bool bEn = m_shortcutPic->setShortcut(QKeySequence(m_strScreenShot));
#endif
	return bEn;
}

void HotKeyManager::setScreenShot(QString strValue)
{
	bool bEn = m_shortcutPic->setShortcut(QKeySequence(strValue));
	m_strScreenShot = strValue;
#ifdef Q_OS_WIN
	QDir dir;
	QString currentPath = dir.currentPath();
#else
	QString currentPath = getResourcePath();
#endif
	currentPath += "/config/Settings.ini";
	QSettings * iniReader = new QSettings(currentPath, QSettings::IniFormat);
	iniReader->setValue("/ScreenShot/Key", m_strScreenShot);
}

bool HotKeyManager::InitQuickOpen()
{
	m_shortcutOpen = new QxtGlobalShortcut(this);
	connect(m_shortcutOpen, SIGNAL(activated()), this, SIGNAL(sigQuickOpen()));
	bool bEn = m_shortcutOpen->setShortcut(QKeySequence(m_strQuickOpen));
	return bEn;
}

void HotKeyManager::setQuickOpen(QString strValue)
{
	bool bEn = m_shortcutOpen->setShortcut(QKeySequence(strValue));
	m_strQuickOpen = strValue;
#ifdef Q_OS_WIN
	QDir dir;
	QString currentPath = dir.currentPath();
#else
	QString currentPath = getResourcePath();
#endif
	currentPath += "/config/Settings.ini";
	QSettings * iniReader = new QSettings(currentPath, QSettings::IniFormat);
	iniReader->setValue("/QuickOpen/Key", m_strQuickOpen);
}

QString HotKeyManager::getQuickOpen()
{
	return m_strQuickOpen;
}

void HotKeyManager::setSendMeg(int iValue)
{
	m_iSend = iValue;
#ifdef Q_OS_WIN
	QDir dir;
	QString currentPath = dir.currentPath();
#else
	QString currentPath = getResourcePath();
#endif
	currentPath += "/config/Settings.ini";
	QSettings * iniReader = new QSettings(currentPath, QSettings::IniFormat);
	iniReader->setValue("/SendMsg/Key", m_iSend);
}

void HotKeyManager::setCheckKey(int iValue)
{
	m_iCheckKey = iValue;
#ifdef Q_OS_WIN
	QDir dir;
	QString currentPath = dir.currentPath();
#else
	QString currentPath = getResourcePath();
#endif
	currentPath += "/config/Settings.ini";
	QSettings * iniReader = new QSettings(currentPath, QSettings::IniFormat);
	iniReader->setValue("/CheckKey/Key", m_iCheckKey);
}


QString HotKeyManager::getScreenShot()
{
	return m_strScreenShot;
}
int HotKeyManager::getSendMeg()
{
	return m_iSend;
}
int HotKeyManager::getCheckKey()
{
	return m_iCheckKey;
}

void HotKeyManager::ChangeNewPath()
{	//判断是否是旧版本 用户文件保存在程序目录下
	//判断文件路径是否存在
#ifdef Q_OS_WIN
	QDir dir;
	QString currentPath = dir.currentPath();
	//复制到目标 删除
	bool bSuc = false;
	//剪贴板
	bSuc = copyUserDir(currentPath + "/Clipboard", m_strUserPath + "/Clipboard", false);
	if (bSuc)DelDir(currentPath + "/Clipboard");
	//截屏
	bSuc = copyUserDir(currentPath + "/ScreenShot", m_strUserPath + "/ScreenShot", false);
	if (bSuc)DelDir(currentPath + "/ScreenShot");
	//database
	bSuc = copyUserDir(currentPath + "/database", m_strUserPath + "/database", false);
	if (bSuc)DelDir(currentPath + "/database");
	//file
	bSuc = copyUserDir(currentPath + "/files", m_strUserPath + "/files", false);
	if (bSuc)DelDir(currentPath + "/files");
	//resource
	bSuc = copyUserDir(currentPath + "/resource", m_strUserPath + "/resource", false);
	if (bSuc)DelDir(currentPath + "/resource");
	//wallet
	bSuc = copyUserDir(currentPath + "/wallet", m_strUserPath + "/wallet", false);
	if (bSuc)DelDir(currentPath + "/wallet");
#else
#endif 
}
bool HotKeyManager::copyUserDir(QString oldPath , QString newPath,bool bCover)
{
	QDir sourceDir(oldPath);
	QDir targetDir(newPath);
	if (!targetDir.exists()) {    /**< 如果目标目录不存在，则进行创建 */
		if (!targetDir.mkpath(targetDir.absolutePath()))
			return false;
	}

	QFileInfoList fileInfoList = sourceDir.entryInfoList();
	foreach(QFileInfo fileInfo, fileInfoList) {
		if (fileInfo.fileName() == "." || fileInfo.fileName() == "..")
			continue;

		if (fileInfo.isDir()) {    /**< 当为目录时，递归的进行copy */
			if (!copyUserDir(fileInfo.filePath(),
				targetDir.filePath(fileInfo.fileName()),
				bCover))
				return false;
		}
		else {            /**< 当允许覆盖操作时，将旧文件进行删除操作 */
			if (bCover && targetDir.exists(fileInfo.fileName())) {
				targetDir.remove(fileInfo.fileName());
			}

			/// 进行文件copy
			if (!QFile::copy(fileInfo.filePath(),
				targetDir.filePath(fileInfo.fileName()))) {
				return false;
			}
		}
	}
	return true; 
}
bool HotKeyManager::DelDir(QString strPath)
{
		if (strPath.isEmpty()) {
			return false;
		}
		QDir dir(strPath);
		if (!dir.exists()) {
			return true;
		}
		dir.setFilter(QDir::AllEntries | QDir::NoDotAndDotDot); //设置过滤
		QFileInfoList fileList = dir.entryInfoList(); // 获取所有的文件信息
		foreach(QFileInfo file, fileList) { //遍历文件信息
			if (file.isFile()) { // 是文件，删除
				file.dir().remove(file.fileName());
			}
			else { // 递归删除
				DelDir(file.absoluteFilePath());
			}
		}
		return dir.rmpath(dir.absolutePath()); // 删除文件夹
}

QString HotKeyManager::getUserFileSize(QString strId)
{
	double dSize = 0;
	dSize += getDirSize(m_strUserPath + "/Clipboard");
	dSize += getDirSize(m_strUserPath + "/ScreenShot");
	dSize += getDirSize(m_strUserPath + "/files/"+ strId);
	dSize += getDirSize(m_strUserPath + "/resource/photo/"+ strId);
	dSize += getDirSize(m_strUserPath + "/resource/vedio/"+ strId);
	dSize += getDirSize(m_strUserPath + "/resource/audio/"+ strId);
	QFileInfo fileInfo(m_strUserPath + "/database/" + strId + "/message.db");
	dSize += fileInfo.size();

	char unit = 'B';
	double curSize = dSize;    //这个目录的大小

	if (curSize > 1024)
	{
		//当前的大小比1024个字节还大，上面计数是按字节大小得到的fileInfo.size()
		curSize /= 1024;  //除
		unit = 'K';   //KB

		if (curSize > 1024)
		{
			//还大
			curSize /= 1024;
			unit = 'M';   //MB

			if (curSize > 1024)
			{
				curSize /= 1024;
				unit = 'G';   //GB
			} //if
		}   //if
	} //if
	QString strSize = QString::number(curSize) + unit;

	return strSize;
}
double HotKeyManager::getDirSize(QString strPath)
{
	QDir dir(strPath); //这个类可以提供文件的目录和类容

	double size = 0;
	//得到文件目录下的所有的文件和目录
	foreach(QFileInfo fileInfo, dir.entryInfoList(QDir::Files))
	{
		size += fileInfo.size();    //把所有文件的大小加起来
	}

	//得到所有子目录下文件的大小
	//列出目录列表，不列出特殊的条目，“.”和".."
	foreach(QString subDir, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot))
	{
		//路径+当前系统下的分割符+目录名
		size += getDirSize(strPath + QDir::separator() + subDir);    //递归调用，遍历所有目录，深度优先
	}
	return size;
}
void HotKeyManager::DelUserFile(QString strId)
{	
	DelDir(m_strUserPath + "/Clipboard");
	DelDir(m_strUserPath + "/ScreenShot");
	//QString strDbPath = m_strUserPath + "/database/" + strId + "/message.db";
	//QFile::remove(strDbPath);
	DelDir(m_strUserPath + "/files/"+ strId);
	DelDir(m_strUserPath + "/resource/audio/"+ strId);
	DelDir(m_strUserPath + "/resource/photo/"+ strId);
	DelDir(m_strUserPath + "/resource/vedio/"+ strId);
}
void HotKeyManager::DelTempUserFile()
{
	DelDir(m_strUserPath + "/Clipboard");
	DelDir(m_strUserPath + "/ScreenShot");
}
QString HotKeyManager::getUserPath()
{
	return m_strUserPath;
}
void HotKeyManager::setUserPath(QString strParh)
{
	m_strUserPath = strParh;
#ifdef Q_OS_WIN
	QDir dir;
	QString currentPath = dir.currentPath();
#else
	QString currentPath = getResourcePath();
#endif
	currentPath += "/config/Settings.ini";
	QSettings * iniReader = new QSettings(currentPath, QSettings::IniFormat);
	iniReader->setValue("/UserPath/Path", m_strUserPath);
}
