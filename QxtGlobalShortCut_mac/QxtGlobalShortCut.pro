#-------------------------------------------------
#
# Project created by QtCreator 2018-08-22T14:49:29
#
#-------------------------------------------------

QT       += core gui widgets

include(shared.pri)

QMAKE_LFLAGS += -framework Carbon -framework CoreFoundation

INCLUDEPATH += ../SharedLib_mac/common

HEADERS += \
    qxtglobal.h \
    qxtglobalshortcut_p.h \
    qxtglobalshortcut.h \
    qxtwindowsystem_mac.h \
    qxtwindowsystem.h \
    hotkeymanager.h
SOURCES += \
    qxtglobal.cpp \
    qxtglobalshortcut_mac.cpp \
    qxtglobalshortcut.cpp \
    qxtwindowsystem_mac.cpp \
    qxtwindowsystem.cpp \
    hotkeymanager.cpp

