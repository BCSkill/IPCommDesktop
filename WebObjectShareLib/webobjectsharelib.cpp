#include "webobjectsharelib.h"


WebObjectShareLib::WebObjectShareLib(QObject *parent) : QObject(parent)
{
}

//PWR 恢复钱包
void WebObjectShareLib::slotRecoveryResult(QString strReuslt)
{
    emit sigRecoveryResult(strReuslt);
}

void WebObjectShareLib::slotZoomImg(QString strReuslt)
{
    emit sigZoomImg(strReuslt);
}

void WebObjectShareLib::slotLogImg(QString strReuslt)
{
	emit sigZoomImg(strReuslt);
}

void WebObjectShareLib::slotVideoPlay(QString strReuslt)
{
    emit sigVideoPlay(strReuslt);
}

void WebObjectShareLib::slotOpenFile(QString strReuslt)
{
    emit sigOpenFile(strReuslt);
}
void WebObjectShareLib::slotOpenDocument(QString strReuslt)
{
    emit sigOpenDocument(strReuslt);
}
void WebObjectShareLib::slotCancleLoadorDownLoad(QString MsgId)
{
    emit sigCancleLoadorDownLoad(MsgId);
}
void WebObjectShareLib::slotSendFileByID(QString strReuslt)
{
    emit sigSendFileByID(strReuslt);
}
void WebObjectShareLib::slotGetFile(QString strReuslt)
{
    emit sigGetFile(strReuslt);
}
void WebObjectShareLib::slotSaveFile(QString strReuslt)
{
    emit sigSaveFile(strReuslt);
}
void WebObjectShareLib::slotDrag(QStringList strReuslt)
{
    emit sigDrag(strReuslt);
}
void WebObjectShareLib::slotOpenUrl(QString strReuslt)
{
    emit sigOpenUrl(strReuslt);
}
void WebObjectShareLib::slotOpenSecret(QString strReuslt)
{
    emit sigOpenSecret(strReuslt);
}
void WebObjectShareLib::slotMsgID(QString strReuslt)
{
    emit sigMsgID(strReuslt);
}

void WebObjectShareLib::slotOpenDir(QString strReuslt)
{
    emit sigOpenDir(strReuslt);
}

void WebObjectShareLib::slotTransactionData(QString strReuslt)
{
    emit sigTransactionData(strReuslt);
}

void WebObjectShareLib::slotSendFile(QString strReuslt)
{
    emit sigSendFile(strReuslt);
}

void WebObjectShareLib::slotOpenGroupFile(QString strReuslt)
{
    emit sigOpenGroupFile(strReuslt);
}

void WebObjectShareLib::slotClickUserHeader(QString strReuslt)
{
    emit sigClickUserHeader(strReuslt);
}

void WebObjectShareLib::slotLocation(QString strReuslt)
{
    emit sigLocation(strReuslt);
}

void WebObjectShareLib::slotTransmit(QString strReuslt)
{
    emit sigTransmit(strReuslt);
}

void WebObjectShareLib::slotPopUpMenu(bool isShowCopy, bool isShowTransmit, QString msgId)
{
	emit sigPopUpMenu(isShowCopy, isShowTransmit,msgId);
}

void WebObjectShareLib::slotOnChatIFrameLoad(QString chatId)
{
	emit sigOnChatIFrameLoad(chatId);
}

void WebObjectShareLib::slotShowMore()
{
	emit sigShowMore();

}