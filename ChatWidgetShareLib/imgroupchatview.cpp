﻿#include "imgroupchatview.h"
#include "imgroupchatdispatcher.h"
#include "imgroupchatstore.h"
#include "imgroupchatviewmodel.h"
#include "imgroupdispatcher.h"
#include "imgroupview.h"

#include "qlabelheader.h"
#include "profilemanager.h"
#include "SearchWidget/atwidget.h"
#include "immainwidget.h"
#include "childWidget/expresswidget.h"
#include "thread/threadloadgroupuserlist.h"
#include "childWidget/groupsearchwidget.h"
#include "childWidget/groupsearchwidget.h"
#include "redPacketWidget/GroupPackWidget.h"
#include "ui_groupchatwidget.h"
#include "settingsmanager.h"
#include "childWidget/groupaddbuddywidget.h"
#include "childWidget/noticewidget.h"
#include "imvideoplayer.h"
#include "define.h"
#include "chatdatamanager.h"
#include "redPacketWidget/RedPackDetail.h"
#include "imcommon.h"

#ifdef Q_OS_WIN
#include <tchar.h>
#else
#include "oescreenshot.h"
#endif

#include <QFile>
#include <QVBoxLayout>
#include <QDragEnterEvent>
#include <QMovie>
#include <QMenu>
#include <QMimeData>
#include <QClipboard>
#include <QFileDialog>
#include <QTextBlock>
#include <QProcess>
#include <QDesktopServices>
#include <QTextDocumentFragment>

extern SettingsManager* gSettingsManager;
extern QString gI18NLocale;

extern QString gThemeStyle;

IMGroupChatView::IMGroupChatView(QString groupId, QWidget *parent) : QWidget(parent)
, mExpressWidget(NULL), thread(NULL), m_pWebObject(NULL), m_groupView((IMGroupView*)parent)
, m_bNeedManulInit(false), m_bHasStartGroupUserThread(false), m_bReloadGroupUserList(false)
, atWidget(NULL), atPosition(0), mbExpress_widget_is_showing(false), groupPackWidget(NULL)
, videoWidget(NULL), detailWidget(NULL), openPacketWidget(NULL)
{
	ui = new Ui::GroupChatWidget();
	ui->setupUi(this);

	m_groupId = groupId;
	this->setObjectName(groupId);

	m_vm = new IMGroupChatViewModel(this);
	m_dispatcher = new IMGroupChatDispatcher(this);
	m_store = new IMGroupChatStore(this);

	m_vm->init();
	m_dispatcher->init();
	m_store->init();

	initSelf();
	initWebView();
	initTextEditSend();
	initTextEditSendContextMenu();
	initConnectSelfSignals();
}

IMGroupView* IMGroupChatView::getGroupView()
{
	return m_groupView;
}

AtWidget *IMGroupChatView::getAtWidget()
{
	return atWidget;
}

GroupUserList* IMGroupChatView::getGroupUserList()
{
	return ui->mGroupUserlistWidget;
}

IMGroupChatView::~IMGroupChatView()
{
	delete ui;
}

WId IMGroupChatView::winId() const
{
	return (WId)this;
}

QString IMGroupChatView::getGroupId()
{
	return m_groupId;
}


IMGroupChatDispatcher* IMGroupChatView::dispatcher()
{
	return m_dispatcher;
}

IMGroupChatStore* IMGroupChatView::store()
{
	return m_store;
}

IMGroupChatViewModel* IMGroupChatView::vm()
{
	return m_vm;
}

QWebEngineViewDelegate *IMGroupChatView::getWebView()
{
	return ui->mGroupWebView;
}


QTextEdit* IMGroupChatView::getTextEditSend()
{
	return ui->mTextEditGroup;
}

void IMGroupChatView::doUpdateGroupMemberCount(int member_count)
{
	QString str_member_count = tr(" Members %1").arg(member_count);
	ui->mLabelGroupNum->setText(str_member_count);
}

void IMGroupChatView::removeMemberByUserId(QString strUserId)
{
	for (int i = 0; i < ui->mGroupUserlistWidget->count(); i++)
	{
		QModelIndex item = ui->mGroupUserlistWidget->item(i);
		if (ui->mGroupUserlistWidget->data(item, GroupUserListModel::UserIdRole).toString() == strUserId)
		{
			ui->mGroupUserlistWidget->takeItem(i);
			break;
		}
	}
}

void IMGroupChatView::initSelf()
{
	atWidget = new AtWidget(this);
	atWidget->setGroupID(m_groupId);
	connect(atWidget, SIGNAL(sigAtGroupUser(QString)), this, SLOT(slotAtGroupUser(QString)));
	connect(atWidget, SIGNAL(sigSendAtMessage(QString, QString)), m_dispatcher, SIGNAL(sigSendAtMessage(QString, QString)));
	atWidget->hide();


	connect(ui->mGroupUserlistWidget, SIGNAL(sigGroupListContextMenuRequested(int)), this, SLOT(slotGroupListContextMenuRequested(int)));//参数为INDEX值
	connect(ui->mGroupUserlistWidget, SIGNAL(sigItemDoubleClicked(int)), m_dispatcher, SIGNAL(sigItemDoubleClicked(int)));//参数为INDEX值

	movie = new QMovie(":/GroupChat/Resources/groupchat/refresh.gif", QByteArray(), this);
	ui->refreshBtn->installEventFilter(this);

	QFile file(":/QSS/Resources/QSS/ChatWidgetShareLib/groupchatwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	ui->mPButtonGroupFont->hide();
	ui->mPButtonGroupShake->hide();

#ifdef Q_OS_WIN
	if (IMMainWidget::self())
	{
		connect(IMMainWidget::self(), SIGNAL(sigParseScreenCutMessage(QString)), this, SLOT(slotParseScreenCutMessage(QString)));
	}
#endif

	m_menuTimer = new QTimer(this);

	if (IMMainWidget::self())
	{
		//更新HTML里的头像
		connect(IMMainWidget::self(), SIGNAL(sigUpdateBuddyHeaderImagePath(int, QString)), this, SLOT(slotUpdateHtmlBuddyHeaderImagePath(int, QString)));

		//更新HTML里的昵称
		connect(IMMainWidget::self(), SIGNAL(sigUpdateBuddyNickName(int, QString, int)), this, SLOT(slotUpdateHtmlBuddyNickName(int, QString, int)));
	}


	//截图
	connect(ui->mPButtonGroupCutPicture, SIGNAL(clicked()), this, SLOT(slotClickedCutPic()));
	//表情
	connect(ui->mPButtonGroupExpress, SIGNAL(clicked()), this, SLOT(slotClickedExpress()));
	//字体
	connect(ui->mPButtonGroupFont, SIGNAL(clicked()), this, SLOT(slotClickedFont()));
	//图片
	connect(ui->mPButtonGroupPicture, SIGNAL(clicked()), this, SLOT(slotClickedPicture()));
	//发送
	connect(ui->mPButtonGroupSend, SIGNAL(clicked()), this, SLOT(slotClickedSend()));
	//抖动
	connect(ui->mPButtonGroupShake, SIGNAL(clicked()), this, SLOT(slotClickedShake()));
	//语音
	connect(ui->mPButtonGroupRedPacket, SIGNAL(clicked()), this, SLOT(slotClickedRedPacket()));

	//消息记录。
	connect(ui->mPButtonGroupLog, SIGNAL(clicked()), this, SIGNAL(sigOpenMessageLog()));

	connect(ui->mPButtonGroupSpeak, SIGNAL(clicked()), this, SLOT(slotClickedSpeak()));

	connect(ui->mPButtonGroupFile, SIGNAL(clicked()), this, SLOT(slotGroupFile()));

	connect(ui->addBtn, SIGNAL(clicked()), this, SLOT(slotClickAdd()));

	//通告
	connect(ui->mPButtonNotice, SIGNAL(clicked()), this, SLOT(slotClickedNotice()));

	//部落成员加载线程。
	if (!thread)
	{
		thread = new ThreadLoadGroupUserList;
	}
	connect(thread, SIGNAL(sigLoadGroupUserList(BuddyInfo)), m_dispatcher, SIGNAL(sigThreadLoadGroupUserInfo(BuddyInfo)));
	connect(thread, SIGNAL(finished()), this, SLOT(slotFinished()));

	ui->mGroupUserlistWidget->installEventFilter(this);
	

	searchWidget = new GroupSearchWidget(this);
	searchWidget->setGroupID(m_groupId);
	searchWidget->hide();
	connect(ui->searchBtn, SIGNAL(toggled(bool)), this, SLOT(slotShowOrHideSearch(bool)));
	connect(searchWidget, SIGNAL(sigOpenSearchUser(QString)), this, SLOT(slotOpenSearchUser(QString)));

	sendTip = new QLabel(this);
	QPixmap tipImage(":/PerChat/Resources/person/sendtip.png");
	sendTip->resize(tipImage.size());
	sendTip->setPixmap(tipImage);
	sendTip->hide();

	noSpeakTip = new QLabel(this);
	QPixmap speakImage(":/GroupChat/Resources/groupchat/noSpeakTip.png");
	noSpeakTip->resize(speakImage.size());
	noSpeakTip->setPixmap(speakImage);
	noSpeakTip->hide();


	UserInfo info = gDataManager->getUserInfo();
	BuddyInfo user = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(m_groupId, QString::number(info.nUserID));
	if (user.nUserType == 0) //0是普通群员，1是管理员，9是群主。
	{
		ui->mPButtonGroupSpeak->hide();
		ui->addBtn->hide();
	}
}

QPointer<ExpressWidget> IMGroupChatView::getExpressWidget()
{
	if (mExpressWidget == NULL)
	{
		mExpressWidget = new ExpressWidget(this);
		rectExpressWidget = QRect(mExpressWidget->pos() + this->pos(), mExpressWidget->size());

		mExpressWidget->disconnect();
		connect(mExpressWidget, SIGNAL(sigExpressImagePath(QString)), this, SLOT(slotExpressNormalImagePath(QString)));
	}

	return mExpressWidget;
}

void IMGroupChatView::initWebView()
{
	ui->mGroupWebView->bindChatId(m_groupId);//绑定chat id

	ui->mGroupWebView->installEventFilter(this);
	ui->mGroupWebView->setContextMenuPolicy(Qt::NoContextMenu);
	ui->mGroupWebView->setAcceptDrops(false);

	QWebChannel * pWebChannel = QWebEngineViewManager::instance()->m_settingFlagsMap[ui->mGroupWebView->engine()->page()].bindWebChannel;
	WebObjectShareLib* bindChatManagerObject = QWebEngineViewManager::instance()->m_settingFlagsMap[ui->mGroupWebView->engine()->page()].bindChatManagerObject;

	if (!pWebChannel)
	{
		pWebChannel = new QWebChannel();
		bindChatManagerObject = new WebObjectShareLib();

		pWebChannel->registerObject("chat_manager", bindChatManagerObject);

		ui->mGroupWebView->page()->setWebChannel(pWebChannel);

		QWebEngineViewManager::instance()->m_settingFlagsMap[ui->mGroupWebView->engine()->page()].bindWebChannel = pWebChannel;
		QWebEngineViewManager::instance()->m_settingFlagsMap[ui->mGroupWebView->engine()->page()].bindChatManagerObject = bindChatManagerObject;
	}

	connect(bindChatManagerObject, SIGNAL(sigOnChatIFrameLoad(QString)), ui->mGroupWebView, SLOT(slotOnChatIFrameLoad(QString)));//IFRAME加载完成事件绑定

	WebObjectShareLib* pWebObject = new WebObjectShareLib(this);
	m_pWebObject = pWebObject;
	connect(pWebObject, SIGNAL(sigZoomImg(QString)), m_dispatcher, SIGNAL(sigZoomImg(QString)));
	connect(pWebObject, SIGNAL(sigVideoPlay(QString)), this, SLOT(slotVideoPlay(QString)));
	connect(pWebObject, SIGNAL(sigOpenDocument(QString)), this, SLOT(slotOpenDocument(QString)));
	connect(pWebObject, SIGNAL(sigCancleLoadorDownLoad(QString)), this, SLOT(slotCancleLoadorDownLoad(QString)));
	connect(pWebObject, SIGNAL(sigGetFile(QString)), m_dispatcher, SIGNAL(sigGetFile(QString)));
	connect(pWebObject, SIGNAL(sigSaveFile(QString)), m_dispatcher, SIGNAL(sigSaveFile(QString)));
	connect(pWebObject, SIGNAL(sigOpenGroupFile(QString)), this, SLOT(slotOpenGroupFile(QString)));
	connect(pWebObject, SIGNAL(sigSendFile(QString)), m_dispatcher, SIGNAL(sigSendFile(QString)));
	connect(pWebObject, SIGNAL(sigSendFileByID(QString)), this, SLOT(slotSendFileByID(QString)));
	connect(pWebObject, SIGNAL(sigDrag(QStringList)), m_dispatcher, SIGNAL(sigDrags(QStringList)));
	connect(pWebObject, SIGNAL(sigOpenUrl(QString)), this, SLOT(slotOpenLink(QString)));
	connect(pWebObject, SIGNAL(sigClickUserHeader(QString)), this, SLOT(slotClickUserHeader(QString)));
	connect(pWebObject, SIGNAL(sigTransmit(QString)), m_dispatcher, SIGNAL(sigTransmit(QString)));
	connect(pWebObject, SIGNAL(sigPopUpMenu(bool, bool, QString)), this, SLOT(slotPopUpMenu(bool, bool, QString)));
	connect(pWebObject, SIGNAL(sigMsgID(QString)), this, SLOT(slotMsgID(QString)));
	connect(pWebObject, SIGNAL(sigShowMore()), m_dispatcher, SIGNAL(sigLoadMore()));

	pWebChannel->registerObject(QString("chat_") + ui->mGroupWebView->chatId(), pWebObject);

	ui->mGroupWebView->page()->setWebChannel(pWebChannel);

	connect(ui->mGroupWebView, SIGNAL(renderProcessTerminated(QWebEnginePage::RenderProcessTerminationStatus, int)), this, SLOT(slotWebViewRenderProcessTerminated(QWebEnginePage::RenderProcessTerminationStatus, int)));
	connect(ui->mGroupWebView, SIGNAL(loadFinished(bool)), this, SLOT(slotWebEngineFinish(bool)));

	if (gThemeStyle == "White")
	{
		ui->mGroupWebView->page()->setBackgroundColor(QColor("#ffffff"));
	}
	else if (gThemeStyle == "Blue")
	{
		ui->mGroupWebView->page()->setBackgroundColor(QColor("#042439"));
	}

	ui->mGroupWebView->loadChatManager();
}

void IMGroupChatView::initTextEditSend()
{
	ui->mTextEditGroup->setContextMenuPolicy(Qt::NoContextMenu);
	ui->mTextEditGroup->setAcceptDrops(false);
	
	ui->mTextEditGroup->installEventFilter(this);
	ui->mTextEditGroup->setFocus();

	connect(ui->mTextEditGroup, SIGNAL(textChanged()), this, SLOT(slotChangeText()));
}

void IMGroupChatView::initTextEditSendContextMenu()
{
	m_Menu = new QMenu(this);

	ActCopy = new QAction(tr("Copy"), this);
	connect(ActCopy, SIGNAL(triggered()), this, SLOT(slotCopy()));

	ActCut = new QAction(tr("Cut"), this);
	connect(ActCut, SIGNAL(triggered()), this, SLOT(slotCut()));

	ActPaste = new QAction(tr("Paste"), this);
	connect(ActPaste, SIGNAL(triggered()), this, SLOT(slotPaste()));

	ActDel = new QAction(tr("Delete"), this);
	connect(ActDel, SIGNAL(triggered()), this, SLOT(slotDel()));

	QFile style(":/QSS/Resources/QSS/ChatWidgetShareLib/imgroupchatviewmenu.qss");
	style.open(QFile::ReadOnly);
	QString sheet = QLatin1String(style.readAll());
	m_Menu->setStyleSheet(sheet);
	style.close();
	//m_Menu->setStyleSheet("QMenu{background-color: #72a4d6;color: #042439;}QMenu::item{padding:4px;padding-left:20px;padding-right:30px;}QMenu::item:selected{color: white;border:none;padding:4px;padding-left:20px;padding-right:30px;}");
}

void IMGroupChatView::initConnectSelfSignals()
{
	connect(this, SIGNAL(sigGroupBuddyPerChat(QString)), this, SLOT(slotGroupBuddyPerChat(QString)));
	connect(this, SIGNAL(sigShowNormalWindow()), this, SLOT(slotShowNormalWindow()));

	connect(this, SIGNAL(sigOpenMessageLog()), this, SLOT(slotOpenGroupLog()));

	connect(m_dispatcher, SIGNAL(sigMakeGroupHeader(QString)), m_groupView->dispatcher(), SIGNAL(sigMakeGroupHeader(QString)));//TODO WJF 测试下看是否信号走通了功能是否正常
	connect(m_dispatcher, SIGNAL(sigUpdateSelfMessage(bool, QVariant, QString, bool, QMap<QString, QVariant>)), m_groupView->dispatcher(), SIGNAL(sigUpdateSelfMessage(bool, QVariant, QString, bool, QMap<QString, QVariant>)));
	connect(m_dispatcher, SIGNAL(sigKeyUpDown(QKeyEvent *)), m_groupView->dispatcher(), SIGNAL(sigKeyUpDown(QKeyEvent *)));

	connect(m_dispatcher, SIGNAL(sigTransmit(QString)), m_groupView->dispatcher(), SIGNAL(sigTransmit(QString)));
	connect(m_dispatcher, SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget *)), m_groupView->dispatcher(), SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget *)));
	connect(m_dispatcher, SIGNAL(sigHostingCharge(int, QString, QString, QString)), m_groupView->dispatcher(), SIGNAL(sigHostingCharge(int, QString, QString, QString)));
}

bool IMGroupChatView::eventFilter(QObject *obj, QEvent *e)
{
	if (obj == ui->mGroupWebView && e->type() == QEvent::Drop)
	{
		QDropEvent *dropEvent = (QDropEvent *)e;
		if (dropEvent->mimeData()->hasUrls())
		{
			QList<QUrl> urls = dropEvent->mimeData()->urls();
			if (urls.isEmpty())
				return true;
			//获取文件名
			foreach(QUrl url, urls)
			{
				QString file_name = url.toLocalFile();
				m_store->slotSendFile(file_name);
			}
		}

		return true;
	}
	if (obj == ui->mGroupWebView)
	{
		if (e->type() == QEvent::FocusIn)
		{
			OnCloseExpress();
		}
	}
	if (obj == ui->mTextEditGroup)
	{
		if (e->type() == QEvent::FocusIn)
		{
			OnCloseExpress();
		}
		if (e->type() == QEvent::FocusOut)
		{
			if (!atWidget->isFocusIn())
			{
				atWidget->hide();
			}
		}
		if (e->type() == QEvent::KeyPress)
		{
			QKeyEvent *event = static_cast<QKeyEvent*>(e);
			if (event->key() == Qt::Key_Return && atWidget->isVisible())
			{
				atWidget->keyEvent(event);
				return true;
			}

			if (gSettingsManager->getSendMeg())
			{
				if (event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter)
				{
					if (Qt::ControlModifier == (event->modifiers()&(~Qt::KeypadModifier)))
					{
						slotClickedSend();
						return true;
					}
				}
			}
			else
			{
				if ((event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter) && (event->modifiers() == 0 || event->modifiers() == Qt::KeypadModifier))
				{
					slotClickedSend();
					return true;
				}
			}
			if ((event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter) &&
				(Qt::ControlModifier == (event->modifiers()&(~Qt::KeypadModifier)) || Qt::ShiftModifier == (event->modifiers()&(~Qt::KeypadModifier))))
			{
				ui->mTextEditGroup->textCursor().insertText("\n");
				return true;
			}

			if (event->key() == Qt::Key_V && (event->modifiers() == Qt::ControlModifier))
			{
				QClipboard *board = QApplication::clipboard();
				const QMimeData *mimeData = board->mimeData();

				if (mimeData->hasUrls())
				{
					QList<QUrl> urls = mimeData->urls();
					foreach(QUrl url, urls)
					{
						QString filePath = url.toLocalFile();
						if (filePath.isEmpty())
						{
							//本地文件不存在，说明可能是文本内容
							if (mimeData->hasText())
							{
								QString text = mimeData->text();
								ui->mTextEditGroup->textCursor().insertText(text);
							}

							return true;
						}

						//如果是文件夹，则直接返回
						if (QFileInfo(filePath).isDir())
						{
							return true;
						}
#ifdef Q_OS_WIN
						filePath.remove("file:///");
#else
						filePath.remove("file://");
#endif
						QStringList types;
						types << ".png" << ".gif" << ".bmp" << ".jpg" << ".jpeg";
						bool hasSend = false;
						foreach(QString type, types)
						{
							if (filePath.toLower().endsWith(type))
							{
								InsertTextEditPic(filePath);
								hasSend = true;
								break;
							}
						}

						if (!hasSend)
							emit m_dispatcher->sigSendFile(filePath);
					}

					return true;
				}
				if (mimeData->hasHtml())
				{
					QString html = mimeData->html();

					QTextDocument doc;
					doc.setHtml(html);
					QString strTmp = doc.toPlainText();
					QChar qc = 65532;
					strTmp.replace(qc, "");
					if (!strTmp.isEmpty())
					{
						ui->mTextEditGroup->textCursor().insertText(strTmp);
					}
					else
					{
						QString html = mimeData->html();

						QTextDocument doc;
						doc.setHtml(html);
						QString strTmp = doc.toPlainText();
						QChar qc = 65532;
						strTmp.replace(qc, "");
						if (!strTmp.isEmpty())
						{
							ui->mTextEditGroup->textCursor().insertText(strTmp);
						}
						else
						{
							QTextDocumentFragment fragment;
							fragment = QTextDocumentFragment::fromHtml(html);
							ui->mTextEditGroup->textCursor().insertFragment(fragment);
						}
					}
					return true;
				}
				if (mimeData->hasImage())
				{
					QImage image = qvariant_cast<QImage>(mimeData->imageData());

					QString imageID = QString::number(QDateTime::currentDateTime().toTime_t());
#ifdef Q_OS_WIN
					QString strPath = gSettingsManager->getUserPath() + "/Clipboard/" + imageID + ".png";
#else
					QString strPath = gSettingsManager->getUserPath() + "/Clipboard/" + imageID + ".png";
#endif
					QDir fileDir = QFileInfo(strPath).absoluteDir();
					QString strFileDir = QFileInfo(strPath).absolutePath();
					if (!fileDir.exists())
						fileDir.mkpath(strFileDir);

					image.save(strPath, "PNG");
					InsertTextEditPic(strPath);

					return true;
				}
				if (mimeData->hasText())
				{
					QString text = mimeData->text();
					ui->mTextEditGroup->textCursor().insertText(text);

					return true;
				}
			}

			if (event->key() == Qt::Key_Up || event->key() == Qt::Key_Down)
			{
				if (atWidget->isVisible())
				{
					atWidget->keyEvent(event);
					return true;
				}
				else
				{
					QString text = ui->mTextEditGroup->toPlainText();
					QString strText = ui->mTextEditGroup->toPlainText();
					if (!text.contains("\n") && strText.isEmpty())
						emit m_dispatcher->sigKeyUpDown(event);
				}
			}
		}

	}
	if (obj == ui->mGroupUserlistWidget)
	{
		if (e->type() == QEvent::FocusIn)
		{
			OnCloseExpress();
		}
	}
	if (obj == ui->refreshBtn)
	{
		if (e->type() == QEvent::MouseButtonPress)
		{
			if (ui->refreshBtn->movie())
			{
				if (ui->refreshBtn->movie()->state() != QMovie::Running)
				{
					ui->refreshBtn->setMovie(movie);
					movie->start();

					ReLoadGroupUserList();
				}
			}
			else
			{
				ui->refreshBtn->setMovie(movie);
				movie->start();

				ReLoadGroupUserList();
			}
		}
	}

	return QWidget::eventFilter(obj, e);
}

void IMGroupChatView::ReLoadGroupUserList()
{
	m_bReloadGroupUserList = true;

	IMRequestBuddyInfo *request = new IMRequestBuddyInfo();
	UserInfo userInfo = gDataManager->getUserInfo();
	QString url = gDataManager->getAppConfigInfo().MessageServerAddress;
	connect(request, SIGNAL(sigParseGroupBuddyInfo(QString, QList<BuddyInfo>)), this, SLOT(slotParseGroupBuddyInfo(QString, QList<BuddyInfo>)));
	request->RequestGroupBuddyInfo(url, QString::number(userInfo.nUserID), userInfo.strUserPWD, m_groupId);
}

void IMGroupChatView::slotParseGroupBuddyInfo(QString string, QList<BuddyInfo> list)
{
	movie->stop();
	ui->refreshBtn->setPixmap(QPixmap(":/GroupChat/Resources/groupchat/refresh.png"));

	gDataBaseOpera->DBDeleteGroupBuddyByID(string);
	for (int i = 0; i < list.size(); i++)
	{
		gDataBaseOpera->DBInsertGroupBuddyInfo(string, list[i]);
	}
	UserInfo info = gDataManager->getUserInfo();
	BuddyInfo user = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(m_groupId, QString::number(info.nUserID));
	if (user.nUserType == 0) //0是普通群员，1是管理员，9是群主。
	{
		ui->mPButtonGroupSpeak->hide();
		ui->addBtn->hide();
	}
	else
	{
		ui->mPButtonGroupSpeak->show();
		ui->addBtn->show();
	}
	if (thread)
	{
		thread->requestInterruption();
	}

#ifdef Q_OS_WIN
	Sleep(200);
#else
	int ms = 200;
	struct timespec ts = { ms / 1000, (ms % 1000) * 1000 * 1000 };
	nanosleep(&ts, NULL);
#endif
	StartGroupUserThread();
}


void IMGroupChatView::showEvent(QShowEvent *event)
{
	ui->mTextEditGroup->setFocus();
}

void IMGroupChatView::resizeEvent(QResizeEvent * event)
{
	if (searchWidget->isVisible())
	{
		searchWidget->clearAndHide();
		ui->searchBtn->setChecked(false);
	}

	QWidget::resizeEvent(event);
}

void IMGroupChatView::ExpressHide(QRect rect, QPoint pos)
{
	if (rect.contains(pos))
	{
		OnCloseExpress();
	}
}

void IMGroupChatView::mousePressEvent(QMouseEvent *event)
{
	QRect rect;
	rect = QRect(ui->mLabelBKTool->pos() + this->pos(), ui->mLabelBKTool->size());
	ExpressHide(rect, event->pos());
	rect = QRect(ui->mLabelGroupNum->pos() + this->pos(), ui->mLabelBKTool->size());
	ExpressHide(rect, event->pos());
	if (!rectExpressWidget.contains(event->pos()))
	{
		if (mExpressWidget != NULL)
			mExpressWidget->deleteLater();
		mbExpress_widget_is_showing = false;
	}

	QPoint eventPos = event->pos();
	QPoint TestPos = ui->mTextEditGroup->pos();
	QRect TextRect = ui->mTextEditGroup->rect();
	if ((eventPos.x() >= TestPos.x() && eventPos.x() <= TestPos.x() + TextRect.width()) &&
		(eventPos.y() >= TestPos.y() && eventPos.y() <= TestPos.y() + TextRect.height()))
	{
		m_Menu->clear();
		QString strText = ui->mTextEditGroup->textCursor().selectedText();
		if (strText != "")
		{
			m_Menu->addAction(ActCopy);
			m_Menu->addAction(ActCut);
			m_Menu->addAction(ActPaste);
			m_Menu->addAction(ActDel);
			m_Menu->exec(event->globalPos());
		}
		else
		{
			m_Menu->addAction(ActPaste);
			m_Menu->exec(event->globalPos());
		}
	}
	return QWidget::mousePressEvent(event);
}

void IMGroupChatView::slotGroupBuddyPerChat(QString strBuddyID)
{
	emit m_groupView->dispatcher()->sigShowGroupBuddyPerChat(OpenPer, QVariant(strBuddyID));
}

void IMGroupChatView::slotShowNormalWindow()
{
	emit m_groupView->dispatcher()->sigShowNormalWindow();           //窗口还原
}



void IMGroupChatView::slotOpenGroupLog()
{
	emit m_groupView->dispatcher()->sigOpenGroupLog(objectName());
}

void IMGroupChatView::insertItem(QString strUserID, QString strPicPath, QString strNickName, QString adminPicPath, bool bHasNote)
{
	ui->mGroupUserlistWidget->insertItem(strUserID, strPicPath, strNickName, adminPicPath, bHasNote);
}

void IMGroupChatView::updateGroupBuddyImagePath(int userID, QString imagePath)
{
	for (int i = 0; i < ui->mGroupUserlistWidget->count(); i++)
	{
		QModelIndex item = ui->mGroupUserlistWidget->item(i);
		if (ui->mGroupUserlistWidget->data(item, GroupUserListModel::UserIdRole).toInt() == userID)
		{
			QString headPicPath = QStringLiteral("image://HeadProvider/") + imagePath;
			ui->mGroupUserlistWidget->setData(item, headPicPath, GroupUserListModel::HeadUrlRole);
			break;
		}
	}
}

void IMGroupChatView::setNoSpeak(bool bEnable)
{
	if (bEnable)
	{
		ui->mTextEditGroup->clear();
		ui->mTextEditGroup->setPlaceholderText(tr("Group has been silenced..."));
		ui->mTextEditGroup->setReadOnly(true);

		ui->mPButtonGroupExpress->setEnabled(false);
		ui->mPButtonGroupPicture->setEnabled(false);
		ui->mPButtonGroupCutPicture->setEnabled(false);
		ui->mPButtonGroupFile->setEnabled(false);
		disconnect(m_pWebObject, SIGNAL(sigDrag(QStringList)), m_dispatcher, SIGNAL(sigDrags(QStringList)));
	}
	else
	{
		ui->mTextEditGroup->setPlaceholderText("");
		ui->mTextEditGroup->setReadOnly(false);

		ui->mPButtonGroupExpress->setEnabled(true);
		ui->mPButtonGroupPicture->setEnabled(true);
		ui->mPButtonGroupCutPicture->setEnabled(true);
		ui->mPButtonGroupFile->setEnabled(true);
		connect(m_pWebObject, SIGNAL(sigDrag(QStringList)), m_dispatcher, SIGNAL(sigDrags(QStringList)));
	}
}

void IMGroupChatView::StartGroupUserThread()    //加载groupuser到线程的方法。
{
	m_bHasStartGroupUserThread = true;

	ui->mGroupUserlistWidget->clear();
	QList<BuddyInfo> groupInfo = gDataBaseOpera->DBGetGroupBuddyInfoFromID(m_groupId);//从数据库中加载部落成员信息

	m_vm->updateGroupMemberCount(groupInfo.size());     // 更新部落成员计数到Label上

	int groupInfo_size = groupInfo.size();
	//提前下载前9个用户头像
	for (int i = 0; i < groupInfo_size; i++)
	{
		if (i == 9)
			break;
		m_store->slotThreadLoadGroupUserInfo(groupInfo[i]);
	}

	for (int i = 0; i < groupInfo_size; i++)
	{
		if (i == 9)
			break;
		groupInfo.pop_front();
	}

	if (thread)
	{
		thread->SetListBuddyInfo(groupInfo);
	}

	if (m_bNeedManulInit)
	{
		m_bNeedManulInit = true;
		m_store->InitMessageInfo();
		ui->mTextEditGroup->setFocus();
	}

	if (m_bReloadGroupUserList)
	{
		m_bReloadGroupUserList = false;
		thread->start();
	}
}

void IMGroupChatView::insertGroupUserList()
{
	if (thread)
	{
		thread->requestInterruption();
	}
#ifdef Q_OS_WIN
	Sleep(200);
#else
	int ms = 200;
	struct timespec ts = { ms / 1000, (ms % 1000) * 1000 * 1000 };
	nanosleep(&ts, NULL);
#endif
	StartGroupUserThread();
}

void IMGroupChatView::startThread()
{
	thread->start();
}

void IMGroupChatView::slotAtGroupUser(QString path)
{
	QTextCursor cursor = ui->mTextEditGroup->textCursor();//得到当前text的光标
	if (cursor.hasSelection())//如果有选中，则取消，以免受受影响
		cursor.clearSelection();
	int count = ui->mTextEditGroup->toPlainText().count() - atPosition;
	for (int i = 0; i < count + 1; i++)
	{
		cursor.deletePreviousChar();//删除前一个字符
	}
	ui->mTextEditGroup->setTextCursor(cursor);//让光标移到删除后的位置

											  //加入@图片。
	QTextImageFormat imageFormat;
	imageFormat.setName(path);
	ui->mTextEditGroup->textCursor().insertImage(imageFormat);
}

void IMGroupChatView::slotGroupListContextMenuRequested(int index)
{
	m_menuTimer->disconnect();
	m_menuTimer->stop();
	m_menuTimer->setSingleShot(true);

	connect(m_menuTimer, &QTimer::timeout, this, [this, index]() {


		QCoreApplication::processEvents();

		QModelIndex item = ui->mGroupUserlistWidget->item(index);
		if (!item.isValid())
			return;


		QMenu *popMenu = new QMenu(this);

		//获取个人信息。
		UserInfo info = gDataManager->getUserInfo();

		//获取点击的部落成员的相关信息。
		QString strBuddyID = ui->mGroupUserlistWidget->data(item, GroupUserListModel::UserIdRole).toString();;
		//如果点击的是自己，没有相关的操作。
		if (strBuddyID.toInt() == info.nUserID)
		{

		}
		else
		{
			//是其他部落成员，就获取他的信息。
			BuddyInfo buddyInfo = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(m_groupId, strBuddyID);

			//设置&取消管理员功能的判断。
			BuddyInfo user = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(m_groupId, QString::number(info.nUserID));
			if (user.nUserType == 9)  //9是群主，有权限设置或者取消管理员。
			{

				if (buddyInfo.nUserType == 1)  //1是管理员，因此增加“取消管理员”的选项。
				{
					QAction *cancelMangaer = new QAction(tr("Cancel Administrator Privileges"), this);
					connect(cancelMangaer, SIGNAL(triggered()), this, SLOT(slotCancelManager()));
					popMenu->addAction(cancelMangaer);
				}
				if (buddyInfo.nUserType == 0)  //0是普通成员，因此增加“设置管理员”的选项。
				{
					QAction *setManager = new QAction(tr("Set as Administrator"), this);
					connect(setManager, SIGNAL(triggered()), this, SLOT(slotSetManager()));
					popMenu->addAction(setManager);
				}
			}

			//查看资料项。所有的其他成员都可以查看资料。
			QAction *showProfile = new QAction(tr("View Profile"), this);
			connect(showProfile, SIGNAL(triggered()), this, SLOT(slotShowProfile()));
			popMenu->addAction(showProfile);

			//设置群备注项，自己或者其他成员都可以设置备注。
			QAction *setNote = new QAction(tr("Set Remark Name"), this);
			connect(setNote, SIGNAL(triggered()), this, SLOT(slotOpenNote()));
			popMenu->addAction(setNote);

			//移除部落成员功能的判断，当用户权限大于指定成员时，可使用踢人功能。
			if (user.nUserType > buddyInfo.nUserType)
			{
				QAction *removeBuddy = new QAction(tr("Remove Group Member"), this);
				connect(removeBuddy, SIGNAL(triggered()), this, SLOT(slotRemoveBuddy()));
				popMenu->addAction(removeBuddy);
			}
		}

		if (popMenu->actions().count() > 0)
			popMenu->exec(QCursor::pos());
		delete popMenu;

	});



	m_menuTimer->start(0);

}

void IMGroupChatView::slotRemoveBuddy()
{
	QModelIndex item = ui->mGroupUserlistWidget->currentItem();
	if (item.isValid())
	{
		QString strBuddyID = ui->mGroupUserlistWidget->data(item, GroupUserListModel::UserIdRole).toString();

		UserInfo user = gDataManager->getUserInfo();
		HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
		connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotTest(bool, QString)));
		QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/deleteGroupUser"
			+ QString("?userId=%1&passWord=%2&groupId=%3&deleteUserId=%4").arg(user.nUserID).arg(user.strUserPWD).arg(m_groupId).arg(strBuddyID);

		http->getHttpRequest(url);
	}
}

void IMGroupChatView::slotTest(bool success, QString result)
{
	if (success)
	{
		//qDebug() << result;
	}
}

void IMGroupChatView::slotOpenNote()
{
	QModelIndex item = ui->mGroupUserlistWidget->currentItem();
	if (item.isValid())
	{
		QString buddyID = ui->mGroupUserlistWidget->data(item, GroupUserListModel::UserIdRole).toString();

		//写入数据库。
		BuddyInfo buddyInfo = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(m_groupId, buddyID);
		InputBox *box = new InputBox(this);
		connect(box, SIGNAL(sigEnter(QString)), this, SLOT(slotSetNote(QString)));
		box->setEmpty(true);
		box->setLineText(buddyInfo.strNote, buddyInfo.strNickName);
		box->init(tr("Please enter your alias:"));
	}
}

void IMGroupChatView::slotSetNote(QString note)
{
	QModelIndex item = ui->mGroupUserlistWidget->currentItem();
	if (item.isValid())
	{
		QString buddyID = ui->mGroupUserlistWidget->data(item, GroupUserListModel::UserIdRole).toString();

		//写入数据库。
		BuddyInfo buddyInfo = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(m_groupId, buddyID);
		buddyInfo.strNote = note;
		gDataBaseOpera->DBInsertGroupBuddyInfo(m_groupId, buddyInfo);
		//显示备注名
		QString strTmpNote = note;
		if (note.isEmpty())
		{
			strTmpNote = buddyInfo.strNickName;
		}

		ui->mGroupUserlistWidget->setData(item, strTmpNote, GroupUserListModel::NickNameRole);

		//上传服务器，这里根据是不是自己，调用的接口不同。
		HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
		UserInfo userInfo = gDataManager->getUserInfo();
		QString url;
		if (QString::number(userInfo.nUserID) == buddyID)
		{
			//设置自己的备注。
			url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/updateOwnerNoteInGroup"
				+ QString("?userId=%1&passWord=%2&groupId=%3&note=%4").arg(userInfo.nUserID).arg(userInfo.strUserPWD).arg(m_groupId).arg(note);
		}
		else
		{
			//设置其他成员的备注。
			url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/updateOtherUserNoteInGroup"
				+ QString("?userId=%1&passWord=%2&groupId=%3&otherUserId=%4&note=%5").arg(userInfo.nUserID).arg(userInfo.strUserPWD).arg(m_groupId).arg(buddyID).arg(note);
		}

		http->getHttpRequest(url);
	}
}

void IMGroupChatView::slotShowProfile()
{
	QModelIndex item = ui->mGroupUserlistWidget->currentItem();
	if (item.isValid())
	{
		QString buddyID = ui->mGroupUserlistWidget->data(item, GroupUserListModel::UserIdRole).toString();

		emit profilemanager::getInstance()->sigCreatePerFile(buddyID);
	}
}

void IMGroupChatView::slotSetManager()
{
	QModelIndex item = ui->mGroupUserlistWidget->currentItem();
	QString headPicPath = QStringLiteral("image://HeadProvider/") + QStringLiteral(":/GroupChat/Resources/groupchat/manager.png");

	ui->mGroupUserlistWidget->setData(item, headPicPath, GroupUserListModel::AdminUrlRole);

	//获取要设置管理员的好友的id。
	QString strUserID = ui->mGroupUserlistWidget->data(item, GroupUserListModel::UserIdRole).toString();
	UserInfo user = gDataManager->getUserInfo();
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
	QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/setGroupMana"
		+ QString("?userId=%1&passWord=%2&groupId=%3&otherUserId=%4").arg(user.nUserID).arg(user.strUserPWD).arg(m_groupId).arg(strUserID);

	http->getHttpRequest(url);

	//写入数据库。
	BuddyInfo info = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(m_groupId, strUserID);
	info.nUserType = 1;  //1是管理员。
	gDataBaseOpera->DBInsertGroupBuddyInfo(m_groupId, info);
}

void IMGroupChatView::slotCancelManager()
{
	QModelIndex item = ui->mGroupUserlistWidget->currentItem();

	//更改显示。
	ui->mGroupUserlistWidget->setData(item, "", GroupUserListModel::AdminUrlRole);;  //0是普通成员。

																					 //获取要取消管理员的好友的id。
	QString strUserID = ui->mGroupUserlistWidget->data(item, GroupUserListModel::UserIdRole).toString();
	UserInfo user = gDataManager->getUserInfo();
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotTest(bool, QString)));
	QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/cancelGroupMana"
		+ QString("?userId=%1&passWord=%2&groupId=%3&otherUserId=%4").arg(user.nUserID).arg(user.strUserPWD).arg(m_groupId).arg(strUserID);

	http->getHttpRequest(url);

	//写入数据库。
	BuddyInfo info = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(m_groupId, strUserID);
	info.nUserType = 0;  //0是普通成员。
	gDataBaseOpera->DBInsertGroupBuddyInfo(m_groupId, info);
}

#ifdef Q_OS_WIN
void IMGroupChatView::slotParseScreenCutMessage(QString strMsg)
{
	QJsonParseError jsonError;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(strMsg.toUtf8(), &jsonError);
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QVariantMap result = jsonDocument.toVariant().toMap();
			QString strCMD = result["CMD"].toString();

			int widgetPtr = result["widgetPtr"].toInt();
			if (widgetPtr != (int)this)
			{
				return;//不是发给该聊天窗口的，不处理消息
			}

			if (strCMD == "ScreenCancle")
			{
				slotScreenCanclePixMap();
			}
			else if (strCMD == "ScreenShotComplete")
			{
				slotSendScreenShotPic();
			}
		}
	}
}
#endif

void IMGroupChatView::InsertTextEditPic(QString strPath)
{
	QFile file(strPath);
	if (file.open(QIODevice::ReadOnly))
	{
		QByteArray byteArray = file.readAll();
		file.close();
		QImage image = QImage::fromData(byteArray);
		if (!image.isNull())
		{
			QTextImageFormat imageFormat;

			if (image.width() > 100 || image.height() > 100)
			{
				if (image.width() > image.height())
				{
					imageFormat.setWidth(100);
				}
				else
				{
					imageFormat.setHeight(100);
				}
			}

			imageFormat.setName(strPath);
			ui->mTextEditGroup->textCursor().insertImage(imageFormat);
}
	}
}

void IMGroupChatView::slotScreenCanclePixMap()
{
	//emit sigShowNormalWindow();
#ifdef Q_OS_MAC
	OEScreenshot *act = qobject_cast<OEScreenshot*>(sender());
	if (act)
	{
		act->showNormal();
		act->close();
		emit sigShowNormalWindow();
	}
#endif
}

void IMGroupChatView::slotSendScreenShotPic()
{
#ifdef Q_OS_MAC
	OEScreenshot *act = qobject_cast<OEScreenshot*>(sender());
	if (act)
	{
		act->showNormal();
		act->close();
		emit sigShowNormalWindow();
	}
#endif
	QString imageID = QString::number(QDateTime::currentDateTime().toTime_t());
#ifdef Q_OS_WIN
	QString strPath = gSettingsManager->getUserPath() + "/Clipboard/" + imageID + ".png";
#else
	QString strPath = gSettingsManager->getUserPath() + "/Clipboard/" + imageID + ".png";
#endif
	QDir fileDir = QFileInfo(strPath).absoluteDir();
	QString strFileDir = QFileInfo(strPath).absolutePath();
	if (!fileDir.exists())
		fileDir.mkpath(strFileDir);

	QClipboard *board = QApplication::clipboard();
	const QMimeData *mimeData = board->mimeData();
	if (mimeData->hasImage())
	{
		QImage image = qvariant_cast<QImage>(mimeData->imageData());
		image.save(strPath, "PNG");
		InsertTextEditPic(strPath);
	}
}

void IMGroupChatView::slotUpdateHtmlBuddyHeaderImagePath(int buddyId, QString path)
{
	m_vm->runJs(QString("window.UpdateHtmlBuddyHeaderImagePath(\"%1\",\"%2\")").arg(QString::number(buddyId), path));
}

void IMGroupChatView::slotUpdateHtmlBuddyNickName(int buddyId, QString nickName, int iGrp)
{
	for (int i = 0; i < ui->mGroupUserlistWidget->count(); i++)
	{
		int iID = ui->mGroupUserlistWidget->data(ui->mGroupUserlistWidget->item(i), GroupUserListModel::UserIdRole).toInt();
		if (buddyId == iID)
		{
			bool bHadNote = ui->mGroupUserlistWidget->data(ui->mGroupUserlistWidget->item(i), GroupUserListModel::HasNoteRole).toBool();
			if (bHadNote == false || iGrp == m_groupId.toInt())
			{
				ui->mGroupUserlistWidget->setData(ui->mGroupUserlistWidget->item(i), nickName, GroupUserListModel::NickNameRole);
				m_vm->runJs(QString("window.UpdateHtmlBuddyNickName(\"%1\",\"%2\")").arg(QString::number(buddyId), IMCommon::jsParam(nickName)));
				if (iGrp > -1)
				{
					ui->mGroupUserlistWidget->setData(ui->mGroupUserlistWidget->item(i), true, GroupUserListModel::HasNoteRole);
				}
			}
		}
	}
}

void IMGroupChatView::OnCloseExpress()
{
	if (mExpressWidget != NULL)
		mExpressWidget->deleteLater();
	mbExpress_widget_is_showing = false;   // 更新表情窗口状态为 隐藏  wxd add
}

//截图
void IMGroupChatView::slotClickedCutPic()
{
	OnCloseExpress();

#ifdef Q_OS_WIN
	TCHAR szAppPath[MAX_PATH] = { 0 };
	_stprintf(szAppPath, L"0x%x,%d %s", IMMainWidget::self()->winId(), winId(), gSettingsManager->getLanguage().toStdWString().c_str());
	ShellExecute(NULL, L"open", L"ScreenShotTool.exe", szAppPath, NULL, SW_SHOW);
#else
	OEScreenshot *screenCut = OEScreenshot::Instance();
	connect(screenCut, SIGNAL(sigScreenComplete()), this, SLOT(slotSendScreenShotPic()));
	connect(screenCut, SIGNAL(sigScreenCanclePixMap()), this, SLOT(slotScreenCanclePixMap()));
#endif
	ui->mTextEditGroup->setFocus();
}

//表情
void IMGroupChatView::slotClickedExpress()
{
	if (getExpressWidget() != NULL)
	{
		if (!mbExpress_widget_is_showing)
		{
			mExpressWidget->showNormalEmotion(QPoint());   // 不能在 ExpressWidget 类里面 move, 因为GroupChatWidget是父窗口, 子窗口内部无法根据父窗口的位置移动 wxd note
			mExpressWidget->move(QPoint(0, ui->mLabelBKTool->y() - mExpressWidget->height()));
			mExpressWidget->show();
			mbExpress_widget_is_showing = true;
		}
		else
		{
			mExpressWidget->deleteLater();
			mbExpress_widget_is_showing = false;
		}
	}
}

//字体
void IMGroupChatView::slotClickedFont()
{
	OnCloseExpress();
}

//图片
void IMGroupChatView::slotClickedPicture()
{
	OnCloseExpress();
	QString fileName = QFileDialog::getOpenFileName(this,
		tr("Open"), ".",
		tr("Image File") + QStringLiteral("(*.bmp; *.jpeg; *.jpg; *.png; *.gif)"));
	if (!fileName.isEmpty())
	{
		InsertTextEditPic(fileName);
	}
	OnCloseExpress();
	ui->mTextEditGroup->setFocus();
}

//发送
void IMGroupChatView::slotClickedSend()
{
	OnCloseExpress();
	QString strText = ui->mTextEditGroup->toPlainText();
	if (strText.isEmpty())
	{
		if (sendTip->isHidden())
		{
			QTimer *timer = new QTimer(this);
			connect(timer, SIGNAL(timeout()), sendTip, SLOT(hide()));
			int x = ui->mTextEditGroup->pos().x() + ui->mTextEditGroup->width() - sendTip->width() - 10;
			int y = ui->mTextEditGroup->pos().y() + ui->mTextEditGroup->height() - sendTip->height() - 90;
			sendTip->move(x, y);
			sendTip->show();
			timer->setSingleShot(true);
			timer->start(3000);
		}
		return;
	}
	else
	{
		QTextDocument *document = ui->mTextEditGroup->document();
#ifdef Q_OS_WIN
		QTextBlock &currentBlock = document->begin();
#else
		QTextBlock currentBlock = document->begin();
#endif
		QTextBlock::iterator it;

		QString saveText;
		QString html;

		while (true)
		{
			//整个textEdit分为若干个block
			for (it = currentBlock.begin(); !(it.atEnd());)
			{
				QTextFragment currentFragment = it.fragment();
				QTextImageFormat newImageFormat = currentFragment.charFormat().toImageFormat();

				if (newImageFormat.isValid()) {
					// 判断出这个fragment为image
					++it;

					QString name = newImageFormat.name();
					//at字段。
					if (atWidget->isAtGroupBuddyFormat(name))
					{
						if (!saveText.isEmpty())
						{
							m_store->SendTextMessage(saveText, html);
							saveText.clear();
							html.clear();
						}

						atWidget->addAtGroupBuddy(name);
					}
					else
					{
						//判断是不是表情。
						if (name.startsWith(":/expression/Resources"))   //表情。
						{
							int len = currentFragment.length();
							for (int i = 0; i < len; i++)//相同的多个表情会导致len>1，原因不明
							{
								QString strImgPath = name.remove(":/expression/Resources");
								QString qrc_path("<img src='qrc:/expression/Resources");
								qrc_path = qrc_path + strImgPath + "'/>";
								//发送表情
								QString str_img_description = ExpressWidget::GetDescriptionByImagePath(strImgPath);
								if (atWidget->isAtGroupChating())
									atWidget->addAtChatText(str_img_description);
								else
								{
									saveText += str_img_description;
									html += qrc_path;
								}
							}
						}
						else
						{
							if (!saveText.isEmpty())
							{
								m_store->SendTextMessage(saveText, html);
								saveText.clear();
								html.clear();
							}
							//at发送
							if (atWidget->isAtGroupChating())
								atWidget->sendAtMessage();
							m_store->SendPicture(newImageFormat.name());
						}
					}

					continue;
				}
				//文字内容
				if (currentFragment.isValid())
				{
					++it;
					QString text = currentFragment.text();
					if (atWidget->isAtGroupChating())
						atWidget->addAtChatText(text);
					else
					{
						saveText += text;
						html += text;
					}
				}
			}

			currentBlock = currentBlock.next();
			if (!currentBlock.isValid())
			{
				if (!saveText.isEmpty())
					m_store->SendTextMessage(saveText, html);
				//at发送
				if (atWidget->isAtGroupChating())
					atWidget->sendAtMessage();
				break;
			}
			else
			{
				if (!saveText.isEmpty())
				{
					saveText.append("\n");
					html.append("\n");
				}
			}
		}
	}

	ui->mTextEditGroup->clear();
}
//抖动
void IMGroupChatView::slotClickedShake()
{
	OnCloseExpress();
}


void IMGroupChatView::slotCloseGivePackWidget()
{
	if (groupPackWidget)
		groupPackWidget->close();

	groupPackWidget = NULL;
}


//语音
void IMGroupChatView::slotClickedRedPacket()
{
	if (groupPackWidget)
		groupPackWidget->close();

	groupPackWidget = new GroupPackWidget;
	connect(groupPackWidget, SIGNAL(sigClose()), this, SLOT(slotCloseGivePackWidget()));
	connect(groupPackWidget, SIGNAL(sigHostingCharge(int, QString, QString, QString)), m_dispatcher, SIGNAL(sigHostingCharge(int, QString, QString, QString)));
	connect(groupPackWidget, SIGNAL(sigGivePacketData(QString)), m_dispatcher, SIGNAL(sigGivePacketData(QString)));
	groupPackWidget->setGroup(m_groupId);
	groupPackWidget->show();
}

void IMGroupChatView::slotExpressNormalImagePath(QString strPath)
{
	OnCloseExpress();

	QTextImageFormat imageFormat;
	imageFormat.setName(":/expression/Resources" + strPath);
	ui->mTextEditGroup->textCursor().insertImage(imageFormat);
	ui->mTextEditGroup->setFocus();
}

void IMGroupChatView::slotClickedSpeak()
{
	int noSpeak;
	if (ui->mPButtonGroupSpeak->isChecked())
	{
		noSpeak = 1;
		if (noSpeakTip->isHidden())
		{
			QTimer *timer = new QTimer(this);
			connect(timer, SIGNAL(timeout()), noSpeakTip, SLOT(hide()));
			int x = ui->mTextEditGroup->pos().x() + 90;
			int y = ui->mTextEditGroup->pos().y() + 5;
			noSpeakTip->move(x, y);
			noSpeakTip->show();
			timer->setSingleShot(true);
			timer->start(2000);
		}
	}
	else
	{
		noSpeak = 0;
	}
	UserInfo userInfo = gDataManager->getUserInfo();
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;

	QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/updateGroupNoSpeak"
		+ QString("?userId=%1&passWord=%2&groupId=%3&noSpeak=%4").arg(userInfo.nUserID).arg(userInfo.strUserPWD).arg(m_groupId).arg(noSpeak);

	http->getHttpRequest(url);
}

void IMGroupChatView::slotGroupFile()
{
	QString strFileName = QFileDialog::getOpenFileName(this, tr("Send"), ".", tr("All(*.*)"));

	if (!strFileName.isEmpty())
		m_store->slotSendFile(strFileName);
}

void IMGroupChatView::slotClickAdd()
{
	GroupAddBuddyWidget *addWidget = new GroupAddBuddyWidget();
	connect(addWidget, SIGNAL(sigTipMessage(int, QString, QString)), m_dispatcher, SIGNAL(sigTipMessage(int, QString, QString)));
	addWidget->setGroupID(m_groupId);
	addWidget->show();
}

void IMGroupChatView::slotClickedNotice()
{
	NoticeWidget *pWidget = new NoticeWidget(this->objectName());
	pWidget->show();

	connect(pWidget, SIGNAL(sigSendNotice(QMap<QString, QString>)), m_dispatcher, SIGNAL(sigSendNotice(QMap<QString, QString>)));
}

void IMGroupChatView::slotFinished()
{
	emit m_dispatcher->sigMakeGroupHeader(m_groupId);
}

void IMGroupChatView::slotShowOrHideSearch(bool checked)
{
	int margin = 5;
	if (checked)
	{
		QPoint pointZero(0, 0);
		int x = (ui->mGroupUserlistWidget->mapTo(this, pointZero)).x() + margin;
		int y = (ui->mGroupUserlistWidget->mapTo(this, pointZero)).y() + margin;
		int w = ui->mGroupUserlistWidget->width() - 2 * margin;

		searchWidget->move(x, y);
		searchWidget->resize(w, searchWidget->height());
		searchWidget->show();
		searchWidget->getFocus();
		searchWidget->setMaximumHeight(ui->mGroupUserlistWidget->height() - 2 * margin);
	}
	else
		searchWidget->clearAndHide();
}

void IMGroupChatView::slotGetItemInfo(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();

		if (map.value("result").toString() == "success")
		{
			QVariantMap user = map.value("user").toMap();

			QString userId = QString::number(user.value("userId").toInt());
			int disableStrangers = user.value("disableStrangers").toInt();

			//好友列表中有该人，或者该人设置可以接收陌生人消息，就直接聊天。
			if (gDataBaseOpera->DBJudgeFriendIsHaveByID(userId) || disableStrangers == 0)
				emit sigGroupBuddyPerChat(userId);
			else
			{
				//好友列表没有，弹出详情页。
				emit profilemanager::getInstance()->sigCreatePerFile(userId);
			}
		}
	}
}

void IMGroupChatView::slotOpenSearchUser(QString strUserID)
{
	UserInfo userInfo = gDataManager->getUserInfo();

	if (strUserID != QString::number(userInfo.nUserID))  //不是自己。
	{
		HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
		connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotGetItemInfo(bool, QString)));

		QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/user/getOtherUserByUserId?otherUserId=" + strUserID;

		http->getHttpRequest(url);

		searchWidget->clearAndHide();
		ui->searchBtn->setChecked(false);
	}
}

void IMGroupChatView::slotCloseVideoWidget()
{
	videoWidget->close();
	videoWidget = NULL;
}

void IMGroupChatView::slotVideoPlay(QString mediaPath)
{
	if (videoWidget)
		videoWidget->close();
	videoWidget = new IMVideoPlayer;
	connect(videoWidget, SIGNAL(sigClose()), this, SLOT(slotCloseVideoWidget()));
	videoWidget->videoPlay(mediaPath);
}

void IMGroupChatView::slotOpenDocument(QString msgID)
{
	UserInfo userInfo = gDataManager->getUserInfo();
	QString fileName = gDataBaseOpera->DBGetFileInfoLocalPath(msgID);
	//用QT打开目录
	if (fileName == "")
	{
		MessageInfo tmpifno = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);
		QJsonParseError jsonError;
		QString strFileName;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(tmpifno.strMsg.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QVariantMap result = jsonDocument.toVariant().toMap();
				strFileName = result["FileName"].toString();
			}
		}
		UserInfo userInfo = gDataManager->getUserInfo();
		QString currentPath = gSettingsManager->getUserPath();
		fileName = currentPath + QString("/files/") + QString("%1").arg(userInfo.nUserID) + QString("/") + strFileName;
	}
	if (fileName == "")
	{
		qDebug() << tr("Empty Filename! Open Document Failed!") << endl;
	}
	else {
#ifdef Q_OS_WIN
		//windows系统实现定位到文件功能
		const QString explorer = "explorer";
		QStringList param;
		if (!QFileInfo(fileName).isDir())
			param << QLatin1String("/select,");
		param << QDir::toNativeSeparators(fileName);
		QProcess::startDetached(explorer, param);
#else
		QString filePath = fileName.left(fileName.lastIndexOf("/"));
		QDesktopServices::openUrl(QUrl::fromLocalFile(filePath));//打开带中文的目录
#endif
	}
}

void IMGroupChatView::slotCancleLoadorDownLoad(QString MsgId)
{
	emit m_dispatcher->sigCancel(MsgId);
}

void IMGroupChatView::slotOpenGroupFile(QString msgID)
{
	UserInfo userInfo = gDataManager->getUserInfo();
	QString fileName = gDataBaseOpera->DBGetFileInfoLocalPath(msgID);
	//用QT打开目录
	if (fileName == "")
	{
		MessageInfo tmpifno = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);
		QJsonParseError jsonError;
		QString strFileName;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(tmpifno.strMsg.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QVariantMap result = jsonDocument.toVariant().toMap();
				strFileName = result["FileName"].toString();
			}
		}
		UserInfo userInfo = gDataManager->getUserInfo();
		QString currentPath = gSettingsManager->getUserPath();
		fileName = currentPath + QString("/files/") + QString("%1").arg(userInfo.nUserID) + QString("/") + strFileName;
	}
	QFile file(fileName);
	if (fileName == "" || !file.exists())
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("Empty file name or file doesn't exist!"));
	}
	else {

		QDesktopServices bs;
		bs.openUrl(QUrl::fromLocalFile(fileName));
	}
}

void IMGroupChatView::slotSendFileByID(QString msgID)
{
	UserInfo userInfo = gDataManager->getUserInfo();
	QString fileName = gDataBaseOpera->DBGetFileInfoLocalPath(msgID);
	QString strend = QString("DeleteByMegId(\"%1\")").arg(msgID);
	ui->mGroupWebView->page()->runJavaScript(strend);
	gSocketMessage->DeleteByMsgId(msgID);
	emit m_dispatcher->sigSendFile(fileName);
}

void IMGroupChatView::slotOpenLink(QString link)
{
	QUrl url(link);
	QString strlink = "";
	QString strScheme = url.scheme();
	if (strScheme.isEmpty())
		strlink = "http://" + link;
	else
		strlink = link;
	QDesktopServices::openUrl(QUrl(strlink));
}

void IMGroupChatView::slotClickUserHeader(QString msgID)
{
	MessageInfo messageInfo = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);
	QString groupUserID = QString::number(messageInfo.nFromUserID);
	if (!groupUserID.isEmpty())
	{
		emit profilemanager::getInstance()->sigCreatePerFile(groupUserID);
	}
}

void IMGroupChatView::slotPopUpMenu(bool isShowCopy, bool isShowTransmit, QString msgId)
{
	QMenu *popMenu = new QMenu(this);
	UserInfo userInfo = gDataManager->getUserInfo();
	MessageInfo tmpifno = gSocketMessage->DBGetMessageInfoWithMsgID(msgId);

	popMenu->setStyleSheet("QMenu::item{padding-left: 5px;padding-right: 5px;}");
	if (isShowCopy)
	{
		QAction *menuItem = new QAction(tr("Copy"), this);
		connect(menuItem, SIGNAL(triggered()), this, SLOT(slotWebViewCopy()));
		popMenu->addAction(menuItem);
	}
	//消息类型为1图片或5文件
	else if ((tmpifno.MessageChildType == 1) || (tmpifno.MessageChildType == 5))
	{
		QAction *menuItem_picCopy = new QAction(tr("Copy"), this);
		QVariant v = qVariantFromValue(msgId);

		menuItem_picCopy->setData(v);
		connect(menuItem_picCopy, SIGNAL(triggered()), this, SLOT(slotFileViewCopy()));
		popMenu->addAction(menuItem_picCopy);
	}

	if (tmpifno.MessageState == MESSAGE_STATE_FAILURE)
	{
		QAction *menuItem = new QAction(tr("Resend"), this);

		QVariant v = qVariantFromValue(msgId);
		menuItem->setData(v);

		connect(menuItem, SIGNAL(triggered()), this, SLOT(slotReSend()));
		popMenu->addAction(menuItem);
	}
	else if (tmpifno.MessageState == MESSAGE_STATE_UNLOADED)
	{
		QAction *menuItem_resend = new QAction(tr("Reload"), this);
		QVariant v = qVariantFromValue(msgId);
		menuItem_resend->setData(v);
		connect(menuItem_resend, SIGNAL(triggered()), this, SLOT(slotReLoad()));
		popMenu->addAction(menuItem_resend);
	}
	else if (isShowTransmit)
	{
		QAction *menuItem = new QAction(tr("Forward"), this);

		QVariant v = qVariantFromValue(msgId);
		menuItem->setData(v);

		connect(menuItem, SIGNAL(triggered()), this, SLOT(slotWebViewTransmit()));
		popMenu->addAction(menuItem);
	}

	if (true)
	{
		QAction *menuItem = new QAction(tr("Clear"), this);
		connect(menuItem, SIGNAL(triggered()), this, SLOT(slotWebViewClear()));
		popMenu->addAction(menuItem);
	}

	if (popMenu->actions().count() > 0)
		popMenu->exec(QCursor::pos());
	delete popMenu;
}

void IMGroupChatView::slotWebViewClear()
{
	m_vm->runJs("clear_only()");
}

void IMGroupChatView::slotWebViewTransmit()
{
	QAction *act = qobject_cast<QAction *>(sender());
	QVariant v = act->data();
	QString msgId = v.value<QString>();

	emit m_dispatcher->sigTransmit(msgId);
}

void IMGroupChatView::slotReLoad()
{
	QAction *act = qobject_cast<QAction *>(sender());
	QVariant v = act->data();
	QString msgId = v.value<QString>();
	MessageInfo tmpifno = gSocketMessage->DBGetMessageInfoWithMsgID(msgId);
	gSocketMessage->DBMsgReLoad(tmpifno);
}

void IMGroupChatView::slotReSend()
{
	QAction *act = qobject_cast<QAction *>(sender());
	QVariant v = act->data();
	QString msgId = v.value<QString>();
	MessageInfo tmpifno = gSocketMessage->DBGetMessageInfoWithMsgID(msgId);

	QString strend = QString("DeleteByMegId(\"%1\")").arg(msgId);
	ui->mGroupWebView->page()->runJavaScript(strend);
	QString strMsg = tmpifno.strMsg;
	gSocketMessage->DeleteByMsgId(msgId);

	if (tmpifno.MessageChildType == Message_TEXT)
	{
		m_store->SendTextMessage(strMsg, strMsg);
	}
	else if (tmpifno.MessageChildType == Message_PIC)
	{
		m_store->SendPicture(strMsg);
	}
	else if (tmpifno.MessageChildType == Message_FILE)
	{
		m_store->slotSendFile(strMsg);
	}
}

void IMGroupChatView::slotFileViewCopy()
{
	QAction *act = qobject_cast<QAction *>(sender());
	QVariant v = act->data();
	QString msgId = v.value<QString>();


	UserInfo userInfo = gDataManager->getUserInfo();
	MessageInfo tmpifno = gSocketMessage->DBGetMessageInfoWithMsgID(msgId);
	QString filePath;
	if (tmpifno.MessageChildType == 1)
	{
		MessageInfo message = gSocketMessage->DBGetMessageInfoWithMsgID(msgId);
		filePath = message.strMsg;
	}
	else if (tmpifno.MessageChildType == 5)
	{
		filePath = gDataBaseOpera->DBGetFileInfoLocalPath(msgId);
	}

#ifdef Q_OS_WIN
	//存在一个坑，必须使用反斜杠才能够查找到对应文件
	QString winFilePath = filePath.replace("/", "\\");
#else
    QString winFilePath = filePath;
#endif

	QMimeData *mimeData = new QMimeData;
	QList<QUrl> ourls;
	QUrl url = QUrl::fromLocalFile(winFilePath);
	ourls << url;

	mimeData->setUrls(ourls);
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setMimeData(mimeData);
}

void IMGroupChatView::slotWebViewCopy()
{
	ui->mGroupWebView->triggerPageAction(QWebEnginePage::Copy);
}

void IMGroupChatView::slotMsgID(QString msgID)
{
	MessageInfo msgInfo = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);

	if (msgInfo.MessageChildType == MessageType::Message_COMMON)
	{
		QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		QString userType = map.value("userType").toString();
		int id = map.value("id").toInt();
		if (userType == "user")
		{
			BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(QString::number(id));
			if (buddy.nUserId < 0)
			{
				buddy = gDataBaseOpera->DBGetGroupUserFromID(QString::number(id));
				if (buddy.nUserId < 0)
				{
					HttpNetWork::HttpNetWorkShareLib *PersonInfo = new HttpNetWork::HttpNetWorkShareLib();
					connect(PersonInfo, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotShareBuddyInfoResult(bool, QString)));
					AppConfig configInfo = gDataManager->getAppConfigInfo();
					QString strRequest = configInfo.MessageServerAddress + HTTP_GETBUDDYINFOBYID + QString::number(id);
					PersonInfo->getHttpRequest(strRequest);
					return;
				}
			}
			emit profilemanager::getInstance()->sigCreatePerFile(QString::number(id));
		}
		if (userType == "group")
		{
			GroupInfo group = gDataBaseOpera->DBGetGroupFromID(QString::number(id));
			emit profilemanager::getInstance()->sigCreateGrpFile(QString::number(id));
		}
	}
	if (msgInfo.MessageChildType == MessageType::Message_REDBAG)
	{
		UserInfo user = gDataManager->getUserInfo();

		QJsonDocument json = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
		QVariantMap map = json.toVariant().toMap();
		QString invalid = map.value("invalid").toString();

		int trusteeshipCoinId;
		QString packetId;

		if (invalid == "true")  //已经领取/失效
		{
			QVariantMap redPacket = map.value("redPacket").toMap();
			trusteeshipCoinId = redPacket.value("trusteeshipCoinId").toInt();
			packetId = redPacket.value("packetId").toString();
			OPRequestShareLib *request = new OPRequestShareLib;
			request->setObjectName(msgID);
			connect(request, SIGNAL(sigGetPacketResult(QString)), this, SLOT(slotGetPacketResult(QString)));
			request->getRedBag(user.strAccountName, QString::number(user.nUserID),
				QString::number(trusteeshipCoinId), packetId);
		}
		else                    //尚未领取
		{
			packetId = map.value("packetId").toString();
			OPRequestShareLib *request = new OPRequestShareLib;
			connect(request, SIGNAL(sigPacketStatus(bool)), this, SLOT(slotPacketStatus(bool)));
			request->setObjectName(msgID);
			request->readRedBagStatus(QString::number(user.nUserID),
				packetId);
		}
	}
}

void IMGroupChatView::slotPacketStatus(bool valid)
{
	OPRequestShareLib *request = qobject_cast<OPRequestShareLib *>(sender());
	QString msgID = request->objectName();
	MessageInfo msgInfo = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);
	UserInfo user = gDataManager->getUserInfo();
	if (valid)
	{
		if (openPacketWidget)
			openPacketWidget->close();
		openPacketWidget = new OpenPacketWidget;
		connect(openPacketWidget, SIGNAL(sigClose()), this, SLOT(slotClosePacketWidget()));
		connect(openPacketWidget, SIGNAL(sigOpenPacket(QString)), this, SLOT(slotOpenPacket(QString)));

		BuddyInfo buddy = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(m_groupId, QString::number(msgInfo.nFromUserID));
		QString name = buddy.strNote;
		if (buddy.strNote.isEmpty())
			name = buddy.strNickName;

		QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		packType type = map.value("luckMode").toInt() == 0 ? packType::groupRandomPack : packType::groupCommonPack;
		QString remarks = map.value("leaveMessage").toString();

		openPacketWidget->setInfo(buddy.strLocalAvatar, name, msgID, remarks, type);
		openPacketWidget->show();
	}
	else
	{
		QJsonDocument json = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
		QVariantMap map = json.toVariant().toMap();
		int trusteeshipCoinId = map.value("trusteeshipCoinId").toInt();
		QString packetId = map.value("packetId").toString();
		UserInfo user = gDataManager->getUserInfo();

		OPRequestShareLib *request = new OPRequestShareLib;
		request->setObjectName(msgID);
		connect(request, SIGNAL(sigGetPacketResult(QString)), this, SLOT(slotGetPacketResult(QString)));
		request->getRedBag(user.strAccountName, QString::number(user.nUserID),
			QString::number(trusteeshipCoinId), packetId);
	}
}

void IMGroupChatView::slotOpenPacket(QString msgID)
{
	slotClosePacketWidget();

	MessageInfo msgInfo = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);
	QJsonDocument json = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = json.toVariant().toMap();
	int trusteeshipCoinId = map.value("trusteeshipCoinId").toInt();
	QString packetId = map.value("packetId").toString();
	UserInfo user = gDataManager->getUserInfo();

	OPRequestShareLib *request = new OPRequestShareLib;
	request->setObjectName(msgID);
	connect(request, SIGNAL(sigGetPacketResult(QString)), this, SLOT(slotGetPacketResult(QString)));
	request->getRedBag(user.strAccountName, QString::number(user.nUserID),
		QString::number(trusteeshipCoinId), packetId);
}


void IMGroupChatView::slotClosePacketWidget()
{
	openPacketWidget->close();
	openPacketWidget = NULL;
}

void IMGroupChatView::slotGetPacketResult(QString result)
{
	if (!result.isEmpty())
	{
		//获得msgID。
		OPRequestShareLib *request = qobject_cast<OPRequestShareLib *>(sender());
		QString msgID = request->objectName();

		//设置红包为已打开状态。
		QString strSend = QString("setColor('%1');").arg(msgID);
		m_vm->runJs(strSend);

		//给消息体增加已经失效的标志，并写入数据库。
		QJsonDocument json = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = json.toVariant().toMap();
		map.insert("invalid", "true");

		QJsonDocument doc = QJsonDocument::fromVariant(map);
		gSocketMessage->DBUpdateMessageContent(msgID.toUtf8(), doc.toJson());

		//获取用户信息，消息结构体。
		UserInfo user = gDataManager->getUserInfo();
		MessageInfo msgInfo = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);

		//显示红包详情页面。
		if (detailWidget)
			detailWidget->close();
		detailWidget = new RedPackDetail;
		connect(detailWidget, SIGNAL(sigClose()), this, SLOT(slotCloseDetailWidget()));

		//红包发送者信息
		double coin = map.value("data").toDouble();
		if (coin < 0)
			coin = map.value("coin").toDouble();

		BuddyInfo buddy = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(m_groupId, QString::number(msgInfo.nFromUserID));
		if (buddy.strNote.isEmpty())
			buddy.strNote = buddy.strNickName;
		detailWidget->setMyInfo(buddy.strLocalAvatar, buddy.strNote, QString::number(coin, 'f', 5));

		//红包金额和领取状态信息
		QVariantMap packet = map.value("redPacket").toMap();
		int type = packet.value("type").toInt();
		QString remarks = packet.value("remarks").toString();
		double totalMoney = packet.value("totalMoney").toDouble();
		int totalSize = packet.value("totalSize").toInt();
		int remainSize = packet.value("remainSize").toInt();
		int trusteeshipCoinId = packet.value("trusteeshipCoinId").toInt();
		QString assetUnit;
		if (trusteeshipCoinId == 1)
			assetUnit = "PWR";
		detailWidget->setPackInfo(remarks, QString::number(totalMoney, 'f', 5), assetUnit, totalSize, totalSize - remainSize);

		//红包领取记录信息
		QVariantList list = map.value("list").toList();
		foreach(QVariant varRecord, list)
		{
			QVariantMap record = varRecord.toMap();
			bool best = false;
			if (type == 0)
				best = record.value("best").toInt() == 1 ? true : false;
			QString imUserId = record.value("imUserId").toString();
			QString nickName = record.value("nickName").toString();
			QDateTime dateTime = QDateTime::fromTime_t(record.value("time").toLongLong() / 1000);
			double money = record.value("coin").toDouble();
			BuddyInfo recordBuddy = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(m_groupId, imUserId);
			detailWidget->addPerson(recordBuddy.strLocalAvatar, nickName,
				dateTime.date().toString("yyyy-MM-dd"), dateTime.time().toString("hh:mm:ss"),
				QString::number(money, 'f', 5), assetUnit, best);
		}

		detailWidget->show();
	}
	else
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("Fail to check the red packet!"));
	}
}

void IMGroupChatView::slotCloseDetailWidget()
{
	detailWidget->close();
	detailWidget = NULL;
}

void IMGroupChatView::slotShareBuddyInfoResult(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		if (map.value("result").toString() == "success")
		{
			QVariantMap user = map.value("user").toMap();

			BuddyInfo buddy;
			buddy.BuddyType = 0;
			buddy.nUserType = user.value("userType").toInt();
			buddy.disableStrangers = user.value("disableStrangers").toInt();
			buddy.nUserId = user.value("userId").toInt();
			buddy.strEmail = user.value("email").toString();
			buddy.strHttpAvatar = user.value("avatar").toString();
			if (buddy.strHttpAvatar.isEmpty())
				buddy.strHttpAvatar = "https://tc.ipcom.io/panserver/files/c64b5061-77dc-45af-b91b-e5e06d401347/download";
			buddy.strLocalAvatar = QDir::tempPath() + "/" + QString::number(buddy.nUserId) + ".png";
			buddy.strMobilePhone = user.value("phone").toString();
			buddy.strNickName = user.value("nickName").toString();
			buddy.strPhone = user.value("phone").toString();
			buddy.strSex = user.value("sex").toString();
			buddy.strSign = user.value("sign").toString();
			buddy.strUserName = user.value("userName").toString();

			HttpNetWork::HttpDownLoadFile *down = new HttpNetWork::HttpDownLoadFile;
			down->setData(QVariant::fromValue(buddy));
			connect(down, SIGNAL(sigDownFinished(bool)), this, SLOT(slotShareBuddyHeader(bool)));
			down->StartDownLoadFile(buddy.strHttpAvatar, buddy.strLocalAvatar);
		}
	}
}

void IMGroupChatView::slotShareBuddyHeader(bool success)
{
	HttpNetWork::HttpDownLoadFile *down = qobject_cast<HttpNetWork::HttpDownLoadFile *>(sender());
	BuddyInfo buddy = down->getData().value<BuddyInfo>();

	emit profilemanager::getInstance()->sigCreatePerFile(QString::number(buddy.nUserId));
}

void IMGroupChatView::slotWebViewRenderProcessTerminated(QWebEnginePage::RenderProcessTerminationStatus terminationStatus, int exitCode)
{
	switch (terminationStatus)
	{
	case QWebEnginePage::NormalTerminationStatus:
		qDebug() << "webView: NormalTerminationStatus ";
		break;
	case QWebEnginePage::AbnormalTerminationStatus:
		qDebug() << "webView: AbnormalTerminationStatus ";
		break;
	case QWebEnginePage::CrashedTerminationStatus:
		qDebug() << "webView: CrashedTerminationStatus ";
		break;
	case QWebEnginePage::KilledTerminationStatus:
		qDebug() << "webView: KilledTerminationStatus ";
		break;
	default:
		break;
	}

	if (terminationStatus != QWebEnginePage::NormalTerminationStatus)
	{
		QTimer::singleShot(0, this, [=] {

			if (ChatDataManager::self())
			{
				emit ChatDataManager::self()->sigWebEngineViewTermination(ui->mGroupWebView->chatId());
			}

		});
	}

}

void IMGroupChatView::slotWebEngineFinish(bool bResult)
{
	if (bResult)
	{
		//切换合适的语言
		m_vm->runJs(QString("switchI18NLocale('%1')").arg(gI18NLocale));

		//切换主题样式
		if (gThemeStyle == "White")
		{
			m_vm->runJs("changeStyle('dayStyle')");
		}
		else if (gThemeStyle == "Blue")
		{
			m_vm->runJs("changeStyle('nightStyle')");
		}

		if (m_bHasStartGroupUserThread)
		{
			m_store->InitMessageInfo();
			ui->mTextEditGroup->setFocus();
		}
		else
		{
			//Web引擎初始化早了，数据还未准备,需要自己初始化
			m_bNeedManulInit = true;
		}

	}
}

void IMGroupChatView::slotChangeText()
{
	if (atWidget->isVisible())
	{
		QString text = ui->mTextEditGroup->toPlainText();
		if (text.length() >= atPosition)
		{
			text = text.right(text.length() - atPosition);
			if (!text.isEmpty())
				atWidget->search(text);
		}
		else
		{
			atWidget->hide();
		}
	}
	else
	{
		QString text = ui->mTextEditGroup->toPlainText();
		if (text.right(1) == tr("@") || text.right(1) == tr("@"))
		{
#ifdef Q_OS_WIN
			const QInputMethod *inputMethod = qApp->inputMethod();
			QRectF rect = inputMethod->cursorRectangle();
			QPoint point = rect.topLeft().toPoint() + gDataManager->getMainWidget()->pos();
			point.setY(point.y() - atWidget->height() - 5);
			point.setX(point.x() + 10);
#else
			QTextCursor tc = ui->mTextEditGroup->textCursor();
			QTextLayout *pLayout = tc.block().layout();
			int nCurpos = tc.position() - tc.block().position();
			int nTextline = pLayout->lineForTextPosition(nCurpos).lineNumber() + tc.block().firstLineNumber();
			QRect rect = ui->mTextEditGroup->cursorRect();
			QPoint point = rect.topLeft();
			int height = rect.height() * nTextline;
			point.setY(ui->mTextEditGroup->y() - atWidget->height() + height);
			point.setX(point.x() + 10);
#endif
			this->atPosition = ui->mTextEditGroup->toPlainText().length();
			atWidget->move(point);
			atWidget->search();
			atWidget->show();
		}
	}
}

void IMGroupChatView::slotCopy()
{
	QClipboard*clipBoard = QApplication::clipboard();
	clipBoard->setText(ui->mTextEditGroup->textCursor().selectedText());
}

void IMGroupChatView::slotCut()
{
	QClipboard*clipBoard = QApplication::clipboard();
	clipBoard->setText(ui->mTextEditGroup->textCursor().selectedText());
	ui->mTextEditGroup->textCursor().deleteChar();
}

void IMGroupChatView::slotPaste()
{
	QClipboard *board = QApplication::clipboard();
	const QMimeData *mimeData = board->mimeData();
	if (mimeData->hasUrls())
	{
		QList<QUrl> urls = mimeData->urls();
		foreach(QUrl url, urls)
		{
			QString filePath = url.toLocalFile();
			if (filePath.isEmpty())
			{
				//本地文件不存在，说明可能是文本内容
				if (mimeData->hasText())
				{
					QString text = mimeData->text();
					ui->mTextEditGroup->textCursor().insertText(text);
				}
				return;
			}

			//如果是文件夹，则直接返回
			if (QFileInfo(filePath).isDir())
			{
				return;
			}
#ifdef Q_OS_WIN
			filePath.remove("file:///");
#else
			filePath.remove("file://");
#endif
			QStringList types;
			types << ".png" << ".gif" << ".bmp" << ".jpg" << ".jpeg";
			bool hasSend = false;
			foreach(QString type, types)
			{
				if (filePath.toLower().endsWith(type))
				{
					QFile file(filePath);
					if (file.open(QIODevice::ReadOnly))
					{
						QByteArray byteArray = file.readAll();
						file.close();
						QImage image = QImage::fromData(byteArray);
						if (!image.isNull())
						{
							QTextImageFormat imageFormat;

							if (image.width() > 100 || image.height() > 100)
							{
								if (image.width() > image.height())
								{
									imageFormat.setWidth(100);
								}
								else
								{
									imageFormat.setHeight(100);
								}
							}

							imageFormat.setName(filePath);
							ui->mTextEditGroup->textCursor().insertImage(imageFormat);
							hasSend = true;
						}
					}
					break;
				}
			}

			if (!hasSend)
				emit m_dispatcher->sigSendFile(filePath);
		}
		return;
	}
	else if (mimeData->hasHtml())
	{
		QString html = mimeData->html();

		QTextDocument doc;
		doc.setHtml(html);
		QString strTmp = doc.toPlainText();
		QChar qc = 65532;
		strTmp.replace(qc, "");
		if (!strTmp.isEmpty())
		{
			ui->mTextEditGroup->textCursor().insertText(strTmp);
		}
		else
		{
			QTextDocumentFragment fragment;
			fragment = QTextDocumentFragment::fromHtml(html);
			ui->mTextEditGroup->textCursor().insertFragment(fragment);
		}
	}
	else if (mimeData->hasImage())
	{
		QImage image = qvariant_cast<QImage>(mimeData->imageData());

		QString imageID = QString::number(QDateTime::currentDateTime().toTime_t());
#ifdef Q_OS_WIN
		QString strPath = gSettingsManager->getUserPath() + "/Clipboard/" + imageID + ".png";
#else
		QString strPath = gSettingsManager->getUserPath() + "/Clipboard/" + imageID + ".png";
#endif
		QDir fileDir = QFileInfo(strPath).absoluteDir();
		QString strFileDir = QFileInfo(strPath).absolutePath();
		if (!fileDir.exists())
			fileDir.mkpath(strFileDir);

		image.save(strPath, "PNG");
		InsertTextEditPic(strPath);
	}
	else
	{
		QClipboard*clipBoard = QApplication::clipboard();
		ui->mTextEditGroup->insertPlainText(clipBoard->text());
	}
}

void IMGroupChatView::slotDel()
{
	ui->mTextEditGroup->textCursor().deleteChar();
}
