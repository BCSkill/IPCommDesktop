﻿#include "groupfilewidget.h"
#include "ui_groupfilewidget.h"

GroupFileWidget::GroupFileWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::GroupFileWidget();
	ui->setupUi(this);

	ui->tableWidget->installEventFilter(this);

	QFont font("Microsoft YaHei", 9);
	ui->tableWidget->horizontalHeader()->setFont(font);
	ui->tableWidget->horizontalHeader()->setFrameShape(QFrame::NoFrame);
	ui->tableWidget->horizontalHeader()->setMinimumHeight(24);
	ui->tableWidget->verticalHeader()->setVisible(false);
	ui->tableWidget->verticalHeader()->setDefaultSectionSize(40);

	ui->tableWidget->horizontalHeader()->resizeSection(4, 48);
	ui->tableWidget->horizontalHeader()->setSectionResizeMode(4, QHeaderView::Fixed);
	ui->tableWidget->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
	ui->tableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
	ui->tableWidget->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
	ui->tableWidget->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);
	
	ui->tableWidget->setFrameShape(QFrame::NoFrame);
	ui->tableWidget->setShowGrid(false);
	ui->tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
	ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
	ui->tableWidget->setFocusPolicy(Qt::NoFocus);

	QFile file(":/QSS/Resources/QSS/ChatWidgetShareLib/groupfilewidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();
}

GroupFileWidget::~GroupFileWidget()
{
	delete ui;
}

void GroupFileWidget::loadFileMessage()
{
	ui->tableWidget->clearContents();
	ui->tableWidget->setRowCount(0);

	int chatID = this->objectName().toInt();
	int amount = gSocketMessage->DBGetMessageRecordPageNum(chatID);
	for (int i = 0; i < amount; i++)
	{
		QList<MessageInfo> msgList = gSocketMessage->DBGetMessageRecordByPage(chatID, i, MessageType::Message_FILE);
		foreach (MessageInfo msg, msgList)
		{
			//目前只有接收并存在的文件会展示。
			QString filePath = gDataBaseOpera->DBGetFileInfoLocalPath(msg.msgID);
			if (!filePath.isEmpty() && QFile(filePath).exists())
			{
				this->insertRowByMessageInfo(msg);
			}
		}
	}

	int count = ui->tableWidget->rowCount();
	ui->informLabel->setText(tr("%1 files in total").arg(count));
}

bool GroupFileWidget::eventFilter(QObject * obj, QEvent * e)
{
	if (obj == ui->tableWidget)
	{
		if (e->type() == QEvent::Enter)
		{
			ui->tableWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
			ui->tableWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
		}
		if (e->type() == QEvent::Leave)
		{
			ui->tableWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
			ui->tableWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
		}
	}

	return QWidget::eventFilter(obj, e);
}

void GroupFileWidget::insertRowByMessageInfo(MessageInfo msg)
{
	int lastRow = ui->tableWidget->rowCount();
	ui->tableWidget->insertRow(lastRow);

	QJsonDocument doc = QJsonDocument::fromJson(msg.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();

	QString fileName = map.value("FileName").toString();

	FileTypeEx fileInfo;
	QString filePath = gDataBaseOpera->DBGetFileInfoLocalPath(msg.msgID);
	long double curSize = fileInfo.GetFileSize(filePath);    //这个目录的大小
	char unit = 'B';
	if (curSize > 1024)
	{
		//当前的大小比1024个字节还大，上面计数是按字节大小得到的fileInfo.size()
		curSize /= 1024;  //除
		unit = 'K';   //KB

		if (curSize > 1024)
		{
			//还大
			curSize /= 1024;
			unit = 'M';   //MB

			if (curSize > 1024)
			{
				curSize /= 1024;
				unit = 'G';   //GB
			} //if
		}   //if
	} //if
	QString fileSize = QString::number(curSize, 'f', 2) + unit;

	BuddyInfo buddy = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(this->objectName(), QString::number(msg.nFromUserID));
	QString uploader = buddy.strNote;
	if (uploader.isEmpty())
		uploader = buddy.strNickName;
	if (uploader.isEmpty())
		uploader = tr("unknown");

	qint64 time = msg.ServerTime == 0 ? msg.ClientTime : msg.ServerTime;
	if (QString::number(time).length() > 10)
		time = time / 1000;
	QDateTime fileDateTime = QDateTime::fromTime_t(time);
	QString fileTime = fileDateTime.toString("yyyy-MM-dd hh:mm");
	if (fileTime.startsWith("1970"))
		qDebug() << "time: " << msg.msgID;

	QTableWidgetItem *nameItem = new QTableWidgetItem(fileName);
	QTableWidgetItem *timeItem = new QTableWidgetItem(fileTime);
	QTableWidgetItem *sizeItem = new QTableWidgetItem(fileSize);
	QTableWidgetItem *uploaderItem = new QTableWidgetItem(uploader);

	QFont font("Microsoft YaHei", 9);
	nameItem->setFont(font);
	timeItem->setFont(font);
	sizeItem->setFont(font);
	uploaderItem->setFont(font);

	
	QString icoPath = fileInfo.GetFilePic(filePath);
	icoPath = icoPath.remove("qrc");  //该类默认获得的文件图标路径是qrc打头的。
	nameItem->setToolTip(fileName);
	nameItem->setIcon(QIcon(icoPath));

	timeItem->setTextAlignment(Qt::AlignCenter);
	sizeItem->setTextAlignment(Qt::AlignCenter);
	uploaderItem->setTextAlignment(Qt::AlignCenter);

	nameItem->setTextColor(QColor("#108ee9"));
	timeItem->setTextColor(QColor("#6a82a5"));
	sizeItem->setTextColor(QColor("#6a82a5"));
	uploaderItem->setTextColor(QColor("#6a82a5"));

	ui->tableWidget->setItem(lastRow, 0, nameItem);
	ui->tableWidget->setItem(lastRow, 1, timeItem);
	ui->tableWidget->setItem(lastRow, 2, sizeItem);
	ui->tableWidget->setItem(lastRow, 3, uploaderItem);

	QPushButton *openBtn = new QPushButton;
	openBtn->setStyleSheet("QPushButton{background-image:url(':/GroupChat/Resources/groupchat/openFile.png');background-position: center; background-repeat:no-repeat;border: none;}");
	openBtn->setToolTip(tr("open file"));
	openBtn->setCursor(Qt::PointingHandCursor);
	openBtn->setFont(font);
	openBtn->setObjectName(msg.msgID);
	connect(openBtn, SIGNAL(clicked()), this, SLOT(slotClickDownloadBtn()));
	ui->tableWidget->setCellWidget(lastRow, 4, openBtn);
}

void GroupFileWidget::slotClickDownloadBtn()
{
	QPushButton *senderBtn = qobject_cast<QPushButton*>(sender());
	QString msgID = senderBtn->objectName();
	QString filePath = gDataBaseOpera->DBGetFileInfoLocalPath(msgID);

	if (!filePath.isEmpty())
	{
		qDebug() << filePath;
		QDesktopServices::openUrl(QUrl::fromLocalFile(filePath));
	}
}