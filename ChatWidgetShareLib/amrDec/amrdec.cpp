﻿#include "amrdec.h"
#include <QDebug>

const int sizes[] = { 12, 13, 15, 17, 19, 20, 26, 31, 5, 6, 5, 5, 0, 0, 0, 0 };

AmrDec::AmrDec(QObject *parent)
	: QThread(parent)
{

}

AmrDec::~AmrDec()
{
}

void AmrDec::convertAmrToWav(QString amr, QString wav, bool usingThread)
{
	this->amrPath = amr;
	this->wavPath = wav;

	if (usingThread)
		this->start();
	else
		doConvert();
}

void AmrDec::run()
{
	doConvert();
}

void AmrDec::doConvert()
{
	std::string amrString = amrPath.toLocal8Bit().toStdString();
	const char* amrChar = amrString.c_str();
	std::string wavString = wavPath.toLocal8Bit().toStdString();
	const char* wavChar = wavString.c_str();

	FILE* in;
	char header[6];
	int n;
	void *wav, *amr;
	in = fopen(amrChar, "rb");
	if (!in) {
		perror(amrChar);

		emit sigConvertFinished("failed");
	}
	else
	{
		n = fread(header, 1, 6, in);
		if (n != 6 || memcmp(header, "#!AMR\n", 6)) {
			fprintf(stderr, "Bad header\n");
			emit sigConvertFinished("failed");
		}
		else
		{
			wav = wav_write_open(wavChar, SAMPLE_RATE, BITS_PER_SAMPLE, CHANNELS);
			if (!wav) {
				fprintf(stderr, "Unable to open %s\n", wavChar);
				emit sigConvertFinished("failed");
			}
			else
			{
                amr = Decoder_Interface_init();
				while (1) {
					uint8_t buffer[500], littleendian[320], *ptr;
					int size, i;
					int16_t outbuffer[160];
					/* Read the mode byte */
					n = fread(buffer, 1, 1, in);
					if (n <= 0)
						break;
					/* Find the packet size */
					size = sizes[(buffer[0] >> 3) & 0x0f];
					n = fread(buffer + 1, 1, size, in);
					if (n != size)
						break;

					/* Decode the packet */
					Decoder_Interface_Decode(amr, buffer, outbuffer, 0);
					/* Convert to little endian and write to wav */
					ptr = littleendian;
					for (i = 0; i < 160; i++) {
						*ptr++ = (outbuffer[i] >> 0) & 0xff;
						*ptr++ = (outbuffer[i] >> 8) & 0xff;
					}
					wav_write_data(wav, littleendian, 320);
				}
				fclose(in);
				Decoder_Interface_exit(amr);
				wav_write_close(wav);
				emit sigConvertFinished(wavPath);
			}
		}
	}
}
