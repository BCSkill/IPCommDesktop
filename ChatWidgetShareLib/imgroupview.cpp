﻿#include "imgroupview.h"
#include "imgroupdispatcher.h"
#include "imgroupstore.h"
#include "imgroupviewmodel.h"
#include "ui_groupwidget.h"
#include "qlabelheader.h"
#include "profilemanager.h"
#include "imgroupchatview.h"
#include "imgroupchatdispatcher.h"
#include <QFile>
#include <QVBoxLayout>
#include <QDragEnterEvent>
#include <QMimeData>
#include <QFileInfo>

IMGroupView::IMGroupView(QString groupID, QWidget *parent) : QWidget(parent)
,m_groupChatView(NULL)
{
	ui = new Ui::GroupWidget();
	ui->setupUi(this);

	this->setObjectName(groupID);
	m_groupId = groupID;

	m_vm = new IMGroupViewModel(this);
	m_dispatcher = new IMGroupDispatcher(this);
	m_store = new IMGroupStore(this);

	m_vm->init();
	m_dispatcher->init();
	m_store->init();

	initSelf();
	initChatWidget();

	initConnectSelfSignals();
}

IMGroupView::~IMGroupView()
{
	delete ui;
}

#ifdef Q_OS_WIN
WId IMGroupView::getChatWidgetID()
{
	return this->chatWidgetID;
}
#endif


IMGroupDispatcher* IMGroupView::dispatcher()
{
	return m_dispatcher;
}

IMGroupStore* IMGroupView::store()
{
	return m_store;
}

IMGroupViewModel* IMGroupView::vm()
{
	return m_vm;
}

IMGroupChatView* IMGroupView::getGroupChatView()
{
	return m_groupChatView;
}

void IMGroupView::initConnectSelfSignals()
{
	
}

void IMGroupView::initSelf()
{
	ui->mLabelGroupNickChat->installEventFilter(this);

	//加载样式
	QFile file(":/QSS/Resources/QSS/ChatWidgetShareLib/groupchat.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	this->setAcceptDrops(true);//支持拖放

	setAttribute(Qt::WA_DeleteOnClose, true);
}

void IMGroupView::dragEnterEvent(QDragEnterEvent *event)
{
	//如果为文件，则支持拖放
	if (event->mimeData()->hasFormat("text/uri-list"))
		event->acceptProposedAction();
}

void IMGroupView::dropEvent(QDropEvent *event)
{
	QList<QUrl> urls = event->mimeData()->urls();
	if (urls.isEmpty())
		return;

	//获取文件名
	foreach(QUrl url, urls)
	{
		QString file_name = url.toLocalFile();

		if (QFileInfo(file_name).isFile())
		{
			emit m_groupChatView->dispatcher()->sigSendFile(file_name);
		}

	}
}

void IMGroupView::mousePressEvent(QMouseEvent *event)
{
	QRect rect;
	rect = QRect(ui->mGroupLabelBK->pos() + this->pos(), ui->mGroupLabelBK->size());
	emit m_groupChatView->dispatcher()->sigExpressHide(rect, event->pos());
	rect = QRect(ui->mLabelGroupNickChat->pos() + this->pos(), ui->mLabelGroupNickChat->size());
	emit m_groupChatView->dispatcher()->sigExpressHide(rect, event->pos());

	return QWidget::mousePressEvent(event);
}

bool IMGroupView::eventFilter(QObject *obj, QEvent *e)
{
	if (obj == ui->mLabelGroupNickChat && e->type() == QEvent::MouseButtonPress)
	{
		emit profilemanager::getInstance()->sigCreateGrpFile(this->objectName());
	}

	return QWidget::eventFilter(obj, e);
}


void IMGroupView::initChatWidget()
{
	m_groupChatView = new IMGroupChatView(m_groupId, this);
	m_groupChatView->setObjectName(this->objectName());
	mGroupFileWidget = new GroupFileWidget(this);
	mGroupFileWidget->setObjectName(this->objectName());

#ifdef Q_OS_WIN
	this->chatWidgetID = m_groupChatView->winId();
#endif

	ui->mGroupStackedWidget->addWidget(m_groupChatView);
	ui->mGroupStackedWidget->addWidget(mGroupFileWidget);

	connect(ui->chatPageBtn, SIGNAL(clicked()), this, SLOT(slotClickedChatWidget()));
	connect(ui->filePageBtn, SIGNAL(clicked()), this, SLOT(slotClickedFileWidget()));

}

void IMGroupView::setGroupNameAndGroupId(QString strGroupName, QString strGroupID)
{
	ui->mLabelGroupNickChat->setText(strGroupName);
	ui->mLabelGroupNickChat->setObjectName(strGroupID);

	QFont font;
	font.setFamily(QString::fromUtf8("\345\276\256\350\275\257\351\233\205\351\273\221"));
	font.setPointSize(16);
	QFontMetrics fm(font);
	int str_width = fm.width(strGroupName);
	ui->mLabelGroupNickChat->setFixedSize(str_width + 8, ui->mLabelGroupNickChat->height());
}


void IMGroupView::slotClickedChatWidget()
{
	ui->mGroupStackedWidget->setCurrentWidget(m_groupChatView);
}

void IMGroupView::slotClickedFileWidget()
{
	ui->mGroupStackedWidget->setCurrentWidget(mGroupFileWidget);
	mGroupFileWidget->loadFileMessage();
}