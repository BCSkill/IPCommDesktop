﻿#include "messagelistdispatcher.h"
#include "messageliststore.h"
#include "profilemanager.h"
#include "messagelistview.h"
#include "messagebox.h"
#include "httpnetworksharelib.h"
#include "imdatabaseoperainfo.h"

extern IMDataBaseOperaInfo *gDataBaseOpera;

MessageListDispatcher* MessageListDispatcher::s_instance = NULL;

MessageListDispatcher::MessageListDispatcher(QObject *parent) : QObject(parent)
{
	s_instance = this;

	connect(this, SIGNAL(sigClickIndex(int)), this, SLOT(slotClickIndex(int)));
	connect(this, SIGNAL(sigClickItem(QString)), this, SLOT(slotClickItem(QString)));
	connect(this, SIGNAL(sigRightClickItem(QString)), this, SLOT(slotRightClickItem(QString)));
	connect(this, SIGNAL(sigCloseChat(QString)), this, SLOT(slotCloseChat(QString)));

	connect(this, SIGNAL(sigCloseAllChat()), this, SLOT(slotCloseAllChat()));
	connect(this, SIGNAL(sigInsertPerToPerMessage(MessageInfo, BuddyInfo, bool)), this, SLOT(slotInsertPerToPerMessage(MessageInfo, BuddyInfo, bool)));
	connect(this, SIGNAL(sigInsertPerToGroupMessage(MessageInfo, GroupInfo,bool)), this, SLOT(slotInsertPerToGroupMessage(MessageInfo, GroupInfo,bool)));
	connect(this, SIGNAL(sigDealPerMessageType(MessageInfo, BuddyInfo)), this, SLOT(slotDealPerMessageType(MessageInfo, BuddyInfo)));
	connect(this, SIGNAL(sigDealGroupMessageType(MessageInfo, GroupInfo)), this, SLOT(slotDealGroupMessageType(MessageInfo, GroupInfo)));
	connect(this, SIGNAL(sigUpdateMessage(bool, QVariant)), this, SLOT(slotUpdateMessage(bool, QVariant)));

	connect(this, SIGNAL(sigKeyUpDown(QKeyEvent *)), this, SLOT(slotKeyUpDown(QKeyEvent *)));
	connect(this, SIGNAL(sigViewKeyPressEvent(QKeyEvent *)), this, SLOT(slotKeyUpDown(QKeyEvent *)));
	connect(this, SIGNAL(sigInsertSelfMessage(bool, QVariant, QString, bool, QMap<QString, QVariant>)), this, SLOT(slotInsertSelfMessage(bool, QVariant, QString, bool, QMap<QString, QVariant>)));

	connect(this, SIGNAL(sigSetDraft(QString, QString)), this, SLOT(slotSetDraft(QString, QString)));

	connect(this, SIGNAL(sigViewDetails(QString, bool)), this, SLOT(slotViewDetails(QString, bool)));
	connect(this, SIGNAL(sigCancleMessageTop(QString)), this, SLOT(slotCancleMessageTop(QString)));
	connect(this, SIGNAL(sigMessageTop(QString)), this, SLOT(slotMessageTop(QString)));
	connect(this, SIGNAL(sigMessagePrompt(QString)), this, SLOT(slotMessagePrompt(QString)));
	connect(this, SIGNAL(sigMessageNoPrompt(QString)), this, SLOT(slotMessageNoPrompt(QString)));
	connect(this, SIGNAL(sigDissolveGroup(QString)), this, SLOT(slotDissolveGroup(QString)));
	connect(this, SIGNAL(sigUserQuitGroup(QString)), this, SLOT(slotUserQuitGroup(QString)));

	connect(this, SIGNAL(sigCheckHttp(bool, QString)), this, SLOT(slotCheckHttp(bool, QString)));
}

MessageListDispatcher* MessageListDispatcher::instance()
{
	return s_instance;
}

void MessageListDispatcher::init()
{
	m_store = MessageListStore::instance();
}

void MessageListDispatcher::slotCloseChat(QString strUserId)
{
	m_store->closeChat(strUserId);
}

void MessageListDispatcher::slotCloseAllChat()
{
	m_store->closeAllChat();
}

void MessageListDispatcher::slotInsertPerToPerMessage(MessageInfo messageinfo, BuddyInfo buddyInfo, bool bChangeNum)
{
	m_store->insertPerToPerMessage(messageinfo, buddyInfo, bChangeNum);
}

void MessageListDispatcher::slotInsertPerToGroupMessage(MessageInfo messageinfo, GroupInfo groupInfo, bool bChangeNum)
{
	m_store->insertPerToGroupMessage(messageinfo, groupInfo, bChangeNum);
}

void MessageListDispatcher::slotDealPerMessageType(MessageInfo msgTmp, BuddyInfo buddyInfo)
{
	m_store->dealPerMessageType(msgTmp, buddyInfo);
}

void MessageListDispatcher::slotDealGroupMessageType(MessageInfo msgTmp, GroupInfo groupInfo)
{
	m_store->dealGroupMessageType(msgTmp, groupInfo);
}

void MessageListDispatcher::slotUpdateMessage(bool isGroup, QVariant var)
{
	m_store->updateMessage(isGroup, var);
}

void MessageListDispatcher::slotKeyUpDown(QKeyEvent *event)
{
	if (event->key() == Qt::Key_Up)
	{
		m_store->doUpDownKeyClick(true);
	}
	if (event->key() == Qt::Key_Down)
	{
		m_store->doUpDownKeyClick(false);
	}
}

void MessageListDispatcher::slotInsertSelfMessage(bool isGroup, QVariant var, QString msg, bool bNeedOpen, QMap<QString, QVariant> params)
{
	m_store->insertSelfMessage(isGroup, var, msg, bNeedOpen, params);
}

void MessageListDispatcher::slotClickIndex(int idx)
{
	m_store->clickIndex(idx);
}

void MessageListDispatcher::slotClickItem(QString strUserId)
{
	m_store->clickItemByUserId(strUserId);
}

void MessageListDispatcher::slotRightClickItem(QString strUserId)
{
	m_store->rightClickItemByUserId(strUserId);
}

void MessageListDispatcher::slotSetDraft(QString strUserId, QString strMsg)
{
	m_store->setDraft(strUserId, strMsg);
}

void MessageListDispatcher::slotViewDetails(QString strUserId, bool isPer)
{
	if (isPer)
		emit profilemanager::getInstance()->sigCreatePerFile(strUserId);
	else
		emit profilemanager::getInstance()->sigCreateGrpFile(strUserId);
}

void MessageListDispatcher::slotCancleMessageTop(QString strUserId)
{
	m_store->cancelMessageTop(strUserId);
}

void MessageListDispatcher::slotMessageTop(QString strUserId)
{
	m_store->setMessageTop(strUserId);
}

void MessageListDispatcher::slotMessagePrompt(QString strUserId)
{
	m_store->setMessagePrompt(strUserId);
}

void MessageListDispatcher::slotMessageNoPrompt(QString strUserId)
{
	m_store->cancelMessagePrompt(strUserId);
}

void MessageListDispatcher::slotDissolveGroup(QString strUserId)
{
	MessageListView* view = reinterpret_cast<MessageListView*>(this->parent());
	IMessageBox* pBox = new IMessageBox(view);
	pBox->initAsk(tr("Notice"), tr("Are you sure to dismiss this group?"));
	connect(pBox, &IMessageBox::sigOK, this, [=] {
		m_store->dissolveGroup(strUserId);
		pBox->deleteLater();
	});
}

void MessageListDispatcher::slotUserQuitGroup(QString strUserId)
{
	MessageListView* view = reinterpret_cast<MessageListView*>(this->parent());
	IMessageBox* pBox = new IMessageBox(view);
	pBox->initAsk(tr("Notice"), tr("Are you sure to quit group?"));
	connect(pBox, &IMessageBox::sigOK, this, [=] {
		m_store->quitGroup(strUserId);
		pBox->deleteLater();
	});
}

void MessageListDispatcher::slotCheckHttp(bool bResult, QString strResult)
{
	MessageListView* view = reinterpret_cast<MessageListView*>(this->parent());

	HttpNetWork::HttpNetWorkShareLib *http = qobject_cast<HttpNetWork::HttpNetWorkShareLib *>(sender());
	if (http)
	{
		if (bResult&&strResult.contains("success"))
		{
			//成功后手动删除本地数据库和list 防止收不到服务器的删除消息
			if (http)
			{
				QString strGroupID = http->getData().toString();
				gDataBaseOpera->DBDeleteGroupInfoByID(strGroupID);
			}
		}
		else
		{
			//提示失败
			IMessageBox::tip(view, tr("Notice"), tr("Network request failed!"));
		}
		http->deleteLater();
	}

}