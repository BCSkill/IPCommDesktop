﻿#include "HeadProvider.h"
#include <QFile>
#include <QImageReader>


HeadProvider::HeadProvider(ImageType type, Flags flags) :
    QQuickImageProvider(type,flags)
{
    
}

HeadProvider::~HeadProvider()
{
    
}

QImage HeadProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
	QImageReader reader;
	
	reader.setDecideFormatFromContent(true);

	QStringList id_list = id.split('?');

	QString file;
	if (id_list.count() > 1)
	{
		file = id_list[0];
	}
	else
	{
		file = id_list[0];
	}

	reader.setFileName(file);

	size->setWidth(reader.size().width());
	size->setHeight(reader.size().height());

	reader.setScaledSize(requestedSize);

    return reader.read();
}
