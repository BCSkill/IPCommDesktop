﻿#include "messagelistview.h"
#include <QGuiApplication>
#include <QtQml>
#include <QQuickItem>
#include "HeadProvider.h"
#include "messagelistviewmodel.h"
#include "messagelistdispatcher.h"
#include "messageliststore.h"

extern QString gThemeStyle;

MessageListView::MessageListView(QWidget *parent)
	: QQuickWidget(parent)
{
	this->setResizeMode(QQuickWidget::SizeRootObjectToView);

	HeadProvider *imageProvider = new HeadProvider(QQmlImageProviderBase::Image);
	this->engine()->addImageProvider(QLatin1String("HeadProvider"), imageProvider);


	MessageListViewModel* vm = new MessageListViewModel(this);
	MessageListDispatcher* dispatcher = new MessageListDispatcher(this);
	MessageListStore* store = new MessageListStore(this);

	vm->init();
	dispatcher->init();
	store->init();

	this->rootContext()->setContextProperty("vm", vm);
	this->rootContext()->setContextProperty("vm_model", vm->model());
	this->rootContext()->setContextProperty("dispatcher", dispatcher);
	this->setSource(QUrl("qrc:/MessageList/Resources/MessageList/MessageList.qml"));

	

	//切换主题样式
	if (gThemeStyle == "White")
	{
		vm->changeStyle("dayStyle");
	}
	else if (gThemeStyle == "Blue")
	{
		vm->changeStyle("nightStyle");
	}

	QString strColor = this->rootObject()->property("clearColor").toString();
	this->setClearColor(QColor(strColor));
	
}

void MessageListView::mouseReleaseEvent(QMouseEvent *event)
{
#ifdef Q_OS_MAC
	//MAC下触摸板经常拖动偶尔导致无法正常点击，所以发射信号使其正常
	if (event->button() == Qt::LeftButton)
	{
		emit qGuiApp->applicationStateChanged(Qt::ApplicationInactive);
		emit qGuiApp->applicationStateChanged(Qt::ApplicationActive);
	}
#endif

	QQuickWidget::mouseReleaseEvent(event);
}

MessageListDispatcher* MessageListView::dispatcher()
{
	return MessageListDispatcher::instance();
}

MessageListViewModel* MessageListView::vm()
{
	return MessageListViewModel::instance();
}

MessageListStore* MessageListView::store()
{
	return MessageListStore::instance();
}

void MessageListView::keyPressEvent(QKeyEvent *event)
{
	emit dispatcher()->sigViewKeyPressEvent(event);

	return QQuickWidget::keyPressEvent(event);
}