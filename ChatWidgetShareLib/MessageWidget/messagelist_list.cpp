﻿#include "messagelist_list.h"
#ifdef Q_OS_MAC
#include "mycommonstyle.h"
#endif
#include "QStringLiteralBak.h"

MessageList::MessageList(QWidget *parent)
	: QListWidget(parent)
{
	connect(this, SIGNAL(itemPressed(QListWidgetItem *)), this, SLOT(doDoubleClickedItem(QListWidgetItem *)));

	QFile scroolbar_style_qss(":/QSS/Resources/QSS/scrollbarStyle.qss");
	scroolbar_style_qss.open(QFile::ReadOnly);
	this->verticalScrollBar()->setStyleSheet(scroolbar_style_qss.readAll());
	this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	this->setMouseTracking(true);
#ifdef Q_OS_MAC
    this->setStyle(new MyProxyStyle);
#endif
}

MessageList::~MessageList()
{

}

MessageListInfo MessageList::OnInsertPerToPerMessage(MessageInfo messageinfo, BuddyInfo buddyInfo)
{
	MessageListInfo messageListInfo;

	//首先生成信息时间。
	QString strtemp = QString("%1").arg(messageinfo.ClientTime);
	if (strtemp.length() == 13)
		messageinfo.ClientTime = messageinfo.ClientTime / 1000;

	messageinfo.strMsg = OnDealMessageType(messageinfo);

	//首先判断列表中是否存在
	for (int i = 0; i < this->count(); i++)
	{
		QListWidgetItem *item = this->item(i);
		if (!item)
		{
			continue;
		}
		CFrientItemMessage *buddy = (CFrientItemMessage*)this->itemWidget(item);
		if (buddy != NULL)
		{
			if (buddy->mNickName->objectName().toInt() == buddyInfo.nUserId)
			{
				/*
				//更新。
				buddy->OnSetPicPath(buddyInfo.strLocalAvatar, 0);
				buddy->OnSetMessageTime(strTime);
				if (item->isSelected() && this->isVisible())
					buddy->OnSetMessageNum("");
				else
				{
					int unReadNum = buddy->getMessageNum() + 1;
					buddy->OnSetMessageNum(QString::number(unReadNum));
				}
				if (buddyInfo.strNote.isEmpty())
					buddy->OnSetNickNameText(buddyInfo.strNickName);
				else
					buddy->OnSetNickNameText(buddyInfo.strNote);
				if (messageinfo.strMsg.isEmpty())
					item->setSelected(true);
				else
					buddy->OnSetAutoGrapthText(messageinfo.strMsg);
				*/

				QString strNick = buddyInfo.strNote;
				if (buddyInfo.strNote.isEmpty())
					strNick = buddyInfo.strNickName;

				QString unReadNum;
				if (item->isSelected())
					unReadNum = "";
				else
					unReadNum = QString::number(buddy->getMessageNum() + 1);

				this->takeItem(i);
				this->OnInsertMessage(QString::number(buddyInfo.nUserId),
					buddyInfo.strLocalAvatar,
					strNick,
					messageinfo.strMsg, messageinfo.ClientTime, unReadNum, 0);

				messageListInfo.nBudyyID = buddyInfo.nUserId;
				messageListInfo.strLastMessage = messageinfo.strMsg;
				messageListInfo.nLastTime = messageinfo.ClientTime;
				messageListInfo.strBuddyName = buddyInfo.strNickName;
				messageListInfo.strBuddyHeaderImage = buddyInfo.strLocalAvatar;
				messageListInfo.messageType = messageinfo.MessageChildType;
				messageListInfo.nUnReadNum = buddy->getMessageNum()+1;////该值将要被写到数据库里，数据库未读数量应与显示的未读数量一致
				messageListInfo.isGroup = 0;

				return messageListInfo;
			}
		}
	}

	if (this->count() >= 30)
	{
		QListWidgetItem *item = this->item(0);
		if (item)
		{
			CFrientItemMessage *widget = (CFrientItemMessage *)this->itemWidget(item);
			if (widget)
			{
				for (int j = 1; j < this->count(); j++)
				{
					QListWidgetItem *currentItem = this->item(j);
					if (currentItem)
					{
						CFrientItemMessage *currentWidget = (CFrientItemMessage *)this->itemWidget(currentItem);
						if (currentWidget)
						{
							if (widget->getLastTime() > currentWidget->getLastTime())
							{
								item = currentItem;
								widget = currentWidget;
							}
						}
					}
				}

			QString deleteID = widget->mNickName->objectName();
			this->takeItem(this->row(item));
			}
		}
	}

	QString strNick = buddyInfo.strNote.isEmpty() ? buddyInfo.strNickName : buddyInfo.strNote;

	this->OnInsertMessage(QString::number(buddyInfo.nUserId),
		buddyInfo.strLocalAvatar,
		strNick,
		messageinfo.strMsg, messageinfo.ClientTime, "1");

	messageListInfo.nBudyyID = buddyInfo.nUserId;
	messageListInfo.strLastMessage = messageinfo.strMsg;
	messageListInfo.nLastTime = messageinfo.ClientTime;
	messageListInfo.strBuddyName = buddyInfo.strNickName;
	messageListInfo.strBuddyHeaderImage = buddyInfo.strLocalAvatar;
	messageListInfo.messageType = messageinfo.MessageChildType;
	messageListInfo.nUnReadNum = 1;
	messageListInfo.isGroup = 0;

	return messageListInfo;
}

MessageListInfo MessageList::OnInsertPerToGroupMessage(MessageInfo messageinfo, GroupInfo groupInfo)
{
	MessageListInfo messageListInfo;

	QString strtemp = QString("%1").arg(messageinfo.ClientTime);
	if (strtemp.length() == 13)
		messageinfo.ClientTime = messageinfo.ClientTime / 1000;

	messageinfo.strMsg = OnDealMessageType(messageinfo);
	
	if (!messageinfo.strMsg.isEmpty())
	{
		QString fromID = QString::number(messageinfo.nFromUserID);
		QString groupID = QString::number(messageinfo.nToUserID);
		BuddyInfo buddyInfo = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(groupID, fromID);
		if (!buddyInfo.strNote.isEmpty())
			messageinfo.strMsg = buddyInfo.strNote + ":" + messageinfo.strMsg;
		else
		{
			if (!buddyInfo.strNickName.isEmpty())
				messageinfo.strMsg = buddyInfo.strNickName + ":" + messageinfo.strMsg;
		}
	}

	//首先判断列表中是否存在
	for (int i = 0; i < this->count(); i++)
	{
		QListWidgetItem *item = this->item(i);
		if (item)
		{
			CFrientItemMessage *buddy = (CFrientItemMessage*)this->itemWidget(item);
			if (buddy != NULL)
			{
				if (buddy->mNickName->objectName() == groupInfo.groupId)
				{
					QString unReadNum;
					if (item->isSelected() && this->isVisible())
						unReadNum = "";
					else
						unReadNum = QString::number(buddy->getMessageNum() + 1);

					this->takeItem(i);
					this->OnInsertMessage(groupInfo.groupId,
						groupInfo.groupLoacalHeadImage,
						groupInfo.groupName,
						messageinfo.strMsg, messageinfo.ClientTime, unReadNum, 1);

					messageListInfo.nBudyyID = groupInfo.groupId.toInt();
					messageListInfo.strLastMessage = messageinfo.strMsg;
					messageListInfo.nLastTime = messageinfo.ClientTime;
					messageListInfo.strBuddyName = groupInfo.groupName;
					messageListInfo.strBuddyHeaderImage = groupInfo.groupLoacalHeadImage;
					messageListInfo.messageType = messageinfo.MessageChildType;
					messageListInfo.nUnReadNum = buddy->getMessageNum()+1;//该值将要被写到数据库里，数据库未读数量应与显示的未读数量一致
					messageListInfo.isGroup = 1;

					return messageListInfo;
				}
			}
		}
	}

	if (this->count() >= 30)
	{
		QListWidgetItem *item = this->item(0);
		if (item)
		{
			CFrientItemMessage *widget = (CFrientItemMessage *)this->itemWidget(item);
			if (widget)
			{
				for (int j = 1; j < this->count(); j++)
				{
					QListWidgetItem *currentItem = this->item(j);
					if (currentItem)
					{
						CFrientItemMessage *currentWidget = (CFrientItemMessage *)this->itemWidget(currentItem);
						if (currentWidget)
						{
							if (widget->getLastTime() > currentWidget->getLastTime())
							{
								item = currentItem;
								widget = currentWidget;
							}
						}
					}
				}
				QString deleteID = widget->mNickName->objectName();
				this->takeItem(this->row(item));
			}
		}
	}
	
	this->OnInsertMessage(groupInfo.groupId,
		groupInfo.groupLoacalHeadImage,
		groupInfo.groupName,
		messageinfo.strMsg, messageinfo.ClientTime, "1", 1);

	//插入新的
	messageListInfo.nBudyyID = groupInfo.groupId.toInt();
	messageListInfo.strLastMessage = messageinfo.strMsg;
	messageListInfo.nLastTime = messageinfo.ClientTime;
	messageListInfo.strBuddyName = groupInfo.groupName;
	messageListInfo.strBuddyHeaderImage = groupInfo.groupLoacalHeadImage;
	messageListInfo.messageType = messageinfo.MessageChildType;
	messageListInfo.nUnReadNum = 1;
	messageListInfo.isGroup = 1;

	return messageListInfo;
}

MessageListInfo MessageList::OnClickPerMessage(MessageInfo messageinfo, BuddyInfo buddyInfo)
{
	//首先生成信息时间。
	QString strtemp = QString("%1").arg(messageinfo.ClientTime);
	if (strtemp.length() == 13)
		messageinfo.ClientTime = messageinfo.ClientTime / 1000;

	//根据消息类型设置显示的内容。
	messageinfo.strMsg = OnDealMessageType(messageinfo);

	MessageListInfo messageListInfo;
	messageListInfo.nBudyyID = buddyInfo.nUserId;
	messageListInfo.strLastMessage = messageinfo.strMsg;
	messageListInfo.nLastTime = messageinfo.ClientTime;
	messageListInfo.strBuddyName = buddyInfo.strNickName;
	messageListInfo.strBuddyHeaderImage = buddyInfo.strLocalAvatar;
	messageListInfo.messageType = messageinfo.MessageChildType;
	messageListInfo.nUnReadNum = 0;
	messageListInfo.isGroup = 0;

	//首先判断列表中是否存在
	for (int i = 0; i < this->count(); i++)
	{
		QListWidgetItem *item = this->item(i);
		if (item)
		{
			CFrientItemMessage *buddy = (CFrientItemMessage*)this->itemWidget(item);
			if (buddy != NULL)
			{
				if (buddy->mNickName->objectName().toInt() == buddyInfo.nUserId)
				{
					item->setSelected(true);
					buddy->OnSetMessageNum("");
					return messageListInfo;
				}
			}		
		}
	}

	if (this->count() >= 30)
	{
		QListWidgetItem *item = this->item(0);
		if (item)
		{
			CFrientItemMessage *widget = (CFrientItemMessage *)this->itemWidget(item);
			if (widget)
			{
				for (int j = 1; j < this->count(); j++)
				{
					QListWidgetItem *currentItem = this->item(j);
					if (currentItem)
					{
						CFrientItemMessage *currentWidget = (CFrientItemMessage *)this->itemWidget(currentItem);
						if (currentWidget)
						{
							if (widget->getLastTime() > currentWidget->getLastTime())
							{
								item = currentItem;
								widget = currentWidget;
							}
						}
					}
				}
				QString deleteID = widget->mNickName->objectName();
				this->takeItem(this->row(item));
			}
		}
	}

	QString strNick = buddyInfo.strNote.isEmpty() ? buddyInfo.strNickName : buddyInfo.strNote;

	this->OnInsertMessage(QString::number(buddyInfo.nUserId),
		buddyInfo.strLocalAvatar,
		strNick,
		messageinfo.strMsg, messageinfo.ClientTime, "");

	return messageListInfo;
}

MessageListInfo MessageList::OnClickGroupMessage(MessageInfo messageinfo, GroupInfo groupInfo)
{
	QString strtemp = QString("%1").arg(messageinfo.ClientTime);
	if (strtemp.length() == 13)
		messageinfo.ClientTime = messageinfo.ClientTime / 1000;

	messageinfo.strMsg = OnDealMessageType(messageinfo);

	if (!messageinfo.strMsg.isEmpty())
	{
		QString fromID = QString::number(messageinfo.nFromUserID);
		QString groupID = QString::number(messageinfo.nToUserID);
		BuddyInfo buddyInfo = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(groupID, fromID);
		if (buddyInfo.strNote.isEmpty())
			messageinfo.strMsg = buddyInfo.strNickName + ":" + messageinfo.strMsg;
		else
			messageinfo.strMsg = buddyInfo.strNote + ":" + messageinfo.strMsg;
	}

	MessageListInfo messageListInfo;
	messageListInfo.nBudyyID = groupInfo.groupId.toInt();
	messageListInfo.strLastMessage = messageinfo.strMsg;
	messageListInfo.nLastTime = messageinfo.ClientTime;
	messageListInfo.strBuddyName = groupInfo.groupName;
	messageListInfo.strBuddyHeaderImage = groupInfo.groupLoacalHeadImage;
	messageListInfo.messageType = messageinfo.MessageChildType;
	messageListInfo.nUnReadNum = 0;
	messageListInfo.isGroup = 1;

	//首先判断列表中是否存在
	for (int i = 0; i < this->count(); i++)
	{
		QListWidgetItem *item = this->item(i);
		if (item)
		{
			CFrientItemMessage *buddy = (CFrientItemMessage*)this->itemWidget(item);
			if (buddy != NULL)
			{
				if (buddy->mNickName->objectName() == groupInfo.groupId)
				{
					item->setSelected(true);
					buddy->OnSetMessageNum("");
					return messageListInfo;
				}	
			}
		}
	}

	if (this->count() >= 30)
	{
		QListWidgetItem *item = this->item(0);
		if (item)
		{
			CFrientItemMessage *widget = (CFrientItemMessage *)this->itemWidget(item);
			if (widget)
			{
				for (int j = 1; j < this->count(); j++)
				{
					QListWidgetItem *currentItem = this->item(j);
					if (currentItem)
					{
						CFrientItemMessage *currentWidget = (CFrientItemMessage *)this->itemWidget(currentItem);
						if (currentWidget)
						{
							if (widget->getLastTime() > currentWidget->getLastTime())
							{
								item = currentItem;
								widget = currentWidget;
							}
						}
					}
				}
		QString deleteID = widget->mNickName->objectName();
		this->takeItem(this->row(item));
			}
		}
	}

	this->OnInsertMessage(groupInfo.groupId,
		groupInfo.groupLoacalHeadImage,
		groupInfo.groupName,
		messageinfo.strMsg, messageinfo.ClientTime, "", 1);

	return messageListInfo;
}

QString MessageList::OnDealMessageType(MessageInfo StInfo)
{
	QString strValue = "";
	//根据消息类型设置显示的内容
	if (StInfo.MessageChildType == MessageType::Message_TEXT)
		strValue = StInfo.strMsg;
	if (StInfo.MessageChildType == MessageType::Message_PIC) //1图片3视频5文件。
		strValue = QStringLiteralBak("[图片]");
	if (StInfo.MessageChildType == MessageType::Message_AUDIO)
		strValue = QStringLiteralBak("[音频]");
	if (StInfo.MessageChildType == MessageType::Message_VEDIO) //1图片3视频5文件。
		strValue = QStringLiteralBak("[视频]");
	if (StInfo.MessageChildType == MessageType::Message_FILE) //1图片3视频5文件。
		strValue = QStringLiteralBak("[文件]");
	if (StInfo.MessageChildType == MessageType::Message_AD)
		strValue = QStringLiteralBak("该消息类型暂不支持");
	if (StInfo.MessageChildType == MessageType::Message_TRANSFER)
		strValue = QStringLiteralBak("[转账]");
	if (StInfo.MessageChildType == MessageType::Message_SECRETLETTER)
		strValue = QStringLiteralBak("[密信]");
	if (StInfo.MessageChildType == MessageType::Message_SECRETIMAGE)
		strValue = QStringLiteralBak("[密图]");
	if (StInfo.MessageChildType == MessageType::Message_SECRETFILE)
		strValue = QStringLiteralBak("[密件]");
	if (StInfo.MessageChildType == MessageType::Message_NOTICE)  //通告消息
	{
		QJsonDocument doc = QJsonDocument::fromJson(StInfo.strMsg.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		QString webTitle = map.value("webTitle").toString();
		strValue = QStringLiteralBak("[通告]") + webTitle;
	}
	if (StInfo.MessageChildType == MessageType::Message_LOCATION)  //位置消息。
		strValue = QStringLiteralBak("[位置]");
	if (StInfo.MessageChildType == MessageType::Message_GW)
		strValue = QStringLiteralBak("[引力波]");
	if (StInfo.MessageChildType == MessageType::Message_NOTIFY)  //通知消息
	{
		QJsonDocument doc = QJsonDocument::fromJson(StInfo.strMsg.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		if (map.value("type").toString() == "notification")
		{
			strValue = map.value("content").toString();
		}
		if (map.value("type").toString() == "receivedFile")
		{
			strValue = QStringLiteralBak("对方已成功接收文件") + QString("\"%1\"").arg(map.value("fileName").toString());
		}
	}
	if (StInfo.MessageChildType == MessageType::Message_COMMON)  //通用消息
	{
		QJsonDocument doc = QJsonDocument::fromJson(StInfo.strMsg.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		QString systemName = map.value("systemName").toString();
		strValue = QString("[%1]").arg(systemName);
	}

	return strValue;
}

void MessageList::OnInsertMessage(QString strUserID,
	QString strPicPath,
	QString strNickName,
	QString strAutoGrapthText,
	int MessageTime,
	QString strMessageNum,
	int nType,
	int nHeight)
{
	//先对列表中是否存在进行判断。
	for (int i = 0; i < this->count(); i++)
	{
		QListWidgetItem *item = this->item(i);
		if (item)
		{
			CFrientItemMessage *message = (CFrientItemMessage *)this->itemWidget(item);
			if (message)
			{
				//如果列表中存在，直接退出。
				if (message->mNickName->objectName() == strUserID)
				return;
			}
		}
	}

	CFrientItemMessage *buddy = new CFrientItemMessage(this);
	buddy->resize(this->width(), nHeight);
	buddy->OnInitMessage(strUserID);
	buddy->OnSetPicPath(strPicPath, nType);
	buddy->OnSetNickNameText(strNickName);
	buddy->OnSetAutoGrapthText(strAutoGrapthText);
	buddy->OnSetMessageTime(MessageTime);
	buddy->OnSetMessageNum(strMessageNum);
	QListWidgetItem *newItem = new QListWidgetItem(); //创建一个newItem
	//newItem->setSizeHint(QSize(this->width(),nHeight));
    newItem->setData(Qt::UserRole,QVariant(QString::number(nType)));
	newItem->setSizeHint(QSize(this->width(), 58));
	this->insertItem(0, newItem); //将该newItem插入到后面
	this->setItemWidget(newItem, buddy); //将buddy赋给该newItem
	if (strAutoGrapthText.isEmpty() || strMessageNum.isEmpty())
	{
		newItem->setSelected(true);
		this->setCurrentRow(0);
	}
	    
	connect(buddy, SIGNAL(sigCurrentChatClose(QString)), this, SIGNAL(sigCloseChat(QString)));
	connect(buddy, SIGNAL(sigChangeMessageNum()), this, SLOT(slotCountMessageNum()));
	connect(buddy, SIGNAL(sigCurrentChatClose(QString)), this, SLOT(slotCountMessageNum()));

	slotCountMessageNum();
}

void MessageList::keyPressEvent(QKeyEvent *event)
{
	if (event->key() == Qt::Key_Up)
	{
		if (this->count() > 0)
		{
			if (this->currentRow() < 0)
			{
				QListWidgetItem *item = this->item(0);
				if (item)
				{
					item->setSelected(true);
					this->setCurrentItem(item);
					this->doDoubleClickedItem(item);
				}
			}
			else
			{
				if (this->currentRow() != 0)
				{
					QListWidgetItem *item = this->item(this->currentRow() - 1);
					if (item)
					{
						item->setSelected(true);
						this->setCurrentItem(item);
						this->doDoubleClickedItem(item);
					}
				}
			}
		}
	}
	if (event->key() == Qt::Key_Down)
	{
		if (this->count() > 0)
		{
			if (this->currentRow() < 0)
			{
				QListWidgetItem *item = this->item(0);
				//item->setFlags()
				if (item)
				{
					item->setSelected(true);
					this->setCurrentItem(item);
					this->doDoubleClickedItem(item);
				}
			}
			else
			{
				if (this->currentRow() != this->count() - 1)
				{
					QListWidgetItem *item = this->item(this->currentRow() + 1);
					if (item)
					{
						item->setSelected(true);
						this->setCurrentItem(item);
						this->doDoubleClickedItem(item);
					}
				}
			}
		}
	}

	if (event->key() == Qt::Key_Return)
	{
		QListWidgetItem *item = this->currentItem();
		if (item)
			this->doDoubleClickedItem(item);
	}

	return QWidget::keyPressEvent(event);
}

void MessageList::enterEvent(QEvent * event)
{
	this->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
}

void MessageList::leaveEvent(QEvent * event)
{
	this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

void MessageList::doDoubleClickedItem(QListWidgetItem *item)
{
	int nCurrentItem = this->currentRow();
	MessageInfo info;
	CFrientItemMessage *buddy = (CFrientItemMessage*)this->itemWidget(item);
	if (buddy == NULL)
	{
		return;
	}
	if (buddy->mNickName)
	{
		QString strUserID = buddy->mNickName->objectName();
		buddy->OnSetMessageNum("");
		QString strNikeName = buddy->GetNikeName();

		emit sigDoubleClickItem(strUserID);
		emit sigDealTrayIocnFlash(strUserID);
	}
}

MessageListInfo MessageList::getCurrentItemInfo()
{
    MessageListInfo listInfo;
    listInfo.nBudyyID = -1;
    QListWidgetItem *item = this->currentItem();
    if(item)
    {
        CFrientItemMessage *buddy = (CFrientItemMessage*)this->itemWidget(item);
        if(buddy)
        {
            QString strUserID = buddy->mNickName->objectName();
            listInfo.nBudyyID = strUserID.toInt();
        }
        QString type = item->data(Qt::UserRole).toString();
        listInfo.isGroup = type.toInt();
    }
    return listInfo;
}

void MessageList::OnUpdateMessage(bool isGroup, QVariant var)
{
	if (isGroup)
	{
		GroupInfo groupInfo = var.value<GroupInfo>();
		for (int i = 0; i < this->count(); i++)
		{
			CFrientItemMessage *message = (CFrientItemMessage *)this->itemWidget(this->item(i));
			if (message)
			{
				if (message->mNickName->objectName() == groupInfo.groupId)
				{
					message->OnSetPicPath(groupInfo.groupLoacalHeadImage, isGroup);
					message->OnSetNickNameText(groupInfo.groupName);
				}
			}
		}
	}
	else
	{
		BuddyInfo buddyInfo = var.value<BuddyInfo>();
		for (int i = 0; i < this->count(); i++)
		{
			CFrientItemMessage *message = (CFrientItemMessage *)this->itemWidget(this->item(i));
			if (message)
			{
				if (message->mNickName->objectName() == QString::number(buddyInfo.nUserId))
				{
					message->OnSetPicPath(buddyInfo.strLocalAvatar, isGroup);
					if (buddyInfo.strNote.isEmpty())
						message->OnSetNickNameText(buddyInfo.strNickName);
					else
						message->OnSetNickNameText(buddyInfo.strNote);
				}
			}
		}
	}
}

void MessageList::slotCountMessageNum()
{
	getMessageUnreadNum();
}

int MessageList::getMessageUnreadNum()
{
	int count = 0;

	for (int i = 0; i < this->count(); i++)
	{
		CFrientItemMessage *message = (CFrientItemMessage *)this->itemWidget(this->item(i));
		if (message)
			count += message->getMessageNum();
	}

	emit sigMessageNum(count);

	return count;
}

void MessageList::slotKeyUpDown(QKeyEvent *event)
{
	this->keyPressEvent(event);
}

void MessageList::slotInsertSelfMessage(bool isGroup, QVariant var, QString msg,bool bNeedOpen)
{
	if (isGroup)
	{
		GroupInfo groupInfo = var.value<GroupInfo>();
		for (int i = 0; i < this->count(); i++)
		{
			CFrientItemMessage *message = (CFrientItemMessage *)this->itemWidget(this->item(i));
			if (message)
			{
				if (message->mNickName->objectName() == groupInfo.groupId)
				{
					QString strOpen = "";
					int unreadMsgCount = message->getMessageNum();
					if (!bNeedOpen)
					{
						strOpen = QString::number(unreadMsgCount);
					}
					int time = QDateTime::currentDateTime().toTime_t();
					this->takeItem(i);
					this->OnInsertMessage(groupInfo.groupId,
					groupInfo.groupLoacalHeadImage,
					groupInfo.groupName,
					msg, time, strOpen, 1);
				}
			}
		}
	}
	else
	{
		BuddyInfo buddyInfo = var.value<BuddyInfo>();
		for (int i = 0; i < this->count(); i++)
		{
			CFrientItemMessage *message = (CFrientItemMessage *)this->itemWidget(this->item(i));
			if (message)
			{
				if (message->mNickName->objectName() == QString::number(buddyInfo.nUserId))
				{
					int time_t = QDateTime::currentDateTime().toTime_t();

					QString strNick;
					if (buddyInfo.strNote.isEmpty())
						strNick = buddyInfo.strNickName;
					else
						strNick = buddyInfo.strNote;

					QString strOpen = "";
					int unreadMsgCount = message->getMessageNum();
					if (!bNeedOpen)
					{
						strOpen = QString::number(unreadMsgCount);
					}
					this->takeItem(i);
					this->OnInsertMessage(QString::number(buddyInfo.nUserId),
					buddyInfo.strLocalAvatar,
					strNick,
					msg, time_t, strOpen, 0);
				}
			}
		}
		

	}
}

void MessageList::onDeleteItem(int row)
{
	QListWidgetItem *aimItem = this->item(row);
	if (aimItem)
		this->takeItem(row);

	//点击下一项。
	if (this->count() > 0)
	{
		QListWidgetItem *item = this->item(0);
		if (item)
		{
			item->setSelected(true);
			this->doDoubleClickedItem(item);
		}
	}
}

