﻿#include "messageliststore.h"
#include "imdatamanagersharelib.h"
#include "imdatabaseoperainfo.h"
#include "imsocketmessageinfo.h"
#include "messagelistviewmodel.h"
#include "messagelistdispatcher.h"
#include "messagelistview.h"
#include "httpnetworksharelib.h"
#include "contactsdatamanager.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonParseError>
#include <QJsonValue>
#include <QFile>
#include <QDateTime>
#include <QCoreApplication>
#include <QMenu>

extern IMDataBaseOperaInfo *gDataBaseOpera;
extern IMDataManagerShareLib *gDataManager;
extern IMSocketMessageInfo *gSocketMessage;

MessageListStore* MessageListStore::s_instance = NULL;

MessageListStore::MessageListStore(QObject *parent) : QObject(parent), m_vm(NULL), m_dispatcher(NULL)
{
	s_instance = this;
	m_menuTimer = new QTimer(this);

	backToCurrentFlag = true;
}

MessageListStore* MessageListStore::instance()
{
	return s_instance;
}

QString MessageListStore::msgJsonToDesc(QString strJson)
{
	//将strJson转成可能带<font>标签的描述信息
	QString strRet;

	QJsonParseError jsonError;
	QJsonDocument doucment = QJsonDocument::fromJson(strJson.toUtf8(), &jsonError);  // 转化为 JSON 文档
	if (!doucment.isNull() && (jsonError.error == QJsonParseError::NoError))
	{
		QJsonArray jsonArr = doucment.array();

		for (int i = 0; i < jsonArr.count(); i++)
		{
			QJsonObject object = jsonArr.at(i).toObject();
			if (object.value("type").toInt() == MessageListStore::MsgJsonText)
			{
				strRet += object.value("msg").toString();
			}
			else if (object.value("type").toInt() == MessageListStore::MsgJsonAt && i == 0)
			{
				strRet += QObject::tr("<font color=\'#f7931e\' >[I was @]</font>");
			}
			else if (object.value("type").toInt() == MessageListStore::MsgJsonAt && i == 1)
			{
				strRet = QObject::tr("<font color=\'#f7931e\' >[I was @]</font>") + strRet;
			}
		}
	}
	return strRet;
}

void MessageListStore::clickIndex(int idx)
{
	m_vm->doClickIndex(idx);
}

void MessageListStore::clickItemByUserId(QString strUserId)
{
	QString userId = strUserId;

	UserInfo userInfo = gDataManager->getUserInfo();
	//点击的时候先把标志清空（之前上传文件失败后，再点击如果不清空，会导致失败标志一直存在）
	//如果点击的是最后一条消息且该消息发送失败，则不用隐藏失败标志
	if (!m_lastMsgIdFailureInfoMap[userId])
	{
		int idx = m_vm->getIndexByUserId(userId);
		if (idx >= 0)
		{
			m_vm->resetSendStatusByIndex(idx);
		}
	}


	//用户点击了，因为有可能有@成员信息这些类型，点击后需要去除，所以需要更新下消息文字
	QString lastMsgDesc;
	QJsonArray jsonClickArr;//点击后去除@成员等类型后的数组
	QString lastMsgJson = gDataBaseOpera->DBGetLastMsgJsonInMessageList(userId);
	QJsonParseError jsonError;
	int msgTypeToUpdate = 0;
	QJsonDocument doucment = QJsonDocument::fromJson(lastMsgJson.toUtf8(), &jsonError);  // 转化为 JSON 文档
	if (!doucment.isNull() && (jsonError.error == QJsonParseError::NoError))
	{
		QJsonArray jsonArr = doucment.array();
		for (int i = 0; i < jsonArr.count(); i++)
		{
			QJsonObject object = jsonArr.at(i).toObject();

			if (object.value("type").toInt() == MsgJsonText)
			{
				jsonClickArr.append(object);
				lastMsgDesc += object.value("msg").toString();

				QList<MessageListInfo> listInfo = gDataBaseOpera->DBGetALLMessageListInfo();
				for (int j = 0; j < listInfo.size(); j++)
				{
					if (listInfo[j].nBudyyID == userId.toInt())
					{
						QString fUserbUFF, toUserBuff, lastMsgInInfo;
						QString buddyName;
						//如果消息不为本地端发送 则不走翻译接口
						lastMsgInInfo = gSocketMessage->DBGetLastSysMsgInMessageInfo(listInfo[j].nBudyyID, fUserbUFF, toUserBuff, listInfo[j].messageType);
						msgTypeToUpdate = listInfo[j].messageType;
						listInfo[j].strLastMessage.replace("\n", "");

						//若为群组 则增加发言人名字 文本消息不作处理
						if (listInfo[j].isGroup == 1)
						{
							int fromUser = gSocketMessage->DBGetFromUserOfLastMsgInMessageList(listInfo[j].nBudyyID, listInfo[j].messageType);
							BuddyInfo buddyInfo = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(QString::number(listInfo[j].nBudyyID), QString::number(fromUser));
							QString buddyName = (buddyInfo.strNote == "" || buddyInfo.strNote == NULL) ? buddyInfo.strNickName : buddyInfo.strNote;
						}
						if (listInfo[j].messageType == Message_AT)
							break;
						else if (listInfo[j].messageType == Message_NOTIFY)
						{
							if (userInfo.nUserID != fUserbUFF.toInt())
								break;
							if (lastMsgInInfo != NULL)
							{
								QJsonDocument doc = QJsonDocument::fromJson(lastMsgInInfo.toUtf8());
								QVariantMap map = doc.toVariant().toMap();
								if (map.value("type").toString() == "notification")
								{
									QString inviterID = map.value("inviterID").toString();
									QString invitedID = map.value("invitedID").toString();
									//若为移动端等发来的消息，则不作处理
									if ((inviterID == "0" || inviterID == NULL || inviterID == "") && (invitedID == "0" || invitedID == NULL || invitedID == ""))
										lastMsgDesc = map.value("content").toString();
									else if (inviterID == "0" || inviterID == NULL || inviterID == "")
										lastMsgDesc = invitedID + tr(" joined the group");
									else
										lastMsgDesc = inviterID + tr(" invited ") + invitedID + tr(" to join the group");

								}
								if (map.value("type").toString() == "receivedFile")
								{
									if (fUserbUFF.toInt() == userInfo.nUserID)
										lastMsgDesc = tr("File received successfully ") + QString("\"%1\"").arg(map.value("fileName").toString());
									else if (toUserBuff.toInt() == userInfo.nUserID)
										lastMsgDesc = tr("The other party has successfully received your file ") + QString("\"%1\"").arg(map.value("fileName").toString());
									QJsonObject object;
									object.insert("type", 0);
									object.insert("msg", lastMsgDesc);
									QJsonArray jsonarray;
									jsonarray.append(object);
									QJsonDocument document;
									document.setArray(jsonarray);
									QString strClickJson = (QString)document.toJson();
									gDataBaseOpera->DBUpdateLastMsgJsonInMessageList(QString::number(listInfo[j].nBudyyID), strClickJson, listInfo[j].messageType);
									listInfo[j].strLastMessage = lastMsgDesc;
								}
							}
						}
						else if (listInfo[j].messageType == Message_TEXT)
						{
							lastMsgDesc = object.value("msg").toString();
						}
						else
						{
							//获取发言人名字
							if (listInfo[j].isGroup == 1)
							{
								int fromUser = gSocketMessage->DBGetFromUserOfLastMsgInMessageList(listInfo[j].nBudyyID, listInfo[j].messageType);
								BuddyInfo buddyInfo = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(QString::number(listInfo[j].nBudyyID), QString::number(fromUser));
								buddyName = (buddyInfo.strNote == "" || buddyInfo.strNote == NULL) ? buddyInfo.strNickName : buddyInfo.strNote;
							}

							switch (listInfo[j].messageType)
							{
							case Message_NOTICE:
							{
								lastMsgInInfo = gSocketMessage->DBGetLastSysMsgInMessageInfo(listInfo[j].nBudyyID, fUserbUFF, toUserBuff, Message_NOTICE);
								QJsonDocument doc = QJsonDocument::fromJson(lastMsgInInfo.toUtf8());
								QVariantMap map = doc.toVariant().toMap();
								QString webTitle = map.value("webTitle").toString();
								if (listInfo[j].isGroup == 1)
									lastMsgDesc = buddyName + ":" + tr("[Announcement]") + webTitle;
								else
								{
									lastMsgDesc = tr("[Announcement]") + webTitle;
								}
								break;
							}
							case Message_URL:
							{
								lastMsgInInfo = gSocketMessage->DBGetLastSysMsgInMessageInfo(listInfo[j].nBudyyID, fUserbUFF, toUserBuff, Message_URL);
								QJsonDocument doc = QJsonDocument::fromJson(lastMsgInInfo.toUtf8());
								QVariantMap map = doc.toVariant().toMap();
								QString webTitle = map.value("subject").toString();
								if (listInfo[j].isGroup == 1)
									lastMsgDesc = buddyName + ":" + tr("[Share]") + webTitle;
								else
								{
									lastMsgDesc = tr("[Share]") + webTitle;
								}
								break;
							}
							case Message_COMMON:
							{
								QString msgTitle;
								lastMsgInInfo = gSocketMessage->DBGetLastSysMsgInMessageInfo(listInfo[j].nBudyyID, fUserbUFF, toUserBuff, Message_COMMON);
								QJsonDocument doc = QJsonDocument::fromJson(lastMsgInInfo.toUtf8());
								QVariantMap map = doc.toVariant().toMap();
								if (map.isEmpty())
									msgTitle = lastMsgInInfo;
								else
								{
									QString systemName = map.value("systemName").toString();
									msgTitle = QString("[%1]").arg(systemName);
								}
								if (listInfo[j].isGroup == 1)
									lastMsgDesc = buddyName + ":" + msgTitle;
								else
								{
									lastMsgDesc = msgTitle;
								}
								break;
							}
							default:
							{
								MessageInfo messageinfo;
								messageinfo.MessageChildType = listInfo[j].messageType;
								messageinfo.strMsg = listInfo[j].strLastMessage;
								QString buff01, buff02;
								if (listInfo[j].isGroup == 1)
								{
									QString strLastBuff = this->OnDealMessageType(messageinfo, buff01, buff02);
									lastMsgDesc = buddyName + ":" + strLastBuff;
								}
								else
								{
									lastMsgDesc = this->OnDealMessageType(messageinfo, buff01, buff02);
								}
								break;

							}
							}
						}
					}
				}
			}
		}
	}

	//用户点击后，还要更新下MESSAGELIST表中的LastMsgJson字段，里面去除@成员等相关信息
	QJsonDocument document;
	document.setArray(jsonClickArr);
	QString strClickJson = (QString)document.toJson();
	if (msgTypeToUpdate == Message_AT)
		msgTypeToUpdate = Message_TEXT;
	strClickJson.replace("\n", "");
	gDataBaseOpera->DBUpdateLastMsgJsonInMessageList(userId, strClickJson, msgTypeToUpdate);


	//更新消息
	int idx = m_vm->getIndexByUserId(userId);
	if (idx >= 0)
	{
		m_vm->updateMsgByIndex(idx, lastMsgDesc);
	}

	emitMessageUnreadNum();

	emit m_dispatcher->sigDealTrayIocnFlash(userId);
}

void MessageListStore::rightClickItemByUserId(QString strUserId)
{
	QString userId = strUserId;

	m_menuTimer->disconnect();
	m_menuTimer->stop();
	m_menuTimer->setSingleShot(true);
	connect(m_menuTimer, &QTimer::timeout, this, [this, userId]() {
		QCoreApplication::processEvents();

		int topOrder;
		int type;
		int currIndex;
		int msgPrompt;
		int currId;
		QString ifIsGroupId;
		int iUserId = userId.toInt();

		MessageListView* view = reinterpret_cast<MessageListView*>(m_vm->parent());

		QMenu *rightMenu = new QMenu(view);
		QFile style(":/QSS/Resources/QSS/ChatWidgetShareLib/messageliststoremenu.qss");
		style.open(QFile::ReadOnly);
		QString sheet = QLatin1String(style.readAll());
		rightMenu->setStyleSheet(sheet);
		style.close();
// 		rightMenu->setStyleSheet("QMenu{border:none;background-color: #72a4d6;color: #042439;}\
// 			QMenu::item{padding:4px;padding-left:10px;padding-right:10px;}\
// 			QMenu::item:selected{color: white;border:none;padding:4px;padding-left:10px;padding-right:10px;}");

		if (userId == NULL || userId == "")
			return;


		QAction *viewDetails = new QAction(tr("View Details"), this);
		connect(viewDetails, &QAction::triggered, this, [&] {
			emit m_dispatcher->sigViewDetails(userId, m_vm->getItemTypeByUserId(userId) == 0);
		});
		QAction *closeChat = new QAction(tr("Close Chat"), this);
		connect(closeChat, &QAction::triggered, this, [=] {
			emit m_dispatcher->sigCloseChatQuery(userId);
		});
		QAction *closeAllChat = new QAction(tr("Close All Chats"), this);
		connect(closeAllChat, &QAction::triggered, this, [=] {
			emit m_dispatcher->sigCloseAllChatQuery();
		});

		int idx = m_vm->getIndexByUserId(userId);
		if (idx >= 0)
		{
			currId = iUserId;
			currIndex = idx;
			topOrder = m_vm->getMsgTopOrderByIndex(idx);
			type = m_vm->getItemTypeByIndex(idx);
			msgPrompt = gDataBaseOpera->DBGetMsgPromptFlagInInfoTables(currId, type);
		}

		if (topOrder > 0)
		{
			QAction *messageCancleTop = new QAction(tr("Remove From Top"), this);
			connect(messageCancleTop, &QAction::triggered, this, [=] {
				emit m_dispatcher->sigCancleMessageTop(userId);
			});
			rightMenu->addAction(messageCancleTop);
		}
		else
		{
			QAction *messageTop = new QAction(tr("Set Message Top"), this);
			connect(messageTop, &QAction::triggered, this, [=] {
				emit m_dispatcher->sigMessageTop(userId);
			});
			rightMenu->addAction(messageTop);
		}
		if (msgPrompt == 1)
		{
			QAction *messagePrompt = new QAction(tr("Cancle No-Disturbing"), this);
			connect(messagePrompt, &QAction::triggered, this, [=] {
				emit m_dispatcher->sigMessagePrompt(userId);
			});
			rightMenu->addAction(messagePrompt);
		}
		else
		{
			QAction *messageNoPrompt = new QAction(tr("Set No-Disturbing"), this);
			connect(messageNoPrompt, &QAction::triggered, this, [=] {
				emit m_dispatcher->sigMessageNoPrompt(userId);
			});
			rightMenu->addAction(messageNoPrompt);
		}
		rightMenu->addAction(viewDetails);
		rightMenu->addAction(closeChat);
		rightMenu->addAction(closeAllChat);

		if (type == 1)
		{
			ifIsGroupId = m_vm->getUserIdByIndex(currIndex);
			GroupInfo gInfo = gDataBaseOpera->DBGetGroupFromID(ifIsGroupId);
			UserInfo info = gDataManager->getUserInfo();
			if (gInfo.createUserId.toInt() == info.nUserID)
			{
				//管理员显示解散群
				QAction *dissolveGroup = new QAction(tr("Dismiss Group"), this);
				connect(dissolveGroup, &QAction::triggered, this, [=] {
					emit m_dispatcher->sigDissolveGroup(userId);
				});
				rightMenu->addAction(dissolveGroup);
			}
			else
			{
				//成员显示退群
				QAction *userquitgroup = new QAction(tr("Quit Group"), this);
				connect(userquitgroup, &QAction::triggered, this, [=] {
					emit m_dispatcher->sigUserQuitGroup(userId);
				});
				rightMenu->addAction(userquitgroup);
			}
		}
		rightMenu->exec(QCursor::pos());
		delete rightMenu;

	});
	m_menuTimer->start(0);
}

void MessageListStore::closeChat(QString strUserId)
{
	gDataBaseOpera->DBDeleteMessageByID(strUserId);
	m_vm->deleteByUserId(strUserId);
	emit m_dispatcher->sigSetMsgRead(strUserId);//关闭聊天窗口时强制设置成已读
	emitMessageUnreadNum();

	if (m_vm->rowCount() > 0)
	{
		m_vm->doClickIndex(0);
	}
}

void MessageListStore::closeAllChat()
{
	for (int i = m_vm->count(); i >= 0; i--)
	{
		QString currId = m_vm->getUserIdByIndex(i);
		m_vm->deleteByIndex(i);

		gDataBaseOpera->DBDeleteMessageByID(currId);

		//设置全部会话已读
		emit m_dispatcher->sigSetMsgRead(currId);
	}

	emitMessageUnreadNum();
}

void MessageListStore::doUpDownKeyClick(bool isUp)
{
	m_vm->doUpDownKeyClick(isUp);
}

QString MessageListStore::getValidImagePath(QString imgPath, bool isPer, QString headProvider)
{
	QString retPath = headProvider + imgPath;

	bool isValid = true;
	QFile file(imgPath);
	if (file.open(QIODevice::ReadOnly))
	{
		QByteArray byteArray = file.readAll();
		file.close();
		QImage image = QImage::fromData(byteArray);

		if (image.isNull())
		{
			isValid = false;
		}
	}
	else
	{
		isValid = false;
	}

	if (!isValid)
	{
		if (isPer)//个人
		{
			retPath = headProvider + QStringLiteral(":/PerChat/Resources/person/temp.png");
		}
		else //群组
		{
			retPath = headProvider + QStringLiteral(":/GroupChat/Resources/groupchat/group.png");
		}
	}

	return retPath;
}

QString MessageListStore::getHumanReadableChatTime(int MessageTime)
{
	QDateTime dateTime = QDateTime::fromTime_t(MessageTime);
	QString strTime = dateTime.toString("hh:mm");

	if (dateTime.daysTo(QDateTime::currentDateTime()) >= 2)
		strTime = dateTime.toString("M-d ") + strTime;
	else
	{
		if (dateTime.daysTo(QDateTime::currentDateTime()) >= 1)
			strTime = tr("Yesterday") + strTime;
	}

	return strTime;
}


QString MessageListStore::OnDealMessageType(MessageInfo StInfo, QString& strJson, QString& strPlainText)
{
	//返回值如果有@成员的话，可能会带有<font>字体内容
	//strJson返回消息内容的JSON表示格式，与MESSAGELIST的LastMsgJson一致
	//strPlainText返回普通的文本，后面可能需要在它前面加上发送者姓名：普通文本
	QJsonArray jsonArr;

	QString strValue = "";
	//根据消息类型设置显示的内容
	if (StInfo.MessageChildType == MessageType::Message_TEXT)
		strValue = StInfo.strMsg;
	if (StInfo.MessageChildType == MessageType::Message_PIC) //1图片3视频5文件。
		strValue = tr("[Image]");
	if (StInfo.MessageChildType == MessageType::Message_AUDIO)
		strValue = tr("[Audio]");
	if (StInfo.MessageChildType == MessageType::Message_VEDIO) //1图片3视频5文件。
		strValue = tr("[Video]");
	if (StInfo.MessageChildType == MessageType::Message_FILE) //1图片3视频5文件。
		strValue = tr("[File]");
	if (StInfo.MessageChildType == MessageType::Message_AD)
		strValue = tr("This type of message is not supported for now");
	if (StInfo.MessageChildType == MessageType::Message_TRANSFER)
		strValue = tr("[Tranfer]");
	if (StInfo.MessageChildType == MessageType::Message_REDBAG)
		strValue = tr("[Red Packet]");
	if (StInfo.MessageChildType == MessageType::Message_SECRETLETTER)
		strValue = tr("[Secret Message]");
	if (StInfo.MessageChildType == MessageType::Message_SECRETIMAGE)
		strValue = tr("[Secret Image]");
	if (StInfo.MessageChildType == MessageType::Message_SECRETFILE)
		strValue = tr("[Secret File]");
	if (StInfo.MessageChildType == MessageType::Message_LOCATION)  //位置消息。
		strValue = tr("[Location]");
	if (StInfo.MessageChildType == MessageType::Message_GW)
		strValue = tr("[Gravitational Waves]");
	if (StInfo.MessageChildType == MessageType::Message_NOTICE)  //通告消息
	{
		QJsonDocument doc = QJsonDocument::fromJson(StInfo.strMsg.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		QString webTitle = map.value("webTitle").toString();
		strValue = tr("[Announcement]") + webTitle;
	}
	if (StInfo.MessageChildType == MessageType::Message_URL)  //分享消息
	{
		QJsonDocument doc = QJsonDocument::fromJson(StInfo.strMsg.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		QString subject = map.value("subject").toString();
		strValue = tr("[Share]") + subject;
	}
	if (StInfo.MessageChildType == MessageType::Message_NOTIFY)  //通知消息
	{
		QJsonDocument doc = QJsonDocument::fromJson(StInfo.strMsg.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		if (map.value("type").toString() == "notification")
		{
			strValue = map.value("content").toString();
		}
		if (map.value("type").toString() == "receivedFile")
		{
			if (StInfo.nFromUserID == gDataManager->getUserInfo().nUserID)
			{
				strValue = tr("File received successfully ") + QString("\"%1\"").arg(map.value("fileName").toString());
			}
			else
			{
				strValue = tr("The other party has successfully received your file ") + QString("\"%1\"").arg(map.value("fileName").toString());
			}
		}
	}
	if (StInfo.MessageChildType == MessageType::Message_COMMON)  //通用消息
	{
		QJsonDocument doc = QJsonDocument::fromJson(StInfo.strMsg.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		if (map.isEmpty())
			strValue = StInfo.strMsg;
		else
		{
			QString systemName = map.value("systemName").toString();
			strValue = QString("[%1]").arg(systemName);
		}
	}
	if (StInfo.MessageChildType == MessageType::Message_AT)
	{
		QJsonDocument doc = QJsonDocument::fromJson(StInfo.strMsg.toUtf8());
		QVariantMap map = doc.toVariant().toMap();

		QString content = map.value("content").toString();


		QVariantList buddyList = map.value("ID").toList();
		foreach(QVariant var, buddyList)
		{
			QVariantMap map = var.toMap();
			int id = map.value("userId").toInt();
			if (id == gDataManager->getUserInfo().nUserID)
			{
				QJsonObject object;
				object.insert("type", MsgJsonAt);
				jsonArr.append(object);

				break;
			}
		}

		strValue = content;
	}

	strPlainText = strValue;

	QJsonObject object;
	object.insert("type", 0);
	object.insert("msg", strValue);

	jsonArr.append(object);

	QJsonDocument document;
	document.setArray(jsonArr);
	strJson = (QString)document.toJson();

	QString strRet;

	for (int i = 0; i < jsonArr.count(); i++)
	{
		QJsonObject object = jsonArr.at(i).toObject();
		if (object.value("type").toInt() == MsgJsonText)
		{
			strRet += object.value("msg").toString();
		}
		else if (object.value("type").toInt() == MsgJsonAt && i == 0)
		{
			strRet += tr("<font color=\'#f7931e\' >[I was @]</font>");
		}
		else if (object.value("type").toInt() == MsgJsonAt && i == 1)
		{
			strRet = tr("<font color=\'#f7931e\' >[I was @]</font>") + strRet;
		}

	}

	return strRet;
}

void MessageListStore::OnInsertMessage(QString strUserID, QString strPicPath, QString strNickName, QString strMsgContent, int MessageTime, QString strMessageNum, int msgTopOrder, int nType, bool bHasDraft)
{
	//先对列表中是否存在进行判断。
	if (m_vm->existUserId(strUserID))
	{
		//如果列表中存在，直接退出。
		return;
	}

	//判断strPicPath是否有效的图片，无效的话用默认图片
	QString headPicPath = getValidImagePath(strPicPath, nType == 0, QStringLiteral("image://HeadProvider/"));

	QString strTime = getHumanReadableChatTime(MessageTime);

	int msgPrompt = gDataBaseOpera->DBGetMsgPromptFlagInInfoTables(strUserID.toInt(), nType);//是否开启免打扰

																							 //插入时根据置顶信息判断插入位置
	MessageListItem item(nType, strUserID, headPicPath, strNickName, strMsgContent, strTime, strMessageNum.toInt(), msgTopOrder, msgPrompt, bHasDraft);
	m_vm->insertItemWithOrder(item);

	//重新执行setData以刷新头像缓存
	m_vm->updateHeadUrlByUserId(strUserID, headPicPath);

	if (!backToCurrentFlag)
	{
		int idx = m_vm->getIndexByUserId(backToCurrentID);
		if (idx >= 0)
		{
			m_vm->setCurrentIndex(idx);
			backToCurrentFlag = true;
		}
	}

	int iPromptFlag = gDataBaseOpera->DBGetMsgPromptFlagInInfoTables(strUserID.toInt(), nType);
	if (iPromptFlag != 1)
		emitMessageUnreadNum();
}

void MessageListStore::emitMessageUnreadNum()
{
	emit m_dispatcher->sigMessageUnreadNumChanged(m_vm->getAllUnreadNum());
}

void MessageListStore::SetSendStatusFailure(QString buddyOrGroupId, int idx)
{
	qDebug() << "SetSendStatusFailure() 设置发送失败标志！！！";
	m_lastMsgIdFailureInfoMap[buddyOrGroupId] = true;//避免点击的时候消掉

	m_vm->setSendStatusFailedByIndex(idx);
}
void MessageListStore::init()
{
	m_vm = MessageListViewModel::instance();
	m_dispatcher = MessageListDispatcher::instance();
}

void MessageListStore::initHistory()
{
	UserInfo userInfo = gDataManager->getUserInfo();
	QString strMyUserId = QString::number(userInfo.nUserID);
	QList<MessageListInfo> listInfo = gDataBaseOpera->DBGetALLMessageListInfo();
	for (int i = 0; i < listInfo.size(); i++)
	{
		QString senderID, recvID;
		QString strMsgInInfo;
		strMsgInInfo = gSocketMessage->DBGetLastSysMsgInMessageInfo(listInfo[i].nBudyyID, senderID, recvID, listInfo[i].messageType);
		QString content;
		QString buddyName;
		//若chatID对应messageinfo表最后一条消息为本地端所发送 则走翻译端口

		if (listInfo[i].messageType == Message_NOTIFY)
		{
			if (userInfo.nUserID == senderID.toInt())
			{
				if (strMsgInInfo != NULL)
				{
					QJsonDocument doc = QJsonDocument::fromJson(strMsgInInfo.toUtf8());
					QVariantMap map = doc.toVariant().toMap();
					if (map.value("type").toString() == "notification")
					{
						QString inviterID = map.value("inviterID").toString();
						QString invitedID = map.value("invitedID").toString();
						//若为移动端等发来的消息，则不作处理
						if ((inviterID == "0" || inviterID == NULL || inviterID == "") && (invitedID == "0" || invitedID == NULL || invitedID == ""))
							content = map.value("content").toString();
						else if (inviterID == "0" || inviterID == NULL || inviterID == "")
							content = invitedID + tr(" joined the group");
						else
							content = inviterID + tr(" invited ") + invitedID + tr(" to join the group");

					}
					if (map.value("type").toString() == "receivedFile")
					{
						if (senderID.toInt() == userInfo.nUserID)
							content = tr("File received successfully ") + QString("\"%1\"").arg(map.value("fileName").toString());
						else if (recvID.toInt() == userInfo.nUserID)
							content = tr("The other party has successfully received your file ") + QString("\"%1\"").arg(map.value("fileName").toString());
						QJsonObject object;
						object.insert("type", 0);
						object.insert("msg", content);
						QJsonArray jsonarray;
						jsonarray.append(object);
						QJsonDocument document;
						document.setArray(jsonarray);
						QString strClickJson = (QString)document.toJson();
						gDataBaseOpera->DBUpdateLastMsgJsonInMessageList(QString::number(listInfo[i].nBudyyID), strClickJson, listInfo[i].messageType);
						listInfo[i].strLastMessage = content;
					}
				}
			}
			else
				content = listInfo[i].strLastMessage;
		}
		else if (listInfo[i].messageType == Message_TEXT)
		{
			content = listInfo[i].strLastMessage;
		}
		else
		{
			//获取发言人名字
			QString fUserbUFF, toUserBuff;
			QString& lastMsgInInfo = strMsgInInfo;
			if (listInfo[i].isGroup == 1)
			{
				int fromUser = gSocketMessage->DBGetFromUserOfLastMsgInMessageList(listInfo[i].nBudyyID, listInfo[i].messageType);
				BuddyInfo buddyInfo = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(QString::number(listInfo[i].nBudyyID), QString::number(fromUser));
				buddyName = (buddyInfo.strNote == "" || buddyInfo.strNote == NULL) ? buddyInfo.strNickName : buddyInfo.strNote;
			}

			switch (listInfo[i].messageType)
			{
			case Message_TEXT:
			{
				content = listInfo[i].strLastMessage;
				if (listInfo[i].isGroup == 1)
				{
					content = buddyName + ":" + content;
				}
			}
			break;
			case Message_NOTICE:
			{
				QJsonDocument doc = QJsonDocument::fromJson(lastMsgInInfo.toUtf8());
				QVariantMap map = doc.toVariant().toMap();
				QString webTitle = map.value("webTitle").toString();
				if (listInfo[i].isGroup == 1)
					content = buddyName + ":" + tr("[Announcement]") + webTitle;
				else
				{
					content = tr("[Announcement]") + webTitle;
				}
				break;
			}
			case Message_URL:
			{
				QJsonDocument doc = QJsonDocument::fromJson(lastMsgInInfo.toUtf8());
				QVariantMap map = doc.toVariant().toMap();
				QString webTitle = map.value("subject").toString();
				if (listInfo[i].isGroup == 1)
					content = buddyName + ":" + tr("[Share]") + webTitle;
				else
				{
					content = tr("[Share]") + webTitle;
				}
				break;
			}
			case Message_COMMON:
			{
				QString msgTitle;
				QJsonDocument doc = QJsonDocument::fromJson(lastMsgInInfo.toUtf8());
				QVariantMap map = doc.toVariant().toMap();
				if (map.isEmpty())
					msgTitle = lastMsgInInfo;
				else
				{
					QString systemName = map.value("systemName").toString();
					msgTitle = QString("[%1]").arg(systemName);
				}
				if (listInfo[i].isGroup == 1)
					content = buddyName + ":" + msgTitle;
				else
				{
					content = msgTitle;
				}
				break;
			}
			default:
			{
				MessageInfo messageinfo;
				messageinfo.MessageChildType = listInfo[i].messageType;
				messageinfo.strMsg = listInfo[i].strLastMessage;
				QString buff01, buff02;
				if (listInfo[i].isGroup == 1)
				{
					QString strLastBuff = this->OnDealMessageType(messageinfo, buff01, buff02);
					content = buddyName + ":" + strLastBuff;
				}
				else
				{
					content = this->OnDealMessageType(messageinfo, buff01, buff02);
				}
				break;

			}
			}

			QJsonObject strObject;
			strObject.insert("type", 0);
			strObject.insert("msg", content);
			QJsonArray jsonarray;
			jsonarray.append(strObject);
			QJsonDocument document;
			document.setArray(jsonarray);
			QString strClickJson = (QString)document.toJson();
			//特殊处理@成员消息
			QJsonObject jsonTmp;
			{
				bool isPrevExistAt = false;
				QString groupID = QString::number(listInfo[i].nBudyyID);
				QString lastMsgJson = gDataBaseOpera->DBGetLastMsgJsonInMessageList(groupID);
				QJsonParseError jsonError;
				QJsonDocument doucment = QJsonDocument::fromJson(lastMsgJson.toUtf8(), &jsonError);  // 转化为 JSON 文档
				if (!doucment.isNull() && (jsonError.error == QJsonParseError::NoError))
				{
					QJsonArray jsonArr = doucment.array();
					for (int i = 0; i < jsonArr.count(); i++)
					{
						QJsonObject object = jsonArr.at(i).toObject();
						if (object.value("type").toInt() == MsgJsonAt)
						{
							jsonTmp = object;
							isPrevExistAt = true;
						}
					}
				}

				if (isPrevExistAt)
				{
					listInfo[i].strLastMessage = tr("<font color=\'#f7931e\' >[I was @]</font>") + content;
				}
				else
				{
					listInfo[i].strLastMessage = content;
				}
			}
			if (listInfo[i].messageType == Message_AT)
			{
				int fromUser = gSocketMessage->DBGetFromUserOfLastMsgInMessageList(listInfo[i].nBudyyID, listInfo[i].messageType);
				BuddyInfo buddyInfo = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(QString::number(listInfo[i].nBudyyID), QString::number(fromUser));
				buddyName = (buddyInfo.strNote == "" || buddyInfo.strNote == NULL) ? buddyInfo.strNickName : buddyInfo.strNote;

				QString msgTitle, buff01, buff02, fUserbUFF, toUserBuff;
				QString lastMsgInInfo = gSocketMessage->DBGetLastSysMsgInMessageInfo(listInfo[i].nBudyyID, fUserbUFF, toUserBuff, Message_AT);
				QJsonDocument doc = QJsonDocument::fromJson(lastMsgInInfo.toUtf8());
				QVariantMap map = doc.toVariant().toMap();

				QString atStr = map.value("content").toString();
				listInfo[i].strLastMessage = tr("<font color=\'#f7931e\' >[I was @]</font>") + buddyName + ":" + atStr;
			}
		}


		QString strUnReadNum = "";
		if (listInfo[i].nUnReadNum == 0)
		{
			strUnReadNum = "";
		}
		else
		{
			strUnReadNum = QString("%1").arg(listInfo[i].nUnReadNum);
		}
		listInfo[i].strLastMessage.replace("\n", "");

		//当最后消息的时间使用毫秒保存的时候，需要通过删除后三位转化为为秒，避免溢出。
		if (QString::number(listInfo[i].nLastTime).length() == 13)
			listInfo[i].nLastTime /= 1000;



		this->OnInsertMessage(QString("%1").arg(listInfo[i].nBudyyID),
			listInfo[i].strBuddyHeaderImage,
			listInfo[i].strBuddyName,
			listInfo[i].strLastMessage, listInfo[i].nLastTime, QString("%1").arg(strUnReadNum), listInfo[i].msgTopOrder, listInfo[i].isGroup);

		//判断MessaeInfo表中的listInfo[i].nBudyyID对应的最后一条记录是否发送失败，是的话要显示失败图标
		if (gSocketMessage->IsLastMsgIdFailure(strMyUserId, QString::number(listInfo[i].nBudyyID)))
		{
			this->SetSendStatusFailure(QString::number(listInfo[i].nBudyyID), 0);//设置第一个条目的状态为发送失败(OnInsertMessage操作完成后新插入的就是第一个)
		}
	}

	QTimer::singleShot(500, this, [=] { emitMessageUnreadNum(); });
}


QString MessageListStore::UpdateMsgJsonText(QString& strJson, const QString& strPlainText)
{
	//返回值如果有@成员的话，可能会带有<font>字体内容
	//strJson为消息内容的JSON表示格式，与MESSAGELIST的LastMsgJson一致
	//strPlainText为待更新的文本内容，其实就是type为0的文本内容

	QJsonParseError jsonError;
	QJsonDocument doucment = QJsonDocument::fromJson(strJson.toUtf8(), &jsonError);  // 转化为 JSON 文档
	if (!doucment.isNull() && (jsonError.error == QJsonParseError::NoError))
	{
		QJsonArray jsonArr = doucment.array();
		return UpdateMsgJsonText(jsonArr, strPlainText, strJson);
	}


	return "";
}

QString MessageListStore::UpdateMsgJsonText(const QJsonArray& jsonArr, const QString& strPlainText, QString& strRetJson)
{
	QString strRet;

	QJsonArray tmpJsonArr;

	for (int i = 0; i < jsonArr.count(); i++)
	{
		QJsonObject object = jsonArr.at(i).toObject();
		if (object.value("type").toInt() == MsgJsonText)
		{
			object.insert("msg", strPlainText);
		}

		tmpJsonArr.append(object);
	}

	QJsonDocument document;
	document.setArray(tmpJsonArr);
	strRetJson = (QString)document.toJson();


	for (int i = 0; i < tmpJsonArr.count(); i++)
	{
		QJsonObject object = tmpJsonArr.at(i).toObject();
		if (object.value("type").toInt() == MsgJsonText)
		{
			strRet += object.value("msg").toString();
		}
		else if (object.value("type").toInt() == MsgJsonAt && i == 0)
		{
			strRet += tr("<font color=\'#f7931e\' >[I was @]</font>");
		}
		else if (object.value("type").toInt() == MsgJsonAt && i == 1)
		{
			strRet = tr("<font color=\'#f7931e\' >[I was @]</font>") + strRet;
		}


	}

	return strRet;
}

void MessageListStore::insertPerToPerMessage(MessageInfo messageinfo, BuddyInfo buddyInfo, bool bChangeNum)
{
	UserInfo userInfo = gDataManager->getUserInfo();
	MessageListInfo messageListInfo;
	int msgTopOrder = 0;
	msgTopOrder = gDataBaseOpera->DBGetMsgTopOrderInMessageList(buddyInfo.nUserId);

	//首先生成信息时间。
	QString strtemp = QString("%1").arg(messageinfo.ClientTime);
	if (strtemp.length() == 13)
		messageinfo.ClientTime = messageinfo.ClientTime / 1000;

	QString strPlainText;
	messageinfo.strMessageListDesc = OnDealMessageType(messageinfo, messageinfo.strMessageListJson, strPlainText);

	//首先判断列表中是否存在
	int idx = m_vm->getIndexByUserId(QString::number(buddyInfo.nUserId));
	if (idx >= 0)
	{
		QString userId = QString::number(buddyInfo.nUserId);
		int unreadMsgCount = m_vm->getUnreadMsgCountByIndex(idx);

		QString strNick = buddyInfo.strNote;
		if (buddyInfo.strNote.isEmpty())
			strNick = buddyInfo.strNickName;

		QString unReadNum;
		if (userId == m_vm->getCurrentSelectedUserId())
		{
			unReadNum = "";

			//此处说明当前正和某人聊天，此时当前聊天窗口有新消息进来
			//由于后面的OnInsertMessage中的doClickIndex(int index)需要判断数据库表MessageList中的LastMsgJson内容来处理@成员等类型的消息，所以此处需要提前更新LastMsgJson
			//messageinfo.strMessageListJson因为当前窗口正打开，所以要去除@成员等信息

			QJsonParseError jsonError;
			QJsonDocument doucment = QJsonDocument::fromJson(messageinfo.strMessageListJson.toUtf8(), &jsonError);  // 转化为 JSON 文档
			if (!doucment.isNull() && (jsonError.error == QJsonParseError::NoError))
			{
				QJsonArray jsonArr = doucment.array();

				QJsonArray jsonClickArr;
				for (int i = 0; i < jsonArr.count(); i++)
				{
					QJsonObject object = jsonArr.at(i).toObject();
					if (object.value("type").toInt() == MsgJsonText)
					{
						jsonClickArr.append(object);
					}

				}

				QJsonDocument document;
				document.setArray(jsonClickArr);
				QString strClickJson = (QString)document.toJson();

				messageinfo.strMessageListJson = strClickJson;
			}

			gDataBaseOpera->DBUpdateLastMsgJsonInMessageList(userId, messageinfo.strMessageListJson, messageinfo.MessageChildType);
		}
		else
		{
			if (bChangeNum)
			{
				unReadNum = QString::number(unreadMsgCount + 1);
			}
			else
			{
				unReadNum = QString::number(unreadMsgCount);
			}
			gDataBaseOpera->DBUpdateLastMsgJsonInMessageList(userId, messageinfo.strMessageListJson, messageinfo.MessageChildType);
		}

		if (m_vm->getCurrentIndex() == idx)
		{
			backToCurrentFlag = false;
			backToCurrentID = userId;
		}

		QString strmsg = messageinfo.strMessageListDesc;

		if (m_vm->isDraftFlagByIndex(idx))
		{
			strmsg = m_vm->getMsgByIndex(idx);
			m_vm->deleteByIndex(idx);

			this->OnInsertMessage(QString::number(buddyInfo.nUserId),
				buddyInfo.strLocalAvatar,
				strNick,
				strmsg, messageinfo.ClientTime, unReadNum, msgTopOrder, 0, true);
		}
		else
		{
			m_vm->deleteByIndex(idx);
			this->OnInsertMessage(QString::number(buddyInfo.nUserId),
				buddyInfo.strLocalAvatar,
				strNick,
				strmsg, messageinfo.ClientTime, unReadNum, msgTopOrder, 0);
		}


		messageListInfo.nBudyyID = buddyInfo.nUserId;
		messageListInfo.strLastMessage = messageinfo.strMessageListDesc;
		messageListInfo.strLastMessageJson = messageinfo.strMessageListJson;
		messageListInfo.nLastTime = messageinfo.ClientTime;
		messageListInfo.strBuddyName = buddyInfo.strNickName;
		messageListInfo.strBuddyHeaderImage = buddyInfo.strLocalAvatar;
		messageListInfo.messageType = messageinfo.MessageChildType;
		messageListInfo.nUnReadNum = unReadNum.toInt();////该值将要被写到数据库里，数据库未读数量应与显示的未读数量一致
		messageListInfo.isGroup = 0;
		messageListInfo.msgTopOrder = msgTopOrder;


	}
	else
	{
		QString strNick = buddyInfo.strNote.isEmpty() ? buddyInfo.strNickName : buddyInfo.strNote;

		this->OnInsertMessage(QString::number(buddyInfo.nUserId),
			buddyInfo.strLocalAvatar,
			strNick,
			messageinfo.strMessageListDesc, messageinfo.ClientTime, bChangeNum ? "1" : "0", msgTopOrder, 0);

		messageListInfo.nBudyyID = buddyInfo.nUserId;
		messageListInfo.strLastMessage = messageinfo.strMessageListDesc;
		messageListInfo.strLastMessageJson = messageinfo.strMessageListJson;
		messageListInfo.nLastTime = messageinfo.ClientTime;
		messageListInfo.strBuddyName = buddyInfo.strNickName;
		messageListInfo.strBuddyHeaderImage = buddyInfo.strLocalAvatar;
		messageListInfo.messageType = messageinfo.MessageChildType;
		messageListInfo.nUnReadNum = bChangeNum ? 1 : 0;
		messageListInfo.isGroup = 0;
		messageListInfo.msgTopOrder = msgTopOrder;
	}

	gDataBaseOpera->DBInsertPerMessageListInfo(messageListInfo);
}

void MessageListStore::insertPerToGroupMessage(MessageInfo messageinfo, GroupInfo groupInfo, bool bChangeNum)
{
	MessageListInfo messageListInfo;
	int msgTopOrder = 0;
	msgTopOrder = gDataBaseOpera->DBGetMsgTopOrderInMessageList(groupInfo.groupId.toInt());
	QString strtemp = QString("%1").arg(messageinfo.ClientTime);
	if (strtemp.length() == 13)
		messageinfo.ClientTime = messageinfo.ClientTime / 1000;

	QString strPlainText;
	messageinfo.strMessageListDesc = OnDealMessageType(messageinfo, messageinfo.strMessageListJson, strPlainText);

	if (!strPlainText.isEmpty())
	{
		QString fromID = QString::number(messageinfo.nFromUserID);
		QString groupID = QString::number(messageinfo.nToUserID);
		BuddyInfo buddyInfo = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(groupID, fromID);

		if ((buddyInfo.strNote.isEmpty() && buddyInfo.strNickName.isEmpty()) || messageinfo.MessageChildType == Message_NOTIFY)
		{
			//如果取不到任何名字信息，不显示冒号
			//后期处理
		}
		else
		{
			//strPlainText需要追加上名字信息
			if (buddyInfo.strNote.isEmpty())
				strPlainText = buddyInfo.strNickName + ":" + strPlainText;
			else
				strPlainText = buddyInfo.strNote + ":" + strPlainText;

			//因为strPlainText改变了，需要重新修正下messageinfo.strMessageListDesc,messageinfo.strMessageListJson
			messageinfo.strMessageListDesc = UpdateMsgJsonText(messageinfo.strMessageListJson, strPlainText);
		}


	}

	//判断下数据表MESSAGELIST的字段LastMsgJson是否上一条记录包含@成员信息，包含的话messageinfo.strMessageListDesc特殊处理下
	QJsonObject jsonTmp;
	{
		bool isPrevExistAt = false;

		QString groupID = QString::number(messageinfo.nToUserID);
		QString lastMsgJson = gDataBaseOpera->DBGetLastMsgJsonInMessageList(groupID);
		QJsonParseError jsonError;
		QJsonDocument doucment = QJsonDocument::fromJson(lastMsgJson.toUtf8(), &jsonError);  // 转化为 JSON 文档
		if (!doucment.isNull() && (jsonError.error == QJsonParseError::NoError))
		{
			QJsonArray jsonArr = doucment.array();
			for (int i = 0; i < jsonArr.count(); i++)
			{
				QJsonObject object = jsonArr.at(i).toObject();
				if (object.value("type").toInt() == MsgJsonAt)
				{
					jsonTmp = object;
					isPrevExistAt = true;
				}

			}
		}

		if (isPrevExistAt)
		{
			bool isCurrentExistAt = false;

			QJsonParseError jsonError;
			QJsonDocument doucment = QJsonDocument::fromJson(messageinfo.strMessageListJson.toUtf8(), &jsonError);  // 转化为 JSON 文档
			if (!doucment.isNull() && (jsonError.error == QJsonParseError::NoError))
			{
				QJsonArray jsonArr = doucment.array();

				for (int i = 0; i < jsonArr.count(); i++)
				{
					QJsonObject object = jsonArr.at(i).toObject();
					if (object.value("type").toInt() == MsgJsonAt)//@成员
					{
						isCurrentExistAt = true;
					}

				}
			}
			QJsonArray jsonArr = doucment.array();
			jsonArr.append(jsonTmp);
			doucment.setArray(jsonArr);
			messageinfo.strMessageListJson = (QString)doucment.toJson();
			if (!isCurrentExistAt)
			{
				messageinfo.strMessageListDesc = tr("<font color=\'#f7931e\' >[I was @]</font>") + messageinfo.strMessageListDesc;
			}

		}
	}

	//首先判断列表中是否存在
	int idx = m_vm->getIndexByUserId(groupInfo.groupId);
	if (idx >= 0)
	{
		QString userId = groupInfo.groupId;
		int unreadMsgCount = m_vm->getUnreadMsgCountByIndex(idx);

		QString unReadNum;
		if (m_vm->getCurrentSelectedUserId() == userId)
		{
			unReadNum = "";

			//此处说明当前正和某人聊天，此时当前聊天窗口有新消息进来
			//由于后面的OnInsertMessage中的doClickIndex(int index)需要判断数据库表MessageList中的LastMsgJson内容来处理@成员等类型的消息，所以此处需要提前更新LastMsgJson
			//messageinfo.strMessageListJson因为当前窗口正打开，所以要去除@成员等信息

			QJsonParseError jsonError;
			QJsonDocument doucment = QJsonDocument::fromJson(messageinfo.strMessageListJson.toUtf8(), &jsonError);  // 转化为 JSON 文档
			if (!doucment.isNull() && (jsonError.error == QJsonParseError::NoError))
			{
				QJsonArray jsonArr = doucment.array();

				QJsonArray jsonClickArr;
				for (int i = 0; i < jsonArr.count(); i++)
				{
					QJsonObject object = jsonArr.at(i).toObject();
					if (object.value("type").toInt() == MsgJsonText)
					{
						jsonClickArr.append(object);
					}

				}

				QJsonDocument document;
				document.setArray(jsonClickArr);
				QString strClickJson = (QString)document.toJson();

				messageinfo.strMessageListJson = strClickJson;
			}

			gDataBaseOpera->DBUpdateLastMsgJsonInMessageList(userId, messageinfo.strMessageListJson, messageinfo.MessageChildType);
		}
		else
		{
			if (bChangeNum)
			{
				unReadNum = QString::number(unreadMsgCount + 1);
			}
			else
			{
				unReadNum = QString::number(unreadMsgCount);
			}

			gDataBaseOpera->DBUpdateLastMsgJsonInMessageList(userId, messageinfo.strMessageListJson, messageinfo.MessageChildType);
		}

		if (m_vm->getCurrentIndex() == idx)
		{
			backToCurrentFlag = false;
			backToCurrentID = userId;
		}

		if (m_vm->isDraftFlagByIndex(idx))
		{
			QString strmsg = m_vm->getMsgByIndex(idx);
			m_vm->deleteByIndex(idx);
			this->OnInsertMessage(groupInfo.groupId,
				groupInfo.groupLoacalHeadImage,
				groupInfo.groupName,
				strmsg, messageinfo.ClientTime, unReadNum, msgTopOrder, 1, true);
		}
		else
		{
			m_vm->deleteByIndex(idx);
			this->OnInsertMessage(groupInfo.groupId,
				groupInfo.groupLoacalHeadImage,
				groupInfo.groupName,
				messageinfo.strMessageListDesc, messageinfo.ClientTime, unReadNum, msgTopOrder, 1);
		}


		messageListInfo.nBudyyID = groupInfo.groupId.toInt();
		messageListInfo.strLastMessage = messageinfo.strMessageListDesc;
		messageListInfo.strLastMessageJson = messageinfo.strMessageListJson;
		messageListInfo.nLastTime = messageinfo.ClientTime;
		messageListInfo.strBuddyName = groupInfo.groupName;
		messageListInfo.strBuddyHeaderImage = groupInfo.groupLoacalHeadImage;
		messageListInfo.messageType = messageinfo.MessageChildType;
		messageListInfo.nUnReadNum = unReadNum.toInt();//该值将要被写到数据库里，数据库未读数量应与显示的未读数量一致
		messageListInfo.isGroup = 1;
		messageListInfo.msgTopOrder = msgTopOrder;

		gDataBaseOpera->DBInsertGroupMessageListInfo(messageListInfo);
	}
	else
	{
		this->OnInsertMessage(groupInfo.groupId,
			groupInfo.groupLoacalHeadImage,
			groupInfo.groupName,
			messageinfo.strMessageListDesc, messageinfo.ClientTime, bChangeNum ? "1" : "0", msgTopOrder, 1);

		//插入新的
		messageListInfo.nBudyyID = groupInfo.groupId.toInt();
		messageListInfo.strLastMessage = messageinfo.strMessageListDesc;
		messageListInfo.strLastMessageJson = messageinfo.strMessageListJson;
		messageListInfo.nLastTime = messageinfo.ClientTime;
		messageListInfo.strBuddyName = groupInfo.groupName;
		messageListInfo.strBuddyHeaderImage = groupInfo.groupLoacalHeadImage;
		messageListInfo.messageType = messageinfo.MessageChildType;
		messageListInfo.nUnReadNum = bChangeNum ? 1 : 0;
		messageListInfo.isGroup = 1;
		messageListInfo.msgTopOrder = msgTopOrder;

		/*如果为新手机端群组消息，本地MESSAGELIST表在个人发消息之前不会更新，需要手动更新，以能进行置顶和屏蔽操作*/
		gDataBaseOpera->DBInsertGroupMessageListInfo(messageListInfo);
	}
}


void MessageListStore::OnClickPerMessage(MessageInfo messageinfo, BuddyInfo buddyInfo)
{
	int msgTopOrder = 0;
	msgTopOrder = gDataBaseOpera->DBGetMsgTopOrderInMessageList(buddyInfo.nUserId);
	//首先生成信息时间。
	QString strtemp = QString("%1").arg(messageinfo.ClientTime);
	if (strtemp.length() == 13)
		messageinfo.ClientTime = messageinfo.ClientTime / 1000;

	QString strPlainText;
	messageinfo.strMessageListDesc = OnDealMessageType(messageinfo, messageinfo.strMessageListJson, strPlainText);

	MessageListInfo messageListInfo;
	messageListInfo.nBudyyID = buddyInfo.nUserId;
	messageListInfo.strLastMessage = messageinfo.strMessageListDesc;
	messageListInfo.strLastMessageJson = messageinfo.strMessageListJson;
	messageListInfo.nLastTime = messageinfo.ClientTime;
	messageListInfo.strBuddyName = buddyInfo.strNickName;
	messageListInfo.strBuddyHeaderImage = buddyInfo.strLocalAvatar;
	messageListInfo.messageType = messageinfo.MessageChildType;
	messageListInfo.nUnReadNum = 0;
	messageListInfo.isGroup = 0;
	messageListInfo.msgTopOrder = msgTopOrder;


	gDataBaseOpera->DBInsertPerMessageListInfo(messageListInfo);
	//首先判断列表中是否存在
	int idx = m_vm->getIndexByUserId(QString::number(buddyInfo.nUserId));
	if (idx >= 0)
	{
		//有的话模拟点击
		m_vm->doClickIndex(idx);
	}
	else
	{
		QString strNick = buddyInfo.strNote.isEmpty() ? buddyInfo.strNickName : buddyInfo.strNote;

		this->OnInsertMessage(QString::number(buddyInfo.nUserId),
			buddyInfo.strLocalAvatar,
			strNick,
			messageinfo.strMessageListDesc, messageinfo.ClientTime, "", msgTopOrder);


		int idx = m_vm->getIndexByUserId(QString::number(buddyInfo.nUserId));
		if (idx >= 0)
		{
			m_vm->doClickIndex(idx);
		}
	}

}

void MessageListStore::OnClickGroupMessage(MessageInfo messageinfo, GroupInfo groupInfo)
{
	int msgTopOrder = 0;
	msgTopOrder = gDataBaseOpera->DBGetMsgTopOrderInMessageList(groupInfo.groupId.toInt());
	QString strtemp = QString("%1").arg(messageinfo.ClientTime);
	if (strtemp.length() == 13)
		messageinfo.ClientTime = messageinfo.ClientTime / 1000;

	QString strPlainText;
	messageinfo.strMessageListDesc = OnDealMessageType(messageinfo, messageinfo.strMessageListJson, strPlainText);

	if (!strPlainText.isEmpty())
	{
		QString fromID = QString::number(messageinfo.nFromUserID);
		QString groupID = QString::number(messageinfo.nToUserID);
		BuddyInfo buddyInfo = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(groupID, fromID);
		if ((buddyInfo.strNote.isEmpty() && buddyInfo.strNickName.isEmpty()) || messageinfo.MessageChildType == Message_NOTIFY)
		{
			//如果取不到任何名字信息，不显示冒号
			//后期处理
		}
		else
		{
			//strPlainText需要追加上名字信息
			if (buddyInfo.strNote.isEmpty())
				strPlainText = buddyInfo.strNickName + ":" + strPlainText;
			else
				strPlainText = buddyInfo.strNote + ":" + strPlainText;

			messageinfo.strMessageListDesc = UpdateMsgJsonText(messageinfo.strMessageListJson, strPlainText);
		}
	}

	MessageListInfo messageListInfo;
	messageListInfo.nBudyyID = groupInfo.groupId.toInt();
	messageListInfo.strLastMessage = messageinfo.strMessageListDesc;
	messageListInfo.strLastMessageJson = messageinfo.strMessageListJson;
	messageListInfo.nLastTime = messageinfo.ClientTime;
	messageListInfo.strBuddyName = groupInfo.groupName;
	messageListInfo.strBuddyHeaderImage = groupInfo.groupLoacalHeadImage;
	messageListInfo.messageType = messageinfo.MessageChildType;
	messageListInfo.nUnReadNum = 0;
	messageListInfo.isGroup = 1;
	messageListInfo.msgTopOrder = msgTopOrder;

	gDataBaseOpera->DBInsertGroupMessageListInfo(messageListInfo);
	//首先判断列表中是否存在
	int idx = m_vm->getIndexByUserId(groupInfo.groupId);
	if (idx >= 0)
	{
		//有的话模拟点击
		m_vm->doClickIndex(idx);
	}
	else
	{
		//没有的话插入条目再模拟点击该条目
		this->OnInsertMessage(groupInfo.groupId,
			groupInfo.groupLoacalHeadImage,
			groupInfo.groupName,
			messageinfo.strMessageListDesc, messageinfo.ClientTime, "", msgTopOrder, 1);

		idx = m_vm->getIndexByUserId(groupInfo.groupId);
		if (idx >= 0)
		{
			m_vm->doClickIndex(idx);
		}
	}

}


void MessageListStore::dealPerMessageType(MessageInfo msgTmp, BuddyInfo buddyInfo)
{
	this->OnClickPerMessage(msgTmp, buddyInfo);
}

void MessageListStore::dealGroupMessageType(MessageInfo msgTmp, GroupInfo groupInfo)
{
	this->OnClickGroupMessage(msgTmp, groupInfo);
}

void MessageListStore::updateMessage(bool isGroup, QVariant var)
{
	if (isGroup)
	{
		GroupInfo groupInfo = var.value<GroupInfo>();
		int idx = m_vm->getIndexByUserId(groupInfo.groupId);

		if (idx >= 0)
		{
			QString headPicPath = getValidImagePath(groupInfo.groupLoacalHeadImage, false, QStringLiteral("image://HeadProvider/"));
			m_vm->updateHeadUrlByIndex(idx, headPicPath);
			m_vm->updateNickNameByIndex(idx, groupInfo.groupName);
		}
	}
	else
	{
		BuddyInfo buddyInfo = var.value<BuddyInfo>();
		int idx = m_vm->getIndexByUserId(QString::number(buddyInfo.nUserId));

		if (idx >= 0)
		{
			QString headPicPath = getValidImagePath(buddyInfo.strLocalAvatar, true, QStringLiteral("image://HeadProvider/"));
			m_vm->updateHeadUrlByIndex(idx, headPicPath);
			if (buddyInfo.strNote.isEmpty())
			{
				m_vm->updateNickNameByIndex(idx, buddyInfo.strNickName);
			}
			else
			{
				m_vm->updateNickNameByIndex(idx, buddyInfo.strNote);
			}
		}
	}
}

void MessageListStore::insertSelfMessage(bool isGroup, QVariant var, QString msg, bool bNeedOpen, QMap<QString, QVariant> params)
{
	QString buddyOrGroupId;
	int msgTopOrder = 0;
	if (isGroup)
	{
		GroupInfo groupInfo = var.value<GroupInfo>();
		buddyOrGroupId = groupInfo.groupId;
		msgTopOrder = gDataBaseOpera->DBGetMsgTopOrderInMessageList(groupInfo.groupId.toInt());
	}
	else
	{
		BuddyInfo buddyInfo = var.value<BuddyInfo>();
		buddyOrGroupId = QString::number(buddyInfo.nUserId);
		msgTopOrder = gDataBaseOpera->DBGetMsgTopOrderInMessageList(buddyInfo.nUserId);
	}

	int sendStaus = -1;//目前限制为MESSAGE_STATE_PRESEND，MESSAGE_STATE_FAILURE，MESSAGE_STATE_SEND三个值
	if (params.contains("SendStatus"))
	{
		sendStaus = params["SendStatus"].toInt();

		if (sendStaus == MESSAGE_STATE_PRESEND)
		{
			//预发送，则删除条目再插入条目，为了方便使用，预保留发送过的消息(之前发送失败的消息需要立刻在MessageList列表中显示，所以提前保留以便使用)
			if (params.contains("MsgId"))
			{
				QString msgId = params["MsgId"].toString();

				m_lastMsgIdInfoMap[buddyOrGroupId] = msgId;
				m_msgIdInfoMap[msgId] = msg;
				m_lastMsgIdFailureInfoMap[buddyOrGroupId] = false;
			}

		}
		else if (sendStaus == MESSAGE_STATE_FAILURE)
		{
			//发送失败，也是删除条目再插入条目，比如前面一个消息发送失败，则失败后覆盖最新的消息显示，以便提醒用户
			//发送失败时msg为空数据,需要根据m_msgIdInfoMap复原该消息内容
			//发送失败则m_msgIdInfoMap清除该MsgId对应的条目
			if (params.contains("MsgId"))
			{
				QString msgId = params["MsgId"].toString();
				QString msgDesc = m_msgIdInfoMap[msgId];//由于MESSAGE_STATE_FAILURE会触发两次，如果删除，第二次会导致取不出值，显示消息为空
				m_msgIdInfoMap.remove(msgId);

				if (m_lastMsgIdInfoMap[buddyOrGroupId] == msgId)
				{
					m_lastMsgIdFailureInfoMap[buddyOrGroupId] = true;//最后一条消息发送失败
				}

				//最后一条消息的MsgId已发送，更新SendStatus;
				int idx = m_vm->getIndexByUserId(buddyOrGroupId);
				if (idx >= 0)
				{
					m_vm->setSendStatusFailedByIndex(idx);

					if (!msgDesc.isEmpty())
					{
						m_vm->updateMsgByIndex(idx, msgDesc);
					}
				}

			}

			return;
		}
		else if (sendStaus == MESSAGE_STATE_SEND)
		{
			//已发送，则需要判断当前正在显示的最后一条消息的MsgId是否是已发送成功的MsgId，若一致，改状态为已发送，否则，不修改状态
			//发送成功则m_msgIdInfoMap清除该MsgId对应的条目

			if (params.contains("MsgId"))
			{
				QString msgId = params["MsgId"].toString();

				QString msgDesc = m_msgIdInfoMap[msgId];
				m_msgIdInfoMap.remove(msgId);

				if (m_lastMsgIdInfoMap[buddyOrGroupId] == msgId)
				{
					//最后一条消息的MsgId已发送，更新SendStatus;
					int idx = m_vm->getIndexByUserId(buddyOrGroupId);
					if (idx >= 0)
					{
						m_vm->resetSendStatusByIndex(idx);
					}

				}
			}

			return;
		}
		else
		{
			return;
		}
	}

	if (isGroup)
	{
		GroupInfo groupInfo = var.value<GroupInfo>();
		QString strOpen = "";

		int idx = m_vm->getIndexByUserId(groupInfo.groupId);
		if (idx >= 0)
		{
			QString userId = groupInfo.groupId;
			QString strCur = m_vm->getCurrentSelectedUserId();
			int unreadMsgCount = m_vm->getUnreadMsgCountByIndex(idx);

			if (!bNeedOpen&&strCur != userId)
			{
				strOpen = QString::number(unreadMsgCount);
			}

			if (m_vm->getCurrentIndex() == idx)
			{
				backToCurrentFlag = false;
				backToCurrentID = userId;
			}

			m_vm->deleteByIndex(idx);
		}


		int time = QDateTime::currentDateTime().toTime_t();
		this->OnInsertMessage(groupInfo.groupId,
			groupInfo.groupLoacalHeadImage,
			groupInfo.groupName,
			msg, time, strOpen,
			msgTopOrder, 1);
	}
	else
	{
		BuddyInfo buddyInfo = var.value<BuddyInfo>();
		QString strOpen = "";

		int idx = m_vm->getIndexByUserId(QString::number(buddyInfo.nUserId));
		if (idx >= 0)
		{
			QString userId = QString::number(buddyInfo.nUserId);
			QString strCur = m_vm->getCurrentSelectedUserId();
			int unreadMsgCount = m_vm->getUnreadMsgCountByIndex(idx);

			if (!bNeedOpen&&strCur != userId)
			{
				strOpen = QString::number(unreadMsgCount);
			}

			if (m_vm->getCurrentIndex() == idx)
			{
				backToCurrentFlag = false;
				backToCurrentID = userId;
			}

			m_vm->deleteByIndex(idx);
		}


		int time_t = QDateTime::currentDateTime().toTime_t();
		QString strNick;
		if (buddyInfo.strNote.isEmpty())
			strNick = buddyInfo.strNickName;
		else
			strNick = buddyInfo.strNote;
		this->OnInsertMessage(QString::number(buddyInfo.nUserId),
			buddyInfo.strLocalAvatar,
			strNick,
			msg, time_t, strOpen, msgTopOrder, 0);
	}


	//新插入的是index为0的条目，更新该条目的SendStatus状态图片
	int idx = m_vm->getIndexByUserId(buddyOrGroupId);
	if (idx >= 0)
	{
		QString userId = buddyOrGroupId;

		if (sendStaus == MESSAGE_STATE_PRESEND)
		{
			m_vm->setSendStatusWaitByIndex(idx);
		}
		m_vm->doClickIndex(idx);
	}

}


MessageListInfo MessageListStore::getCurrentItemInfo()
{
	MessageListInfo listInfo;
	listInfo.nBudyyID = -1;
	int idx = m_vm->getCurrentIndex();

	if (idx >= 0)
	{
		listInfo.nBudyyID = m_vm->getUserIdByIndex(idx).toInt();
		listInfo.isGroup = m_vm->getItemTypeByIndex(idx);
	}

	return listInfo;
}

bool MessageListStore::isExistUserId(QString strUserId)
{
	return m_vm->existUserId(strUserId);
}

void MessageListStore::setDraft(QString strUserId, QString strMsg)
{
	int idx = m_vm->getIndexByUserId(strUserId);
	if (idx >= 0)
	{
		strMsg = tr("<font color=\'#f7931e\' >[Draft]</font>") + strMsg;
		m_vm->updateMsgByIndex(idx, strMsg);
		m_vm->updateDraftFlagByIndex(idx, true);
	}
}

void MessageListStore::cancelMessageTop(QString strUserId)
{
	int rowNum = m_vm->getIndexByUserId(strUserId);

	int		cch_Type = m_vm->getValueByIndexAndType(rowNum, MessageListModel::ItemTypeRole).toInt();
	QString cch_Id = m_vm->getValueByIndexAndType(rowNum, MessageListModel::UserIdRole).toString();
	QString cch_HeadUrl = m_vm->getValueByIndexAndType(rowNum, MessageListModel::HeadUrlRole).toString();
	QString cch_NickName = m_vm->getValueByIndexAndType(rowNum, MessageListModel::NickNameRole).toString();
	QString cch_Msg = m_vm->getValueByIndexAndType(rowNum, MessageListModel::MsgRole).toString();
	QString cch_MsgTime = m_vm->getValueByIndexAndType(rowNum, MessageListModel::MsgTimeRole).toString();
	int		cch_UnReadMsgCount = m_vm->getValueByIndexAndType(rowNum, MessageListModel::UnreadMsgCountRole).toInt();
	int		cch_MsgPrompt = m_vm->getValueByIndexAndType(rowNum, MessageListModel::MsgPromptRole).toInt();


	gDataBaseOpera->DBUpdateMsgTopOrderInMessageList(cch_Id.toInt(), 0);
	if (m_vm->getValueByIndexAndType(rowNum, MessageListModel::DraftFlagRole).toBool() == true)
	{
		cch_Msg = m_vm->getValueByIndexAndType(rowNum, MessageListModel::MsgRole).toString();
		m_vm->deleteByIndex(rowNum);
		MessageListItem item(cch_Type, cch_Id, cch_HeadUrl, cch_NickName, cch_Msg, cch_MsgTime, cch_UnReadMsgCount, 0, cch_MsgPrompt, true);


		for (int i = 0; i < m_vm->rowCount(); i++)
		{
			int topOrder = m_vm->getValueByIndexAndType(i, MessageListModel::MsgTopOrderRole).toInt();
			if (topOrder == 0)
			{
				m_vm->insertItem(item, i);
				m_vm->setCurrentIndex(i);
				break;
			}
			else if (i == m_vm->rowCount() - 1)
			{
				m_vm->pushBack(item);
				m_vm->setCurrentIndex(m_vm->rowCount());
				break;
			}
		}
		if (m_vm->rowCount() == 0)
		{
			m_vm->insertItem(item, 0);
			m_vm->setCurrentIndex(0);
		}
	}
	else
	{
		m_vm->deleteByIndex(rowNum);
		MessageListItem item(cch_Type, cch_Id, cch_HeadUrl, cch_NickName, cch_Msg, cch_MsgTime, cch_UnReadMsgCount, 0, cch_MsgPrompt, false);

		for (int i = 0; i < m_vm->rowCount(); i++)
		{
			int topOrder = m_vm->getValueByIndexAndType(i, MessageListModel::MsgTopOrderRole).toInt();
			if (topOrder == 0)
			{
				m_vm->insertItem(item, i);
				m_vm->setCurrentIndex(i);
				break;
			}
			else if (i == m_vm->rowCount() - 1)
			{
				m_vm->pushBack(item);
				m_vm->setCurrentIndex(m_vm->rowCount());
				break;
			}
		}
		if (m_vm->rowCount() == 0)
		{
			m_vm->insertItem(item, 0);
			m_vm->setCurrentIndex(0);
		}
	}
}

void MessageListStore::setMessageTop(QString strUserId)
{
	int rowNum = m_vm->getIndexByUserId(strUserId);
	int		cch_Type = m_vm->getValueByIndexAndType(rowNum, MessageListModel::ItemTypeRole).toInt();
	QString cch_Id = m_vm->getValueByIndexAndType(rowNum, MessageListModel::UserIdRole).toString();
	QString cch_HeadUrl = m_vm->getValueByIndexAndType(rowNum, MessageListModel::HeadUrlRole).toString();
	QString cch_NickName = m_vm->getValueByIndexAndType(rowNum, MessageListModel::NickNameRole).toString();
	QString cch_Msg = m_vm->getValueByIndexAndType(rowNum, MessageListModel::MsgRole).toString();
	QString cch_MsgTime = m_vm->getValueByIndexAndType(rowNum, MessageListModel::MsgTimeRole).toString();
	int		cch_UnReadMsgCount = m_vm->getValueByIndexAndType(rowNum, MessageListModel::UnreadMsgCountRole).toInt();
	int		cch_MsgPrompt = m_vm->getValueByIndexAndType(rowNum, MessageListModel::MsgPromptRole).toInt();

	int maxTopOrderValue = 0, minTopOrderValue = 0;
	gDataBaseOpera->DBGetMaximumTopOrderInMessageList(&maxTopOrderValue, &minTopOrderValue);
	maxTopOrderValue++;
	gDataBaseOpera->DBUpdateMsgTopOrderInMessageList(cch_Id.toInt(), maxTopOrderValue);

	if (m_vm->getValueByIndexAndType(rowNum, MessageListModel::DraftFlagRole).toBool() == true)
	{
		cch_Msg = m_vm->getValueByIndexAndType(rowNum, MessageListModel::MsgRole).toString();
		m_vm->deleteByIndex(rowNum);
		MessageListItem item(cch_Type, cch_Id, cch_HeadUrl, cch_NickName, cch_Msg, cch_MsgTime, cch_UnReadMsgCount, maxTopOrderValue, cch_MsgPrompt, true);
		m_vm->pushFront(item);
	}
	else
	{
		m_vm->deleteByIndex(rowNum);
		MessageListItem item(cch_Type, cch_Id, cch_HeadUrl, cch_NickName, cch_Msg, cch_MsgTime, cch_UnReadMsgCount, maxTopOrderValue, cch_MsgPrompt, false);
		m_vm->pushFront(item);
	}

	m_vm->setCurrentIndex(0);
}

void MessageListStore::setMessagePrompt(QString strUserId)
{
	//允许提示（禁用免打扰）
	int rowNum = m_vm->getIndexByUserId(strUserId);
	int currId = strUserId.toInt();
	int currType = m_vm->getValueByIndexAndType(rowNum, MessageListModel::ItemTypeRole).toInt();

	m_vm->setValueByIndexAndType(rowNum, MessageListModel::MsgPromptRole, 0);
	gDataBaseOpera->DBSetMsgPromptInInfoTables(currId, currType);

	emitMessageUnreadNum();
}

void MessageListStore::cancelMessagePrompt(QString strUserId)
{
	//不允许提示（启用免打扰）
	int rowNum = m_vm->getIndexByUserId(strUserId);
	int currId = strUserId.toInt();
	int currType = m_vm->getValueByIndexAndType(rowNum, MessageListModel::ItemTypeRole).toInt();

	m_vm->setValueByIndexAndType(rowNum, MessageListModel::MsgPromptRole, 1);
	gDataBaseOpera->DBCancleMsgPromptInInfoTables(currId, currType);

	emitMessageUnreadNum();
}

void MessageListStore::dissolveGroup(QString strUserId)
{
	//解散群
	UserInfo info = gDataManager->getUserInfo();
	QString strGroupID;
	int rowNum = m_vm->getIndexByUserId(strUserId);
	strGroupID = strUserId;
	m_vm->deleteByIndex(rowNum);
	gDataBaseOpera->DBDeleteOneRecordInMessageList(strGroupID.toInt());
	m_vm->doClickIndex(0);

	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
	http->setData(QVariant(strGroupID));
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), m_dispatcher, SIGNAL(sigCheckHttp(bool, QString)));
	QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/dissolveGroup"
		+ QString("?userId=%1&passWord=%2&groupId=%3").arg(info.nUserID).arg(info.strUserPWD).arg(strGroupID);
	http->getHttpRequest(url);

	emitMessageUnreadNum();
}

void MessageListStore::quitGroup(QString strUserId)
{
	//退出群
	UserInfo info = gDataManager->getUserInfo();
	QString strGroupID;
	int rowNum = m_vm->getIndexByUserId(strUserId);
	strGroupID = strUserId;
	m_vm->deleteByIndex(rowNum);
	gDataBaseOpera->DBDeleteOneRecordInMessageList(strGroupID.toInt());
	m_vm->doClickIndex(0);

	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
	http->setData(QVariant(strGroupID));
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), m_dispatcher, SIGNAL(sigCheckHttp(bool, QString)));
	QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/userQuitGroup"
		+ QString("?userId=%1&passWord=%2&groupId=%3").arg(info.nUserID).arg(info.strUserPWD).arg(strGroupID);
	http->getHttpRequest(url);

	emitMessageUnreadNum();
}