#include "messagelistviewmodel.h"
#include "messagelistview.h"
#include <QQuickItem>

MessageListViewModel* MessageListViewModel::s_instance = NULL;

MessageListViewModel::MessageListViewModel(QObject *parent) : QObject(parent), m_model(new MessageListModel(this))
{
	s_instance = this;
}


MessageListViewModel* MessageListViewModel::instance()
{
	return s_instance;
}

void MessageListViewModel::init()
{
	
}

MessageListModel* MessageListViewModel::model()
{
	return m_model;
}

int MessageListViewModel::count()
{
	return m_model->rowCount();
}

QString MessageListViewModel::getUserIdByIndex(int idx)
{
	return m_model->data(m_model->index(idx), MessageListModel::UserIdRole).toString();
}

void MessageListViewModel::deleteByIndex(int idx)
{
	m_model->takeItem(idx);
}

void MessageListViewModel::deleteByUserId(QString strUserId)
{
	int idx = getIndexByUserId(strUserId);
	if (idx >= 0)
	{
		deleteByIndex(idx);
	}
}

bool MessageListViewModel::existUserId(QString strUserId)
{
	for (int i = 0; i < m_model->rowCount(); i++)
	{
		QString userId = m_model->data(m_model->index(i), MessageListModel::UserIdRole).toString();
		if (strUserId == userId)
		{
			return true;
		}
	}

	return false;
}

int MessageListViewModel::getUnreadMsgCountByIndex(int idx)
{
	return m_model->data(m_model->index(idx), MessageListModel::UnreadMsgCountRole).toInt();
}

QString MessageListViewModel::getMsgByIndex(int idx)
{
	return m_model->data(m_model->index(idx), MessageListModel::MsgRole).toString();
}

QString MessageListViewModel::getCurrentSelectedUserId()
{
	return m_model->currentSelectedUserId();
}

void MessageListViewModel::setSendStatusFailedByIndex(int idx)
{
	m_model->setData(m_model->index(idx), QStringLiteral("image://HeadProvider/:/MessageRemined/Resources/MessageReminder/send_failed.png"), MessageListModel::SendStatusUrlRole);
}

void MessageListViewModel::resetSendStatusByIndex(int idx)
{
	m_model->setData(m_model->index(idx), QStringLiteral(""), MessageListModel::SendStatusUrlRole);
}

void MessageListViewModel::setSendStatusWaitByIndex(int idx)
{
	m_model->setData(m_model->index(idx), QStringLiteral("image://HeadProvider/:/MessageRemined/Resources/MessageReminder/wait_for_send2.png"), MessageListModel::SendStatusUrlRole);
}

bool MessageListViewModel::isDraftFlagByIndex(int idx)
{
	return m_model->data(m_model->index(idx), MessageListModel::DraftFlagRole).toBool();
}

void MessageListViewModel::updateHeadUrlByUserId(QString strUserId, QString headPicPath)
{
	for (int i = 0; i < m_model->rowCount(); i++)
	{
		QString userId = m_model->data(m_model->index(i), MessageListModel::UserIdRole).toString();
		if (strUserId == userId)
		{
			m_model->setData(m_model->index(i), headPicPath, MessageListModel::HeadUrlRole);
			return;
		}
	}
}

void MessageListViewModel::updateHeadUrlByIndex(int index, QString headPicPath)
{
	m_model->setData(m_model->index(index), headPicPath, MessageListModel::HeadUrlRole);
}
void MessageListViewModel::updateNickNameByIndex(int index, QString nickName)
{
	m_model->setData(m_model->index(index), nickName, MessageListModel::NickNameRole);
}

void MessageListViewModel::updateMsgByIndex(int index, QString msg)
{
	m_model->setData(m_model->index(index), msg, MessageListModel::MsgRole);
}

void MessageListViewModel::updateDraftFlagByIndex(int index, bool hasDraft)
{
	m_model->setData(m_model->index(index), hasDraft, MessageListModel::DraftFlagRole);
}

int MessageListViewModel::getMsgTopOrderByIndex(int index)
{
	return m_model->data(m_model->index(index), MessageListModel::MsgTopOrderRole).toInt();
}

int MessageListViewModel::getItemTypeByIndex(int index)
{
	return m_model->data(m_model->index(index), MessageListModel::ItemTypeRole).toInt();
}

int MessageListViewModel::getItemTypeByUserId(QString strUserId)
{
	int idx = getIndexByUserId(strUserId);
	if (idx >= 0)
	{
		return getItemTypeByIndex(idx);
	}

	return -1;
}

int MessageListViewModel::rowCount()
{
	return m_model->rowCount();
}

QVariant MessageListViewModel::getValueByIndexAndType(int index, MessageListModel::MessageListItemRoles roleType)
{
	return m_model->data(m_model->index(index), roleType);
}

void MessageListViewModel::setValueByIndexAndType(int index, MessageListModel::MessageListItemRoles roleType, const QVariant &value)
{
	m_model->setData(m_model->index(index), value, roleType);
}

void MessageListViewModel::doUpDownKeyClick(bool isUp)
{
	QObject* rootItem = rootObject();
	if (rootItem)
	{
		QVariant param = isUp;
		QMetaObject::invokeMethod(rootItem, "doUpDownKeyClick", Q_ARG(QVariant, param));
	}
}

void MessageListViewModel::changeStyle(QString styleName)
{
	QObject* rootItem = rootObject();
	if (rootItem)
	{
		QVariant param = styleName;
		QMetaObject::invokeMethod(rootItem, "changeStyle", Q_ARG(QVariant, param));
	}

	MessageListView* view = reinterpret_cast<MessageListView*>(this->parent());
	QString strColor = this->rootObject()->property("clearColor").toString();
	view->setClearColor(QColor(strColor));
}

void MessageListViewModel::insertItem(MessageListItem item, const int index)
{
	m_model->insertItem(&item, index);
}

void MessageListViewModel::pushBack(MessageListItem item)
{
	m_model->pushBack(&item);
}

void MessageListViewModel::pushFront(MessageListItem item)
{
	m_model->pushFront(&item);
}


void MessageListViewModel::insertItemWithOrder(MessageListItem item)
{
	for (int i = 0; i < m_model->rowCount(); i++)
	{
		int topOrder = m_model->data(m_model->index(i), MessageListModel::MsgTopOrderRole).toInt();

		if (item.msgTopOrder() == 0)
		{
			if (topOrder == 0)
			{
				m_model->insertItem(&item, i);
				break;
			}
			if (i == m_model->rowCount() - 1)
			{
				m_model->pushBack(&item);
				break;
			}
		}
		else if (item.msgTopOrder() > 0)
		{
			int topOrderAfter = m_model->data(m_model->index(i + 1), MessageListModel::MsgTopOrderRole).toInt();

			if ((item.msgTopOrder() > topOrder) && (i == 0))
			{
				m_model->pushFront(&item);
				break;
			}
			else if ((item.msgTopOrder() < topOrder) && (item.msgTopOrder() > topOrderAfter))
			{
				m_model->insertItem(&item, i + 1);
				break;
			}
			else if (i == m_model->rowCount() - 1)
			{
				m_model->pushBack(&item);
				break;
			}
		}
	}

	if (m_model->rowCount() == 0)
	{
		m_model->pushFront(&item);
	}
}

int MessageListViewModel::getIndexByUserId(QString strUserId)
{
	for (int i = 0; i < m_model->rowCount(); i++)
	{
		QString id = m_model->data(m_model->index(i), MessageListModel::UserIdRole).toString();
		if (id == strUserId)
		{
			return i;
		}
	}

	return -1;
}


QQuickItem *MessageListViewModel::rootObject() const
{
	MessageListView* view = reinterpret_cast<MessageListView*>(this->parent());
	return view->rootObject();
}


int MessageListViewModel::getCurrentIndex()
{
	QObject* listViewMessageList = rootObject()->findChild<QObject*>("listViewMessageList");
	return listViewMessageList->property("currentIndex").toInt();
}
void MessageListViewModel::setCurrentIndex(int index)
{
	QObject* listViewMessageList = rootObject()->findChild<QObject*>("listViewMessageList");
	listViewMessageList->setProperty("currentIndex", index);
}

void MessageListViewModel::doClickIndex(int index)
{
	QObject* rootItem = rootObject();
	if (rootItem)
	{
		QVariant param_index = index;
		QMetaObject::invokeMethod(rootItem, "doClickIndex", Q_ARG(QVariant, param_index));
	}
}

int MessageListViewModel::getAllUnreadNum()
{
	int count = 0;

	for (int i = 0; i < m_model->rowCount(); i++)
	{
		int unreadMsgCount = m_model->data(m_model->index(i), MessageListModel::UnreadMsgCountRole).toInt();
		int iPromptFlag = m_model->data(m_model->index(i), MessageListModel::MsgPromptRole).toInt();
		if (iPromptFlag != 1)
			count += unreadMsgCount;
	}

	return count;
}