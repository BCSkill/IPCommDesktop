﻿#include "messagelist_qml.h"
#include "QStringLiteralBak.h"
#ifdef Q_OS_MAC
#include "mycommonstyle.h"
#endif

#include <QtQml>
#include <QQuickItem>
#include "HeadProvider.h"
#include <QToolTip>

MessageList::MessageList(QWidget *parent)
	: QQuickWidget(parent), m_pMsglistModel(new MessageListModel())
{
	this->setClearColor(QColor("#0a182d"));
	this->setResizeMode(QQuickWidget::SizeRootObjectToView);

	HeadProvider *imageProvider = new HeadProvider(QQmlImageProviderBase::Image);
	this->engine()->addImageProvider(QLatin1String("HeadProvider"), imageProvider);

	this->rootContext()->setContextProperty("messageList", this);
	this->rootContext()->setContextProperty("messageListModel", m_pMsglistModel);
	this->setSource(QUrl("qrc:/MessageList/Resources/MessageList/MessageList.qml"));

	/*connect(this, SIGNAL(itemPressed(QListWidgetItem *)), this, SLOT(doDoubleClickedItem(QListWidgetItem *)));

	QFile scroolbar_style_qss(":/ScrollBar/Resources/QSS/scrollbarStyle.qss");
	scroolbar_style_qss.open(QFile::ReadOnly);
	this->verticalScrollBar()->setStyleSheet(scroolbar_style_qss.readAll());
	this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	this->setMouseTracking(true);
#ifdef Q_OS_MAC
	this->setStyle(new MyProxyStyle);
#endif*/
}

MessageList::~MessageList()
{
	if (m_pMsglistModel)
	{
		delete m_pMsglistModel;
		m_pMsglistModel = NULL;
	}
}

bool MessageList::IsValidImage(QString imgPath)
{
	QFile file(imgPath);
	if (file.open(QIODevice::ReadOnly))
	{
		QByteArray byteArray = file.readAll();
		file.close();
		QImage image = QImage::fromData(byteArray);

		if (image.isNull())
		{
			return false;
		}
	}
	else
	{
		return false;
	}

	return true;
}

int MessageList::getCurrentIndex()
{
	QObject* listViewMessageList = this->rootObject()->findChild<QObject*>("listViewMessageList");
	return listViewMessageList->property("currentIndex").toInt();
}

void MessageList::doClickIndex(int index)
{
	QObject* rootItem = rootObject();
	if (rootItem)
	{
		QVariant param_index = index;
		QMetaObject::invokeMethod(rootItem, "doClickIndex", Q_ARG(QVariant, param_index));
	}
}

void MessageList::doUpDownKeyClick(bool isUp)
{
	QObject* rootItem = rootObject();
	if (rootItem)
	{
		QVariant param = isUp;
		QMetaObject::invokeMethod(rootItem, "doUpDownKeyClick", Q_ARG(QVariant, param));
	}
}


void MessageList::doClickItem(QString userId)
{
	slotCountMessageNum();
	emit sigDoubleClickItem(userId);
	emit sigDealTrayIocnFlash(userId);

}


void MessageList::showToolTip(QString txt)
{
	QToolTip::showText(QCursor::pos(), txt);
}


MessageListInfo MessageList::OnInsertPerToPerMessage(MessageInfo messageinfo, BuddyInfo buddyInfo)
{
	MessageListInfo messageListInfo;

	//首先生成信息时间。
	QString strtemp = QString("%1").arg(messageinfo.ClientTime);
	if (strtemp.length() == 13)
		messageinfo.ClientTime = messageinfo.ClientTime / 1000;

	//根据消息类型设置显示的内容。
	if (messageinfo.MessageChildType == MessageType::Message_PIC) //1图片3视频5文件。
		messageinfo.strMsg = QStringLiteralBak("[图片]");
	if (messageinfo.MessageChildType == MessageType::Message_AUDIO)
		messageinfo.strMsg = QStringLiteralBak("[音频]");
	if (messageinfo.MessageChildType == MessageType::Message_VEDIO) //1图片3视频5文件。
		messageinfo.strMsg = QStringLiteralBak("[视频]");
	if (messageinfo.MessageChildType == MessageType::Message_FILE) //1图片3视频5文件。
		messageinfo.strMsg = QStringLiteralBak("[文件]");
	if (messageinfo.MessageChildType == MessageType::Message_AD)
		messageinfo.strMsg = QStringLiteralBak("该消息类型暂不支持");
	if (messageinfo.MessageChildType == MessageType::Message_TRANSFER)
		messageinfo.strMsg = QStringLiteralBak("[转账]");
	if (messageinfo.MessageChildType == MessageType::Message_SECRETLETTER)
		messageinfo.strMsg = QStringLiteralBak("[密信]");
	if (messageinfo.MessageChildType == MessageType::Message_SECRETIMAGE)
		messageinfo.strMsg = QStringLiteralBak("[密图]");
	if (messageinfo.MessageChildType == MessageType::Message_SECRETFILE)
		messageinfo.strMsg = QStringLiteralBak("[密件]");
	if (messageinfo.MessageChildType == MessageType::Message_NOTICE)  //通告消息
	{
		QJsonDocument doc = QJsonDocument::fromJson(messageinfo.strMsg.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		QString webTitle = map.value("webTitle").toString();
		messageinfo.strMsg = QStringLiteralBak("[通告]") + webTitle;
	}
	if (messageinfo.MessageChildType == MessageType::Message_LOCATION)  //位置消息。
		messageinfo.strMsg = QStringLiteralBak("[位置]");
	if (messageinfo.MessageChildType == MessageType::Message_GW)
		messageinfo.strMsg = QStringLiteralBak("[引力波]");

	//首先判断列表中是否存在
	for (int i = 0; i < m_pMsglistModel->rowCount(); i++)
	{
		QString userId = m_pMsglistModel->data(m_pMsglistModel->index(i), MessageListModel::UserIdRole).toString();
		int unreadMsgCount = m_pMsglistModel->data(m_pMsglistModel->index(i), MessageListModel::UnreadMsgCountRole).toInt();
		if (userId.toInt() == buddyInfo.nUserId)
		{
			QString strNick = buddyInfo.strNote;
			if (buddyInfo.strNote.isEmpty())
				strNick = buddyInfo.strNickName;

			QString unReadNum;
			if (userId == m_pMsglistModel->currentSelectedUserId())
				unReadNum = "";
			else
				unReadNum = QString::number(unreadMsgCount + 1);

			m_pMsglistModel->takeItem(i);

			this->OnInsertMessage(QString::number(buddyInfo.nUserId),
				buddyInfo.strLocalAvatar,
				strNick,
				messageinfo.strMsg, messageinfo.ClientTime, unReadNum, 0);

			messageListInfo.nBudyyID = buddyInfo.nUserId;
			messageListInfo.strLastMessage = messageinfo.strMsg;
			messageListInfo.nLastTime = messageinfo.ClientTime;
			messageListInfo.strBuddyName = buddyInfo.strNickName;
			messageListInfo.strBuddyHeaderImage = buddyInfo.strLocalAvatar;
			messageListInfo.messageType = messageinfo.MessageChildType;
			messageListInfo.nUnReadNum = unreadMsgCount + 1;////该值将要被写到数据库里，数据库未读数量应与显示的未读数量一致
			messageListInfo.isGroup = 0;

			return messageListInfo;
		}
	}

	

	QString strNick = buddyInfo.strNote.isEmpty() ? buddyInfo.strNickName : buddyInfo.strNote;

	this->OnInsertMessage(QString::number(buddyInfo.nUserId),
		buddyInfo.strLocalAvatar,
		strNick,
		messageinfo.strMsg, messageinfo.ClientTime, "1");

	messageListInfo.nBudyyID = buddyInfo.nUserId;
	messageListInfo.strLastMessage = messageinfo.strMsg;
	messageListInfo.nLastTime = messageinfo.ClientTime;
	messageListInfo.strBuddyName = buddyInfo.strNickName;
	messageListInfo.strBuddyHeaderImage = buddyInfo.strLocalAvatar;
	messageListInfo.messageType = messageinfo.MessageChildType;
	messageListInfo.nUnReadNum = 1;
	messageListInfo.isGroup = 0;

	return messageListInfo;
}

MessageListInfo MessageList::OnInsertPerToGroupMessage(MessageInfo messageinfo, GroupInfo groupInfo)
{
	MessageListInfo messageListInfo;

	QString strtemp = QString("%1").arg(messageinfo.ClientTime);
	if (strtemp.length() == 13)
		messageinfo.ClientTime = messageinfo.ClientTime / 1000;

	if (messageinfo.MessageChildType == MessageType::Message_PIC) //1图片3视频5文件。
		messageinfo.strMsg = QStringLiteralBak("[图片]");
	if (messageinfo.MessageChildType == MessageType::Message_AUDIO)
		messageinfo.strMsg = QStringLiteralBak("[音频]");
	if (messageinfo.MessageChildType == MessageType::Message_VEDIO) //1图片3视频5文件。
		messageinfo.strMsg = QStringLiteralBak("[视频]");
	if (messageinfo.MessageChildType == MessageType::Message_FILE) //1图片3视频5文件。
		messageinfo.strMsg = QStringLiteralBak("[文件]");
	if (messageinfo.MessageChildType == MessageType::Message_AD)
		messageinfo.strMsg = QStringLiteralBak("该消息类型暂不支持");
	if (messageinfo.MessageChildType == MessageType::Message_NOTICE)  //通告消息
	{
		QJsonDocument doc = QJsonDocument::fromJson(messageinfo.strMsg.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		QString webTitle = map.value("webTitle").toString();
		messageinfo.strMsg = QStringLiteralBak("[通告]") + webTitle;
	}
	if (messageinfo.MessageChildType == MessageType::Message_LOCATION)   //位置消息。
		messageinfo.strMsg = QStringLiteralBak("[位置]");

	if (!messageinfo.strMsg.isEmpty())
	{
		QString fromID = QString::number(messageinfo.nFromUserID);
		QString groupID = QString::number(messageinfo.nToUserID);
		BuddyInfo buddyInfo = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(groupID, fromID);
		
		if (buddyInfo.strNote.isEmpty() && buddyInfo.strNickName.isEmpty())
		{
			//如果取不到任何名字信息，不显示冒号
			//后期处理
		}
		else
		{
			if (buddyInfo.strNote.isEmpty())
				messageinfo.strMsg = buddyInfo.strNickName + ":" + messageinfo.strMsg;
			else
				messageinfo.strMsg = buddyInfo.strNote + ":" + messageinfo.strMsg;
		}

		
	}

	//首先判断列表中是否存在
	for (int i = 0; i < m_pMsglistModel->rowCount(); i++)
	{
		QString userId = m_pMsglistModel->data(m_pMsglistModel->index(i), MessageListModel::UserIdRole).toString();
		int unreadMsgCount = m_pMsglistModel->data(m_pMsglistModel->index(i), MessageListModel::UnreadMsgCountRole).toInt();

		if (userId == groupInfo.groupId)
		{
			QString unReadNum;
			if (m_pMsglistModel->currentSelectedUserId() == userId && this->isVisible())
				unReadNum = "";
			else
				unReadNum = QString::number(unreadMsgCount + 1);

			m_pMsglistModel->takeItem(i);
			this->OnInsertMessage(groupInfo.groupId,
				groupInfo.groupLoacalHeadImage,
				groupInfo.groupName,
				messageinfo.strMsg, messageinfo.ClientTime, unReadNum, 1);

			messageListInfo.nBudyyID = groupInfo.groupId.toInt();
			messageListInfo.strLastMessage = messageinfo.strMsg;
			messageListInfo.nLastTime = messageinfo.ClientTime;
			messageListInfo.strBuddyName = groupInfo.groupName;
			messageListInfo.strBuddyHeaderImage = groupInfo.groupLoacalHeadImage;
			messageListInfo.messageType = messageinfo.MessageChildType;
			messageListInfo.nUnReadNum = unreadMsgCount + 1;//该值将要被写到数据库里，数据库未读数量应与显示的未读数量一致
			messageListInfo.isGroup = 1;

			return messageListInfo;
		}
	}

	this->OnInsertMessage(groupInfo.groupId,
		groupInfo.groupLoacalHeadImage,
		groupInfo.groupName,
		messageinfo.strMsg, messageinfo.ClientTime, "1", 1);

	//插入新的
	messageListInfo.nBudyyID = groupInfo.groupId.toInt();
	messageListInfo.strLastMessage = messageinfo.strMsg;
	messageListInfo.nLastTime = messageinfo.ClientTime;
	messageListInfo.strBuddyName = groupInfo.groupName;
	messageListInfo.strBuddyHeaderImage = groupInfo.groupLoacalHeadImage;
	messageListInfo.messageType = messageinfo.MessageChildType;
	messageListInfo.nUnReadNum = 1;
	messageListInfo.isGroup = 1;

	return messageListInfo;
}

MessageListInfo MessageList::OnClickPerMessage(MessageInfo messageinfo, BuddyInfo buddyInfo)
{
	//首先生成信息时间。
	QString strtemp = QString("%1").arg(messageinfo.ClientTime);
	if (strtemp.length() == 13)
		messageinfo.ClientTime = messageinfo.ClientTime / 1000;

	//根据消息类型设置显示的内容。
	if (messageinfo.MessageChildType == MessageType::Message_PIC) //1图片3视频5文件。
		messageinfo.strMsg = QStringLiteralBak("[图片]");
	if (messageinfo.MessageChildType == MessageType::Message_AUDIO)
		messageinfo.strMsg = QStringLiteralBak("[音频]");
	if (messageinfo.MessageChildType == MessageType::Message_VEDIO) //1图片3视频5文件。
		messageinfo.strMsg = QStringLiteralBak("[视频]");
	if (messageinfo.MessageChildType == MessageType::Message_FILE) //1图片3视频5文件。
		messageinfo.strMsg = QStringLiteralBak("[文件]");
	if (messageinfo.MessageChildType == MessageType::Message_AD)
		messageinfo.strMsg = QStringLiteralBak("该消息类型暂不支持");
	if (messageinfo.MessageChildType == MessageType::Message_TRANSFER)
		messageinfo.strMsg = QStringLiteralBak("[转账]");
	if (messageinfo.MessageChildType == MessageType::Message_SECRETLETTER)
		messageinfo.strMsg = QStringLiteralBak("[密信]");
	if (messageinfo.MessageChildType == MessageType::Message_SECRETIMAGE)
		messageinfo.strMsg = QStringLiteralBak("[密图]");
	if (messageinfo.MessageChildType == MessageType::Message_SECRETFILE)
		messageinfo.strMsg = QStringLiteralBak("[密件]");
	if (messageinfo.MessageChildType == MessageType::Message_NOTICE)  //通告消息
	{
		QJsonDocument doc = QJsonDocument::fromJson(messageinfo.strMsg.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		QString webTitle = map.value("webTitle").toString();
		messageinfo.strMsg = QStringLiteralBak("[通告]") + webTitle;
	}
	if (messageinfo.MessageChildType == MessageType::Message_LOCATION)  //位置消息。
		messageinfo.strMsg = QStringLiteralBak("[位置]");
	if (messageinfo.MessageChildType == MessageType::Message_GW)
		messageinfo.strMsg = QStringLiteralBak("[引力波]");

	MessageListInfo messageListInfo;
	messageListInfo.nBudyyID = buddyInfo.nUserId;
	messageListInfo.strLastMessage = messageinfo.strMsg;
	messageListInfo.nLastTime = messageinfo.ClientTime;
	messageListInfo.strBuddyName = buddyInfo.strNickName;
	messageListInfo.strBuddyHeaderImage = buddyInfo.strLocalAvatar;
	messageListInfo.messageType = messageinfo.MessageChildType;
	messageListInfo.nUnReadNum = 0;
	messageListInfo.isGroup = 0;

	//首先判断列表中是否存在
	for (int i = 0; i < m_pMsglistModel->rowCount(); i++)
	{
		QString userId = m_pMsglistModel->data(m_pMsglistModel->index(i), MessageListModel::UserIdRole).toString();
		int unreadMsgCount = m_pMsglistModel->data(m_pMsglistModel->index(i), MessageListModel::UnreadMsgCountRole).toInt();
		if (userId.toInt() == buddyInfo.nUserId)
		{
			doClickIndex(i);
			return messageListInfo;
		}

	}


	QString strNick = buddyInfo.strNote.isEmpty() ? buddyInfo.strNickName : buddyInfo.strNote;

	this->OnInsertMessage(QString::number(buddyInfo.nUserId),
		buddyInfo.strLocalAvatar,
		strNick,
		messageinfo.strMsg, messageinfo.ClientTime, "");

	return messageListInfo;
}

MessageListInfo MessageList::OnClickGroupMessage(MessageInfo messageinfo, GroupInfo groupInfo)
{
	QString strtemp = QString("%1").arg(messageinfo.ClientTime);
	if (strtemp.length() == 13)
		messageinfo.ClientTime = messageinfo.ClientTime / 1000;

	if (messageinfo.MessageChildType == MessageType::Message_PIC) //1图片3视频5文件。
		messageinfo.strMsg = QStringLiteralBak("[图片]");
	if (messageinfo.MessageChildType == MessageType::Message_AUDIO)
		messageinfo.strMsg = QStringLiteralBak("[音频]");
	if (messageinfo.MessageChildType == MessageType::Message_VEDIO) //1图片3视频5文件。
		messageinfo.strMsg = QStringLiteralBak("[视频]");
	if (messageinfo.MessageChildType == MessageType::Message_FILE) //1图片3视频5文件。
		messageinfo.strMsg = QStringLiteralBak("[文件]");
	if (messageinfo.MessageChildType == MessageType::Message_AD)
		messageinfo.strMsg = QStringLiteralBak("该消息类型暂不支持");
	if (messageinfo.MessageChildType == MessageType::Message_NOTICE)  //通告消息
	{
		QJsonDocument doc = QJsonDocument::fromJson(messageinfo.strMsg.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		QString webTitle = map.value("webTitle").toString();
		messageinfo.strMsg = QStringLiteralBak("[通告]") + webTitle;
	}
	if (messageinfo.MessageChildType == MessageType::Message_LOCATION)   //位置消息。
		messageinfo.strMsg = QStringLiteralBak("[位置]");

	if (!messageinfo.strMsg.isEmpty())
	{
		QString fromID = QString::number(messageinfo.nFromUserID);
		QString groupID = QString::number(messageinfo.nToUserID);
		BuddyInfo buddyInfo = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(groupID, fromID);
		if (buddyInfo.strNote.isEmpty() && buddyInfo.strNickName.isEmpty())
		{
			//如果取不到任何名字信息，不显示冒号
			//后期处理
		}
		else
		{
			if (buddyInfo.strNote.isEmpty())
				messageinfo.strMsg = buddyInfo.strNickName + ":" + messageinfo.strMsg;
			else
				messageinfo.strMsg = buddyInfo.strNote + ":" + messageinfo.strMsg;
		}
	}

	MessageListInfo messageListInfo;
	messageListInfo.nBudyyID = groupInfo.groupId.toInt();
	messageListInfo.strLastMessage = messageinfo.strMsg;
	messageListInfo.nLastTime = messageinfo.ClientTime;
	messageListInfo.strBuddyName = groupInfo.groupName;
	messageListInfo.strBuddyHeaderImage = groupInfo.groupLoacalHeadImage;
	messageListInfo.messageType = messageinfo.MessageChildType;
	messageListInfo.nUnReadNum = 0;
	messageListInfo.isGroup = 1;

	//首先判断列表中是否存在
	for (int i = 0; i < m_pMsglistModel->rowCount(); i++)
	{
		QString userId = m_pMsglistModel->data(m_pMsglistModel->index(i), MessageListModel::UserIdRole).toString();
		int unreadMsgCount = m_pMsglistModel->data(m_pMsglistModel->index(i), MessageListModel::UnreadMsgCountRole).toInt();

		if (userId == groupInfo.groupId)
		{
			doClickIndex(i);
			return messageListInfo;
		}
	}

	

	this->OnInsertMessage(groupInfo.groupId,
		groupInfo.groupLoacalHeadImage,
		groupInfo.groupName,
		messageinfo.strMsg, messageinfo.ClientTime, "", 1);

	return messageListInfo;
}


void MessageList::OnInsertMessage(QString strUserID,
	QString strPicPath,
	QString strNickName,
	QString strAutoGrapthText,
	int MessageTime,
	QString strMessageNum,
	int nType,
	int nHeight)
{
	//先对列表中是否存在进行判断。
	for (int i = 0; i < m_pMsglistModel->rowCount(); i++)
	{
		QString userId = m_pMsglistModel->data(m_pMsglistModel->index(i), MessageListModel::UserIdRole).toString();
		if (strUserID == userId)
		{
			//如果列表中存在，直接退出。
			return;
		}
	}

	//判断strPicPath是否有效的图片，无效的话用默认图片
	if (!IsValidImage(strPicPath))
	{
		if (nType == 0)//个人
		{
			strPicPath = QStringLiteral(":/PerChat/Resources/person/temp.png");
		}
		else //群组
		{
			strPicPath = QStringLiteral(":/GroupChat/Resources/groupchat/group.png");
		}
	}

	QString headPicPath = QStringLiteral("image://HeadProvider/") + strPicPath;

	QDateTime dateTime = QDateTime::fromTime_t(MessageTime);
	QString strTime = dateTime.toString("hh:mm");

	if (dateTime.daysTo(QDateTime::currentDateTime()) >= 2)
		strTime = dateTime.toString("M-d ") + strTime;
	else
	{
		if (dateTime.daysTo(QDateTime::currentDateTime()) >= 1)
			strTime = QStringLiteralBak("昨天 ") + strTime;
	}

	MessageListItem *item = new MessageListItem(nType, strUserID, headPicPath, strNickName, strAutoGrapthText, strTime, strMessageNum.toInt());
	m_pMsglistModel->pushFront(item);

	if (strAutoGrapthText.isEmpty() || strMessageNum.isEmpty())
	{
		this->doClickIndex(0);
	}

	slotCountMessageNum();

}

void MessageList::keyPressEvent(QKeyEvent *event)
{
	if (event->key() == Qt::Key_Up)
	{
		doUpDownKeyClick(true);
	}
	if (event->key() == Qt::Key_Down)
	{
		doUpDownKeyClick(false);
	}


	return QQuickWidget::keyPressEvent(event);
}



MessageListInfo MessageList::getCurrentItemInfo()
{
    MessageListInfo listInfo;
    listInfo.nBudyyID = -1;
	int idx = getCurrentIndex();

	QString strUserID = m_pMsglistModel->data(m_pMsglistModel->index(idx), MessageListModel::UserIdRole).toString();
	listInfo.nBudyyID = strUserID.toInt();

	int nType = m_pMsglistModel->data(m_pMsglistModel->index(idx), MessageListModel::ItemTypeRole).toInt();
	listInfo.isGroup = nType;

	return listInfo;
}

void MessageList::OnUpdateMessage(bool isGroup, QVariant var)
{
	if (isGroup)
	{
		GroupInfo groupInfo = var.value<GroupInfo>();
		for (int i = 0; i < m_pMsglistModel->rowCount(); i++)
		{
			QString userId = m_pMsglistModel->data(m_pMsglistModel->index(i), MessageListModel::UserIdRole).toString();
			if (userId == groupInfo.groupId)
			{
				QString headUrl = "";
				if (IsValidImage(groupInfo.groupLoacalHeadImage))
				{
					headUrl = QStringLiteral("image://HeadProvider/") + groupInfo.groupLoacalHeadImage;
				}
				else
				{
					if (isGroup)
					{
						headUrl = QStringLiteral("image://HeadProvider/:/GroupChat/Resources/groupchat/group.png");
					}
					else
					{
						headUrl = QStringLiteral("image://HeadProvider/:/PerChat/Resources/person/temp.png");
					}
				}

				
				m_pMsglistModel->setData(m_pMsglistModel->index(i), headUrl, MessageListModel::HeadUrlRole);
				m_pMsglistModel->setData(m_pMsglistModel->index(i), groupInfo.groupName, MessageListModel::NickNameRole);
			}
		}
	}
	else
	{
		BuddyInfo buddyInfo = var.value<BuddyInfo>();
		for (int i = 0; i < m_pMsglistModel->rowCount(); i++)
		{
			QString userId = m_pMsglistModel->data(m_pMsglistModel->index(i), MessageListModel::UserIdRole).toString();

			if (userId == QString::number(buddyInfo.nUserId))
			{
				QString headUrl = "";
				if (IsValidImage(buddyInfo.strLocalAvatar))
				{
					headUrl = QStringLiteral("image://HeadProvider/") + buddyInfo.strLocalAvatar;
				}
				else
				{
					if (isGroup)
					{
						headUrl = QStringLiteral("image://HeadProvider/:/GroupChat/Resources/groupchat/group.png");
					}
					else
					{
						headUrl = QStringLiteral("image://HeadProvider/:/PerChat/Resources/person/temp.png");
					}
				}

				
				m_pMsglistModel->setData(m_pMsglistModel->index(i), headUrl, MessageListModel::HeadUrlRole);
				if (buddyInfo.strNote.isEmpty())
				{
					m_pMsglistModel->setData(m_pMsglistModel->index(i), buddyInfo.strNickName, MessageListModel::NickNameRole);
				}
				else
				{
					m_pMsglistModel->setData(m_pMsglistModel->index(i), buddyInfo.strNote, MessageListModel::NickNameRole);
				}

			}
		}
	}
}

void MessageList::slotCountMessageNum()
{
	getMessageUnreadNum();
}

int MessageList::getMessageUnreadNum()
{
	int count = 0;

	for (int i = 0; i < m_pMsglistModel->rowCount(); i++)
	{
		int unreadMsgCount = m_pMsglistModel->data(m_pMsglistModel->index(i), MessageListModel::UnreadMsgCountRole).toInt();
		count += unreadMsgCount;
	}

	emit sigMessageNum(count);

	return count;
}

void MessageList::slotKeyUpDown(QKeyEvent *event)
{
	this->keyPressEvent(event);
}

void MessageList::slotInsertSelfMessage(bool isGroup, QVariant var, QString msg)
{
	if (isGroup)
	{
		GroupInfo groupInfo = var.value<GroupInfo>();
		for (int i = 0; i <  m_pMsglistModel->rowCount(); i++)
		{
			QString userId = m_pMsglistModel->data(m_pMsglistModel->index(i), MessageListModel::UserIdRole).toString();
			if (userId == groupInfo.groupId)
			{
				if (userId == groupInfo.groupId)
				{
					int time = QDateTime::currentDateTime().toTime_t();

					m_pMsglistModel->takeItem(i);
					this->OnInsertMessage(groupInfo.groupId,
						groupInfo.groupLoacalHeadImage,
						groupInfo.groupName,
						msg, time, "", 1);
				}
			}

		}
	}
	else
	{
		BuddyInfo buddyInfo = var.value<BuddyInfo>();
		for (int i = 0; i < m_pMsglistModel->rowCount(); i++)
		{
			QString userId = m_pMsglistModel->data(m_pMsglistModel->index(i), MessageListModel::UserIdRole).toString();
			if (userId == QString::number(buddyInfo.nUserId))
			{
				if (userId == QString::number(buddyInfo.nUserId))
				{
					int time_t = QDateTime::currentDateTime().toTime_t();

					QString strNick;
					if (buddyInfo.strNote.isEmpty())
						strNick = buddyInfo.strNickName;
					else
						strNick = buddyInfo.strNote;

					m_pMsglistModel->takeItem(i);
					this->OnInsertMessage(QString::number(buddyInfo.nUserId),
						buddyInfo.strLocalAvatar,
						strNick,
						msg, time_t, "", 0);
				}
			}

		}
		

	}
}

void MessageList::onDeleteItem(int row)
{
	m_pMsglistModel->takeItem(row);
	slotCountMessageNum();

	if (m_pMsglistModel->rowCount() > 0)
	{
		doClickIndex(0);
	}
}

