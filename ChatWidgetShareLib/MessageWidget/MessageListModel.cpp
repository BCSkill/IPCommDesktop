﻿#include "MessageListModel.h"
#include <QDateTime>

MessageListItem::MessageListItem(const int &itemType, const QString &userId, const QString &headUrl, const QString &nickName, const QString &msg, const QString &msgTime, const int &unreadMsgCount, const int &msgTopOrder, const int &msgPrompt, const bool &bHasDraft)
	: m_itemType(itemType), m_userId(userId), m_headUrl(headUrl), m_nickName(nickName), m_msg(msg), m_msgTime(msgTime), m_unreadMsgCount(unreadMsgCount), m_msgTopOrder(msgTopOrder), m_msgPrompt(msgPrompt), m_bHadDraft(bHasDraft)
{
}

int MessageListItem::itemType() const
{
	return m_itemType;
}

QString MessageListItem::userId() const
{
	return m_userId;
}

QString MessageListItem::headUrl() const
{
	return m_headUrl;
}

QString MessageListItem::nickName() const
{
	return m_nickName;
}

QString MessageListItem::msg() const
{
	return m_msg;
}

QString MessageListItem::msgTime() const
{
	return m_msgTime;
}

int MessageListItem::unreadMsgCount() const
{
	return m_unreadMsgCount;
}

QString MessageListItem::sendStatusUrl() const
{
	return m_sendStatusUrl;
}

int MessageListItem::msgTopOrder() const
{
	return m_msgTopOrder;
}

int MessageListItem::msgPrompt() const
{
	return m_msgPrompt;
}
bool MessageListItem::bHasDraft() const
{
	return m_bHadDraft;
}

void MessageListItem::setItemType(QVariant val)
{
	m_itemType = val.toInt();
}

void MessageListItem::setUserId(QVariant val)
{
	m_userId = val.toString();
}

void MessageListItem::setHeadUrl(QVariant val)
{
	m_headUrl = val.toString();
}

void MessageListItem::setNickName(QVariant val)
{
	m_nickName = val.toString();
}

void MessageListItem::setMsg(QVariant val)
{
	m_msg = val.toString();
}

void MessageListItem::setMsgTime(QVariant val)
{
	m_msgTime = val.toString();
}

void MessageListItem::setUnreadMsgCount(QVariant val)
{
	m_unreadMsgCount = val.toInt();
}

void MessageListItem::setSendStatusUrl(QVariant val)
{
	m_sendStatusUrl = val.toString();
}

void MessageListItem::setMsgTopOrder(QVariant val)
{
	m_msgTopOrder = val.toInt();
}
void MessageListItem::setMsgPrompt(QVariant val)
{
	m_msgPrompt = val.toInt();
}
void MessageListItem::setbHasDraft(QVariant val)
{
	m_bHadDraft= val.toBool();
}







MessageListModel::MessageListModel(QObject *parent)
	: QAbstractListModel(parent)
{
}

void MessageListModel::pushFront(const MessageListItem *item)
{
	beginInsertRows(QModelIndex(), 0, 0);
	m_items.push_front(*item);
	endInsertRows();
}

void MessageListModel::pushBack(const MessageListItem *item)
{
	beginInsertRows(QModelIndex(), rowCount(), rowCount());
	m_items.push_back(*item);
	endInsertRows();
}
void MessageListModel::insertItem(const MessageListItem *item, const int index)
{
	beginInsertRows(QModelIndex(), index, index);
	m_items.insert(index, *item);
	endInsertRows();
}


int MessageListModel::rowCount(const QModelIndex & parent) const {
	Q_UNUSED(parent);
	return m_items.count();
}


QVariant MessageListModel::data(const QModelIndex & index, int role) const {
	if (index.row() < 0 || index.row() >= m_items.count())
		return QVariant();

	const MessageListItem &item = m_items[index.row()];

	if (role == ItemTypeRole)
	{
		return item.itemType();
	}
	else if (role == UserIdRole)
	{
		return item.userId();
	}
	else if (role == HeadUrlRole)
	{
		return item.headUrl();
	}
	else if (role == NickNameRole)
	{
		return item.nickName();
	}
	else if (role == MsgRole)
	{
		return item.msg();
	}
	else if (role == MsgTimeRole)
	{
		return item.msgTime();
	}
	else if (role == UnreadMsgCountRole)
	{
		return item.unreadMsgCount();
	}
	else if (role == SendStatusUrlRole)
	{
		return item.sendStatusUrl();
	}
	else if (role == MsgTopOrderRole)
	{
		return item.msgTopOrder();
	}
	else if (role == MsgPromptRole)
	{
		return item.msgPrompt();
	}
	else if (role == DraftFlagRole)
	{
		return item.bHasDraft();
	}


	return QVariant();
}

bool MessageListModel::setData(const QModelIndex &index, const QVariant &value, int role/* = Qt::EditRole*/)
{
	if (index.row() < 0 || index.row() >= m_items.count())
		return false;

	MessageListItem &item = m_items[index.row()];
	if (role == ItemTypeRole)
	{
		item.setItemType(value);
	}
	else if (role == UserIdRole)
	{
		item.setUserId(value);
	}
	else if (role == HeadUrlRole)
	{
		item.setHeadUrl(value.toString() + "?" + QString::number(QDateTime::currentDateTime().toMSecsSinceEpoch()));
	}
	else if (role == NickNameRole)
	{
		item.setNickName(value);
	}
	else if (role == MsgRole)
	{
		item.setMsg(value);
	}
	else if (role == MsgTimeRole)
	{
		item.setMsgTime(value);
	}
	else if (role == UnreadMsgCountRole)
	{
		item.setUnreadMsgCount(value);
	}
	else if (role == SendStatusUrlRole)
	{
		item.setSendStatusUrl(value);
	}
	else if (role == MsgTopOrderRole)
	{
		item.setMsgTopOrder(value);
	}
	else if (role == MsgPromptRole)
	{
		item.setMsgPrompt(value);
	}
	else if (role == DraftFlagRole)
	{
		item.setbHasDraft(value);
	}

	emit dataChanged(index, index);
	return true;
}

//![0]
QHash<int, QByteArray> MessageListModel::roleNames() const {
	QHash<int, QByteArray> roles;
	roles[ItemTypeRole] = "itemType";
	roles[UserIdRole] = "userId";
	roles[HeadUrlRole] = "headUrl";
	roles[NickNameRole] = "nickName";
	roles[MsgRole] = "msg";
	roles[MsgTimeRole] = "msgTime";
	roles[UnreadMsgCountRole] = "unreadMsgCount";
	roles[SendStatusUrlRole] = "sendStatusUrl";
	roles[MsgTopOrderRole] = "msgTopOrder";
	roles[MsgPromptRole] = "msgPrompt";

	return roles;
}

void MessageListModel::clear()
{
	if (m_items.count() > 0)
	{
		beginRemoveRows(QModelIndex(), 0, m_items.count() - 1);
		m_items.clear();
		endRemoveRows();
	}
	
}

void MessageListModel::takeItem(int row)
{
	beginRemoveRows(QModelIndex(), row, row);
	m_items.removeAt(row);
	endRemoveRows();
}

QString MessageListModel::currentSelectedUserId()
{
	return  m_currentSelectedUserId;
}

void MessageListModel::setCurrentSelectedUserId(QString userId)
{
	m_currentSelectedUserId = userId;
}