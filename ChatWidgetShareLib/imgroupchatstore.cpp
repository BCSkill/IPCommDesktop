﻿#include "imgroupchatstore.h"
#include "imgroupchatview.h"
#include "imgroupchatdispatcher.h"
#include "imgroupchatviewmodel.h"
#include "imbuddy.h"
#include "imdatabaseoperainfo.h"
#include "settingsmanager.h"
#include "amrDec/amrdec.h"
#include "VedioFrameOpera.h"
#include "childWidget/filetypeex.h"
#include "httpnetworksharelib.h"
#include "imdownloadheaderimg.h"
#include "imdatamanagersharelib.h"
#include "imsocketmessageinfo.h"
#include "questionbox.h"
#include "messagebox.h"
#include "imtranstype.h"
#include "GroupUserWidget/GroupUserListModel.h"
#include "profilemanager.h"
#include "imcommon.h"
#include "childWidget/expresswidget.h"

#include <QDateTime>
#include <QFile>
#include <QJsonDocument>
#include <QImageReader>
#include <QCryptographicHash>
#include <QDir>
#include <QDebug>
#include <QUuid>
#include <QSettings>
#include <QFileDialog>
#include <QProcess>


/*获取当前时间点*1000宏定义*/
#define GETLOCALTIME (1000*((qlonglong)(QDateTime::currentDateTime().toTime_t())))

extern IMDataBaseOperaInfo *gDataBaseOpera;
extern SettingsManager* gSettingsManager;
extern IMDataManagerShareLib *gDataManager;
extern IMSocketMessageInfo *gSocketMessage;

extern QString gI18NLocale;

IMGroupChatStore::IMGroupChatStore(QObject *parent) : QObject(parent)
, m_iHttp(0)
{

}

void IMGroupChatStore::init()
{
	m_view = reinterpret_cast<IMGroupChatView*>(parent());
	m_dispatcher = m_view->dispatcher();
	m_vm = m_view->vm();

	connect(m_dispatcher, SIGNAL(sigProcessRecvMessageInfo(MessageInfo)), this, SLOT(slotProcessRecvMessageInfo(MessageInfo)));
	connect(m_dispatcher, SIGNAL(sigUserQuitGroup(QString)), this, SLOT(slotUserQuitGroup(QString)));
	connect(m_dispatcher, SIGNAL(sigInsertGroupUser(BuddyInfo)), this, SLOT(slotInsertGroupUser(BuddyInfo)));
	connect(m_dispatcher, SIGNAL(sigSetNoSpeak(int)), this, SLOT(slotSetNoSpeak(int)));
	connect(m_dispatcher, SIGNAL(sigInsertGroupUserList()), this, SLOT(slotInsertGroupUserList()));
	connect(m_dispatcher, SIGNAL(sigShowByChatId(bool)), this, SLOT(slotShowByChatId(bool)));
	connect(m_dispatcher, SIGNAL(sigUpdateMessageStateInfo(QByteArray, int, int)), this, SLOT(slotUpdateMessageStateInfo(QByteArray, int, int)));
	connect(m_dispatcher, SIGNAL(sigInsertTextEditPic(QString)), this, SLOT(slotInsertTextEditPic(QString)));
	connect(m_dispatcher, SIGNAL(sigSendTransmitMessage(MessageInfo)), this, SLOT(slotSendTransmitMessage(MessageInfo)));
	connect(m_dispatcher, SIGNAL(sigShareID(int, QString)), this, SLOT(slotShareID(int, QString)));

	connect(m_dispatcher, SIGNAL(sigSendAtMessage(QString, QString)), this, SLOT(slotSendAtMessage(QString, QString)));
	
	connect(m_dispatcher, SIGNAL(sigGivePacketData(QString)), this, SLOT(slotGivePacketData(QString)));
	connect(m_dispatcher, SIGNAL(sigSendNotice(QMap<QString, QString>)), this, SLOT(slotSendNotice(QMap<QString, QString>)));
	connect(m_dispatcher, SIGNAL(sigThreadLoadGroupUserInfo(BuddyInfo)), this, SLOT(slotThreadLoadGroupUserInfo(BuddyInfo)));

	connect(m_dispatcher, SIGNAL(sigZoomImg(QString)), this, SLOT(slotZoomImg(QString)));
	connect(m_dispatcher, SIGNAL(sigGetFile(QString)), this, SLOT(slotGetFile(QString)));
	connect(m_dispatcher, SIGNAL(sigSaveFile(QString)), this, SLOT(slotSaveFile(QString)));
	connect(m_dispatcher, SIGNAL(sigSendFile(QString)), this, SLOT(slotSendFile(QString)));
	connect(m_dispatcher, SIGNAL(sigDrags(QStringList)), this, SLOT(slotDrags(QStringList)));
	connect(m_dispatcher, SIGNAL(sigLoadMore()), this, SLOT(slotLoadMore()));

	connect(m_dispatcher, SIGNAL(sigTipMessage(int, QString, QString)), this, SLOT(slotTipMessage(int, QString, QString)));

	connect(m_dispatcher, SIGNAL(sigItemDoubleClicked(int)), this, SLOT(slotItemDoubleClicked(int)));

	connect(m_dispatcher, SIGNAL(sigProcessSendMessageInfo(QString, MessageInfo)), this, SLOT(slotProcessSendMessageInfo(QString, MessageInfo)));
}

QString IMGroupChatStore::GetBuddyHeaderImage(QString strGroupID, QString strBuddyID)
{
	BuddyInfo buddyInfo = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(strGroupID, strBuddyID);
	if (buddyInfo.nUserId == strBuddyID.toInt())
	{
		return buddyInfo.strLocalAvatar;
	}

	return "";
}

void IMGroupChatStore::onInsertTimeLine(qlonglong time_t)
{
	//添加时间线。
	QString strSend = QString("addTime(\"%1\");").arg(time_t);
	m_vm->runJs(strSend);
}

QString IMGroupChatStore::objectName() const
{
	return m_vm->getGroupId();
}

bool IMGroupChatStore::IsValidImage(QString imgPath)
{
	QFile file(imgPath);
	if (file.open(QIODevice::ReadOnly))
	{
		QByteArray byteArray = file.readAll();
		file.close();
		QImage image = QImage::fromData(byteArray);

		if (image.isNull())
		{
			return false;
		}
	}
	else
	{
		return false;
	}

	return true;
}

QString IMGroupChatStore::convertHeaderToHtmlSrc(QString headerImage)
{
	if (IsValidImage(headerImage))
	{
		if (headerImage.startsWith("file:///"))
		{
		}
		else
		{
			headerImage = "file:///" + headerImage;
		}
		headerImage.replace(" ", "%20");
	}
	else
	{
		headerImage = "qrc:/html/Resources/html/temp.png";
	}

	return headerImage;
}

//解析内容里的url,转成<a href='http://www.xxx.com'>http://www.xxx.com</a>,其它内容toHtmlEscaped()
QString IMGroupChatStore::recognizeUrl(const QString& strMessage)
{
	QString strRet;

	if (strMessage.isEmpty())
		return strMessage;

	QString str_src(strMessage);

	// 从 strMessage 中过滤 url形式的正则表达式
	QRegExp re_url_val("((ht|f)tp(s?)\\:\\/\\/|[0-9a-zA-Z]+\\.)[0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*(:(0-9)*)*(\\/?)([a-zA-Z0-9\\-\\.\\?\\,\\'\\/\\\\\\+&amp;%\\$#=!_\\*]*)?");

	int nFlags = 0;
	while (re_url_val.indexIn(str_src) != -1)
	{
		QString cap_str = re_url_val.cap(0);
		int cur_idx = re_url_val.indexIn(str_src);


		QString leftStr = str_src.left(cur_idx);


		strRet += leftStr.toHtmlEscaped();

		str_src.remove(0, cur_idx + cap_str.length());    // 删除已经遍历过的字符

														  // 格式化为 <a />标签形式
		QString cap_str_link;
		if (cap_str.startsWith("http://")
			|| cap_str.startsWith("https://")
			|| cap_str.startsWith("ftp://")
			|| cap_str.startsWith("ftps://")
			)
		{
			cap_str_link = cap_str;
		}
		else
		{
			cap_str_link = QString("http://") + cap_str;
		}

		strRet += QString("<a href=javascript:window.bridge.slotOpenUrl(&quot;%1&quot;) >%2</a>").arg(QString(cap_str_link).toUtf8().toPercentEncoding(), cap_str.toHtmlEscaped());

		nFlags = 1;
	}
	if (nFlags == 0)
	{
		strRet = strMessage.toHtmlEscaped();

		return strRet;
	}

	strRet += str_src.toHtmlEscaped();

	return strRet;
}

// 将 包含[xx]表情形式的信息字符串 转换为 表情的路径形式并且HTML编码
QString IMGroupChatStore::formatMessageFromImgDescriptionWithHtmlEncode(const QString& strMessage)
{
	QString strRet;

	if (strMessage.isEmpty())
		return strMessage;

	QString str_src(strMessage);

	QRegExp re_img_val("\\[[^\\[^\\]]*\\]");      // 从 strMessage 中过滤 [xxx] 形式的正则表达式

	int nFlags = 0;
	while (re_img_val.indexIn(str_src) != -1)
	{
		QString img_descrip = re_img_val.cap(0);
		int cur_idx = re_img_val.indexIn(str_src);

		strRet += recognizeUrl(str_src.left(cur_idx));

		str_src.remove(0, cur_idx + img_descrip.length());    // 删除已经遍历过的字符

		QString img_path = ExpressWidget::GetImagePathByDescription(img_descrip);
		if (!img_path.isEmpty())
		{
			// 格式化为 <img />标签形式
			strRet += "<img src='qrc:/expression/Resources" + img_path + "'/>";
		}
		else
		{
			// 如果没有找到则保持原字符串不变
			strRet += recognizeUrl(img_descrip);
		}
		nFlags = 1;
	}
	if (nFlags == 0)
	{
		strRet = recognizeUrl(strMessage);

		return strRet;
	}

	strRet += recognizeUrl(str_src);

	return strRet;
}

//接收消息添加到webview
void IMGroupChatStore::OnInsertRecvMessageTextInfo(QString strMsg, QString strHeadImage, QString strNickName, QString strMsgId, int score, QString strBuddyId)
{
	QString strPath = convertHeaderToHtmlSrc(strHeadImage);

	QString strMessage = formatMessageFromImgDescriptionWithHtmlEncode(strMsg);

	//传入备注名。
	strMessage = strMessage.toUtf8().toPercentEncoding();
	QString strSend = QString("GroupRecvAppend(\"%1\",\"%2\",\"%3\",\"%4\",%5,\"%6\")").arg(strPath, strMessage, IMCommon::jsParam(strNickName), strMsgId, QString::number(score), strBuddyId);
	m_vm->runJs(strSend);
}

QString IMGroupChatStore::GetFileMd5(QString fileNamePath)
{
	QFile theFile(fileNamePath);
	theFile.open(QIODevice::ReadOnly);
	QByteArray ba = QCryptographicHash::hash(theFile.readAll(), QCryptographicHash::Md5);
	theFile.close();
	return QString(ba.toHex());
}


const QString IMGroupChatStore::GetSmallImg(QString strPath)
{
	//后缀可能是非法后缀，安卓拍照时传的是.pic后缀名
	strPath.replace("%20", " ");
	QString strSmallPath = "";
	QImageReader reader;
	reader.setDecideFormatFromContent(true);
	reader.setAutoTransform(true);
	reader.setFileName(QString(strPath).replace("file:///", ""));
	if (!reader.canRead())
	{
		return strPath;
	}

	QString strType = strPath.right(strPath.length() - strPath.lastIndexOf('.')).toLower();

	if (strType == ".gif")
	{
		//gif动态图不做转换
		return strPath;
	}

	QImage img = reader.read();

	//图片超过300*300时或者有EXIF旋转信息时则生成缩略图并返回缩略图地址，其它情况返回原图地址
	if ((strType != ".jpg" && strType != ".jpeg" && strType != ".png" && strType != ".gif" && strType != ".bmp") || (img.width() > 300 || img.height() > 300) || reader.transformation() != QImageIOHandler::TransformationNone)
	{
		img = img.scaled(300, 300, Qt::AspectRatioMode::KeepAspectRatio, Qt::TransformationMode::SmoothTransformation);

		strSmallPath = QString("file:///") + QDir::tempPath() + "/" + GetFileMd5(QString(strPath).replace("file:///", "")) + ".small" + strType;
		QString strFixSmallPath = QString(strSmallPath).replace("file:///", "");

		qDebug() << "small pic ===============>>" << strFixSmallPath;

		if (!QFile(strFixSmallPath).exists())
		{
			img.save(strFixSmallPath);

			//若是生成不成功，后缀可能不是标准图片格式，再生成一次
			if (!QFile(strFixSmallPath).exists())
			{
				img.save(strFixSmallPath, "PNG");
			}
		}
	}
	else
	{
		return strPath;
	}


	return strSmallPath;
}

void IMGroupChatStore::OnInsertRecvPictureInfo(QString strIsUserDefine, QString strUserDefinePicPath, QString strPicPath, QString strHeadImage, QString strNickName, QString strMsgId, int score, QString strBuddyId)
{
	QString strPath = convertHeaderToHtmlSrc(strHeadImage);

	//传入备注名。
	if (!strPicPath.isEmpty())
	{
		strPicPath = GetSmallImg(strPicPath);
	}
	QString strSend = QString("GroupPictureAppend(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",\"%6\",%7,\"%8\")").arg(strPath).arg(strIsUserDefine).arg(strUserDefinePicPath).arg(strPicPath).arg(IMCommon::jsParam(strNickName)).arg(strMsgId).arg(score).arg(strBuddyId);
	m_vm->runJs(strSend);
}

int IMGroupChatStore::CalWavLength(QString strPath)
{
	int iSize = 0;
	QFile file(strPath);
	file.open(QIODevice::ReadOnly);
	QDataStream in(&file);
	in.setByteOrder(QDataStream::LittleEndian);
	unsigned int iRate = 0;
	in.skipRawData(12);
	char ch[5];
	in.readRawData(ch, 4);
	ch[4] = '\0';
	QString strType(ch);

	if (strType == "fmt ")
	{
		in.skipRawData(12);
		in >> iRate;
	}
	else if (strType == "JUNK")
	{
		unsigned int iJunk;
		unsigned short sNum;
		in >> iJunk;
		in.skipRawData(iJunk);
		in.readRawData(ch, 4);
		in >> iRate;
		in >> sNum;
		in >> sNum;
		in >> iRate;
		in >> iRate;
	}
	else
	{
		iRate = BYTES_PER_SECOND;
	}

	iSize = file.size() / iRate;
	return iSize;
}

void IMGroupChatStore::OnRecvAudioMessage(MessageInfo messageInfo, QString nickName, QString strHeadImage)
{
	QString msgID = QString(messageInfo.msgID);
	QString audioPath = messageInfo.strMsg;

	if (audioPath.endsWith(".amr"))
	{
		QString wavPath = audioPath;
		wavPath.replace(".amr", ".wav");

		QFile audioFile(wavPath);
		if (!audioFile.exists())
		{
#ifdef Q_OS_WIN
			AmrDec amrDec;
			amrDec.convertAmrToWav(audioPath, wavPath);
#else
			QString appPath = QDir::currentPath() + "/ffmpeg";
			QStringList arguments;
			arguments << "-i" << audioPath << wavPath;
			QProcess process(this);
			process.start(appPath, arguments);
			process.waitForFinished();
			process.close();
#endif
		}

		audioPath = wavPath;
	}

	QFile file(audioPath);
	int duration = CalWavLength(audioPath); //file.size() / BYTES_PER_SECOND;

	QString strPath = convertHeaderToHtmlSrc(strHeadImage);

	//传入备注名。
	QString strSend = QString("GroupAudioAppend(\"%1\",\"%2\", %3, \"%4\",\"%5\",%6, \"%7\")").arg(strPath).arg(audioPath).arg(duration).arg(IMCommon::jsParam(nickName)).arg(msgID).arg(messageInfo.integral).arg(QString::number(messageInfo.nFromUserID));
	m_vm->runJs(strSend);
}

void IMGroupChatStore::OnInsertRecVideoInfo(QString strIsLoading, QString strVideoPicPath, QString strVideoPath, QString strHeadImage, QString strNickName, QString strMsgId, int score, QString strBuddyId)
{
	QString strPath = convertHeaderToHtmlSrc(strHeadImage);

	//传入备注名。
	QString strSend = QString("GroupVdoAppend(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",\"%6\",%7,\"%8\")").arg(strPath).arg(strIsLoading).arg(strVideoPicPath).arg(strVideoPath).arg(IMCommon::jsParam(strNickName)).arg(strMsgId).arg(score).arg(strBuddyId);
	m_vm->runJs(strSend);
}

void IMGroupChatStore::OnRecvGroupFile(MessageInfo messageInfo, QString strBuddyNickName)
{
	QString strFromID = QString("%1").arg(messageInfo.nToUserID);
	QString strBuddyID = QString("%1").arg(messageInfo.nFromUserID);
	QString strBuddyHeadImage = GetBuddyHeaderImage(strFromID, strBuddyID);

	QJsonParseError jsonError;
	QString strFileName;
	QString FileName;
	QString strFileLocalPath;
	QString fileSize;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(messageInfo.strMsg.toUtf8(), &jsonError);
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QVariantMap result = jsonDocument.toVariant().toMap();
			strFileName = result["FileName"].toString();
			strFileLocalPath = result["FileLocalPath"].toString();
			fileSize = result["FileSize"].toString();
		}
	}

	FileTypeEx* fileType = new FileTypeEx(this);
	QString strPath = fileType->GetFilePic(strFileName);
	delete fileType;
	QString OtherHeadImg = convertHeaderToHtmlSrc(strBuddyHeadImage);

	QString strUlID = QString(messageInfo.msgID) + QString("recv");
	QString strUlID2 = QString(messageInfo.msgID) + QString("recv2");
	QString strSize = QString("(") + fileSize + QString(")");
	//传入备注名。
	QString strRecFile = QString("RecGroupFile(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",\"%6\",\"%7\",\"%8\",%9,\"%10\")").arg(OtherHeadImg).arg(IMCommon::jsParam(strBuddyNickName)).arg(strFileName).arg(strPath).arg(QString(messageInfo.msgID)).arg(strUlID).arg(strUlID2).arg(strSize).arg(messageInfo.integral).arg(QString::number(messageInfo.nFromUserID));
	m_vm->runJs(strRecFile);
}

void IMGroupChatStore::onRecvRedBagMessage(MessageInfo messageInfo, QString nickName, QString strHeadImage)
{
	QJsonDocument json = QJsonDocument::fromJson(messageInfo.strMsg.toUtf8());
	QVariantMap map = json.toVariant().toMap();
	QString invalid = map.value("invalid").toString();

	QString headerPath = convertHeaderToHtmlSrc(strHeadImage);
	QString remarks;
	if (invalid.isEmpty())
	{
		remarks = map.value("leaveMessage").toString();
	}
	else
	{
		QVariantMap packet = map.value("redPacket").toMap();
		remarks = packet.value("remarks").toString();
	}

	if (remarks.isEmpty())
		remarks = tr("Best Wishes");

	QString strSend = QString("GroupRedPacketAppend('%1', '%2', '%3', '%4', '%5', %6, '%7');")
		.arg(headerPath).arg(IMCommon::jsParam(nickName)).arg(remarks).arg(invalid).arg(QString(messageInfo.msgID)).arg(messageInfo.integral).arg(QString::number(messageInfo.nFromUserID));
	m_vm->runJs(strSend);
}

void IMGroupChatStore::OnRecvNoticeMessage(MessageInfo msgInfo, QString nickName, QString headerImage)
{
	QString strPath = convertHeaderToHtmlSrc(headerImage);
	QString msg = msgInfo.strMsg;
	QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();
	QString imageUrl = map.value("imageUrl").toString();
	QString imageTitle = map.value("imageTitle").toString();
	QString webUrl = map.value("webUrl").toString();
	QString webTitle = map.value("webTitle").toString();

	strPath.replace(" ", "%20");
	//传入备注名。
	QString strSend = QString("GroupNoticeAppend(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",\"%6\",\"%7\", %8,\"%9\");")
		.arg(strPath).arg(IMCommon::jsParam(nickName))
		.arg("").arg(imageTitle).arg(webUrl).arg(webTitle)
		.arg(QString(msgInfo.msgID)).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));

	m_vm->runJs(strSend);
	/////////////////图片方向
	QString strFilePath = QDir::tempPath() + "/" + QUuid::createUuid().toString() + ".jpg";
	HttpNetWork::HttpDownLoadFile *netWork = new HttpNetWork::HttpDownLoadFile;
	netWork->setData(QVariant::fromValue(msgInfo));
	netWork->setObjectName(strFilePath);
	connect(netWork, SIGNAL(sigDownFinished(bool)), this, SLOT(slotUpdateNotice(bool)));
	netWork->StartDownLoadFile(imageUrl, strFilePath);
	/////////////////
}

void IMGroupChatStore::slotUpdateNotice(bool)
{
	HttpNetWork::HttpDownLoadFile *act = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());
	if (!act)
	{
		return;
	}
	QVariant var = act->getData();
	MessageInfo msgInfo = var.value<MessageInfo>();
	QString filePath = GetSmallImg(act->objectName());

	QString strSend = QString("UpdateNoticeImg(\"%1\",\"%2\");")
		.arg(QString(msgInfo.msgID)).arg(filePath);

	m_vm->runJs(strSend);
}

void IMGroupChatStore::onRecvShareUrlMessage(MessageInfo msgInfo, QString nickName, QString headerImage)
{
	QString strPath = convertHeaderToHtmlSrc(headerImage);

	QString msg = msgInfo.strMsg;
	QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();
	QString subject = map.value("subject").toString();
	QString text = map.value("text").toString();
	QString url = map.value("url").toString();
	QString imageUrl = map.value("imgUrl").toString();

	QString strSend = QString("GroupShareUrlAppend(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",\"%6\",\"%7\", %8,\"%9\");")
		.arg(subject).arg(text).arg(url).arg(imageUrl)
		.arg(IMCommon::jsParam(nickName)).arg(strPath).arg(QString(msgInfo.msgID))
		.arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));

	m_vm->runJs(strSend);
}

void IMGroupChatStore::onRecvLocationMessage(MessageInfo msgInfo, QString nickName, QString headerImage)
{
	headerImage = "file:///" + headerImage;

	msgInfo.strMsg.remove("\n");
	msgInfo.strMsg.remove(QChar(32));
	msgInfo.strMsg = msgInfo.strMsg.replace("\"", "\\\"");

	headerImage.replace(" ", "%20");
	//传入备注名。
	QString strSend = QString("GroupLocationAppend(\"%1\",\"%2\",\"%3\",\"%4\",%5,\"%6\");")
		.arg(headerImage).arg(QString(msgInfo.msgID)).arg(IMCommon::jsParam(nickName))
		.arg(msgInfo.strMsg)
		.arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));

	m_vm->runJs(strSend);
}

void IMGroupChatStore::OnRecvNotifyMessage(MessageInfo msgInfo)
{
	QJsonDocument json = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = json.toVariant().toMap();

	QString content;
	if (map.value("type").toString() == "notification")
	{
		content = map.value("content").toString();
	}
	if (map.value("type").toString() == "receivedFile")
	{
		content = tr("The other party has successfully received your file");
	}

	QString strSend = QString("tipMessage(\"%1\");").arg(content);
	m_vm->runJs(strSend);
}

void IMGroupChatStore::OnRecvCommonMessage(MessageInfo messageInfo, QString nickName, QString strHeadImage)
{
	QJsonDocument doc = QJsonDocument::fromJson(messageInfo.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();

	QString image = map.value("image").toString();
	QString smallIcon = map.value("smallIcon").toString();
	QString title = map.value("title").toString();
	QString content = map.value("content").toString();
	QString systemName = map.value("systemName").toString();
	QString userType = map.value("userType").toString();

	if (image.isEmpty())
	{
		if (userType == "user")
			image = "https://tc.ipcom.io/panserver/files/c64b5061-77dc-45af-b91b-e5e06d401347/download";
		if (userType == "group")
			image = "https://tc.ipcom.io/panserver/files/0bb98df6-cddd-444e-a36c-c50beac4dcd1/download";
	}

	QString headerPath = convertHeaderToHtmlSrc(strHeadImage);
	QString strSend = QString("GroupCommonAppend('%1', '%2', '%3', '%4', '%5','%6', '%7', '%8', %9, '%10');")
		.arg(image).arg(smallIcon).arg(title).arg(content).arg(systemName)
		.arg(IMCommon::jsParam(nickName)).arg(headerPath).arg(QString(messageInfo.msgID)).arg(messageInfo.integral).arg(QString::number(messageInfo.nFromUserID));
	m_vm->runJs(strSend);
}

void IMGroupChatStore::OnProcessRecvImgMessage(MessageInfo messageInfo, QString strBuddyNickName, QString strBuddyHeadImage)
{
	if (messageInfo.strMsg == "load")
	{
		QString strTemp = "qrc:/html/Resources/html/load.gif";
		QString strIsUserDefine = "true";
		QString strUserDefinePicPath = strTemp;
		QString strPicPath = "";

		OnInsertRecvPictureInfo(strIsUserDefine, strUserDefinePicPath, strPicPath, strBuddyHeadImage, strBuddyNickName, QString(messageInfo.msgID), messageInfo.integral, QString::number(messageInfo.nFromUserID));
	}
	else if (messageInfo.strMsg != "fail" && messageInfo.MessageState != MESSAGE_STATE_UNLOADED)
	{

		messageInfo.strMsg.replace(" ", "%20");

		if (!QFile::exists(messageInfo.strMsg))
		{
#ifdef Q_OS_WIN
			messageInfo.strMsg = gSettingsManager->getUserPath() + messageInfo.strMsg;
#else
			messageInfo.strMsg = messageInfo.strMsg.replace("%20", " ");
#endif
		}

		QString strIsUserDefine = "false";
		QString strUserDefinePicPath = "";
		QString strPicPath = QString("file:///") + messageInfo.strMsg;

		OnInsertRecvPictureInfo(strIsUserDefine, strUserDefinePicPath, strPicPath, strBuddyHeadImage, strBuddyNickName, QString(messageInfo.msgID), messageInfo.integral, QString::number(messageInfo.nFromUserID));
		m_listPic.push_back(messageInfo.strMsg);
	}
	else
	{
		QString strTemp = "qrc:/html/Resources/html/picfail.png";
		QString strIsUserDefine = "true";
		QString strUserDefinePicPath = strTemp;
		QString strPicPath = "";
		OnInsertRecvPictureInfo(strIsUserDefine, strUserDefinePicPath, strPicPath, strBuddyHeadImage, strBuddyNickName, QString(messageInfo.msgID), messageInfo.integral, QString::number(messageInfo.nFromUserID));

	}
}

void IMGroupChatStore::OnProcessRecvAudioMessage(MessageInfo messageInfo, QString strBuddyNickName, QString strBuddyHeadImage)
{
	if (messageInfo.strMsg == "fail" || messageInfo.MessageState == MESSAGE_STATE_UNLOADED)
	{
		QString strTemp = "qrc:/html/Resources/html/audiofail.png";
		QString strIsUserDefine = "true";
		QString strUserDefinePicPath = strTemp;
		QString strPicPath = "";

		OnInsertRecvPictureInfo(strIsUserDefine, strUserDefinePicPath, strPicPath, strBuddyHeadImage, strBuddyNickName, QString(messageInfo.msgID), messageInfo.integral, QString::number(messageInfo.nFromUserID));

	}
	else
	{
		OnRecvAudioMessage(messageInfo, strBuddyNickName, strBuddyHeadImage);
	}

}

void IMGroupChatStore::OnProcessRecvVideoMessage(MessageInfo messageInfo, QString strBuddyNickName, QString strBuddyHeadImage)
{
	if (messageInfo.strMsg == "load")
	{
		QString strTemp = "qrc:/html/Resources/html/load.gif";
		QString strIsUserDefine = "true";
		QString strUserDefinePicPath = strTemp;
		QString strPicPath = "";
		OnInsertRecvPictureInfo(strIsUserDefine, strUserDefinePicPath, strPicPath, strBuddyHeadImage, strBuddyNickName, QString(messageInfo.msgID), messageInfo.integral, QString::number(messageInfo.nFromUserID));
	}
	else if (messageInfo.strMsg != "fail" && messageInfo.MessageState != MESSAGE_STATE_UNLOADED) {
#ifdef Q_OS_WIN
		messageInfo.strMsg.replace(" ", "%20");
#endif
		QString strPicPath = messageInfo.strMsg.left(messageInfo.strMsg.indexOf("."));
		strPicPath += ".png";
		VedioFrameOpera pVdo;
		pVdo.CreateVedioPicture(messageInfo.strMsg, strPicPath);
		QString strPhotoImgPath = QString("<img height:auto width:auto src='file:///%1' name = '%2' onload='movePlayIcon(this);loadpic()'/> ").arg(strPicPath).arg(messageInfo.strMsg);


		QString strIsLoading = "false";//是否是正加载
		QString strVideoPicPath = "file:///" + strPicPath;//视频第一帧图
		QString strVideoPath = messageInfo.strMsg;//视频路径

		OnInsertRecVideoInfo(strIsLoading, strVideoPicPath, strVideoPath, strBuddyHeadImage, strBuddyNickName, QString(messageInfo.msgID), messageInfo.integral, QString::number(messageInfo.nFromUserID));
	}
	else
	{
		QString strTemp = "qrc:/html/Resources/html/videofail.png";

		QString strIsUserDefine = "true";
		QString strUserDefinePicPath = strTemp;
		QString strPicPath = "";
		OnInsertRecvPictureInfo(strIsUserDefine, strUserDefinePicPath, strPicPath, strBuddyHeadImage, strBuddyNickName, QString(messageInfo.msgID), messageInfo.integral, QString::number(messageInfo.nFromUserID));

	}
}

void IMGroupChatStore::OnProcessRecvAtMessage(MessageInfo messageInfo, QString strBuddyNickName, QString strBuddyHeadImage)
{
	QJsonDocument doc = QJsonDocument::fromJson(messageInfo.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();
	QString content = map.value("content").toString();
	OnInsertRecvMessageTextInfo(content, strBuddyHeadImage, strBuddyNickName, QString(messageInfo.msgID), messageInfo.integral, QString::number(messageInfo.nFromUserID));
}

void IMGroupChatStore::slotProcessRecvMessageInfo(MessageInfo messageInfo)
{
	QString strFromID = QString("%1").arg(messageInfo.nToUserID);
	QString strBuddyID = QString("%1").arg(messageInfo.nFromUserID);
	QString strBuddyHeadImage = GetBuddyHeaderImage(strFromID, strBuddyID);

	QList<BuddyInfo> buddyInfo = gDataBaseOpera->DBGetGroupBuddyInfoFromID(strFromID);
	QString strBuddyNickName;
	for (int j = 0; j < buddyInfo.count(); j++)
	{
		if (buddyInfo.at(j).nUserId == strBuddyID.toInt())
		{

			if (buddyInfo.at(j).strNote.isEmpty())
				strBuddyNickName = buddyInfo.at(j).strNickName;
			else
				strBuddyNickName = buddyInfo.at(j).strNote;
		}
	}

	if (strBuddyNickName.isEmpty())
		strBuddyNickName = strBuddyID;

	//插入时间线。
	onInsertTimeLine(GETLOCALTIME);

	switch (messageInfo.MessageChildType)
	{
	case MessageType::Message_TEXT: // 文字消息
		OnInsertRecvMessageTextInfo(messageInfo.strMsg, strBuddyHeadImage, strBuddyNickName, QString(messageInfo.msgID), messageInfo.integral, QString::number(messageInfo.nFromUserID));
		break;
	case MessageType::Message_PIC: // 图片消息
		OnProcessRecvImgMessage(messageInfo, strBuddyNickName, strBuddyHeadImage);
		break;
	case MessageType::Message_AUDIO:  //音频消息
		OnProcessRecvAudioMessage(messageInfo, strBuddyNickName, strBuddyHeadImage);
		break;
	case MessageType::Message_VEDIO: // 视频消息
		OnProcessRecvVideoMessage(messageInfo, strBuddyNickName, strBuddyHeadImage);
		break;
	case MessageType::Message_FILE: //文件消息
		OnRecvGroupFile(messageInfo, strBuddyNickName);
		break;
	case MessageType::Message_REDBAG:
		onRecvRedBagMessage(messageInfo, strBuddyNickName, strBuddyHeadImage);
		break;
	case MessageType::Message_NOTICE://通告消息
		OnRecvNoticeMessage(messageInfo, strBuddyNickName, strBuddyHeadImage);
		break;
	case MessageType::Message_URL: //分享消息
		onRecvShareUrlMessage(messageInfo, strBuddyNickName, strBuddyHeadImage);
		break;
	case MessageType::Message_LOCATION: //位置消息
		onRecvLocationMessage(messageInfo, strBuddyNickName, strBuddyHeadImage);
		break;
	case MessageType::Message_NOTIFY:
		OnRecvNotifyMessage(messageInfo);
		break;
	case MessageType::Message_COMMON:
		OnRecvCommonMessage(messageInfo, strBuddyNickName, strBuddyHeadImage);
		break;
	case MessageType::Message_AT:
		OnProcessRecvAtMessage(messageInfo, strBuddyNickName, strBuddyHeadImage);
		break;
	default:
		break;
	}
}

void IMGroupChatStore::slotUserQuitGroup(QString strUserId)
{
	QString numText = QString::number(m_vm->getGroupMemberCount());

	QList<BuddyInfo> groupInfo = gDataBaseOpera->DBGetGroupBuddyInfoFromID(m_vm->getGroupId());//从数据库中加载部落成员信息
	m_vm->updateGroupMemberCount(groupInfo.size());     // 更新部落成员计数到Label上
	m_vm->removeMemberByUserId(strUserId);
}

void IMGroupChatStore::OnInsertGroupUserItem(QString strUserID, QString strPicPath, QString strNickName, int mana, bool bHasNote)
{
	//判断strPicPath是否有效的图片，无效的话用默认图片
	if (!IsValidImage(strPicPath))
	{
		strPicPath = QStringLiteral(":/PerChat/Resources/person/temp.png");
	}


	QString adminPicPath = "";

	if (mana == 0)
	{
		adminPicPath = "";
	}
	else if (mana == 1)
	{
		adminPicPath = QStringLiteral("image://HeadProvider/") + QStringLiteral(":/GroupChat/Resources/groupchat/manager.png");//管理员
	}
	else if (mana == 9)
	{
		adminPicPath = QStringLiteral("image://HeadProvider/") + QStringLiteral(":/GroupChat/Resources/groupchat/chairman.png");//群主
	}

	QString headPicPath = QStringLiteral("image://HeadProvider/") + strPicPath;

	m_vm->insertItem(strUserID, headPicPath, strNickName, adminPicPath, bHasNote);
}

void IMGroupChatStore::slotUpdateGroupBuddyImagePath(int userID, QString imagePath)
{
	m_vm->updateGroupBuddyImagePath(userID, imagePath);
}

void IMGroupChatStore::slotThreadLoadGroupUserInfo(BuddyInfo buddyInfo)
{
	QString strUserID = QString("%1").arg(buddyInfo.nUserId);
	if (buddyInfo.strNote.isEmpty())
	{
		QString strName = buddyInfo.strNickName;
		BuddyInfo eTmpInfo = gDataBaseOpera->DBGetBuddyInfoByID(QString::number(buddyInfo.nUserId));
		if (!eTmpInfo.strNote.isEmpty())
		{
			strName = eTmpInfo.strNote;
		}
		OnInsertGroupUserItem(strUserID, buddyInfo.strLocalAvatar,
			strName, buddyInfo.nUserType, false);
	}
	else
	{
		OnInsertGroupUserItem(strUserID, buddyInfo.strLocalAvatar,
			buddyInfo.strNote, buddyInfo.nUserType, true);
	}

	QFileInfo info(buddyInfo.strLocalAvatar);
	if (info.size() == 0)  //文件不存在，或者文件内容为空。
	{
		IMDownLoadHeaderImg *down = new IMDownLoadHeaderImg;
		connect(down, SIGNAL(sigUpdateBuddyHeaderImagePath(int, QString)), this, SLOT(slotUpdateGroupBuddyImagePath(int, QString)));
		down->StartDownLoadBuddyeHeaderImage(buddyInfo);
	}
}

void IMGroupChatStore::slotInsertGroupUser(BuddyInfo info)
{
	QString numText = QString::number(m_vm->getGroupMemberCount());
	QList<BuddyInfo> groupInfo = gDataBaseOpera->DBGetGroupBuddyInfoFromID(m_vm->getGroupId());//从数据库中加载部落成员信息
	m_vm->updateGroupMemberCount(groupInfo.size());     // 更新部落成员计数到Label上
	slotThreadLoadGroupUserInfo(info);
}

void IMGroupChatStore::slotSetNoSpeak(int noSpeak)
{
	m_vm->setNoSpeak(noSpeak == 1);
}

void IMGroupChatStore::slotInsertGroupUserList()
{
	m_vm->insertGroupUserList();
}

void IMGroupChatStore::OnClearMessageInfo()
{
	m_vm->runJs("clear()");
}

void IMGroupChatStore::ChangeFileState(QString strMsgId, int iState)
{
	QString strScript = QString("ChangeFileState(\"%1\",\"%2\")").arg(strMsgId).arg(iState);
	m_vm->runJs(strScript);
}

//用于接收文件
void IMGroupChatStore::slotGetFile(QString msgID)
{
	MessageInfo messageInfo = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);
	QJsonDocument jsonDocument = QJsonDocument::fromJson(messageInfo.strMsg.toUtf8());
	QVariantMap result = jsonDocument.toVariant().toMap();
	QString fileName = result["FileName"].toString();
	UserInfo userInfo = gDataManager->getUserInfo();
	QDir dir;
#ifdef Q_OS_WIN
	QString currentPath = gSettingsManager->getUserPath();
#else
	QString currentPath = gSettingsManager->getUserPath();
#endif
	QString strFilePath = currentPath + QString("/files/") + QString("%1").arg(userInfo.nUserID) + QString("/") + fileName;

	QFile recvFile(strFilePath);
	if (recvFile.exists())
	{
		QuestionBox *box = new QuestionBox(NULL);
		connect(box, SIGNAL(sigEnter()), this, SLOT(slotCoverFile()));
		box->setData(QVariant::fromValue(messageInfo));
		box->init(tr("Notice"), tr("This file already exists, overwrite or not?"));
		box->show();
	}
	else
	{
		QString fileID = result["FileId"].toString();

		QString strUlID = QString(messageInfo.msgID) + QString("recv");
		QString coverFile = QString("coverFile(\"%1\")").arg(strUlID);
		QString strUlID2 = QString(messageInfo.msgID) + QString("recv2");

		ChangeFileState(messageInfo.msgID, 1);

		AppConfig configInfo = gDataManager->getAppConfigInfo();
		QString httpUrl = QString(configInfo.PanServerDownloadURL) + fileID + QString("/download");
		HttpNetWork::HttpDownLoadFile *netWork = new HttpNetWork::HttpDownLoadFile;
		m_iHttp++;
		netWork->setData(QVariant::fromValue(messageInfo));
		netWork->setObjectName(strFilePath);
		connect(netWork, SIGNAL(sigDownFinished(bool)), this, SLOT(slotRequestHttpFileResult(bool)));
		connect(netWork, SIGNAL(sigDownloadProgress(qint64, qint64)), this, SLOT(slotDownLoadFileProgress(qint64, qint64)));

		connect(m_dispatcher, SIGNAL(sigCancel(QString)), netWork, SLOT(slotLoadorDownLoadCancle(QString)));
		connect(netWork, SIGNAL(sigDownFailed()), this, SLOT(slotDownFailed()));
		netWork->StartDownLoadFile(httpUrl, strFilePath);
	}
}

void IMGroupChatStore::slotRequestHttpFileResult(bool result)
{
	m_iHttp--;
	HttpNetWork::HttpDownLoadFile *act = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());
	if (act)
	{
		QVariant var = act->getData();
		MessageInfo msgInfo = var.value<MessageInfo>();
		QString filePath = act->objectName();
		if (result)
		{
			gDataBaseOpera->DBOnInsertFileInfo(QString(msgInfo.msgID), filePath, "");//将接收到的文件信息插入数据库

			QString strUlID = msgInfo.msgID + QString("recv");
			QString strChange = QString("ChangeLiTwo(\"%1\")").arg(strUlID);


			QString strUlID2 = msgInfo.msgID + QString("recv2");
			QString strChange2 = QString("ChangeLiTwo(\"%1\")").arg(strUlID2);
	
			ChangeFileState(msgInfo.msgID, 2);
		}
		else {
			QString strUlID = msgInfo.msgID + QString("recv");
			QString strChange = QString("ChangeLiThr(\"%1\")").arg(strUlID);
	

			QString strUlID2 = msgInfo.msgID + QString("recv2");
			QString strChange2 = QString("ChangeLiThr(\"%1\")").arg(strUlID2);

			ChangeFileState(msgInfo.msgID, 0);
		}

	}
}

void IMGroupChatStore::slotCoverFile()
{
	QuestionBox *box = qobject_cast<QuestionBox *>(sender());
	QVariant msgVar = box->getData();
	MessageInfo messageInfo = msgVar.value<MessageInfo>();
	QJsonDocument jsonDocument = QJsonDocument::fromJson(messageInfo.strMsg.toUtf8());
	QVariantMap result = jsonDocument.toVariant().toMap();

	QString fileID = result["FileId"].toString();

	QString strUlID2 = QString(messageInfo.msgID) + QString("recv2");
	QString strUlID = QString(messageInfo.msgID) + QString("recv");
	QString coverFile = QString("coverFile(\"%1\")").arg(strUlID);

	ChangeFileState(messageInfo.msgID, 1);
	AppConfig configInfo = gDataManager->getAppConfigInfo();
	QString httpUrl = QString(configInfo.PanServerDownloadURL) + fileID + QString("/download");

	QString fileName = result["FileName"].toString();
	UserInfo userInfo = gDataManager->getUserInfo();
	QDir dir;
#ifdef Q_OS_WIN
	QString currentPath = gSettingsManager->getUserPath();
#else
	QString currentPath = gSettingsManager->getUserPath();
#endif
	QString strFilePath = currentPath + QString("/files/") + QString("%1").arg(userInfo.nUserID) + QString("/") + fileName;
	QFile recvFile(strFilePath);
	recvFile.remove();

	HttpNetWork::HttpDownLoadFile *netWork = new HttpNetWork::HttpDownLoadFile;
	m_iHttp++;
	netWork->setData(QVariant::fromValue(messageInfo));
	netWork->setObjectName(strFilePath);
	connect(netWork, SIGNAL(sigDownFinished(bool)), this, SLOT(slotRequestHttpFileResult(bool)));
	connect(netWork, SIGNAL(sigDownloadProgress(qint64, qint64)), this, SLOT(slotDownLoadFileProgress(qint64, qint64)));
	connect(m_dispatcher, SIGNAL(sigCancel(QString)), netWork, SLOT(slotLoadorDownLoadCancle(QString)));
	connect(netWork, SIGNAL(sigDownFailed()), this, SLOT(slotDownFailed()));
	netWork->StartDownLoadFile(httpUrl, strFilePath);
}

void IMGroupChatStore::InsertSendFileMessage(MessageInfo msgInfo)
{
	//插入时间线。
	onInsertTimeLine(msgInfo.ServerTime);

	UserInfo userinfo = gDataManager->getUserInfo();
	QString strHeadPath = convertHeaderToHtmlSrc(userinfo.strUserAvatarLocal);

	//解析messageinfo，解析出文件名，路径
	QJsonParseError jsonError;
	QString strFileName;
	QString FileName;
	QString strFileLocalPath;
	QString strurl;
	QString strJsonSize;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8(), &jsonError);
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QVariantMap result = jsonDocument.toVariant().toMap();
			strFileName = result["FileName"].toString();
			strFileLocalPath = result["FileLocalPath"].toString();
			strurl = result["url"].toString();
			strJsonSize = result["FileSize"].toString();
		}
	}
	else//本地发送失败时非json格式
	{
		strFileLocalPath = msgInfo.strMsg;
		int ipos = strFileLocalPath.lastIndexOf("/");
		if (ipos > -1)
		{
			strFileName = strFileLocalPath.right(strFileLocalPath.length() - ipos - 1);
		}
	}

	FileTypeEx* fileType = new FileTypeEx(this);
	QString strPath = fileType->GetFilePic(strFileName);

	QString newUlID = QString(msgInfo.msgID) + QString("send");
	QString strSize = QString("(") + gSocketMessage->ByteConversion(fileType->GetFileSize(strFileLocalPath)) + QString(")");
	delete fileType;

	//根据msg中是否有url判断是本地还是其他设备 若为其他设备则检测resource下是否存在
	if (strurl.isEmpty())
	{
		QString strend = QString("SendGroupFile(\"%1\",\"%2\",\"%3\",%4,\"%5\",\"%6\",\"%7\",\"%8\")").arg(strHeadPath).arg(strFileName).arg(QString(msgInfo.msgID)).arg(msgInfo.MessageState).arg(strPath).arg(newUlID).arg(strSize).arg(QString::number(msgInfo.nFromUserID));
		m_vm->runJs(strend);
		if (msgInfo.MessageState == 3 || msgInfo.MessageState == 4 || msgInfo.MessageState == 5 || msgInfo.MessageState == 6 || msgInfo.MessageState == 7)
		{
			//正在发送--->已成功发送
			QString newLiID = QString(msgInfo.msgID) + QString("send");
			QString changeLi = QString("ChangeLi(\"%1\")").arg(newLiID);

			//取消标签消失
			QString cancleID = QString(msgInfo.msgID) + QString("can");
			QString changeLi2 = QString("ChangeLi(\"%1\")").arg(cancleID);

			ChangeFileState(msgInfo.msgID, 2);
		}
		else if (msgInfo.MessageState == MESSAGE_STATE_FAILURE || msgInfo.MessageState == MESSAGE_STATE_WAITSEND)
		{
			QString newLiID = QString(msgInfo.msgID) + QString("send");
			QString changeLi = QString("ChangeLiCanel(\"%1\")").arg(newLiID);

			QString canID = QString(msgInfo.msgID) + QString("can");
			QString Cancle = QString("ChangeLiCanel(\"%1\")").arg(canID);

			ChangeFileState(msgInfo.msgID, 1);
		}
	}
	else
	{
		if (strSize == "(0B)")
		{
			strSize = "(" + strJsonSize + ")";
		}
		QString newLiID = QString(msgInfo.msgID) + QString("recv");
		QString newLiID2 = QString(msgInfo.msgID) + QString("recv2");
		QString strend = QString("SendGroupOtherFile(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",\"%6\",\"%7\",%8,%9,\"%10\")").arg(strHeadPath).arg(strFileName).arg(strPath).arg(QString(msgInfo.msgID)).arg(newLiID).arg(newLiID2).arg(strSize).arg(msgInfo.MessageState).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));
		m_vm->runJs(strend);
		UserInfo userInfo = gDataManager->getUserInfo();
		QString currentPath = gSettingsManager->getUserPath();
		QString strFilePath = gDataBaseOpera->DBGetFileInfoLocalPath(msgInfo.msgID);

		if (QFile::exists(strFilePath))
		{
			QString strUlID = QString(msgInfo.msgID) + QString("recv");
			QString changeLi = QString("ChangeLiTwo(\"%1\")").arg(strUlID);

			QString strUlID2 = QString(msgInfo.msgID) + QString("recv2");
			QString changeLi2 = QString("ChangeLiTwo(\"%1\")").arg(strUlID2);

			QString strUlID3 = QString(msgInfo.msgID) + QString("recv2");
			QString changeLi3 = QString("ChangeLiTwo(\"%1\")").arg(strUlID3);
			ChangeFileState(msgInfo.msgID, 2);
		}
		else//小于10M的直接下载
		{
			QString strNumber;
			QString strUnit;
			for (int i = 0; i < strJsonSize.length(); i++)
			{
				if (strJsonSize[i] >= '0' && strJsonSize[i] <= '9' || strJsonSize[i] == '.')
				{
					strNumber.append(strJsonSize[i]);
				}
				else
				{
					strUnit.append(strJsonSize[i]);
				}
			}
			if (strUnit == "B" || strUnit == "KB" || (strUnit == "MB"&&strNumber.toDouble() < 10))
			{
				QJsonDocument jsonDocument = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
				QVariantMap result = jsonDocument.toVariant().toMap();
				QString fileName = result["FileName"].toString();
				UserInfo userInfo = gDataManager->getUserInfo();
				QDir dir;
				QString currentPath = gSettingsManager->getUserPath();
				QString strFilePath = currentPath + QString("/files/") + QString("%1").arg(userInfo.nUserID) + QString("/") + fileName;
				QFile recvFile(strFilePath);
				if (!recvFile.exists())
				{
					slotGetFile(msgInfo.msgID);
				}
			}
		}
	}
}

//发送消息添加到webview
void IMGroupChatStore::OnInertSendMessageTextInfo(QString strMsg, QString strHeadImage, MessageInfo msgInfo)
{
	QList<int> types;
	types << MessageType::Message_TEXT << MessageType::Message_PIC << MessageType::Message_FILE << MessageType::Message_AT;
	types << MessageType::Message_NOTICE << MessageType::Message_URL << MessageType::Message_LOCATION;
	types << MessageType::Message_AUDIO << MessageType::Message_VEDIO;
	types << MessageType::Message_NOTIFY << MessageType::Message_COMMON;
	if (types.contains((int)msgInfo.MessageChildType))
	{
		//插入时间线。
		if (msgInfo.ServerTime)
			onInsertTimeLine(msgInfo.ServerTime);
		else
			onInsertTimeLine(msgInfo.ClientTime);
	}

	QString strPath = convertHeaderToHtmlSrc(strHeadImage);

	int msgState = msgInfo.MessageState;
	if (msgState > MESSAGE_STATE_SEND)
	{

	}
	if (msgInfo.MessageChildType == MessageType::Message_TEXT)
	{
		QString strMessage = formatMessageFromImgDescriptionWithHtmlEncode(strMsg);

		strMessage = strMessage.toUtf8().toPercentEncoding();
		QString strSend = QString("SendGroupAppend(\"%1\",\"%2\",\"%3\",%4,\"%5\");").arg(strPath, strMessage, QString(msgInfo.msgID), QString::number(msgState), QString::number(msgInfo.nFromUserID));
		m_vm->runJs(strSend);
	}
	if (msgInfo.MessageChildType == MessageType::Message_PIC)
	{
		QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		QString path = map.value("path").toString();
		if (!path.isEmpty())
			msgInfo.strMsg = path;

		if (!QFile::exists(msgInfo.strMsg))
		{
			msgInfo.strMsg = gSettingsManager->getUserPath() + msgInfo.strMsg;
		}

		QFile image(msgInfo.strMsg);
		if (!image.exists())
			return;

		QString filePathEscape = msgInfo.strMsg;
		filePathEscape.replace(" ", "%20");
		filePathEscape.replace("#", "%23");

		strPath.replace(" ", "%20");

		QString strIsUserDefine = "false";
		QString strUserDefinePicPath = "";
		QString strPicPath = QString("file:///") + filePathEscape;

		if (!strPicPath.isEmpty())
		{
			strPicPath = GetSmallImg(strPicPath);
		}
		QString strSend = QString("SendGroupPicture(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",%6,\"%7\");").arg(strPath).arg(strIsUserDefine).arg(strUserDefinePicPath).arg(strPicPath).arg(QString(msgInfo.msgID)).arg(msgState).arg(QString::number(msgInfo.nFromUserID));
		m_vm->runJs(strSend);

		m_listPic.push_back(msgInfo.strMsg);
	}
	if (msgInfo.MessageChildType == MessageType::Message_FILE)
	{
		msgInfo.MessageState = msgState;
		InsertSendFileMessage(msgInfo);
	}
	if (msgInfo.MessageChildType == MessageType::Message_REDBAG)
	{
		QJsonDocument json = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
		QVariantMap map = json.toVariant().toMap();
		QString invalid = map.value("invalid").toString();
		QString remarks;
		if (invalid == "true")    //已经领取/过期等等。
		{
			QVariantMap packet = map.value("redPacket").toMap();
			remarks = packet.value("remarks").toString();
		}
		else                      //还未领取。
		{
			remarks = map.value("leaveMessage").toString();
		}

		if (remarks.isEmpty())
			remarks = tr("Best Wishes");

		QString strSend = QString("GroupRedPacketSend('%1', '%2', '%3', '%4', %5, %6, '%7');")
			.arg(strPath).arg(remarks).arg(invalid).arg(QString(msgInfo.msgID)).arg(msgInfo.MessageState).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));
		m_vm->runJs(strSend);
	}
	if (msgInfo.MessageChildType == MessageType::Message_NOTICE)
	{
		strPath.replace(" ", "%20");

		QString msg = msgInfo.strMsg;
		QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		QString imageUrl = map.value("imageUrl").toString();
		QString imageTitle = map.value("imageTitle").toString();
		QString webUrl = map.value("webUrl").toString();
		QString webTitle = map.value("webTitle").toString();

		QString strSend = QString("SendGroupNotice(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",\"%6\",%7,%8,\"%9\");")
			.arg(strPath).arg("").arg(imageTitle).arg(webUrl).arg(webTitle).arg(QString(msgInfo.msgID))
			.arg(msgState).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));

		m_vm->runJs(strSend);
		/////////////////图片方向
		QString strFilePath = QDir::tempPath() + "/" + QUuid::createUuid().toString() + ".jpg";
		HttpNetWork::HttpDownLoadFile *netWork = new HttpNetWork::HttpDownLoadFile;
		netWork->setData(QVariant::fromValue(msgInfo));
		netWork->setObjectName(strFilePath);
		connect(netWork, SIGNAL(sigDownFinished(bool)), this, SLOT(slotUpdateNotice(bool)));
		netWork->StartDownLoadFile(imageUrl, strFilePath);
		/////////////////
	}
	if (msgInfo.MessageChildType == MessageType::Message_URL)
	{
		QString msg = msgInfo.strMsg;
		QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		QString subject = map.value("subject").toString();
		QString text = map.value("text").toString();
		QString url = map.value("url").toString();
		QString imageUrl = map.value("imgUrl").toString();

		QString strSend = QString("SendGroupShareUrl(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",\"%6\",%7,%8,\"%9\");")
			.arg(subject).arg(text).arg(url).arg(imageUrl)
			.arg(strPath).arg(QString(msgInfo.msgID))
			.arg(msgInfo.MessageState).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));

		m_vm->runJs(strSend);
	}
	if (msgInfo.MessageChildType == MessageType::Message_LOCATION)
	{
		strPath.replace(" ", "%20");
		msgInfo.strMsg.remove("\n");
		msgInfo.strMsg.remove(QChar(32));
		msgInfo.strMsg = msgInfo.strMsg.replace("\"", "\\\"");

		QString strSend = QString("SendGroupLocation(\"%1\", \"%2\", \"%3\", %4, \"%5\");")
			.arg(strPath).arg(msgInfo.strMsg).arg(QString(msgInfo.msgID)).arg(msgState).arg(QString::number(msgInfo.nFromUserID));

		m_vm->runJs(strSend);
	}
	if (msgInfo.MessageChildType == MessageType::Message_AUDIO)
	{
		QString msgID = QString(msgInfo.msgID);
		QString audioPath = msgInfo.strMsg;

		if (audioPath.endsWith(".amr"))
		{
			QString wavPath = audioPath;
			wavPath.replace(".amr", ".wav");

			QFile audioFile(wavPath);
			if (!audioFile.exists())
			{
#ifdef Q_OS_WIN
				AmrDec amrDec;
				amrDec.convertAmrToWav(audioPath, wavPath);
#else
				QString appPath = QDir::currentPath() + "/ffmpeg";
				QStringList arguments;
				arguments << "-i" << audioPath << wavPath;
				QProcess process(this);
				process.start(appPath, arguments);
				process.waitForFinished();
				process.close();
#endif
			}

			audioPath = wavPath;
		}

		QFile file(audioPath);
		int duration = CalWavLength(audioPath); //file.size() / BYTES_PER_SECOND;

		strPath.replace(" ", "%20");
		QString strSend = QString("SendGroupAudio(\"%1\",\"%2\", %3, \"%4\",%5, \"%6\");").arg(strPath).arg(audioPath).arg(duration).arg(msgID).arg(msgState).arg(QString::number(msgInfo.nFromUserID));
		m_vm->runJs(strSend);
	}
	if (msgInfo.MessageChildType == MessageType::Message_VEDIO)
	{
		QString strIsLoading;//是否是正加载
		QString strVideoPicPath;//视频第一帧图
		QString strVideoPath;//视频路径

		QString strMsg = "";
		QString strPhotoImgPath = "";
		QString msgID = QString(msgInfo.msgID);
		if (msgInfo.strMsg == "load")
		{
			strMsg = "qrc:/html/Resources/html/load.gif";
			strPhotoImgPath = QString("<img width=30px height=30px src='") + strMsg + QString("' />");

			strIsLoading = "true";
		}
		else if (msgInfo.strMsg != "fail")
		{
#ifdef Q_OS_WIN
			msgInfo.strMsg.replace(" ", "%20");
#endif
			QString strPicPath = msgInfo.strMsg.left(msgInfo.strMsg.indexOf("."));
			strPicPath += ".png";
			VedioFrameOpera pVdo;
			pVdo.CreateVedioPicture(msgInfo.strMsg, strPicPath);
			strPhotoImgPath = QString("<img height:auto width:auto src='file:///%1' name = '%2' onload='movePlayIcon(this);loadpic()'/> ").arg(strPicPath).arg(msgInfo.strMsg);

			strIsLoading = "false";
			strVideoPicPath = "file:///" + strPicPath;
			strVideoPath = msgInfo.strMsg;
		}
		QString strPath = convertHeaderToHtmlSrc(strHeadImage);

		QString strSend = QString("GroupSendVdo(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",%6,%7,\"%8\");").arg(strPath).arg(strIsLoading).arg(strVideoPicPath).arg(strVideoPath).arg(msgID).arg(msgInfo.MessageState).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));
		m_vm->runJs(strSend);
	}
	if (msgInfo.MessageChildType == MessageType::Message_NOTIFY)
	{
		QJsonDocument doc = QJsonDocument::fromJson(strMsg.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		QString content;
		if (map.value("type").toString() == "notification")
		{
			QString inviterID = map.value("inviterID").toString();
			QString invitedID = map.value("invitedID").toString();
			//若为移动端等发来的消息，则不作处理
			if ((inviterID == "0" || inviterID == NULL || inviterID == "") && (invitedID == "0" || invitedID == NULL || invitedID == ""))
				content = map.value("content").toString();
			else if (inviterID == "0" || inviterID == NULL || inviterID == "")
				content = invitedID + tr(" joined the group");
			else
				content = inviterID + tr(" invited ") + invitedID + tr(" to join the group");
		}
		if (map.value("type").toString() == "receivedFile")
		{
			content = tr("The other party has successfully received your file");
		}

		QString strSend = QString("tipMessage(\"%1\");").arg(content);
		m_vm->runJs(strSend);
	}
	if (msgInfo.MessageChildType == MessageType::Message_COMMON)
	{
		QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
		QVariantMap map = doc.toVariant().toMap();

		QString image = map.value("image").toString();
		QString smallIcon = map.value("smallIcon").toString();
		QString title = map.value("title").toString();
		QString content = map.value("content").toString();
		QString systemName = map.value("systemName").toString();
		QString userType = map.value("userType").toString();

		if (image.isEmpty())
		{
			if (userType == "user")
				image = "https://tc.ipcom.io/panserver/files/c64b5061-77dc-45af-b91b-e5e06d401347/download";
			if (userType == "group")
				image = "https://tc.ipcom.io/panserver/files/0bb98df6-cddd-444e-a36c-c50beac4dcd1/download";
		}

		QString strSend = QString("SendGroupCommon('%1', '%2', '%3', '%4', '%5','%6', '%7', %8, %9, '%10');")
			.arg(image).arg(smallIcon).arg(title).arg(content).arg(systemName)
			.arg(strPath).arg(QString(msgInfo.msgID)).arg(msgInfo.MessageState).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));
		m_vm->runJs(strSend);
	}
	if (msgInfo.MessageChildType == MessageType::Message_AT)
	{
		QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
		QVariantMap map = doc.toVariant().toMap();

		QString content = map.value("content").toString();

		QString strMessage = formatMessageFromImgDescriptionWithHtmlEncode(content);
		strMessage = strMessage.toUtf8().toPercentEncoding();
		QString strSend = QString("SendGroupAppend(\"%1\",\"%2\",\"%3\",%4,\"%5\");").arg(strPath, strMessage, QString(msgInfo.msgID), QString::number(msgState), QString::number(msgInfo.nFromUserID));
		m_vm->runJs(strSend);
	}
}

void IMGroupChatStore::OnInsertRecvFileMessage(MessageInfo msgInfo, QString strBuddyNickName)
{
	QString strFromID = QString("%1").arg(msgInfo.nToUserID);
	QString strBuddyID = QString("%1").arg(msgInfo.nFromUserID);
	QString strBuddyHeadImage = GetBuddyHeaderImage(strFromID, strBuddyID);

	//解析messageinfo，解析出文件名，路径
	QJsonParseError jsonError;
	QString strFileName;
	QString FileName;
	QString strFileLocalPath;
	QString fileSize;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8(), &jsonError);
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QVariantMap result = jsonDocument.toVariant().toMap();
			strFileName = result["FileName"].toString();
			strFileLocalPath = result["FileLocalPath"].toString();
			fileSize = result["FileSize"].toString();
		}

		FileTypeEx* fileType = new FileTypeEx(this);
		QString strPath = fileType->GetFilePic(strFileName);
		delete fileType;

		QString OtherHeadImg = convertHeaderToHtmlSrc(strBuddyHeadImage);

		QString strUlID = QString(msgInfo.msgID) + QString("recv");
		QString strUlID2 = QString(msgInfo.msgID) + QString("recv2");
		QString strSize = QString("(") + fileSize + QString(")");
		//传入备注名。
		QString strRecFile = QString("RecGroupFile(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",\"%6\",\"%7\",\"%8\",%9,\"%10\")").arg(OtherHeadImg).arg(IMCommon::jsParam(strBuddyNickName)).arg(strFileName).arg(strPath).arg(QString(msgInfo.msgID)).arg(strUlID).arg(strUlID2).arg(strSize).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));
		m_vm->runJs(strRecFile);

		UserInfo userInfo = gDataManager->getUserInfo();
		QString strFilePath = gDataBaseOpera->DBGetFileInfoLocalPath(QString(msgInfo.msgID));
		QFile fileLocal(strFilePath);
		if (strFilePath != "")
		{
			if (fileLocal.exists())
			{
				ChangeFileState(msgInfo.msgID, 2);
			}
		}
	}
}

void IMGroupChatStore::SendTextMessage(QString strText, QString strHtmlText)
{
	if (gSocketMessage)
	{
		UserInfo userInfo = gDataManager->getUserInfo();
		int nUserID = userInfo.nUserID;

		MessageInfo msgInfo = gSocketMessage->SendTextMessage(nUserID, m_vm->getGroupId().toInt(), 1, (short)0, strText);

		OnInertSendMessageTextInfo(strText, userInfo.strUserAvatarLocal, msgInfo);//用最原始的文本消息

		GroupInfo group = gDataBaseOpera->DBGetGroupFromID(m_vm->getGroupId());
		MessageListInfo messageListInfo;
		messageListInfo.nBudyyID = group.groupId.toInt();
		messageListInfo.strLastMessage = msgInfo.strMsg;
		messageListInfo.nLastTime = msgInfo.ClientTime;
		messageListInfo.strBuddyName = group.groupName;
		messageListInfo.strBuddyHeaderImage = group.groupLoacalHeadImage;
		messageListInfo.messageType = msgInfo.MessageChildType;
		messageListInfo.nUnReadNum = 0;
		messageListInfo.isGroup = 1;
		gDataBaseOpera->DBInsertGroupMessageListInfo(messageListInfo);

		//更新至消息列表，true代表部落消息
		QMap<QString, QVariant> params;
		params["SendStatus"] = MESSAGE_STATE_PRESEND;
		params["MsgId"] = QString(msgInfo.msgID);
		emit m_dispatcher->sigUpdateSelfMessage(true, QVariant::fromValue(group), msgInfo.strMsg, true, params);
	}
}

void IMGroupChatStore::SendPicture(QString fileName)
{
	//插入时间线。
	onInsertTimeLine(GETLOCALTIME);

	QFile file(fileName);
	if (!file.exists())
	{
		QJsonParseError jsonError;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(fileName.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QVariantMap result = jsonDocument.toVariant().toMap();
				fileName = result["path"].toString();
			}
		}
	}

	QString filePathEscape = fileName;
	filePathEscape.replace(" ", "%20");
	filePathEscape.replace("#", "%23");



	QString strIsUserDefine = "false";
	QString strUserDefinePicPath = "";
	QString strPicPath = QString("file:///") + filePathEscape;

	QImage img;
	QByteArray arr;
	QFile filepic(fileName);
	if (filepic.open(QIODevice::ReadOnly))
	{//20181214wmc 手机表情包图片常存在后缀与实际不符的情况 此处直接load会打不开
		arr = filepic.readAll();
	}
	filepic.close();
	img.loadFromData(arr);

	UserInfo userInfo = gDataManager->getUserInfo();
	int nUserID = userInfo.nUserID;
	AppConfig configInfo = gDataManager->getAppConfigInfo();
	MessageInfo messageInfo = gSocketMessage->SendPicMessage(nUserID, m_vm->getGroupId().toInt(), 1, "");
	messageInfo.strMsg = fileName;
	gSocketMessage->DBInsertMessageInfo(messageInfo.nFromUserID, messageInfo.nToUserID, messageInfo);

	QString strPath = convertHeaderToHtmlSrc(userInfo.strUserAvatarLocal);

	if (!strPicPath.isEmpty())
	{
		strPicPath = GetSmallImg(strPicPath);
	}
	QString strSend = QString("SendGroupPicture(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",%6,\"%7\");").arg(strPath).arg(strIsUserDefine).arg(strUserDefinePicPath).arg(strPicPath).arg(QString(messageInfo.msgID)).arg(0).arg(QString::number(messageInfo.nFromUserID));

	m_vm->runJs(strSend);

	PicMessage *picMessage = new PicMessage;
	picMessage->nFromUserID = nUserID;
	picMessage->nToUserID = m_vm->getGroupId().toInt();
	picMessage->nDeliverType = 1;
	picMessage->strUpLoadUrl = configInfo.PanServerUploadURL;
	picMessage->strDownLoadUrl = configInfo.PanServerDownloadURL;
	picMessage->strPicPath = fileName;
	picMessage->nPicHeight = img.height();
	picMessage->nPicWidth = img.width();

	HttpNetWork::HttpUpLoadFile *httpUpLoad = new HttpNetWork::HttpUpLoadFile(this);
	m_iHttp++;
	httpUpLoad->setUserData(Qt::UserRole, picMessage);
	httpUpLoad->setData(QVariant::fromValue(messageInfo));
	connect(httpUpLoad, SIGNAL(sigUpLoadFinished(bool, QByteArray)), this, SLOT(slotUpPicReplyFinished(bool, QByteArray)));
	QVariantMap pargram;
	pargram.insert("parentId", "66662");
	pargram.insert("createUser", "6662");
	httpUpLoad->StartUpLoadFile(configInfo.PanServerUploadURL, fileName, pargram);
	m_listPic.push_back(fileName);

	//更新至消息列表，true代表部落消息
	QMap<QString, QVariant> params;
	params["SendStatus"] = MESSAGE_STATE_PRESEND;
	params["MsgId"] = QString(messageInfo.msgID);
	GroupInfo group = gDataBaseOpera->DBGetGroupFromID(m_vm->getGroupId());
	MessageListInfo messageListInfo;
	messageListInfo.nBudyyID = group.groupId.toInt();
	messageListInfo.strLastMessage = tr("[Image]");
	messageListInfo.nLastTime = QDateTime::currentMSecsSinceEpoch();
	messageListInfo.strBuddyName = group.groupName;
	messageListInfo.strBuddyHeaderImage = group.groupLoacalHeadImage;
	messageListInfo.messageType = MessageType::Message_PIC;
	messageListInfo.nUnReadNum = 0;
	messageListInfo.isGroup = 1;
	gDataBaseOpera->DBInsertGroupMessageListInfo(messageListInfo);
	emit m_dispatcher->sigUpdateSelfMessage(true, QVariant::fromValue(group), tr("[Image]"), true, params);
}

/*发送图片 上传完图片 结果*/
void IMGroupChatStore::slotUpPicReplyFinished(bool result, QByteArray strResult)
{
	m_iHttp--;
	HttpNetWork::HttpUpLoadFile *act = qobject_cast<HttpNetWork::HttpUpLoadFile*>(sender());
	PicMessage *picMessage = (PicMessage *)act->userData(Qt::UserRole);
	QVariant var = act->getData();
	MessageInfo msgInfo = var.value<MessageInfo>();

	if (result)
	{
		gSocketMessage->setUpPicReplyFinished(strResult, picMessage, msgInfo);

		GroupInfo group = gDataBaseOpera->DBGetGroupFromID(m_vm->getGroupId());
		MessageListInfo messageListInfo;
		messageListInfo.nBudyyID = group.groupId.toInt();
		messageListInfo.strLastMessage = tr("[Image]");
		messageListInfo.nLastTime = msgInfo.ClientTime;
		messageListInfo.strBuddyName = group.groupName;
		messageListInfo.strBuddyHeaderImage = group.groupLoacalHeadImage;
		messageListInfo.messageType = msgInfo.MessageChildType;
		messageListInfo.nUnReadNum = 0;
		messageListInfo.isGroup = 1;
		gDataBaseOpera->DBInsertGroupMessageListInfo(messageListInfo);

		//更新至消息列表，true代表部落消息
		QMap<QString, QVariant> params;
		params["SendStatus"] = MESSAGE_STATE_SEND;
		params["MsgId"] = QString(msgInfo.msgID);
		emit m_dispatcher->sigUpdateSelfMessage(true, QVariant::fromValue(group), messageListInfo.strLastMessage, true, params);
	}
	else
	{
		gSocketMessage->SetMessageState(msgInfo.msgID, MESSAGE_STATE_FAILURE);
		this->UpdateMessageStateInfo(msgInfo.msgID, MESSAGE_STATE_FAILURE, msgInfo.integral);
	}
}

void IMGroupChatStore::UpdateMessageStateInfo(QByteArray msgID, int nState, int integral)
{
	QString strSend = QString("SetGroupMsgState(\"%1\",%2,%3)").arg(QString(msgID)).arg(nState).arg(integral);

	m_vm->runJs(strSend);

	QMap<QString, QVariant> params;
	params["SendStatus"] = nState;
	params["MsgId"] = QString(msgID);
	GroupInfo group = gDataBaseOpera->DBGetGroupFromID(m_vm->getGroupId());
	emit m_dispatcher->sigUpdateSelfMessage(true, QVariant::fromValue(group), "", true, params);
}

void IMGroupChatStore::sendTransmitFile(MessageInfo msg)
{
	//插入时间线。
	onInsertTimeLine(GETLOCALTIME);

	UserInfo userinfo = gDataManager->getUserInfo();
	int nUserID = userinfo.nUserID;

	QJsonDocument doc = QJsonDocument::fromJson(msg.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();

	QString fileName = map.value("FileName").toString();
	QString filePath = gDataBaseOpera->DBGetFileInfoLocalPath(msg.msgID);

	QFile transmitFile(filePath);
	if (transmitFile.exists())
	{
		MessageInfo messageInfo = gSocketMessage->SendFileMessage(nUserID, m_vm->getGroupId().toInt(), 1, "");
		gDataBaseOpera->DBOnInsertFileInfo(QString(messageInfo.msgID), filePath, "");//将发送文件信息插入数据库

		FileTypeEx* fileType = new FileTypeEx(this);
		QString strPath = fileType->GetFilePic(filePath);

		QString strLiID = QString(messageInfo.msgID) + QString("send");
		QString strsize = gSocketMessage->ByteConversion(fileType->GetFileSize(filePath));
		delete fileType;
		strsize = QString("(") + strsize + QString(")");
		QString strHeadPath = convertHeaderToHtmlSrc(userinfo.strUserAvatarLocal);
		QString strend = QString("SendGroupFile(\"%1\",\"%2\",\"%3\",%4,\"%5\",\"%6\",\"%7\",\"%8\")").arg(strHeadPath).arg(fileName).arg(QString(messageInfo.msgID)).arg(0).arg(strPath).arg(strLiID).arg(strsize).arg(QString::number(messageInfo.nFromUserID));
		m_vm->runJs(strend);
		AppConfig conf = gDataManager->getAppConfigInfo();

		FileMessage *fileMessage = new FileMessage;
		fileMessage->nFromUserID = nUserID;
		fileMessage->nToUserID = m_vm->getGroupId().toInt();
		fileMessage->nDeliverType = 0;
		fileMessage->strUpLoadUrl = conf.PanServerUploadURL;
		fileMessage->strDownLoadUrl = conf.PanServerDownloadURL;
		fileMessage->strFilePath = filePath;

		QFile upfile(filePath);
		fileMessage->FileName = fileName;
		fileMessage->FileType = fileName.split(".").last();
		fileMessage->FileSize = gSocketMessage->ByteConversion(upfile.size());

		MessageInfo msgInfo = messageInfo;

		QString newLiID = QString(msgInfo.msgID) + QString("send");
		QString changeLi = QString("ChangeLi(\"%1\")").arg(newLiID);
		

		QString cancleID = msgInfo.msgID + QString("can");
		QString cancle = QString("ChangeLi(\"%1\")").arg(cancleID);
		
		ChangeFileState(msgInfo.msgID, 2);
		QVariantMap resultMap;
		resultMap.insert("result", "success");
		resultMap.insert("fileid", map.value("FileId"));
		QByteArray result = QJsonDocument::fromVariant(resultMap).toJson();

		gSocketMessage->setUpFileReplyFinished(result, fileMessage, msgInfo);

		GroupInfo group = gDataBaseOpera->DBGetGroupFromID(m_vm->getGroupId());
		MessageListInfo messageListInfo;
		messageListInfo.nBudyyID = group.groupId.toInt();
		messageListInfo.strLastMessage = tr("[File]");
		messageListInfo.nLastTime = msgInfo.ClientTime;
		messageListInfo.strBuddyName = group.groupName;
		messageListInfo.strBuddyHeaderImage = group.groupLoacalHeadImage;
		messageListInfo.messageType = msgInfo.MessageChildType;
		messageListInfo.nUnReadNum = 0;
		messageListInfo.isGroup = 1;
		gDataBaseOpera->DBInsertGroupMessageListInfo(messageListInfo);

		//更新至消息列表，true代表部落消息
		QMap<QString, QVariant> params;
		emit m_dispatcher->sigUpdateSelfMessage(true, QVariant::fromValue(group), messageListInfo.strLastMessage, true, params);
	}
	else
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("Local file does't exist and cannot be forwarded"));
	}
}

void IMGroupChatStore::SendVideo(MessageInfo msg)
{
	QString fileLocalPath = msg.strMsg;
	QFile transmitFile(fileLocalPath);

	if (transmitFile.exists())
	{
		if (gSocketMessage)
		{
			UserInfo userInfo = gDataManager->getUserInfo();
			int nUserID = userInfo.nUserID;

			QString getFileHttpPath = gDataBaseOpera->DBGetFileInfoHttpPathByLocalPath(msg.strMsg);
			MessageInfo msgInfo = gSocketMessage->SendVideoMessage(nUserID, m_vm->getGroupId().toInt(), 1, Message_VEDIO, getFileHttpPath);

			/*查詢一遍本地路徑並更新數據庫*/
			msgInfo.strMsg = msg.strMsg;
			fileLocalPath = fileLocalPath.replace(gSettingsManager->getUserPath(), "");
			gSocketMessage->DBRestoreLocalPathInMessageInfo(msgInfo.msgID, fileLocalPath);
			OnInertSendMessageTextInfo(msg.strMsg, userInfo.strUserAvatarLocal, msgInfo);//用最原始的文本消息

			GroupInfo group = gDataBaseOpera->DBGetGroupFromID(m_vm->getGroupId());
			MessageListInfo messageListInfo;
			messageListInfo.nBudyyID = group.groupId.toInt();
			messageListInfo.strLastMessage = tr("[File]");
			messageListInfo.nLastTime = msgInfo.ClientTime;
			messageListInfo.strBuddyName = group.groupName;
			messageListInfo.strBuddyHeaderImage = group.groupLoacalHeadImage;
			messageListInfo.messageType = msgInfo.MessageChildType;
			messageListInfo.nUnReadNum = 0;
			messageListInfo.isGroup = 1;
			gDataBaseOpera->DBInsertGroupMessageListInfo(messageListInfo);
			//更新至消息列表，true代表部落消息
			QMap<QString, QVariant> params;

			emit m_dispatcher->sigUpdateSelfMessage(true, QVariant::fromValue(group), messageListInfo.strLastMessage, true, params);
		}
	}
	else
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("Local file does't exist and cannot be forwarded"));
	}
}

void IMGroupChatStore::OnSendTransmitMessage(MessageInfo msg)
{
	if (initFinisthed)  //界面初始化完毕
	{
		switch (msg.MessageChildType)
		{
		case MessageType::Message_TEXT:
			SendTextMessage(msg.strMsg, msg.strMsg);
			break;
		case MessageType::Message_PIC:
			SendPicture(msg.strMsg);
			break;
		case MessageType::Message_FILE:
			sendTransmitFile(msg);
			break;
		case MessageType::Message_VEDIO:
			SendVideo(msg);
			break;
		default:
			break;
		}
	}
	else
	{
		waitMessages.append(msg);
	}
}

void IMGroupChatStore::InitMessageInfo(QString strEndRowId/* = ""*/)
{
	//strEndRowId有值时，说明当前点击的是获取更多消息

	if (!strEndRowId.isEmpty())
	{
		m_vm->runJs("BeginShowMore()");
		m_foremostRowId = "";
		m_isShowingMore = true;
	}

	if (strEndRowId.isEmpty())
	{
		//清空
		OnClearMessageInfo();
	}

	//获取好友头像
	UserInfo userInfo = gDataManager->getUserInfo();
	QString strUserHeadImage = userInfo.strUserAvatarLocal;

	QMap<QString, QList<MessageInfo> > mapTemp = gSocketMessage->DBGetRecnetMessage(userInfo.nUserID, m_vm->getGroupId().toInt(), strEndRowId);
	QMap<QString, QList<MessageInfo> >::iterator itor = mapTemp.begin();
	for (; itor != mapTemp.end(); ++itor)
	{
		if (itor.key() == m_vm->getGroupId())
		{
			QList<MessageInfo> msgList = itor.value();

			if (msgList.count() >= (strEndRowId.isEmpty() ? DB_MESSAGEINFO_INIT_MAX_NUM : DB_MESSAGEINFO_FETCH_MAX_NUM))
			{
				m_vm->runJs("AddShowMore()");
			}


			if (msgList.size() > 0)
			{
				m_foremostRowId = msgList[0].strRowId;
			}

			for (int i = 0; i < msgList.size(); i++)
			{
				if (msgList[i].nFromUserID == userInfo.nUserID)
				{
					//说明我是发送方
					OnInertSendMessageTextInfo(msgList[i].strMsg, strUserHeadImage, msgList[i]);
				}
				else
				{
					//插入时间线，
					onInsertTimeLine(msgList[i].ServerTime);

					QString strBuddyID = QString("%1").arg(msgList[i].nFromUserID);
					QString strBuddyHeadImage = GetBuddyHeaderImage(m_vm->getGroupId(), strBuddyID);

					QList<BuddyInfo> buddyInfo = gDataBaseOpera->DBGetGroupBuddyInfoFromID(QString::number(msgList[i].nToUserID));
					QString strBuddyNickName;
					for (int j = 0; j < buddyInfo.count(); j++)
					{
						if (buddyInfo.at(j).nUserId == strBuddyID.toInt())
						{
							if (buddyInfo.at(j).strNote.isEmpty())
								strBuddyNickName = buddyInfo.at(j).strNickName;
							else
								strBuddyNickName = buddyInfo.at(j).strNote;
						}
					}

					if (strBuddyNickName.isEmpty())
						strBuddyNickName = strBuddyID;

					if (msgList[i].MessageChildType == MessageType::Message_TEXT)
					{
						OnInsertRecvMessageTextInfo(msgList[i].strMsg, strBuddyHeadImage, strBuddyNickName, QString(msgList[i].msgID), msgList[i].integral, QString::number(msgList[i].nFromUserID));
					}
					if (msgList[i].MessageChildType == MessageType::Message_PIC)
					{
						if (!QFile::exists(msgList[i].strMsg))
						{
#ifdef Q_OS_WIN
							msgList[i].strMsg = gSettingsManager->getUserPath() + msgList[i].strMsg;
#else
							msgList[i].strMsg = msgList[i].strMsg.replace("%20", " ");
#endif
						}

						QString strIsUserDefine = "false";
						QString strUserDefinePicPath = "";
						QString strPicPath = QString("file:///") + msgList[i].strMsg;

						OnInsertRecvPictureInfo(strIsUserDefine, strUserDefinePicPath, strPicPath, strBuddyHeadImage, strBuddyNickName, QString(msgList[i].msgID), msgList[i].integral, QString::number(msgList[i].nFromUserID));
						m_listPic.push_back(msgList[i].strMsg);
					}
					if (msgList[i].MessageChildType == MessageType::Message_AUDIO)
					{
						OnRecvAudioMessage(msgList[i], strBuddyNickName, strBuddyHeadImage);
					}
					if (msgList[i].MessageChildType == MessageType::Message_VEDIO)
					{
#ifdef Q_OS_WIN
						msgList[i].strMsg.replace(" ", "%20");
#endif
						QString strPicPath = msgList[i].strMsg.left(msgList[i].strMsg.indexOf("."));
						strPicPath += ".png";
						VedioFrameOpera pVdo;
						pVdo.CreateVedioPicture(msgList[i].strMsg, strPicPath);
						QString strPhotoImgPath = QString("<img height:auto width:auto src='file:///%1' name = '%2' onload='movePlayIcon(this);loadpic()'/> ").arg(strPicPath).arg(msgList[i].strMsg);

						QString strIsLoading = "false";//是否是正加载
						QString strVideoPicPath = "file:///" + strPicPath;//视频第一帧图
						QString strVideoPath = msgList[i].strMsg;//视频路径

						OnInsertRecVideoInfo(strIsLoading, strVideoPicPath, strVideoPath, strBuddyHeadImage, strBuddyNickName, QString(msgList[i].msgID), msgList[i].integral, QString::number(msgList[i].nFromUserID));
					}
					if (msgList[i].MessageChildType == MessageType::Message_FILE)
					{
						OnInsertRecvFileMessage(msgList[i], strBuddyNickName);
					}
					if (msgList[i].MessageChildType == MessageType::Message_REDBAG)
					{
						onRecvRedBagMessage(msgList[i], strBuddyNickName, strBuddyHeadImage);
					}
					if (msgList[i].MessageChildType == MessageType::Message_NOTICE)
					{
						OnRecvNoticeMessage(msgList[i], strBuddyNickName, strBuddyHeadImage);
					}
					if (msgList[i].MessageChildType == MessageType::Message_URL)
					{
						onRecvShareUrlMessage(msgList[i], strBuddyNickName, strBuddyHeadImage);
					}
					if (msgList[i].MessageChildType == MessageType::Message_LOCATION)
					{
						onRecvLocationMessage(msgList[i], strBuddyNickName, strBuddyHeadImage);
					}
					if (msgList[i].MessageChildType == MessageType::Message_COMMON)
					{
						OnRecvCommonMessage(msgList[i], strBuddyNickName, strBuddyHeadImage);
					}
					if (msgList[i].MessageChildType == MessageType::Message_NOTIFY)
					{
						OnRecvNotifyMessage(msgList[i]);
					}
					if (msgList[i].MessageChildType == MessageType::Message_AT)
					{
						QJsonDocument doc = QJsonDocument::fromJson(msgList[i].strMsg.toUtf8());
						QVariantMap map = doc.toVariant().toMap();
						QString content = map.value("content").toString();
						OnInsertRecvMessageTextInfo(content, strBuddyHeadImage, strBuddyNickName, QString(msgList[i].msgID), msgList[i].integral, QString::number(msgList[i].nFromUserID));
					}
				}
			}
			break;
		}
	}

	initFinisthed = true;
	while (!waitMessages.isEmpty())
	{
		MessageInfo msg = waitMessages.first();
		OnSendTransmitMessage(msg);
		waitMessages.removeFirst();
	}

	m_vm->startThread();

	if (!strEndRowId.isEmpty())
	{
		m_vm->runJs("EndShowMore()");
		m_isShowingMore = false;
	}

}

void IMGroupChatStore::slotShowByChatId(bool isNew)
{
	m_vm->showByChatId(isNew);
}

bool IMGroupChatStore::IsDownloading()
{
	if (m_iHttp > 0)
	{
		return true;
	}
	return false;
}

void IMGroupChatStore::slotUpdateMessageStateInfo(QByteArray msgID, int nState, int integral)
{
	QString strSend = QString("SetGroupMsgState(\"%1\",%2,%3)").arg(QString(msgID)).arg(nState).arg(integral);
	m_vm->runJs(strSend);

	QMap<QString, QVariant> params;
	params["SendStatus"] = nState;
	params["MsgId"] = QString(msgID);
	GroupInfo group = gDataBaseOpera->DBGetGroupFromID(m_vm->getGroupId());
	emit m_dispatcher->sigUpdateSelfMessage(true, QVariant::fromValue(group), "", true, params);
}


void IMGroupChatStore::OnSendMessageTextInfo(QString strPath, MessageInfo msgInfo)
{
	QString strMessage = formatMessageFromImgDescriptionWithHtmlEncode(msgInfo.strMsg);

	strMessage = strMessage.toUtf8().toPercentEncoding();
	QString strSend = QString("SendGroupAppend(\"%1\",\"%2\",\"%3\",%4,\"%5\");").arg(strPath, strMessage, QString(msgInfo.msgID), QString::number(msgInfo.MessageState), QString::number(msgInfo.nFromUserID));
	m_vm->runJs(strSend);
}

void IMGroupChatStore::OnSendImgMessage(QString strPath, MessageInfo msgInfo)
{
	QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();
	QString path = map.value("path").toString();
	if (!path.isEmpty())
		msgInfo.strMsg = path;

	if (!QFile::exists(msgInfo.strMsg))
	{
		msgInfo.strMsg = gSettingsManager->getUserPath() + msgInfo.strMsg;
	}

	QFile image(msgInfo.strMsg);
	if (!image.exists())
		return;

	QString filePathEscape = msgInfo.strMsg;
	filePathEscape.replace(" ", "%20");
	filePathEscape.replace("#", "%23");

	strPath.replace(" ", "%20");

	QString strIsUserDefine = "false";
	QString strUserDefinePicPath = "";
	QString strPicPath = QString("file:///") + filePathEscape;

	if (!strPicPath.isEmpty())
	{
		strPicPath = GetSmallImg(strPicPath);
	}
	QString strSend = QString("SendGroupPicture(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",%6,\"%7\");").arg(strPath).arg(strIsUserDefine).arg(strUserDefinePicPath).arg(strPicPath).arg(QString(msgInfo.msgID)).arg(msgInfo.MessageState).arg(QString::number(msgInfo.nFromUserID));
	m_vm->runJs(strSend);
	m_listPic.push_back(msgInfo.strMsg);
}

void IMGroupChatStore::OnSendFileMessage(QString strPath, MessageInfo msgInfo)
{
	msgInfo.MessageState = msgInfo.MessageState;
	InsertSendFileMessage(msgInfo);
}

void IMGroupChatStore::OnSendRedBagMessage(QString strPath, MessageInfo msgInfo)
{
	QJsonDocument json = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = json.toVariant().toMap();
	QString invalid = map.value("invalid").toString();
	QString remarks;
	if (invalid == "true")    //已经领取/过期等等。
	{
		QVariantMap packet = map.value("redPacket").toMap();
		remarks = packet.value("remarks").toString();
	}
	else                      //还未领取。
	{
		remarks = map.value("leaveMessage").toString();
	}

	if (remarks.isEmpty())
		remarks = tr("Best Wishes");

	QString strSend = QString("GroupRedPacketSend('%1', '%2', '%3', '%4', %5, %6, '%7');")
		.arg(strPath).arg(remarks).arg(invalid).arg(QString(msgInfo.msgID)).arg(msgInfo.MessageState).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));
	m_vm->runJs(strSend);
}

void IMGroupChatStore::OnSendNoticeMessage(QString strPath, MessageInfo msgInfo)
{
	strPath.replace(" ", "%20");

	QString msg = msgInfo.strMsg;
	QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();
	QString imageUrl = map.value("imageUrl").toString();
	QString imageTitle = map.value("imageTitle").toString();
	QString webUrl = map.value("webUrl").toString();
	QString webTitle = map.value("webTitle").toString();

	QString strSend = QString("SendGroupNotice(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",\"%6\",%7,%8,\"%9\");")
		.arg(strPath).arg("").arg(imageTitle).arg(webUrl).arg(webTitle).arg(QString(msgInfo.msgID))
		.arg(msgInfo.MessageState).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));

	m_vm->runJs(strSend);
	/////////////////图片方向
	QString strFilePath = QDir::tempPath() + "/" + QUuid::createUuid().toString() + ".jpg";
	HttpNetWork::HttpDownLoadFile *netWork = new HttpNetWork::HttpDownLoadFile;
	netWork->setData(QVariant::fromValue(msgInfo));
	netWork->setObjectName(strFilePath);
	connect(netWork, SIGNAL(sigDownFinished(bool)), this, SLOT(slotUpdateNotice(bool)));
	netWork->StartDownLoadFile(imageUrl, strFilePath);
	/////////////////
}

void IMGroupChatStore::OnSendShareUrlMessage(QString strPath, MessageInfo msgInfo)
{
	QString msg = msgInfo.strMsg;
	QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();
	QString subject = map.value("subject").toString();
	QString text = map.value("text").toString();
	QString url = map.value("url").toString();
	QString imageUrl = map.value("imgUrl").toString();

	QString strSend = QString("SendGroupShareUrl(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",\"%6\",%7,%8,\"%9\");")
		.arg(subject).arg(text).arg(url).arg(imageUrl)
		.arg(strPath).arg(QString(msgInfo.msgID))
		.arg(msgInfo.MessageState).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));

	m_vm->runJs(strSend);
}

void IMGroupChatStore::OnSendLocationMessage(QString strPath, MessageInfo msgInfo)
{
	strPath.replace(" ", "%20");
	msgInfo.strMsg.remove("\n");
	msgInfo.strMsg.remove(QChar(32));
	msgInfo.strMsg = msgInfo.strMsg.replace("\"", "\\\"");

	QString strSend = QString("SendGroupLocation(\"%1\", \"%2\", \"%3\", %4, \"%5\");")
		.arg(strPath).arg(msgInfo.strMsg).arg(QString(msgInfo.msgID)).arg(msgInfo.MessageState).arg(QString::number(msgInfo.nFromUserID));

	m_vm->runJs(strSend);
}

void IMGroupChatStore::OnSendAudioMessage(QString strPath, MessageInfo msgInfo)
{
	QString msgID = QString(msgInfo.msgID);
	QString audioPath = msgInfo.strMsg;

	if (audioPath.endsWith(".amr"))
	{
		QString wavPath = audioPath;
		wavPath.replace(".amr", ".wav");

		QFile audioFile(wavPath);
		if (!audioFile.exists())
		{
#ifdef Q_OS_WIN
			AmrDec amrDec;
			amrDec.convertAmrToWav(audioPath, wavPath);
#else
			QString appPath = QDir::currentPath() + "/ffmpeg";
			QStringList arguments;
			arguments << "-i" << audioPath << wavPath;
			QProcess process(this);
			process.start(appPath, arguments);
			process.waitForFinished();
			process.close();
#endif
	}

		audioPath = wavPath;
}

	QFile file(audioPath);
	int duration = CalWavLength(audioPath); //file.size() / BYTES_PER_SECOND;

	strPath.replace(" ", "%20");
	QString strSend = QString("SendGroupAudio(\"%1\",\"%2\", %3, \"%4\",%5, \"%6\");").arg(strPath).arg(audioPath).arg(duration).arg(msgID).arg(msgInfo.MessageState).arg(QString::number(msgInfo.nFromUserID));
	m_vm->runJs(strSend);
}

void IMGroupChatStore::OnSendVideoMessage(QString strPath, MessageInfo msgInfo)
{
	QString strIsLoading;//是否是正加载
	QString strVideoPicPath;//视频第一帧图
	QString strVideoPath;//视频路径

	QString strMsg = "";
	QString strPhotoImgPath = "";
	QString msgID = QString(msgInfo.msgID);
	if (msgInfo.strMsg == "load")
	{
		strMsg = "qrc:/html/Resources/html/load.gif";
		strPhotoImgPath = QString("<img width=30px height=30px src='") + strMsg + QString("' />");

		strIsLoading = "true";
	}
	else if (msgInfo.strMsg != "fail")
	{
#ifdef Q_OS_WIN
		msgInfo.strMsg.replace(" ", "%20");
#endif
		QString strPicPath = msgInfo.strMsg.left(msgInfo.strMsg.indexOf("."));
		strPicPath += ".png";
		VedioFrameOpera pVdo;
		pVdo.CreateVedioPicture(msgInfo.strMsg, strPicPath);
		strPhotoImgPath = QString("<img height:auto width:auto src='file:///%1' name = '%2' onload='movePlayIcon(this);loadpic()'/> ").arg(strPicPath).arg(msgInfo.strMsg);

		strIsLoading = "false";
		strVideoPicPath = "file:///" + strPicPath;
		strVideoPath = msgInfo.strMsg;
	}

	QString strSend = QString("GroupSendVdo(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",%6,%7,\"%8\");").arg(strPath).arg(strIsLoading).arg(strVideoPicPath).arg(strVideoPath).arg(msgID).arg(msgInfo.MessageState).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));
	m_vm->runJs(strSend);
}

void IMGroupChatStore::OnSendNotifyMessage(QString strPath, MessageInfo msgInfo)
{
	QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();
	QString content;
	if (map.value("type").toString() == "notification")
	{
		QString inviterID = map.value("inviterID").toString();
		QString invitedID = map.value("invitedID").toString();
		//若为移动端等发来的消息，则不作处理
		if ((inviterID == "0" || inviterID == NULL || inviterID == "") && (invitedID == "0" || invitedID == NULL || invitedID == ""))
			content = map.value("content").toString();
		else if (inviterID == "0" || inviterID == NULL || inviterID == "")
			content = invitedID + tr(" joined the group");
		else
			content = inviterID + tr(" invited ") + invitedID + tr(" to join the group");
	}
	if (map.value("type").toString() == "receivedFile")
	{
		content = tr("The other party has successfully received your file");
	}

	QString strSend = QString("tipMessage(\"%1\");").arg(content);
	m_vm->runJs(strSend);
}

void IMGroupChatStore::OnSendCommonMessage(QString strPath, MessageInfo msgInfo)
{
	QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();

	QString image = map.value("image").toString();
	QString smallIcon = map.value("smallIcon").toString();
	QString title = map.value("title").toString();
	QString content = map.value("content").toString();
	QString systemName = map.value("systemName").toString();
	QString userType = map.value("userType").toString();

	if (image.isEmpty())
	{
		if (userType == "user")
			image = "https://tc.ipcom.io/panserver/files/c64b5061-77dc-45af-b91b-e5e06d401347/download";
		if (userType == "group")
			image = "https://tc.ipcom.io/panserver/files/0bb98df6-cddd-444e-a36c-c50beac4dcd1/download";
	}

	QString strSend = QString("SendGroupCommon('%1', '%2', '%3', '%4', '%5','%6', '%7', %8, %9, '%10');")
		.arg(image).arg(smallIcon).arg(title).arg(content).arg(systemName)
		.arg(strPath).arg(QString(msgInfo.msgID)).arg(msgInfo.MessageState).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));
	m_vm->runJs(strSend);
}

void IMGroupChatStore::OnSendAtMessage(QString strPath, MessageInfo msgInfo)
{
	QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();

	QString content = map.value("content").toString();

	QString strMessage = formatMessageFromImgDescriptionWithHtmlEncode(content);
	strMessage = strMessage.toUtf8().toPercentEncoding();
	QString strSend = QString("SendGroupAppend(\"%1\",\"%2\",\"%3\",%4,\"%5\");").arg(strPath, strMessage, QString(msgInfo.msgID), QString::number(msgInfo.MessageState), QString::number(msgInfo.nFromUserID));
	m_vm->runJs(strSend);
}

void IMGroupChatStore::slotProcessSendMessageInfo(QString strHeadImage, MessageInfo msgInfo)
{
	QList<int> types;
	types << MessageType::Message_TEXT << MessageType::Message_PIC << MessageType::Message_FILE << MessageType::Message_AT;
	types << MessageType::Message_NOTICE << MessageType::Message_URL << MessageType::Message_LOCATION;
	types << MessageType::Message_AUDIO << MessageType::Message_VEDIO;
	types << MessageType::Message_NOTIFY << MessageType::Message_COMMON;
	if (types.contains((int)msgInfo.MessageChildType))
	{
		//插入时间线。
		if (msgInfo.ServerTime)
			onInsertTimeLine(msgInfo.ServerTime);
		else
			onInsertTimeLine(msgInfo.ClientTime);
	}

	QString strPath = convertHeaderToHtmlSrc(strHeadImage);

	switch (msgInfo.MessageChildType)
	{
	case MessageType::Message_TEXT:
		OnSendMessageTextInfo(strPath, msgInfo);
		break;
	case MessageType::Message_PIC:
		OnSendImgMessage(strPath, msgInfo);
		break;
	case MessageType::Message_FILE:
		OnSendFileMessage(strPath, msgInfo);
		break;
	case MessageType::Message_REDBAG:
		OnSendRedBagMessage(strPath, msgInfo);
		break;
	case MessageType::Message_NOTICE:
		OnSendNoticeMessage(strPath, msgInfo);
		break;
	case MessageType::Message_URL:
		OnSendShareUrlMessage(strPath, msgInfo);
		break;
	case MessageType::Message_LOCATION:
		OnSendLocationMessage(strPath, msgInfo);
		break;
	case MessageType::Message_COMMON:
		OnSendCommonMessage(strPath, msgInfo);
		break;
	case MessageType::Message_AUDIO:
		OnSendAudioMessage(strPath, msgInfo);
		break;
	case MessageType::Message_VEDIO:
		OnSendVideoMessage(strPath, msgInfo);
		break;
	case MessageType::Message_NOTIFY:
		OnSendNotifyMessage(strPath, msgInfo);
		break;
	case MessageType::Message_AT:
		OnSendAtMessage(strPath, msgInfo);
		break;
	default:
		break;
	}
}

void IMGroupChatStore::slotInsertTextEditPic(QString strPath)
{
	m_vm->insertImageToTextEditSend(strPath);
}

void IMGroupChatStore::slotSendTransmitMessage(MessageInfo msg)
{
	if (initFinisthed)  //界面初始化完毕
	{
		switch (msg.MessageChildType)
		{
		case MessageType::Message_TEXT:
			SendTextMessage(msg.strMsg, msg.strMsg);
			break;
		case MessageType::Message_PIC:
			SendPicture(msg.strMsg);
			break;
		case MessageType::Message_FILE:
			sendTransmitFile(msg);
			break;
		case MessageType::Message_VEDIO:
			SendVideo(msg);
			break;
		default:
			break;
		}
	}
	else
	{
		waitMessages.append(msg);
	}
}

void IMGroupChatStore::slotShareID(int type, QString contactID)
{
	QString title;
	QString AndroidShow;
	QString strShow;
	QString userType;
	QString systemName;
	QString content;
	QString image;

	QString smallIcon = "https://tc.ipcom.io/panserver/files/2c6a7eda-7995-4bd6-828b-4ba9b4d2c163/download";
	QString viewType = "display";
	int id = contactID.toInt();


	QVariantMap map;
	if (type == OpenPer)
	{
		BuddyInfo buddyInfo = gDataBaseOpera->DBGetBuddyInfoByID(contactID);
		title = buddyInfo.strNickName;
		AndroidShow = "com.efounder.chat.activity.JFUserInfoActivity";
		strShow = "EFUserDetailViewController";
		userType = "user";
		systemName = tr("Personal Business Card");
		content = tr("ID:") + contactID;
		if (buddyInfo.strHttpAvatar.isEmpty())
			image = "https://tc.ipcom.io/panserver/files/c64b5061-77dc-45af-b91b-e5e06d401347/download";
		else
			image = buddyInfo.strHttpAvatar;
	}
	if (type == OpenGroup)
	{
		GroupInfo groupInfo = gDataBaseOpera->DBGetGroupFromID(contactID);
		title = groupInfo.groupName;
		AndroidShow = "com.efounder.chat.activity.AddGroupUserInfoActivity";
		strShow = "GroupApplicantViewController";
		userType = "group";
		systemName = tr("Group Business Card");
		content = tr("Group ID:") + contactID;
		if (groupInfo.groupHttpHeadImage.isEmpty())
			image = "https://tc.ipcom.io/panserver/files/0bb98df6-cddd-444e-a36c-c50beac4dcd1/download";
		else
			image = groupInfo.groupHttpHeadImage;
	}

	map.insert("title", title);
	map.insert("smallIcon", smallIcon);
	map.insert("time", "");
	map.insert("AndroidShow", AndroidShow);
	map.insert("show", strShow);
	map.insert("userType", userType);
	map.insert("systemName", systemName);
	map.insert("content", content);
	map.insert("viewType", viewType);
	map.insert("id", id);
	map.insert("image", image);

	QString text = QJsonDocument::fromVariant(map).toJson();
	UserInfo userinfo = gDataManager->getUserInfo();
	MessageInfo msgInfo = gSocketMessage->SendTextMessage(userinfo.nUserID, objectName().toInt(), 1, (short)MessageType::Message_COMMON, text);

	OnInertSendMessageTextInfo(msgInfo.strMsg, userinfo.strUserAvatarLocal, msgInfo);

	GroupInfo group = gDataBaseOpera->DBGetGroupFromID(m_vm->getGroupId());
	MessageListInfo messageListInfo;
	messageListInfo.nBudyyID = group.groupId.toInt();
	messageListInfo.strLastMessage = QString("[%1]").arg(systemName);
	messageListInfo.nLastTime = msgInfo.ClientTime;
	messageListInfo.strBuddyName = group.groupName;
	messageListInfo.strBuddyHeaderImage = group.groupLoacalHeadImage;
	messageListInfo.messageType = msgInfo.MessageChildType;
	messageListInfo.nUnReadNum = 0;
	messageListInfo.isGroup = 1;
	gDataBaseOpera->DBInsertGroupMessageListInfo(messageListInfo);

	//更新至消息列表，true代表部落消息
	QMap<QString, QVariant> params;
	emit m_dispatcher->sigUpdateSelfMessage(true, QVariant::fromValue(group), messageListInfo.strLastMessage, true, params);
}

QString IMGroupChatStore::getDraft()
{
	return m_vm->getDraft();
}

void IMGroupChatStore::slotSendAtMessage(QString content, QString message)
{
	if (gSocketMessage)
	{
		UserInfo userInfo = gDataManager->getUserInfo();
		int nUserID = userInfo.nUserID;

		MessageInfo msgInfo = gSocketMessage->SendTextMessage(nUserID, m_vm->getGroupId().toInt(), 1, (short)MessageType::Message_AT, message);

		OnInertSendMessageTextInfo(message, userInfo.strUserAvatarLocal, msgInfo);//用最原始的文本消息

		GroupInfo group = gDataBaseOpera->DBGetGroupFromID(m_vm->getGroupId());
		MessageListInfo messageListInfo;
		messageListInfo.nBudyyID = group.groupId.toInt();
		messageListInfo.strLastMessage = content;
		messageListInfo.nLastTime = msgInfo.ClientTime;
		messageListInfo.strBuddyName = group.groupName;
		messageListInfo.strBuddyHeaderImage = group.groupLoacalHeadImage;
		messageListInfo.messageType = msgInfo.MessageChildType;
		messageListInfo.nUnReadNum = 0;
		messageListInfo.isGroup = 1;
		gDataBaseOpera->DBInsertGroupMessageListInfo(messageListInfo);

		//更新至消息列表，true代表部落消息
		QMap<QString, QVariant> params;
		params["SendStatus"] = MESSAGE_STATE_PRESEND;
		params["MsgId"] = QString(msgInfo.msgID);
		emit m_dispatcher->sigUpdateSelfMessage(true, QVariant::fromValue(group), content, true, params);
	}
}

void IMGroupChatStore::slotItemDoubleClicked(int index)
{
	QString strUserID = m_vm->getValueByIndexAndType(index, GroupUserListModel::UserIdRole).toString();

	UserInfo userInfo = gDataManager->getUserInfo();

	if (strUserID != QString::number(userInfo.nUserID))  //不是自己。
	{
		HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
		connect(http, SIGNAL(sigReplyFinished(bool, QString)), m_view, SLOT(slotGetItemInfo(bool, QString)));

		QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/user/getOtherUserByUserId?otherUserId=" + strUserID;

		http->getHttpRequest(url);
	}
}



void IMGroupChatStore::slotGivePacketData(QString packetData)
{
	UserInfo userinfo = gDataManager->getUserInfo();

	MessageInfo msgInfo = gSocketMessage->SendTextMessage(userinfo.nUserID, objectName().toInt(), 1, (short)MessageType::Message_REDBAG, packetData);

	OnInertSendMessageTextInfo(msgInfo.strMsg, userinfo.strUserAvatarLocal, msgInfo);

	//根据消息类型设置显示的内容。
	msgInfo.strMsg = tr("[Red Packet]");

	GroupInfo group = gDataBaseOpera->DBGetGroupFromID(m_vm->getGroupId());
	MessageListInfo messageListInfo;
	messageListInfo.nBudyyID = group.groupId.toInt();
	messageListInfo.strLastMessage = msgInfo.strMsg;
	messageListInfo.nLastTime = msgInfo.ClientTime;
	messageListInfo.strBuddyName = group.groupName;
	messageListInfo.strBuddyHeaderImage = group.groupLoacalHeadImage;
	messageListInfo.messageType = msgInfo.MessageChildType;
	messageListInfo.nUnReadNum = 0;
	messageListInfo.isGroup = 1;
	gDataBaseOpera->DBInsertGroupMessageListInfo(messageListInfo);

	//更新左侧消息列表内容
	QMap<QString, QVariant> params;
	emit m_dispatcher->sigUpdateSelfMessage(true, QVariant::fromValue(group), messageListInfo.strLastMessage, true, params);
}

void IMGroupChatStore::slotSendFile(QString fileName)
{
	//判断文件是否大于100M
	QFile file(fileName);
	qint64 iSize = file.size();
	if (iSize > 1024 * 1024 * 100)
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("Cannot send file larger than 100M in size"));
		return;
	}
	//插入时间线
	onInsertTimeLine(GETLOCALTIME);

	QString FileName = fileName.mid(fileName.lastIndexOf("/") + 1);
	UserInfo userinfo = gDataManager->getUserInfo();
	QString strHeadPath = convertHeaderToHtmlSrc(userinfo.strUserAvatarLocal);

	int nUserID = userinfo.nUserID;

	MessageInfo messageInfo = gSocketMessage->SendFileMessage(nUserID, m_vm->getGroupId().toInt(), 1, "");
	gDataBaseOpera->DBOnInsertFileInfo(QString(messageInfo.msgID), fileName, "");//将发送文件信息插入数据库
	messageInfo.strMsg = fileName;
	gSocketMessage->DBInsertMessageInfo(messageInfo.nFromUserID, messageInfo.nToUserID, messageInfo);

	FileTypeEx* fileType = new FileTypeEx(this);
	QString strPath = fileType->GetFilePic(fileName);

	QString strLiID = QString(messageInfo.msgID) + QString("send");
	QString strsize = gSocketMessage->ByteConversion(fileType->GetFileSize(fileName));
	delete fileType;
	strsize = QString("(") + strsize + QString(")");
	QString strend = QString("SendGroupFile(\"%1\",\"%2\",\"%3\",%4,\"%5\",\"%6\",\"%7\",\"%8\")").arg(strHeadPath).arg(FileName).arg(QString(messageInfo.msgID)).arg(0).arg(strPath).arg(strLiID).arg(strsize).arg(QString::number(messageInfo.nFromUserID));
	m_vm->runJs(strend);
	AppConfig conf = gDataManager->getAppConfigInfo();

	FileMessage *fileMessage = new FileMessage;
	fileMessage->nFromUserID = nUserID;
	fileMessage->nToUserID = m_vm->getGroupId().toInt();
	fileMessage->nDeliverType = 0;
	fileMessage->strUpLoadUrl = conf.PanServerUploadURL;
	fileMessage->strDownLoadUrl = conf.PanServerDownloadURL;
	fileMessage->strFilePath = fileName;

	QFile upfile(fileName);
	int nIndex = fileName.lastIndexOf("/");
	QString strFileName = fileName.mid(nIndex + 1);
	nIndex = strFileName.lastIndexOf(".");
	fileMessage->FileName = strFileName;
	fileMessage->FileType = strFileName.mid(nIndex + 1, strFileName.length());
	fileMessage->FileSize = gSocketMessage->ByteConversion(upfile.size());
	HttpNetWork::HttpUpLoadFile *httpUpLoad = new HttpNetWork::HttpUpLoadFile(this);
	m_iHttp++;
	httpUpLoad->setUserData(Qt::UserRole, fileMessage);
	httpUpLoad->setData(QVariant::fromValue(messageInfo));
	connect(httpUpLoad, SIGNAL(sigUpLoadFinished(bool, QByteArray)), this, SLOT(slotUpFileReplyFinished(bool, QByteArray)));
	connect(httpUpLoad, SIGNAL(sigUpLoadProgress(qint64, qint64)), this, SLOT(doUpLoadFileProgress(qint64, qint64)));
	connect(m_dispatcher, SIGNAL(sigCancel(QString)), httpUpLoad, SLOT(slotLoadorDownLoadCancle(QString)));
	QVariantMap pargram;
	pargram.insert("parentId", "66662");
	pargram.insert("createUser", "6662");
	httpUpLoad->StartUpLoadFile(conf.PanServerUploadURL, fileName, pargram);

	//更新至消息列表，true代表部落消息
	QMap<QString, QVariant> params;
	params["SendStatus"] = MESSAGE_STATE_PRESEND;
	params["MsgId"] = QString(messageInfo.msgID);

	//预填充数据库，因为doClickItem会从数据库字段中取出内容进行显示
	GroupInfo group = gDataBaseOpera->DBGetGroupFromID(m_vm->getGroupId());
	MessageListInfo messageListInfo;
	messageListInfo.nBudyyID = group.groupId.toInt();
	messageListInfo.strLastMessage = tr("[File]");
	messageListInfo.nLastTime = QDateTime::currentMSecsSinceEpoch();
	messageListInfo.strBuddyName = group.groupName;
	messageListInfo.strBuddyHeaderImage = group.groupLoacalHeadImage;
	messageListInfo.messageType = MessageType::Message_FILE;
	messageListInfo.nUnReadNum = 0;
	messageListInfo.isGroup = 1;
	gDataBaseOpera->DBInsertGroupMessageListInfo(messageListInfo);
	emit m_dispatcher->sigUpdateSelfMessage(true, QVariant::fromValue(group), tr("[File]"), true, params);
}

void IMGroupChatStore::doUpLoadFileProgress(qint64 sendnum, qint64 total)
{
	HttpNetWork::HttpUpLoadFile *requestHttp = qobject_cast<HttpNetWork::HttpUpLoadFile*>(sender());
	if (requestHttp)
	{
		QVariant var = requestHttp->getData();
		MessageInfo msgInfo = var.value<MessageInfo>();
		if (total > 0)
		{
			int num = 250 * sendnum / total;
			QString processID = msgInfo.msgID + QString("bar");
			QString strend = QString("ProgressBar(\"%1\",%2)").arg(processID).arg(num);
			m_vm->runJs(strend);
		}
		else {
			QString processID = msgInfo.msgID + QString("bar");
			QString strend = QString("ProgressBar(\"%1\",%2)").arg(processID).arg(0);;
			m_vm->runJs(strend);
		}
	}
}

void IMGroupChatStore::slotUpFileReplyFinished(bool bResult, QByteArray result)
{
	m_iHttp--;
	HttpNetWork::HttpUpLoadFile *requestHttp = qobject_cast<HttpNetWork::HttpUpLoadFile*>(sender());
	FileMessage *fileMessage = (FileMessage *)requestHttp->userData(Qt::UserRole);
	QVariant var = requestHttp->getData();
	MessageInfo msgInfo = var.value<MessageInfo>();
	if (bResult && requestHttp)
	{
		QString newLiID = QString(msgInfo.msgID) + QString("send");
		QString changeLi = QString("ChangeLi(\"%1\")").arg(newLiID);

		QString cancleID = msgInfo.msgID + QString("can");
		QString cancle = QString("ChangeLi(\"%1\")").arg(cancleID);
		
		ChangeFileState(msgInfo.msgID, 2);

		gSocketMessage->setUpFileReplyFinished(result, fileMessage, msgInfo);

		GroupInfo group = gDataBaseOpera->DBGetGroupFromID(m_vm->getGroupId());
		MessageListInfo messageListInfo;
		messageListInfo.nBudyyID = group.groupId.toInt();
		messageListInfo.strLastMessage = tr("[File]");
		messageListInfo.nLastTime = msgInfo.ClientTime;
		messageListInfo.strBuddyName = group.groupName;
		messageListInfo.strBuddyHeaderImage = group.groupLoacalHeadImage;
		messageListInfo.messageType = msgInfo.MessageChildType;
		messageListInfo.nUnReadNum = 0;
		messageListInfo.isGroup = 1;
		gDataBaseOpera->DBInsertGroupMessageListInfo(messageListInfo);

		//更新至消息列表，true代表部落消息
		QMap<QString, QVariant> params;
		params["SendStatus"] = MESSAGE_STATE_SEND;
		params["MsgId"] = QString(msgInfo.msgID);
		emit m_dispatcher->sigUpdateSelfMessage(true, QVariant::fromValue(group), messageListInfo.strLastMessage, false, params);
	}
	else
	{
		gSocketMessage->SetMessageState(msgInfo.msgID, MESSAGE_STATE_FAILURE);
		this->UpdateMessageStateInfo(msgInfo.msgID, MESSAGE_STATE_FAILURE, msgInfo.integral);

		QString newLiID = QString(msgInfo.msgID) + QString("send");
		QString changeLi = QString("ChangeLiFailed(\"%1\")").arg(newLiID);
		
		QString cancleID = msgInfo.msgID + QString("can");
		QString cancle = QString("ChangeLiFailed(\"%1\")").arg(cancleID);
		
		ChangeFileState(msgInfo.msgID, 1);
	}
}

void IMGroupChatStore::slotSendNotice(QMap<QString, QString> mapData)
{
	UserInfo userInfo = gDataManager->getUserInfo();
	QString strPath = convertHeaderToHtmlSrc(userInfo.strUserAvatarLocal);

	QString imageUrl = mapData.value("imageUrl");
	QString imageTitle = mapData.value("imageTitle");
	QString webUrl = mapData.value("webUrl");
	QString webTitle = mapData.value("webTitle");
	int nUserID = userInfo.nUserID;

	QFile file(imageUrl);
	if (!file.exists())
	{
		QJsonParseError jsonError;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(imageUrl.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QVariantMap result = jsonDocument.toVariant().toMap();
				imageUrl = result["path"].toString();
			}
		}
	}
	QString strFileName = QString("<img src='") + QString("file:///") + imageUrl + QString("'/>");
	QImage img(imageUrl);

	QString toUserID = m_vm->getGroupId();
	AppConfig configInfo = gDataManager->getAppConfigInfo();
	MessageInfo messageInfo = gSocketMessage->SendNoteiceMessage(nUserID, toUserID.toInt(), 1, "");

	HttpNetWork::HttpUpLoadFile *httpUpLoad = new HttpNetWork::HttpUpLoadFile(this);
	mapData.insert("imageUrl", configInfo.PanServerDownloadURL);

	QString processID = messageInfo.msgID;
	mapData.insert("MsgId", processID);
	httpUpLoad->setData(QVariant::fromValue(mapData));
	connect(httpUpLoad, SIGNAL(sigUpLoadFinished(bool, QByteArray)), this, SLOT(slotUpNoticePicReplyFinished(bool, QByteArray)));
	QVariantMap pargram;
	pargram.insert("parentId", "66662");
	pargram.insert("createUser", "6662");
	httpUpLoad->StartUpLoadFile(configInfo.PanServerUploadURL, imageUrl, pargram);

#ifndef Q_OS_WIN
	imageUrl = QString("file:///") + imageUrl;
#endif

	//插入时间线。
	onInsertTimeLine(GETLOCALTIME);
	QString strSend = QString("SendGroupNotice(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",\"%6\",%7,%8,\"%9\");")
		.arg(strPath).arg(imageUrl).arg(imageTitle).arg(webUrl).arg(webTitle).arg(QString(messageInfo.msgID))
		.arg(messageInfo.MessageState).arg(messageInfo.integral).arg(QString::number(messageInfo.nFromUserID));
	m_vm->runJs(strSend);

	//更新至消息列表，true代表部落消息
	QMap<QString, QVariant> params;
	params["SendStatus"] = MESSAGE_STATE_PRESEND;
	params["MsgId"] = QString(messageInfo.msgID);

	//预填充数据库，因为doClickItem会从数据库字段中取出内容进行显示
	GroupInfo group = gDataBaseOpera->DBGetGroupFromID(m_vm->getGroupId());
	MessageListInfo messageListInfo;
	messageListInfo.nBudyyID = group.groupId.toInt();
	messageListInfo.strLastMessage = tr("[Announcement]");
	messageListInfo.nLastTime = QDateTime::currentMSecsSinceEpoch();
	messageListInfo.strBuddyName = group.groupName;
	messageListInfo.strBuddyHeaderImage = group.groupLoacalHeadImage;
	messageListInfo.messageType = MessageType::Message_NOTICE;
	messageListInfo.nUnReadNum = 0;
	messageListInfo.isGroup = 1;
	gDataBaseOpera->DBInsertGroupMessageListInfo(messageListInfo);
	emit m_dispatcher->sigUpdateSelfMessage(true, QVariant::fromValue(group), tr("[Announcement]"), true, params);
}

/*发送图片 上传完图片 结果*/
void IMGroupChatStore::slotUpNoticePicReplyFinished(bool result, QByteArray strResult)
{
	HttpNetWork::HttpUpLoadFile *act = qobject_cast<HttpNetWork::HttpUpLoadFile*>(sender());
	QVariant var = act->getData();
	QMap<QString, QString> mapData = var.value<QMap<QString, QString>>();
	MessageInfo msgInfo = gSocketMessage->DBGetMessageInfoWithMsgID(mapData.value("MsgId"));

	if (result)
	{
		gSocketMessage->setUpNoticePicReplyFinished(strResult, mapData, msgInfo);
		GroupInfo group = gDataBaseOpera->DBGetGroupFromID(m_vm->getGroupId());
		MessageListInfo messageListInfo;
		messageListInfo.nBudyyID = this->objectName().toInt();
		messageListInfo.strLastMessage = tr("[Announcement]");
		messageListInfo.nLastTime = msgInfo.ClientTime;
		messageListInfo.strBuddyName = group.groupName;
		BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(this->objectName());
		if (buddy.strNickName.isEmpty())
			buddy = gDataBaseOpera->DBGetGroupUserFromID(this->objectName());
		messageListInfo.strBuddyHeaderImage = buddy.strLocalAvatar;
		messageListInfo.messageType = msgInfo.MessageChildType;
		messageListInfo.nUnReadNum = 0;
		messageListInfo.isGroup = 0;
		gDataBaseOpera->DBInsertPerMessageListInfo(messageListInfo);
		//更新至消息列表，false代表个人消息
		QMap<QString, QVariant> params;
		params["SendStatus"] = MESSAGE_STATE_SEND;
		params["MsgId"] = QString(msgInfo.msgID);
		emit m_dispatcher->sigUpdateSelfMessage(true, QVariant::fromValue(group), messageListInfo.strLastMessage, true, params);
	}
	else
	{
		this->UpdateMessageStateInfo(msgInfo.msgID, MESSAGE_STATE_FAILURE, msgInfo.integral);
	}
}

void IMGroupChatStore::slotZoomImg(QString strId)
{
	MessageInfo msgInfo = gSocketMessage->DBGetMessageInfoWithMsgID(strId);
	if (msgInfo.MessageState == MESSAGE_STATE_UNLOADED || msgInfo.MessageChildType != Message_PIC)
	{
		return;
	}
	QFile file(msgInfo.strMsg);
	if (!file.exists())
	{
		QJsonParseError jsonError;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QVariantMap result = jsonDocument.toVariant().toMap();
				qDebug() << "img:" << result["path"].toString();
				msgInfo.strMsg = result["path"].toString();
			}
		}
	}
	emit m_dispatcher->sigOpenPic(msgInfo.strMsg, &m_listPic, m_view);
}


void  IMGroupChatStore::slotDownLoadFileProgress(qint64 recvnum, qint64 total)
{
	HttpNetWork::HttpDownLoadFile *act = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());

	if (act)
	{
		QVariant var = act->getData();
		MessageInfo msgInfo = var.value<MessageInfo>();
		if (total > 0)
		{
			int num = recvnum * 250 / total;
			QString barId = msgInfo.msgID + "bar";
			QString strSend = QString("ProgressBar(\"%1\",%2)").arg(barId).arg(num);

			m_vm->runJs(strSend);

		}
		else {
			QString barId = msgInfo.msgID + "bar";
			QString strSend = QString("ProgressBar(\"%1\",%2)").arg(barId).arg(0);
			m_vm->runJs(strSend);
		}
	}
}

void IMGroupChatStore::slotDownFailed()
{
	m_iHttp--;
	HttpNetWork::HttpDownLoadFile *act = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());
	if (act)
	{
		QVariant var = act->getData();
		MessageInfo msgInfo = var.value<MessageInfo>();
	}
}

void IMGroupChatStore::slotSaveFile(QString msgID)
{
	const QString DEFAULT_DIR_KEY("default_dir");
	QSettings MySettings;
	QString default_dir = MySettings.value(DEFAULT_DIR_KEY).toString();

	MessageInfo messageInfo = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);
	QJsonDocument jsonDocument = QJsonDocument::fromJson(messageInfo.strMsg.toUtf8());
	QVariantMap result = jsonDocument.toVariant().toMap();
	QString fileName = result["FileName"].toString();
	QString fileID = result["FileId"].toString();
	QString fileExt = result["FileType"].toString();

	QString strFilePath = QFileDialog::getSaveFileName(m_view, tr("Save as"), (default_dir.isEmpty() ? "" : default_dir + "/") + fileName);
	if (strFilePath == "")
	{
		return;
	}

	if (QFileInfo(strFilePath).suffix().toLower() != fileExt.toLower())
	{
		//无后缀，自动添加后缀
		strFilePath += "." + fileExt;
	}

	QFileInfo fileInfo(strFilePath);
	MySettings.setValue(DEFAULT_DIR_KEY, fileInfo.path());

	QString strUlID2 = QString(msgID) + QString("recv2");

	QString strUlID = QString(msgID) + QString("recv");
	QString coverFile = QString("coverFile(\"%1\")").arg(strUlID);

	ChangeFileState(messageInfo.msgID, 1);


	AppConfig configInfo = gDataManager->getAppConfigInfo();
	QString httpUrl = QString(configInfo.PanServerDownloadURL) + fileID + QString("/download");

	HttpNetWork::HttpDownLoadFile *netWork = new HttpNetWork::HttpDownLoadFile;
	m_iHttp++;
	netWork->setData(QVariant::fromValue(messageInfo));
	netWork->setObjectName(strFilePath);
	connect(netWork, SIGNAL(sigDownFinished(bool)), this, SLOT(slotRequestHttpFileResult(bool)));
	connect(netWork, SIGNAL(sigDownloadProgress(qint64, qint64)), this, SLOT(slotDownLoadFileProgress(qint64, qint64)));
	connect(m_dispatcher, SIGNAL(sigCancel(QString)), netWork, SLOT(slotLoadorDownLoadCancle(QString)));
	connect(netWork, SIGNAL(sigDownFailed()), this, SLOT(slotDownFailed()));
	netWork->StartDownLoadFile(httpUrl, strFilePath);
}

void IMGroupChatStore::slotDrags(QStringList list)
{
	foreach(QString file, list)
	{
		file = file.replace("\\", "/");
		this->slotSendFile(file);
	}
}

void IMGroupChatStore::slotLoadMore()
{
	InitMessageInfo(this->m_foremostRowId);
}

void IMGroupChatStore::slotTipMessage(int type, QString recvID, QString strMessage)
{
	if (gSocketMessage)
	{
		UserInfo userInfo = gDataManager->getUserInfo();
		int nUserID = userInfo.nUserID;

		MessageInfo msgInfo = gSocketMessage->SendTextMessage(nUserID, m_vm->getGroupId().toInt(), 1, (short)MessageType::Message_NOTIFY, strMessage);

		QJsonDocument doc = QJsonDocument::fromJson(strMessage.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		if (map.value("type").toString() == "notification")
		{
			//strMessage = map.value("content").toString();
			QString inviterID = map.value("inviterID").toString();
			QString invitedID = map.value("invitedID").toString();
			//若为移动端等发来的消息，则不作处理
			if ((inviterID == "0" || inviterID == NULL || inviterID == "") && (invitedID == "0" || invitedID == NULL || invitedID == ""))
				strMessage = map.value("content").toString();
			else if (inviterID == "0" || inviterID == NULL || inviterID == "")
				strMessage = invitedID + tr(" joined the group");
			else
				strMessage = inviterID + tr(" invited ") + invitedID + tr(" to join the group");
		}
		if (map.value("type").toString() == "receivedFile")
		{
			strMessage = tr("Received the file successfully!");
		}

		QString strSend = QString("tipMessage(\"%1\");").arg(strMessage);
		m_vm->runJs(strSend);

		GroupInfo group = gDataBaseOpera->DBGetGroupFromID(m_vm->getGroupId());
		MessageListInfo messageListInfo;
		messageListInfo.nBudyyID = group.groupId.toInt();
		messageListInfo.strLastMessage = strMessage;
		messageListInfo.nLastTime = msgInfo.ClientTime;
		messageListInfo.strBuddyName = group.groupName;
		messageListInfo.strBuddyHeaderImage = group.groupLoacalHeadImage;
		messageListInfo.messageType = msgInfo.MessageChildType;
		messageListInfo.nUnReadNum = 0;
		messageListInfo.isGroup = 1;
		gDataBaseOpera->DBInsertGroupMessageListInfo(messageListInfo);

		//更新至消息列表，true代表部落消息
		QMap<QString, QVariant> params;
		emit m_dispatcher->sigUpdateSelfMessage(true, QVariant::fromValue(group), strMessage, true, params);
	}
}
