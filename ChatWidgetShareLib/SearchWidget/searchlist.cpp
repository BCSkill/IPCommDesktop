﻿#include "searchlist.h"
#include "ui_searchlist.h"
#include "QStringLiteralBak.h"
#include "stdafx.h"
#ifdef Q_OS_MAC
#include "mycommonstyle.h"
#endif

SearchList::SearchList(QWidget *parent)
	: QListWidget(parent)
{
	ui = new Ui::SearchList();
	ui->setupUi(this);

	connect(this, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(slotDoubleClickedItem(QListWidgetItem *)));

	// 给滚动条改变样式
	QFile scroolbar_style_qss(":/QSS/Resources/QSS/scrollbarStyle.qss");
	scroolbar_style_qss.open(QFile::ReadOnly);
	this->verticalScrollBar()->setStyleSheet(scroolbar_style_qss.readAll());
	this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	this->setMouseTracking(true);

	//加载样式
	QFile file(":/QSS/Resources/QSS/listWidgetStyle.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

#ifdef Q_OS_MAC
	this->setStyle(new MyProxyStyle);
#endif
}

SearchList::~SearchList()
{
	delete ui;
}

void SearchList::keyPressEvent(QKeyEvent * event)
{
	if (event->key() == Qt::Key_Up)
	{
		if (this->count() > 0)
		{
			QListWidgetItem *item = NULL;
			for (int i = this->count() - 1; i >= 0; i--)
			{
				if (this->currentRow() < 0 && this->item(i)->flags() != Qt::NoItemFlags)
				{
					item = this->item(i);
					break;
				}

				if (this->currentRow() > i && this->item(i)->flags() != Qt::NoItemFlags)
				{
					item = this->item(i);
					break;
				}
			}
			if (item)
			{
				item->setSelected(true);
				this->setCurrentItem(item);
			}
		}
	}
	if (event->key() == Qt::Key_Down)
	{
		if (this->count() > 0)
		{
			QListWidgetItem *item = NULL;
			for (int i = 0; i < this->count(); i++)
			{
				if (this->currentRow() < 0 && this->item(i)->flags() != Qt::NoItemFlags)
				{
					item = this->item(i);
					break;
				}

				if (this->currentRow() < i && this->item(i)->flags() != Qt::NoItemFlags)
				{
					item = this->item(i);
					break;
				}
			}
			if (item)
			{
				item->setSelected(true);
				this->setCurrentItem(item);
			}
		}
	}

	if (event->key() == Qt::Key_Return)
	{
		QListWidgetItem *item = this->currentItem();
		if (item)
			this->slotDoubleClickedItem(item);
	}

	return QWidget::keyPressEvent(event);
}

void SearchList::enterEvent(QEvent * event)
{
	this->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
}

void SearchList::leaveEvent(QEvent * event)
{
	this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

void SearchList::slotDoubleClickedItem(QListWidgetItem *item)
{
	QString type = item->data(Qt::UserRole).toString();
	if (type == tr("Friends"))
	{
		CFrientItemBuddy *buddy = (CFrientItemBuddy*)this->itemWidget(item);
		if (buddy != NULL)
		{
			QString buddyID = buddy->mNickName->objectName();
			emit sigOpenChat(OpenPer, QVariant(buddyID));
		}
	}
	if (type == tr("Group"))
	{
		CFrientItemGroup *group = (CFrientItemGroup*)this->itemWidget(item);
		if (group != NULL)
		{
			QString groupID = group->mNickName->objectName();
			emit sigOpenChat(OpenGroup, QVariant(groupID));
		}
	}
}

void SearchList::onInsertContacts(QList<BuddyInfo> buddyList, QList<GroupInfo> groupList ,QList<BuddyInfo> GroupbuddyList)
{
	this->clear();

	if (!buddyList.isEmpty())
	{
		//先插入字母。
		QListWidgetItem *newItem = new QListWidgetItem(tr("Friends"));    //创建一个Item
		newItem->setSizeHint(QSize(this->width(), 25));//设置宽度、高度
		newItem->setFlags(Qt::NoItemFlags);
		newItem->setData(Qt::UserRole, tr("Friends"));
		this->addItem(newItem);

		foreach(BuddyInfo buddy, buddyList)
		{
			CFrientItemBuddy *buddyWidget = new CFrientItemBuddy(this);
			buddyWidget->resize(this->width(), 62);
			buddyWidget->OnInitContacts(QString::number(buddy.nUserId));
			buddyWidget->OnSetPicPath(buddy.strLocalAvatar);
			if (buddy.strNote.isEmpty())
				buddyWidget->OnSetNickNameText(buddy.strNickName);
			else
				buddyWidget->OnSetNickNameText(buddy.strNote);
			buddyWidget->OnSetAutoGrapthText(buddy.strSign);
			QListWidgetItem *buddyItem = new QListWidgetItem(); //创建一个newItem
			//newItem->setSizeHint(QSize(this->width(),nHeight));
			buddyItem->setSizeHint(QSize(this->width(), 58));
			buddyItem->setData(Qt::UserRole, tr("Friends"));

			this->addItem(buddyItem);
			this->setItemWidget(buddyItem, buddyWidget); //将buddy赋给该newItem

			//“Friends”项和第一项
			if (this->count() == 2)
			{
				buddyItem->setSelected(true);
				this->setCurrentItem(buddyItem);
			}
				
		}
	}

	if (!groupList.isEmpty())
	{
		//先插入字母。
		QListWidgetItem *newItem = new QListWidgetItem(tr("Group"));    //创建一个Item
		newItem->setSizeHint(QSize(this->width(), 25));//设置宽度、高度
		newItem->setFlags(Qt::NoItemFlags);
		newItem->setData(Qt::UserRole, tr("Group"));
		this->addItem(newItem);

		foreach(GroupInfo group, groupList)
		{
			CFrientItemGroup *groupWidget = new CFrientItemGroup(this);
			groupWidget->resize(this->width(), 62);
			groupWidget->OnInitGroup(group.groupId);
			groupWidget->OnSetPicPath(group.groupLoacalHeadImage);
			groupWidget->OnSetNickNameText(group.groupName);
			QListWidgetItem *groupItem = new QListWidgetItem(); //创建一个newItem
			//newItem->setSizeHint(QSize(this->width(),nHeight));
			groupItem->setSizeHint(QSize(this->width(), 58));
			groupItem->setData(Qt::UserRole, tr("Group"));

			this->addItem(groupItem);
			this->setItemWidget(groupItem, groupWidget); //将buddy赋给该newItem

			if (this->count() == 2)
			{
				groupItem->setSelected(true);
				this->setCurrentItem(groupItem);
			}	
		}
	}

	if (!GroupbuddyList.isEmpty())
	{
		int i = 1;
		foreach(BuddyInfo buddy, GroupbuddyList)
		{
			bool bIsBuddy = gDataBaseOpera->DBJudgeBuddyIsHaveByID(QString::number(buddy.nUserId));
			if (bIsBuddy)
				break;
			if (i == 1)
			{
				//先插入字母。
				QListWidgetItem *newItem = new QListWidgetItem(tr("other friends"));    //创建一个Item
				newItem->setSizeHint(QSize(this->width(), 25));//设置宽度、高度
				newItem->setFlags(Qt::NoItemFlags);
				newItem->setData(Qt::UserRole, tr("Friends"));
				this->addItem(newItem);
			}
			if (i > 40)
			{//暂时限制为加载40个
				break;
			}
			i++;
			CFrientItemBuddy *buddyWidget = new CFrientItemBuddy(this);
			buddyWidget->resize(this->width(), 62);
			buddyWidget->OnInitContacts(QString::number(buddy.nUserId));
			buddyWidget->OnSetPicPath(buddy.strLocalAvatar);
			if (buddy.strNote.isEmpty())
				buddyWidget->OnSetNickNameText(buddy.strNickName);
			else
				buddyWidget->OnSetNickNameText(buddy.strNote);
			buddyWidget->OnSetAutoGrapthText(buddy.strSign);
			QListWidgetItem *buddyItem = new QListWidgetItem(); //创建一个newItem
																//newItem->setSizeHint(QSize(this->width(),nHeight));
			buddyItem->setSizeHint(QSize(this->width(), 58));
			buddyItem->setData(Qt::UserRole, tr("Friends"));

			this->addItem(buddyItem);
			this->setItemWidget(buddyItem, buddyWidget); //将buddy赋给该newItem

														 //“Friends”项和第一项
			if (this->count() == 2)
			{
				buddyItem->setSelected(true);
				this->setCurrentItem(buddyItem);
			}
		}
	}
}

void SearchList::onChangeItem(QKeyEvent *event)
{
	this->keyPressEvent(event);
}
