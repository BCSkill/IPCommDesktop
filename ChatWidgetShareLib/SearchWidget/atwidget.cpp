﻿#include "atwidget.h"
#include "ui_atwidget.h"
#include "QStringLiteralBak.h"

extern QString gThemeStyle;

AtWidget::AtWidget(QWidget *parent)
	: QListWidget(parent)
{
	ui = new Ui::AtWidget();
	ui->setupUi(this);

    this->mouseIn = false;

    connect(this, SIGNAL(itemActivated(QListWidgetItem *)), this, SLOT(slotClickedItem(QListWidgetItem *)));
	connect(this, SIGNAL(itemClicked(QListWidgetItem *)), this, SLOT(slotClickedItem(QListWidgetItem *)));
#ifdef Q_OS_WIN
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::Tool | Qt::WindowStaysOnTopHint);		// 去掉标题栏 且不能移动
#else
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);		// 去掉标题栏 且不能移动
#endif

	this->resize(100, 142);
    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	QFile style(":/QSS/Resources/QSS/ChatWidgetShareLib/atwidget.qss");
	style.open(QFile::ReadOnly);
	QString sheet = QLatin1String(style.readAll());
	this->setStyleSheet(sheet);
	style.close();
}

AtWidget::~AtWidget()
{
	delete ui;
}

bool AtWidget::isFocusIn()
{
	return this->mouseIn;
}

void AtWidget::setGroupID(QString groupID)
{
	this->currentID = groupID;
}

bool AtWidget::isAtGroupBuddyFormat(QString imageFormatName)
{
	if (imageFormatName.startsWith(QDir::tempPath() + "/at"))
	    return true;
	else
	{
		return false;
	}
}

bool AtWidget::isAtGroupChating()
{
	if (atContent.isEmpty())
		return false;
	else
		return true;
}

void AtWidget::addAtGroupBuddy(QString imageFormatName)
{
	//获取要at的ID
	QFileInfo imageInfo(imageFormatName);
	QString name = imageInfo.baseName().remove("at");
	int buddyID = name.toInt();

	//拼接@的字符串
	QString text = "@";
	BuddyInfo buddy = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(currentID, name);
	if (buddy.strNote.isEmpty())
		text.append(buddy.strNickName);
	else
		text.append(buddy.strNote);

	//获取字符串的起止位置信息，拼接json要用。
	int start = atContent.length();
	int length = text.length();

	//追加content。
	atContent.append(text);
	//追加at数据的list。
	QVariantMap atData;
	atData.insert("userId", buddyID);
	atData.insert("text", text);
	atData.insert("start", start);
	atData.insert("length", length);
	atList.append(atData);
}

void AtWidget::addAtChatText(QString chatText)
{
	atContent.append(" " + chatText);
}

void AtWidget::sendAtMessage()
{
	if (atContent.isEmpty())
	{

	}
	else
	{
		QVariantMap map;
		map.insert("type", "at");
		map.insert("content", atContent);
		map.insert("ID", atList);
		QJsonDocument doc = QJsonDocument::fromVariant(map);
		QString message = doc.toJson();

		emit sigSendAtMessage(atContent, message);
		atContent.clear();
		atList.clear();
	}
}

void AtWidget::keyEvent(QKeyEvent *event)
{
	this->keyPressEvent(event);
}

void AtWidget::enterEvent(QEvent * event)
{
	this->mouseIn = true;
	return QWidget::enterEvent(event);
}

void AtWidget::leaveEvent(QEvent * event)
{
	this->mouseIn = false;
	return QWidget::leaveEvent(event);
}

void AtWidget::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Return && this->isVisible() )
    {
        slotClickedItem(this->currentItem());
    }
    if(event->key() == Qt::Key_Up || event->key() == Qt::Key_Down)
    {
        return QListWidget::keyPressEvent(event);
    }
    //return QListWidget::keyPressEvent(event);
}

void AtWidget::search(QString key)
{
	this->clear();

	QList<BuddyInfo> allList = gDataBaseOpera->DBGetGroupBuddyInfoFromID(currentID);
	QList<BuddyInfo> buddyList;
	if (key.isEmpty())
	{
		for (int i = 0; i < allList.count() && i < 5; i++)
		{
			buddyList.append(allList.at(i));
		}
	}
	else
	{
		key = key.toLower();
		foreach(BuddyInfo buddy, allList)
		{
			PinYin pinyin;
			if (!buddy.strNote.isEmpty())
			{
				pinyin = getPinYin(buddy.strNote);
				if (buddy.strNote.toLower().contains(key) || pinyin.fullPinYin.contains(key) || pinyin.easyPinYin.contains(key))
				{
					buddyList.append(buddy);
					continue;
				}
			}

			if (!buddy.strNickName.isEmpty())
			{
				pinyin = getPinYin(buddy.strNickName);
				if (buddy.strNickName.toLower().contains(key) || pinyin.fullPinYin.contains(key) || pinyin.easyPinYin.contains(key))
				{
					buddyList.append(buddy);
					continue;
				}
			}
		}
	}
	
	for (int i = 0; i < buddyList.count(); i++)
	{
		BuddyInfo buddyInfo = buddyList.at(i);

		if (buddyInfo.nUserId == gDataManager->getUserInfo().nUserID)
			continue;

		CFrientStyleWidget *buddy = new CFrientStyleWidget();
		buddy->onInitGroupAtList(QString::number(buddyInfo.nUserId));
		buddy->OnSetPicPath(buddyInfo.strLocalAvatar);
		if (buddyInfo.strNote.isEmpty())
			buddy->OnSetNickNameText(buddyInfo.strNickName);
		else
			buddy->OnSetNickNameText(buddyInfo.strNote);
		QListWidgetItem *newItem = new QListWidgetItem(); //创建一个newItem
		newItem->setSizeHint(QSize(this->width() - 4, 28));
		this->addItem(newItem);
		this->setItemWidget(newItem, buddy); //将buddy赋给该newItem

		if (this->count() == 1)
		{
			newItem->setSelected(true);
			this->setCurrentItem(newItem);
		}
	}

	if (this->count() == 0)
		this->hide();
	else
	{
		int oldHeight = this->height();
		int curHeight;
		if (this->count() > 5)
			curHeight = 142;
		else
			curHeight = this->count() * 28 + 2;

		int diff = curHeight - oldHeight;
		this->resize(this->width(), curHeight);
		this->move(this->x(), this->y() - diff);
	}
}

void AtWidget::slotClickedItem(QListWidgetItem *item)
{
	CFrientStyleWidget *buddy = (CFrientStyleWidget*)this->itemWidget(item);
	if (buddy == NULL)
	{
		qDebug() << "AtWidget::slotClickedItem空指针";
		return;
	}
	QString nickName = buddy->mNickName->text();
	QString strUserID = buddy->mNickName->objectName();

	//拼接字符串
	QString text = tr("@") + nickName;

	//设置字体
	QFont font;
	font.setFamily("Microsoft YaHei");
	font.setPointSize(12);

	//获取绘制字体的像素尺寸
	QFontMetrics fm(font);
	QRect rect = fm.boundingRect(text);
	rect.setHeight(rect.height() - 2);

	//设置画布大小，宽度留10像素的空白
	QPixmap atImage(rect.width()+ 10, rect.height());
	atImage.fill(QColor(0, 0, 0, 0));

	//设置画笔
	QPainter *painter = new QPainter;
	painter->begin(&atImage);
	if (gThemeStyle == "Blue")
		painter->setPen(QColor(Qt::white));
	if (gThemeStyle == "White")
		painter->setPen(QColor(16, 142, 233));

	painter->setFont(font);
	
	//绘制文字
	painter->drawText(QRect(QPoint(0,0), rect.size()),
		Qt::AlignLeft|Qt::AlignTop, text);
	painter->end();

	//保存图片
	QString path = QDir::tempPath() + "/at" + strUserID + ".png";
	atImage.save(path);

	emit sigAtGroupUser(path);
	this->hide();
}
