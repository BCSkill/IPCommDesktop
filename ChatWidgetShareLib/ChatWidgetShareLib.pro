# ----------------------------------------------------
# This file is generated by the Qt Visual Studio Tools.
# ------------------------------------------------------

QT += core xml sql network gui widgets webenginewidgets quick quickwidgets quickcontrols2 multimediawidgets multimedia

INCLUDEPATH += ../SharedLib_mac/openssl/include \
    ../SharedLib_mac/common \
    ../SharedLib_mac/AlphabeticalSortSharedLib/include \
    ../SharedLib_mac/SqlLiteShareLib/include \
    ../SharedLib_mac/IMDataManagerShareLib/include \
    ../SharedLib_mac/IMSocketNetWorkShareLib/include \
    ../SharedLib_mac/SocketNetWorkShareLib/include \
    ../SharedLib_mac/IMSocketDataBaseShareLib/include \
    ../SharedLib_mac/IMDataBaseOperaInfo/include \
    ../SharedLib_mac/HttpNetWorkShareLib/include \
    ../SharedLib_mac/OPDataManager/include \
    ../SharedLib_mac/BaseUI/include \
    ../SharedLib_mac/OPDateBaseShareLib/include \
    ../SharedLib_mac/OPRequestShareLib/include \
    ../SharedLib_mac/IMDownLoadHeaderImg/include \
    ../SharedLib_mac/IMRequestBuddyInfo/include \
    ../SharedLib_mac/AlphabeticalSortinclude/include \
    ../SharedLib_mac/VideoPlayShareLib/include \
    ../SharedLib_mac/ContactsProfileShareLib/include \
    ../SharedLib_mac/botan/include \
    ../SharedLib_mac/IMDownLoadHeaderImg/include \
    ../SharedLib_mac/IMRequestBuddyInfo/include \
    ../SharedLib_mac/WebObjectShareLib/include \
    ../SharedLib_mac/ScreenCutShareLib/include \
    ./../SharedLib_mac/QRenCodeShareLib/include \
    ./../SharedLib_mac/libqrencode/include \
    ./../SharedLib_mac/QxtGlobalShortCut/include \
    ./../SharedLib_mac/ewalletShareLib/include \
    ./../SharedLib_mac/SettingsManagerShareLib/include \
    ../SharedLib_mac/OPMainMangerShareLib/include \
    ../SharedLib_mac/OPMainWidget/include \
    ../SharedLib_mac/ContactsWidget/include


include(ChatWidgetShareLib.pri)
include(shared.pri)

FORMS += \
    groupfilewidget.ui \
    qwebengineviewdelegate.ui

HEADERS += \
    groupfilewidget.h \
    imperchatdispatcher.h \
    imperchatstore.h \
    imperchatview.h \
    imperchatviewmodel.h \
    qwebenginepagedelegate.h \
    qwebengineviewdelegate.h \
    qwebengineviewmanager.h \
    imgroupchatdispatcher.h \
    imgroupchatstore.h \
    imgroupchatview.h \
    imgroupchatviewmodel.h \
    imgroupdispatcher.h \
    imgroupstore.h \
    imgroupview.h \
    imgroupviewmodel.h \
    childWidget/filetypeex.h

SOURCES += \
    groupfilewidget.cpp \
    imperchatdispatcher.cpp \
    imperchatstore.cpp \
    imperchatview.cpp \
    imperchatviewmodel.cpp \
    qwebenginepagedelegate.cpp \
    qwebengineviewdelegate.cpp \
    qwebengineviewmanager.cpp \
    imgroupchatdispatcher.cpp \
    imgroupchatstore.cpp \
    imgroupchatview.cpp \
    imgroupchatviewmodel.cpp \
    imgroupdispatcher.cpp \
    imgroupstore.cpp \
    imgroupview.cpp \
    imgroupviewmodel.cpp \
    childWidget/filetypeex.cpp
