﻿#include "noticewidget.h"
#include "ui_noticewidget.h"
#include <QLineEdit>
#include "QStringLiteralBak.h"
#include <QFileDialog>
#include <QMouseEvent>
#include <QMovie>
#include "messagebox.h"
#include <QDesktopWidget>
NoticeWidget::NoticeWidget(QString strUserId,QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::NoticeWidget();
	ui->setupUi(this);
	m_strPath = "";

	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog | Qt::Tool);
	setWindowModality(Qt::WindowModal);
	setAttribute(Qt::WA_DeleteOnClose);
	setAttribute(Qt::WA_TranslucentBackground);
	ui->urltitleedit->setContextMenuPolicy(Qt::NoContextMenu);
	ui->urledit->setContextMenuPolicy(Qt::NoContextMenu);
	
	m_pNameLabel = new QLineEdit(this);
	m_pNameLabel->setContextMenuPolicy(Qt::NoContextMenu);
	m_pNameLabel->resize(280,30);
	m_pNameLabel->move(10,150);
	m_pNameLabel->setStyleSheet("background-color:rgba(100,100,100,80);border:none;color:white;padding-left:10px;");
	m_pNameLabel->setPlaceholderText(tr("Please enter the title of the image"));
	QFont font1(tr("微软雅黑"), 10);
	m_pNameLabel->setFont(font1);
	ui->urltitleedit->setFont(font1);
	ui->urllabel->setFont(font1);
	ui->urledit->setFont(font1);
	QFont font2(tr("微软雅黑"), 12);
	ui->piclabel->setFont(font2);
	//ui->piclabel->setContentsMargins(0, 0, 0, 15);
	ui->piclabel->installEventFilter(this);//安装事件过滤器

	QFile file(":/QSS/Resources/QSS/ChatWidgetShareLib/noticewidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->closeBtn, SIGNAL(clicked(bool)), this, SLOT(close()));
	connect(ui->sendBtn, SIGNAL(clicked(bool)), this, SLOT(slotSend()));

#ifdef Q_OS_LINUX
setLinuxCenter();
#endif
}
NoticeWidget::~NoticeWidget()
{
	delete ui;
}

void NoticeWidget::show()
{
	QWidget::show();
	this->raise();
	this->activateWindow();

	m_pNameLabel->setFocus();
}

bool NoticeWidget::eventFilter(QObject *obj, QEvent *event)
{
	if (obj == ui->piclabel)//指定某个QLabel
	{
		if (event->type() == QEvent::MouseButtonPress)//mouse button pressed
		{
			QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
			if (mouseEvent->button() == Qt::LeftButton)
			{
				QString fileName = QFileDialog::getOpenFileName(this,
					tr("Open"), ".",
					tr("Image File") + QStringLiteral("(*.bmp; *.jpeg; *.jpg; *.png; *.gif)"));
				if (!fileName.isEmpty())
				{
					m_strPath = fileName;
					QPixmap pix(fileName);
					int with = ui->piclabel->width();
					int height = ui->piclabel->height();
					int pixW = pix.width();
					int pixH = pix.height();
					//int iValue = pixW / with < pixH / height ? with : height;
					QPixmap pixs = pix.scaled(with, with, Qt::KeepAspectRatio, Qt::SmoothTransformation);
					ui->piclabel->setPixmap(pixs);
					return true;
				}
				return false;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	else
	{
		return NoticeWidget::eventFilter(obj, event);
	}
}

//鼠标事件的处理。
void NoticeWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void NoticeWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void NoticeWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}
void NoticeWidget::slotSend()
{
	//检查为空
	if (m_strPath == "")
	{
		IMessageBox::tip(this,tr("Notice"),tr("Please Choose Image!"));
		return;
	}
	QString strPicName = m_pNameLabel->text();
	QString strUrlTitle = ui->urltitleedit->text();
	QString strUrl = ui->urledit->text();
	if (strPicName == "")
	{
		IMessageBox::tip(this, tr("Notice"),tr("Please enter the title of the image！"));
		return;
	}
	if (strUrlTitle == "")
	{
		IMessageBox::tip(this, tr("Notice"),tr("Please enter the title of web page!"));
		return;
	}
	if (strUrl == "")
	{
		IMessageBox::tip(this, tr("Notice"),tr("Please enter the URL！"));
		return;
	}
	QMap<QString, QString> mapData;
	mapData.insert("imageUrl", m_strPath);
	mapData.insert("imageTitle", strPicName);
	mapData.insert("webTitle", strUrlTitle);
	mapData.insert("webUrl", strUrl);
	emit sigSendNotice(mapData);
	this->close();
}

void NoticeWidget::setLinuxCenter()
{
    window()->setGeometry
    (
            QStyle::alignedRect
            (
                    Qt::LeftToRight,
                    Qt::AlignCenter,
                    window()->size(),
                    qApp->desktop()->availableGeometry()
            )
    );
}
