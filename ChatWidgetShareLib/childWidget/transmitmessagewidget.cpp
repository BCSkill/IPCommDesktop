﻿#include "transmitmessagewidget.h"
#include "ui_transmitmessagewidget.h"
#include "QStringLiteralBak.h"
#ifndef Q_OS_WIN
#include "mycommonstyle.h"
#endif


#ifdef Q_OS_LINUX
    #include <qdesktopwidget.h>
#endif

TransmitMessageWidget::TransmitMessageWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::TransmitMessageWidget();
	ui->setupUi(this);

#ifdef Q_OS_WIN
	shadow = new Shadow();
#endif
	setWindowFlags(Qt::FramelessWindowHint);
	this->setAttribute(Qt::WA_DeleteOnClose);			// 设置在关闭该窗口时 直接 delete该窗口的对象 即 close()走析构函数
	setWindowIcon(QIcon(":/Login/Resources/login/system.ico"));

	QFile file(":/QSS/Resources/QSS/ChatWidgetShareLib/transmitmessagewidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	ui->searchLineEdit->installEventFilter(this);

	connect(ui->closeBtn, SIGNAL(clicked()), this, SLOT(close()));
	connect(ui->cancelBtn, SIGNAL(clicked()), this, SLOT(close()));
	
	connect(ui->enterBtn, SIGNAL(clicked()), this, SLOT(slotEnter()));

	connect(ui->buddyBtn, SIGNAL(clicked()), this, SLOT(slotSwitchBuddyTab()));
	connect(ui->groupBtn, SIGNAL(clicked()), this, SLOT(slotSwitchGroupTab()));

	connect(ui->messageList, SIGNAL(itemClicked(QListWidgetItem *)), this, SLOT(slotClickMessageItem(QListWidgetItem *)));
	connect(ui->buddyList, SIGNAL(itemClicked(QListWidgetItem *)), this, SLOT(slotClickBuddyItem(QListWidgetItem *)));
	connect(ui->groupList, SIGNAL(itemClicked(QListWidgetItem *)), this, SLOT(slotClickGroupItem(QListWidgetItem *)));
	connect(ui->searchList, SIGNAL(itemClicked(QListWidgetItem *)), this, SLOT(slotClickSearchItem(QListWidgetItem *)));

	connect(ui->messageList, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(slotDBClickMessageItem(QListWidgetItem *)));
	connect(ui->buddyList, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(slotDBClickBuddyItem(QListWidgetItem *)));
	connect(ui->groupList, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(slotDBClickGroupItem(QListWidgetItem *)));
	connect(ui->searchList, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(slotDBClickSearchItem(QListWidgetItem *)));

	connect(ui->searchLineEdit, SIGNAL(textEdited(QString)), this, SLOT(slotEditSearchLine(QString)));

	ui->groupList->hide();
	ui->searchList->hide();

#ifndef Q_OS_WIN
    ui->messageList->setStyle(new MyProxyStyle);
    ui->buddyList->setStyle(new MyProxyStyle);
    ui->groupList->setStyle(new MyProxyStyle);
    ui->searchList->setStyle(new MyProxyStyle);
#endif

#ifdef Q_OS_LINUX
	setLinuxCenter();
#endif
}

TransmitMessageWidget::~TransmitMessageWidget()
{
	delete ui;

#ifdef Q_OS_WIN
	if (shadow)
		delete shadow;
#endif
}

void TransmitMessageWidget::show()
{
	QWidget::show();
#ifdef Q_OS_WIN
	shadow->resize(width() + 20, height() + 20);
	shadow->move(x() - 10, y() - 10);
	shadow->show();
	// 窗口被激活了，强制重绘  
	InvalidateRect((HWND)this->winId(), nullptr, FALSE);
#endif
}

void TransmitMessageWidget::changeEvent(QEvent * event)
{
#ifdef Q_OS_WIN
	if (event->type() == QEvent::WindowStateChange)
	{
		if (this->windowState() == Qt::WindowMaximized)
			shadow->hide();
		if (this->windowState() == Qt::WindowMinimized)
			shadow->hide();

		if (this->windowState() == Qt::WindowNoState)
			shadow->show();
	}
#endif
	QWidget::changeEvent(event);
}


void TransmitMessageWidget::moveEvent(QMoveEvent *event)
{
#ifdef Q_OS_WIN
	shadow->move(event->pos().x() - 10, event->pos().y() - 10);
#endif
}

void TransmitMessageWidget::resizeEvent(QResizeEvent* event)
{
#ifdef Q_OS_WIN
	shadow->resize(event->size().width() + 20, event->size().height() + 20);
#endif
}

//鼠标事件的处理。
void TransmitMessageWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void TransmitMessageWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void TransmitMessageWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

bool TransmitMessageWidget::eventFilter(QObject *obj, QEvent *e)
{
	if (obj == ui->searchLineEdit && e->type() == QEvent::KeyPress)
	{
		QKeyEvent *event = (QKeyEvent *)e;
		if (event->key() == Qt::Key_Up)
		{
			if (ui->searchList->count() > 0)
			{
				QListWidgetItem *item = NULL;
				for (int i = ui->searchList->count() - 1; i >= 0; i--)
				{
					if (ui->searchList->currentRow() < 0)
					{
						item = ui->searchList->item(i);
						break;
					}

					if (ui->searchList->currentRow() > i)
					{
						item = ui->searchList->item(i);
						break;
					}
				}
				if (item)
				{
					item->setSelected(true);
					ui->searchList->setCurrentItem(item);
					slotClickSearchItem(item);
				}
			}
		}
		if (event->key() == Qt::Key_Down)
		{
			if (ui->searchList->count() > 0)
			{
				QListWidgetItem *item = NULL;
				for (int i = 0; i < ui->searchList->count(); i++)
				{
					if (ui->searchList->currentRow() < 0)
					{
						item = ui->searchList->item(i);
						break;
					}

					if (ui->searchList->currentRow() < i)
					{
						item = ui->searchList->item(i);
						break;
					}
				}
				if (item)
				{
					item->setSelected(true);
					ui->searchList->setCurrentItem(item);
					slotClickSearchItem(item);
				}
			}
		}

		if (event->key() == Qt::Key_Return)
		{
			QListWidgetItem *item = ui->searchList->currentItem();
			if (item)
				slotEnter();
		}
	}

	if (obj == ui->searchLineEdit && e->type() == QEvent::MouseButtonPress)
	{
		ui->searchLineEdit->selectAll();
		return true;
	}

	return QWidget::eventFilter(obj, e);
}

void TransmitMessageWidget::init()
{
	QList<MessageListInfo> infoList = gDataBaseOpera->DBGetALLMessageListInfo();

	for (int i = infoList.count(); i > 0; i--)
	{
		MessageListInfo info = infoList.at(i-1);

		if (info.isGroup == 0 && gDataBaseOpera->DBJudgeGroupIsHaveByID(QString::number(info.nBudyyID)))
			info.isGroup = 1;

		if (info.isGroup == 0) //Friends。
		{
			BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(QString::number(info.nBudyyID));
			if (buddy.nUserId == info.nBudyyID)  //查询成功。
			{
				if (buddy.nUserType != 0)  //0为普通联系人，1等是星际通讯、引力场之类。此处不用buddyType是因为非Friends也需要在此显示。
					continue;
			}
		}

		CFrientStyleWidget *buddy = new CFrientStyleWidget();
		buddy->OnInitLogUserList(QString::number(info.nBudyyID));
		buddy->OnSetPicPath(info.strBuddyHeaderImage, info.isGroup);
		buddy->OnSetNickNameText(info.strBuddyName);
		QListWidgetItem *newItem = new QListWidgetItem(); //创建一个newItem

		newItem->setData(Qt::UserRole, info.isGroup);
		newItem->setSizeHint(QSize(this->width(), 30));
		ui->messageList->addItem(newItem);
		ui->messageList->setItemWidget(newItem, buddy); //将buddy赋给该newItem
	}

	slotSwitchBuddyTab();
}

void TransmitMessageWidget::slotSwitchBuddyTab()
{
	ui->groupList->hide();
	ui->buddyList->show();

	if (ui->buddyList->count() == 0)
	{
		QList<BuddyInfo> buddyList = gDataBaseOpera->DBGetBuddyInfo();
		foreach(BuddyInfo buddy, buddyList)
		{
			if (buddy.BuddyType == 0)
				continue;

			CFrientStyleWidget *widget = new CFrientStyleWidget();
			widget->OnInitLogUserList(QString::number(buddy.nUserId));
			widget->OnSetPicPath(buddy.strLocalAvatar);
			if (buddy.strNote.isEmpty())
				widget->OnSetNickNameText(buddy.strNickName);
			else
				widget->OnSetNickNameText(buddy.strNote);

			QListWidgetItem *newItem = new QListWidgetItem(); //创建一个newItem
			newItem->setSizeHint(QSize(this->width(), 30));
			ui->buddyList->addItem(newItem);
			ui->buddyList->setItemWidget(newItem, widget); //将buddy赋给该newItem
		}
	}
}

void TransmitMessageWidget::slotSwitchGroupTab()
{
	ui->buddyList->hide();
	ui->groupList->show();

	if (ui->groupList->count() == 0)
	{
		QList<GroupInfo> groupList = gDataBaseOpera->DBGetAllGroupInfo();
		foreach(GroupInfo group, groupList)
		{
			CFrientStyleWidget *widget = new CFrientStyleWidget();
			widget->OnInitLogUserList(group.groupId);
			widget->OnSetPicPath(group.groupLoacalHeadImage, 1);
			widget->OnSetNickNameText(group.groupName);

			QListWidgetItem *newItem = new QListWidgetItem(); //创建一个newItem
			newItem->setSizeHint(QSize(this->width(), 30));
			ui->groupList->addItem(newItem);
			ui->groupList->setItemWidget(newItem, widget); //将buddy赋给该newItem
		}
	}
}

void TransmitMessageWidget::slotClickSearchItem(QListWidgetItem *item)
{
	doListCancelSelected(ui->messageList);
	doListCancelSelected(ui->buddyList);
	doListCancelSelected(ui->groupList);

	if (item->data(Qt::UserRole).toInt())  //1是Tribes，0是Friends。
		this->type = OpenGroup;
	else
		this->type = OpenPer;

	CFrientStyleWidget *message = (CFrientStyleWidget *)ui->searchList->itemWidget(item);
	if (message == NULL)
	{
		qDebug() << "TransmitMessageWidget::slotClickSearchItem空指针";
		return;
	}
	int chatID = message->mNickName->objectName().toInt();
	this->contactID = QString::number(chatID);
}

void TransmitMessageWidget::slotClickMessageItem(QListWidgetItem *item)
{
	doListCancelSelected(ui->buddyList);
	doListCancelSelected(ui->groupList);

	if (item->data(Qt::UserRole).toInt())  //1是Tribes，0是Friends。
		this->type = OpenGroup;
	else
		this->type = OpenPer;

	CFrientStyleWidget *message = (CFrientStyleWidget *)ui->messageList->itemWidget(item);
	if (message == NULL)
	{
		qDebug() << "TransmitMessageWidget::slotClickMessageItem空指针";
		return;
	}
	int chatID = message->mNickName->objectName().toInt();
	this->contactID = QString::number(chatID);
}

void TransmitMessageWidget::slotClickBuddyItem(QListWidgetItem *item)
{
	doListCancelSelected(ui->groupList);
	doListCancelSelected(ui->searchList);
	doListCancelSelected(ui->messageList);

	this->type = OpenPer;
	CFrientStyleWidget *buddy = (CFrientStyleWidget *)ui->buddyList->itemWidget(item);
	if (buddy == NULL)
	{
		qDebug() << "TransmitMessageWidget::slotClickBuddyItem空指针";
		return;
	}
	int chatID = buddy->mNickName->objectName().toInt();
	this->contactID = QString::number(chatID);
}

void TransmitMessageWidget::slotClickGroupItem(QListWidgetItem *item)
{
	doListCancelSelected(ui->messageList);
	doListCancelSelected(ui->searchList);
	doListCancelSelected(ui->buddyList);

	this->type = OpenGroup;
	CFrientStyleWidget *group = (CFrientStyleWidget *)ui->groupList->itemWidget(item);
	if (group == NULL)
	{
		qDebug() << "TransmitMessageWidget::slotClickGroupItem空指针";
		return;
	}
	int chatID = group->mNickName->objectName().toInt();
	this->contactID = QString::number(chatID);
}

void TransmitMessageWidget::slotEnter()
{
	if (this->contactID.isEmpty())
	{
		IMessageBox::tip(this, tr("Notice"), tr("Please choose a friend or a group"));
	}
	else
	{
		emit sigTransmitContact(type, contactID);
		this->close();
	}
}

void TransmitMessageWidget::slotDBClickSearchItem(QListWidgetItem *item)
{
	slotClickSearchItem(item);
	slotEnter();
}

void TransmitMessageWidget::slotDBClickMessageItem(QListWidgetItem *item)
{
	slotClickMessageItem(item);
	slotEnter();
}

void TransmitMessageWidget::slotDBClickBuddyItem(QListWidgetItem *item)
{
	slotClickBuddyItem(item);
	slotEnter();
}

void TransmitMessageWidget::slotDBClickGroupItem(QListWidgetItem *item)
{
	slotClickGroupItem(item);
	slotEnter();
}

void TransmitMessageWidget::slotEditSearchLine(QString text)
{
	if (text.isEmpty())
	{
		ui->searchList->hide();
		ui->recentWidget->show();

		//退出搜索后，务必将保存的id清空。
		this->contactID.clear();
	}
	else
	{
		ui->recentWidget->hide();
		ui->searchList->clear();
		ui->searchList->show();

		QList<BuddyInfo> AllBuddyList = gDataBaseOpera->DBGetBuddyInfo();
		QList<GroupInfo> AllGroupList = gDataBaseOpera->DBGetAllGroupInfo();

		QList<BuddyInfo> buddyList;
		foreach(BuddyInfo buddy, AllBuddyList)
		{
			if (buddy.BuddyType != 1)
				continue;

			PinYin pinyin;
			if (!buddy.strNickName.isEmpty())
			{
				pinyin = getPinYin(buddy.strNickName);
				if (buddy.strNickName.toLower().contains(text) || pinyin.fullPinYin.contains(text) || pinyin.easyPinYin.contains(text))
				{
					buddyList.append(buddy);
					continue;
				}
			}

			if (!buddy.strNote.isEmpty())
			{
				pinyin = getPinYin(buddy.strNote);
				if (buddy.strNote.toLower().contains(text) || pinyin.fullPinYin.contains(text) || pinyin.easyPinYin.contains(text))
				{
					buddyList.append(buddy);
					continue;
				}
			}
		}

		QList<GroupInfo> groupList;
		foreach(GroupInfo group, AllGroupList)
		{
			PinYin pinyin;
			if (!group.groupName.isEmpty())
			{
				PinYin pinyin = getPinYin(group.groupName);
				if (group.groupName.toLower().contains(text) || pinyin.fullPinYin.contains(text) || pinyin.easyPinYin.contains(text))
				{
					groupList.append(group);
					continue;
				}
			}
		}

		this->onInsertSearchResult(buddyList, groupList);
	}
}

void TransmitMessageWidget::onInsertSearchResult(QList<BuddyInfo> buddyList, QList<GroupInfo> groupList)
{
	if (!buddyList.isEmpty())
	{
		//先插入字母。
		QListWidgetItem *newItem = new QListWidgetItem(tr("Friends"));    //创建一个Item
		newItem->setSizeHint(QSize(ui->searchList->width(), 25));//设置宽度、高度
		newItem->setFlags(Qt::NoItemFlags);
		newItem->setData(Qt::UserRole, tr("Friends"));
		ui->searchList->addItem(newItem);

		foreach(BuddyInfo buddy, buddyList)
		{
			CFrientStyleWidget *buddyWidget = new CFrientStyleWidget();
			buddyWidget->OnInitLogUserList(QString::number(buddy.nUserId));
			buddyWidget->OnSetPicPath(buddy.strLocalAvatar);
			if (buddy.strNote.isEmpty())
				buddyWidget->OnSetNickNameText(buddy.strNickName);
			else
				buddyWidget->OnSetNickNameText(buddy.strNote);
			QListWidgetItem *buddyItem = new QListWidgetItem(); //创建一个newItem

			buddyItem->setData(Qt::UserRole, 0);  //0是Friends，1是Tribes
			buddyItem->setSizeHint(QSize(ui->searchList->width(), 30));
			ui->searchList->addItem(buddyItem);
			ui->searchList->setItemWidget(buddyItem, buddyWidget); //将buddy赋给该newItem
			//“Friends”项和第一项
			if (ui->searchList->count() == 2)
			{
				buddyItem->setSelected(true);
				ui->searchList->setCurrentItem(buddyItem);
				//默认设置为选取第一项。
				this->slotClickSearchItem(buddyItem);
			}
		}
	}

	if (!groupList.isEmpty())
	{
		//先插入字母。
		QListWidgetItem *newItem = new QListWidgetItem(tr("Groups"));    //创建一个Item
		newItem->setSizeHint(QSize(ui->searchList->width(), 25));//设置宽度、高度
		newItem->setFlags(Qt::NoItemFlags);
		newItem->setData(Qt::UserRole, tr("Groups"));
		ui->searchList->addItem(newItem);

		foreach(GroupInfo group, groupList)
		{
			CFrientStyleWidget *groupWidget = new CFrientStyleWidget();
			groupWidget->OnInitLogUserList(group.groupId);
			groupWidget->OnSetPicPath(group.groupLoacalHeadImage);
			groupWidget->OnSetNickNameText(group.groupName);
			QListWidgetItem *groupItem = new QListWidgetItem(); //创建一个newItem

			groupItem->setData(Qt::UserRole, 1);  //0是Friends，1是Tribes
			groupItem->setSizeHint(QSize(ui->searchList->width(), 30));
			ui->searchList->addItem(groupItem);
			ui->searchList->setItemWidget(groupItem, groupWidget); //将buddy赋给该newItem
			if (ui->searchList->count() == 2)
			{
				groupItem->setSelected(true);
				ui->searchList->setCurrentItem(groupItem);
				//默认设置为选取第一项。
				this->slotClickSearchItem(groupItem);
			}
		}
	}
}

void TransmitMessageWidget::doListCancelSelected(QListWidget *list)
{
	for (int i = 0; i < list->count(); i++)
	{
		QListWidgetItem *item = list->item(i);
		if (item && item->isSelected())
		{
			item->setSelected(false);
			return;
		}	
	}
}

#ifdef Q_OS_LINUX
void TransmitMessageWidget::setLinuxCenter()
{
    window()->setGeometry
    (
            QStyle::alignedRect
            (
                    Qt::LeftToRight,
                    Qt::AlignCenter,
                    window()->size(),
                    qApp->desktop()->availableGeometry()
            )
    );
}
#endif