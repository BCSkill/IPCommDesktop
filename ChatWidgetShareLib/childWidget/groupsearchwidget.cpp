﻿#include "groupsearchwidget.h"
#include "ui_groupsearchwidget.h"
#include <qdebug.h>
#ifdef Q_OS_MAC
#include "mycommonstyle.h"
#endif

GroupSearchWidget::GroupSearchWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::GroupSearchWidget();
	ui->setupUi(this);

	connect(ui->lineEdit, SIGNAL(textChanged(QString)), this, SLOT(slotSearch(QString)));
    connect(ui->listWidget, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(slotDBClickItem(QListWidgetItem *)));

#ifdef Q_OS_MAC
    ui->lineEdit->setStyle(new MyProxyStyle);
#endif

	QFile file(":/QSS/Resources/QSS/ChatWidgetShareLib/groupsearchwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	ui->lineEdit->installEventFilter(this);
}

GroupSearchWidget::~GroupSearchWidget()
{
	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

void GroupSearchWidget::setGroupID(QString groupID)
{
	currentID = groupID;
}

void GroupSearchWidget::getFocus()
{
	ui->lineEdit->setFocus();
}

void GroupSearchWidget::clearAndHide()
{
	ui->lineEdit->clear();
	this->hide();
}

void GroupSearchWidget::slotSearch(QString key)
{
	key = key.toLower();
	ui->listWidget->clear();
	this->resize(this->width(), this->minimumHeight());

	if (key.isEmpty())
		return;

	QList<BuddyInfo> allList = gDataBaseOpera->DBGetGroupBuddyInfoFromID(currentID);
	QList<BuddyInfo> buddyList;
	foreach(BuddyInfo buddy, allList)
	{
		PinYin pinyin;
		if (!buddy.strNote.isEmpty())
		{
			pinyin = getPinYin(buddy.strNote);
			if (buddy.strNote.toLower().contains(key) || pinyin.fullPinYin.contains(key) || pinyin.easyPinYin.contains(key))
			{
				buddyList.append(buddy);
				continue;
			}
		}

		if (!buddy.strNickName.isEmpty())
		{
			pinyin = getPinYin(buddy.strNickName);
			if (buddy.strNickName.toLower().contains(key) || pinyin.fullPinYin.contains(key) || pinyin.easyPinYin.contains(key))
			{
				buddyList.append(buddy);
				continue;
			}
		}
	}
	
	if (buddyList.count() == 0)
	{
		QListWidgetItem *item = new QListWidgetItem;
		item->setText("搜索结果为空……");
		item->setTextAlignment(Qt::AlignCenter);
		item->setFlags(Qt::NoItemFlags);
		item->setSizeHint(QSize(this->width() - 4, 30));
		ui->listWidget->addItem(item);
		this->resize(this->width(), this->height() + 30);
	}
	else
	{
		for (int i = 0; i < buddyList.count(); i++)
		{
			BuddyInfo buddyInfo = buddyList.at(i);

			CFrientStyleWidget *buddy = new CFrientStyleWidget();
			buddy->OnInitGroupUserList(QString::number(buddyInfo.nUserId));
			buddy->OnSetPicPath(buddyInfo.strLocalAvatar);
			if (buddyInfo.strNote.isEmpty())
				buddy->OnSetNickNameText(buddyInfo.strNickName);
			else
				buddy->OnSetNickNameText(buddyInfo.strNote);
			QListWidgetItem *newItem = new QListWidgetItem(); //创建一个newItem
			newItem->setSizeHint(QSize(this->width() - 4, 30));
			ui->listWidget->insertItem(ui->listWidget->count(), newItem); //将该newItem插入到后面
			ui->listWidget->setItemWidget(newItem, buddy); //将buddy赋给该newItem

			if (ui->listWidget->count() == 1)
			{
				newItem->setSelected(true);
				ui->listWidget->setCurrentItem(newItem);
			}

			this->resize(this->width(), this->height() + 30);
		}
	}

	this->resize(this->width(), this->height() + 3);
}

void GroupSearchWidget::slotDBClickItem(QListWidgetItem *item)
{
	CFrientStyleWidget *buddy = (CFrientStyleWidget*)ui->listWidget->itemWidget(item);
	if (buddy == NULL)
	{
		qDebug() << "GroupSearchWidget::slotDBClickItem空指针";
		return;
	}
	QString strUserID = buddy->mNickName->objectName();

	emit sigOpenSearchUser(strUserID);
}

bool GroupSearchWidget::eventFilter(QObject *obj, QEvent *e)
{
	if (obj == ui->lineEdit && e->type() == QEvent::KeyPress)
	{
		QKeyEvent *event = (QKeyEvent *)e;
		if (event->key() == Qt::Key_Up)
		{
			if (ui->listWidget->count() > 0)
			{
				QListWidgetItem *item = NULL;
				for (int i = ui->listWidget->count() - 1; i >= 0; i--)
				{
					if (ui->listWidget->currentRow() < 0)
					{
						item = ui->listWidget->item(i);
						break;
					}

					if (ui->listWidget->currentRow() > i)
					{
						item = ui->listWidget->item(i);
						break;
					}
				}
				if (item)
				{
					item->setSelected(true);
					ui->listWidget->setCurrentItem(item);
				}
			}
		}
		if (event->key() == Qt::Key_Down)
		{
			if (ui->listWidget->count() > 0)
			{
				QListWidgetItem *item = NULL;
				for (int i = 0; i < ui->listWidget->count(); i++)
				{
					if (ui->listWidget->currentRow() < 0)
					{
						item = ui->listWidget->item(i);
						break;
					}

					if (ui->listWidget->currentRow() < i)
					{
						item = ui->listWidget->item(i);
						break;
					}
				}
				if (item)
				{
					item->setSelected(true);
					ui->listWidget->setCurrentItem(item);
				}
			}
		}

		if (event->key() == Qt::Key_Return)
		{
			QListWidgetItem *item = ui->listWidget->currentItem();
			if (item)
				slotDBClickItem(item);
		}
	}

	if (obj == ui->lineEdit && e->type() == QEvent::MouseButtonPress)
	{
		ui->lineEdit->selectAll();
		return true;
	}

	return QWidget::eventFilter(obj, e);
}
