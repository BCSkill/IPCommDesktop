﻿#include "groupaddbuddywidget.h"
#include "ui_groupaddbuddywidget.h"
#include "mycommonstyle.h"
#include "QStringLiteralBak.h"

#ifdef Q_OS_LINUX
    #include <qdesktopwidget.h>
#endif

GroupAddBuddyWidget::GroupAddBuddyWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::GroupAddBuddyWidget();
	ui->setupUi(this);

	this->setWindowFlags(Qt::FramelessWindowHint);		// 去掉标题栏 且不能移动
	this->setAttribute(Qt::WA_DeleteOnClose);			// 设置在关闭该窗口时 直接 delete该窗口的对象 即 close()走析构函数

	setWindowIcon(QIcon(":/Login/Resources/login/system.ico"));
#ifdef Q_OS_WIN
	shadow = new Shadow();
#endif

	QFile file(":/QSS/Resources/QSS/ChatWidgetShareLib/groupaddbuddywidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->closeBtn, SIGNAL(clicked()), this, SLOT(close()));
	connect(ui->cancelBtn, SIGNAL(clicked()), this, SLOT(close()));
	connect(ui->contactsList, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(slotContactsItemClicked(QListWidgetItem*)));
	connect(ui->lineEdit, SIGNAL(textEdited(QString)), this, SLOT(slotSearch(QString)));
	connect(ui->searchList, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(slotClickSearchList(QListWidgetItem *)));
	connect(ui->enterBtn, SIGNAL(clicked()), this, SLOT(slotClickEnterBtn()));

	ui->lineEdit->installEventFilter(this);
	ui->searchList->hide();

#ifndef Q_OS_WIN
    ui->lineEdit->setStyle(new MyProxyStyle);
    ui->searchList->setStyle(new MyProxyStyle);
    ui->contactsList->setStyle(new MyProxyStyle);
    ui->memberList->setStyle(new MyProxyStyle);
#endif
	initContactsList();

	ui->lineEdit->setFocus();


#ifdef Q_OS_LINUX
	setLinuxCenter();
#endif
}

GroupAddBuddyWidget::~GroupAddBuddyWidget()
{
	delete ui;

#ifdef Q_OS_WIN
	if (shadow)
		delete shadow;
#endif
}

void GroupAddBuddyWidget::changeEvent(QEvent * event)
{
	if (event->type() == QEvent::WindowStateChange)
	{
#ifdef Q_OS_WIN
		if (this->windowState() == Qt::WindowMaximized)
			shadow->hide();
		if (this->windowState() == Qt::WindowMinimized)
			shadow->hide();

		if (this->windowState() == Qt::WindowNoState)
			shadow->show();
#endif
	}

	QWidget::changeEvent(event);
}


void GroupAddBuddyWidget::moveEvent(QMoveEvent *event)
{
#ifdef Q_OS_WIN
	shadow->move(event->pos().x() - 10, event->pos().y() - 10);
#endif
}

void GroupAddBuddyWidget::resizeEvent(QResizeEvent* event)
{
#ifdef Q_OS_WIN
	shadow->resize(event->size().width() + 20, event->size().height() + 20);
#endif
}

void GroupAddBuddyWidget::show()
{
	QWidget::show();
#ifdef Q_OS_WIN
	shadow->resize(width() + 20, height() + 20);
	shadow->move(x() - 10, y() - 10);
	shadow->show();
#endif
#ifdef Q_OS_WIN
	// 窗口被激活了，强制重绘  
	InvalidateRect((HWND)this->winId(), nullptr, FALSE);
#endif
}

//鼠标事件的处理。
void GroupAddBuddyWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void GroupAddBuddyWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void GroupAddBuddyWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

void GroupAddBuddyWidget::initContactsList()
{
	QMap<QString, QList<BuddyInfo> > contacts_map = gDataBaseOpera->DB_GetBuddyInfo();
	QMap<QString, QList<BuddyInfo> >::iterator itor = contacts_map.begin();
	for (; itor != contacts_map.end(); ++itor)
	{
		// 创建 buddy_label 的list item   buddy_label -- A B C.....
		QString buddys_label = itor.key();
		if (buddys_label == "~")
			buddys_label = "#";
		if (buddys_label.isEmpty())
			continue;
		QListWidgetItem *newItem = new QListWidgetItem(buddys_label);
		newItem->setSizeHint(QSize(this->width(), 25));
		newItem->setData(Qt::UserRole, buddys_label);
		newItem->setFlags(Qt::ItemFlag::NoItemFlags);			// 给标签设置特殊的flag 供与正常item区别
		ui->contactsList->insertItem(ui->contactsList->count(), newItem);         //加到QListWidget中

		// 将 QList<BuddyInfo> 中的成员加入之后的list item 
		// buddys 为指向 QList<BuddyInfo>的指针
		QList<BuddyInfo>&  buddys = itor.value();
		for (int i = 0; i < buddys.size(); ++i)
		{
			int nFlags = 0;
			QString strAvatar = (buddys[i]).strLocalAvatar;
			QString strNickName = (buddys[i]).strNickName;
			QString strNote = (buddys[i]).strNote;
			QString strBuddyID = QString("%1").arg((buddys[i]).nUserId);

			CFrientStyleWidget *buddy = new CFrientStyleWidget();
			buddy->OnInitCreateGroupContactsList(strBuddyID);
			buddy->OnSetPicPath(strAvatar);
			if (!strNote.isEmpty())
				buddy->OnSetNickNameText(strNote);
			else
				buddy->OnSetNickNameText(strNickName);

			QListWidgetItem *newItem = new QListWidgetItem(); //创建一个newItem
			newItem->setSizeHint(QSize(ui->contactsList->width(), 50));
			newItem->setData(Qt::UserRole, strBuddyID);

			ui->contactsList->insertItem(ui->contactsList->count(), newItem); //将该newItem插入到后面
			ui->contactsList->setItemWidget(newItem, buddy); //将buddy赋给该newItem
		}
	}
}

void GroupAddBuddyWidget::slotContactsItemClicked(QListWidgetItem* the_item)
{
	// 如果item时标签(buddy_label)的话 直接返回 不做任何处理
	if (the_item->flags() == Qt::ItemFlag::NoItemFlags)
		return;

	CFrientStyleWidget *widget = (CFrientStyleWidget*)ui->contactsList->itemWidget(the_item);
	if (widget == NULL)
	{
		qDebug() << "GroupAddBuddyWidget::slotContactsItemClicked空指针";
		return;
	}
	if (gDataBaseOpera->DBJudgeGroupIsHaveBuddy(groupID, widget->objectName()))
	{
		IMessageBox::tip(this, tr("Notice"), tr("This member has been in the group already!"));
	}
	else
	{
		widget->changeCGCheckBtnStatus();
		if (widget->getCGCheckBtnStatus() == true)  //选中
		{
			QList<BuddyInfo> buddyList = gDataBaseOpera->DBGetBuddyInfo();
			foreach(BuddyInfo buddy, buddyList)
			{
				QString strBuddyID = QString("%1").arg(buddy.nUserId);
				if (strBuddyID != widget->objectName())
					continue;

				QString strAvatar = buddy.strLocalAvatar;
				QString strNickName = buddy.strNickName;
				QString strNote = buddy.strNote;

				CFrientStyleWidget *member = new CFrientStyleWidget();
				member->OnInitCreateGroupMemberList(strBuddyID);
				connect(member, SIGNAL(sigRemoveBuddyFromCGMemberList(QString)), this, SLOT(slotMemberItemClicked(QString)));

				member->OnSetPicPath(strAvatar);
				if (strNote.isEmpty())
					member->OnSetNickNameText(strNickName);
				else
					member->OnSetNickNameText(strNote);

				QListWidgetItem *newItem = new QListWidgetItem(); //创建一个newItem
				newItem->setSizeHint(QSize(ui->memberList->width(), 50));
				newItem->setData(Qt::UserRole, strBuddyID);

				ui->memberList->insertItem(ui->memberList->count(), newItem); //将该newItem插入到后面
				ui->memberList->setItemWidget(newItem, member); //将buddy赋给该newItem
				break;
			}
		}
		else
		{
			for (int i = 0; i < ui->memberList->count(); i++)
			{
				QListWidgetItem* cur_item = ui->memberList->item(i);
				if (cur_item)
				{
					CFrientStyleWidget*	buddy = (CFrientStyleWidget*)ui->memberList->itemWidget(cur_item);
					if (buddy)
					{
						if (buddy->objectName() == widget->objectName())
						{
							ui->memberList->removeItemWidget(cur_item);
							delete cur_item;
							break;
						}
					}
				}
			}
		}

		updateGroupMembersCount();
	}
}

void GroupAddBuddyWidget::updateGroupMembersCount()
{
	QString str_info_show;
	
	if (ui->memberList->count() > 0)
	{
		QString str_count = QString::number(ui->memberList->count());
		str_info_show = tr("You have selected ") + str_count + tr(" contact(s)");
	}
	else
		str_info_show = tr("Please select contacts you want to invite");

	ui->countLabel->setText(str_info_show);
}

void GroupAddBuddyWidget::slotMemberItemClicked(QString buddy_id)
{
	for (int i = 0; i < ui->memberList->count(); i++)
	{
		QListWidgetItem* cur_item = ui->memberList->item(i);
		if (cur_item)
		{
			CFrientStyleWidget*	buddy = (CFrientStyleWidget*)ui->memberList->itemWidget(cur_item);
			if (buddy)
			{
				if (buddy->objectName() == buddy_id)
				{
					ui->memberList->removeItemWidget(cur_item);
					delete cur_item;
					break;
				}
			}
		}
	}

	for (int i = 0; i < ui->contactsList->count(); i++)
	{
		QListWidgetItem* cur_item = ui->contactsList->item(i);
		if (cur_item)
		{
			CFrientStyleWidget* buddy = (CFrientStyleWidget*)ui->contactsList->itemWidget(cur_item);
			if (buddy)
			{
				if (buddy->objectName() == buddy_id)
				{
					buddy->changeCGCheckBtnStatus();
				}
			}
		}
	}

	if (ui->searchList->isVisible())
	{
		for (int i = 0; i < ui->searchList->count(); i++)
		{
			QListWidgetItem *item = ui->searchList->item(i);
			if (item)
			{
				CFrientStyleWidget *widget = (CFrientStyleWidget*)ui->searchList->itemWidget(item);
				if (widget)
				{
					if (widget->objectName() == buddy_id)
					{
						widget->changeCGCheckBtnStatus();
					}
				}
			}
		}
	}

	updateGroupMembersCount();
}

void GroupAddBuddyWidget::slotSearch(QString text)
{
	if (text.isEmpty())
	{
		ui->searchList->hide();
		ui->contactsList->show();
	}
	else
	{
		ui->contactsList->hide();
		ui->searchList->clear();
		ui->searchList->show();

		text = text.toLower();

		QList<BuddyInfo> buddyList = gDataBaseOpera->DBGetBuddyInfo();
		foreach(BuddyInfo buddy, buddyList)
		{
			if (buddy.BuddyType != 1)
				continue;

			PinYin pinyin;
			if (!buddy.strNote.isEmpty())
			{
				pinyin = getPinYin(buddy.strNote);
				if (buddy.strNote.toLower().contains(text) || pinyin.fullPinYin.contains(text) || pinyin.easyPinYin.contains(text))
				{
					QString buddyID = QString::number(buddy.nUserId);
					CFrientStyleWidget *widget = new CFrientStyleWidget();
					widget->OnInitCreateGroupContactsList(buddyID);
					for (int i = 0; i < ui->memberList->count(); i++)
					{
						QListWidgetItem *item = ui->memberList->item(i);
						if (item)
						{
							CFrientStyleWidget *member = (CFrientStyleWidget *)ui->memberList->itemWidget(item);

							if (member)
							{
								if (member->objectName() == buddyID)
									widget->changeCGCheckBtnStatus();
							}
						}
					}
					widget->OnSetPicPath(buddy.strLocalAvatar);
					widget->OnSetNickNameText(buddy.strNote);

					QListWidgetItem *newItem = new QListWidgetItem(); //创建一个newItem
					newItem->setSizeHint(QSize(ui->searchList->width(), 50));
					newItem->setData(Qt::UserRole, buddyID);

					ui->searchList->insertItem(ui->searchList->count(), newItem); //将该newItem插入到后面
					ui->searchList->setItemWidget(newItem, widget); //将buddy赋给该newItem

					if (ui->searchList->count() == 1)
					{
						newItem->setSelected(true);
						ui->searchList->setCurrentItem(newItem);
					}
					continue;
				}
			}

			if (!buddy.strNickName.isEmpty())
			{
				pinyin = getPinYin(buddy.strNickName);
				if (buddy.strNickName.toLower().contains(text) || pinyin.fullPinYin.contains(text) || pinyin.easyPinYin.contains(text))
				{
					QString buddyID = QString::number(buddy.nUserId);
					CFrientStyleWidget *widget = new CFrientStyleWidget();
					widget->OnInitCreateGroupContactsList(buddyID);
					for (int i = 0; i < ui->memberList->count(); i++)
					{
						QListWidgetItem *item = ui->memberList->item(i);
						CFrientStyleWidget *member = (CFrientStyleWidget *)ui->memberList->itemWidget(item);

						if (member)
						{
							if (member->objectName() == buddyID)
								widget->changeCGCheckBtnStatus();
						}
					}
					widget->OnSetPicPath(buddy.strLocalAvatar);
					widget->OnSetNickNameText(buddy.strNickName);

					QListWidgetItem *newItem = new QListWidgetItem(); //创建一个newItem
					newItem->setSizeHint(QSize(ui->searchList->width(), 50));
					newItem->setData(Qt::UserRole, buddyID);

					ui->searchList->insertItem(ui->searchList->count(), newItem); //将该newItem插入到后面
					ui->searchList->setItemWidget(newItem, widget); //将buddy赋给该newItem

					if (ui->searchList->count() == 1)
					{
						newItem->setSelected(true);
						ui->searchList->setCurrentItem(newItem);
					}
					continue;
				}
			}
		}
	}
}

void GroupAddBuddyWidget::slotClickSearchList(QListWidgetItem *item)
{
	CFrientStyleWidget *widget = (CFrientStyleWidget*)ui->searchList->itemWidget(item);
	if (widget == NULL)
	{
		qDebug() << "GroupAddBuddyWidget::slotClickSearchList空指针";
		return;
	}

	if (gDataBaseOpera->DBJudgeGroupIsHaveBuddy(groupID, widget->objectName()))
	{
		IMessageBox::tip(this, tr("Notice"), tr("This member has been in the group already!"));
	}
	else
	{
		widget->changeCGCheckBtnStatus();
		const bool btn_is_checked = true;
		if (widget->getCGCheckBtnStatus() == true)  //选中
		{
			QList<BuddyInfo> buddyList = gDataBaseOpera->DBGetBuddyInfo();
			foreach(BuddyInfo buddy, buddyList)
			{
				QString strBuddyID = QString("%1").arg(buddy.nUserId);
				if (strBuddyID != widget->objectName())
					continue;

				QString strAvatar = buddy.strLocalAvatar;
				QString strNickName = buddy.strNickName;
				QString strNote = buddy.strNote;

				CFrientStyleWidget *member = new CFrientStyleWidget();
				member->OnInitCreateGroupMemberList(strBuddyID);
				connect(member, SIGNAL(sigRemoveBuddyFromCGMemberList(QString)), this, SLOT(slotMemberItemClicked(QString)));

				member->OnSetPicPath(strAvatar);
				if (strNote.isEmpty())
					member->OnSetNickNameText(strNickName);
				else
					member->OnSetNickNameText(strNote);

				QListWidgetItem *newItem = new QListWidgetItem(); //创建一个newItem
				newItem->setSizeHint(QSize(ui->memberList->width(), 50));
				newItem->setData(Qt::UserRole, strBuddyID);

				ui->memberList->insertItem(ui->memberList->count(), newItem); //将该newItem插入到后面
				ui->memberList->setItemWidget(newItem, member); //将buddy赋给该newItem
				break;
			}
		}
		else
		{
			for (int i = 0; i < ui->memberList->count(); i++)
			{
				QListWidgetItem* cur_item = ui->memberList->item(i);
				if (cur_item)
				{
					CFrientStyleWidget*	buddy = (CFrientStyleWidget*)ui->memberList->itemWidget(cur_item);
					if (buddy)
					{
						if (buddy->objectName() == widget->objectName())
						{
							ui->memberList->removeItemWidget(cur_item);
							delete cur_item;
							break;
						}
					}
				}
			}
		}

		for (int i = 0; i < ui->contactsList->count(); i++)
		{
			QListWidgetItem *buddyItem = ui->contactsList->item(i);
			if (buddyItem)
			{
				if (buddyItem->flags() == Qt::NoItemFlags)
					continue;
				CFrientStyleWidget *buddyWidget = (CFrientStyleWidget*)ui->contactsList->itemWidget(buddyItem);
				if (buddyWidget != NULL)
				{
					if (buddyWidget->objectName() == widget->objectName())
					{
						buddyWidget->changeCGCheckBtnStatus();
						return;
					}
				}
			}
		}
	}
}

bool GroupAddBuddyWidget::eventFilter(QObject *obj, QEvent *e)
{
	if (obj == ui->lineEdit && e->type() == QEvent::KeyPress)
	{
		QKeyEvent *event = (QKeyEvent *)e;
		if (event->key() == Qt::Key_Up)
		{
			if (ui->searchList->count() > 0)
			{
				QListWidgetItem *item = NULL;
				for (int i = ui->searchList->count() - 1; i >= 0; i--)
				{
					if (ui->searchList->currentRow() < 0)
					{
						item = ui->searchList->item(i);
						break;
					}

					if (ui->searchList->currentRow() > i)
					{
						item = ui->searchList->item(i);
						break;
					}
				}
				if (item)
				{
					item->setSelected(true);
					ui->searchList->setCurrentItem(item);
				}
			}
		}
		if (event->key() == Qt::Key_Down)
		{
			if (ui->searchList->count() > 0)
			{
				QListWidgetItem *item = NULL;
				for (int i = 0; i < ui->searchList->count(); i++)
				{
					if (ui->searchList->currentRow() < 0)
					{
						item = ui->searchList->item(i);
						break;
					}

					if (ui->searchList->currentRow() < i)
					{
						item = ui->searchList->item(i);
						break;
					}
				}
				if (item)
				{
					item->setSelected(true);
					ui->searchList->setCurrentItem(item);
				}
			}
		}

		if (event->key() == Qt::Key_Return)
		{
			QListWidgetItem *item = ui->searchList->currentItem();
			if (item)
				slotClickSearchList(item);
		}
	}

	if (obj == ui->lineEdit && e->type() == QEvent::MouseButtonPress)
	{
		ui->lineEdit->selectAll();
		return true;
	}

	return QWidget::eventFilter(obj, e);
}

void GroupAddBuddyWidget::slotClickEnterBtn()
{
	UserInfo user = gDataManager->getUserInfo();

	QString addUserID;
	for (int i = 0; i < ui->memberList->count(); i++)
	{
		QListWidgetItem* cur_item = ui->memberList->item(i);
		if (cur_item)
		{
			CFrientStyleWidget*	buddy = (CFrientStyleWidget*)ui->memberList->itemWidget(cur_item);
			if (buddy)
			{
				QString id = buddy->objectName();

				//加入列表。
				if (!addUserID.isEmpty())
					addUserID.append(";");
				addUserID.append(id);

				//发送加人通知。
				UserInfo user = gDataManager->getUserInfo();
				BuddyInfo buddyInfo = gDataBaseOpera->DBGetBuddyInfoByID(id);
				QString content = user.strUserNickName + tr(" invited ") + buddyInfo.strNickName + tr(" to join the group");
				QVariantMap map;
				map.insert("type", "notification");
				map.insert("content", content);
				map.insert("inviterID", user.strUserNickName);
				map.insert("invitedID", buddyInfo.strNickName);
				map.insert("CMD", "addUserToGroup");
				emit sigTipMessage(GroupMessage, groupID, QJsonDocument::fromVariant(map).toJson());
			}
		}
	}

	if (!addUserID.isEmpty())
	{
		HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib(this);
		QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/addUserToGroup"
			+ QString("?userId=%1&passWord=%2&addUserId=%3&groupId=%4").arg(user.nUserID).arg(user.strUserPWD).arg(addUserID).arg(groupID);
		http->getHttpRequest(url);
		IMessageBox *box = new IMessageBox(this);
		connect(box, SIGNAL(sigClose()), this, SLOT(close()));
		box->init(tr("Notice"), tr("Added Successfully!"));
	}
	else
	{
		this->close();
	}
}

#ifdef Q_OS_LINUX
void GroupAddBuddyWidget::setLinuxCenter()
{
    window()->setGeometry
    (
            QStyle::alignedRect
            (
                    Qt::LeftToRight,
                    Qt::AlignCenter,
                    window()->size(),
                    qApp->desktop()->availableGeometry()
            )
    );
}
#endif