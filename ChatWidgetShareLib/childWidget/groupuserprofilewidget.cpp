﻿#include "groupuserprofilewidget.h"
#include "ui_groupuserprofilewidget.h"
#include "QStringLiteralBak.h"

GroupUserProfileWidget::GroupUserProfileWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::GroupUserProfileWidget();
	ui->setupUi(this);
	m_pPicWidget = NULL;

	this->setWindowFlags(Qt::FramelessWindowHint);
	this->setAttribute(Qt::WA_TranslucentBackground);

	setWindowModality(Qt::WindowModal);
	this->setAttribute(Qt::WA_DeleteOnClose);
	setWindowIcon(QIcon(":/Login/Resources/login/system.ico"));

	QPixmap mask(":/profile/Resources/profile/profileMask.png");
	this->setMask(mask.mask());

	closeBtn = new QPushButton(this);
	closeBtn->setStyleSheet("QPushButton{border-image: url(:/profile/Resources/profile/close.png);}");
	closeBtn->setCursor(Qt::PointingHandCursor);
	closeBtn->setToolTip(tr("Close"));
	closeBtn->resize(24, 24);
	closeBtn->move(310, 10);

	QFile file(":/QSS/Resources/QSS/ChatWidgetShareLib/groupuserprofilewidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(closeBtn, SIGNAL(clicked(bool)), this, SLOT(close()));
	connect(ui->copyAddressBtn, SIGNAL(clicked(bool)), this, SLOT(slotCopyAddress()));

	ui->headerLabel->installEventFilter(this);
	ui->starLabel->installEventFilter(this);
}

GroupUserProfileWidget::~GroupUserProfileWidget()
{
	if (m_pPicWidget)
	{
		delete m_pPicWidget;
		m_pPicWidget = NULL;
	}

	if (ui)
	{
		delete ui;
		ui = NULL;
	}

}

void GroupUserProfileWidget::setBuddy(BuddyInfo buddy)
{
	buddyID = QString::number(buddy.nUserId);
	headerPath = buddy.strLocalAvatar;

// 	OPRequestShareLib *request = new OPRequestShareLib;
// 	connect(request, SIGNAL(sigBuddyAddressInfo(AddressInfo)), this, SLOT(slotAddressInfo(AddressInfo)));
// 	connect(request, SIGNAL(sigBuddyAddressInfo(AddressInfo)), request, SLOT(deleteLater()));
// 	request->getBuddyAddressInfo(buddyID);

	if (gDataBaseOpera->DBJudgeFriendIsHaveByID(buddyID))
	{
		ui->applyBtn->setText(tr("Send"));
		connect(ui->applyBtn, SIGNAL(clicked()), this, SLOT(slotOpenChat()));
	}
	else
	{
		connect(ui->applyBtn, SIGNAL(clicked(bool)), this, SLOT(slotAddGroupUser()));
	}

	//先清空。
	ui->nameLabel->clear();
	ui->sexLabel->clear();
	ui->signLabel->clear();
	ui->lianxinLabel->clear();
	ui->addressLabel->clear();

	QPixmap pix(buddy.strLocalAvatar);
	if (pix.isNull())
	{
		QByteArray bytearray = "";
		QFile file(buddy.strLocalAvatar);
		if (file.open(QIODevice::ReadOnly) && file.size() != 0)
		{
			bytearray = file.readAll();
		}
		file.close();
		if (!pix.loadFromData(bytearray) || bytearray == "")
		{
			pix.load(":/PerChat/Resources/person/temp.png");
			headerPath = ":/PerChat/Resources/person/temp.png";
		}
	}

	pix = pix.scaled(ui->headerLabel->size(), Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation);

	ui->headerLabel->setPixmap(pix);

	if (buddy.strSex == "F")
		ui->sexLabel->setPixmap(QPixmap(":/profile/Resources/profile/female.png"));
	if (buddy.strSex == "M")
		ui->sexLabel->setPixmap(QPixmap(":/profile/Resources/profile/male.png"));

	ui->signLabel->setText(buddy.strSign);
	ui->nameLabel->setText(buddy.strNickName);
	ui->lianxinLabel->setText(QString::number(buddy.nUserId));
}

void GroupUserProfileWidget::slotAddressInfo(AddressInfo info)
{
	if (!info.ethAddress.isEmpty())
	{
		ui->addressLabel->setObjectName(info.ethAddress);
		ui->addressLabel->setText(info.ethAddress.left(6) + "******" + info.ethAddress.right(4));

		setStar(info.planet);
	}
}

void GroupUserProfileWidget::setStar(QString star)
{
	//绘制星球球体。
	QPixmap background(":/ewallet/Resources/ewallet/pwrWidget/background.png");
	background = background.scaled(ui->starLabel->size(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

	QString starPath = ":/star/Resources/walletStar/" + star + ".png";
	QPixmap starImage(starPath);
	if (!starImage.isNull())
	{
		starImage = starImage.scaledToWidth(ui->starLabel->width() * 2, Qt::SmoothTransformation);
		starImage = starImage.copy(ui->starLabel->width() / 2, ui->starLabel->height() / 4, ui->starLabel->width(), ui->starLabel->height());
	}

	QPixmap background1(":/ewallet/Resources/ewallet/background1.png");
	background1 = background1.scaledToWidth(ui->starLabel->width() * 2, Qt::SmoothTransformation);
	background1 = background1.copy(ui->starLabel->width() / 2, 0, ui->starLabel->width(), ui->starLabel->height());

	QPixmap background2(":/ewallet/Resources/ewallet/background2.png");
	background2 = background2.scaledToWidth(ui->starLabel->width() * 2, Qt::SmoothTransformation);
	background2 = background2.copy(ui->starLabel->width() / 2, ui->starLabel->height() / 4, ui->starLabel->width(), ui->starLabel->height());

	QPainter *painter = new QPainter;
	painter->begin(&background);
	if (!starImage.isNull())
		painter->drawPixmap(0, 0, starImage);
	painter->drawPixmap(0, 0, background1);
	painter->drawPixmap(0, 0, background2);
	painter->end();
	delete painter;

	ui->starLabel->setPixmap(background);
}

void GroupUserProfileWidget::slotCopyAddress()
{
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(ui->addressLabel->objectName());
}

void GroupUserProfileWidget::slotAddGroupUser()
{
	UserInfo userInfo = gDataManager->getUserInfo();

	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotApplyResult(bool, QString)));

	QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/user/applyAddFriend"
		+ QString("?userId=%1&passWord=%2&friendUserId=%3").arg(userInfo.nUserID).arg(userInfo.strUserPWD).arg(buddyID);

	http->getHttpRequest(url);
}

void GroupUserProfileWidget::slotApplyResult(bool success, QString result)
{
	HttpNetWork::HttpNetWorkShareLib *act = qobject_cast<HttpNetWork::HttpNetWorkShareLib*>(sender());
	if (act)
	{
		if (success)
		{
			if (result.contains("success"))
			{
				ui->tipLabel->setText(tr("Request sent successfully"));
				return;
			}
		}
		ui->tipLabel->setText(tr("Request sent Failed"));
		act->deleteLater();
	}
}

bool GroupUserProfileWidget::eventFilter(QObject *obj, QEvent *e)
{
	if (obj == ui->starLabel)
	{
		if (e->type() == QEvent::MouseButtonPress)
		{
			QMouseEvent *event = (QMouseEvent *)e;
			mouse = event->pos();   //设置移动的原始位置。
		}
		if (e->type() == QEvent::MouseMove)
		{
			QMouseEvent *event = (QMouseEvent *)e;
			if (mouse.x() >= 0)
			{
				//首先通过做差值，获得鼠标位移的距离。
				int x = event->pos().x() - mouse.x();
				int y = event->pos().y() - mouse.y();
				//移动本窗体。
				this->move(this->x() + x, this->y() + y);
			}
		}
		if (e->type() == QEvent::MouseButtonRelease)
		{
			QMouseEvent *event = (QMouseEvent *)e;
			mouse.setX(-1);
		}
	}

	if (obj == ui->headerLabel)
	{
		if (e->type() == QEvent::MouseButtonPress)
		{
			//20180828wmc 屏蔽原有图片浏览窗口使用新窗口
			// 			ZoomImg *zoom = new ZoomImg;
			// 			zoom->OpenImg(headerPath);
			if (m_pPicWidget != NULL)
			{
				delete m_pPicWidget;
			}
			m_pPicWidget = new PicWidget();
			connect(m_pPicWidget, SIGNAL(sigPicClose()), this, SLOT(slotPicClose()));
			m_pPicWidget->setPath(headerPath, this,1);
			m_pPicWidget->show();
		}
	}

	return QWidget::eventFilter(obj, e);
}

void GroupUserProfileWidget::slotOpenChat()
{
	emit sigOpenChat(buddyID);
	this->close();
}
void GroupUserProfileWidget::slotPicClose()
{
	m_pPicWidget = NULL;
}




