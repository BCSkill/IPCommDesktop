﻿#include "threadloadgroupuserlist.h"
#include <QDebug>

ThreadLoadGroupUserList::ThreadLoadGroupUserList(QObject *parent)
: QThread(parent)
{
	//singleCount = 30; //默认每次加载40个成员。
	singleCount = INT_MAX;//全部加载
}

ThreadLoadGroupUserList::~ThreadLoadGroupUserList()
{
	requestInterruption();
	quit();
	wait();
}

void ThreadLoadGroupUserList::SetListBuddyInfo(QList<BuddyInfo> tempList)
{
	listBuddyInfo = tempList;
}

void ThreadLoadGroupUserList::run()
{
	msleep(200);

	for (int i = 0; i < singleCount; i++)
	{
		if (listBuddyInfo.isEmpty())
			break;
		else
		{
			if (!isInterruptionRequested())
			{
				msleep(20);
				emit sigLoadGroupUserList(listBuddyInfo.first());
				listBuddyInfo.pop_front();
			}
			else
				break;
		}
	}
}
