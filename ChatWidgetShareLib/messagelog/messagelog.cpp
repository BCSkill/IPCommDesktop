﻿#include "messagelog.h"
#include "stdafx.h"
#include "QJsonDocument"
#include "secretWidget/openletterwidget.h"
#include "secretWidget/enterpasswordwidget.h"
#include <QTextCodec>
#include <QDesktopWidget>
#include "QStringLiteralBak.h"
#include "amrDec/amrdec.h"
#include "VedioFrameOpera.h"
#include "profilemanager.h"
#include "ui_messagelog.h"
#include "globalmanager.h"
#include "questionbox.h"
#include <QProcess>
#include <QSettings>
#ifndef Q_OS_WIN
#include "mycommonstyle.h"
#else
#endif

extern QString gI18NLocale;

extern QString gThemeStyle;

MessageLog::MessageLog(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::MessageLog();
	ui->setupUi(this);

	m_pPicWidget = NULL;
	videoWidget = NULL;

#ifdef Q_OS_WIN
	shadow = new Shadow();
#endif
	setWindowFlags(Qt::FramelessWindowHint);
	setAttribute(Qt::WA_DeleteOnClose);
	setWindowIcon(QIcon(":/Login/Resources/login/system.ico"));

	ui->dateLabel->setText(QDate::currentDate().toString("yyyy-MM-dd"));
	dateSelector = new QCalendarWidget(this);
	dateSelector->hide();
	dateSelector->setFont(QFont(tr("微软雅黑"), 8));
	dateSelector->setHorizontalHeaderFormat(QCalendarWidget::ShortDayNames);
	dateSelector->setVerticalHeaderFormat(QCalendarWidget::NoVerticalHeader);
	dateSelector->resize(240, 170);

	mExpressWidget = new ExpressWidget();
	//#ifdef Q_OS_WIN
	//	ui->webView->InitCefUrl("file:///./html/messageLog.html");
	//	connect(ui->webView, SIGNAL(sigZoomImg(QString)), this, SLOT(slotZoomImg(QString)));
	//	connect(ui->webView, SIGNAL(sigVideoPlay(QString)), this, SLOT(slotVideoPlay(QString)));
	//	connect(ui->webView, SIGNAL(sigOpenFile(QString)), this, SLOT(slotOpenFile(QString)));
	//	connect(ui->webView, SIGNAL(sigOpenDir(QString)), this, SLOT(slotOpenDir(QString)));
	//	connect(ui->webView, SIGNAL(sigOpenUrl(QString)), this, SLOT(slotOpenLink(QString)));
	//	connect(ui->webView, SIGNAL(sigLocation(QString)), this, SLOT(slotLocation(QString)));
	//
	//	connect(ui->webView, SIGNAL(sigInitFinished()), this,SIGNAL(sigInitFinished()));
	//#else
	m_pWebChannel = NULL;
	m_pWebObject = NULL;
	detailWidget = NULL;
	openPacketWidget = NULL;
	enterWidget = NULL;

	m_pWebChannel = new QWebChannel(this);
	m_pWebObject = new WebObjectShareLib(this);
	connect(m_pWebObject, SIGNAL(sigZoomImg(QString)), this, SLOT(slotZoomImg(QString)));
	connect(m_pWebObject, SIGNAL(sigVideoPlay(QString)), this, SLOT(slotVideoPlay(QString)));
	connect(m_pWebObject, SIGNAL(sigOpenFile(QString)), this, SLOT(slotOpenFile(QString)));
	connect(m_pWebObject, SIGNAL(sigOpenDir(QString)), this, SLOT(slotOpenDir(QString)));
	connect(m_pWebObject, SIGNAL(sigLocation(QString)), this, SLOT(slotLocation(QString)));
	connect(m_pWebObject, SIGNAL(sigOpenUrl(QString)), this, SLOT(slotOpenLink(QString)));
	connect(m_pWebObject, SIGNAL(sigMsgID(QString)), this, SLOT(slotMsgID(QString)));
	connect(m_pWebObject, SIGNAL(sigGetFile(QString)), this, SLOT(slotGetFile(QString)));
	connect(m_pWebObject, SIGNAL(sigSaveFile(QString)), this, SLOT(slotSaveFile(QString)));
	connect(m_pWebObject, SIGNAL(sigCancleLoadorDownLoad(QString)), this, SLOT(slotCancleLoadorDownLoad(QString)));
	/*QString urlName = QDir::currentPath() + ("/html/messageLog.html");
	QUrl url = QUrl::fromUserInput(urlName);*/
    //QUrl url = "qrc:/html/Resources/html/messageLog.html";
    QUrl url = QUrl::fromUserInput("qrc:/html/Resources/html/messageLog.html");
	m_pWebChannel->registerObject("web", m_pWebObject);
	ui->webView->page()->load(url);
	ui->webView->page()->setWebChannel(m_pWebChannel);
	connect(ui->webView, SIGNAL(loadFinished(bool)), this, SIGNAL(sigInitFinished()));
	connect(ui->webView, SIGNAL(loadFinished(bool)), this, SLOT(slotInitFinished()));
	//#endif

	if (gThemeStyle == "White")
	{
		ui->webView->page()->setBackgroundColor(QColor("#ffffff"));
	}
	else if (gThemeStyle == "Blue")
	{
		ui->webView->page()->setBackgroundColor(QColor("#042439"));
	}


	QFile style(":/QSS/Resources/QSS/ChatWidgetShareLib/messagelog.qss");
	style.open(QFile::ReadOnly);
	QString sheet = QLatin1String(style.readAll());
	setStyleSheet(sheet);
	style.close();

	QFile scroolbar_style_qss(":/QSS/Resources/QSS/scrollbarStyle.qss");
	scroolbar_style_qss.open(QFile::ReadOnly);

	QString scrollStyle = scroolbar_style_qss.readAll();

	ui->groupList->verticalScrollBar()->setStyleSheet(scrollStyle);
	ui->perList->verticalScrollBar()->setStyleSheet(scrollStyle);

	//增加对键盘事件的处理。
	ui->perList->installEventFilter(this);
	ui->groupList->installEventFilter(this);

#ifndef Q_OS_WIN
	ui->perList->setStyle(new MyProxyStyle);
	ui->groupList->setStyle(new MyProxyStyle);
#endif
	//隐藏搜索按钮。
	ui->clearBtn->hide();

	connect(ui->minBtn, SIGNAL(clicked()), this, SLOT(showMinimized()));
	connect(ui->maxBtn, SIGNAL(clicked()), this, SLOT(maxOrRstrWindow()));
	connect(ui->closeBtn, SIGNAL(clicked()), this, SLOT(close()));
#ifdef Q_OS_WIN
	connect(ui->closeBtn, SIGNAL(clicked()), shadow, SLOT(hide()));
#endif
	connect(ui->contactsBtn, SIGNAL(toggled(bool)), this, SLOT(doClickContacts(bool)));
	connect(ui->groupBtn, SIGNAL(toggled(bool)), this, SLOT(doClickGroup(bool)));

	connect(ui->perList, SIGNAL(itemClicked(QListWidgetItem *)), this, SLOT(doClickPerItem(QListWidgetItem *)));
	connect(ui->groupList, SIGNAL(itemClicked(QListWidgetItem *)), this, SLOT(doClickGroupItem(QListWidgetItem *)));

	connect(ui->firstPageBtn, SIGNAL(clicked()), this, SLOT(toFirstPage()));
	connect(ui->endPageBtn, SIGNAL(clicked()), this, SLOT(toEndPage()));
	connect(ui->nextPageBtn, SIGNAL(clicked()), this, SLOT(toNextPage()));
	connect(ui->lastPageBtn, SIGNAL(clicked()), this, SLOT(toLastPage()));

	connect(ui->dateBtn, SIGNAL(clicked()), this, SLOT(doShowDateSelector()));
	connect(dateSelector, SIGNAL(clicked(QDate)), this, SLOT(toDateLogPage(QDate)));

	connect(ui->searchBtn, SIGNAL(clicked()), this, SLOT(doSearch()));
	connect(ui->clearBtn, SIGNAL(clicked()), this, SLOT(doClearSearch()));

	ui->perList->hide();
	ui->groupList->hide();

#ifdef Q_OS_LINUX
setLinuxCenter();
#endif
}

MessageLog::~MessageLog()
{
	if (dateSelector)
		delete dateSelector;

	if (mExpressWidget)
		delete mExpressWidget;

	if (m_pPicWidget)
	{
		delete m_pPicWidget;
		m_pPicWidget = NULL;
	}

	if (videoWidget)
		delete videoWidget;

#ifdef Q_OS_WIN
	if (shadow)
		delete shadow;
#endif
	if (detailWidget)
	{
		delete detailWidget;
		detailWidget = NULL;
	}
	if (openPacketWidget)
	{
		delete openPacketWidget;
		openPacketWidget = NULL;
	}
	if (enterWidget)
	{
		delete enterWidget;
		enterWidget = NULL;
	}

	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

void MessageLog::slotInitFinished()
{
	//切换合适的语言
	ui->webView->page()->runJavaScript(QString("switchI18NLocale('%1')").arg(gI18NLocale));

	//切换主题样式
	if (gThemeStyle == "White")
	{
		ui->webView->page()->runJavaScript("changeStyle('dayStyle')");
	}
	else if (gThemeStyle == "Blue")
	{
		ui->webView->page()->runJavaScript("changeStyle('nightStyle')");
	}
}

void MessageLog::changeEvent(QEvent * event)
{
#ifdef Q_OS_WIN
	if (event->type() == QEvent::WindowStateChange)
	{
		if (this->windowState() == Qt::WindowMaximized)
			shadow->hide();
		if (this->windowState() == Qt::WindowMinimized)
			shadow->hide();

		if (this->windowState() == Qt::WindowNoState)
			shadow->show();
	}
#endif
	QWidget::changeEvent(event);
}


void MessageLog::moveEvent(QMoveEvent *event)
{
#ifdef Q_OS_WIN
	shadow->move(event->pos().x() - 10, event->pos().y() - 10);
#endif
}

void MessageLog::resizeEvent(QResizeEvent* event)
{
#ifdef Q_OS_WIN
	shadow->resize(event->size().width() + 20, event->size().height() + 20);
#endif
}

//鼠标事件的处理。
void MessageLog::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	dateSelector->hide();   //隐藏日期选择器。
	return QWidget::mousePressEvent(event);
}
void MessageLog::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void MessageLog::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

bool MessageLog::eventFilter(QObject *obj, QEvent *e)
{
	if (e->type() == QEvent::KeyPress)
	{
		if (obj == ui->perList)
		{
			int currentRow = ui->perList->currentRow();
			int row = currentRow;
			QKeyEvent *keyEvent = (QKeyEvent *)e;
			if (keyEvent->key() == Qt::Key_Up)
			{
				if (row - 1 >= 0)   //最小移到0位的列表项，是合法的。
					row--;
			}
			if (keyEvent->key() == Qt::Key_Down)
			{
				if (row + 1 < ui->perList->count())   //最多移到列表末项，是合法的。
					row++;
			}
			if (row != currentRow)  //通过键盘选择了新的列表项。
			{
				//获得当前聊天对象在消息记录界面的列表项，进行点击处理。
				QListWidgetItem *currentItem = ui->perList->item(row);
				ui->perList->setCurrentItem(currentItem);
				doClickPerItem(currentItem);

				return true;
			}
		}
		if (obj == ui->groupList)
		{
			int currentRow = ui->groupList->currentRow();
			int row = currentRow;
			QKeyEvent *keyEvent = (QKeyEvent *)e;
			if (keyEvent->key() == Qt::Key_Up)
			{
				if (row - 1 >= 0)   //最小移到0位的列表项，是合法的。
					row--;
			}
			if (keyEvent->key() == Qt::Key_Down)
			{
				if (row + 1 < ui->groupList->count())   //最多移到列表末项，是合法的。
					row++;
			}
			if (row != currentRow)  //通过键盘选择了新的列表项。
			{
				//获得当前聊天对象在消息记录界面的列表项，进行点击处理。
				QListWidgetItem *currentItem = ui->groupList->item(row);
				ui->groupList->setCurrentItem(currentItem);
				doClickGroupItem(currentItem);
				return true;
			}
		}
	}
	return QWidget::eventFilter(obj, e);
}

void MessageLog::show()
{
	QWidget::show();
#ifdef Q_OS_WIN
	shadow->resize(width() + 20, height() + 20);
	shadow->move(x() - 10, y() - 10);
	shadow->show();
#endif
#ifdef Q_OS_WIN
	// 窗口被激活了，强制重绘  
	InvalidateRect((HWND)this->winId(), nullptr, FALSE);
#endif
}

void MessageLog::maxOrRstrWindow()
{
	if (this->isMaximized())   //当前窗口已经最大化，进行还原。
	{
		this->showNormal();
		//将按钮图标变为最大化。
		ui->maxBtn->setStyleSheet("QPushButton#maxBtn{border-image:url(:/PerChat/Resources/person/maxsize04.png);}"
			"QPushButton#maxBtn:hover{border-image:url(:/PerChat/Resources/person/maxsize36.png);}");
	}
	else                        //当前尺寸为原始尺寸，因此执行最大化操作。                      
	{
		this->showMaximized();
		//将按钮图标变为还原。
		ui->maxBtn->setStyleSheet("QPushButton#maxBtn{border-image:url(:/PerChat/Resources/person/rstrsize04.png);}"
			"QPushButton#maxBtn:hover{border-image:url(:/PerChat/Resources/person/rstrsize36.png);}");
	}
}

void MessageLog::doClickContacts(bool checked)
{
	if (checked)
	{
		ui->groupList->hide();
		ui->perList->show();

		if (ui->perList->count() == 0)
		{
			QList<BuddyInfo> listBuddyInfo = gDataBaseOpera->DBGetBuddyInfo();
			for (int i = 0; i < listBuddyInfo.size(); i++)
			{
				if (listBuddyInfo[i].BuddyType == 0)
				{
					continue;
				}
				QString strAvatar = (listBuddyInfo[i]).strLocalAvatar;
				QString strNickName = (listBuddyInfo[i]).strNickName;
				QString strBuddyID = QString("%1").arg((listBuddyInfo[i]).nUserId);
				ui->perList->OnInsertLogUserList(strBuddyID, strAvatar, strNickName, 0);
			}
		}
	}
}

void MessageLog::doClickGroup(bool checked)
{
	if (checked)
	{
		ui->perList->hide();
		ui->groupList->show();

		if (ui->groupList->count() == 0)
		{
			QList<GroupInfo> groupInfo = gDataBaseOpera->DBGetAllGroupInfo();
			for (int i = 0; i < groupInfo.size(); i++)
			{
				ui->groupList->OnInsertLogUserList(groupInfo[i].groupId,
					groupInfo[i].groupLoacalHeadImage,
					groupInfo[i].groupName, 1);
			}
		}
	}
}

void MessageLog::loadPerLogByID(QString id)
{
	doClickContacts(true);
	ui->contactsBtn->setChecked(true);
	this->show();             //显示窗口。
	this->activateWindow();
	int currentRow;    //存储当前聊天对象，在好友列表中的行数，之后会打开这个好友的聊天记录。

					   //找到当前聊天对象。
	for (int i = 0; i < ui->perList->count(); i++)
	{
		QListWidgetItem *item = ui->perList->item(i);
		QWidget *widget = ui->perList->itemWidget(item);
		if (widget)
		{
			CFrientStyleWidget *buddy = (CFrientStyleWidget *)widget;
			if (buddy == NULL)
			{
				qDebug() << "MessageLog::loadPerLogByID空指针";
				return;
			}

			QString chatID = buddy->mNickName->objectName();
			if (chatID == id)
			{
				currentRow = i;
				break;
			}
		}
	}

	//获得当前聊天对象在消息记录界面的列表项，进行点击处理。
	QListWidgetItem *currentItem = ui->perList->item(currentRow);
	if (currentItem)
	{
		ui->perList->setCurrentItem(currentItem);
		doClickPerItem(currentItem);
	}
	ui->lineEdit->clear();
}

void MessageLog::loadGroupLogByID(QString id)
{
	doClickGroup(true);
	ui->groupBtn->setChecked(true);
	this->show();             //显示窗口。
	this->activateWindow();
	int currentRow;    //存储当前聊天对象，在部落列表中的行数，之后会打开这个部落的聊天记录。

					   //找到当前聊天对象。
	for (int i = 0; i < ui->groupList->count(); i++)
	{
		QListWidgetItem *item = ui->groupList->item(i);
		if (item)
		{
			QWidget *widget = ui->groupList->itemWidget(item);
			if (widget)
			{
				CFrientStyleWidget *buddy = (CFrientStyleWidget *)widget;
				if (buddy == NULL)
				{
					qDebug() << "MessageLog::loadGroupLogByID空指针";
					return;
				}

				QString chatID = buddy->mNickName->objectName();
				if (chatID == id)
				{
					currentRow = i;
					break;
				}
			}
		}
	}
	//获得当前聊天对象在消息记录界面的列表项，进行点击处理。
	QListWidgetItem *currentItem = ui->groupList->item(currentRow);
	if (currentItem)
	{
		ui->groupList->setCurrentItem(currentItem);
		doClickGroupItem(currentItem);
	}
}

void MessageLog::doClickPerItem(QListWidgetItem *item)
{
	QWidget *widget = ui->perList->itemWidget(item);
	if (!widget)
	{
		return;
	}
	CFrientStyleWidget *buddy = (CFrientStyleWidget *)widget;
	if (buddy == NULL)
	{
		qDebug() << "MessageLog::doClickPerItem空指针";
		return;
	}
	int chatID = buddy->mNickName->objectName().toInt();
	if (keyString.isEmpty())
		pageAmount = gSocketMessage->DBGetMessageRecordPageNum(chatID);
	else
		pageAmount = gSocketMessage->DBGetSearchMessagePageNum(chatID, keyString);

	if (pageAmount == 0)    //消息为空。
	{
		//#ifdef Q_OS_WIN
		//		ui->webView->ExecuteJavaScript("clear()");
		//		ui->webView->ExecuteJavaScript(QString("setPerTitle(\"%1\")").arg(buddy->GetNikeName()));
		//		ui->webView->ExecuteJavaScript("space()");
		//#else
		ui->webView->page()->runJavaScript("clear()");
		ui->webView->page()->runJavaScript(QString("setPerTitle(\"%1\")").arg(buddy->GetNikeName()));
		ui->webView->page()->runJavaScript("space()");
		//#endif
		ui->firstPageBtn->setEnabled(false);
		ui->endPageBtn->setEnabled(false);
		ui->lastPageBtn->setEnabled(false);
		ui->nextPageBtn->setEnabled(false);
	}
	else
	{
		currentPage = 1;
		this->showCurrentPageLog(true);
	}
}

void MessageLog::doClickGroupItem(QListWidgetItem *item)
{
	QWidget *widget = ui->groupList->itemWidget(item);
	if (!widget)
	{
		return;
	}
	CFrientStyleWidget *buddy = (CFrientStyleWidget *)widget;
	if (buddy == NULL)
	{
		qDebug() << "MessageLog::doClickGroupItem空指针";
		return;
	}
	int chatID = buddy->mNickName->objectName().toInt();
	if (keyString.isEmpty())
		pageAmount = gSocketMessage->DBGetMessageRecordPageNum(chatID);
	else
		pageAmount = gSocketMessage->DBGetSearchMessagePageNum(chatID, keyString);

	if (pageAmount == 0)    //消息为空。
	{
		ui->firstPageBtn->setEnabled(false);
		ui->endPageBtn->setEnabled(false);
		ui->lastPageBtn->setEnabled(false);
		ui->nextPageBtn->setEnabled(false);

		//#ifdef Q_OS_WIN
		//        ui->webView->ExecuteJavaScript("clear()");
		//        ui->webView->ExecuteJavaScript(QString("setGroupTitle(\"%1\")").arg(buddy->GetNikeName()));
		//        ui->webView->ExecuteJavaScript("space()");
		//#else
		ui->webView->page()->runJavaScript("clear()");
		ui->webView->page()->runJavaScript(QString("setGroupTitle(\"%1\")").arg(buddy->GetNikeName()));
		ui->webView->page()->runJavaScript("space()");
		//#endif
	}
	else
	{
		currentPage = 1;
		this->showCurrentPageLog(false);
	}
}



//解析内容里的url,转成<a href='http://www.xxx.com'>http://www.xxx.com</a>,其它内容toHtmlEscaped()
QString MessageLog::recognizeUrl(const QString& strMessage)
{
	QString strRet;

	if (strMessage.isEmpty())
		return strMessage;

	QString str_src(strMessage);

	// 从 strMessage 中过滤 url形式的正则表达式
	QRegExp re_url_val("((ht|f)tp(s?)\\:\\/\\/|[0-9a-zA-Z]+\\.)[0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*(:(0-9)*)*(\\/?)([a-zA-Z0-9\\-\\.\\?\\,\\'\\/\\\\\\+&amp;%\\$#=!_\\*]*)?");

	int nFlags = 0;
	while (re_url_val.indexIn(str_src) != -1)
	{
		QString cap_str = re_url_val.cap(0);
		int cur_idx = re_url_val.indexIn(str_src);


		QString leftStr = str_src.left(cur_idx);


		strRet += leftStr.toHtmlEscaped();

		str_src.remove(0, cur_idx + cap_str.length());    // 删除已经遍历过的字符

														  // 格式化为 <a />标签形式
		QString cap_str_link;
		if (cap_str.startsWith("http://")
			|| cap_str.startsWith("https://")
			|| cap_str.startsWith("ftp://")
			|| cap_str.startsWith("ftps://")
			)
		{
			cap_str_link = cap_str;
		}
		else
		{
			cap_str_link = QString("http://") + cap_str;
		}

		strRet += QString("<a href=javascript:window.bridge.slotOpenUrl(&quot;%1&quot;) >%2</a>").arg(QString(cap_str_link).toUtf8().toPercentEncoding(), cap_str.toHtmlEscaped());

		nFlags = 1;
	}
	if (nFlags == 0)
	{
		strRet = strMessage.toHtmlEscaped();

		return strRet;
	}

	strRet += str_src.toHtmlEscaped();

	return strRet;
}

// 将 包含[xx]表情形式的信息字符串 转换为 表情的路径形式并且HTML编码
QString MessageLog::formatMessageFromImgDescriptionWithHtmlEncode(const QString& strMessage)
{
	QString strRet;

	if (strMessage.isEmpty())
		return strMessage;

	QString str_src(strMessage);

	QRegExp re_img_val("\\[[^\\[^\\]]*\\]");      // 从 strMessage 中过滤 [xxx] 形式的正则表达式

	int nFlags = 0;
	while (re_img_val.indexIn(str_src) != -1)
	{
		QString img_descrip = re_img_val.cap(0);
		int cur_idx = re_img_val.indexIn(str_src);

		strRet += recognizeUrl(str_src.left(cur_idx));

		str_src.remove(0, cur_idx + img_descrip.length());    // 删除已经遍历过的字符

		QString img_path = ExpressWidget::GetImagePathByDescription(img_descrip);
		if (!img_path.isEmpty())
		{
			// 格式化为 <img />标签形式
			strRet += "<img src='qrc:/expression/Resources" + img_path + "'/>";
		}
		else
		{
			// 如果没有找到则保持原字符串不变
			strRet += recognizeUrl(img_descrip);
		}
		nFlags = 1;
	}
	if (nFlags == 0)
	{
		strRet = recognizeUrl(strMessage);

		return strRet;
	}

	strRet += recognizeUrl(str_src);

	return strRet;
}



QString MessageLog::GetFileMd5(QString fileNamePath)
{
	QFile theFile(fileNamePath);
	theFile.open(QIODevice::ReadOnly);
	QByteArray ba = QCryptographicHash::hash(theFile.readAll(), QCryptographicHash::Md5);
	theFile.close();
	return QString(ba.toHex());
}

const QString MessageLog::GetSmallImg(QString strPath)
{
	//后缀可能是非法后缀，安卓拍照时传的是.pic后缀名
	strPath.replace("%20", " ");
	QString strSmallPath = "";
	QImageReader reader;
	reader.setDecideFormatFromContent(true);
	reader.setAutoTransform(true);
	reader.setFileName(QString(strPath).replace("file:///", ""));
	if (!reader.canRead())
	{
		return strPath;
	}

	QString strType = strPath.right(strPath.length() - strPath.lastIndexOf('.')).toLower();

	if (strType == ".gif")
	{
		//gif动态图不做转换
		return strPath;
	}

	QImage img = reader.read();

	//图片超过300*300时或者有EXIF旋转信息时则生成缩略图并返回缩略图地址，其它情况返回原图地址
	if ((strType != ".jpg" && strType != ".jpeg" && strType != ".png" && strType != ".gif" && strType != ".bmp") || (img.width() > 300 || img.height() > 300) || reader.transformation() != QImageIOHandler::TransformationNone)
	{
		img = img.scaled(300, 300, Qt::AspectRatioMode::KeepAspectRatio, Qt::TransformationMode::SmoothTransformation);

		strSmallPath = QString("file:///") + QDir::tempPath() + "/" + GetFileMd5(QString(strPath).replace("file:///", "")) + ".small" + strType;
		QString strFixSmallPath = QString(strSmallPath).replace("file:///", "");

		qDebug() << "small pic ===============>>" << strFixSmallPath;

		if (!QFile(strFixSmallPath).exists())
		{
			img.save(strFixSmallPath);

			//若是生成不成功，后缀可能不是标准图片格式，再生成一次
			if (!QFile(strFixSmallPath).exists())
			{
				img.save(strFixSmallPath, "PNG");
			}
		}
	}
	else
	{
		return strPath;
	}


	return strSmallPath;
}


void MessageLog::showCurrentPageLog(bool isPer)
{
	//获得要显示的消息对象的buddy，其中包含chatID跟nickName。
	CFrientStyleWidget *buddy = NULL;
	if (isPer)
		buddy = (CFrientStyleWidget *)ui->perList->itemWidget(ui->perList->currentItem());
	else
		buddy = (CFrientStyleWidget *)ui->groupList->itemWidget(ui->groupList->currentItem());

	if (buddy == NULL)
	{
		qDebug() << "MessageLog::showCurrentPageLog空指针";
		return;
	}
	//获取当前用户的信息，其中包含用户的nickName。
	UserInfo userInfo = gDataManager->getUserInfo();

	//根据用户跟部落的不同，写入标题。用户写“与”XXX的聊天记录，部落是“在”XXX的聊天记录。
	//#ifdef Q_OS_WIN
	//	ui->webView->ExecuteJavaScript("clear()");
	//	if (isPer)
	//		ui->webView->ExecuteJavaScript(QString("setPerTitle(\"%1\")").arg(buddy->GetNikeName()));
	//	else
	//		ui->webView->ExecuteJavaScript(QString("setGroupTitle(\"%1\")").arg(buddy->GetNikeName()));
	//#else
	ui->webView->page()->runJavaScript("clear()");
	if (isPer)
		ui->webView->page()->runJavaScript(QString("setPerTitle(\"%1\")").arg(buddy->GetNikeName()));
	else
		ui->webView->page()->runJavaScript(QString("setGroupTitle(\"%1\")").arg(buddy->GetNikeName()));
	//#endif
	//获取chatID以后，就可以获得当前页的全部消息记录。
	int chatID = buddy->mNickName->objectName().toInt();
	QList<MessageInfo> info;
	if (keyString.isEmpty())
		info = gSocketMessage->DBGetMessageRecordByPage(chatID, currentPage - 1);
	else
		info = gSocketMessage->DBGetSearchMessageRecordByPage(chatID, currentPage - 1);
	QDate currentDate;   //设置一个日期变量用于暂存日期，当消息日期与它不符的时候，就插入一条时间线。
	m_listPic.clear();
	if (m_pPicWidget)
	{
		delete m_pPicWidget;
		m_pPicWidget = NULL;
	}
	for (int i = 0; i < info.count(); i++)
	{
		MessageInfo message = info.at(i);   //获取当前要显示的信息。
		QString string;  //先定义第三个参数，内容。
		QString name;    //获取到第一个参数，发送方的昵称。
		if (userInfo.nUserID == message.nFromUserID)
		{
			string = "send";
			name = userInfo.strUserNickName;
		}
		else
		{
			string = "recv";
			if (isPer)
				name = buddy->GetNikeName();
			else
			{
				QList<BuddyInfo> buddyInfo = gDataBaseOpera->DBGetGroupBuddyInfoFromID(QString::number(chatID));
				for (int j = 0; j < buddyInfo.count(); j++)
				{
					if (buddyInfo.at(j).nUserId == message.nFromUserID)
					{
						name = buddyInfo.at(j).strNickName;
						break;
					}
				}
			}
		}
		QDateTime date = QDateTime::fromTime_t(message.ClientTime / 1000);
		QString time = date.toString("hh:mm:ss");  //获取到第二个参数，发送时间。
												   //在这个地方插入时间线。
		if (i == 0)    //一开始加载消息记录前，先加载时间线。
		{
			currentDate = date.date();
			QString strDate = currentDate.toString("yyyy-MM-dd");

			//#ifdef Q_OS_WIN
			//            ui->webView->ExecuteJavaScript(QString("addDate(\"%1\")").arg(strDate));
			//#else
			ui->webView->page()->runJavaScript(QString("addDate(\"%1\")").arg(strDate));
			//#endif
		}
		else
		{
			//当前消息日期与暂存日期不符，插入一条时间线。
			if (currentDate != date.date())
			{
				currentDate = date.date();
				QString strDate = currentDate.toString("yyyy-MM-dd");

				//#ifdef Q_OS_WIN
				//            ui->webView->ExecuteJavaScript(QString("addDate(\"%1\")").arg(strDate));
				//#else
				ui->webView->page()->runJavaScript(QString("addDate(\"%1\")").arg(strDate));
				//#endif
			}
		}
		QString error;
		if (message.MessageChildType == MessageType::Message_TEXT)  //文字
		{
			QString strMessage = formatMessageFromImgDescriptionWithHtmlEncode(message.strMsg);
			strMessage = strMessage.toUtf8().toPercentEncoding();

			string += QString("Text(\"%1\",\"%2\",\"%3\")").arg(name, time, strMessage);
		}
		if (message.MessageChildType == MessageType::Message_PIC)  //图片
		{
			QString strPicPath = QString("file:///") + message.strMsg;
			QString strSmallPicPath = GetSmallImg(strPicPath);
			string += QString("Img(\"%1\",\"%2\",\"%3\",\"%4\")").arg(name,time , strPicPath, strSmallPicPath);
			m_listPic.push_back(message.strMsg);
		}
		if (message.MessageChildType == MessageType::Message_AUDIO)  //音频
		{
			QString audioPath = message.strMsg;
			if (audioPath.endsWith(".amr"))
			{
				QString wavPath = audioPath;
				wavPath.replace(".amr", ".wav");

				QFile audioFile(wavPath);
				if (!audioFile.exists())
				{
#ifdef Q_OS_WIN
					AmrDec amrDec;
					amrDec.convertAmrToWav(audioPath, wavPath);
#else
					QString appPath = QDir::currentPath() + "/ffmpeg";
					QStringList arguments;
					arguments << "-i" << audioPath << wavPath;
					QProcess process(this);
					process.start(appPath, arguments);
					process.waitForFinished();
					process.close();
#endif
				}

				audioPath = wavPath;
			}
			QFile file(audioPath);
			int iSi = file.size();
			int duration = CalWavLength(audioPath);//file.size() / BYTES_PER_SECOND;
			string += QString("Audio(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\")").arg(name).arg(time).arg(audioPath).arg(duration).arg(QString(message.msgID));
		}
		if (message.MessageChildType == MessageType::Message_VEDIO)  //视频
		{
			message.strMsg.replace(" ", "%20");
			QString strPicPath = message.strMsg.left(message.strMsg.indexOf("."));
			strPicPath += ".png";
			VedioFrameOpera pVdo;
			pVdo.CreateVedioPicture(message.strMsg, strPicPath);
			//QString strPhotoImgPath = QString("<img height:auto width:auto src='file:///%1' name = '%2' onload='movePlayIcon(this)'/> ").arg(strPicPath).arg(message.strMsg);
			QString strVideoPicPath = "file:///"+ strPicPath;//视频第一帧图
			QString strVideoPath = message.strMsg;//视频路径

			string += QString("Video(\"%1\",\"%2\",\"%3\",\"%4\")").arg(name,time,strVideoPicPath,strVideoPath);//arg新写法避免路径中存在%号引起BUG
		}

		if (message.MessageChildType == MessageType::Message_FILE)  //文件
		{
			QString strFlag = "0";
			QJsonDocument document = QJsonDocument::fromJson(message.strMsg.toUtf8());
			QString fileName = gDataBaseOpera->DBGetFileInfoLocalPath(message.msgID);
			if (fileName.isEmpty())
			{
				strFlag = "0";
				QJsonValue vname = document.object().take("FileName");
				fileName = vname.toString();
			}
			else
			{
				strFlag = "2";
			}

			QJsonValue value = document.object().take("FileSize");
			QString fileSize = value.toString();
			QString fileIcon = "qrc:/FileType/Resources/FileType/007.png";  //默认赋值一个未知图标。
														//根据不同的文件类型赋值不同的文件图标路径。
														//先获取扩展名。
			QFileInfo fileInfo(fileName);
			QString suffix = fileInfo.suffix().toLower();
			//罗列所有的文件类型。
			QStringList program, zip, text, image, audio, video;
			QStringList word, excel, ppt;
			program << "exe" << "bat" << "msi";
			zip << "rar" << "zip" << "7z";
			text << "txt" << "pdf" << "htm" << "html" << "xml";
			image << "png" << "ico" << "jpg" << "jpeg" << "bmp" << "gif";
			audio << "wav" << "mp3" << "wma" << "aac" << "flac";
			video << "avi" << "wmv" << "3gp" << "mp4" << "rmvb" << "mov" << "mkv";
			video << "rm" << "mpg" << "mpeg" << "ogg";
			word << "doc" << "docx";
			excel << "xls" << "xlsx";
			ppt << "ppt" << "pptx";
			if (program.contains(suffix))
				fileIcon = "qrc:/FileType/Resources/FileType/012.png";
			if (zip.contains(suffix))
				fileIcon = "qrc:/FileType/Resources/FileType/008.png";
			if (text.contains(suffix))
				fileIcon = "qrc:/FileType/Resources/FileType/010.png";
			if (image.contains(suffix))
				fileIcon = "qrc:/FileType/Resources/FileType/005.png";
			if (audio.contains(suffix))
				fileIcon = "qrc:/FileType/Resources/FileType/013.png";
			if (video.contains(suffix))
				fileIcon = "qrc:/FileType/Resources/FileType/004.png";
			if (word.contains(suffix))
				fileIcon = "qrc:/FileType/Resources/FileType/002.png";
			if (excel.contains(suffix))
				fileIcon = "qrc:/FileType/Resources/FileType/003.png";
			if (ppt.contains(suffix))
				fileIcon = "qrc:/FileType/Resources/FileType/001.png";

			string += QString("File(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",\"%6\",\"%7\")").arg(QString(message.msgID)).arg(name).arg(time).arg(fileIcon).arg(fileName).arg(fileSize).arg(strFlag);
		}
		if (message.MessageChildType == MessageType::Message_NOTICE)
		{
			QString msg = message.strMsg;
			QJsonDocument doc = QJsonDocument::fromJson(message.strMsg.toUtf8());
			QVariantMap map = doc.toVariant().toMap();
			QString imageUrl = map.value("imageUrl").toString();
			QString imageTitle = map.value("imageTitle").toString();
			QString webUrl = map.value("webUrl").toString();
			QString webTitle = map.value("webTitle").toString();

			string += QString("Notice(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",\"%6\",\"%7\");")
				.arg(name).arg(time)
				.arg("").arg(imageTitle).arg(webUrl).arg(webTitle).arg(QString(message.msgID));

			/////////////////图片方向
			QString strFilePath = QDir::tempPath() + "/" + QUuid::createUuid().toString() + ".jpg";
			HttpNetWork::HttpDownLoadFile *netWork = new HttpNetWork::HttpDownLoadFile;
			netWork->setData(QVariant::fromValue(message));
			netWork->setObjectName(strFilePath);
			connect(netWork, SIGNAL(sigDownFinished(bool)), this, SLOT(slotUpdateNotice(bool)));
			netWork->StartDownLoadFile(imageUrl, strFilePath);
			/////////////////
		}
		if (message.MessageChildType == MessageType::Message_LOCATION)
		{
			QString msg = message.strMsg;
			QJsonDocument doc = QJsonDocument::fromJson(message.strMsg.toUtf8());
			QVariantMap map = doc.toVariant().toMap();
			QString imageUrl = map.value("url").toString();
			QString imageTitle = map.value("name").toString();
			QString webTitle = map.value("specific").toString();

			string += QString("Location(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",\"%6\");")
				.arg(name).arg(time).arg(QString(message.msgID))
				.arg(imageUrl).arg(imageTitle).arg(webTitle);
		}
		if (message.MessageChildType == MessageType::Message_COMMON)
		{
			QJsonDocument doc = QJsonDocument::fromJson(message.strMsg.toUtf8());
			QVariantMap map = doc.toVariant().toMap();

			QString image = map.value("image").toString();
			QString smallIcon = map.value("smallIcon").toString();
			QString title = map.value("title").toString();
			QString content = map.value("content").toString();
			QString systemName = map.value("systemName").toString();

			string += QString("Common('%1', '%2', '%3', '%4', '%5', '%6', '%7', '%8');")
				.arg(name).arg(time).arg(image).arg(smallIcon).arg(title).arg(content).arg(systemName).arg(QString(message.msgID));
		}
		if (message.MessageChildType == MessageType::Message_SECRETLETTER)
		{
			string += QString("SecretText('%1', '%2', '%3');").arg(name).arg(time).arg(QString(message.msgID));
		}
		if (message.MessageChildType == MessageType::Message_SECRETIMAGE)
		{
			string += QString("SecretPic('%1', '%2', '%3');").arg(name).arg(time).arg(QString(message.msgID));
		}
		if (message.MessageChildType == MessageType::Message_SECRETFILE)
		{
			string += QString("SecretFile('%1', '%2', '%3');").arg(name).arg(time).arg(QString(message.msgID));
		}
		if (message.MessageChildType == MessageType::Message_TRANSFER)
		{
			UserInfo userInfo = gDataManager->getUserInfo();
			QString userID = QString::number(userInfo.nUserID);
			QJsonDocument json = QJsonDocument::fromJson(message.strMsg.toUtf8());
			QVariantMap map = json.toVariant().toMap();
			QString transfer;
			string += QString("Transfer('%1', '%2', '%3');").arg(name).arg(time).arg(QString(message.msgID));
		}
		if (message.MessageChildType == MessageType::Message_AT)
		{
			QJsonDocument doc = QJsonDocument::fromJson(message.strMsg.toUtf8());
			QVariantMap map = doc.toVariant().toMap();

			QString content = map.value("content").toString();

 			QString strMessage = formatMessageFromImgDescriptionWithHtmlEncode(content);
 			strMessage = strMessage.toUtf8().toPercentEncoding();

 			string += QString("Text(\"%1\",\"%2\",\"%3\")").arg(name, time, strMessage);
		}
		if (message.MessageChildType == MessageType::Message_REDBAG)
		{
			QJsonDocument json = QJsonDocument::fromJson(message.strMsg.toUtf8());
			QVariantMap map = json.toVariant().toMap();
			QString invalid = map.value("invalid").toString();
			QString remarks;
			if (invalid == "true")    //已经领取/过期等等。
			{
				QVariantMap packet = map.value("redPacket").toMap();
				remarks = packet.value("remarks").toString();
			}
			else                      //还未领取。
			{
				remarks = map.value("leaveMessage").toString();
			}

			if (remarks.isEmpty())
				remarks = tr("Best Wishes");
			if (ui->perList->isVisible() && string == "send")
				invalid = "false";
			QString strSend = QString("RedPacketAppend(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\");")
				.arg(name, time, remarks, QString(message.msgID), invalid);
			string += strSend;
		}
		//在webview中执行显示方法。
		//#ifdef Q_OS_WIN
		//        if (error.isEmpty())
		//            ui->webView->ExecuteJavaScript(string);
		//        else
		//            qDebug() << error;
		//#else
		if (error.isEmpty())
			ui->webView->page()->runJavaScript(string);
		else
			qDebug() << error;
		//#endif

	}

#ifdef Q_OS_WIN
#else
	ui->webView->page()->runJavaScript("scrollToTop()");
#endif
	//显示完毕后对四个翻页按钮的状态进行设置。
	ui->firstPageBtn->setEnabled(false);
	ui->endPageBtn->setEnabled(false);
	ui->lastPageBtn->setEnabled(false);
	ui->nextPageBtn->setEnabled(false);

	if (currentPage > 1)
	{
		ui->firstPageBtn->setEnabled(true);
		ui->lastPageBtn->setEnabled(true);
	}
	if (currentPage < pageAmount)
	{
		ui->nextPageBtn->setEnabled(true);
		ui->endPageBtn->setEnabled(true);
	}
}

void MessageLog::toFirstPage()
{
	currentPage = 1;
	showCurrentPageLog(ui->perList->isVisible());
}
void MessageLog::toEndPage()
{
	currentPage = pageAmount;
	showCurrentPageLog(ui->perList->isVisible());
}
void MessageLog::toLastPage()
{
	currentPage--;
	showCurrentPageLog(ui->perList->isVisible());
}
void MessageLog::toNextPage()
{
	currentPage++;
	showCurrentPageLog(ui->perList->isVisible());
}

void MessageLog::slotZoomImg(QString strId)
{
// 	MessageInfo msgInfo = gSocketMessage->DBGetMessageInfoWithMsgID(strId);
// 	if (msgInfo.MessageState == MESSAGE_STATE_UNLOADED || msgInfo.MessageChildType != Message_PIC)
// 	{
// 		return;
// 	}
// 	QFile file(msgInfo.strMsg);
// 	if (!file.exists())
// 	{
// 		QJsonParseError jsonError;
// 		QJsonDocument jsonDocument = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8(), &jsonError);
// 		if (jsonError.error == QJsonParseError::NoError)
// 		{
// 			if (jsonDocument.isObject())
// 			{
// 				QVariantMap result = jsonDocument.toVariant().toMap();
// 				qDebug() << "img:" << result["path"].toString();
// 				msgInfo.strMsg = result["path"].toString();
// 			}
// 		}
// 	}
	sigOpenPic(strId, &m_listPic, this);
	//#ifdef Q_OS_WIN
	// #ifdef Q_OS_WIN
	// 	QString fileName = strfileName.mid(strfileName.indexOf("file:///") + 8);
	// #else
	//     QString fileName = strfileName.mid(strfileName.indexOf("file:///") + 7);
	// #endif
	// 	if (m_pPicWidget != NULL)
	// 	{
	// 		delete m_pPicWidget;
	// 	}
	// 	fileName = decodeURI(fileName);
	// 	m_pPicWidget = new PicWidget();
	// 	connect(m_pPicWidget, SIGNAL(sigPicClose()), this, SLOT(slotPicClose()));
	// 	connect(m_pPicWidget, SIGNAL(sigChangePic(int)), this, SLOT(slotChangePic(int)));
	// 	m_pPicWidget->setPath(fileName, this);
	// 
	// 	for (m_PicIter = m_listPic.begin(); m_PicIter != m_listPic.end(); m_PicIter++)
	// 	{
	// 		//m_PicIter
	// 		QString strTmp = *m_PicIter;
	// 		if (strTmp == fileName)
	// 		{
	// 			break;
	// 		}
	// 	}
	// 	m_pPicWidget->show();
	//#else
	// QDesktopServices::openUrl(QUrl(strfileName));
	//#endif
}

void MessageLog::slotOpenFile(QString msgID)
{
	QString fileName = gDataBaseOpera->DBGetFileInfoLocalPath(msgID);

	if (fileName == "")
	{
		MessageInfo tmpifno = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);
		QJsonParseError jsonError;
		QString strFileName;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(tmpifno.strMsg.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QVariantMap result = jsonDocument.toVariant().toMap();
				strFileName = result["FileName"].toString();
			}
		}
		UserInfo userInfo = gDataManager->getUserInfo();
		QString currentPath = gSettingsManager->getUserPath();
		fileName = currentPath + QString("/files/") + QString("%1").arg(userInfo.nUserID) + QString("/") + strFileName;
	}
	QFile file(fileName);
	if (fileName == "" || !file.exists())
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("Empty file name or file doesn't exist!"));
	}
	else
	{
		QDesktopServices bs;
		bs.openUrl(QUrl::fromLocalFile(fileName));
	}
}

void MessageLog::slotOpenDir(QString msgID)
{
	QString fileName = gDataBaseOpera->DBGetFileInfoLocalPath(msgID);
	if (fileName == "")
	{
		MessageInfo tmpifno = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);
		QJsonParseError jsonError;
		QString strFileName;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(tmpifno.strMsg.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QVariantMap result = jsonDocument.toVariant().toMap();
				strFileName = result["FileName"].toString();
			}
		}
		UserInfo userInfo = gDataManager->getUserInfo();
		QString currentPath = gSettingsManager->getUserPath();
		fileName = currentPath + QString("/files/") + QString("%1").arg(userInfo.nUserID) + QString("/") + strFileName;
	}
	if (fileName == "")
	{
		qDebug() << tr("fileName为空!OpenDocument Failed!") << endl;
	}
	else
	{
#ifdef Q_OS_WIN
		//windows系统实现定位到文件功能
		const QString explorer = "explorer";
		QStringList param;
		if (!QFileInfo(fileName).isDir())
			param << QLatin1String("/select,");
		param << QDir::toNativeSeparators(fileName);
		QProcess::startDetached(explorer, param);
#else
		QString filePath = fileName.left(fileName.lastIndexOf("/"));
		QDesktopServices::openUrl(QUrl::fromLocalFile(filePath));
#endif

	}
}

void MessageLog::doShowDateSelector()
{
	if (dateSelector->isVisible())
		dateSelector->hide();
	else
	{
		QPoint toolPos = ui->toolWidget->pos();
		int x = toolPos.x();
		int y = toolPos.y() - dateSelector->height();
		dateSelector->move(x, y);
		dateSelector->show();
	}
}

void MessageLog::toDateLogPage(QDate date)
{
	//获得要显示的消息对象的buddy。
	CFrientStyleWidget *buddy = NULL;
	if (ui->perList->isVisible())
		buddy = (CFrientStyleWidget *)ui->perList->itemWidget(ui->perList->currentItem());
	else
		buddy = (CFrientStyleWidget *)ui->groupList->itemWidget(ui->groupList->currentItem());
	if (buddy == NULL)
	{
		qDebug() << "MessageLog::toDateLogPage空指针";
		return;
	}
	int chatID = buddy->mNickName->objectName().toInt();
	int page = -1;    //定义一个变量保存符合条件的页码，如果没有，负数可用于判空。
					  //遍历所有的消息记录。
	for (int i = 0; i < pageAmount; i++)
	{
		QList<MessageInfo> info = gSocketMessage->DBGetMessageRecordByPage(chatID, i);
		for (int j = 0; j < info.count(); j++)
		{
			MessageInfo message = info.at(j);
			QDateTime time = QDateTime::fromTime_t(message.ClientTime / 1000);

			if (date == time.date())  //当前页符合条件。
			{
				page = i;
				break;
			}
		}
		if (page >= 0)
			break;
	}

	if (page >= 0)    //如果找到了符合条件的页码。
	{
		ui->dateLabel->setText(date.toString("yyyy-MM-dd"));
		//不是当前页再进行加载。
		if (currentPage != page + 1)   //currentpage存储的是1为开头的页码，所以需要加1。
		{
			currentPage = page + 1;
			showCurrentPageLog(ui->perList->isVisible());
		}
		//#ifdef Q_OS_WIN
		//		//滚动至目标区域。
		//		ui->webView->ExecuteJavaScript(QString("scrollDate(\"%1\")").arg(date.toString("yyyy-MM-dd")));
		//#else
		ui->webView->page()->runJavaScript(QString("scrollDate(\"%1\")").arg(date.toString("yyyy-MM-dd")));
		//#endif
		//隐藏时间选择器。
		dateSelector->hide();
	}
	else
	{
		//提示没有符合条件的页码。
		IMessageBox::tip(this, tr("Sorry"), tr("There are no qualified message records"));
	}
}

void MessageLog::doSearch()
{
	if (ui->lineEdit->text().isEmpty())
		IMessageBox::tip(this, tr("Notice"), tr("Unable to search empty content"));
	else
	{
		this->keyString = ui->lineEdit->text();
		if (ui->perList->isVisible())
		{
			QListWidgetItem *item = ui->perList->currentItem();
			if (item)
			{
				doClickPerItem(item);
			}
		}
		else
		{
			QListWidgetItem *item = ui->groupList->currentItem();
			if (item)
			{
				doClickGroupItem(item);
			}
		}

		this->keyString = "";
	}
}

void MessageLog::doClearSearch()
{
	ui->lineEdit->clear();
	this->keyString = "";
	if (ui->perList->isVisible())
		doClickPerItem(ui->perList->currentItem());
	else
		doClickGroupItem(ui->groupList->currentItem());
}

void MessageLog::slotOpenLink(QString link)
{
	if (link.contains("."))
		QDesktopServices::openUrl(QUrl::fromLocalFile(link));
	else
		slotOpenSecret(link);
}
void MessageLog::slotLocation(QString msgID)
{
	MessageInfo msgInfo = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);
	QJsonDocument json = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = json.toVariant().toMap();
	QString strlatitude = map.value("latitude").toString();
	QString strlongitude = map.value("longitude").toString();
	QString strname = map.value("name").toString();
	QString strspecific = map.value("specific").toString();
	QString strLink = "http://api.map.baidu.com/marker?";
	strLink += "location=" + strlatitude + "," + strlongitude;
	strLink += "&title=" + strname + "&content=" + strspecific;
	strLink += "&coord_type=gcj02&output=html";
	QDesktopServices::openUrl(QUrl(strLink));
}
void MessageLog::slotPicClose()
{
	m_pPicWidget = NULL;
}

void MessageLog::slotChangePic(int iType)
{
	QString strText = *m_PicIter;
	if (iType == 0)//上一个
	{
		if (m_PicIter != m_listPic.begin())
		{
			m_PicIter--;
			m_pPicWidget->resetPath(*m_PicIter);
			if (m_PicIter == m_listPic.begin())
			{
				m_pPicWidget->setNoteTip(1);
			}
			else
			{
				m_pPicWidget->setNoteTip(0);
			}
		}
		else
		{
			m_pPicWidget->setNoteTip(1);
		}
	}
	else if (iType == 1)
	{
		if (m_PicIter + 1 != m_listPic.end())
		{
			m_PicIter++;
			m_pPicWidget->resetPath(*m_PicIter);
			if (m_PicIter + 1 == m_listPic.end())
			{
				m_pPicWidget->setNoteTip(2);
			}
			else
			{
				m_pPicWidget->setNoteTip(0);
			}
		}
		else
		{
			m_pPicWidget->setNoteTip(2);
		}
	}
}

QString MessageLog::decodeURI(QString str)
{
	QByteArray array;
	for (int i = 0; i < str.length();) {
		if (0 == QString::compare(str.mid(i, 1), QString("%"))) {
			if ((i + 2) < str.length()) {
				array.append(str.mid(i + 1, 2).toShort(0, 16));
				i = i + 3;
			}
			else {
				array.append(str.mid(i, 1));
				i++;
			}
		}
		else {
			if (str.mid(i, 1) == "+")
				array.append(" ");
			else
				array.append(str.mid(i, 1));
			i++;
		}
	}
	QTextCodec *code = QTextCodec::codecForName("UTF-8");
	return code->toUnicode(array);
}

void MessageLog::slotCloseVideoWidget()
{
	videoWidget->close();
	videoWidget = NULL;
}

void MessageLog::slotVideoPlay(QString mediaPath)
{
	if (videoWidget)
		videoWidget->close();
	videoWidget = new IMVideoPlayer;
	connect(videoWidget, SIGNAL(sigClose()), this, SLOT(slotCloseVideoWidget()));
	videoWidget->videoPlay(mediaPath);
}
void MessageLog::slotOpenSecret(QString msgID)
{
	UserInfo userInfo = gDataManager->getUserInfo();
	QMap<QString, QList<MessageInfo> > mapTemp = gSocketMessage->DBGetAllMessage();
	QMap<QString, QList<MessageInfo> >::iterator itor = mapTemp.begin();
	for (; itor != mapTemp.end(); ++itor)
	{
		for (int i = 0; i < itor.value().size(); i++)
		{
			if (itor.value()[i].msgID == msgID.toUtf8())
			{
				//已经找到指定的消息。
				MessageInfo message = itor.value()[i];
				if (itor.value()[i].nFromUserID == userInfo.nUserID)
				{
					//用户发送的。
					if (message.MessageChildType == Message_SECRETLETTER)
					{
						OpenLetterWidget *openWidget = new OpenLetterWidget(message.strMsg);
						openWidget->show();
					}
					if (message.MessageChildType == Message_SECRETIMAGE || message.MessageChildType == Message_SECRETFILE)
					{
						if (enterWidget)
							enterWidget->close();
						enterWidget = new EnterPasswordWidget(message);
						connect(enterWidget, SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget*)), this, SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget*)));//图片浏览
						connect(enterWidget, SIGNAL(sigQuit()), this, SLOT(slotCloseEnterWidget()));//图片浏览
					}
				}
				else
				{
					//对方发送的。
					if (enterWidget)
						enterWidget->close();
					enterWidget = new EnterPasswordWidget(message);
					connect(enterWidget, SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget*)), this, SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget*)));//图片浏览
					connect(enterWidget, SIGNAL(sigQuit()), this, SLOT(slotCloseEnterWidget()));//图片浏览
				}
			}
		}
	}
}

void MessageLog::slotMsgID(QString msgID)
{
	MessageInfo msgInfo = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);

	if (msgInfo.MessageChildType == MessageType::Message_TRANSFER)
	{
		QJsonDocument json = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
		QVariantMap map = json.toVariant().toMap();
		RecordInfo record;

		UserInfo userInfo = gDataManager->getUserInfo();
		QString userID = QString::number(userInfo.nUserID);
		if (userID == map.value("sendIMUserId").toString())
		{
			//用户是发送者。
			record.status = "sender";
		}
		else
		{
			record.status = "receiver";
		}
		record.age = map.value("tradingTime").toString();
		record.from = map.value("sendWalletAddress").toString();
		record.to = map.value("receiveWalletAddress").toString();
		record.txHash = map.value("tradingID").toString();
		record.value = map.value("money").toString();
		record.txFee = map.value("txFee").toString();
		record.block = map.value("block").toString().toInt();
		record.sendId = map.value("sendIMUserId").toString();
		record.receiveId = map.value("receiveIMUserId").toString();

		emit sigTransferRecord(record);
	}
	if (msgInfo.MessageChildType == MessageType::Message_COMMON)
	{
		QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		QString userType = map.value("userType").toString();
		int id = map.value("id").toInt();
		if (userType == "user")
		{
			emit profilemanager::getInstance()->sigCreatePerFile(map.value("id").toString());
		}
		if (userType == "group")
		{
			emit profilemanager::getInstance()->sigCreateGrpFile(map.value("id").toString());
		}
	}
	if (msgInfo.MessageChildType == MessageType::Message_REDBAG)
	{
		UserInfo user = gDataManager->getUserInfo();

		QJsonDocument json = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
		QVariantMap map = json.toVariant().toMap();
		QString invalid = map.value("invalid").toString();

		if (msgInfo.nToUserID == user.nUserID || !ui->perList->isVisible())  //接收红包的处理。
		{
			if (invalid == "true")
			{
				//显示红包详情页面。
				if (detailWidget)
					detailWidget->close();
				detailWidget = new RedPackDetail;
				connect(detailWidget, SIGNAL(sigClose()), this, SLOT(slotCloseDetailWidget()));
				//红包发送者信息
				double coin = map.value("data").toDouble();
				if (coin < 0)
					coin = map.value("coin").toDouble();

				BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(QString::number(msgInfo.nFromUserID));
				if (buddy.strNote.isEmpty())
					buddy.strNote = buddy.strNickName;
				detailWidget->setMyInfo(buddy.strLocalAvatar, buddy.strNote, QString::number(coin, 'f', 5));

				//红包金额和领取状态信息
				QVariantMap packet = map.value("redPacket").toMap();
				QString remarks = packet.value("remarks").toString();
				double totalMoney = packet.value("totalMoney").toDouble();
				int totalSize = packet.value("totalSize").toInt();
				int remainSize = packet.value("remainSize").toInt();
				int trusteeshipCoinId = packet.value("trusteeshipCoinId").toInt();
				QString assetUnit;
				if (trusteeshipCoinId == 1)
					assetUnit = "PWR";
				detailWidget->setPackInfo(remarks, QString::number(totalMoney, 'f', 5), assetUnit, totalSize, totalSize - remainSize);

				//红包领取记录信息
				QVariantList list = map.value("list").toList();
				foreach(QVariant varRecord, list)
				{
					QVariantMap record = varRecord.toMap();
					QDateTime dateTime = QDateTime::fromTime_t(record.value("time").toLongLong() / 1000);
					double money = record.value("coin").toDouble();
					detailWidget->addPerson(user.strUserAvatarLocal, user.strUserNickName,
						dateTime.date().toString("yyyy-MM-dd"), dateTime.time().toString("hh:mm:ss"),
						QString::number(money, 'f', 5), assetUnit);
				}

				detailWidget->show();
			}
			else
			{
				QString packetId = map.value("packetId").toString();

				OPRequestShareLib *request = new OPRequestShareLib;
				connect(request, SIGNAL(sigPacketStatus(bool)), this, SLOT(slotPacketStatus(bool)));
				request->setObjectName(msgID);
				request->readRedBagStatus(QString::number(user.nUserID),
					packetId);
			}
		}
		else
		{
			int trusteeshipCoinId;
			QString packetId;

			if (invalid == "true")  //已经领取/失效
			{
				QVariantMap redPacket = map.value("redPacket").toMap();
				trusteeshipCoinId = redPacket.value("trusteeshipCoinId").toInt();
				packetId = redPacket.value("packetId").toString();
			}
			else                    //尚未领取
			{
				trusteeshipCoinId = map.value("trusteeshipCoinId").toInt();
				packetId = map.value("packetId").toString();
			}

			UserInfo user = gDataManager->getUserInfo();
			OPRequestShareLib *request = new OPRequestShareLib;
			request->setObjectName(msgID);
			connect(request, SIGNAL(sigGetPacketResult(QString)), this, SLOT(slotGetPacketResult(QString)));
			request->getRedBag(user.strAccountName, QString::number(user.nUserID),
				QString::number(trusteeshipCoinId), packetId);
		}
	}
}

int MessageLog::CalWavLength(QString strPath)
{
	int iSize = 0;
	QFile file(strPath);
	file.open(QIODevice::ReadOnly);
	QDataStream in(&file);
	in.setByteOrder(QDataStream::LittleEndian);
	unsigned int iRate = 0;
	in.skipRawData(12);
	char ch[5];
	in.readRawData(ch, 4);
	ch[4] = '\0';
	QString strType(ch);

	if (strType == "fmt ")
	{
		in.skipRawData(12);
		in >> iRate;
	}
	else if (strType == "JUNK")
	{
		unsigned int iJunk;
		unsigned short sNum;
		in >> iJunk;
		in.skipRawData(iJunk);
		in.readRawData(ch, 4);
		in >> iRate;
		in >> sNum;
		in >> sNum;
		in >> iRate;
		//in.setByteOrder(QDataStream::BigEndian);
		in >> iRate;
	}
	else
	{
		iRate = BYTES_PER_SECOND;
	}

	iSize = file.size() / iRate;
	return iSize;
}

void MessageLog::slotGetPacketResult(QString result)
{
	if (!result.isEmpty())
	{
		//获得msgID。
		OPRequestShareLib *request = qobject_cast<OPRequestShareLib *>(sender());
		QString msgID = request->objectName();

		//获取用户信息，消息结构体。
		UserInfo user = gDataManager->getUserInfo();
		MessageInfo msgInfo = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);

		//给消息体增加已经失效的标志，并写入数据库。
		QJsonDocument json = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = json.toVariant().toMap();
		map.insert("invalid", "true");
		QJsonDocument doc = QJsonDocument::fromVariant(map);
		gSocketMessage->DBUpdateMessageContent(msgID.toUtf8(), doc.toJson());

		if (msgInfo.nToUserID == user.nUserID)  //接收红包的处理
		{
			//设置红包为已打开状态。
			QString strSend = QString("setColor('%1');").arg(msgID);
			ui->webView->page()->runJavaScript(strSend);

			//显示红包详情页面。
			if (detailWidget)
				detailWidget->close();
			detailWidget = new RedPackDetail;
			connect(detailWidget, SIGNAL(sigClose()), this, SLOT(slotCloseDetailWidget()));

			//红包发送者信息
			double coin = map.value("data").toDouble();
			if (coin < 0)
				coin = map.value("coin").toDouble();

			BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(QString::number(msgInfo.nFromUserID));
			if (buddy.strNote.isEmpty())
				buddy.strNote = buddy.strNickName;
			detailWidget->setMyInfo(buddy.strLocalAvatar, buddy.strNote, QString::number(coin, 'f', 5));

			//红包金额和领取状态信息
			QVariantMap packet = map.value("redPacket").toMap();
			QString remarks = packet.value("remarks").toString();
			double totalMoney = packet.value("totalMoney").toDouble();
			int totalSize = packet.value("totalSize").toInt();
			int remainSize = packet.value("remainSize").toInt();
			int trusteeshipCoinId = packet.value("trusteeshipCoinId").toInt();
			QString assetUnit;
			if (trusteeshipCoinId == 1)
				assetUnit = "PWR";
			detailWidget->setPackInfo(remarks, QString::number(totalMoney, 'f', 5), assetUnit, totalSize, totalSize - remainSize);

			//红包领取记录信息
			QVariantList list = map.value("list").toList();
			foreach(QVariant varRecord, list)
			{
				QVariantMap record = varRecord.toMap();
				QDateTime dateTime = QDateTime::fromTime_t(record.value("time").toLongLong() / 1000);
				double money = record.value("coin").toDouble();
				detailWidget->addPerson(user.strUserAvatarLocal, user.strUserNickName,
					dateTime.date().toString("yyyy-MM-dd"), dateTime.time().toString("hh:mm:ss"),
					QString::number(money, 'f', 5), assetUnit);
			}

			detailWidget->show();
		}
		else    //发送红包的处理。
		{
			//显示红包详情页面。
			if (detailWidget)
				detailWidget->close();
			detailWidget = new RedPackDetail;
			connect(detailWidget, SIGNAL(sigClose()), this, SLOT(slotCloseDetailWidget()));

			//红包发送者信息
			detailWidget->setMyInfo(user.strUserAvatarLocal, user.strUserNickName, "0");

			//红包金额和领取状态信息
			QVariantMap packet = map.value("redPacket").toMap();
			QString remarks = packet.value("remarks").toString();
			double totalMoney = packet.value("totalMoney").toDouble();
			int totalSize = packet.value("totalSize").toInt();
			int remainSize = packet.value("remainSize").toInt();
			int trusteeshipCoinId = packet.value("trusteeshipCoinId").toInt();
			QString assetUnit;
			if (trusteeshipCoinId == 1)
				assetUnit = "PWR";
			detailWidget->setPackInfo(remarks, QString::number(totalMoney, 'f', 5), assetUnit, totalSize, totalSize - remainSize);

			//红包领取记录信息
			QVariantList list = map.value("list").toList();
			foreach(QVariant varRecord, list)
			{
				QVariantMap record = varRecord.toMap();
				QString imUserId = record.value("imUserId").toString();
				QString nickName = record.value("nickName").toString();
				QDateTime dateTime = QDateTime::fromTime_t(record.value("time").toLongLong() / 1000);
				double money = record.value("coin").toDouble();
				BuddyInfo recordBuddy = gDataBaseOpera->DBGetBuddyInfoByID(imUserId);
				UserInfo user = gDataManager->getUserInfo();
				if(recordBuddy.nUserId==-1)
					recordBuddy = gDataBaseOpera->DBGetGroupUserFromID(imUserId);
				QString strPath = recordBuddy.strLocalAvatar;
				if (user.nUserID == imUserId)
					strPath = user.strUserAvatarLocal;
				bool best = false;
				if(list.size()>1)
					best = record.value("best").toInt() == 1 ? true : false;
				detailWidget->addPerson(strPath, nickName,
					dateTime.date().toString("yyyy-MM-dd"), dateTime.time().toString("hh:mm:ss"),
					QString::number(money, 'f', 5), assetUnit, best);
			}

			detailWidget->show();
		}
	}
	else
	{
		IMessageBox::tip(this, tr("Notice"), tr("Failed to check the red packet!"));
	}
}

void MessageLog::slotCloseDetailWidget()
{
	detailWidget->close();
	detailWidget = NULL;
}

void MessageLog::slotPacketStatus(bool valid)
{
	OPRequestShareLib *request = qobject_cast<OPRequestShareLib *>(sender());
	QString msgID = request->objectName();
	MessageInfo msgInfo = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);
	UserInfo user = gDataManager->getUserInfo();
	if (valid)
	{
		if (openPacketWidget)
			openPacketWidget->close();
		openPacketWidget = new OpenPacketWidget;
		connect(openPacketWidget, SIGNAL(sigClose()), this, SLOT(slotClosePacketWidget()));
		connect(openPacketWidget, SIGNAL(sigOpenPacket(QString)), this, SLOT(slotOpenPacket(QString)));

		BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(QString::number(msgInfo.nFromUserID));
		QString name = buddy.strNote;
		if (buddy.strNote.isEmpty())
			name = buddy.strNickName;

		QJsonDocument json = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
		QVariantMap map = json.toVariant().toMap();
		QString remarks = map.value("leaveMessage").toString();

		openPacketWidget->setInfo(buddy.strLocalAvatar, name, msgID, remarks, commonPack);
		openPacketWidget->show();
	}
	else
	{
		QJsonDocument json = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
		QVariantMap map = json.toVariant().toMap();
		int trusteeshipCoinId = map.value("trusteeshipCoinId").toInt();
		QString packetId = map.value("packetId").toString();
		UserInfo user = gDataManager->getUserInfo();

		OPRequestShareLib *request = new OPRequestShareLib;
		request->setObjectName(msgID);
		connect(request, SIGNAL(sigGetPacketResult(QString)), this, SLOT(slotGetPacketResult(QString)));
		request->getRedBag(user.strAccountName, QString::number(user.nUserID),
			QString::number(trusteeshipCoinId), packetId);
	}
}

void MessageLog::slotClosePacketWidget()
{
	openPacketWidget->close();
	openPacketWidget = NULL;
}

void MessageLog::slotOpenPacket(QString msgID)
{
	slotClosePacketWidget();

	MessageInfo msgInfo = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);
	QJsonDocument json = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = json.toVariant().toMap();
	int trusteeshipCoinId = map.value("trusteeshipCoinId").toInt();
	QString packetId = map.value("packetId").toString();
	UserInfo user = gDataManager->getUserInfo();

	OPRequestShareLib *request = new OPRequestShareLib;
	request->setObjectName(msgID);
	connect(request, SIGNAL(sigGetPacketResult(QString)), this, SLOT(slotGetPacketResult(QString)));
	request->getRedBag(user.strAccountName, QString::number(user.nUserID),
		QString::number(trusteeshipCoinId), packetId);
}

void MessageLog::slotCloseEnterWidget()
{
	enterWidget->close();
	enterWidget = NULL;
}

void MessageLog::setLinuxCenter()
{
    window()->setGeometry
    (
            QStyle::alignedRect
            (
                    Qt::LeftToRight,
                    Qt::AlignCenter,
                    window()->size(),
                    qApp->desktop()->availableGeometry()
            )
    );
}
void MessageLog::slotUpdateNotice(bool)
{
	HttpNetWork::HttpDownLoadFile *act = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());
	if (!act)
	{
		return;
	}
	QVariant var = act->getData();
	MessageInfo msgInfo = var.value<MessageInfo>();
	QString filePath = GetSmallImg(act->objectName());

	QString strSend = QString("UpdateNoticeImg(\"%1\",\"%2\");")
		.arg(QString(msgInfo.msgID)).arg(filePath);

	ui->webView->page()->runJavaScript(strSend);
}

void MessageLog::closeEvent(QCloseEvent * event)
{
	emit sigClose();
	QWidget::closeEvent(event);
}

void MessageLog::slotGetFile(QString msgID)
{
	MessageInfo messageInfo = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);
	QJsonDocument jsonDocument = QJsonDocument::fromJson(messageInfo.strMsg.toUtf8());
	QVariantMap result = jsonDocument.toVariant().toMap();
	QString fileName = result["FileName"].toString();
	UserInfo userInfo = gDataManager->getUserInfo();
#ifdef Q_OS_WIN
	QDir dir;
	QString currentPath = gSettingsManager->getUserPath();
#else
	QString currentPath = gSettingsManager->getUserPath();
#endif
	QString strFilePath = currentPath + QString("/files/") + QString("%1").arg(userInfo.nUserID) + QString("/") + fileName;

	QFile recvFile(strFilePath);
	if (recvFile.exists())
	{
		QuestionBox *box = new QuestionBox(NULL);
		connect(box, SIGNAL(sigEnter()), this, SLOT(slotCoverFile()));
		box->setData(QVariant::fromValue(messageInfo));
		box->init(tr("Notice"), tr("This file already exists, overwrite or not?"));
		box->show();
	}
	else
	{
		QString fileID = result["FileId"].toString();

		QString strUlID = QString(messageInfo.msgID) + QString("recv");
		QString coverFile = QString("coverFile(\"%1\")").arg(strUlID);
		QString strUlID2 = QString(messageInfo.msgID) + QString("recv2");
		//QString changeLi = QString("ChangeLiTwo(\"%1\")").arg(strUlID2);
		//#ifdef Q_OS_WIN
		//		ui->mWebView->ExecuteJavaScript(changeLi);
		//		ui->mWebView->ExecuteJavaScript(coverFile);
		//#else
		//ui->mWebView->page()->runJavaScript(changeLi);
		ui->webView->page()->runJavaScript(coverFile);
		//#endif

		AppConfig configInfo = gDataManager->getAppConfigInfo();
		QString httpUrl = QString(configInfo.PanServerDownloadURL) + fileID + QString("/download");
		HttpNetWork::HttpDownLoadFile *netWork = new HttpNetWork::HttpDownLoadFile;
		netWork->setData(QVariant::fromValue(messageInfo));
		netWork->setObjectName(strFilePath);
		connect(netWork, SIGNAL(sigDownFinished(bool)), this, SLOT(slotRequestHttpFileResult(bool)));
		connect(netWork, SIGNAL(sigDownFailed()), this, SLOT(slotDownFailed()));
		connect(netWork, SIGNAL(sigDownloadProgress(qint64, qint64)), this, SLOT(slotDownLoadFileProgress(qint64, qint64)));
		connect(this, SIGNAL(sigCancle(QString)), netWork, SLOT(slotLoadorDownLoadCancle(QString)));
		netWork->StartDownLoadFile(httpUrl, strFilePath);
	}
}

void MessageLog::slotDownLoadFileProgress(qint64 recvnum, qint64 total)
{
	HttpNetWork::HttpDownLoadFile *act = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());
	if (act)
	{
		QVariant var = act->getData();
		MessageInfo msgInfo = var.value<MessageInfo>();
		if (total > 0)
		{
			int num = recvnum * 300 / total;
			QString barId = msgInfo.msgID + "bar";
			QString strend = QString("ProgressBar(\"%1\",%2)").arg(barId).arg(num);
			//#ifdef Q_OS_WIN
			//    ui->mWebView->ExecuteJavaScript(strend);
			//#else
			ui->webView->page()->runJavaScript(strend);
			//#endif
		}
		else {
			QString barId = msgInfo.msgID + "bar";
			QString strend = QString("ProgressBar(\"%1\",%2)").arg(barId).arg(0);
			//#ifdef Q_OS_WIN
			//    ui->mWebView->ExecuteJavaScript(strend);
			//#else
			ui->webView->page()->runJavaScript(strend);
			//#endif
		}
	}
}
void MessageLog::slotCoverFile()
{
	QuestionBox *box = qobject_cast<QuestionBox *>(sender());
	QVariant msgVar = box->getData();
	MessageInfo messageInfo = msgVar.value<MessageInfo>();
	QJsonDocument jsonDocument = QJsonDocument::fromJson(messageInfo.strMsg.toUtf8());
	QVariantMap result = jsonDocument.toVariant().toMap();
	QString fileID = result["FileId"].toString();

	ChangeFileState(messageInfo.msgID, 1);

	AppConfig configInfo = gDataManager->getAppConfigInfo();

	QString httpUrl = QString(configInfo.PanServerDownloadURL) + fileID + QString("/download");

	QString fileName = result["FileName"].toString();
	UserInfo userInfo = gDataManager->getUserInfo();

	QDir dir;
#ifdef Q_OS_WIN
	QString currentPath = gSettingsManager->getUserPath();
#else
	QString currentPath = gSettingsManager->getUserPath();
#endif

	QString strFilePath = currentPath + QString("/files/") + QString("%1").arg(userInfo.nUserID) + QString("/") + fileName;
	QFile recvFile(strFilePath);
	recvFile.remove();


	HttpNetWork::HttpDownLoadFile *netWork = new HttpNetWork::HttpDownLoadFile;
	netWork->setData(QVariant::fromValue(messageInfo));
	netWork->setObjectName(strFilePath);
	connect(netWork, SIGNAL(sigDownFinished(bool)), this, SLOT(slotRequestHttpFileResult(bool)));
	connect(netWork, SIGNAL(sigDownFailed()), this, SLOT(slotDownFailed()));
	connect(netWork, SIGNAL(sigDownloadProgress(qint64, qint64)), this, SLOT(slotDownLoadFileProgress(qint64, qint64)));
	connect(this, SIGNAL(sigCancle(QString)), netWork, SLOT(slotLoadorDownLoadCancle(QString)));
	netWork->StartDownLoadFile(httpUrl, strFilePath);
}

void MessageLog::ChangeFileState(QString strMsgId, int iState)
{
	QString strScript = QString("ChangeFileState(\"%1\",\"%2\")").arg(strMsgId).arg(iState);
	ui->webView->page()->runJavaScript(strScript);
}
void MessageLog::slotRequestHttpFileResult(bool result)
{
	HttpNetWork::HttpDownLoadFile *act = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());
	if (act)
	{
		QVariant var = act->getData();
		MessageInfo msgInfo = var.value<MessageInfo>();
		QString filePath = act->objectName();
		if (result)
		{
			gDataBaseOpera->DBOnInsertFileInfo(QString(msgInfo.msgID), filePath, "");//将接收到的文件信息插入数据库

			QString strUlID = msgInfo.msgID + QString("recv");
			QString strChange = QString("ChangeLiTwo(\"%1\")").arg(strUlID);
			//#ifdef Q_OS_WIN
			//    ui->mWebView->ExecuteJavaScript(strChange);
			//#else
			//@ui->mWebView->page()->runJavaScript(strChange);
			//#endif

			QString strUlID2 = msgInfo.msgID + QString("recv2");
			QString strChange2 = QString("ChangeLiTwo(\"%1\")").arg(strUlID2);
			//#ifdef Q_OS_WIN
			//    ui->mWebView->ExecuteJavaScript(strChange2);
			//#else
			//@ui->mWebView->page()->runJavaScript(strChange2);
			//#endif
			ChangeFileState(msgInfo.msgID, 2);
		}
		else {
			QString strUlID = msgInfo.msgID + QString("recv");
			QString strChange = QString("ChangeLiThr(\"%1\")").arg(strUlID);
			//#ifdef Q_OS_WIN
			//    ui->mWebView->ExecuteJavaScript(strChange);
			//#else
			//@ui->mWebView->page()->runJavaScript(strChange);
			//#endif

			QString strUlID2 = msgInfo.msgID + QString("recv2");
			QString strChange2 = QString("ChangeLiThr(\"%1\")").arg(strUlID2);
			//#ifdef Q_OS_WIN
			//            ui->mWebView->ExecuteJavaScript(strChange2);
			//#else
			//@ui->mWebView->page()->runJavaScript(strChange2);
			//#endif
			ChangeFileState(msgInfo.msgID, 0);
		}
	}
}

void MessageLog::slotDownFailed()
{
	HttpNetWork::HttpDownLoadFile *act = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());
	if (act)
	{
		QVariant var = act->getData();
		MessageInfo msgInfo = var.value<MessageInfo>();
		ChangeFileState(msgInfo.msgID, 3);
	}
}

bool MessageLog::slotSaveFile(QString msgID)
{
	const QString DEFAULT_DIR_KEY("default_dir");
	QSettings MySettings;
	QString default_dir = MySettings.value(DEFAULT_DIR_KEY).toString();

	MessageInfo messageInfo = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);
	QJsonDocument jsonDocument = QJsonDocument::fromJson(messageInfo.strMsg.toUtf8());
	QVariantMap result = jsonDocument.toVariant().toMap();
	QString fileName = result["FileName"].toString();
	QString fileID = result["FileId"].toString();
	QString fileExt = result["FileType"].toString();

	QString strFilePath = QFileDialog::getSaveFileName(this, tr("Save as"), (default_dir.isEmpty() ? "" : default_dir + "/") + fileName);
	if (strFilePath == "")
	{
		return false;
	}

	if (QFileInfo(strFilePath).suffix().toLower() != fileExt.toLower())
	{
		//无后缀，自动添加后缀
		strFilePath += "." + fileExt;
	}

	QFileInfo fileInfo(strFilePath);
	MySettings.setValue(DEFAULT_DIR_KEY, fileInfo.path());

	QString strUlID2 = QString(msgID) + QString("recv2");
	//QString changeLi = QString("ChangeLiTwo(\"%1\")").arg(strUlID2);
	QString strUlID = QString(msgID) + QString("recv");
	QString coverFile = QString("coverFile(\"%1\")").arg(strUlID);
	//#ifdef Q_OS_WIN
	//    ui->mWebView->ExecuteJavaScript(changeLi);
	//	ui->mWebView->ExecuteJavaScript(coverFile);
	//#else
	//ui->mWebView->page()->runJavaScript(changeLi);
	ui->webView->page()->runJavaScript(coverFile);
	//#endif



	AppConfig configInfo = gDataManager->getAppConfigInfo();
	QString httpUrl = QString(configInfo.PanServerDownloadURL) + fileID + QString("/download");

	HttpNetWork::HttpDownLoadFile *netWork = new HttpNetWork::HttpDownLoadFile;
	netWork->setData(QVariant::fromValue(messageInfo));
	netWork->setObjectName(strFilePath);
	connect(netWork, SIGNAL(sigDownFinished(bool)), this, SLOT(slotRequestHttpFileResult(bool)));
	connect(netWork, SIGNAL(sigDownFailed()), this, SLOT(slotDownFailed()));
	connect(netWork, SIGNAL(sigDownloadProgress(qint64, qint64)), this, SLOT(slotDownLoadFileProgress(qint64, qint64)));
	connect(this, SIGNAL(sigCancle(QString)), netWork, SLOT(slotLoadorDownLoadCancle(QString)));
	netWork->StartDownLoadFile(httpUrl, strFilePath);
	return true;
}

void MessageLog::slotCancleLoadorDownLoad(QString msgId)
{
	emit sigCancle(msgId);
}