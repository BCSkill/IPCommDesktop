﻿#include "chatdatamanager.h"
#include "QStringLiteralBak.h"

ChatDataManager::ChatDataManager(QObject *parent)
	: QObject(parent)
{
	messageLog = NULL;
	chatWidget = new ChatWidget();
	searchList = chatWidget->getSearchList();
	messageList = chatWidget->getMessageList();
	m_pPicWidget = NULL;

	readHistoryMessage();
	stackedWidget = chatWidget->getStackedWidget();

	connect(chatWidget, SIGNAL(sigShowDeviceWidget()), this, SIGNAL(sigShowDeviceWidget()));
	connect(chatWidget, SIGNAL(sigSearchText(QString)), this, SLOT(slotSearchText(QString)));
	connect(searchList, SIGNAL(sigOpenChat(int, QVariant)), this, SLOT(recvChat(int, QVariant)));
	connect(searchList, SIGNAL(sigOpenChat(int, QVariant)), chatWidget, SLOT(slotClickCancelSearch()));

	connect(messageList, SIGNAL(sigDoubleClickItem(QString)), this, SLOT(slotClickedChat(QString)));
	connect(messageList, SIGNAL(sigCloseChat(QString)), this, SLOT(slotCloseChat(QString)));
	connect(messageList, SIGNAL(sigDealTrayIocnFlash(QString)), this, SIGNAL(sigDealTrayIocnFlash(QString)));
	connect(messageList, SIGNAL(sigMessageNum(int)), this, SIGNAL(sigCountMessageNum(int)));

	connect(this, SIGNAL(sigUpdateSelfMessage(bool, QVariant, QString)), messageList, SLOT(slotInsertSelfMessage(bool, QVariant, QString)));

    if(messageList->count() > 0)
    {
        QListWidgetItem *item = messageList->item(0);
        if(item)
        {
            item->setSelected(true);
            messageList->itemPressed(item);
        }
    }

  /*  if(messageList->m_pMsglistModel->rowCount() > 0)
    {
        messageList->doClickIndex(0);
    }*/

	//加载设备登录信息，因为刚登录时获取到的信息不完整，因此延时一秒进行请求。
	QTimer::singleShot(1000, this, SLOT(slotRefreshDeviceInfo()));
}

ChatDataManager::~ChatDataManager()
{
	if (m_pPicWidget)
		delete m_pPicWidget;
}

ChatWidget * ChatDataManager::getChatWidget()
{
	return chatWidget;
}

void ChatDataManager::recvChat(int type, QVariant message, QVariant info)
{
	switch (type)
	{
	case PerMessage:
		OnDealPerMessage(message,info);
		break;
	case GroupMessage:
		OnDealGroupMessage(message, info);
		break;
	case OpenPer:
		OnDealOpenPerMessage(message);
		break;
	case OpenGroup:
		OnDealOpenGroupMessage(message);
		break;
	case BuddyUpdate:
		OnDealBuddyUpdateMessage(message);
		break;
	case GroupUpdate:
		OnDealGroupUpdateMessage(message);
		break;
	case QuitGroup:
		OnDealQuitGroupMessage(message, info);
		break;
	case GroupAddUser:
		OnDealGroupAddUserMessage(message, info);
		break;
	case GroupNoSpeak:
		OnDealGroupNoSpeak(message, info);
		break;
	default:
		break;
	}
}

// Qualifier: 处理接收到的个人消息
void ChatDataManager::OnDealPerMessage(QVariant message, QVariant info)
{
	MessageInfo messageInfo = message.value<MessageInfo>();
	BuddyInfo buddyInfo = info.value<BuddyInfo>();
	if (messageInfo.strMsg == "load" && messageInfo.MessageChildType != MessageType::Message_TEXT)
	{
		//如果判断出是加载消息（非文字消息且内容为load），就不在messageList显示。
	}
	else
	{
		MessageListInfo listInfo = messageList->OnInsertPerToPerMessage(messageInfo, buddyInfo);
		gDataBaseOpera->DBInsertPerMessageListInfo(listInfo);
	}

	IMPerChat *perChat = NULL;
	for (int i = 0; i < stackedWidget->count(); i++)
	{
		QWidget *widget = stackedWidget->widget(i);
		if (widget)
		{
			QString widgetID = widget->objectName();
			if (widgetID.toInt() == buddyInfo.nUserId)
				perChat = (IMPerChat *)widget;
		}
	}

	if (perChat)
	{
		UserInfo userInfo = gDataManager->getUserInfo();
		/*判断是不是当前窗口，如果是当前窗口，需要设置消息已读*/
		QWidget *currentWidget = stackedWidget->currentWidget();
		if (currentWidget)
		{
			if ((IMPerChat *)currentWidget == perChat)
			{
				QList<QByteArray> msgIDList = gSocketMessage->SetPerMessageRead(messageInfo.nFromUserID);
				for (int i = 0; i < msgIDList.size(); i++)
				{
					gSocketMessage->SetMessageState(msgIDList[i], MESSAGE_STATE_READ);
					gSocketMessage->SendMessageReadMessage(userInfo.nUserID, messageInfo.nFromUserID, 0, QString(msgIDList[i]));
				}
			}
		}

		//插入时间线。
		perChat->onInsertTimeLine(messageInfo.ServerTime);

		switch (messageInfo.MessageChildType)
		{
		case 0: // 文字消息
			perChat->OnInsertRecMessageTextInfo(messageInfo.strMsg, buddyInfo.strLocalAvatar, QString(messageInfo.msgID), messageInfo.integral);
			break;
		case 1: // 图片消息
			perChat->OnRecvImgMessage(messageInfo, buddyInfo.strLocalAvatar);
			break;
		case 2:  //音频消息。
			perChat->OnRecvAudioMessage(messageInfo, buddyInfo.strLocalAvatar);
			break;
		case 3: //视频消息
			perChat->OnRecVdoMessage(messageInfo, buddyInfo.strLocalAvatar);
			break;
		case 5://文件消息
			if (gDataBaseOpera)
			{
				QString strLocalFilePath = gDataBaseOpera->DBGetFileInfoLocalPath(messageInfo.msgID);
				perChat->OnRecvFileMessage(messageInfo, buddyInfo.strLocalAvatar, strLocalFilePath);
			}
			break;
		case 8: //转账消息。
			perChat->onRecvTransferMessage(messageInfo, buddyInfo.strLocalAvatar);
			break;
		case 20:  //密文消息。
		{
		    perChat->onRecvSecretLetter(messageInfo, buddyInfo.strLocalAvatar);
			break;
		}
		case 21: //密图消息。
		{
			perChat->onRecvSecretImage(messageInfo, buddyInfo.strLocalAvatar);
			break;
		}
		case 22: //密图消息。
		{
			perChat->onRecvSecretFile(messageInfo, buddyInfo.strLocalAvatar);
			break;
		}
		case 23:  //通告消息。
			perChat->OnRecvNoticeMessage(messageInfo, buddyInfo.strLocalAvatar);
			break;
		case 55:  //定位消息。
			perChat->onRecvLocationMessage(messageInfo, buddyInfo.strLocalAvatar);
			break;
		case 62:      //任务消息
			perChat->OnRecvCommonMessage(messageInfo, buddyInfo.strLocalAvatar);
			break;
		case 80:
			perChat->OnRecvGWMessage(messageInfo, buddyInfo.strLocalAvatar);
			break;
		case MessageType::Message_NOTIFY:
			perChat->OnRecvNotifyMessage(messageInfo);
		default:
			break;
		}
	}
}

// Qualifier: 处理接收到的群组消息
void ChatDataManager::OnDealGroupMessage(QVariant message, QVariant info)
{
	MessageInfo messageInfo = message.value<MessageInfo>();
	GroupInfo groupInfo = info.value<GroupInfo>();
	if (messageInfo.strMsg == "load" && messageInfo.MessageChildType != MessageType::Message_TEXT)
	{
		//如果判断出是加载消息（非文字消息且内容为load），就不在messageList显示。
	}
	else
	{
		MessageListInfo listInfo = messageList->OnInsertPerToGroupMessage(messageInfo, groupInfo);
		gDataBaseOpera->DBInsertGroupMessageListInfo(listInfo);
	}

	GroupWidget *groupChat = NULL;
	for (int i = 0; i < stackedWidget->count(); i++)
	{
		QWidget *widget = stackedWidget->widget(i);
		if (widget)
		{
			QString widgetID = widget->objectName();
			if (widgetID == groupInfo.groupId)
				groupChat = (GroupWidget *)widget;
		}
	}
	if (groupChat)
	{
		UserInfo userInfo = gDataManager->getUserInfo();
		/*判断是不是当前窗口，如果是当前窗口，需要设置消息已读*/
		QWidget *currentWidget = stackedWidget->currentWidget();
		if (currentWidget)
		{
			if ((GroupWidget *)currentWidget == groupChat)
			{
				QList<QByteArray> msgIDList = gSocketMessage->SetPerMessageRead(messageInfo.nFromUserID);
				for (int i = 0; i < msgIDList.size(); i++)
				{
					gSocketMessage->SetMessageState(msgIDList[i], MESSAGE_STATE_READ);
					//gSocketMessage->SendMessageReadMessage(userInfo.nUserID, messageInfo.nFromUserID, 1, QString(msgIDList[i]));
				}
			}
		}
		groupChat->OnInsertRevMessage(messageInfo);
	}
}

// Qualifier: 处理接收到的打开个人消息
void ChatDataManager::OnDealOpenPerMessage(QVariant message)
{
	MessageInfo messageInfo;
	messageInfo.ClientTime = QDateTime::currentMSecsSinceEpoch();
	messageInfo.MessageChildType = 0;
	//填充消息
	UserInfo userInfo = gDataManager->getUserInfo();
	BuddyInfo buddyInfo = gDataBaseOpera->DBGetBuddyInfoByID(message.toString());
	int iId = message.toInt();
	QMap<QString, QList<MessageInfo> > mapTemp = gSocketMessage->DBGetRecnetMessage(userInfo.nUserID, iId);
	QMap<QString, QList<MessageInfo> >::iterator itor = mapTemp.begin();
	messageInfo.strMsg = "";
	for (; itor != mapTemp.end(); ++itor)
	{
		if (itor.key() == message.toString())
		{
			MessageInfo MsgTmp = itor.value().last();
			messageInfo.strMsg = messageList->OnDealMessageType(MsgTmp);
			messageInfo.ClientTime = MsgTmp.ClientTime;
			break;
		}
	}

	//从群组中打开个人聊天界面的情况
	if (buddyInfo.strNickName.isEmpty())
		buddyInfo = gDataBaseOpera->DBGetGroupUserFromID(message.toString());

	MessageListInfo listInfo = messageList->OnClickPerMessage(messageInfo, buddyInfo);
	gDataBaseOpera->DBInsertPerMessageListInfo(listInfo);
	IMPerChat *perChat = getPerChat(QString::number(buddyInfo.nUserId));
	if (perChat)
	{
		stackedWidget->setCurrentWidget(perChat);
#ifdef Q_OS_WIN
		gDataManager->setCurrentChatWidget(perChat->winId());
#endif
	}
}

// Qualifier: 处理接收到的打开群组消息
void ChatDataManager::OnDealOpenGroupMessage(QVariant message)
{
	MessageInfo messageInfo;
	messageInfo.ClientTime = QDateTime::currentMSecsSinceEpoch();
	messageInfo.MessageChildType = 0;
	//填充消息
	UserInfo userInfo = gDataManager->getUserInfo();
	GroupInfo groupInfo = gDataBaseOpera->DBGetGroupFromID(message.toString());
	QMap<QString, QList<MessageInfo> > mapTemp = gSocketMessage->DBGetRecnetMessage(userInfo.nUserID, groupInfo.groupId.toInt());
	QString strID = groupInfo.groupId;
	QMap<QString, QList<MessageInfo> >::iterator itor = mapTemp.begin();
	messageInfo.strMsg = "";
	for (; itor != mapTemp.end(); ++itor)
	{
		if (itor.key() == strID)
		{
			MessageInfo MsgTmp = mapTemp[strID].last();
			messageInfo.nFromUserID = MsgTmp.nFromUserID;
			messageInfo.nToUserID = MsgTmp.nToUserID;
			messageInfo.strMsg = messageList->OnDealMessageType(MsgTmp);
			messageInfo.ClientTime = MsgTmp.ClientTime;
			break;
		}
	}
	MessageListInfo listInfo = messageList->OnClickGroupMessage(messageInfo, groupInfo);
	gDataBaseOpera->DBInsertGroupMessageListInfo(listInfo);
	GroupWidget *groupWidget = getGroupChat(groupInfo.groupId);
	if (groupWidget)
	{
		stackedWidget->setCurrentWidget(groupWidget);
#ifdef Q_OS_WIN
		gDataManager->setCurrentChatWidget(groupWidget->getChatWidgetID());
#endif
	}
}

// Qualifier: 处理接收到的更新好友消息
void ChatDataManager::OnDealBuddyUpdateMessage(QVariant message)
{
	messageList->OnUpdateMessage(false, message);
	BuddyInfo buddyInfo = message.value<BuddyInfo>();
	for (int i = 0; i < stackedWidget->count(); i++)
	{
		QWidget *widget = stackedWidget->widget(i);
		if (widget)
		{
			if (widget->objectName() == QString::number(buddyInfo.nUserId))
			{
				IMPerChat *perChat = (IMPerChat *)widget;
				if (buddyInfo.strNote.isEmpty())
					perChat->OnSetNikeName(buddyInfo.strNickName, QString::number(buddyInfo.nUserId));
				else
					perChat->OnSetNikeName(buddyInfo.strNote, QString::number(buddyInfo.nUserId));
				break;
			}
		}
	}
}

// Qualifier: 处理接收到的更新群组消息
void ChatDataManager::OnDealGroupUpdateMessage(QVariant message)
{
	messageList->OnUpdateMessage(true, message);
	GroupInfo groupInfo = message.value<GroupInfo>();
	for (int i = 0; i < stackedWidget->count(); i++)
	{
		QWidget *widget = stackedWidget->widget(i);
		if (widget)
		{
			if (widget->objectName() == groupInfo.groupId)
			{
				GroupWidget *groupChat = (GroupWidget *)widget;

				groupChat->OnSetGroupName(groupInfo.groupName, groupInfo.groupId);
				break;
			}
		}
	}
}

// Qualifier: 处理接收到的退出群组消息
void ChatDataManager::OnDealQuitGroupMessage(QVariant message, QVariant info)
{
	QString groupID = message.toString();
	QString userID = info.toString();

	UserInfo userInfo = gDataManager->getUserInfo();
	if (userInfo.nUserID == userID.toInt() || userID.isEmpty())
	{
		slotCloseChat(groupID);
	}
	else
	{
		for (int i = 0; i < stackedWidget->count(); i++)
		{
			QWidget *widget = stackedWidget->widget(i);
			if (widget)
			{
				if (widget->objectName() == groupID)
				{
					GroupWidget *groupChat = (GroupWidget *)widget;

					groupChat->userQuitGroup(userID);
					break;
				}
			}
		}
	}
}

// Qualifier: 处理接收到的群组添加用户消息
void ChatDataManager::OnDealGroupAddUserMessage(QVariant message, QVariant info)
{
	QVariantMap map = QJsonDocument::fromJson(message.toString().toUtf8()).toVariant().toMap();
	QString groupID = map.value("groupID").toString();
	QString manaID = map.value("manaID").toString();

	BuddyInfo buddyInfo = info.value<BuddyInfo>();
	for (int i = 0; i < stackedWidget->count(); i++)
	{
		QWidget *widget = stackedWidget->widget(i);
		if (widget)
		{
			if (widget->objectName() == groupID)
			{
				GroupWidget *groupChat = (GroupWidget *)widget;
				groupChat->OnInsertGroupUser(manaID, buddyInfo);
				break;
			}
		}
	}
}

void ChatDataManager::OnDealGroupNoSpeak(QVariant id, QVariant noSpeak)
{
	QString groupID = QString::number(id.toInt());
	for (int i = 0; i < stackedWidget->count(); i++)
	{
		QWidget *widget = stackedWidget->widget(i);
		if (widget)
		{
			if (widget->objectName() == groupID)
			{
				GroupWidget *groupChat = (GroupWidget *)widget;
				groupChat->setNoSpeak(noSpeak.toInt());
				break;
			}
		}
	}
}

IMPerChat * ChatDataManager::getPerChat(QString uesrID)
{
	IMPerChat *perChat = NULL;
	for (int i = 0; i < stackedWidget->count(); i++)
	{
		QWidget *widget = stackedWidget->widget(i);
		if (widget)
		{
			QString widgetID = widget->objectName();
			if (widgetID == uesrID)
				perChat = (IMPerChat *)widget;
		}
	}

	if (perChat == NULL)
	{
		perChat = new IMPerChat(uesrID);
		connect(perChat, SIGNAL(sigOpenPerLog(QString)), this, SLOT(slotShowPerLog(QString)));
		connect(perChat, SIGNAL(sigWidgetMinSize()), this, SIGNAL(sigWidgetMinSize()));
		connect(perChat, SIGNAL(sigShowNormalWindow()), this, SIGNAL(sigShowNormalWindow()));
		connect(perChat, SIGNAL(sigKeyUpDown(QKeyEvent *)), messageList, SLOT(slotKeyUpDown(QKeyEvent *)));
		connect(perChat, SIGNAL(sigUpdateSelfMessage(bool, QVariant, QString)), messageList, SLOT(slotInsertSelfMessage(bool, QVariant, QString)));
		connect(perChat, SIGNAL(sigTransferRecord(RecordInfo)), this, SIGNAL(sigTransferRecord(RecordInfo)));
		connect(perChat, SIGNAL(sigAddChatGroup(QString)), this, SIGNAL(sigAddChatGroup(QString)));
		connect(perChat, SIGNAL(sigTransmit(QString)), this, SLOT(slotTransmitWidget(QString)));
        connect(perChat, SIGNAL(sigTransferWindow(QString,QString)), this, SIGNAL(sigTransferWindow(QString,QString)));
		connect(perChat, SIGNAL(sigETHWindow(QString, QString)), this, SIGNAL(sigETHWindow(QString, QString)));
		connect(perChat, SIGNAL(sigBTCWindow(QString, QString)), this, SIGNAL(sigBTCWindow(QString, QString)));
		connect(perChat, SIGNAL(sigEOSWindow(QString, QString)), this, SIGNAL(sigEOSWindow(QString, QString)));
		connect(perChat, SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget*)), this, SLOT(slotOpenPic(QString, QList<QString>*, QWidget*)));//图片浏览
		connect(perChat, SIGNAL(sigShareID(int, QString)), this, SLOT(slotShareWidget(int, QString)));
		connect(perChat, SIGNAL(sigOpenChat(int, QVariant)), this, SLOT(recvChat(int, QVariant)));
		
		BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(uesrID);;

		if (buddy.strNickName.isEmpty())
			buddy = gDataBaseOpera->DBGetGroupUserFromID(uesrID);

		if (buddy.strNote.isEmpty())
			perChat->OnSetNikeName(buddy.strNickName, uesrID);
		else
			perChat->OnSetNikeName(buddy.strNote, uesrID);
		
		stackedWidget->addWidget(perChat);
	}

	return perChat;
}

GroupWidget * ChatDataManager::getGroupChat(QString groupID)
{
	GroupWidget *groupChat = NULL;
	for (int i = 0; i < stackedWidget->count(); i++)
	{
		QWidget *widget = stackedWidget->widget(i);
		if (widget)
		{
			QString widgetID = widget->objectName();
			if (widgetID == groupID)
				groupChat = (GroupWidget *)widget;
		}
	}

	if (groupChat == NULL)
	{
		groupChat = new GroupWidget(groupID);
		connect(groupChat, SIGNAL(sigShowGroupBuddyPerChat(int, QVariant)), this, SLOT(recvChat(int, QVariant)));
		connect(groupChat, SIGNAL(sigOpenGroupLog(QString)), this, SLOT(slotOpenGroupLog(QString)));
		connect(groupChat, SIGNAL(sigGroupMinSize()), this, SIGNAL(sigWidgetMinSize()));
		connect(groupChat, SIGNAL(sigShowNormalWindow()), this, SIGNAL(sigShowNormalWindow()));
		connect(groupChat, SIGNAL(sigKeyUpDown(QKeyEvent *)), messageList, SLOT(slotKeyUpDown(QKeyEvent *)));
		connect(groupChat, SIGNAL(sigUpdateSelfImage(int, QPixmap)), this, SIGNAL(sigUpdateSelfImage(int, QPixmap)));
		connect(groupChat, SIGNAL(sigUpdateSelfMessage(bool, QVariant, QString)), messageList, SLOT(slotInsertSelfMessage(bool, QVariant, QString)));
		connect(groupChat, SIGNAL(sigMakeGroupHeader(QString)), this, SIGNAL(sigMakeGroupHeader(QString)));
		connect(groupChat, SIGNAL(sigTransmit(QString)), this, SLOT(slotTransmitWidget(QString)));
		connect(groupChat, SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget*)), this, SLOT(slotOpenPic(QString, QList<QString>*, QWidget*)));//图片浏览
		connect(groupChat, SIGNAL(sigShareID(int, QString)), this, SLOT(slotShareWidget(int, QString)));
		connect(groupChat, SIGNAL(sigOpenChat(int, QVariant)), this, SLOT(recvChat(int, QVariant)));

		GroupInfo groupInfo = gDataBaseOpera->DBGetGroupFromID(groupID);
		groupChat->OnSetGroupName(groupInfo.groupName, groupID);
		groupChat->setNoSpeak(groupInfo.noSpeak);
		
		stackedWidget->addWidget(groupChat);
		//如果群组成员没有下载，就下载群组成员。
		if (gDataBaseOpera->DBJudgetGroupBuddyIsEmpty(groupID))
		{
			IMRequestBuddyInfo *request = new IMRequestBuddyInfo();
			UserInfo userInfo = gDataManager->getUserInfo();
			QString url = gDataManager->getAppConfigInfo().MessageServerAddress;
			connect(request, SIGNAL(sigParseGroupBuddyInfo(QString, QList<BuddyInfo>)), this, SLOT(slotParseGroupBuddyInfo(QString, QList<BuddyInfo>)));
			request->RequestGroupBuddyInfo(url, QString::number(userInfo.nUserID), userInfo.strUserPWD, groupID);
		}
		else
			groupChat->OnInsertGroupUserList(groupID);
	}

	return groupChat;
}

void ChatDataManager::slotClickedChat(QString userID)
{
#ifdef Q_OS_WIN
	for (int i = 0; i < stackedWidget->count(); i++)
	{
		QWidget *widget = stackedWidget->widget(i);
		if (widget)
		{
			QString widgetID = widget->objectName();
			if (widgetID == userID)
			{
				stackedWidget->setCurrentWidget(widget);

				if (gDataBaseOpera->DBJudgeGroupIsHaveByID(userID))
				{
					GroupWidget *groupWidget = (GroupWidget *)widget;
					gDataManager->setCurrentChatWidget(groupWidget->getChatWidgetID());
				}
				else
				{
					IMPerChat *perWidget = (IMPerChat *)widget;
					gDataManager->setCurrentChatWidget(perWidget->winId());
				}
				gDataBaseOpera->DBUpdateMessageListInfo(userID.toInt());
				return;
			}
		}
	}
#endif
	UserInfo userInfo = gDataManager->getUserInfo();

	//没有该聊天界面，创建。
	if (gDataBaseOpera->DBJudgeGroupIsHaveByID(userID))  //是群组
	{
		GroupWidget *groupWidget = getGroupChat(userID);
		if (groupWidget)
		{
#ifdef Q_OS_WIN
			gDataManager->setCurrentChatWidget(groupWidget->getChatWidgetID());
#endif
			stackedWidget->setCurrentWidget(groupWidget);
			QList<QByteArray> msgIDList = gSocketMessage->SetGroupMessageRead(userID.toInt());
			for (int i = 0; i < msgIDList.size(); i++)
			{
				gSocketMessage->SetMessageState(msgIDList[i], MESSAGE_STATE_READ);
				//gSocketMessage->SendMessageReadMessage(userInfo.nUserID, userID.toInt(), 1, QString(msgIDList[i]));
			}
		}
	}
	else
	{
		IMPerChat *perWidget = getPerChat(userID);
		if (perWidget)
		{
#ifdef Q_OS_WIN
			gDataManager->setCurrentChatWidget(perWidget->winId());
#endif
			stackedWidget->setCurrentWidget(perWidget);
		}
		QList<QByteArray> msgIDList = gSocketMessage->SetPerMessageRead(userID.toInt());

		for (int i = 0; i < msgIDList.size(); i++)
		{
            gSocketMessage->SetMessageState(msgIDList[i], MESSAGE_STATE_READ);
			gSocketMessage->SendMessageReadMessage(userInfo.nUserID, userID.toInt(), 0, QString(msgIDList[i]));
		}
	}

	/*更改数据库状态*/
	gDataBaseOpera->DBUpdateMessageListInfo(userID.toInt());
//#ifdef Q_OS_WIN
//    QTimer::singleShot(500, messageList, SLOT(setFocus()));
//#endif
}

void ChatDataManager::slotCloseChat(QString userID)
{
/*#ifdef Q_OS_WIN
	for (int i = 0; i < messageList->m_pMsglistModel->rowCount(); i++)
	{
		QString strUserId = messageList->m_pMsglistModel->data(messageList->m_pMsglistModel->index(i), MessageListModel::UserIdRole).toString();

		if (strUserId == userID)
		{
			messageList->onDeleteItem(i);
			for (int j = 0; j < stackedWidget->count(); j++)
			{
				QWidget *widget = stackedWidget->widget(j);
				if (widget)
				{
					if (widget->objectName() == userID)
					{
						//如果要关闭的是当前窗口，将当前聊天窗体指针设置为null。
#ifdef Q_OS_WIN
						if (widget == stackedWidget->currentWidget())
							gDataManager->setCurrentChatWidget(NULL);
#endif
						widget->deleteLater();
						stackedWidget->removeWidget(widget);
					}
				}
			}
			gDataBaseOpera->DBDeleteMessageByID(userID);
			break;
		}
	}
/*#else*/
    for (int i = 0; i < messageList->count(); i++)
        {
            QWidget *item = messageList->itemWidget(messageList->item(i));
            if (item)
            {
                CFrientItemMessage *message = (CFrientItemMessage *)item;
                if (message)
                {
                    if (message->mNickName->objectName() == userID)
                    {
                        messageList->onDeleteItem(i);
                        for (int j = 0; j < stackedWidget->count(); j++)
                        {
                            QWidget *widget = stackedWidget->widget(j);
                            if (widget)
                            {
                                if (widget->objectName() == userID)
                                {
#ifdef Q_OS_WIN
									if (widget == stackedWidget->currentWidget())
										gDataManager->setCurrentChatWidget(NULL);
#endif
                                    widget->deleteLater();
                                    stackedWidget->removeWidget(widget);
                                }
                            }
                        }
                        gDataBaseOpera->DBDeleteMessageByID(userID);
                        break;
                    }
                }
            }
    }
//#endif
}

void ChatDataManager::slotParseGroupBuddyInfo(QString groupID, QList<BuddyInfo> list)
{
	for (int i = 0; i < list.count(); i++)
		gDataBaseOpera->DBInsertGroupBuddyInfo(groupID, list.at(i));

	for (int j = 0; j < stackedWidget->count(); j++)
	{
		QWidget *widget = stackedWidget->widget(j);
		if (widget)
		{
			QString widgetID = widget->objectName();
			if (widgetID == groupID)
			{
				GroupWidget *groupWidget = (GroupWidget *)widget;
				groupWidget->OnInsertGroupUserList(groupID);
			}
		}
	}
}

void ChatDataManager::slotUpdateMessageState(MessageInfo msgInfo, int nState)
{
	switch (msgInfo.MsgDeliverType)
	{
	case 0:
	{
			  IMPerChat *perChat = NULL;
			  for (int i = 0; i < stackedWidget->count(); i++)
			  {
				  QWidget *widget = stackedWidget->widget(i);
				  if (widget)
				  {
					  QString widgetID = widget->objectName();
					  if (widgetID.toInt() == msgInfo.nToUserID)
					  {
						  perChat = (IMPerChat *)widget;
						  if (perChat)
						  {
							  perChat->UpdateMessageStateInfo(msgInfo.msgID, nState, msgInfo.integral);
						  }
						  break;
					  }
				  }
			  }
	}
		break;
	case 1:
	{
			  GroupWidget *groupChat = NULL;
			  for (int i = 0; i < stackedWidget->count(); i++)
			  {
				  QWidget *widget = stackedWidget->widget(i);
				  if (widget)
				  {
					  QString widgetID = widget->objectName();
					  if (widgetID == QString::number(msgInfo.nToUserID))
					  {
						  groupChat = (GroupWidget *)widget;
						  if (groupChat)
						  {
							  groupChat->UpdateMessageStateInfo(msgInfo.msgID, nState, msgInfo.integral);
						  }
						  break;
					  }
				  }
			  }
	}
		break;
	default:
		break;
	}
}

void ChatDataManager::readHistoryMessage()
{
	QList<MessageListInfo> listInfo = gDataBaseOpera->DBGetALLMessageListInfo();

	for (int i = 0; i < listInfo.size(); i++)
	{
		QString strEndMsg;

		QString strUnReadNum = "";
		if (listInfo[i].nUnReadNum == 0)
		{
			strUnReadNum = "";
		}
		else
		{
			strUnReadNum = QString("%1").arg(listInfo[i].nUnReadNum);
		}
		listInfo[i].strLastMessage.replace("\n", "");

		//当最后消息的时间使用毫秒保存的时候，需要通过删除后三位转化为为秒，避免溢出。
		if (QString::number(listInfo[i].nLastTime).length() == 13)
			listInfo[i].nLastTime /= 1000;

		messageList->OnInsertMessage(QString("%1").arg(listInfo[i].nBudyyID),
			listInfo[i].strBuddyHeaderImage,
			listInfo[i].strBuddyName,
			listInfo[i].strLastMessage, listInfo[i].nLastTime, QString("%1").arg(strUnReadNum), listInfo[i].isGroup);
	}

	QTimer::singleShot(500, messageList, SLOT(slotCountMessageNum()));
}

void ChatDataManager::slotShowPerLog(QString buddyID)
{
	if (messageLog == NULL)
	{
		messageLog = new MessageLog();
		m_strTmpLogId = buddyID;
		connect(messageLog, SIGNAL(sigInitFinished()), this, SLOT(slotLoadPerLogByID()));
		connect(messageLog, SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget *)), this, SLOT(slotOpenPic(QString, QList<QString>*, QWidget *)));
	}
	else
	{
		messageLog->loadPerLogByID(buddyID);
	}
}

void ChatDataManager::slotOpenGroupLog(QString groupID)
{
	if (messageLog == NULL)
	{
		//增加消息记录对话框。
		messageLog = new MessageLog();
		m_strTmpLogId = groupID;
		connect(messageLog, SIGNAL(sigInitFinished()), this, SLOT(slotLoadGroupLogByID()));
		connect(messageLog, SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget *)), this, SLOT(slotOpenPic(QString, QList<QString>*, QWidget *)));
	}
	else
	{
		messageLog->loadGroupLogByID(groupID);
	}
}

void ChatDataManager::slotLoadGroupLogByID()
{
	messageLog->loadGroupLogByID(m_strTmpLogId);
}

void ChatDataManager::slotLoadPerLogByID()
{
	messageLog->loadPerLogByID(m_strTmpLogId);
}

void ChatDataManager::slotRefreshDeviceInfo()
{
	UserInfo user = gDataManager->getUserInfo();
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotDeviceInfoResult(bool, QString)));
	QString strResult = gDataManager->getAppConfigInfo().MessageServerAddress +
		QString("/IMServer/user/getSessionContextList?userId=%1&passWord=%2").arg(user.nUserID).arg(user.strUserPWD);
	http->getHttpRequest(strResult);
}

void ChatDataManager::slotDeviceInfoResult(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		QVariantList list = map.value("sessionContextList").toList();
		int count = list.count() - 1; //显示的是其他设备的数量，此处是总数量，因此-1.
		chatWidget->setOtherDeviceCount(count);
	}
}

void ChatDataManager::slotRevOtherDeviceMsg(MessageInfo msgInfo)
{
	/*消息列表插入消息*/
	if (messageList)
	{
		UserInfo userInfo = gDataManager->getUserInfo();
		QWidget *widget = NULL;
		for (int i = 0; i < stackedWidget->count(); i++)
		{
			QWidget *page = stackedWidget->widget(i);
			if (page)
			{
				if (page->objectName().toInt() == msgInfo.nToUserID)
				{
					widget = page;
					break;
				}
			}
		}

		if (msgInfo.MsgDeliverType == 0)
		{
			BuddyInfo buddyInfo = gDataBaseOpera->DBGetBuddyInfoByID(QString::number(msgInfo.nToUserID));
			if (buddyInfo.nUserId == -1)
			{
				buddyInfo = gDataBaseOpera->DBGetGroupUserFromID(QString::number(msgInfo.nToUserID));

				if (buddyInfo.strNickName.isEmpty())
				{
					//buddyInfo.nUserId = msgInfo.nToUserID;
					//buddyInfo.strNickName = QStringLiteralBak("正在加载……");
					return;
				}
			}

			messageList->OnInsertPerToPerMessage(msgInfo, buddyInfo);
			if (widget)
			{
				IMPerChat *perChat = (IMPerChat*)widget;
				perChat->OnInertSendMessageTextInfo(msgInfo.strMsg, userInfo.strUserAvatarLocal, msgInfo);
			}
		}
		else if (msgInfo.MsgDeliverType == 1)
		{
			GroupInfo groupInfo = gDataBaseOpera->DBGetGroupFromID(QString::number(msgInfo.nToUserID));

			if (groupInfo.createUserId.isEmpty())
				return;

			messageList->OnInsertPerToGroupMessage(msgInfo, groupInfo);
			if (widget)
			{
				GroupWidget *groupWidget = (GroupWidget*)widget;
				groupWidget->OnInertSendMessageTextInfo(msgInfo.strMsg, userInfo.strUserAvatarLocal, msgInfo);
			}
		}
	}
}

/*插入截图*/
void ChatDataManager::InsertTextEditPic(QString strPath)
{
    /**/
    if(messageList)
    {
        MessageListInfo listInfo = messageList->getCurrentItemInfo();
        if(listInfo.isGroup == 0)  //个人
        {
            IMPerChat *perChat = NULL;
            for (int i = 0; i < stackedWidget->count(); i++)
            {
                QWidget *widget = stackedWidget->widget(i);
                if (widget)
                {
                    QString widgetID = widget->objectName();
                    if (widgetID == QString::number(listInfo.nBudyyID))
                        perChat = (IMPerChat *)widget;
                }
            }
            if(perChat != NULL)
                perChat->InsertTextEditPic(strPath);
        }
        else if(listInfo.isGroup == 1)
        {
            GroupWidget *groupChat = NULL;
            for (int i = 0; i < stackedWidget->count(); i++)
            {
                QWidget *widget = stackedWidget->widget(i);
                if (widget)
                {
                    QString widgetID = widget->objectName();
                    if (widgetID == QString::number(listInfo.nBudyyID))
                        groupChat = (GroupWidget *)widget;
                }
            }
            if(groupChat != NULL)
                groupChat->InsertTextEditPic(strPath);
        }
    }
}

void ChatDataManager::slotSearchText(QString text)
{
	if (!text.isEmpty())
	{
		//将输入小写化处理。
		text = text.toLower();

		QList<BuddyInfo> AllBuddyList = gDataBaseOpera->DBGetBuddyInfo();
		QList<GroupInfo> AllGroupList = gDataBaseOpera->DBGetAllGroupInfo();

		QList<BuddyInfo> buddyList;
		foreach(BuddyInfo buddy, AllBuddyList)
		{
			if (buddy.BuddyType != 1)
				continue;

			PinYin pinyin;
			if (!buddy.strNickName.isEmpty())
			{
				pinyin = getPinYin(buddy.strNickName);
				if (buddy.strNickName.toLower().contains(text) || pinyin.fullPinYin.contains(text) || pinyin.easyPinYin.contains(text))
				{
					buddyList.append(buddy);
					continue;
				}
			}

			if (!buddy.strNote.isEmpty())
			{
				pinyin = getPinYin(buddy.strNote);
				if (buddy.strNote.toLower().contains(text) || pinyin.fullPinYin.contains(text) || pinyin.easyPinYin.contains(text))
				{
					buddyList.append(buddy);
					continue;
				}	
			}
		}

		QList<GroupInfo> groupList;
		foreach(GroupInfo group, AllGroupList)
		{
			PinYin pinyin;
			if (!group.groupName.isEmpty())
			{
				PinYin pinyin = getPinYin(group.groupName);
				if (group.groupName.toLower().contains(text) || pinyin.fullPinYin.contains(text) || pinyin.easyPinYin.contains(text))
				{
					groupList.append(group);
					continue;
				}
			}
		}

		searchList->onInsertContacts(buddyList, groupList);
	}
}

void ChatDataManager::slotTransmitWidget(QString msgID)
{
	TransmitMessageWidget *transmitWidget = new TransmitMessageWidget;
	transmitWidget->setObjectName(msgID);
	transmitWidget->init();

	connect(transmitWidget, SIGNAL(sigTransmitContact(int, QString)), this, SLOT(slotTransmitMessage(int, QString)));
	QTimer::singleShot(100, transmitWidget, SLOT(show()));
}

void ChatDataManager::slotTransmitMessage(int type, QString contactID)
{
	TransmitMessageWidget *transmit = qobject_cast<TransmitMessageWidget *>(sender());
	QString msgID = transmit->objectName();

	MessageInfo message = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);
	if (message.msgID == msgID.toUtf8())  //查询消息有效
	{
		if (type == OpenPer)
		{
			this->OnDealOpenPerMessage(QVariant(contactID));
			IMPerChat *perChat = getPerChat(contactID);
			if (perChat)
				perChat->OnSendTransmitMessage(message);
		}
		if (type == OpenGroup)
		{
			this->OnDealOpenGroupMessage(QVariant(contactID));
			GroupWidget *groupChat = getGroupChat(contactID);
			if (groupChat)
				groupChat->OnSendTransmitMessage(message);
		}
	}
}

void ChatDataManager::slotShareWidget(int type, QString shareID)
{
	TransmitMessageWidget *transmitWidget = new TransmitMessageWidget;
	QVariantMap map;
	map.insert("type", type);
	map.insert("shareID", shareID);
	transmitWidget->setData(map);
	transmitWidget->init();

	connect(transmitWidget, SIGNAL(sigTransmitContact(int, QString)), this, SLOT(slotShareMessage(int, QString)));
	QTimer::singleShot(100, transmitWidget, SLOT(show()));
}

void ChatDataManager::slotShareMessage(int type, QString recvID)
{
	TransmitMessageWidget *transmit = qobject_cast<TransmitMessageWidget *>(sender());
	QVariantMap map = transmit->getData().toMap();
	int shareType = map.value("type").toInt();
	QString shareID = map.value("shareID").toString();

	if (type == OpenPer)
	{
		this->OnDealOpenPerMessage(QVariant(recvID));
		IMPerChat *perChat = getPerChat(recvID);
		if (perChat)
			perChat->OnShareID(shareType, shareID);
	}
	if (type == OpenGroup)
	{
		this->OnDealOpenGroupMessage(QVariant(recvID));
		GroupWidget *groupChat = getGroupChat(recvID);
		if (groupChat)
			groupChat->OnShareID(shareType, shareID);
	}
}

void ChatDataManager::slotTipMessage(int type, QString recvID, QString strMessage)
{
	if (gSocketMessage)
	{
		//向服务器发送消息部分。
		UserInfo userInfo = gDataManager->getUserInfo();
		int deliverType;
		if (type == PerMessage)
			deliverType = 0;
		if (type == GroupMessage)
			deliverType = 1;
		MessageInfo msgInfo = gSocketMessage->SendTextMessage(userInfo.nUserID, recvID.toInt(), deliverType, (short)MessageType::Message_NOTIFY, strMessage);

		//更新至聊天窗口部分。
		if (type == PerMessage)
		{
			for (int i = 0; i < stackedWidget->count(); i++)
			{
				QWidget *widget = stackedWidget->widget(i);
				if (widget)
				{
					if (widget->objectName() == recvID)
					{
						IMPerChat *perChat = (IMPerChat *)widget;
						perChat->OnRecvNotifyMessage(msgInfo);
						break;
					}
				}
			}
		}
		if (type == GroupMessage)
		{
			for (int i = 0; i < stackedWidget->count(); i++)
			{
				QWidget *widget = stackedWidget->widget(i);
				if (widget)
				{
					if (widget->objectName() == recvID)
					{
						GroupWidget *groupChat = (GroupWidget *)widget;
						groupChat->OnInsertRevMessage(msgInfo);
						break;
					}
				}
			}
		}

		//更新至左侧消息列表部分。
		QJsonDocument doc = QJsonDocument::fromJson(strMessage.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		if (map.value("type").toString() == "notification")
		{
			strMessage = map.value("content").toString();
		}
		if (map.value("type").toString() == "receivedFile")
		{
			strMessage = QStringLiteralBak("对方已经成功接收您的文件");
		}

		if (type == PerMessage)
		{
			BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(recvID);
			if (buddy.strNickName.isEmpty())
				buddy = gDataBaseOpera->DBGetGroupUserFromID(recvID);

			MessageListInfo messageListInfo;
			messageListInfo.nBudyyID = buddy.nUserId;
			messageListInfo.strLastMessage = strMessage;
			messageListInfo.nLastTime = msgInfo.ClientTime;
			messageListInfo.strBuddyName = buddy.strNickName;
			messageListInfo.strBuddyHeaderImage = buddy.strLocalAvatar;
			messageListInfo.messageType = msgInfo.MessageChildType;
			messageListInfo.nUnReadNum = 0;
			messageListInfo.isGroup = 0;
			gDataBaseOpera->DBInsertPerMessageListInfo(messageListInfo);
			//更新至消息列表，false代表个人消息
            emit sigUpdateSelfMessage(false, QVariant::fromValue(buddy), strMessage,true);
		}
		if (type == GroupMessage)
		{
			GroupInfo group = gDataBaseOpera->DBGetGroupFromID(recvID);
			MessageListInfo messageListInfo;
			messageListInfo.nBudyyID = group.groupId.toInt();
			messageListInfo.strLastMessage = strMessage;
			messageListInfo.nLastTime = msgInfo.ClientTime;
			messageListInfo.strBuddyName = group.groupName;
			messageListInfo.strBuddyHeaderImage = group.groupLoacalHeadImage;
			messageListInfo.messageType = msgInfo.MessageChildType;
			messageListInfo.nUnReadNum = 0;
			messageListInfo.isGroup = 1;
			gDataBaseOpera->DBInsertGroupMessageListInfo(messageListInfo);

			//更新至消息列表，true代表群组消息
            emit sigUpdateSelfMessage(true, QVariant::fromValue(group), strMessage,true);
		}
	}
}

void ChatDataManager::slotQuickReply(int type, QString contactID, QString strMsg)
{
	MessageInfo info;
	info.MessageChildType = MessageType::Message_TEXT;
	info.strMsg = strMsg;

	if (type == OpenPer)
	{
		this->OnDealOpenPerMessage(QVariant(contactID));
		IMPerChat *perChat = getPerChat(contactID);
		if (perChat)
			perChat->OnSendTransmitMessage(info);
			
	}
	if (type == OpenGroup)
	{
		this->OnDealOpenGroupMessage(QVariant(contactID));
		GroupWidget *groupChat = getGroupChat(contactID);
		if (groupChat)
			groupChat->OnSendTransmitMessage(info);
	}
}


void ChatDataManager::updateBuddyHeaderImgPath(int userId, QString path)
{
	if (messageList)
	{
		BuddyInfo stTmp = gDataBaseOpera->DBGetBuddyInfoByID(QString::number(userId));
		messageList->OnUpdateMessage(false, QVariant::fromValue(stTmp));
	}
}

void ChatDataManager::slotOpenPic(QString strfileName,QList<QString>* listPath,QWidget * pParent)
{
	QString fileName = "";
	if (strfileName.indexOf("file:///") > -1)
	{
#ifdef Q_OS_WIN
		fileName = strfileName.mid(strfileName.indexOf("file:///") + 8);
#else
		fileName = strfileName.mid(strfileName.indexOf("file:///") + 7);
#endif
	}
	else
	{
		fileName = strfileName;
	}


	if (m_pPicWidget != NULL)
	{
		//delete m_pPicWidget;
		m_pPicWidget->deleteLater();

		m_pPicWidget = NULL;
	}
	fileName = decodeURI(fileName);
	m_pPicWidget = new PicWidget();
	if (NULL != listPath)
	{
		connect(m_pPicWidget, SIGNAL(sigPicClose()), this, SLOT(slotClosePic()));
		connect(m_pPicWidget, SIGNAL(sigChangePic(int)), this, SLOT(slotChangePic(int)));
		m_pPicWidget->setPath(fileName, pParent);

		m_listPic = *listPath;
		for (m_PicIter = m_listPic.begin(); m_PicIter != m_listPic.end(); m_PicIter++)
		{
			//m_PicIter
			QString strTmp = *m_PicIter;
			if (strTmp == fileName)
			{
				break;
			}
		}
	}
	else
	{
		connect(m_pPicWidget, SIGNAL(sigPicClose()), this, SLOT(slotClosePic()));
		m_pPicWidget->setPath(fileName, pParent,1);//默认0 1为不显示上一张下一张按钮
	}

	m_pPicWidget->show();
}

void ChatDataManager::slotClosePic()
{
	m_pPicWidget = NULL;
}
void ChatDataManager::slotChangePic(int iType)
{
	QString strText = *m_PicIter;
	if (iType == 0)//上一个
	{
		if (m_PicIter != m_listPic.begin())
		{
			m_PicIter--;
			m_pPicWidget->resetPath(*m_PicIter);
			if (m_PicIter == m_listPic.begin())
			{
				m_pPicWidget->setNoteTip(1);
			}
			else
			{
				m_pPicWidget->setNoteTip(0);
			}
		}
		else
		{
			m_pPicWidget->setNoteTip(1);
		}
	}
	else if (iType == 1)
	{
		if (m_PicIter + 1 != m_listPic.end())
		{
			m_PicIter++;
			m_pPicWidget->resetPath(*m_PicIter);
			if (m_PicIter + 1 == m_listPic.end())
			{
				m_pPicWidget->setNoteTip(2);
			}
			else
			{
				m_pPicWidget->setNoteTip(0);
			}
		}
		else
		{
			m_pPicWidget->setNoteTip(2);
		}
	}
}


QString ChatDataManager::decodeURI(QString str)
{
	QByteArray array;
	for (int i = 0; i < str.length();) {
		if (0 == QString::compare(str.mid(i, 1), QString("%"))) {
			if ((i + 2) < str.length()) {
				array.append(str.mid(i + 1, 2).toShort(0, 16));
				i = i + 3;
			}
			else {
				array.append(str.mid(i, 1));
				i++;
			}
		}
		else {
			if (str.mid(i, 1) == "+")
				array.append(" ");
			else
				array.append(str.mid(i, 1));
			i++;
		}
	}
	QTextCodec *code = QTextCodec::codecForName("UTF-8");
	return code->toUnicode(array);
}

void ChatDataManager::slotTransferMsg(QString strUser, QString strToUser, QString strMsg)
{
    MessageInfo msgInfo = gSocketMessage->SendTextMessage(strUser.toInt(), strToUser.toInt(), 0, (short)8, strMsg);
    slotRevOtherDeviceMsg(msgInfo);
}
