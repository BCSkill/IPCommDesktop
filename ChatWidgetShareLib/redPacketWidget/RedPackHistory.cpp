﻿#include "RedPackHistory.h"
#include "ui_RedPackHistory.h"
#include "QStringLiteralBak.h"

#include <qbitmap.h>
#include <qmath.h>
#include <QFile>
#include <QMouseEvent>
#include <qscrollbar.h>
#include <QDesktopWidget>

RedPackHistory::RedPackHistory(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::RedPackHistory();
	ui->setupUi(this);

	this->setWindowFlags(Qt::Tool | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
	this->setAttribute(Qt::WA_DeleteOnClose);
	this->setAttribute(Qt::WA_TranslucentBackground, true);

	setStyle();

	QFile file(":/QSS/Resources/QSS/ChatWidgetShareLib/RedPackHistory.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->mPButtonReceive, SIGNAL(clicked()), this, SLOT(onReceiveClicked()));
	connect(ui->mPButtonSend, SIGNAL(clicked()), this, SLOT(onSendClicked()));
	connect(ui->mPButtonClose, SIGNAL(clicked()), this, SLOT(onCloseClicked()));

	UserInfo user = gDataManager->getUserInfo();
	this->setInfo(user.strUserAvatarLocal, user.strUserNickName);

	OPRequestShareLib *requestIn = new OPRequestShareLib;
	connect(requestIn, SIGNAL(sigRecords(QList<RecordInfo>)), this, SLOT(slotPacketRecordsIn(QList<RecordInfo>)));
	requestIn->getRedBagRecordsIn(user.strAccountName);

	OPRequestShareLib *requestOut = new OPRequestShareLib;
	connect(requestOut, SIGNAL(sigRecords(QList<RecordInfo>)), this, SLOT(slotPacketRecordsOut(QList<RecordInfo>)));
	requestOut->getRedBagRecordsOut(user.strAccountName);

#ifdef Q_OS_LINUX
setLinuxCenter();
#endif
}

RedPackHistory::~RedPackHistory()
{
	delete ui;
}
//头像路径，昵称，收到的红包总数，发出的红包总数
void RedPackHistory::setInfo(QString avatarPath, QString nickname) {

	loadImg(ui->mLabelAvatar_0, avatarPath);
	ui->mLabelNickname_0->setText(nickname);
	//ui->mLabelReceivePacks->setText(QString::number(receivePacks));
	ui->mLabelNumOfPacks->setText(QString::number(mReceivePacks));
}

//添加收到红包记录，参数分别为发红包者ID，红包金额，红包单位，日期，时间
void RedPackHistory::addReceiveInfo(QString senderID, QString amount, QString unit, QString date, QString time) {
	mReceivePacks++;
	amount.append(unit);
	QLabel *personSenderID = new QLabel(senderID);
	QLabel *personAmount = new QLabel(amount);
	QLabel *personDate = new QLabel(date);
	QLabel *personTime = new QLabel(time);

	personSenderID->setFixedHeight(15);
	personAmount->setFixedHeight(15);
	personDate->setFixedHeight(15);
	personDate->setFixedWidth(70);
	personTime->setFixedHeight(15);

	personSenderID->setStyleSheet("color:#000000;");
	personAmount->setStyleSheet("color:#000000;text-align:right;");
	personDate->setStyleSheet("color:#666666;");
	personTime->setStyleSheet("color:#666666;");

	QFont font;
	font.setFamily(tr("微软雅黑"));
	font.setPointSize(10);
	personSenderID->setFont(font);
	personAmount->setFont(font);

	font.setPointSize(9);
	personDate->setFont(font);
	personTime->setFont(font);

	QHBoxLayout *h1 = new QHBoxLayout();
	h1->addWidget(personSenderID);
	h1->addStretch(1);
	h1->addWidget(personAmount);
	h1->setContentsMargins(0, 0, 0, 0);

	QHBoxLayout *h2 = new QHBoxLayout();
	h2->addWidget(personDate);
	h2->addWidget(personTime);
	h2->setContentsMargins(0, 0, 0, 0);

	QVBoxLayout *v1 = new QVBoxLayout();
	v1->addLayout(h1);
	v1->addLayout(h2);
	v1->setContentsMargins(0, 0, 0, 0);

	QWidget *w = new QWidget();
	w->setObjectName("record");
	w->setLayout(v1);
	w->setStyleSheet("QWidget#record{ border-bottom:1px solid #eeeeee;}");
	w->setContentsMargins(0, 0, 0, 2);

	ui->verticalLayout_7->addWidget(w);
}

//添加发红包记录，参数分别为红包类型，红包金额，红包单位，日期，时间
void RedPackHistory::addSendInfo(QString type, QString amount, QString unit, QString date, QString time) {
	mSendPacks++;
	amount.append(unit);
	QLabel *personType = new QLabel(type);
	QLabel *personAmount = new QLabel(amount);
	QLabel *personDate = new QLabel(date);
	QLabel *personTime = new QLabel(time);

	personType->setFixedHeight(15);
	personAmount->setFixedHeight(15);
	personDate->setFixedHeight(15);
	personDate->setFixedWidth(70);
	personTime->setFixedHeight(15);

	QFont font;
	font.setFamily(tr("微软雅黑"));
	font.setPointSize(10);

	personType->setStyleSheet("color:#000000;");
	personAmount->setStyleSheet("color:#000000;text-align:right;");
	personDate->setStyleSheet("color:#666666;");
	personTime->setStyleSheet("color:#666666;");

	personType->setFont(font);
	personAmount->setFont(font);

	font.setPointSize(9);
	personDate->setFont(font);
	personTime->setFont(font);

	QHBoxLayout *h1 = new QHBoxLayout();
	h1->addWidget(personType);
	h1->addStretch(1);
	h1->addWidget(personAmount);
	h1->setContentsMargins(0, 0, 0, 0);

	QHBoxLayout *h2 = new QHBoxLayout();
	h2->addWidget(personDate);
	h2->addWidget(personTime);
	h2->setContentsMargins(0, 0, 0, 0);

	QVBoxLayout *v1 = new QVBoxLayout();
	v1->addLayout(h1);
	v1->addLayout(h2);
	v1->setContentsMargins(0, 0, 0, 0);
	
	QWidget *w = new QWidget();
	w->setObjectName("record");
	w->setLayout(v1);
	w->setStyleSheet("QWidget#record{ border-bottom:1px solid #eeeeee;}");
	w->setContentsMargins(0, 0, 0, 2);

	ui->verticalLayout_10->addWidget(w);
}
void RedPackHistory::onReceiveClicked() {
	QFont font;
	font.setFamily(tr("微软雅黑"));
	font.setPointSize(10);
	ui->stackedWidget->setCurrentIndex(0);
	ui->mPButtonReceive->setStyleSheet("border:none;color:#ffffff;text-align:bottom;margin-left:1px;");
	ui->mPButtonReceive->setFont(font);
	ui->mPButtonSend->setStyleSheet("border:none;color:#ff9999;text-align:bottom;margin-left:1px;");
	ui->mPButtonSend->setFont(font);
	ui->label_3->setStyleSheet("background-color:#cc3333");
	ui->label_5->setStyleSheet("background-color:#ffffff");
	ui->label_6->setText(tr("Received"));
	ui->label_4->setText(tr("Tokens been deposited in your accounts now, which can be transfered into local wallet"));
	ui->mLabelNumOfPacks->setText(QString::number(mReceivePacks));
}
void RedPackHistory::onSendClicked() {
	QFont font;
	font.setFamily(tr("微软雅黑"));
	font.setPointSize(10);
	ui->stackedWidget->setCurrentIndex(1);
	ui->mPButtonSend->setStyleSheet("border:none;color:#ffffff;text-align:bottom;margin-left:1px;");
	ui->mPButtonSend->setFont(font);
	ui->mPButtonReceive->setStyleSheet("border:none;color:#ff9999;text-align:bottom;margin-left:1px;");
	ui->mPButtonReceive->setFont(font);
	ui->label_5->setStyleSheet("background-color:#cc3333");
	ui->label_3->setStyleSheet("background-color:#ffffff");
	ui->label_6->setText(tr("Sent"));
	ui->label_4->setText(tr(""));
	ui->mLabelNumOfPacks->setText(QString::number(mSendPacks));
}
void RedPackHistory::setStyle() {
	/*QPalette palette(this->palette());
	palette.setColor(QPalette::Background, QColor(192,51,51));
	this->setPalette(palette);*/

	ui->scrollAreaWidgetContents->setStyleSheet("border:none;background-color:#FFFFFF");
	ui->scrollAreaWidgetContents_2->setStyleSheet("border:none;background-color:#FFFFFF");

	QFile scrollfile(":/QSS/Resources/QSS/greyScrollbar.qss");//滚动条样式
	scrollfile.open(QFile::ReadOnly);
	QString scroll;
	scroll = scrollfile.readAll();
	scrollfile.close();
	ui->scrollArea->verticalScrollBar()->setStyleSheet(scroll);
	ui->scrollArea_2->verticalScrollBar()->setStyleSheet(scroll);

}
void RedPackHistory::paintEvent(QPaintEvent *event) {
	QPainterPath path;
	path.setFillRule(Qt::WindingFill);
	path.addRect(6, 6, this->width() - 12, this->height() - 12);

	QPainter painter(this);
	painter.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);// 反锯齿;
	painter.fillPath(path, QBrush(Qt::white));

	QColor color(0, 0, 0, 50);//, 50);
	for (int i = 0; i < 6; i++)
	{
		QPainterPath path;
		path.setFillRule(Qt::WindingFill);
		path.addRect(6 - i, 6 - i, this->width() - (6 - i) * 2, this->height() - (6 - i) * 2);
		color.setAlpha(110 - qSqrt(i) * 40);
		painter.setPen(color);
		painter.drawPath(path);
	}

	painter.setBrush(QBrush(QColor(255, 255, 255)));
	painter.drawRect(6,96, 288,398);

	/*painter.setBrush(QBrush(QColor(16, 33, 63)));
	painter.drawRect(6, 55, 288, 120);*/

}
void RedPackHistory::onCloseClicked() {
	emit sigClose();
}
QPixmap RedPackHistory::PixmapToRound(const QPixmap & img, int radius) {//头像圆角化处理
	if (img.isNull())
	{
		return QPixmap();
	}
	QSize size(img.size());
	QBitmap mask(size);
	QPainter painter(&mask);
	painter.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
	painter.fillRect(mask.rect(), Qt::white);
	painter.setBrush(QColor(0, 0, 0));
	painter.drawRoundedRect(mask.rect(), radius, radius);

	QPixmap image = img;// .scaled(size);
	image.setMask(mask);
	return image;
}
void RedPackHistory::loadImg(QLabel* label, QString avatarPath) {
	QByteArray bytearray;
	QFile file(avatarPath);
	if (file.open(QIODevice::ReadOnly) && file.size() != 0)
	{
		bytearray = file.readAll();
	}
	file.close();
	QPixmap *pixmap = new QPixmap;
	if (!pixmap->loadFromData(bytearray) || bytearray == "")
	{
		pixmap->load(":/PerChat/Resources/person/temp.png");
	}
	pixmap->scaled(label->size(), Qt::KeepAspectRatio);
	label->setScaledContents(true);
	label->setPixmap(PixmapToRound(*pixmap, 20));
}
void RedPackHistory::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		mMoveing = true;
		mMovePosition = event->globalPos() - pos();
	}
	return QWidget::mousePressEvent(event);
}

void RedPackHistory::mouseMoveEvent(QMouseEvent *event)
{
	if (mMoveing && (event->buttons() && Qt::LeftButton)
		&& (event->globalPos() - mMovePosition).manhattanLength() > QApplication::startDragDistance())
	{
		move(event->globalPos() - mMovePosition);
		mMovePosition = event->globalPos() - pos();
	}
	return QWidget::mouseMoveEvent(event);
}

void RedPackHistory::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		mMoveing = false;
	}
}

void RedPackHistory::slotPacketRecordsIn(QList<RecordInfo> records)
{
	foreach(RecordInfo record, records)
	{
		QString date = record.age.split(" ").first();
		QString time = record.age.split(" ").last();
		this->addReceiveInfo(record.from, record.value, record.unit, date, time);
	}

	onReceiveClicked();
	this->show();
}

void RedPackHistory::slotPacketRecordsOut(QList<RecordInfo> records)
{
	foreach(RecordInfo record, records)
	{
		QString date = record.age.split(" ").first();
		QString time = record.age.split(" ").last();
		this->addSendInfo(record.to, record.value, record.unit, date, time);
	}
}

void RedPackHistory::setLinuxCenter()
{
    window()->setGeometry
    (
            QStyle::alignedRect
            (
                    Qt::LeftToRight,
                    Qt::AlignCenter,
                    window()->size(),
                    qApp->desktop()->availableGeometry()
            )
    );
}
