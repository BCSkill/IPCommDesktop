﻿#include "ChooseUnitWidget.h"
#include "ui_ChooseUnitWidget.h"
#include "PackUnitWidget.h"

#include <QListWidgetItem>

ChooseUnitWidget::ChooseUnitWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::ChooseUnitWidget();
	ui->setupUi(this);
	this->setWindowFlags(Qt::Tool | Qt::FramelessWindowHint);
	this->setAttribute(Qt::WA_TranslucentBackground);
	this->setAttribute(Qt::WA_DeleteOnClose);

	QFile file(":/QSS/Resources/QSS/ChatWidgetShareLib/ChooseUnitWidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->mPButtonConfirm, SIGNAL(clicked()), this, SLOT(onConfirmClicked()));
}

ChooseUnitWidget::~ChooseUnitWidget()
{
	delete ui;
}
void ChooseUnitWidget::addUnit(QString picPath, QString unit, QString useable, QString frozen) {//图片路径，单位，可用资产，冻结资产
	QListWidgetItem *item = new QListWidgetItem;
	item->setSizeHint(QSize(ui->listWidget->width(), 60));

	ui->listWidget->addItem(item);
	PackUnitWidget *packUnitWidget = new PackUnitWidget;

	packUnitWidget->setInfo(picPath, unit, useable, frozen);
	ui->listWidget->setItemWidget(item, packUnitWidget);

}
void ChooseUnitWidget::onConfirmClicked() {
	QListWidgetItem *wItem = ui->listWidget->currentItem();
	if(wItem){//如果有被选中的项目
		PackUnitWidget *w = (PackUnitWidget *)ui->listWidget->itemWidget(wItem);
		QString unit = w->m_unit;//unit即返回的单位;
	}
}
void ChooseUnitWidget::paintEvent(QPaintEvent * event) {
	QPainterPath path;
	path.setFillRule(Qt::WindingFill);
	path.addRect(6, 6, this->width() - 12, this->height() - 12);

	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.fillPath(path, QBrush(Qt::white));

	QColor color(0, 0, 0, 50);//, 50);
	for (int i = 0; i < 6; i++)
	{
		QPainterPath path;
		path.setFillRule(Qt::WindingFill);
		path.addRect(6 - i, 6 - i, this->width() - (6 - i) * 2, this->height() - (6 - i) * 2);
		color.setAlpha(110 - qSqrt(i) * 40);
		painter.setPen(color);
		painter.drawPath(path);
	}
}
void ChooseUnitWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		mMoveing = true;
		mMovePosition = event->globalPos() - pos();
	}
	return QWidget::mousePressEvent(event);
}

void ChooseUnitWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mMoveing && (event->buttons() && Qt::LeftButton)
		&& (event->globalPos() - mMovePosition).manhattanLength() > QApplication::startDragDistance())
	{
		move(event->globalPos() - mMovePosition);
		mMovePosition = event->globalPos() - pos();
	}
	return QWidget::mouseMoveEvent(event);
}

void ChooseUnitWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		mMoveing = false;
	}
}
