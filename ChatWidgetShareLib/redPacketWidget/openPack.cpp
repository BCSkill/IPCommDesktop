﻿#include <QMouseEvent>
#include <QFile>
#include <qbitmap.h>
#include <qdebug.h>
#include <qtimer.h>

#include "openPack.h"
#include "ui_openPack.h"
#include "QStringLiteralBak.h"

OpenPacketWidget::OpenPacketWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::OpenPacketWidget();
	ui->setupUi(this);

	m_angle = 0;
	m_change=true;
	this->setWindowModality(Qt::ApplicationModal);
	this->setAttribute(Qt::WA_DeleteOnClose);
	this->setWindowFlags(Qt::Tool | Qt::FramelessWindowHint);
	this->setAttribute(Qt::WA_TranslucentBackground);
	setWindowIcon(QIcon(":/Login/Resources/login/system.ico"));

	setStyle();//设置样式

	QFile file(":/QSS/Resources/QSS/ChatWidgetShareLib/openPack.ui.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->mPButtonClose, SIGNAL(clicked()), this, SLOT(onCloseClicked()));
	connect(ui->mPButtonOpen , SIGNAL(clicked()), this, SLOT(onOpenClicked()));
}

OpenPacketWidget::~OpenPacketWidget()
{
	delete ui;
}
QPixmap PixmapToRound(const QPixmap & img, int radius)//头像圆角化处理
{
	if (img.isNull())
	{
		return QPixmap();
	}
	QSize size(img.size());
	QBitmap mask(size);
	QPainter painter(&mask);
	painter.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
	painter.fillRect(mask.rect(), Qt::white);
	painter.setBrush(QColor(0, 0, 0));
	painter.drawRoundedRect(mask.rect(), radius, radius);

	QPixmap image = img;// .scaled(size);
	image.setMask(mask);
	return image;
}
void OpenPacketWidget::setInfo(QString avatarPath, QString nickname, QString msgID, QString notice, packType type) {//参数分别为头像路径，昵称，红包备注，红包类型
	QByteArray bytearray;
	QFile file(avatarPath);
	qDebug() << file.size();
	if (file.open(QIODevice::ReadOnly) && file.size() != 0)
	{
		bytearray = file.readAll();
	}
	file.close();
	QPixmap *pixmap = new QPixmap;
	if (!pixmap->loadFromData(bytearray) || bytearray == "")
	{
		pixmap->load(":/PerChat/Resources/person/temp.png");
		
	}
	pixmap->scaled(ui->mLabelAvatar->size(), Qt::KeepAspectRatio);
	ui->mLabelAvatar->setScaledContents(true);
	ui->mLabelAvatar->setStyleSheet("border:1px;border-radius:5px;");
	ui->mLabelAvatar->setPixmap(PixmapToRound(*pixmap,20));

	ui->mLabelNickname->setText(nickname);
	QFontMetrics fontWidth(ui->label_5->font());//得到每个字符的宽度
	QString elideNote = fontWidth.elidedText(notice, Qt::ElideRight, 220);//最大宽度150像素

	ui->label_5->setText(elideNote);

	switch (type)
	{
	case commonPack: {
		ui->mLabelPack->setText(tr("A red packet was sent to you"));
		break;
	}
	case groupCommonPack: {
		ui->mLabelPack->setText(tr("Red racket with a identical amount"));
		break;
	}
	case groupRandomPack: {
		ui->mLabelPack->setText(tr("Red packet with a random amount"));
		break;
	}
	default:
		break;
	}

	this->msgID = msgID;
}
void OpenPacketWidget::setStyle() {
	QPalette palette(this->palette());
	palette.setColor(QPalette::Background, QColor(255, 0, 0));
	this->setPalette(palette);
}
void OpenPacketWidget::SLO_updatePaint() {
	m_angle+=1;
	if (m_angle == 90){
		m_change = false;
	}
	if (m_angle == 180) {
		m_pTimer->stop();
		emit sigOpenPacket(msgID);
	}
	update();
}
void OpenPacketWidget::paintEvent(QPaintEvent *event) {
	QPainter painter(this);
	painter.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);// 反锯齿;
	painter.setBrush(QBrush(Qt::red));
	painter.setPen(Qt::transparent);
	QRect rect = this->rect();
	rect.setWidth(rect.width() - 1);
	rect.setHeight(rect.height() - 1);
	painter.drawRoundedRect(rect, 5, 5);
	
	QPixmap pixmap = QPixmap(":/RedPacket/Resources/redPacket/wechatview_red_bg1.png");//背景图片
	painter.drawPixmap(0, 5, 249, 250, pixmap);

	//旋转特效
	QTransform transform;
	transform.translate(125, 0);
	transform.rotate(m_angle, Qt::YAxis);
	painter.setTransform(transform);
	if (m_change) {
		painter.drawPixmap(-40, 211, 80, 80, QPixmap(":/RedPacket/Resources/redPacket/wecahtview_red_package_open.png"));
	}
	else
	{
		painter.drawPixmap(-40, 211, 80, 80, QPixmap(":/RedPacket/Resources/redPacket/wecahtview_red_package_back.png"));
	}
	QWidget::paintEvent(event);
}
void OpenPacketWidget::onCloseClicked() {
	emit sigClose();
}
void OpenPacketWidget::onOpenClicked() {
	m_pTimer = new QTimer(this);
    m_pTimer->setInterval(2);
	connect(m_pTimer, SIGNAL(timeout()), this, SLOT(SLO_updatePaint()));
	ui->mPButtonOpen->hide();
	m_pTimer->start();
}
void OpenPacketWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		mMoveing = true;
		mMovePosition = event->globalPos() - pos();
	}
	return QWidget::mousePressEvent(event);
}

void OpenPacketWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mMoveing && (event->buttons() && Qt::LeftButton)
		&& (event->globalPos() - mMovePosition).manhattanLength() > QApplication::startDragDistance())
	{
		move(event->globalPos() - mMovePosition);
		mMovePosition = event->globalPos() - pos();
	}
	return QWidget::mouseMoveEvent(event);
}

void OpenPacketWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		mMoveing = false;
	}
}
