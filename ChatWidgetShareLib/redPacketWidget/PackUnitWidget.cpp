﻿#include "PackUnitWidget.h"
#include "ui_PackUnitWidget.h"
#include <qfile.h>

PackUnitWidget::PackUnitWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::PackUnitWidget();
	ui->setupUi(this);

	QFile file(":/QSS/Resources/QSS/ChatWidgetShareLib/PackUnitWidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();
}

PackUnitWidget::~PackUnitWidget()
{
	delete ui;
}
void PackUnitWidget::setInfo(QString picPath, QString unit, QString useable, QString frozen) {
	QString path = "border-image:url(";
	path.append(picPath);
	path.append(");");
	
	ui->label->setStyleSheet(path);
	ui->label_2->setText(unit);
	ui->label_4->setText(useable);
	ui->label_3->setText(frozen);

	m_unit = unit;
}
