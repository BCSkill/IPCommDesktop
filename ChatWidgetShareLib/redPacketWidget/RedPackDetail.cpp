﻿#include <qbitmap.h>
#include <QFile>
#include <QMouseEvent>
#include <qscrollbar.h>
#include <QDesktopWidget>
#include <qmath.h>
#include <qdebug.h>

#include "RedPackDetail.h"
#include "ui_RedPackDetail.h"
#include "QStringLiteralBak.h"


RedPackDetail::RedPackDetail(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::RedPackDetail();
	ui->setupUi(this);

	historyWidget = NULL;

	this->setWindowFlags(Qt::Tool | Qt::FramelessWindowHint);
	this->setAttribute(Qt::WA_TranslucentBackground, true);
	this->setAttribute(Qt::WA_DeleteOnClose);
	setWindowIcon(QIcon(":/Login/Resources/login/system.ico"));
	setStyle();

	QFile file(":/QSS/Resources/QSS/ChatWidgetShareLib/RedPackDetail.ui.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->mPButtonClose, SIGNAL(clicked()), this, SLOT(onCloseClicked()));
	connect(ui->mPButtonCheck, SIGNAL(clicked()), this, SLOT(onCheckClicked()));
}

RedPackDetail::~RedPackDetail()
{
	delete ui;

	if (historyWidget)
		delete historyWidget;
}
void RedPackDetail::onCheckClicked() {//点击查看红包记录按钮
	if (historyWidget)
		historyWidget->close();

	historyWidget = new RedPackHistory;
	connect(historyWidget, SIGNAL(sigClose()), this, SLOT(slotCloseHistoryWidget()));
}
void RedPackDetail::slotCloseHistoryWidget()
{
	if (historyWidget)
		historyWidget->close();

	historyWidget = NULL;
}

void RedPackDetail::onCloseClicked() {
	//this->close();
	emit sigClose();
}
void RedPackDetail::setMyInfo(QString avatarPath, QString nickname, QString myAmount) {//设置头像，昵称,我获得的金额
	loadImg(ui->mLabelAvatar, avatarPath);
	ui->mLabelNickname->setText(nickname);
	ui->mLabelMyAmount->setText(myAmount);
}
void RedPackDetail::setPackInfo(QString notice, QString totalAmount, QString unit, int numOfPacks, int committedPacks) {//设置红包备注，红包总金额，红包单位，红包总数，已领取红包数量
	ui->mLabelNotice->setText(notice);
	ui->mLabelUnit->setText(unit);

	QString left = QString::number(numOfPacks);
	left.append(tr(" red packets "));
	left.append(totalAmount);
	left.append(unit);
	ui->mLabelNumOfPacks->setText(left);

	QString right = tr("taken ");
	right.append(QString::number(committedPacks));
	right.append("/");
	right.append(QString::number(numOfPacks));
	ui->mLabelCommittedPacks->setText(right);
}
void RedPackDetail::setStyle() {

	this->move((QApplication::desktop()->width() - this->width()) / 2, (QApplication::desktop()->height() - this->height()) / 2);
	ui->scrollAreaWidgetContents->setStyleSheet("border:none;background-color:#ffffff");

	QFile scrollfile(":/QSS/Resources/QSS/greyScrollbar.qss");//滚动条样式
	scrollfile.open(QFile::ReadOnly);
	QString scroll;
	scroll = scrollfile.readAll();
	scrollfile.close();
	ui->scrollArea->verticalScrollBar()->setStyleSheet(scroll);
}
void RedPackDetail::addPerson(QString avatarPath, QString nickname, QString date, QString time, QString amount, QString unit, bool isMostLucky) {
	amount.append(unit);
	QLabel *personNickname = new QLabel(nickname);
	QLabel *personAmount = new QLabel(amount);
	QLabel *personDate = new QLabel(date);
	QLabel *personTime = new QLabel(time);
	QLabel *personBest = new QLabel(tr("most lucky"));

	personNickname->setFixedHeight(20);
	personAmount->setFixedHeight(20);
	personDate->setFixedHeight(20);
	personDate->setFixedWidth(70);
	personTime->setFixedHeight(20);
	personBest->setFixedHeight(20);

	QFont font;
	font.setFamily(tr("微软雅黑"));
	font.setPointSize(12);

	personNickname->setStyleSheet(" color:#000000;");
	personAmount->setStyleSheet("color:#04122f;text-align:right;");
	personDate->setStyleSheet("color:#333333;");
	personTime->setStyleSheet("color:#333333;");
	personBest->setStyleSheet("color:#ff9933;");

	personNickname->setFont(font);
	personAmount->setFont(font);
	font.setPointSize(9);
	personDate->setFont(font);
	personTime->setFont(font);
	personBest->setFont(font);

	QLabel *personAvatar = new QLabel();
	personAvatar->setFixedHeight(40);
	personAvatar->setFixedWidth(40);
	loadImg(personAvatar, avatarPath);

	QHBoxLayout *h1 = new QHBoxLayout();
	h1->addWidget(personNickname);
	h1->addStretch(1);
	h1->addWidget(personAmount);
	h1->setContentsMargins(0, 0, 0, 0);

	QHBoxLayout *h2 = new QHBoxLayout();
	h2->addWidget(personDate);
	h2->addWidget(personTime);
	if (isMostLucky) {
		h2->addStretch(1);
		h2->addWidget(personBest);
	}
	h2->setContentsMargins(0, 0, 0, 0);

	QVBoxLayout *v1 = new QVBoxLayout();
	v1->addLayout(h1);
	v1->addLayout(h2);
	v1->setContentsMargins(0, 0, 0, 0);

	QHBoxLayout *h3 = new QHBoxLayout();
	h3->addWidget(personAvatar);
	h3->addLayout(v1);
	h3->setContentsMargins(0, 0, 0, 2);
	
	QWidget *w = new QWidget();
	w->setObjectName("record");
	w->setLayout(h3);
	w->setStyleSheet("QWidget#record{ border-bottom:1px solid #eeeeee;}");
	w->setContentsMargins(0, 0, 0, 0);
	ui->verticalLayout_3->addWidget(w);
}
void RedPackDetail::paintEvent(QPaintEvent *event) {
	QPainterPath path;
	/*path.setFillRule(Qt::WindingFill);
	path.addRect(6, 6, this->width() - 12, this->height() - 12);*/

	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing, true);
	//painter.fillPath(path, QBrush(Qt::white));

	QColor color(0, 0, 0, 50);//, 50);
	for (int i = 0; i < 6; i++)
	{
		QPainterPath path;
		path.setFillRule(Qt::WindingFill);
		path.addRect(6 - i, 6 - i, this->width() - (6 - i) * 2, this->height() - (6 - i) * 2);
		color.setAlpha(110 - qSqrt(i) * 40);
		painter.setPen(color);
		painter.drawPath(path);
	}
	//ui->widget_2->setStyleSheet("background-color:#cc3333");
	painter.fillRect(6, 6, width() - 12, 90, QColor(204, 51, 51));

	path.arcTo(6, 6, this->width() - 12, 70, 0, 180 * 16);
	painter.fillPath(path, QColor(239, 67, 67));

	painter.fillRect(6, 6, this->width() - 12, 40, QColor(239, 67, 67));
	painter.fillRect(6, 166, this->width() - 12, 328 , QColor(255, 255, 255));
}
QPixmap RedPackDetail::PixmapToRound(const QPixmap & img, int radius) {//头像圆角化处理
	if (img.isNull())
	{
		return QPixmap();
	}
	QSize size(img.size());
	QBitmap mask(size);
	QPainter painter(&mask);
	painter.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
	painter.fillRect(mask.rect(), Qt::white);
	painter.setBrush(QColor(0, 0, 0));
	painter.drawRoundedRect(mask.rect(), radius, radius);

	QPixmap image = img;// .scaled(size);
	image.setMask(mask);
	return image;
}
void RedPackDetail::loadImg(QLabel* label, QString avatarPath) {
	QByteArray bytearray;
	QFile file(avatarPath);
	if (file.open(QIODevice::ReadOnly) && file.size() != 0)
	{
		bytearray = file.readAll();
	}
	file.close();
	QPixmap *pixmap = new QPixmap;
	if (!pixmap->loadFromData(bytearray) || bytearray == "")
	{
		pixmap->load(":/PerChat/Resources/person/temp.png");
	}
	pixmap->scaled(label->size(), Qt::KeepAspectRatio);
	label->setScaledContents(true);
	label->setPixmap(PixmapToRound(*pixmap, 20));
}
void RedPackDetail::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		mMoveing = true;
		mMovePosition = event->globalPos() - pos();
	}
	return QWidget::mousePressEvent(event);
}

void RedPackDetail::mouseMoveEvent(QMouseEvent *event)
{
	if (mMoveing && (event->buttons() && Qt::LeftButton)
		&& (event->globalPos() - mMovePosition).manhattanLength() > QApplication::startDragDistance())
	{
		move(event->globalPos() - mMovePosition);
		mMovePosition = event->globalPos() - pos();
	}
	return QWidget::mouseMoveEvent(event);
}

void RedPackDetail::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		mMoveing = false;
	}
}