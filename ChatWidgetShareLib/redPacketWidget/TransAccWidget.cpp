﻿#include "TransAccWidget.h"
#include "ui_TransAccWidget.h"
#include "QStringLiteralBak.h"

extern OPDataManager  *gOPDataManager;

TransAccWidget::TransAccWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::TransAccWidget();
	ui->setupUi(this);

	this->setWindowFlags(Qt::Tool | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
	this->setAttribute(Qt::WA_TranslucentBackground);
	this->setAttribute(Qt::WA_DeleteOnClose);
	ui->horizontalSlider->setMinimum(4);
	ui->horizontalSlider->setMaximum(60);

	QFile file(":/QSS/Resources/QSS/ChatWidgetShareLib/TransAccWidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT(slotSetValue(int)));
	connect(ui->mPButtonClose, SIGNAL(clicked()), this, SLOT(slotClose()));
	connect(ui->mPButtonConfirm, SIGNAL(clicked()), this, SLOT(slotConfirm()));
	
	ui->lineEdit->setValidator(new QDoubleValidator(0, 10000, 8, this));

	QString mainAddress = gOPDataManager->getWalletInfo().address;
	QString name = tr("Wallet") + mainAddress.left(6) + "****" + mainAddress.right(4);
	ui->addressBox->addItem(name, mainAddress);
	ui->mLabelAddress->setText(mainAddress);

	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigBalance(QString)), ui->balanceLabel, SLOT(setText(QString)));
	connect(request, SIGNAL(sigBalance(QString)), request, SLOT(deleteLater()));
	request->getBalance(mainAddress);
}
TransAccWidget::~TransAccWidget()
{
	delete ui;
}
void TransAccWidget::slotClose() {
	emit sigClose();
}
void TransAccWidget::slotConfirm() {
	QString address = ui->mLabelAddress->text();
	if (address.isEmpty())
	{
		IMessageBox::tip(this, tr("Notice"), tr("Payment Address Cannot be Empty!"));
		return;
	}
	QString value = ui->lineEdit->text();
	if (value.isEmpty())
	{
		IMessageBox::tip(this, tr("Notice"), tr("Amount of Transfer Cannot be Empty!"));
		return;
	}
	if (value.toDouble() > ui->balanceLabel->text().toDouble())
	{
		IMessageBox::tip(this, tr("Notice"), tr("The Amount of Tranfer is Greater Than") + ui->label->text() + tr("Balance"));
		return;
	}

	InputBox *box = new InputBox(this);
	connect(box, SIGNAL(sigEnter(QString)), this, SLOT(slotConfirmPassword(QString)));
	box->init(tr("Please Enter Your Login Password:"), false, QLineEdit::Password);
}
void TransAccWidget::slotConfirmPassword(QString password)
{
	QString userPWD = gDataManager->getUserInfo().strLoginPWD;

	if (userPWD.isEmpty())
	{
		IMessageBox::tip(this, tr("Notice"), tr("Failed to Verify the Login Password!"));
		return;
	}
	if (password.isEmpty())
	{
		IMessageBox::tip(this, tr("Notice"), tr("Login Password Cannot be Empty!"));
		return;
	}

	//进行计算。
	QByteArray array = QCryptographicHash::hash(password.toUtf8(), QCryptographicHash::Sha1);
	QString string = array.toHex();

	if (string.toLower() != userPWD.toLower())
	{
		IMessageBox::tip(this, tr("Notice"), tr("Incorrect Password!"));
		return;
	}
	else
	{
		QString value = ui->lineEdit->text();
		emit sigHostingCharge(1, ui->mLabelAddress->text(), this->objectName(), value);
	}
}
void TransAccWidget::setInfo(QString unit, double balance, QString wallet, QString address) {
	ui->label->setText(unit);
	ui->label_5->setText(unit);
	//ui->balanceLabel->setText(QString::number(balance));
	//ui->mLabelWallet->setText(wallet);
	//ui->mLabelAddress->setText(address);
	this->setObjectName(address);
}
void TransAccWidget::slotSetValue(int value) {
	double val = (double)value / 10000;
	ui->label_15->setText(QString::number(val));
}
void TransAccWidget::paintEvent(QPaintEvent * event) {
	QPainterPath path;
	path.setFillRule(Qt::WindingFill);
	path.addRect(6, 6, this->width() - 12, this->height() - 12);

	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.fillPath(path, QBrush(Qt::white));

	QColor color(0, 0, 0, 50);//, 50);
	for (int i = 0; i < 6; i++)
	{
		QPainterPath path;
		path.setFillRule(Qt::WindingFill);
		path.addRect(6 - i, 6 - i, this->width() - (6 - i) * 2, this->height() - (6 - i) * 2);
		color.setAlpha(110 - qSqrt(i) * 40);
		painter.setPen(color);
		painter.drawPath(path);
	}
}
void TransAccWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		mMoveing = true;
		mMovePosition = event->globalPos() - pos();
	}
	return QWidget::mousePressEvent(event);
}

void TransAccWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mMoveing && (event->buttons() && Qt::LeftButton)
		&& (event->globalPos() - mMovePosition).manhattanLength() > QApplication::startDragDistance())
	{
		move(event->globalPos() - mMovePosition);
		mMovePosition = event->globalPos() - pos();
	}
	return QWidget::mouseMoveEvent(event);
}

void TransAccWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		mMoveing = false;
	}
}