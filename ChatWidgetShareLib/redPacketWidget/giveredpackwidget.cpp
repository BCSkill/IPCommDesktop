﻿#include "giveredpackwidget.h"
#include "ui_giveredpackwidget.h"
#include "QStringLiteralBak.h"
#include <QDesktopWidget>

GiveRedPackWidget::GiveRedPackWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::GiveRedPackWidget();
	ui->setupUi(this);

	historyWidget = NULL;
	transWidget = NULL;

	this->setWindowFlags(Qt::Tool | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
	this->setAttribute(Qt::WA_TranslucentBackground);
	this->setAttribute(Qt::WA_DeleteOnClose);
	setWindowIcon(QIcon(":/Login/Resources/login/system.ico"));

	ui->numberEdit->setValidator(new QDoubleValidator(0, 10000, 5, this));

	QFile file(":/QSS/Resources/QSS/ChatWidgetShareLib/giveredpackwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->closeBtn, SIGNAL(clicked()), this, SIGNAL(sigClose()));
	connect(ui->recordBtn, SIGNAL(clicked()), this, SLOT(slotClickRecordBtn()));
	connect(ui->numberEdit, SIGNAL(textChanged(QString)), this, SLOT(slotChangeGiveNumber(QString)));
	connect(ui->giveBtn, SIGNAL(clicked()), this, SLOT(slotGiveRedPacket()));
	connect(ui->turnInBtn, SIGNAL(clicked()), this, SLOT(slotClickTurnInBtn()));

	this->getHostingAccount();

#ifdef Q_OS_LINUX
setLinuxCenter();
#endif
}

GiveRedPackWidget::~GiveRedPackWidget()
{
	delete ui;

	if (historyWidget)
		delete historyWidget;
	if (transWidget)
		delete transWidget;
}

void GiveRedPackWidget::show()
{
	QWidget::show();
	this->raise();
	this->activateWindow();
	ui->numberEdit->setFocus();
}

void GiveRedPackWidget::paintEvent(QPaintEvent * event)
{
	QPainterPath path;
	path.setFillRule(Qt::WindingFill);
	path.addRect(6, 6, this->width() - 12, this->height() - 12);

	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.fillPath(path, QBrush(Qt::white));

	QColor color(0, 0, 0, 50);//, 50);
	for (int i = 0; i < 6; i++)
	{
		QPainterPath path;
		path.setFillRule(Qt::WindingFill);
		path.addRect(6 - i, 6 - i, this->width() - (6 - i) * 2, this->height() - (6 - i) * 2);
		color.setAlpha(110 - qSqrt(i) * 40);
		painter.setPen(color);
		painter.drawPath(path);
	}
}

void GiveRedPackWidget::getHostingAccount()
{
	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigAssetsInfo(QList<AssetInfo>)), this, SLOT(slotHostingAccount(QList<AssetInfo>)));
	connect(request, SIGNAL(sigAssetsInfo(QList<AssetInfo>)), request, SLOT(deleteLater()));
	QString userID = gDataManager->getUserInfo().strAccountName;
	request->getHostingAccount(userID);
}

void GiveRedPackWidget::slotHostingAccount(QList<AssetInfo> assetList)
{
	this->assetsList = assetList;

	foreach(AssetInfo asset, assetList)
	{
		if (asset.coinName == "PWR")
		{
			ui->balanceLabel->setText(QString::number(asset.availableCoin,'f',5) + "PWR");
			break;
		}
	}
}

void GiveRedPackWidget::slotClickTurnInBtn()
{
	if (transWidget)
		transWidget->close();

	transWidget = new TransAccWidget;
	connect(transWidget, SIGNAL(sigClose()), this, SLOT(slotCloseTransWidget()));
	connect(transWidget, SIGNAL(sigHostingCharge(int, QString, QString, QString)), this, SIGNAL(sigHostingCharge(int, QString, QString, QString)));

	foreach(AssetInfo asset, assetsList)
	{
		if (asset.coinName == "PWR")
		{
			transWidget->setInfo(asset.coinName, asset.availableCoin, asset.coinFullName, asset.sysAddress);
			break;
		}
	}

	transWidget->show();
}

void GiveRedPackWidget::slotClickRecordBtn()
{
	if (historyWidget)
		historyWidget->close();

	historyWidget = new RedPackHistory;
	connect(historyWidget, SIGNAL(sigClose()), this, SLOT(slotCloseHistoryWidget()));
}

void GiveRedPackWidget::slotChangeGiveNumber(QString strNubmer)
{
	if (strNubmer.isEmpty())
		strNubmer = "0";
	QString tip = tr("Amount: ") + strNubmer + ui->coinLabel->text();
	ui->giveLabel->setText(tip);
}

void GiveRedPackWidget::slotCreatePacket(QString packetID)
{
	if (packetID.isEmpty())
	{
		IMessageBox::tip(this, tr("Notice"), tr("Failed to prepare the red packet!"));
	}
	else
	{
		QString strNumber = ui->numberEdit->text();
		QString remark = ui->remarkEdit->text();
		if (remark.isEmpty())
			remark = tr("Best wishes!");

		QString phoneID = gDataManager->getUserInfo().strAccountName;

		QVariantMap map;
		map.insert("packageState", 0);
		map.insert("count", 1);
		map.insert("totalMoney", strNumber);
		map.insert("leaveMessage", remark);
		map.insert("luckMode", 1);
		map.insert("assetUnit", ui->coinLabel->text());
		map.insert("packetId", packetID);
		map.insert("trusteeshipCoinId", 1);
		map.insert("phoneId", phoneID);
		QJsonDocument doc = QJsonDocument::fromVariant(map);
		QString data = doc.toJson();

		emit sigGivePacketData(data);
		emit sigClose();
	}
}

void GiveRedPackWidget::slotGiveRedPacket()
{
	QString strNumber = ui->numberEdit->text();

	if (strNumber.isEmpty())
		IMessageBox::tip(this, tr("Notice"), tr("Amount cannot be empty!"));
	else
	{
		QString strBalance = ui->balanceLabel->text();
		strBalance = strBalance.remove(strBalance.right(3));

		double number = strNumber.toDouble();
		if (number == 0)
		{
			IMessageBox::tip(this, tr("Notice"), tr("Amount cannot be empty!"));
		}
		else
		{
			if (number > strBalance.toDouble())
			{
				IMessageBox::tip(this, tr("Notice"), tr("The amount of the red packet cannot be greater than your balance!"));
			}
			else
			{
				InputBox *box = new InputBox(this);
				connect(box, SIGNAL(sigEnter(QString)), this, SLOT(slotConfirmPassword(QString)));
				box->init(tr("Please enter your login password"), false, QLineEdit::Password);
			}
		}
	}
}

void GiveRedPackWidget::slotConfirmPassword(QString password)
{
	QString userPWD = gDataManager->getUserInfo().strLoginPWD;

	if (userPWD.isEmpty())
	{
		IMessageBox::tip(this, tr("Notice"), tr("Password Validation Failed!"));
		return;
	}
	if (password.isEmpty())
	{
		IMessageBox::tip(this, tr("Notice"), tr("Password cannot be empty!"));
		return;
	}

	//进行计算。
	QByteArray array = QCryptographicHash::hash(password.toUtf8(), QCryptographicHash::Sha1);
	QString string = array.toHex();

	if (string.toLower() != userPWD.toLower())
	{
		IMessageBox::tip(this, tr("Notice"), tr("Incorrect Password!"));
		return;
	}
	else
	{
		QString remark = ui->remarkEdit->text();
		if (remark.isEmpty())
			remark = tr("Best Wishes!");

		AssetInfo currentAsset;
		foreach(AssetInfo asset, this->assetsList)
		{
			if (asset.coinName == "PWR")
			{
				currentAsset = asset;
				break;
			}
		}

		QString strNumber = ui->numberEdit->text();
		double number = strNumber.toDouble();

		UserInfo user = gDataManager->getUserInfo();
		OPRequestShareLib *request = new OPRequestShareLib;
		connect(request, SIGNAL(sigCreatePacketID(QString)), this, SLOT(slotCreatePacket(QString)));
		request->createRedPacket(user.strAccountName, currentAsset.trusteeshipId, currentAsset.coinName, 1, number, 1, remark, this->objectName(), number);
	}
}

void GiveRedPackWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		mMoveing = true;
		mMovePosition = event->globalPos() - pos();
	}
	return QWidget::mousePressEvent(event);
}

void GiveRedPackWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mMoveing && (event->buttons() && Qt::LeftButton)
		&& (event->globalPos() - mMovePosition).manhattanLength() > QApplication::startDragDistance())
	{
		move(event->globalPos() - mMovePosition);
		mMovePosition = event->globalPos() - pos();
	}
	return QWidget::mouseMoveEvent(event);
}

void GiveRedPackWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		mMoveing = false;
	}
}

void GiveRedPackWidget::slotCloseHistoryWidget()
{
	if (historyWidget)
		historyWidget->close();

	historyWidget = NULL;
}

void GiveRedPackWidget::slotCloseTransWidget()
{
	if (transWidget)
		transWidget->close();

	transWidget = NULL;
}

void GiveRedPackWidget::setLinuxCenter()
{
    window()->setGeometry
    (
            QStyle::alignedRect
            (
                    Qt::LeftToRight,
                    Qt::AlignCenter,
                    window()->size(),
                    qApp->desktop()->availableGeometry()
            )
    );
}
