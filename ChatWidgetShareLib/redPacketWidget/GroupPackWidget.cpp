﻿#include "GroupPackWidget.h"
#include "ui_GroupPackWidget.h"
#include "QStringLiteralBak.h"

GroupPackWidget::GroupPackWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::GroupPackWidget();
	ui->setupUi(this);

	isRandom = true;

	historyWidget = NULL;
	transWidget = NULL;

	this->setWindowFlags(Qt::Tool | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
	this->setAttribute(Qt::WA_TranslucentBackground);
	this->setAttribute(Qt::WA_DeleteOnClose);

	QFile file(":/QSS/Resources/QSS/ChatWidgetShareLib/GroupPackWidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->closeBtn, SIGNAL(clicked()), this, SIGNAL(sigClose()));
	connect(ui->mPButtonRandom, SIGNAL(clicked()), this, SLOT(onRandomClicked()));
	connect(ui->mPButtonCommon, SIGNAL(clicked()), this, SLOT(onCommonClicked()));
	connect(ui->giveBtn, SIGNAL(clicked()), this, SLOT(onGiveClicked()));
	connect(ui->recordBtn, SIGNAL(clicked()), this, SLOT(onRecordClicked()));
	connect(ui->numberEdit, SIGNAL(textChanged(QString)), this, SLOT(onTextChanged(QString)));
	connect(ui->countEdit, SIGNAL(textChanged(QString)), this, SLOT(onNumChanged(QString)));
	connect(ui->turnInBtn, SIGNAL(clicked()), this, SLOT(slotClickTurnInBtn()));

	ui->numberEdit->setValidator(new QDoubleValidator(0, 10000, 5, this));
	ui->countEdit->setValidator(new QDoubleValidator(0, 10000, 0, this));

	this->getHostingAccount();
}

GroupPackWidget::~GroupPackWidget()
{
	delete ui;

	if (historyWidget)
		delete historyWidget;
	if (transWidget)
		delete transWidget;
}

void GroupPackWidget::show()
{
	QWidget::show();
	this->raise();
	this->activateWindow();
	ui->numberEdit->setFocus();
}

void GroupPackWidget::slotClickTurnInBtn()
{
	if (transWidget)
		transWidget->close();

	transWidget = new TransAccWidget;
	connect(transWidget, SIGNAL(sigClose()), this, SLOT(slotCloseTransWidget()));
	connect(transWidget, SIGNAL(sigHostingCharge(int, QString, QString, QString)), this, SIGNAL(sigHostingCharge(int, QString, QString, QString)));

	foreach(AssetInfo asset, assetsList)
	{
		if (asset.coinName == "PWR")
		{
			transWidget->setInfo(asset.coinName, asset.availableCoin, asset.coinFullName, asset.sysAddress);
			break;
		}
	}

	transWidget->show();
}

void GroupPackWidget::getHostingAccount()
{
	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigAssetsInfo(QList<AssetInfo>)), this, SLOT(slotHostingAccount(QList<AssetInfo>)));
	connect(request, SIGNAL(sigAssetsInfo(QList<AssetInfo>)), request, SLOT(deleteLater()));
	QString userID = gDataManager->getUserInfo().strAccountName;
	request->getHostingAccount(userID);
}
void GroupPackWidget::slotHostingAccount(QList<AssetInfo> assetList)
{
	this->assetsList = assetList;

	foreach(AssetInfo asset, assetList)
	{
		if (asset.coinName == "PWR")
		{
			ui->balanceLabel->setText(QString::number(asset.availableCoin,'f',5) + "PWR");
			break;
		}
	}
}

void GroupPackWidget::onGiveClicked() {
	QString strNumber = ui->numberEdit->text();
	QString strCount = ui->countEdit->text();

	if (strNumber.isEmpty() || strCount.isEmpty())
		IMessageBox::tip(this, tr("Notice"), tr("The amount or the number of the red packet cannot be empty!"));
	else
	{
		QString strBalance = ui->balanceLabel->text();
		strBalance = strBalance.remove(strBalance.right(3));

		double number = strNumber.toDouble();
		int count = strCount.toInt();
		if (number == 0 || count == 0)
		{
			IMessageBox::tip(this, tr("Notice"), tr("The amount or the number of the red packet cannot be 0 !"));
		}
		else
		{
			double amount = isRandom ? number : number * ui->countEdit->text().toInt();
			if (amount > strBalance.toDouble())
			{
				IMessageBox::tip(this, tr("Notice"), tr("The amount of the red packet cannot be greater than your balance!"));
			}
			else
			{
				if (count > numOfGroup) {
					IMessageBox::tip(this, tr("Notice"), tr("The number of the red packet cannot be greater than the number of people in the group!"));
				}
				else 
				{
					if(isRandom){
						if (number / count < 0.00001) {
							IMessageBox::tip(this, tr("Notice"), tr("The amount of each red packet cannot be smaller than 0.00001!"));
						}
						else {
							InputBox *box = new InputBox(this);
							connect(box, SIGNAL(sigEnter(QString)), this, SLOT(slotConfirmPassword(QString)));
							box->init(tr("Please enter your login password:"), false, QLineEdit::Password);
						}
					}
					else
					{
						InputBox *box = new InputBox(this);
						connect(box, SIGNAL(sigEnter(QString)), this, SLOT(slotConfirmPassword(QString)));
						box->init(tr("Please enter your login password:"), false, QLineEdit::Password);
					}
				}
			}
		}
	}
}

void GroupPackWidget::slotConfirmPassword(QString password)
{
	QString userPWD = gDataManager->getUserInfo().strLoginPWD;

	if (userPWD.isEmpty())
	{
		IMessageBox::tip(this, tr("Notice"), tr("Failed to verify the login password!"));
		return;
	}
	if (password.isEmpty())
	{
		IMessageBox::tip(this, tr("Notice"), tr("Login password cannot be empty!"));
		return;
	}

	//进行计算。
	QByteArray array = QCryptographicHash::hash(password.toUtf8(), QCryptographicHash::Sha1);
	QString string = array.toHex();

	if (string.toLower() != userPWD.toLower())
	{
		IMessageBox::tip(this, tr("Notice"), tr("Incorrect Password!"));
		return;
	}
	else
	{
		QString remark = ui->remarkEdit->text();
		if (remark.isEmpty())
			remark = tr("Best Wishes");

		AssetInfo currentAsset;
		foreach(AssetInfo asset, this->assetsList)
		{
			if (asset.coinName == "PWR")
			{
				currentAsset = asset;
				break;
			}
		}

		QString strNumber = ui->numberEdit->text();
		double number = strNumber.toDouble();
		double amount = isRandom ? number : number * ui->countEdit->text().toInt();
		QString strCount = ui->countEdit->text();
		int count = strCount.toInt();
		int type = isRandom ? 0 : 1;

		UserInfo user = gDataManager->getUserInfo();
		OPRequestShareLib *request = new OPRequestShareLib;
		connect(request, SIGNAL(sigCreatePacketID(QString)), this, SLOT(slotCreatePacket(QString)));
		request->createRedPacket(user.strAccountName, currentAsset.trusteeshipId, currentAsset.coinName, type, amount, count, remark, "", number);
	}
}

void GroupPackWidget::slotCreatePacket(QString packetID)
{
	if (packetID.isEmpty())
	{
		IMessageBox::tip(this, tr("Notice"), tr("Failed to prepare the red packet!"));
	}
	else
	{
		QString strNumber = ui->numberEdit->text();
		QString remark = ui->remarkEdit->text();
		if (remark.isEmpty())
			remark = tr("Best Wishes");

		int type = isRandom ? 0 : 1;
		double number = ui->numberEdit->text().toDouble();
		int count = ui->countEdit->text().toInt();
		double amount = isRandom ? number : number * count;

		QString phoneID = gDataManager->getUserInfo().strAccountName;

		QVariantMap map;
		map.insert("packageState", 0);
		map.insert("count", count);
		map.insert("totalMoney", amount);
		map.insert("leaveMessage", remark);
		map.insert("luckMode", type);
		map.insert("assetUnit", ui->coinLabel->text());
		map.insert("packetId", packetID);
		map.insert("trusteeshipCoinId", 1);
		map.insert("phoneId", phoneID);
		QJsonDocument doc = QJsonDocument::fromVariant(map);
		QString data = doc.toJson();

		emit sigGivePacketData(data);
		emit sigClose();
	}
}

void GroupPackWidget::onRecordClicked() {
	if (historyWidget)
		historyWidget->close();

	historyWidget = new RedPackHistory;
	connect(historyWidget, SIGNAL(sigClose()), this, SLOT(slotCloseHistoryWidget()));
}

void GroupPackWidget::setGroup(QString groupID)
{
	this->setObjectName(groupID);

	int count = gDataBaseOpera->DBGetGroupBuddyInfoFromID(groupID).count();
	ui->label_14->setText(tr("There are") + QString::number(count) + tr("people in this group"));

	numOfGroup = count;
}

void GroupPackWidget::onRandomClicked() {
	isRandom = true;
	ui->mPButtonRandom->setStyleSheet("border:none;background-color:#eeeeee;color:#cc3333; ");
	ui->mPButtonCommon->setStyleSheet("border:none;background-color:#eeeeee;color:#999999; ");
	ui->label_5->setStyleSheet("background-color:#cc3333;");
	ui->label_6->setStyleSheet("background-color:#eeeeee;");
	ui->label_8->setText(tr("Amount"));
	ui->label_4->show();
	ui->label_10->setText(tr("Each person gets a random amount"));
	ui->numberEdit->clear();
	ui->remarkEdit->clear();
	ui->countEdit->clear();
	ui->giveLabel->setText(tr("Amount 0PWR"));
}
void GroupPackWidget::onCommonClicked() {
	isRandom = false;
	ui->mPButtonCommon->setStyleSheet("border:none;background-color:#eeeeee;color:#cc3333; ");
	ui->mPButtonRandom->setStyleSheet("border:none;background-color:#eeeeee;color:#999999; ");
	ui->label_6->setStyleSheet("background-color:#cc3333;");
	ui->label_5->setStyleSheet("background-color:#eeeeee;");
	ui->label_8->setText(tr("Amount Each"));
	ui->label_4->hide();
	ui->label_10->setText(tr("Each person gets a identical amount"));
	ui->numberEdit->clear();
	ui->remarkEdit->clear();
	ui->countEdit->clear();
	ui->giveLabel->setText(tr("Amount 0PWR"));
}
void GroupPackWidget::onTextChanged(QString text) {
	QString result=tr("Amount ");
	if (isRandom) {
		result.append(text);
		result.append("PWR");
		ui->giveLabel->setText(result);
	}
	else
	{
		if (!text.isEmpty()) {
			result.clear();
			double amount = ui->countEdit->text().toInt() * text.toDouble();
			result.append(tr("Amount "));
			QString number = QString::number(amount, 'f');
			while (number.endsWith("0") || number.endsWith("."))
			{
				number.chop(1);
			}
			result.append(number);
			result.append("PWR");
			ui->giveLabel->setText(result);
		}
		else
		{
			ui->giveLabel->setText(tr("Amount 0PWR"));
		}
	}
}
void GroupPackWidget::onNumChanged(QString text) {
	QString result = tr("Amount ");

	if (!isRandom) {
		double amount = text.toInt() * ui->numberEdit->text().toDouble();
		QString number = QString::number(amount, 'f');
		while (number.endsWith("0") || number.endsWith("."))
		{
			number.chop(1);
		}
		result.append(number);
		result.append("PWR");
		ui->giveLabel->setText(result);
	}
}
void GroupPackWidget::paintEvent(QPaintEvent * event) {
	QPainterPath path;
	path.setFillRule(Qt::WindingFill);
	path.addRect(6, 6, this->width() - 12, this->height() - 12);

	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.fillPath(path, QBrush(Qt::white));

	QColor color(0, 0, 0, 50);//, 50);
	for (int i = 0; i < 6; i++)
	{
		QPainterPath path;
		path.setFillRule(Qt::WindingFill);
		path.addRect(6 - i, 6 - i, this->width() - (6 - i) * 2, this->height() - (6 - i) * 2);
		color.setAlpha(110 - qSqrt(i) * 40);
		painter.setPen(color);
		painter.drawPath(path);
	}
}
void GroupPackWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		mMoveing = true;
		mMovePosition = event->globalPos() - pos();
	}
	return QWidget::mousePressEvent(event);
}

void GroupPackWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mMoveing && (event->buttons() && Qt::LeftButton)
		&& (event->globalPos() - mMovePosition).manhattanLength() > QApplication::startDragDistance())
	{
		move(event->globalPos() - mMovePosition);
		mMovePosition = event->globalPos() - pos();
	}
	return QWidget::mouseMoveEvent(event);
}

void GroupPackWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		mMoveing = false;
	}
}

void GroupPackWidget::slotCloseHistoryWidget()
{
	if (historyWidget)
		historyWidget->close();

	historyWidget = NULL;
}

void GroupPackWidget::slotCloseTransWidget()
{
	if (transWidget)
		transWidget->close();

	transWidget = NULL;
}
