﻿#include "chatdatamanager.h"
#include "QStringLiteralBak.h"
#include "immainwidget.h"
#include "profilemanager.h"
#include <QJsonArray>
#include "MessageWidget/messageliststore.h"
#include "MessageWidget/messagelistdispatcher.h"
#include "MessageWidget/messagelistviewmodel.h"
#include "imperchatview.h"
#include "imperchatdispatcher.h"
#include "imperchatstore.h"
#include "imgroupview.h"
#include "imgroupchatdispatcher.h"
#include "imgroupdispatcher.h"
#include "imgroupchatview.h"
#include "imgroupchatstore.h"

ChatDataManager* ChatDataManager::pThis = NULL;

ChatDataManager::ChatDataManager(QObject *parent)
	: QObject(parent), m_isFirstShowChat(true)
{
	pThis = this;

	messageLog = NULL;
	chatWidget = new ChatWidget();
	searchList = chatWidget->getSearchList();
	messageList = chatWidget->getMessageListView();
	m_pPicWidget = NULL;

	MessageListStore::instance()->initHistory();
	stackedWidget = chatWidget->getStackedWidget();

	connect(chatWidget, SIGNAL(sigShowDeviceWidget()), this, SIGNAL(sigShowDeviceWidget()));
	connect(chatWidget, SIGNAL(sigSearchText(QString)), this, SLOT(slotSearchText(QString)));
	connect(searchList, SIGNAL(sigOpenChat(int, QVariant)), this, SLOT(recvChat(int, QVariant)));
	connect(searchList, SIGNAL(sigOpenChat(int, QVariant)), chatWidget, SLOT(slotClickCancelSearch()));

	connect(messageList->dispatcher(), SIGNAL(sigClickItem(QString)), this, SLOT(slotClickedChat(QString)));
	connect(messageList->dispatcher(), SIGNAL(sigSetMsgRead(QString)), this, SLOT(slotSetMsgRead(QString)));
	connect(messageList->dispatcher(), SIGNAL(sigCloseChat(QString)), this, SLOT(slotCloseChat(QString)), Qt::QueuedConnection);
	connect(messageList->dispatcher(), SIGNAL(sigCloseAllChat()), this, SLOT(slotCloseAllChat()), Qt::QueuedConnection);

	connect(messageList->dispatcher(), SIGNAL(sigCloseChatQuery(QString)), this, SLOT(slotCloseChatQuery(QString)), Qt::QueuedConnection);
	connect(messageList->dispatcher(), SIGNAL(sigCloseAllChatQuery()), this, SLOT(slotCloseAllChatQuery()), Qt::QueuedConnection);
	connect(IMMainWidget::self(), SIGNAL(sigAskExit()), this, SLOT(slotAskExit()), Qt::QueuedConnection);

	connect(messageList->dispatcher(), SIGNAL(sigDealTrayIocnFlash(QString)), this, SIGNAL(sigDealTrayIocnFlash(QString)));
	connect(messageList->dispatcher(), SIGNAL(sigMessageUnreadNumChanged(int)), this, SIGNAL(sigCountMessageNum(int)));

	connect(this, SIGNAL(sigUpdateSelfMessage(bool, QVariant, QString, bool, QMap<QString, QVariant>)), messageList->dispatcher(), SIGNAL(sigInsertSelfMessage(bool, QVariant, QString, bool, QMap<QString, QVariant>)));

	connect(profilemanager::getInstance(), SIGNAL(sigShareID(int, QString)), this, SLOT(slotShareWidget(int, QString)));
	connect(profilemanager::getInstance(), SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget*)), this, SLOT(slotOpenPic(QString, QList<QString>*, QWidget*)));

	connect(this, SIGNAL(sigWebEngineViewTermination(QString)), this, SLOT(slotWebEngineViewTermination(QString)));



	if (messageList->vm()->count() > 0)
	{
		emit messageList->dispatcher()->sigClickIndex(0);
	}

	//加载设备登录信息，因为刚登录时获取到的信息不完整，因此延时一秒进行请求。
	QTimer::singleShot(1000, this, SLOT(slotRefreshDeviceInfo()));
}

ChatDataManager::~ChatDataManager()
{
	if (m_pPicWidget)
		delete m_pPicWidget;
}

ChatDataManager* ChatDataManager::self()
{
	return pThis;
}

ChatWidget * ChatDataManager::getChatWidget()
{
	return chatWidget;
}

void ChatDataManager::recvChat(int type, QVariant message, QVariant info)
{
	switch (type)
	{
	case PerMessage:
		OnDealPerMessage(message, info);
		break;
	case GroupMessage:
		OnDealGroupMessage(message, info);
		break;
	case OpenPer:
		OnDealOpenPerMessage(message);
		break;
	case OpenGroup:
		OnDealOpenGroupMessage(message);
		break;
	case BuddyUpdate:
		OnDealBuddyUpdateMessage(message);
		break;
	case GroupUpdate:
		OnDealGroupUpdateMessage(message);
		break;
	case QuitGroup:
		OnDealQuitGroupMessage(message, info);
		break;
	case GroupAddUser:
		OnDealGroupAddUserMessage(message, info);
		break;
	case GroupNoSpeak:
		OnDealGroupNoSpeak(message, info);
		break;
	case BuddyDelete:
		OnDealBuddyDelete(message);
	default:
		break;
	}
}

// Qualifier: 处理接收到的个人消息
void ChatDataManager::OnDealPerMessage(QVariant message, QVariant info)
{
	MessageInfo messageInfo = message.value<MessageInfo>();
	BuddyInfo buddyInfo = info.value<BuddyInfo>();
	if (messageInfo.strMsg == "load" && messageInfo.MessageChildType != MessageType::Message_TEXT)
	{
		//如果判断出是加载消息（非文字消息且内容为load），就不在messageList显示。
	}
	else
	{
		emit MessageListDispatcher::instance()->sigInsertPerToPerMessage(messageInfo, buddyInfo, true);
	}

	IMPerChatView *perChat = NULL;
	for (int i = 0; i < stackedWidget->count(); i++)
	{
		QWidget *widget = stackedWidget->widget(i);
		if (widget)
		{
			QString widgetID = widget->objectName();
			if (widgetID.toInt() == buddyInfo.nUserId)
				perChat = (IMPerChatView *)widget;
		}
	}

	if (perChat)
	{
		UserInfo userInfo = gDataManager->getUserInfo();
		/*判断是不是当前窗口，如果是当前窗口，需要设置消息已读*/
		QWidget *currentWidget = stackedWidget->currentWidget();
		if (currentWidget)
		{
			if ((IMPerChatView *)currentWidget == perChat)
			{
				QList<QByteArray> msgIDList = gSocketMessage->SetPerMessageRead(messageInfo.nFromUserID);
				for (int i = 0; i < msgIDList.size(); i++)
				{
					gSocketMessage->SetMessageState(msgIDList[i], MESSAGE_STATE_READ);
					gSocketMessage->SendMessageReadMessage(userInfo.nUserID, messageInfo.nFromUserID, 0, QString(msgIDList[i]));
				}
			}
		}

		//插入时间线
		emit perChat->dispatcher()->sigInsertTimeLine(messageInfo.ServerTime);
		emit perChat->dispatcher()->sigProcessRecvMessageInfo(messageInfo, buddyInfo);
	}
}

// Qualifier: 处理接收到的部落消息
void ChatDataManager::OnDealGroupMessage(QVariant message, QVariant info)
{
	MessageInfo messageInfo = message.value<MessageInfo>();
	GroupInfo groupInfo = info.value<GroupInfo>();
	if (messageInfo.strMsg == "load" && messageInfo.MessageChildType != MessageType::Message_TEXT)
	{
		//如果判断出是加载消息（非文字消息且内容为load），就不在messageList显示。
	}
	else
	{
		qDebug() << "OnDealGroupMessage";
		emit MessageListDispatcher::instance()->sigInsertPerToGroupMessage(messageInfo, groupInfo, true);
	}

	IMGroupView *groupChat = NULL;
	for (int i = 0; i < stackedWidget->count(); i++)
	{
		QWidget *widget = stackedWidget->widget(i);
		if (widget)
		{
			QString widgetID = widget->objectName();
			if (widgetID == groupInfo.groupId)
				groupChat = (IMGroupView *)widget;
		}
	}
	if (groupChat)
	{
		UserInfo userInfo = gDataManager->getUserInfo();
		/*判断是不是当前窗口，如果是当前窗口，需要设置消息已读*/
		QWidget *currentWidget = stackedWidget->currentWidget();
		if (currentWidget)
		{
			if ((IMGroupView *)currentWidget == groupChat)
			{
				QList<QByteArray> msgIDList = gSocketMessage->SetPerMessageRead(messageInfo.nFromUserID);
				for (int i = 0; i < msgIDList.size(); i++)
				{
					gSocketMessage->SetMessageState(msgIDList[i], MESSAGE_STATE_READ);
				}
			}
		}

		if (groupChat && groupChat->getGroupChatView())
		{
			emit groupChat->getGroupChatView()->dispatcher()->sigProcessRecvMessageInfo(messageInfo);
		}
	}
}

// Qualifier: 处理接收到的打开个人消息
void ChatDataManager::OnDealOpenPerMessage(QVariant message)
{
	//填充消息
	UserInfo userInfo = gDataManager->getUserInfo();
	BuddyInfo buddyInfo = gDataBaseOpera->DBGetBuddyInfoByID(message.toString());
	if (buddyInfo.strNickName.isEmpty())
		buddyInfo = gDataBaseOpera->DBGetGroupUserFromID(message.toString());
	int iId = message.toInt();
	QMap<QString, QList<MessageInfo> > mapTemp = gSocketMessage->DBGetRecnetMessage(userInfo.nUserID, iId);
	QMap<QString, QList<MessageInfo> >::iterator itor = mapTemp.begin();

	bool isExist = false;
	for (; itor != mapTemp.end(); ++itor)
	{
		if (itor.key() == message.toString())
		{
			isExist = true;

			MessageInfo MsgTmp = itor.value().last();

			//双击打开聊天窗口不需要显示@成员符号，显示普通文本格式即可
			//strMsg可能后面会被用到，因为是自己构造的结构，可以将其赋值为普通文本格式

			emit MessageListDispatcher::instance()->sigDealPerMessageType(MsgTmp, buddyInfo);

			break;
		}
	}

	if (!isExist)
	{
		MessageInfo MsgTmp;
		MsgTmp.ClientTime = QDateTime::currentMSecsSinceEpoch();
		MsgTmp.MessageChildType = 0;
		MsgTmp.strMsg = "";


		emit MessageListDispatcher::instance()->sigDealPerMessageType(MsgTmp, buddyInfo);
	}

}

// Qualifier: 处理接收到的打开部落消息
void ChatDataManager::OnDealOpenGroupMessage(QVariant message)
{
	//填充消息
	UserInfo userInfo = gDataManager->getUserInfo();
	GroupInfo groupInfo = gDataBaseOpera->DBGetGroupFromID(message.toString());
	QMap<QString, QList<MessageInfo> > mapTemp = gSocketMessage->DBGetRecnetMessage(userInfo.nUserID, groupInfo.groupId.toInt());
	QString strID = groupInfo.groupId;
	QMap<QString, QList<MessageInfo> >::iterator itor = mapTemp.begin();

	bool isExist = false;
	for (; itor != mapTemp.end(); ++itor)
	{
		if (itor.key() == strID)
		{
			isExist = true;

			MessageInfo MsgTmp = mapTemp[strID].last();

			//双击打开聊天窗口不需要显示@成员符号，显示普通文本格式即可
			//strMsg可能后面会被用到，因为是自己构造的结构，可以将其赋值为普通文本格式
			emit MessageListDispatcher::instance()->sigDealGroupMessageType(MsgTmp, groupInfo);
			break;
		}
	}
	if (!isExist)
	{
		MessageInfo MsgTmp;
		MsgTmp.ClientTime = QDateTime::currentMSecsSinceEpoch();
		MsgTmp.MessageChildType = 0;
		MsgTmp.strMsg = "";

		emit MessageListDispatcher::instance()->sigDealGroupMessageType(MsgTmp, groupInfo);
	}


	IMGroupView *groupWidget = getGroupChat(groupInfo.groupId);
	if (groupWidget)
	{
		stackedWidget->setCurrentWidget(groupWidget);
#ifdef Q_OS_WIN
		gDataManager->setCurrentChatWidget(groupWidget->getChatWidgetID());
#endif
	}
}

// Qualifier: 处理接收到的更新好友消息
void ChatDataManager::OnDealBuddyUpdateMessage(QVariant message)
{
	emit MessageListDispatcher::instance()->sigUpdateMessage(false, message);
	BuddyInfo buddyInfo = message.value<BuddyInfo>();
	for (int i = 0; i < stackedWidget->count(); i++)
	{
		QWidget *widget = stackedWidget->widget(i);
		if (widget)
		{
			if (widget->objectName() == QString::number(buddyInfo.nUserId))
			{
				IMPerChatView *perChat = (IMPerChatView *)widget;
				if (buddyInfo.strNote.isEmpty())
					emit perChat->dispatcher()->sigSetNickNameAndBuddyId(buddyInfo.strNickName, QString::number(buddyInfo.nUserId));
				else
					emit perChat->dispatcher()->sigSetNickNameAndBuddyId(buddyInfo.strNote, QString::number(buddyInfo.nUserId));
				break;
			}
		}
	}
}

// Qualifier: 处理接收到的更新部落消息
void ChatDataManager::OnDealGroupUpdateMessage(QVariant message)
{
	emit MessageListDispatcher::instance()->sigUpdateMessage(true, message);
	GroupInfo groupInfo = message.value<GroupInfo>();
	for (int i = 0; i < stackedWidget->count(); i++)
	{
		QWidget *widget = stackedWidget->widget(i);
		if (widget)
		{
			if (widget->objectName() == groupInfo.groupId)
			{
				IMGroupView *groupChat = (IMGroupView *)widget;

				emit groupChat->dispatcher()->sigSetGroupNameAndGroupId(groupInfo.groupName, groupInfo.groupId);
				break;
			}
		}
	}
}

// Qualifier: 处理接收到的退出部落消息
void ChatDataManager::OnDealQuitGroupMessage(QVariant message, QVariant info)
{
	QString groupID = message.toString();
	QString userID = info.toString();

	UserInfo userInfo = gDataManager->getUserInfo();
	if (userInfo.nUserID == userID.toInt() || userID.isEmpty())
	{
		slotCloseChat(groupID);
	}
	else
	{
		for (int i = 0; i < stackedWidget->count(); i++)
		{
			QWidget *widget = stackedWidget->widget(i);
			if (widget)
			{
				if (widget->objectName() == groupID)
				{
					IMGroupView *groupChat = (IMGroupView *)widget;

					if (groupChat && groupChat->getGroupChatView())
					{
						emit groupChat->getGroupChatView()->dispatcher()->sigUserQuitGroup(userID);
					}
					
					break;
				}
			}
		}
	}
}

void ChatDataManager::OnDealBuddyDelete(QVariant message)
{
	QString buddyId = message.toString();
	slotCloseChat(buddyId);
}

// Qualifier: 处理接收到的部落添加用户消息
void ChatDataManager::OnDealGroupAddUserMessage(QVariant message, QVariant info)
{
	QVariantMap map = QJsonDocument::fromJson(message.toString().toUtf8()).toVariant().toMap();
	QString groupID = map.value("groupID").toString();
	QString manaID = map.value("manaID").toString();

	BuddyInfo buddyInfo = info.value<BuddyInfo>();
	for (int i = 0; i < stackedWidget->count(); i++)
	{
		QWidget *widget = stackedWidget->widget(i);
		if (widget)
		{
			if (widget->objectName() == groupID)
			{
				IMGroupView *groupChat = (IMGroupView *)widget;

				if (groupChat && groupChat->getGroupChatView())
				{
					emit groupChat->getGroupChatView()->dispatcher()->sigInsertGroupUser(buddyInfo);
				}
				
				break;
			}
		}
	}
}

void ChatDataManager::OnDealGroupNoSpeak(QVariant id, QVariant noSpeak)
{
	QString groupID = QString::number(id.toInt());
	for (int i = 0; i < stackedWidget->count(); i++)
	{
		QWidget *widget = stackedWidget->widget(i);
		if (widget)
		{
			if (widget->objectName() == groupID)
			{
				IMGroupView *groupChat = (IMGroupView *)widget;
				if (groupChat && groupChat->getGroupChatView())
				{
					emit groupChat->getGroupChatView()->dispatcher()->sigSetNoSpeak(noSpeak.toInt());
				}
				
				break;
			}
		}
	}
}

IMPerChatView * ChatDataManager::getPerChat(QString uesrID)
{
	IMPerChatView *perChat = NULL;
	for (int i = 0; i < stackedWidget->count(); i++)
	{
		QWidget *widget = stackedWidget->widget(i);
		if (widget)
		{
			QString widgetID = widget->objectName();
			if (widgetID == uesrID)
				perChat = (IMPerChatView *)widget;
		}
	}

	if (perChat == NULL)
	{
		perChat = new IMPerChatView(uesrID);
		connect(perChat->dispatcher(), SIGNAL(sigOpenPerLog(QString)), this, SLOT(slotShowPerLog(QString)));
		connect(perChat->dispatcher(), SIGNAL(sigWidgetMinSize()), this, SIGNAL(sigWidgetMinSize()));
		connect(perChat->dispatcher(), SIGNAL(sigShowNormalWindow()), this, SIGNAL(sigShowNormalWindow()));
		connect(perChat->dispatcher(), SIGNAL(sigKeyUpDown(QKeyEvent *)), messageList->dispatcher(), SIGNAL(sigKeyUpDown(QKeyEvent *)));
		connect(perChat->dispatcher(), SIGNAL(sigUpdateSelfMessage(bool, QVariant, QString, bool, QMap<QString, QVariant>)), messageList->dispatcher(), SIGNAL(sigInsertSelfMessage(bool, QVariant, QString, bool, QMap<QString, QVariant>)));
		connect(perChat->dispatcher(), SIGNAL(sigTransferRecord(RecordInfo)), this, SIGNAL(sigTransferRecord(RecordInfo)));
		connect(perChat->dispatcher(), SIGNAL(sigAddChatGroup(QString)), this, SIGNAL(sigAddChatGroup(QString)));
		connect(perChat->dispatcher(), SIGNAL(sigTransmit(QString)), this, SLOT(slotTransmitWidget(QString)));
		connect(perChat->dispatcher(), SIGNAL(sigTransferWindow(QString, QString)), this, SIGNAL(sigTransferWindow(QString, QString)));
		connect(perChat->dispatcher(), SIGNAL(sigETHWindow(QString, QString)), this, SIGNAL(sigETHWindow(QString, QString)));
		connect(perChat->dispatcher(), SIGNAL(sigBTCWindow(QString, QString)), this, SIGNAL(sigBTCWindow(QString, QString)));
		connect(perChat->dispatcher(), SIGNAL(sigEOSWindow(QString, QString)), this, SIGNAL(sigEOSWindow(QString, QString)));
		connect(perChat->dispatcher(), SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget*)), this, SLOT(slotOpenPic(QString, QList<QString>*, QWidget*)));//图片浏览//connect(perChat, SIGNAL(sigShareID(int, QString)), this, SLOT(slotShareWidget(int, QString)));
		connect(perChat->dispatcher(), SIGNAL(sigHostingCharge(int, QString, QString, QString)), this, SIGNAL(sigHostingCharge(int, QString, QString, QString)));
		connect(perChat->dispatcher(), SIGNAL(sigUserInputting(int)), this, SLOT(slotUserInputting(int)));
		BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(uesrID);;

		if (buddy.strNickName.isEmpty())
			buddy = gDataBaseOpera->DBGetGroupUserFromID(uesrID);
		
		if (buddy.strNote.isEmpty())
			emit perChat->dispatcher()->sigSetNickNameAndBuddyId(buddy.strNickName, uesrID);
		else
			emit perChat->dispatcher()->sigSetNickNameAndBuddyId(buddy.strNote, uesrID);

		stackedWidget->addWidget(perChat);
	}

	return perChat;
}

IMGroupView * ChatDataManager::getGroupChat(QString groupID)
{
	IMGroupView *groupChat = NULL;
	for (int i = 0; i < stackedWidget->count(); i++)
	{
		QWidget *widget = stackedWidget->widget(i);
		if (widget)
		{
			QString widgetID = widget->objectName();
			if (widgetID == groupID)
				groupChat = (IMGroupView *)widget;
		}
	}

	if (groupChat == NULL)
	{
		groupChat = new IMGroupView(groupID);

		connect(groupChat->dispatcher(), SIGNAL(sigShowGroupBuddyPerChat(int, QVariant)), this, SLOT(recvChat(int, QVariant)));
		connect(groupChat->dispatcher(), SIGNAL(sigOpenGroupLog(QString)), this, SLOT(slotOpenGroupLog(QString)));
		connect(groupChat->dispatcher(), SIGNAL(sigGroupMinSize()), this, SIGNAL(sigWidgetMinSize()));
		connect(groupChat->dispatcher(), SIGNAL(sigShowNormalWindow()), this, SIGNAL(sigShowNormalWindow()));
		connect(groupChat->dispatcher(), SIGNAL(sigKeyUpDown(QKeyEvent *)), messageList->dispatcher(), SIGNAL(sigKeyUpDown(QKeyEvent *)));
		
		connect(groupChat->dispatcher(), SIGNAL(sigUpdateSelfMessage(bool, QVariant, QString, bool, QMap<QString, QVariant>)), messageList->dispatcher(), SIGNAL(sigInsertSelfMessage(bool, QVariant, QString, bool, QMap<QString, QVariant>)));
		connect(groupChat->dispatcher(), SIGNAL(sigMakeGroupHeader(QString)), this, SIGNAL(sigMakeGroupHeader(QString)));
		connect(groupChat->dispatcher(), SIGNAL(sigTransmit(QString)), this, SLOT(slotTransmitWidget(QString)));
		connect(groupChat->dispatcher(), SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget*)), this, SLOT(slotOpenPic(QString, QList<QString>*, QWidget*)));//图片浏览
		connect(groupChat->dispatcher(), SIGNAL(sigHostingCharge(int, QString, QString, QString)), this, SIGNAL(sigHostingCharge(int, QString, QString, QString)));


		GroupInfo groupInfo = gDataBaseOpera->DBGetGroupFromID(groupID);
		emit groupChat->dispatcher()->sigSetGroupNameAndGroupId(groupInfo.groupName, groupID);
		emit groupChat->getGroupChatView()->dispatcher()->sigSetNoSpeak(groupInfo.noSpeak);

		stackedWidget->addWidget(groupChat);
		//如果部落成员没有下载，就下载部落成员。
		if (gDataBaseOpera->DBJudgetGroupBuddyIsEmpty(groupID))
		{
			IMRequestBuddyInfo *request = new IMRequestBuddyInfo();
			UserInfo userInfo = gDataManager->getUserInfo();
			QString url = gDataManager->getAppConfigInfo().MessageServerAddress;
			connect(request, SIGNAL(sigParseGroupBuddyInfo(QString, QList<BuddyInfo>)), this, SLOT(slotParseGroupBuddyInfo(QString, QList<BuddyInfo>)));
			request->RequestGroupBuddyInfo(url, QString::number(userInfo.nUserID), userInfo.strUserPWD, groupID);
		}
		else
		{
			emit groupChat->getGroupChatView()->dispatcher()->sigInsertGroupUserList();
		}
	}

	return groupChat;
}

void ChatDataManager::slotClickedChat(QString userID)
{
	QString OldId = m_currentSelectedChatId;
	m_currentSelectedChatId = userID;//记录当前选中的chat id

	UserInfo userInfo = gDataManager->getUserInfo();
	for (int i = 0; i < stackedWidget->count(); i++)
	{
		QWidget *widget = stackedWidget->widget(i);
		if (widget)
		{
			QString widgetID = widget->objectName();
			if (widgetID == userID)
			{
				stackedWidget->setCurrentWidget(widget);

				if (gDataBaseOpera->DBJudgeGroupIsHaveByID(userID))
				{
					IMGroupView *groupWidget = (IMGroupView *)widget;
#ifdef  Q_OS_WIN
					gDataManager->setCurrentChatWidget(groupWidget->getChatWidgetID());
#endif
					//TODO WJF 此处崩溃过
					emit groupWidget->getGroupChatView()->dispatcher()->sigShowByChatId(false);
					QList<QByteArray> msgIDList = gSocketMessage->SetGroupMessageRead(userID.toInt());
					for (int i = 0; i < msgIDList.size(); i++)
					{
						gSocketMessage->SetMessageState(msgIDList[i], MESSAGE_STATE_READ);
					}
				}
				else
				{
					IMPerChatView *perWidget = (IMPerChatView *)widget;
#ifdef Q_OS_WIN
					gDataManager->setCurrentChatWidget(perWidget->winId());
#endif
					emit perWidget->dispatcher()->sigShowByChatId(false);
					QList<QByteArray> msgIDList = gSocketMessage->SetPerMessageRead(userID.toInt());

					for (int i = 0; i < msgIDList.size(); i++)
					{
						gSocketMessage->SetMessageState(msgIDList[i], MESSAGE_STATE_READ);
						gSocketMessage->SendMessageReadMessage(userInfo.nUserID, userID.toInt(), 0, QString(msgIDList[i]));
					}
				}
				saveDraft(OldId);
				gDataBaseOpera->DBUpdateMessageListInfo(userID.toInt());
				return;
			}
		}
	}

	//没有该聊天界面，创建。
	if (gDataBaseOpera->DBJudgeGroupIsHaveByID(userID))  //是部落
	{
		IMGroupView *groupWidget = getGroupChat(userID);
		if (groupWidget)
		{
#ifdef Q_OS_WIN
			gDataManager->setCurrentChatWidget(groupWidget->getChatWidgetID());
#endif
			stackedWidget->setCurrentWidget(groupWidget);

			emit groupWidget->getGroupChatView()->dispatcher()->sigShowByChatId(true);
			QList<QByteArray> msgIDList = gSocketMessage->SetGroupMessageRead(userID.toInt());
			for (int i = 0; i < msgIDList.size(); i++)
			{
				gSocketMessage->SetMessageState(msgIDList[i], MESSAGE_STATE_READ);
			}
		}
	}
	else
	{
		IMPerChatView *perWidget = getPerChat(userID);
		if (perWidget)
		{
#ifdef Q_OS_WIN
			gDataManager->setCurrentChatWidget(perWidget->winId());
#endif
			stackedWidget->setCurrentWidget(perWidget);

			emit perWidget->dispatcher()->sigShowByChatId(true);
		}
		QList<QByteArray> msgIDList = gSocketMessage->SetPerMessageRead(userID.toInt());

		for (int i = 0; i < msgIDList.size(); i++)
		{
			gSocketMessage->SetMessageState(msgIDList[i], MESSAGE_STATE_READ);
			gSocketMessage->SendMessageReadMessage(userInfo.nUserID, userID.toInt(), 0, QString(msgIDList[i]));
		}
	}
	saveDraft(OldId);
	/*更改数据库状态*/
	gDataBaseOpera->DBUpdateMessageListInfo(userID.toInt());
}

void ChatDataManager::slotSetMsgRead(QString userID)
{
	m_currentSelectedChatId = userID;//记录当前选中的chat id

	UserInfo userInfo = gDataManager->getUserInfo();

#ifdef Q_OS_WIN
	for (int i = 0; i < stackedWidget->count(); i++)
	{
		QWidget *widget = stackedWidget->widget(i);
		if (widget)
		{
			QString widgetID = widget->objectName();
			if (widgetID == userID)
			{
				if (gDataBaseOpera->DBJudgeGroupIsHaveByID(userID))
				{
					QList<QByteArray> msgIDList = gSocketMessage->SetGroupMessageRead(userID.toInt());
					for (int i = 0; i < msgIDList.size(); i++)
					{
						gSocketMessage->SetMessageState(msgIDList[i], MESSAGE_STATE_READ);
					}
				}
				else
				{
					QList<QByteArray> msgIDList = gSocketMessage->SetPerMessageRead(userID.toInt());
					for (int i = 0; i < msgIDList.size(); i++)
					{
						gSocketMessage->SetMessageState(msgIDList[i], MESSAGE_STATE_READ);
						gSocketMessage->SendMessageReadMessage(userInfo.nUserID, userID.toInt(), 0, QString(msgIDList[i]));
					}
				}
				gDataBaseOpera->DBUpdateMessageListInfo(userID.toInt());
				return;
			}
		}
	}
#endif


	//没有该聊天界面，创建。
	if (gDataBaseOpera->DBJudgeGroupIsHaveByID(userID))  //是部落
	{
		IMGroupView *groupWidget = getGroupChat(userID);
		if (groupWidget)
		{
			QList<QByteArray> msgIDList = gSocketMessage->SetGroupMessageRead(userID.toInt());
			for (int i = 0; i < msgIDList.size(); i++)
			{
				gSocketMessage->SetMessageState(msgIDList[i], MESSAGE_STATE_READ);
			}
		}
	}
	else
	{
		QList<QByteArray> msgIDList = gSocketMessage->SetPerMessageRead(userID.toInt());
		for (int i = 0; i < msgIDList.size(); i++)
		{
			gSocketMessage->SetMessageState(msgIDList[i], MESSAGE_STATE_READ);
			gSocketMessage->SendMessageReadMessage(userInfo.nUserID, userID.toInt(), 0, QString(msgIDList[i]));
		}
	}

	/*更改数据库状态*/
	gDataBaseOpera->DBUpdateMessageListInfo(userID.toInt());
}


void ChatDataManager::slotCloseChat(QString userID)
{
	for (int j = 0; j < stackedWidget->count(); j++)
	{
		QWidget *widget = stackedWidget->widget(j);
		if (widget)
		{
			if (widget->objectName() == userID)
			{
				//如果要关闭的是当前窗口，将当前聊天窗体指针设置为null。
#ifdef Q_OS_WIN
				if (widget == stackedWidget->currentWidget())
					gDataManager->setCurrentChatWidget(NULL);
#endif
				widget->deleteLater();
				stackedWidget->removeWidget(widget);
			}
		}
	}


}

void ChatDataManager::slotCloseChatQuery(QString userID)
{
	for (int j = 0; j < stackedWidget->count(); j++)
	{
		QWidget *widget = stackedWidget->widget(j);
		if (widget)
		{
			if (widget->objectName() == userID)
			{
				IMPerChatView *perChat = qobject_cast<IMPerChatView *>(widget);
				IMGroupView *GrpChat = qobject_cast<IMGroupView *>(widget);
				bool bIsDown;
				if (gDataBaseOpera->DBJudgeGroupIsHaveByID(userID))
				{
					bIsDown = GrpChat->getGroupChatView()->store()->IsDownloading();
				}
				else
				{
					bIsDown = perChat->store()->IsDownloading();
				}
				if (bIsDown)
				{
					IMessageBox* pBox = new IMessageBox();
					pBox->setProperty("ID", userID);
					connect(pBox, SIGNAL(sigOK()), SLOT(slotContuinueClose()));
					pBox->initAsk(tr("Warning"), tr("There are files being transferred，contuinue to close ?"));
				}
				else
				{
					messageList->dispatcher()->sigCloseChat(userID);
				}
				return;
			}
		}
	}
	messageList->dispatcher()->sigCloseChat(userID);
}

void ChatDataManager::slotContuinueClose()
{
	IMessageBox *down = qobject_cast<IMessageBox *>(sender());
	QString userID = down->property("ID").toString();
	messageList->dispatcher()->sigCloseChat(userID);
}

void ChatDataManager::slotCloseAllChat()
{
	for (int j = 0; j < stackedWidget->count(); j++)
	{
		QWidget *widget = stackedWidget->widget(j);
		if (widget)
		{
			//如果要关闭的是当前窗口，将当前聊天窗体指针设置为null。
#ifdef Q_OS_WIN
			gDataManager->setCurrentChatWidget(NULL);
#endif
			widget->deleteLater();
			stackedWidget->removeWidget(widget);
			j--;
		}
	}
#ifdef Q_OS_WIN
	gDataManager->setCurrentChatWidget(NULL);
#endif
}

void ChatDataManager::slotCloseAllChatQuery()
{
	for (int j = 0; j < stackedWidget->count(); j++)
	{
		QWidget *widget = stackedWidget->widget(j);
		if (widget)
		{
			IMPerChatView *perChat = qobject_cast<IMPerChatView *>(widget);
			IMGroupView *GrpChat = qobject_cast<IMGroupView *>(widget);
			bool bIsDown = false;
			if (GrpChat)
			{
				bIsDown = GrpChat->getGroupChatView()->store()->IsDownloading();
			}
			else if(perChat)
			{
				bIsDown = perChat->store()->IsDownloading();
			}
			if (bIsDown)
			{
				IMessageBox* pBox = new IMessageBox();

				connect(pBox, SIGNAL(sigOK()), messageList->dispatcher(),SIGNAL(sigCloseAllChat()));
				pBox->initAsk(tr("Warning"), tr("There are files being transferred，contuinue to close ?"));
				return;
			}
		}
	}
	messageList->dispatcher()->sigCloseAllChat();
}

void ChatDataManager::slotAskExit()
{
	for (int j = 0; j < stackedWidget->count(); j++)
	{
		QWidget *widget = stackedWidget->widget(j);
		if (widget)
		{
			IMPerChatView *perChat = qobject_cast<IMPerChatView *>(widget);
			IMGroupView *GrpChat = qobject_cast<IMGroupView *>(widget);
			bool bIsDown = false;
			if (GrpChat)
			{
				bIsDown = GrpChat->getGroupChatView()->store()->IsDownloading();
			}
			else if (perChat)
			{
				bIsDown = perChat->store()->IsDownloading();
			}
			if (bIsDown)
			{
				IMessageBox* pBox = new IMessageBox();

				connect(pBox, SIGNAL(sigOK()), IMMainWidget::self(), SIGNAL(sigAnswerExit()));
				pBox->initAsk(tr("Warning"), tr("There are files being transferred，contuinue to quit ?"));
				return;
			}
		}
	}
	IMMainWidget::self()->sigAnswerExit();
}

void ChatDataManager::slotParseGroupBuddyInfo(QString groupID, QList<BuddyInfo> list)
{
	for (int i = 0; i < list.count(); i++)
		gDataBaseOpera->DBInsertGroupBuddyInfo(groupID, list.at(i));

	for (int j = 0; j < stackedWidget->count(); j++)
	{
		QWidget *widget = stackedWidget->widget(j);
		if (widget)
		{
			QString widgetID = widget->objectName();
			if (widgetID == groupID)
			{
				IMGroupView *groupWidget = (IMGroupView *)widget;
				emit groupWidget->getGroupChatView()->dispatcher()->sigInsertGroupUserList();
			}
		}
	}
}

void ChatDataManager::slotUpdateMessageState(MessageInfo msgInfo, int nState)
{
	switch (msgInfo.MsgDeliverType)
	{
	case 0:
	{
		IMPerChatView *perChat = NULL;
		for (int i = 0; i < stackedWidget->count(); i++)
		{
			QWidget *widget = stackedWidget->widget(i);
			if (widget)
			{
				QString widgetID = widget->objectName();
				if (widgetID.toInt() == msgInfo.nToUserID)
				{
					perChat = (IMPerChatView *)widget;
					if (perChat)
					{
						emit perChat->dispatcher()->sigUpdateMessageStateInfo(msgInfo.msgID, nState, msgInfo.integral);
					}
					break;
				}
			}
		}
	}
	break;
	case 1:
	{
		IMGroupView *groupChat = NULL;
		for (int i = 0; i < stackedWidget->count(); i++)
		{
			QWidget *widget = stackedWidget->widget(i);
			if (widget)
			{
				QString widgetID = widget->objectName();
				if (widgetID == QString::number(msgInfo.nToUserID))
				{
					groupChat = (IMGroupView *)widget;
					if (groupChat && groupChat->getGroupChatView())
					{
						emit groupChat->getGroupChatView()->dispatcher()->sigUpdateMessageStateInfo(msgInfo.msgID, nState, msgInfo.integral);
					}
					break;
				}
			}
		}
	}
	break;
	default:
		break;
	}
}

void ChatDataManager::slotShowPerLog(QString buddyID)
{
	if (messageLog != NULL)
	{
		messageLog->close();
		messageLog = NULL;
	}
	if (messageLog == NULL)
	{
		messageLog = new MessageLog();
		m_strTmpLogId = buddyID;
		connect(messageLog, SIGNAL(sigInitFinished()), this, SLOT(slotLoadPerLogByID()));
		connect(messageLog, SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget *)), this, SLOT(slotOpenPic(QString, QList<QString>*, QWidget *)));
		connect(messageLog, SIGNAL(sigTransferRecord(RecordInfo)), this, SIGNAL(sigTransferRecord(RecordInfo)));
		connect(messageLog, SIGNAL(sigClose()), this, SLOT(slotCloseMessagelog()));
	}

}

void ChatDataManager::slotOpenGroupLog(QString groupID)
{
	if (messageLog != NULL)
	{
		messageLog->close();
		messageLog = NULL;
	}
	if (messageLog == NULL)
	{
		//增加消息记录对话框。
		messageLog = new MessageLog();
		m_strTmpLogId = groupID;
		connect(messageLog, SIGNAL(sigInitFinished()), this, SLOT(slotLoadGroupLogByID()));
		connect(messageLog, SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget *)), this, SLOT(slotOpenPic(QString, QList<QString>*, QWidget *)));
		connect(messageLog, SIGNAL(sigClose()), this, SLOT(slotCloseMessagelog()));
	}

}

void ChatDataManager::slotLoadGroupLogByID()
{
	messageLog->loadGroupLogByID(m_strTmpLogId);
}

void ChatDataManager::slotLoadPerLogByID()
{
	messageLog->loadPerLogByID(m_strTmpLogId);
}

void ChatDataManager::slotRefreshDeviceInfo()
{
	UserInfo user = gDataManager->getUserInfo();
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotDeviceInfoResult(bool, QString)));
	QString strResult = gDataManager->getAppConfigInfo().MessageServerAddress +
		QString("/IMServer/user/getSessionContextList?userId=%1&passWord=%2").arg(user.nUserID).arg(user.strUserPWD);
	http->getHttpRequest(strResult);
}

void ChatDataManager::slotDeviceInfoResult(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		QVariantList list = map.value("sessionContextList").toList();
		int count = list.count() - 1; //显示的是其他设备的数量，此处是总数量，因此-1.
		chatWidget->setOtherDeviceCount(count);
	}
}

void ChatDataManager::slotRevOtherDeviceMsg(MessageInfo msgInfo)
{
	/*消息列表插入消息*/
	if (messageList)
	{
		UserInfo userInfo = gDataManager->getUserInfo();
		QWidget *widget = NULL;
		for (int i = 0; i < stackedWidget->count(); i++)
		{
			QWidget *page = stackedWidget->widget(i);
			if (page)
			{
				if (page->objectName().toInt() == msgInfo.nToUserID)
				{
					widget = page;
					break;
				}
			}
		}

		if (msgInfo.MsgDeliverType == 0)
		{
			BuddyInfo buddyInfo = gDataBaseOpera->DBGetBuddyInfoByID(QString::number(msgInfo.nToUserID));
			if (buddyInfo.nUserId == -1)
			{
				buddyInfo = gDataBaseOpera->DBGetGroupUserFromID(QString::number(msgInfo.nToUserID));

				if (buddyInfo.strNickName.isEmpty())
				{
					return;
				}
			}

			emit MessageListDispatcher::instance()->sigInsertPerToPerMessage(msgInfo, buddyInfo, false);
			if (widget)
			{
				IMPerChatView *perChat = (IMPerChatView*)widget;
				emit perChat->dispatcher()->sigProcessSendMessageInfo(userInfo.strUserAvatarLocal, msgInfo);
			}
		}
		else if (msgInfo.MsgDeliverType == 1)
		{
			GroupInfo groupInfo = gDataBaseOpera->DBGetGroupFromID(QString::number(msgInfo.nToUserID));

			if (groupInfo.createUserId.isEmpty())
				return;

			emit MessageListDispatcher::instance()->sigInsertPerToGroupMessage(msgInfo, groupInfo, false);
			if (widget)
			{
				IMGroupView *groupWidget = (IMGroupView*)widget;
				emit groupWidget->getGroupChatView()->dispatcher()->sigProcessSendMessageInfo(userInfo.strUserAvatarLocal, msgInfo);
			}
		}
	}
}

/*插入截图*/
void ChatDataManager::InsertTextEditPic(QString strPath)
{
	if (messageList)
	{
		MessageListInfo listInfo = messageList->store()->getCurrentItemInfo();
		if (listInfo.isGroup == 0)  //个人
		{
			IMPerChatView *perChat = NULL;
			for (int i = 0; i < stackedWidget->count(); i++)
			{
				QWidget *widget = stackedWidget->widget(i);
				if (widget)
				{
					QString widgetID = widget->objectName();
					if (widgetID == QString::number(listInfo.nBudyyID))
						perChat = (IMPerChatView *)widget;
				}
			}
			if (perChat != NULL)
				emit perChat->dispatcher()->sigInsertTextEditPic(strPath);
		}
		else if (listInfo.isGroup == 1)
		{
			IMGroupView *groupChat = NULL;
			for (int i = 0; i < stackedWidget->count(); i++)
			{
				QWidget *widget = stackedWidget->widget(i);
				if (widget)
				{
					QString widgetID = widget->objectName();
					if (widgetID == QString::number(listInfo.nBudyyID))
						groupChat = (IMGroupView *)widget;
				}
			}

			if (groupChat != NULL)
			{
				emit groupChat->getGroupChatView()->dispatcher()->sigInsertTextEditPic(strPath);
			}
		}
	}
}

void ChatDataManager::slotSearchText(QString text)
{
	if (!text.isEmpty())
	{
		//将输入小写化处理。
		text = text.toLower();

		QList<BuddyInfo> AllBuddyList = gDataBaseOpera->DBGetBuddyInfo();
		QList<GroupInfo> AllGroupList = gDataBaseOpera->DBGetAllGroupInfo();
		QList<BuddyInfo> AllGroupUserLiist = gDataBaseOpera->DBGetAllGroupUserInfo();

		QList<BuddyInfo> buddyList;
		foreach(BuddyInfo buddy, AllBuddyList)
		{
			if (buddy.BuddyType != 1)
				continue;

			PinYin pinyin;
			if (!buddy.strNickName.isEmpty())
			{
				pinyin = getPinYin(buddy.strNickName);
				if (buddy.strNickName.toLower().contains(text) || pinyin.fullPinYin.contains(text) || pinyin.easyPinYin.contains(text))
				{
					buddyList.append(buddy);
					continue;
				}
			}

			if (!buddy.strNote.isEmpty())
			{
				pinyin = getPinYin(buddy.strNote);
				if (buddy.strNote.toLower().contains(text) || pinyin.fullPinYin.contains(text) || pinyin.easyPinYin.contains(text))
				{
					buddyList.append(buddy);
					continue;
				}
			}
		}

		QList<GroupInfo> groupList;
		foreach(GroupInfo group, AllGroupList)
		{
			PinYin pinyin;
			if (!group.groupName.isEmpty())
			{
				PinYin pinyin = getPinYin(group.groupName);
				if (group.groupName.toLower().contains(text) || pinyin.fullPinYin.contains(text) || pinyin.easyPinYin.contains(text))
				{
					groupList.append(group);
					continue;
				}
			}
		}

		QList<BuddyInfo> GroupbuddyList;
		foreach(BuddyInfo buddy, AllGroupUserLiist)
		{
			UserInfo userInfo = gDataManager->getUserInfo();
			if (buddy.nUserId == userInfo.nUserID)
				continue;

			PinYin pinyin;
			if (!buddy.strNickName.isEmpty())
			{
				pinyin = getPinYin(buddy.strNickName);
				if (buddy.strNickName.toLower().contains(text) || pinyin.fullPinYin.contains(text) || pinyin.easyPinYin.contains(text))
				{
					GroupbuddyList.append(buddy);
					continue;
				}
			}
		}

		searchList->onInsertContacts(buddyList, groupList, GroupbuddyList);
	}
}

void ChatDataManager::slotTransmitWidget(QString msgID)
{
	TransmitMessageWidget *transmitWidget = new TransmitMessageWidget;
	transmitWidget->setObjectName(msgID);
	transmitWidget->init();

	connect(transmitWidget, SIGNAL(sigTransmitContact(int, QString)), this, SLOT(slotTransmitMessage(int, QString)));
	QTimer::singleShot(100, transmitWidget, SLOT(show()));
}

void ChatDataManager::slotTransmitMessage(int type, QString contactID)
{
	TransmitMessageWidget *transmit = qobject_cast<TransmitMessageWidget *>(sender());
	QString msgID = transmit->objectName();

	MessageInfo message = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);
	if (message.msgID == msgID.toUtf8())  //查询消息有效
	{
		if (type == OpenPer)
		{
			this->OnDealOpenPerMessage(QVariant(contactID));
			IMPerChatView *perChat = getPerChat(contactID);
			if (perChat)
				emit perChat->dispatcher()->sigSendTransmitMessage(message);
		}
		if (type == OpenGroup)
		{
			this->OnDealOpenGroupMessage(QVariant(contactID));
			IMGroupView *groupChat = getGroupChat(contactID);
			if (groupChat && groupChat->getGroupChatView())
			{
				emit groupChat->getGroupChatView()->dispatcher()->sigSendTransmitMessage(message);
			}
		}
	}
}

void ChatDataManager::slotShareWidget(int type, QString shareID)
{
	TransmitMessageWidget *transmitWidget = new TransmitMessageWidget;
	QVariantMap map;
	map.insert("type", type);
	map.insert("shareID", shareID);
	transmitWidget->setData(map);
	transmitWidget->init();

	connect(transmitWidget, SIGNAL(sigTransmitContact(int, QString)), this, SLOT(slotShareMessage(int, QString)));
	QTimer::singleShot(100, transmitWidget, SLOT(show()));
}

void ChatDataManager::slotShareMessage(int type, QString recvID)
{
	TransmitMessageWidget *transmit = qobject_cast<TransmitMessageWidget *>(sender());
	QVariantMap map = transmit->getData().toMap();
	int shareType = map.value("type").toInt();
	QString shareID = map.value("shareID").toString();

	if (type == OpenPer)
	{
		this->OnDealOpenPerMessage(QVariant(recvID));
		IMPerChatView *perChat = getPerChat(recvID);
		if (perChat)
			emit perChat->dispatcher()->sigShareID(shareType, shareID);
	}
	if (type == OpenGroup)
	{
		this->OnDealOpenGroupMessage(QVariant(recvID));
		IMGroupView *groupChat = getGroupChat(recvID);

		if (groupChat && groupChat->getGroupChatView())
		{
			emit groupChat->getGroupChatView()->dispatcher()->sigShareID(shareType, shareID);
		}
	}
}

void ChatDataManager::slotTipMessage(int type, QString recvID, QString strMessage)
{
	if (gSocketMessage)
	{
		//向服务器发送消息部分。
		UserInfo userInfo = gDataManager->getUserInfo();
		int deliverType;
		if (type == PerMessage)
			deliverType = 0;
		if (type == GroupMessage)
			deliverType = 1;
		MessageInfo msgInfo = gSocketMessage->SendTextMessage(userInfo.nUserID, recvID.toInt(), deliverType, (short)MessageType::Message_NOTIFY, strMessage);

		//更新至聊天窗口部分。
		if (type == PerMessage)
		{
			for (int i = 0; i < stackedWidget->count(); i++)
			{
				QWidget *widget = stackedWidget->widget(i);
				if (widget)
				{
					if (widget->objectName() == recvID)
					{
						IMPerChatView *perChat = (IMPerChatView *)widget;
						emit perChat->dispatcher()->sigRecvNotifyMessage(msgInfo);
						break;
					}
				}
			}
		}
		if (type == GroupMessage)
		{
			for (int i = 0; i < stackedWidget->count(); i++)
			{
				QWidget *widget = stackedWidget->widget(i);
				if (widget)
				{
					if (widget->objectName() == recvID)
					{
						IMGroupView *groupChat = (IMGroupView *)widget;
						if (groupChat && groupChat->getGroupChatView())
						{
							emit groupChat->getGroupChatView()->dispatcher()->sigProcessRecvMessageInfo(msgInfo);
						}
						break;
					}
				}
			}
		}

		//更新至左侧消息列表部分。
		QJsonDocument doc = QJsonDocument::fromJson(strMessage.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		if (map.value("type").toString() == "notification")
		{
			strMessage = map.value("content").toString();
		}
		if (map.value("type").toString() == "receivedFile")
		{
			strMessage = tr("The other party has successfully received your file");
		}

		if (type == PerMessage)
		{
			BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(recvID);
			if (buddy.strNickName.isEmpty())
				buddy = gDataBaseOpera->DBGetGroupUserFromID(recvID);

			MessageListInfo messageListInfo;
			messageListInfo.nBudyyID = buddy.nUserId;
			messageListInfo.strLastMessage = strMessage;
			messageListInfo.nLastTime = msgInfo.ClientTime;
			messageListInfo.strBuddyName = buddy.strNickName;
			messageListInfo.strBuddyHeaderImage = buddy.strLocalAvatar;
			messageListInfo.messageType = msgInfo.MessageChildType;
			messageListInfo.nUnReadNum = 0;
			messageListInfo.isGroup = 0;
			gDataBaseOpera->DBInsertPerMessageListInfo(messageListInfo);

			QMap<QString, QVariant> params;
			//更新至消息列表，false代表个人消息
			emit sigUpdateSelfMessage(false, QVariant::fromValue(buddy), strMessage, true, params);
		}
		if (type == GroupMessage)
		{
			GroupInfo group = gDataBaseOpera->DBGetGroupFromID(recvID);
			MessageListInfo messageListInfo;
			messageListInfo.nBudyyID = group.groupId.toInt();
			messageListInfo.strLastMessage = strMessage;
			messageListInfo.nLastTime = msgInfo.ClientTime;
			messageListInfo.strBuddyName = group.groupName;
			messageListInfo.strBuddyHeaderImage = group.groupLoacalHeadImage;
			messageListInfo.messageType = msgInfo.MessageChildType;
			messageListInfo.nUnReadNum = 0;
			messageListInfo.isGroup = 1;
			gDataBaseOpera->DBInsertGroupMessageListInfo(messageListInfo);

			QMap<QString, QVariant> params;
			//更新至消息列表，false代表个人消息
			//更新至消息列表，true代表部落消息
			emit sigUpdateSelfMessage(true, QVariant::fromValue(group), strMessage, true, params);
		}
	}
}

void ChatDataManager::slotQuickReply(int type, QString contactID, QString strMsg)
{
	MessageInfo info;
	info.MessageChildType = MessageType::Message_TEXT;
	info.strMsg = strMsg;

	if (type == OpenPer)
	{
		this->OnDealOpenPerMessage(QVariant(contactID));
		IMPerChatView *perChat = getPerChat(contactID);
		if(perChat)
			emit perChat->dispatcher()->sigSendTransmitMessage(info);

	}
	if (type == OpenGroup)
	{
		this->OnDealOpenGroupMessage(QVariant(contactID));
		IMGroupView *groupChat = getGroupChat(contactID);

		if (groupChat && groupChat->getGroupChatView())
		{
			emit groupChat->getGroupChatView()->dispatcher()->sigSendTransmitMessage(info);
		}
	}
}


void ChatDataManager::updateBuddyHeaderImgPath(int userId, QString path)
{
	if (messageList)
	{
		BuddyInfo stTmp = gDataBaseOpera->DBGetBuddyInfoByID(QString::number(userId));
		emit MessageListDispatcher::instance()->sigUpdateMessage(false, QVariant::fromValue(stTmp));
	}
}

void ChatDataManager::slotOpenPic(QString strfileName, QList<QString>* listPath, QWidget * pParent)
{
	QString fileName = "";
	if (strfileName.indexOf("file:///") > -1)
	{
#ifdef Q_OS_WIN
		fileName = strfileName.mid(strfileName.indexOf("file:///") + 8);
#else
		fileName = strfileName.mid(strfileName.indexOf("file:///") + 7);
#endif
	}
	else
	{
		fileName = strfileName;
	}


	if (m_pPicWidget != NULL)
	{
		m_pPicWidget->deleteLater();

		m_pPicWidget = NULL;
	}
	m_pPicWidget = new PicWidget();
	if (NULL != listPath)
	{
		connect(m_pPicWidget, SIGNAL(sigPicClose()), this, SLOT(slotClosePic()));
		connect(m_pPicWidget, SIGNAL(sigChangePic(int)), this, SLOT(slotChangePic(int)));
		m_pPicWidget->setPath(fileName, pParent);

		m_listPic = *listPath;
		for (m_PicIter = m_listPic.begin(); m_PicIter != m_listPic.end(); m_PicIter++)
		{
			QString strTmp = *m_PicIter;
			if (strTmp == fileName)
			{
				break;
			}
		}
	}
	else
	{
		connect(m_pPicWidget, SIGNAL(sigPicClose()), this, SLOT(slotClosePic()));
		m_pPicWidget->setPath(fileName, pParent, 1);//默认0 1为不显示上一张下一张按钮
	}

	m_pPicWidget->show();
}

void ChatDataManager::slotClosePic()
{
	m_pPicWidget = NULL;
}
void ChatDataManager::slotChangePic(int iType)
{
	QString strText = *m_PicIter;
	if (iType == 0)//上一个
	{
		if (m_PicIter != m_listPic.begin())
		{
			m_PicIter--;
			m_pPicWidget->resetPath(*m_PicIter);
			if (m_PicIter == m_listPic.begin())
			{
				m_pPicWidget->setNoteTip(1);
			}
			else
			{
				m_pPicWidget->setNoteTip(0);
			}
		}
		else
		{
			m_pPicWidget->setNoteTip(1);
		}
	}
	else if (iType == 1)
	{
		if (m_PicIter + 1 != m_listPic.end())
		{
			m_PicIter++;
			m_pPicWidget->resetPath(*m_PicIter);
			if (m_PicIter + 1 == m_listPic.end())
			{
				m_pPicWidget->setNoteTip(2);
			}
			else
			{
				m_pPicWidget->setNoteTip(0);
			}
		}
		else
		{
			m_pPicWidget->setNoteTip(2);
		}
	}
}


QString ChatDataManager::decodeURI(QString str)
{
	QByteArray array;
	for (int i = 0; i < str.length();) {
		if (0 == QString::compare(str.mid(i, 1), QString("%"))) {
			if ((i + 2) < str.length()) {
				array.append(str.mid(i + 1, 2).toShort(0, 16));
				i = i + 3;
			}
			else {
				array.append(str.mid(i, 1));
				i++;
			}
		}
		else {
			if (str.mid(i, 1) == "+")
				array.append(" ");
			else
				array.append(str.mid(i, 1));
			i++;
		}
	}
	QTextCodec *code = QTextCodec::codecForName("UTF-8");
	return code->toUnicode(array);
}

void ChatDataManager::slotTransferMsg(QString strUser, QString strToUser, QString strMsg)
{
	MessageInfo msgInfo = gSocketMessage->SendTextMessage(strUser.toInt(), strToUser.toInt(), 0, (short)8, strMsg);
	slotRevOtherDeviceMsg(msgInfo);
}


void ChatDataManager::slotWebEngineViewTermination(QString chatId)
{
	if (messageList->store()->isExistUserId(chatId))
	{
		for (int j = 0; j < stackedWidget->count(); j++)
		{
			QWidget *widget = stackedWidget->widget(j);
			if (widget)
			{
				if (widget->objectName() == chatId)
				{
					widget->deleteLater();
					stackedWidget->removeWidget(widget);
				}
			}
		}
	}


	if (m_currentSelectedChatId == chatId)
	{
		QTimer::singleShot(0, this, [=] {

			emit messageList->dispatcher()->sigClickItem(m_currentSelectedChatId);
		});
	}
}

void ChatDataManager::RevInputting(MessageInfo msgInfo)
{
	int iType = 1;
	QString strBuddyId = "";
	QString strMessage = msgInfo.strMsg;
	QJsonParseError jsonError;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(strMessage.toUtf8(), &jsonError);
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QVariantMap result = jsonDocument.toVariant().toMap();
			iType = result["isInput"].toInt();
			strBuddyId = result["fromUserId"].toString();
		}
	}
	IMPerChatView *perChat = NULL;
	for (int i = 0; i < stackedWidget->count(); i++)
	{
		QWidget *widget = stackedWidget->widget(i);
		if (widget)
		{
			QString widgetID = widget->objectName();
			if (widgetID == strBuddyId)
				perChat = (IMPerChatView *)widget;
		}
	}

	if (perChat)
	{
		QWidget *currentWidget = stackedWidget->currentWidget();
		if (currentWidget)
		{
			if ((IMPerChatView *)currentWidget == perChat)
			{
				if (iType == 1)
					emit perChat->dispatcher()->sigStartInputting();
				if (iType == 0)
					emit perChat->dispatcher()->sigEndInputting();
			}
		}
	}
}

void ChatDataManager::slotUserInputting(int iType)
{
	IMPerChatView *perChat = qobject_cast<IMPerChatView *>(sender()->parent());
	QString buddyId = perChat->objectName();
	UserInfo userinfo = gDataManager->getUserInfo();

	QJsonObject json;
	json.insert("CMD", "inputting");
	json.insert("isInput", iType);
	json.insert("fromUserId", QString::number(userinfo.nUserID));
	QString strJSON = QString(QJsonDocument(json).toJson());
	strJSON.remove("\n");
	MessageInfo msg = gSocketMessage->SendTextMessage(userinfo.nUserID, buddyId.toInt(), 0, (short)MessageType::Message_INPUTTING, strJSON);
}

void ChatDataManager::saveDraft(QString buddyId)
{
	//获取对应窗口
	QWidget *Oldwidget = NULL;
	int order = 0;
	for (int i = 0; i < stackedWidget->count(); i++)
	{
		QWidget * widget = stackedWidget->widget(i);
		if (widget)
		{
			QString widgetID = widget->objectName();
			if (widgetID == buddyId && stackedWidget->currentWidget()->objectName() != widgetID)
			{
				Oldwidget = widget;
				order = i;
				break;
			}
		}
	}
	IMPerChatView *perChat = qobject_cast<IMPerChatView *>(Oldwidget);
	IMGroupView *groupChat = qobject_cast<IMGroupView *>(Oldwidget);
	QString strDraft = "";
	//判断是否已关闭
	if (perChat)
	{
		//获取输入框内容
		strDraft = perChat->store()->getDraft();
		if (strDraft.isEmpty())
			return;
		int time = QDateTime::currentDateTime().toTime_t();
		BuddyInfo buddyInfo = gDataBaseOpera->DBGetBuddyInfoByID(buddyId);
		emit messageList->dispatcher()->sigSetDraft(QString::number(buddyInfo.nUserId), strDraft);
	}
	if (groupChat)
	{
		//获取输入框内容
		strDraft = groupChat->getGroupChatView()->store()->getDraft();
		if (strDraft.isEmpty())
			return;
		int time = QDateTime::currentDateTime().toTime_t();
		GroupInfo grpInfp = gDataBaseOpera->DBGetGroupFromID(buddyId);
		emit messageList->dispatcher()->sigSetDraft(grpInfp.groupId, strDraft);
	}
}

void ChatDataManager::slotCloseMessagelog()
{
	messageLog = NULL;
}