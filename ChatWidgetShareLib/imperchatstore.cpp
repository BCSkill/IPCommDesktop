﻿#include "imperchatstore.h"
#include "imbuddy.h"
#include "imdatabaseoperainfo.h"
#include "imperchatdispatcher.h"
#include "imperchatviewmodel.h"
#include "settingsmanager.h"
#include "amrDec/amrdec.h"
#include "VedioFrameOpera.h"
#include "childWidget/filetypeex.h"
#include "childWidget/expresswidget.h"
#include "common.h"
#include "imdatamanagersharelib.h"
#include "httpnetworksharelib.h"
#include "imsocketmessageinfo.h"
#include "questionbox.h"
#include "imperchatview.h"
#include "messagebox.h"
#include "imtranstype.h"
#include <QFile>
#include <QImage>
#include <QImageReader>
#include <QDir>
#include <QCryptographicHash>
#include <QDebug>
#include <QJsonDocument>
#include <QUuid>
#include <QApplication>
#include <QMimeData>
#include <QClipboard>
#include <QSettings>
#include <QFileDialog>
#ifndef Q_OS_WIN
#include <qprocess.h>
#include "oescreenshot.h"
#endif

extern IMDataBaseOperaInfo *gDataBaseOpera;
extern SettingsManager* gSettingsManager;
extern IMDataManagerShareLib *gDataManager;
extern IMSocketMessageInfo *gSocketMessage;

extern QString gI18NLocale;

extern QString gThemeStyle;


/*获取当前时间点*1000宏定义*/
#define GETLOCALTIME (1000*((qlonglong)(QDateTime::currentDateTime().toTime_t())))

IMPerChatStore::IMPerChatStore(QObject *parent) : QObject(parent)
, initFinished(false), m_isShowingMore(false), m_iHttp(0)
{

}

void IMPerChatStore::init()
{
	m_view = reinterpret_cast<IMPerChatView*>(parent());
	m_dispatcher = m_view->dispatcher();
	m_vm = m_view->vm();

	connect(m_dispatcher, SIGNAL(sigInsertTimeLine(qlonglong)), this, SLOT(slotInsertTimeLine(qlonglong)));
	connect(m_dispatcher, SIGNAL(sigProcessRecvMessageInfo(MessageInfo, BuddyInfo)), this, SLOT(slotProcessRecvMessageInfo(MessageInfo, BuddyInfo)));
	connect(m_dispatcher, SIGNAL(sigSetNickNameAndBuddyId(QString, QString)), this, SLOT(slotSetNickNameAndBuddyId(QString, QString)));
	connect(m_dispatcher, SIGNAL(sigShowByChatId(bool)), this, SLOT(slotShowByChatId(bool)));
	connect(m_dispatcher, SIGNAL(sigUpdateMessageStateInfo(QByteArray, int, int)), this, SLOT(slotUpdateMessageStateInfo(QByteArray, int, int)));
	connect(m_dispatcher, SIGNAL(sigProcessSendMessageInfo(QString, MessageInfo)), this, SLOT(slotProcessSendMessageInfo(QString, MessageInfo)));
	connect(m_dispatcher, SIGNAL(sigInsertTextEditPic(QString)), this, SLOT(slotInsertTextEditPic(QString)));
	connect(m_dispatcher, SIGNAL(sigSendTransmitMessage(MessageInfo)), this, SLOT(slotSendTransmitMessage(MessageInfo)));
	connect(m_dispatcher, SIGNAL(sigShareID(int, QString)), this, SLOT(slotShareID(int, QString)));
	connect(m_dispatcher, SIGNAL(sigRecvNotifyMessage(MessageInfo)), this, SLOT(slotRecvNotifyMessage(MessageInfo)));

	connect(m_dispatcher, SIGNAL(sigStartInputting()), this, SLOT(slotStartInputting()));
	connect(m_dispatcher, SIGNAL(sigEndInputting()), this, SLOT(slotEndInputting()));

	connect(m_dispatcher, SIGNAL(sigParseScreenCutMessage(QString)), this, SLOT(slotParseScreenCutMessage(QString)));

	connect(m_dispatcher, SIGNAL(sigUpdateHtmlBuddyHeaderImagePath(int, QString)), this, SLOT(slotUpdateHtmlBuddyHeaderImagePath(int, QString)));
	connect(m_dispatcher, SIGNAL(sigUpdateHtmlBuddyNickName(int, QString, int)), this, SLOT(slotUpdateHtmlBuddyNickName(int, QString, int)));
	connect(m_dispatcher, SIGNAL(sigGivePacket(QString)), this, SLOT(slotGivePacket(QString)));
	connect(m_dispatcher, SIGNAL(sigSendFile(QString)), this, SLOT(slotSendFile(QString)));
	connect(m_dispatcher, SIGNAL(sigSendSecretLetter(QString, QString, QString)), this, SLOT(slotSendSecretLetter(QString, QString, QString)));
	connect(m_dispatcher, SIGNAL(sigSendSecretImage(QString)), this, SLOT(slotSendSecretImage(QString)));
	connect(m_dispatcher, SIGNAL(sigSendSecretFile(QString)), this, SLOT(slotSendSecretFile(QString)));

	connect(m_dispatcher, SIGNAL(sigZoomImg(QString)), this, SLOT(slotZoomImg(QString)));
	connect(m_dispatcher, SIGNAL(sigGetFile(QString)), this, SLOT(slotGetFile(QString)));
	connect(m_dispatcher, SIGNAL(sigSaveFile(QString)), this, SLOT(slotSaveFile(QString)));
	connect(m_dispatcher, SIGNAL(sigDrags(QStringList)), this, SLOT(slotDrags(QStringList)));
	connect(m_dispatcher, SIGNAL(sigLoadMore()), this, SLOT(slotLoadMore()));
	connect(m_dispatcher, SIGNAL(sigWebEngineFinish(bool)), this, SLOT(slotWebEngineFinish(bool)));
	connect(m_dispatcher, SIGNAL(sigSendNotice(QMap<QString, QString>)), this, SLOT(slotSendNotice(QMap<QString, QString>)));

}

QString IMPerChatStore::objectName() const
{
	return m_vm->getBuddyId();
}

bool IMPerChatStore::isFriend(QString strBuddyId)
{
	return gDataBaseOpera->DBJudgeFriendIsHaveByID(strBuddyId);
}

bool IMPerChatStore::isSpecialBuddy(QString strBuddyId)
{
	if (gDataBaseOpera->DBJudgeBuddyIsHaveByID(strBuddyId))
	{
		BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(strBuddyId);

		if (buddy.nUserType != 0)
		{
			return true;
		}
	}

	return false;
}

void IMPerChatStore::slotInsertTimeLine(qlonglong time_t)
{
	QString strSend = QString("addTime(\"%1\");").arg(time_t);
	m_vm->runJs(strSend);
}

void IMPerChatStore::slotProcessRecvMessageInfo(MessageInfo messageInfo, BuddyInfo buddyInfo)
{
	switch (messageInfo.MessageChildType)
	{
	case MessageType::Message_TEXT: // 文字消息
		this->OnRecvMessageTextInfo(messageInfo.strMsg, buddyInfo.strLocalAvatar, QString(messageInfo.msgID), messageInfo.integral, QString::number(messageInfo.nFromUserID));
		break;
	case MessageType::Message_PIC: // 图片消息
		this->OnRecvImgMessage(messageInfo, buddyInfo.strLocalAvatar);
		break;
	case MessageType::Message_AUDIO:  //音频消息。
		this->OnRecvAudioMessage(messageInfo, buddyInfo.strLocalAvatar);
		break;
	case MessageType::Message_VEDIO: //视频消息
		this->OnRecvVideoMessage(messageInfo, buddyInfo.strLocalAvatar);
		break;
	case MessageType::Message_FILE: //文件消息
		if (gDataBaseOpera)
		{
			QString strLocalFilePath = gDataBaseOpera->DBGetFileInfoLocalPath(messageInfo.msgID);
			this->OnRecvFileMessage(messageInfo, buddyInfo.strLocalAvatar, strLocalFilePath);
		}
		break;
	case MessageType::Message_TRANSFER: //转账消息。
		this->OnRecvTransferMessage(messageInfo, buddyInfo.strLocalAvatar);
		break;
	case MessageType::Message_REDBAG:
		this->OnRecvRedBagMessage(messageInfo, buddyInfo.strLocalAvatar);
		break;
	case MessageType::Message_SECRETLETTER:  //密文消息。
		this->OnRecvSecretLetter(messageInfo, buddyInfo.strLocalAvatar);
		break;
	case MessageType::Message_SECRETIMAGE: //密图消息。
		this->OnRecvSecretImage(messageInfo, buddyInfo.strLocalAvatar);
		break;
	case MessageType::Message_SECRETFILE: //密件消息。
		this->OnRecvSecretFile(messageInfo, buddyInfo.strLocalAvatar);
		break;
	case MessageType::Message_NOTICE:  //通告消息。
		this->OnRecvNoticeMessage(messageInfo, buddyInfo.strLocalAvatar);
		break;
	case MessageType::Message_URL:  //分享消息
		this->OnRecvShareUrlMessage(messageInfo, buddyInfo.strLocalAvatar);
		break;
	case MessageType::Message_LOCATION:  //定位消息。
		this->OnRecvLocationMessage(messageInfo, buddyInfo.strLocalAvatar);
		break;
	case MessageType::Message_COMMON:      //任务消息
		this->OnRecvCommonMessage(messageInfo, buddyInfo.strLocalAvatar);
		break;
	case MessageType::Message_GW:
		this->OnRecvGWMessage(messageInfo, buddyInfo.strLocalAvatar);
		break;
	case MessageType::Message_NOTIFY:
		this->OnRecvNotifyMessage(messageInfo);
	default:
		break;
	}
}

void IMPerChatStore::slotSetNickNameAndBuddyId(QString strText, QString strBuddyID)
{
	m_vm->setNickNameAndBuddyId(strText, strBuddyID);
}

QString IMPerChatStore::getDraft()
{
	return m_vm->getDraft();
}

bool IMPerChatStore::IsValidImage(QString imgPath)
{
	QFile file(imgPath);
	if (file.open(QIODevice::ReadOnly))
	{
		QByteArray byteArray = file.readAll();
		file.close();
		QImage image = QImage::fromData(byteArray);

		if (image.isNull())
		{
			return false;
		}
	}
	else
	{
		return false;
	}

	return true;
}

QString IMPerChatStore::convertHeaderToHtmlSrc(QString headerImage)
{
	if (IsValidImage(headerImage))
	{
		if (headerImage.startsWith("file:///"))
		{
		}
		else
		{
			headerImage = "file:///" + headerImage;
		}
		headerImage.replace(" ", "%20");
	}
	else
	{
		headerImage = "qrc:/html/Resources/html/temp.png";
	}

	return headerImage;
}

//解析内容里的url,转成<a href='http://www.xxx.com'>http://www.xxx.com</a>,其它内容toHtmlEscaped()
QString IMPerChatStore::recognizeUrl(const QString& strMessage)
{
	QString strRet;

	if (strMessage.isEmpty())
		return strMessage;

	QString str_src(strMessage);

	// 从 strMessage 中过滤 url形式的正则表达式
	QRegExp re_url_val("((ht|f)tp(s?)\\:\\/\\/|[0-9a-zA-Z]+\\.)[0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*(:(0-9)*)*(\\/?)([a-zA-Z0-9\\-\\.\\?\\,\\'\\/\\\\\\+&amp;%\\$#=!_\\*]*)?");

	int nFlags = 0;
	while (re_url_val.indexIn(str_src) != -1)
	{
		QString cap_str = re_url_val.cap(0);
		int cur_idx = re_url_val.indexIn(str_src);


		QString leftStr = str_src.left(cur_idx);


		strRet += leftStr.toHtmlEscaped();

		str_src.remove(0, cur_idx + cap_str.length());    // 删除已经遍历过的字符

														  // 格式化为 <a />标签形式
		QString cap_str_link;
		if (cap_str.startsWith("http://")
			|| cap_str.startsWith("https://")
			|| cap_str.startsWith("ftp://")
			|| cap_str.startsWith("ftps://")
			)
		{
			cap_str_link = cap_str;
		}
		else
		{
			cap_str_link = QString("http://") + cap_str;
		}

		strRet += QString("<a href=javascript:window.bridge.slotOpenUrl(&quot;%1&quot;) >%2</a>").arg(QString(cap_str_link).toUtf8().toPercentEncoding(), cap_str.toHtmlEscaped());

		nFlags = 1;
	}
	if (nFlags == 0)
	{
		strRet = strMessage.toHtmlEscaped();

		return strRet;
	}

	strRet += str_src.toHtmlEscaped();

	return strRet;
}

// 将 包含[xx]表情形式的信息字符串 转换为 表情的路径形式并且HTML编码
QString IMPerChatStore::formatMessageFromImgDescriptionWithHtmlEncode(const QString& strMessage)
{
	QString strRet;

	if (strMessage.isEmpty())
		return strMessage;

	QString str_src(strMessage);

	QRegExp re_img_val("\\[[^\\[^\\]]*\\]");      // 从 strMessage 中过滤 [xxx] 形式的正则表达式

	int nFlags = 0;
	while (re_img_val.indexIn(str_src) != -1)
	{
		QString img_descrip = re_img_val.cap(0);
		int cur_idx = re_img_val.indexIn(str_src);

		strRet += recognizeUrl(str_src.left(cur_idx));

		str_src.remove(0, cur_idx + img_descrip.length());    // 删除已经遍历过的字符

		QString img_path = ExpressWidget::GetImagePathByDescription(img_descrip);
		if (!img_path.isEmpty())
		{
			// 格式化为 <img />标签形式
			strRet += "<img src='qrc:/expression/Resources" + img_path + "'/>";
		}
		else
		{
			// 如果没有找到则保持原字符串不变
			strRet += recognizeUrl(img_descrip);
		}
		nFlags = 1;
	}
	if (nFlags == 0)
	{
		strRet = recognizeUrl(strMessage);

		return strRet;
	}

	strRet += recognizeUrl(str_src);

	return strRet;
}


//接收消息添加到webview
void IMPerChatStore::OnRecvMessageTextInfo(QString strMsg, QString strHeadImage, QString strMsgID, int score, QString strBuddyId)
{
	QString strPath = convertHeaderToHtmlSrc(strHeadImage);
	QString strMessage = formatMessageFromImgDescriptionWithHtmlEncode(strMsg);

	strMessage = strMessage.toUtf8().toPercentEncoding();

	QString strSend = QString("RecAppend(\"%1\",\"%2\",\"%3\",%4,\"%5\");").arg(strPath, strMessage, strMsgID, QString::number(score), strBuddyId);
	m_vm->runJs(strSend);
}

QString IMPerChatStore::GetFileMd5(QString fileNamePath)
{
	QFile theFile(fileNamePath);
	theFile.open(QIODevice::ReadOnly);
	QByteArray ba = QCryptographicHash::hash(theFile.readAll(), QCryptographicHash::Md5);
	theFile.close();
	return QString(ba.toHex());
}

const QString IMPerChatStore::GetSmallImg(QString strPath)
{
	//后缀可能是非法后缀，安卓拍照时传的是.pic后缀名
	strPath.replace("%20", " ");
	QString strSmallPath = "";
	QImageReader reader;
	reader.setDecideFormatFromContent(true);
	reader.setAutoTransform(true);
	reader.setFileName(QString(strPath).replace("file:///", ""));
	if (!reader.canRead())
	{
		return strPath;
	}

	QString strType = strPath.right(strPath.length() - strPath.lastIndexOf('.')).toLower();

	if (strType == ".gif")
	{
		//gif动态图不做转换
		return strPath;
	}

	QImage img = reader.read();

	//图片超过300*300时或者有EXIF旋转信息时则生成缩略图并返回缩略图地址，其它情况返回原图地址
	if ((strType != ".jpg" && strType != ".jpeg" && strType != ".png" && strType != ".gif" && strType != ".bmp") || (img.width() > 300 || img.height() > 300) || reader.transformation() != QImageIOHandler::TransformationNone)
	{
		img = img.scaled(300, 300, Qt::AspectRatioMode::KeepAspectRatio, Qt::TransformationMode::SmoothTransformation);

		strSmallPath = QString("file:///") + QDir::tempPath() + "/" + GetFileMd5(QString(strPath).replace("file:///", "")) + ".small" + strType;
		QString strFixSmallPath = QString(strSmallPath).replace("file:///", "");

		qDebug() << "small pic ===============>>" << strFixSmallPath;

		if (!QFile(strFixSmallPath).exists())
		{
			img.save(strFixSmallPath);

			//若是生成不成功，后缀可能不是标准图片格式，再生成一次
			if (!QFile(strFixSmallPath).exists())
			{
				img.save(strFixSmallPath, "PNG");
			}
		}
	}
	else
	{
		return strPath;
	}


	return strSmallPath;
}

void IMPerChatStore::InsertRecMessagePictureInfo(QString strIsUserDefine, QString strUserDefinePicPath, QString strPicPath, QString strHeadImage, QString strMsgID, int score, QString strBuddyId)
{
	QString strPath = convertHeaderToHtmlSrc(strHeadImage);
	if (!strPicPath.isEmpty())
	{
		strPicPath = GetSmallImg(strPicPath);
	}

	QString strSend = QString("RecPictureAppend(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",%6,\"%7\");").arg(strPath).arg(strIsUserDefine).arg(strUserDefinePicPath).arg(strPicPath).arg(strMsgID).arg(score).arg(strBuddyId);
	m_vm->runJs(strSend);
}

void IMPerChatStore::OnRecvImgMessage(MessageInfo messageInfo, QString strHeadImage)
{
	QString strMsg = "";

	QString msgID = QString(messageInfo.msgID);
	if (messageInfo.strMsg == "load")
	{
		strMsg = "qrc:/html/Resources/html/load.gif";
		QString strIsUserDefine = "true";
		QString strUserDefinePicPath = strMsg;
		QString strPicPath = "";
		InsertRecMessagePictureInfo(strIsUserDefine, strUserDefinePicPath, strPicPath, strHeadImage, msgID, messageInfo.integral, QString::number(messageInfo.nFromUserID));
	}
	else if (messageInfo.strMsg != "fail" && messageInfo.MessageState != MESSAGE_STATE_UNLOADED)
	{
		messageInfo.strMsg.replace(" ", "%20");

		if (!QFile::exists(messageInfo.strMsg))
		{
#ifdef Q_OS_WIN
			messageInfo.strMsg = gSettingsManager->getUserPath() + messageInfo.strMsg;
#else
			messageInfo.strMsg = messageInfo.strMsg.replace("%20", " ");
#endif
		}
		QString strIsUserDefine = "false";
		QString strUserDefinePicPath = "";
		QString strPicPath = QString("file:///") + messageInfo.strMsg;
		InsertRecMessagePictureInfo(strIsUserDefine, strUserDefinePicPath, strPicPath, strHeadImage, msgID, messageInfo.integral, QString::number(messageInfo.nFromUserID));
		m_listPic.push_back(messageInfo.strMsg);
	}
	else
	{
		strMsg = "qrc:/html/Resources/html/picfail.png";
		QString strIsUserDefine = "true";
		QString strUserDefinePicPath = strMsg;
		QString strPicPath = "";
		InsertRecMessagePictureInfo(strIsUserDefine, strUserDefinePicPath, strPicPath, strHeadImage, msgID, messageInfo.integral, QString::number(messageInfo.nFromUserID));
	}
}


int IMPerChatStore::CalWavLength(QString strPath)
{
	int iSize = 0;
	QFile file(strPath);
	file.open(QIODevice::ReadOnly);
	QDataStream in(&file);
	in.setByteOrder(QDataStream::LittleEndian);
	unsigned int iRate = 0;
	in.skipRawData(12);
	char ch[5];
	in.readRawData(ch, 4);
	ch[4] = '\0';
	QString strType(ch);

	if (strType == "fmt ")
	{
		in.skipRawData(12);
		in >> iRate;
	}
	else if (strType == "JUNK")
	{
		unsigned int iJunk;
		unsigned short sNum;
		in >> iJunk;
		in.skipRawData(iJunk);
		in.readRawData(ch, 4);
		in >> iRate;
		in >> sNum;
		in >> sNum;
		in >> iRate;
		in >> iRate;
	}
	else
	{
		iRate = BYTES_PER_SECOND;
	}

	iSize = file.size() / iRate;
	return iSize;
}

void IMPerChatStore::OnRecvAudioMessage(MessageInfo messageInfo, QString strHeadImage)
{
	QString msgID = QString(messageInfo.msgID);
	QString audioPath = messageInfo.strMsg;
	if (messageInfo.MessageState == MESSAGE_STATE_UNLOADED)
	{
		QString strMsg = "";
		strMsg = "qrc:/html/Resources/html/audiofail.png";
		QString strIsUserDefine = "true";
		QString strUserDefinePicPath = strMsg;
		QString strPicPath = "";
		InsertRecMessagePictureInfo(strIsUserDefine, strUserDefinePicPath, strPicPath, strHeadImage, msgID, messageInfo.integral, QString::number(messageInfo.nFromUserID));
		return;
	}
	if (audioPath.endsWith(".amr"))
	{
		QString wavPath = audioPath;
		wavPath.replace(".amr", ".wav");

		QFile audioFile(wavPath);
		if (!audioFile.exists())
		{
#ifdef Q_OS_WIN
			AmrDec amrDec;
			amrDec.convertAmrToWav(audioPath, wavPath);
#else
			QString appPath = QDir::currentPath() + "/ffmpeg";
			QStringList arguments;
			arguments << "-i" << audioPath << wavPath;
			QProcess process(this);
			process.start(appPath, arguments);
			process.waitForFinished();
			process.close();
#endif
		}

		audioPath = wavPath;
	}


	int duration = CalWavLength(audioPath); //file.size() / BYTES_PER_SECOND;
	QString strPath = convertHeaderToHtmlSrc(strHeadImage);

	QString strSend = QString("RecAudioAppend(\"%1\",\"%2\", %3, \"%4\",%5, \"%6\");").arg(strPath).arg(audioPath).arg(duration).arg(msgID).arg(messageInfo.integral).arg(QString::number(messageInfo.nFromUserID));
	m_vm->runJs(strSend);
}


void IMPerChatStore::InsertRecVideoInfo(QString strIsLoading, QString strVideoPicPath, QString strVideoPath, QString strHeadImage, QString strMsgID, int score, QString strBuddyId)
{
	QString strPath = convertHeaderToHtmlSrc(strHeadImage);

	QString strSend = QString("VdoAppend(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",%6,\"%7\");").arg(strPath).arg(strIsLoading).arg(strVideoPicPath).arg(strVideoPath).arg(strMsgID).arg(score).arg(strBuddyId);
	m_vm->runJs(strSend);
}

void IMPerChatStore::OnRecvVideoMessage(MessageInfo messageInfo, QString strHeadImage)
{
	QString strMsg = "";
	QString msgID = QString(messageInfo.msgID);

	if (messageInfo.strMsg == "load")
	{
		strMsg = "qrc:/html/Resources/html/load.gif";
		QString strIsUserDefine = "true";
		QString strUserDefinePicPath = strMsg;
		QString strPicPath = "";
		InsertRecMessagePictureInfo(strIsUserDefine, strUserDefinePicPath, strPicPath, strHeadImage, msgID, messageInfo.integral, QString::number(messageInfo.nFromUserID));
	}
	else if (messageInfo.strMsg != "fail" && messageInfo.MessageState != MESSAGE_STATE_UNLOADED)
	{
#ifdef Q_OS_WIN
		messageInfo.strMsg.replace(" ", "%20");
#endif
		QString strPicPath = messageInfo.strMsg.left(messageInfo.strMsg.indexOf("."));
		strPicPath += ".png";
		VedioFrameOpera pVdo;
		pVdo.CreateVedioPicture(messageInfo.strMsg, strPicPath);
		QString strPhotoImgPath = QString("<img height:auto width:auto src='file:///%1' name = '%2' onload='movePlayIcon(this);loadpic()'/> ").arg(strPicPath).arg(messageInfo.strMsg);

		QString strIsLoading = "false";//是否是正加载
		QString strVideoPicPath = "file:///" + strPicPath;//视频第一帧图
		QString strVideoPath = messageInfo.strMsg;//视频路径

		InsertRecVideoInfo(strIsLoading, strVideoPicPath, strVideoPath, strHeadImage, msgID, messageInfo.integral, QString::number(messageInfo.nFromUserID));
	}
	else
	{
		QString strMsg = "";
		strMsg = ":/html/Resources/html/videofail.png";
		QString strIsUserDefine = "true";
		QString strUserDefinePicPath = strMsg;
		QString strPicPath = "";
		InsertRecMessagePictureInfo(strIsUserDefine, strUserDefinePicPath, strPicPath, strHeadImage, msgID, messageInfo.integral, QString::number(messageInfo.nFromUserID));
		return;
	}

}


void IMPerChatStore::ChangeFileState(QString strMsgId, int iState)
{
	QString strScript = QString("ChangeFileState(\"%1\",\"%2\")").arg(strMsgId).arg(iState);
	m_vm->runJs(strScript);
}

void IMPerChatStore::OnRecvFileMessage(MessageInfo messageInfo, QString strHeadImage, QString fileLocalPath)
{
	QJsonDocument doc = QJsonDocument::fromJson(messageInfo.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();

	QString strFileName = map["FileName"].toString();
	QString  fileSize = map["FileSize"].toString();

	FileTypeEx* fileType = new FileTypeEx(this);
	QString strPath = fileType->GetFilePic(strFileName);
	fileType->deleteLater();
	fileType = NULL;

	QString headerPath = convertHeaderToHtmlSrc(strHeadImage);

	QString strUlID = QString(messageInfo.msgID) + QString("recv");
	QString strUlID2 = QString(messageInfo.msgID) + QString("recv2");
	QString strSize = QString("(") + fileSize + QString(")");
	strPath.replace(" ", "%20");
	QString strRecFile = QString("RecFile(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",\"%6\",\"%7\",%8,\"%9\")").arg(headerPath).arg(strFileName).arg(strPath).arg(QString(messageInfo.msgID)).arg(strUlID).arg(strUlID2).arg(strSize).arg(messageInfo.integral).arg(QString::number(messageInfo.nFromUserID));
	m_vm->runJs(strRecFile);

	if (fileLocalPath != "")
	{
		QFile localFile(fileLocalPath);
		if (localFile.exists())
		{
			ChangeFileState(messageInfo.msgID, 2);
		}
	}
}


void IMPerChatStore::OnRecvTransferMessage(MessageInfo msgInfo, QString headerImage)
{
	QString strPath = convertHeaderToHtmlSrc(headerImage);

	UserInfo userInfo = gDataManager->getUserInfo();
	QString userID = QString::number(userInfo.nUserID);
	QJsonDocument json = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = json.toVariant().toMap();
	QString transfer;
	if (userID == map.value("sendIMUserId").toString())
	{
		//用户是发送者。
		transfer = "sender";
	}
	else
	{
		transfer = "receiver";
	}

	QString strSend = QString("RecTransfer(\"%1\",\"%2\",\"%3\",%4,\"%5\");")
		.arg(strPath).arg(transfer).arg(QString(msgInfo.msgID)).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));
	m_vm->runJs(strSend);
}

void IMPerChatStore::OnRecvRedBagMessage(MessageInfo messageInfo, QString strHeadImage)
{
	QJsonDocument json = QJsonDocument::fromJson(messageInfo.strMsg.toUtf8());
	QVariantMap map = json.toVariant().toMap();
	QString invalid = map.value("invalid").toString();
	QString remarks;
	if (invalid.isEmpty())
	{
		remarks = map.value("leaveMessage").toString();
	}
	else
	{
		QVariantMap packet = map.value("redPacket").toMap();
		remarks = packet.value("remarks").toString();
	}

	if (remarks.isEmpty())
		remarks = tr("Best Wishes");

	QString headerPath = convertHeaderToHtmlSrc(strHeadImage);
	QString strSend = QString("RecRedPacketAppend('%1', '%2', '%3', '%4', %5, '%6');")
		.arg(headerPath).arg(remarks).arg(invalid).arg(QString(messageInfo.msgID)).arg(messageInfo.integral).arg(QString::number(messageInfo.nFromUserID));
	m_vm->runJs(strSend);
}

void IMPerChatStore::OnRecvSecretLetter(MessageInfo msgInfo, QString headerImage)
{
	QString strPath = convertHeaderToHtmlSrc(headerImage);

	QString strSend = QString("RecSecretAppend(\"%1\",\"%2\",%3,\"%4\");")
		.arg(strPath).arg(QString(msgInfo.msgID)).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));

	m_vm->runJs(strSend);
}

void IMPerChatStore::OnRecvSecretImage(MessageInfo msgInfo, QString headerImage)
{
	QString strPath = convertHeaderToHtmlSrc(headerImage);

	QString strSend = QString("RecSecretImageAppend(\"%1\",\"%2\",%3,\"%4\");")
		.arg(strPath).arg(QString(msgInfo.msgID)).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));

	m_vm->runJs(strSend);
}

void IMPerChatStore::OnRecvSecretFile(MessageInfo msgInfo, QString headerImage)
{
	QString strPath = convertHeaderToHtmlSrc(headerImage);

	QString strSend = QString("RecSecretFileAppend(\"%1\",\"%2\",%3,\"%4\");")
		.arg(strPath).arg(QString(msgInfo.msgID)).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));

	m_vm->runJs(strSend);
}

void IMPerChatStore::OnRecvNoticeMessage(MessageInfo msgInfo, QString headerImage)
{
	QString strPath = convertHeaderToHtmlSrc(headerImage);

	QString msg = msgInfo.strMsg;
	QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();
	QString imageUrl = map.value("imageUrl").toString();
	QString imageTitle = map.value("imageTitle").toString();
	QString webUrl = map.value("webUrl").toString();
	QString webTitle = map.value("webTitle").toString();

	QString strSend = QString("RecNoticeAppend(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",\"%6\",%7,\"%8\");")
		.arg(strPath).arg(QString(msgInfo.msgID))
		.arg("").arg(imageTitle).arg(webUrl).arg(webTitle)
		.arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));

	m_vm->runJs(strSend);
	//图片方向
	QString strFilePath = QDir::tempPath() + "/" + QUuid::createUuid().toString() + ".jpg";
	HttpNetWork::HttpDownLoadFile *netWork = new HttpNetWork::HttpDownLoadFile;
	netWork->setData(QVariant::fromValue(msgInfo));
	netWork->setObjectName(strFilePath);

	connect(netWork, SIGNAL(sigDownFinished(bool)), this, SLOT(slotUpdateNotice(bool)));
	netWork->StartDownLoadFile(imageUrl, strFilePath);
}

void IMPerChatStore::slotUpdateNotice(bool)
{
	HttpNetWork::HttpDownLoadFile *act = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());
	if (!act)
	{
		return;
	}
	QVariant var = act->getData();
	MessageInfo msgInfo = var.value<MessageInfo>();
	QString filePath = GetSmallImg(act->objectName());

	QString strSend = QString("UpdateNoticeImg(\"%1\",\"%2\");")
		.arg(QString(msgInfo.msgID)).arg(filePath);

	m_vm->runJs(strSend);
}


void IMPerChatStore::OnRecvShareUrlMessage(MessageInfo msgInfo, QString headerImage)
{
	QString strPath = convertHeaderToHtmlSrc(headerImage);

	QString msg = msgInfo.strMsg;
	QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();
	QString subject = map.value("subject").toString();
	QString text = map.value("text").toString();
	QString url = map.value("url").toString();
	QString imageUrl = map.value("imgUrl").toString();

	QString strSend = QString("RecShareUrlAppend(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",\"%6\",%7,\"%8\");")
		.arg(subject).arg(text).arg(url).arg(imageUrl)
		.arg(strPath).arg(QString(msgInfo.msgID))
		.arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));

	m_vm->runJs(strSend);
}


void IMPerChatStore::OnRecvLocationMessage(MessageInfo msgInfo, QString headerImage)
{
	QString strPath = convertHeaderToHtmlSrc(headerImage);

	msgInfo.strMsg.remove("\n");
	msgInfo.strMsg.remove(QChar(32));
	msgInfo.strMsg = msgInfo.strMsg.replace("\"", "\\\"");

	headerImage.replace(" ", "%20");
	QString strSend = QString("RecLocationAppend(\"%1\",\"%2\",\"%3\",%4,\"%5\");")
		.arg(strPath).arg(QString(msgInfo.msgID))
		.arg(msgInfo.strMsg)
		.arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));

	m_vm->runJs(strSend);
}


void IMPerChatStore::OnRecvCommonMessage(MessageInfo messageInfo, QString strHeadImage)
{
	QJsonDocument doc = QJsonDocument::fromJson(messageInfo.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();

	QString image = map.value("image").toString();
	QString smallIcon = map.value("smallIcon").toString();
	QString title = map.value("title").toString();
	QString content = map.value("content").toString();
	QString systemName = map.value("systemName").toString();
	QString userType = map.value("userType").toString();

	if (image.isEmpty())
	{
		if (userType == "user")
			image = "https://tc.ipcom.io/panserver/files/c64b5061-77dc-45af-b91b-e5e06d401347/download";
		if (userType == "group")
			image = "https://tc.ipcom.io/panserver/files/0bb98df6-cddd-444e-a36c-c50beac4dcd1/download";
	}

	QString headerPath = convertHeaderToHtmlSrc(strHeadImage);

	QString strSend = QString("RecCommonAppend('%1', '%2', '%3', '%4', '%5','%6', '%7', %8, %9,'%10');")
		.arg(image).arg(smallIcon).arg(title).arg(content).arg(systemName)
		.arg(headerPath).arg(QString(messageInfo.msgID)).arg(messageInfo.MessageState).arg(messageInfo.integral).arg(QString::number(messageInfo.nFromUserID));
	m_vm->runJs(strSend);
}


void IMPerChatStore::OnRecvGWMessage(MessageInfo messageInfo, QString strHeadImage)
{
	QString strPath = convertHeaderToHtmlSrc(strHeadImage);
	QJsonDocument json = QJsonDocument::fromJson(messageInfo.strMsg.toUtf8());
	QVariantMap map = json.toVariant().toMap();
	QString strMessage = map.value("showMsg").toString();

	QString strSend = QString("RecAppend(\"%1\",\"%2\",\"%3\",%4,\"%5\");").arg(strPath).arg(strMessage).arg(QString(messageInfo.msgID)).arg(messageInfo.integral).arg(QString::number(messageInfo.nFromUserID));
	m_vm->runJs(strSend);
}


void IMPerChatStore::OnRecvNotifyMessage(MessageInfo messageInfo)
{
	QJsonDocument json = QJsonDocument::fromJson(messageInfo.strMsg.toUtf8());
	QVariantMap map = json.toVariant().toMap();

	QString content;
	if (map.value("type").toString() == "notification")
	{
		content = map.value("content").toString();
	}
	if (map.value("type").toString() == "receivedFile")
	{
		content = tr("The other party has successfully received your file") + QString("\\\"%1\\\"").arg(map.value("fileName").toString());
	}

	QString strSend = QString("tipMessage(\"%1\");").arg(content);
	m_vm->runJs(strSend);
}

void IMPerChatStore::slotShowByChatId(bool isNew)
{
	m_vm->showByChatId(isNew);
}

void IMPerChatStore::slotUpdateMessageStateInfo(QByteArray msgID, int nState, int score)
{
	QString strSend = QString("SetMsgState(\"%1\",%2,%3)").arg(QString(msgID)).arg(nState).arg(score);
	m_vm->runJs(strSend);

	BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(this->objectName());
	if (buddy.strNickName.isEmpty())
		buddy = gDataBaseOpera->DBGetGroupUserFromID(this->objectName());

	//更新至消息列表，false代表个人消息
	QMap<QString, QVariant> params;
	params["SendStatus"] = nState;
	params["MsgId"] = QString(msgID);
	emit m_dispatcher->sigUpdateSelfMessage(false, QVariant::fromValue(buddy), "", true, params);
}


void IMPerChatStore::slotGetFile(QString msgID)
{
	MessageInfo messageInfo = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);
	QJsonDocument jsonDocument = QJsonDocument::fromJson(messageInfo.strMsg.toUtf8());
	QVariantMap result = jsonDocument.toVariant().toMap();
	QString fileName = result["FileName"].toString();
	UserInfo userInfo = gDataManager->getUserInfo();
#ifdef Q_OS_WIN
	QDir dir;
	QString currentPath = gSettingsManager->getUserPath();
#else
	QString currentPath = gSettingsManager->getUserPath();
#endif
	QString strFilePath = currentPath + QString("/files/") + QString("%1").arg(userInfo.nUserID) + QString("/") + fileName;
	QFile recvFile(strFilePath);

	if (recvFile.exists())
	{
		QuestionBox *box = new QuestionBox(NULL);

		connect(box, SIGNAL(sigEnter()), this, SLOT(slotCoverFile()));
		box->setData(QVariant::fromValue(messageInfo));
		box->init(tr("Notice"), tr("This file already exists, overwrite or not?"));
		box->show();
	}
	else
	{
		QString fileID = result["FileId"].toString();

		QString strUlID = QString(messageInfo.msgID) + QString("recv");
		QString coverFile = QString("coverFile(\"%1\")").arg(strUlID);
		QString strUlID2 = QString(messageInfo.msgID) + QString("recv2");

		m_vm->runJs(coverFile);

		AppConfig configInfo = gDataManager->getAppConfigInfo();
		QString httpUrl = QString(configInfo.PanServerDownloadURL) + fileID + QString("/download");
		HttpNetWork::HttpDownLoadFile *netWork = new HttpNetWork::HttpDownLoadFile;
		m_iHttp++;
		netWork->setData(QVariant::fromValue(messageInfo));
		netWork->setObjectName(strFilePath);

		connect(netWork, SIGNAL(sigDownFinished(bool)), this, SLOT(slotRequestHttpFileResult(bool)));
		connect(netWork, SIGNAL(sigDownFailed()), this, SLOT(slotDownFailed()));
		connect(netWork, SIGNAL(sigDownloadProgress(qint64, qint64)), this, SLOT(slotDownLoadFileProgress(qint64, qint64)));
		connect(m_dispatcher, SIGNAL(sigCancel(QString)), netWork, SLOT(slotLoadorDownLoadCancle(QString)));
		netWork->StartDownLoadFile(httpUrl, strFilePath);
	}
}

void IMPerChatStore::slotDownLoadFileProgress(qint64 recvnum, qint64 total)
{
	HttpNetWork::HttpDownLoadFile *act = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());
	if (act)
	{
		QVariant var = act->getData();
		MessageInfo msgInfo = var.value<MessageInfo>();
		if (total > 0)
		{
			int num = recvnum * 250 / total;
			QString barId = msgInfo.msgID + "bar";
			QString strSend = QString("ProgressBar(\"%1\",%2)").arg(barId).arg(num);
			m_vm->runJs(strSend);
		}
		else {
			QString barId = msgInfo.msgID + "bar";
			QString strSend = QString("ProgressBar(\"%1\",%2)").arg(barId).arg(0);
			m_vm->runJs(strSend);
		}
	}
}


void IMPerChatStore::slotDownFailed()
{
	m_iHttp--;
	HttpNetWork::HttpDownLoadFile *act = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());
	if (act)
	{
		QVariant var = act->getData();
		MessageInfo msgInfo = var.value<MessageInfo>();
		ChangeFileState(msgInfo.msgID, 3);
	}
}

void IMPerChatStore::slotRequestHttpFileResult(bool result)
{
	m_iHttp--;
	bool bNeedSend = true;
	HttpNetWork::HttpDownLoadFile *act = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());
	if (act)
	{
		QVariant var = act->getData();
		MessageInfo msgInfo = var.value<MessageInfo>();
		QString filePath = act->objectName();
		if (result)
		{
			QString strHasFile = gDataBaseOpera->DBGetFileInfoLocalPath(QString(msgInfo.msgID));
			if (!strHasFile.isEmpty())
			{
				bNeedSend = false;
			}
			gDataBaseOpera->DBOnInsertFileInfo(QString(msgInfo.msgID), filePath, "");//将接收到的文件信息插入数据库

			QString strUlID = msgInfo.msgID + QString("recv");
			QString strChange = QString("ChangeLiTwo(\"%1\")").arg(strUlID);

			QString strUlID2 = msgInfo.msgID + QString("recv2");
			QString strChange2 = QString("ChangeLiTwo(\"%1\")").arg(strUlID2);

			ChangeFileState(msgInfo.msgID, 2);
		}
		else {
			QString strUlID = msgInfo.msgID + QString("recv");
			QString strChange = QString("ChangeLiThr(\"%1\")").arg(strUlID);

			QString strUlID2 = msgInfo.msgID + QString("recv2");
			QString strChange2 = QString("ChangeLiThr(\"%1\")").arg(strUlID2);

			ChangeFileState(msgInfo.msgID, 0);
		}
		UserInfo userInfo = gDataManager->getUserInfo();
		if (gSocketMessage && result && msgInfo.nFromUserID != userInfo.nUserID)
		{
			int nUserID = userInfo.nUserID;
			if (m_vm->getBuddyId().isEmpty())
			{
				return;
			}

			QString fileName = QFileInfo(filePath).fileName();
			QVariantMap map;
			map.insert("type", "receivedFile");
			map.insert("fileName", fileName);
			QString strMessage = QJsonDocument::fromVariant(map).toJson();
			QString toUserID = m_vm->getBuddyId();
			MessageInfo msgInfo = gSocketMessage->SendTextMessage(nUserID, toUserID.toInt(), 0, (short)MessageType::Message_NOTIFY, strMessage);

			QString strSend = QString("tipMessage(\"%1\");").arg(tr("Receive file successfully!") + QString("\\\"%1\\\"").arg(fileName));
			m_vm->runJs(strSend);

			MessageListInfo messageListInfo;
			messageListInfo.nBudyyID = this->objectName().toInt();
			messageListInfo.strLastMessage = tr("Receive file successfully!") + fileName;
			messageListInfo.nLastTime = msgInfo.ClientTime;
			messageListInfo.strBuddyName = m_vm->getNickName();
			BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(this->objectName());
			if (buddy.strNickName.isEmpty())
				buddy = gDataBaseOpera->DBGetGroupUserFromID(this->objectName());
			messageListInfo.strBuddyHeaderImage = buddy.strLocalAvatar;
			messageListInfo.messageType = msgInfo.MessageChildType;
			messageListInfo.nUnReadNum = 0;
			messageListInfo.isGroup = 0;
			gDataBaseOpera->DBInsertPerMessageListInfo(messageListInfo);
			//更新至消息列表，false代表个人消息
			QMap<QString, QVariant> params;
			emit m_dispatcher->sigUpdateSelfMessage(false, QVariant::fromValue(buddy), tr("Receive file successfully!") + fileName, false, params);
		}
	}
}

void IMPerChatStore::slotCoverFile()
{
	QuestionBox *box = qobject_cast<QuestionBox *>(sender());
	QVariant msgVar = box->getData();
	MessageInfo messageInfo = msgVar.value<MessageInfo>();
	QJsonDocument jsonDocument = QJsonDocument::fromJson(messageInfo.strMsg.toUtf8());
	QVariantMap result = jsonDocument.toVariant().toMap();

	QString fileID = result["FileId"].toString();

	QString strUlID2 = QString(messageInfo.msgID) + QString("recv2");

	QString strUlID = QString(messageInfo.msgID) + QString("recv");
	QString coverFile = QString("coverFile(\"%1\")").arg(strUlID);

	AppConfig configInfo = gDataManager->getAppConfigInfo();

	QString httpUrl = QString(configInfo.PanServerDownloadURL) + fileID + QString("/download");

	QString fileName = result["FileName"].toString();
	UserInfo userInfo = gDataManager->getUserInfo();

	QDir dir;
#ifdef Q_OS_WIN
	QString currentPath = gSettingsManager->getUserPath();
#else
	QString currentPath = gSettingsManager->getUserPath();
#endif

	QString strFilePath = currentPath + QString("/files/") + QString("%1").arg(userInfo.nUserID) + QString("/") + fileName;
	QFile recvFile(strFilePath);

	//尝试删除文件 若文件已打开或被占用 弹窗提示
	if (!recvFile.remove())
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("File is in use, please check it first!"));
		return;
	}
	

	m_vm->runJs(coverFile);

	HttpNetWork::HttpDownLoadFile *netWork = new HttpNetWork::HttpDownLoadFile;
	m_iHttp++;
	netWork->setData(QVariant::fromValue(messageInfo));
	netWork->setObjectName(strFilePath);
	connect(netWork, SIGNAL(sigDownFinished(bool)), this, SLOT(slotRequestHttpFileResult(bool)));
	connect(netWork, SIGNAL(sigDownFailed()), this, SLOT(slotDownFailed()));
	connect(netWork, SIGNAL(sigDownloadProgress(qint64, qint64)), this, SLOT(slotDownLoadFileProgress(qint64, qint64)));
	connect(m_dispatcher, SIGNAL(sigCancel(QString)), netWork, SLOT(slotLoadorDownLoadCancle(QString)));
	netWork->StartDownLoadFile(httpUrl, strFilePath);
}


void IMPerChatStore::slotProcessSendMessageInfo(QString strHeadImage, MessageInfo msgInfo)
{
	//插入时间线。
	QList<int> types;
	types << MessageType::Message_TEXT << MessageType::Message_PIC << MessageType::Message_FILE;
	types << MessageType::Message_SECRETLETTER << MessageType::Message_SECRETIMAGE << MessageType::Message_SECRETFILE;
	types << MessageType::Message_NOTICE << MessageType::Message_URL << MessageType::Message_LOCATION;
	types << MessageType::Message_AUDIO << MessageType::Message_VEDIO;
	types << MessageType::Message_NOTIFY << MessageType::Message_COMMON;

	if (types.contains((int)msgInfo.MessageChildType))
	{
		//插入时间线。
		if (msgInfo.ServerTime)
			slotInsertTimeLine(msgInfo.ServerTime);
		else
			slotInsertTimeLine(msgInfo.ClientTime);
	}

	QString strPath = convertHeaderToHtmlSrc(strHeadImage);

	switch (msgInfo.MessageChildType)
	{
	case MessageType::Message_TEXT:
		OnSendMessageTextInfo(strPath, msgInfo);
		break;
	case MessageType::Message_PIC:
		OnSendImgMessage(strPath, msgInfo);
		break;
	case MessageType::Message_FILE:
		OnSendFileMessage(strPath, msgInfo);
		break;
	case MessageType::Message_REDBAG:
		OnSendRedBagMessage(strPath, msgInfo);
		break;
	case MessageType::Message_SECRETLETTER:
		OnSendSecretLetter(strPath, msgInfo);
		break;
	case MessageType::Message_SECRETIMAGE:
		OnSendSecretImage(strPath, msgInfo);
		break;
	case MessageType::Message_SECRETFILE:
		OnSendSecretFile(strPath, msgInfo);
		break;
	case MessageType::Message_NOTICE:
		OnSendNoticeMessage(strPath, msgInfo);
		break;
	case MessageType::Message_URL:
		OnSendShareUrlMessage(strPath, msgInfo);
		break;
	case MessageType::Message_LOCATION:
		OnSendLocationMessage(strPath, msgInfo);
		break;
	case MessageType::Message_COMMON:
		OnSendCommonMessage(strPath, msgInfo);
		break;
	case MessageType::Message_AUDIO:
		OnSendAudioMessage(strPath, msgInfo);
		break;
	case MessageType::Message_VEDIO:
		OnSendVideoMessage(strPath, msgInfo);
		break;
	case MessageType::Message_NOTIFY:
		OnSendNotifyMessage(strPath, msgInfo);
		break;
	case MessageType::Message_TRANSFER:
		OnSendTransferMessage(strPath, msgInfo);
		break;
	default:
		break;
	}

}



void IMPerChatStore::OnSendMessageTextInfo(QString strPath, MessageInfo msgInfo)
{
	if (msgInfo.MessageState == MESSAGE_STATE_SENDING)
	{
		//重新插入消息时正在发送改显示为发送失败
		msgInfo.MessageState = MESSAGE_STATE_FAILURE;
		gSocketMessage->DBUpdateMessageStateInfo(msgInfo.msgID, MESSAGE_STATE_FAILURE, msgInfo.integral);
	}
	QString strMessage = formatMessageFromImgDescriptionWithHtmlEncode(msgInfo.strMsg);
	strMessage = strMessage.toUtf8().toPercentEncoding();

	QString strSend = QString("SendAppend(\"%1\",\"%2\",\"%3\",%4,%5,\"%6\");").arg(strPath, strMessage, QString(msgInfo.msgID), QString::number(msgInfo.MessageState), QString::number(msgInfo.integral), QString::number(msgInfo.nFromUserID));
	m_vm->runJs(strSend);
}

void IMPerChatStore::OnSendImgMessage(QString strPath, MessageInfo msgInfo)
{
	QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();
	QString path = map.value("path").toString();
	if (!path.isEmpty())
		msgInfo.strMsg = path;

	if (!QFile::exists(msgInfo.strMsg))
	{
		msgInfo.strMsg = gSettingsManager->getUserPath() + msgInfo.strMsg;
	}

	QFile image(msgInfo.strMsg);
	if (!image.exists())
		return;

	QString filePathEscape = msgInfo.strMsg;
	filePathEscape.replace(" ", "%20");
	filePathEscape.replace("#", "%23");

	QString strIsUserDefine = "false";
	QString strUserDefinePicPath = "";
	QString strPicPath = QString("file:///") + filePathEscape;

	if (!strPicPath.isEmpty())
	{
		strPicPath = GetSmallImg(strPicPath);
	}
	QString strSend = QString("SendPicture(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",%6,%7,\"%8\");").arg(strPath).arg(strIsUserDefine).arg(strUserDefinePicPath).arg(strPicPath).arg(QString(msgInfo.msgID)).arg(msgInfo.MessageState).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));
	m_vm->runJs(strSend);
	m_listPic.push_back(msgInfo.strMsg);
}

void IMPerChatStore::OnSendFileMessage(QString strPath, MessageInfo msgInfo)
{
	QJsonParseError jsonError;
	QString strFileName;
	QString FileName;
	QString strFileLocalPath;
	QString strurl;
	QString strJsonSize;
	int iRecvFlag;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8(), &jsonError);
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QVariantMap result = jsonDocument.toVariant().toMap();
			strFileName = result["FileName"].toString();
			strFileLocalPath = result["FileLocalPath"].toString();
			strurl = result["url"].toString();
			strJsonSize = result["FileSize"].toString();
			iRecvFlag = result["recvFlag"].toInt();
		}
	}
	else//本地发送失败时非json格式
	{
		strFileLocalPath = msgInfo.strMsg;
		int ipos = strFileLocalPath.lastIndexOf("/");
		if (ipos > -1)
		{
			strFileName = strFileLocalPath.right(strFileLocalPath.length() - ipos - 1);
		}
	}

	FileTypeEx* fileType = new FileTypeEx(this);
	QString strPicPath = fileType->GetFilePic(strFileName);

	QString strSize = QString("(") + gSocketMessage->ByteConversion(fileType->GetFileSize(strFileLocalPath)) + QString(")");
	delete fileType;
	QString newLiID = QString(msgInfo.msgID) + QString("send");
	QString newLiID2 = QString(msgInfo.msgID) + QString("send2");

	//根据msg中是否有url判断是本地还是其他设备 若为其他设备则检测resource下是否存在 //201901014改为收到服务器消息时加入的flag
	if (iRecvFlag != 1)
	{
		QString strSend = QString("SendFile(\"%1\",\"%2\",\"%3\",%4,\"%5\",\"%6\",\"%7\",\"%8\")").arg(strPath).arg(strFileName).arg(QString(msgInfo.msgID)).arg(msgInfo.MessageState).arg(strPicPath).arg(newLiID).arg(strSize).arg(QString::number(msgInfo.nFromUserID));
		m_vm->runJs(strSend);
		if (msgInfo.MessageState == 3 || msgInfo.MessageState == 4 || msgInfo.MessageState == 5 || msgInfo.MessageState == 6 || msgInfo.MessageState == 7)
		{
			QString newLiID = QString(msgInfo.msgID) + QString("send");
			QString changeLi = QString("ChangeLi(\"%1\")").arg(newLiID);
			QString canID = QString(msgInfo.msgID) + QString("can");
			QString Cancle = QString("ChangeLi(\"%1\")").arg(canID);
			ChangeFileState(msgInfo.msgID, 2);
		}
		else if (msgInfo.MessageState == MESSAGE_STATE_FAILURE || msgInfo.MessageState == MESSAGE_STATE_WAITSEND)
		{
			QString newLiID = QString(msgInfo.msgID) + QString("send");
			QString changeLi = QString("ChangeLiCanel(\"%1\")").arg(newLiID);
			QString canID = QString(msgInfo.msgID) + QString("can");
			QString Cancle = QString("ChangeLiCanel(\"%1\")").arg(canID);
			ChangeFileState(msgInfo.msgID, 1);
		}
	}
	else
	{
		if (strSize == "(0B)")
		{
			strSize = "(" + strJsonSize + ")";
		}
		newLiID = QString(msgInfo.msgID) + QString("recv");
		newLiID2 = QString(msgInfo.msgID) + QString("recv2");
		QString strSend = QString("SendOtherFile(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",\"%6\",\"%7\",%8,%9,\"%10\")").arg(strPath).arg(strFileName).arg(strPicPath).arg(QString(msgInfo.msgID)).arg(newLiID).arg(newLiID2).arg(strSize).arg(msgInfo.MessageState).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));
		m_vm->runJs(strSend);
		UserInfo userInfo = gDataManager->getUserInfo();
		QString currentPath = gSettingsManager->getUserPath();
		QString strFilePath = gDataBaseOpera->DBGetFileInfoLocalPath(msgInfo.msgID);
		if (QFile::exists(strFilePath))
		{
			QString strUlID = QString(msgInfo.msgID) + QString("recv");
			QString changeLi = QString("ChangeLiTwo(\"%1\")").arg(strUlID);

			QString strUlID2 = QString(msgInfo.msgID) + QString("recv2");
			QString changeLi2 = QString("ChangeLiTwo(\"%1\")").arg(strUlID2);

			QString strUlID3 = QString(msgInfo.msgID) + QString("recv2");
			QString changeLi3 = QString("ChangeLiTwo(\"%1\")").arg(strUlID3);
			ChangeFileState(msgInfo.msgID, 2);
		}
		else//小于10M的直接下载
		{
			QString strNumber;
			QString strUnit;
			for (int i = 0; i < strJsonSize.length(); i++)
			{
				if (strJsonSize[i] >= '0' && strJsonSize[i] <= '9' || strJsonSize[i] == '.')
				{
					strNumber.append(strJsonSize[i]);
				}
				else
				{
					strUnit.append(strJsonSize[i]);
				}
			}
			if (strUnit == "B" || strUnit == "KB" || (strUnit == "MB"&&strNumber.toDouble() < 10))
			{
				QJsonDocument jsonDocument = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
				QVariantMap result = jsonDocument.toVariant().toMap();
				QString fileName = result["FileName"].toString();
				UserInfo userInfo = gDataManager->getUserInfo();
				QDir dir;
				QString currentPath = gSettingsManager->getUserPath();
				QString strFilePath = currentPath + QString("/files/") + QString("%1").arg(userInfo.nUserID) + QString("/") + fileName;
				QFile recvFile(strFilePath);
				if (!recvFile.exists())
				{
					slotGetFile(msgInfo.msgID);//本地不存在同名文件时直接下载
				}
			}
		}
	}
}

void IMPerChatStore::OnSendRedBagMessage(QString strPath, MessageInfo msgInfo)
{
	QJsonDocument json = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = json.toVariant().toMap();
	QString invalid = map.value("invalid").toString();
	QString remarks;
	if (invalid == "true")    //已经领取/过期等等。
	{
		QVariantMap packet = map.value("redPacket").toMap();
		remarks = packet.value("remarks").toString();
	}
	else                      //还未领取。
	{
		remarks = map.value("leaveMessage").toString();
	}

	if (remarks.isEmpty())
		remarks = tr("Best Wishes");

	QString strSend = QString("SendRedPacketAppend('%1', '%2', '%3', '%4', %5, %6 , '%7');")
		.arg(strPath).arg(remarks).arg(invalid).arg(QString(msgInfo.msgID)).arg(msgInfo.MessageState).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));
	m_vm->runJs(strSend);
}

void IMPerChatStore::OnSendSecretLetter(QString strPath, MessageInfo msgInfo)
{
	QString strSend = QString("SendSecretAppend(\"%1\",\"%2\",%3,%4,\"%5\");")
		.arg(strPath).arg(QString(msgInfo.msgID)).arg(msgInfo.MessageState).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));
	m_vm->runJs(strSend);
}

void IMPerChatStore::OnSendSecretImage(QString strPath, MessageInfo msgInfo)
{
	QString strSend = QString("SendSecretImageAppend(\"%1\",\"%2\",%3,%4,\"%5\");")
		.arg(strPath).arg(QString(msgInfo.msgID)).arg(msgInfo.MessageState).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));
	m_vm->runJs(strSend);
}

void IMPerChatStore::OnSendSecretFile(QString strPath, MessageInfo msgInfo)
{
	QString strSend = QString("SendSecretFileAppend(\"%1\",\"%2\",%3,%4,\"%5\");")
		.arg(strPath).arg(QString(msgInfo.msgID)).arg(msgInfo.MessageState).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));
	m_vm->runJs(strSend);
}

void IMPerChatStore::OnSendNoticeMessage(QString strPath, MessageInfo msgInfo)
{
	QString msg = msgInfo.strMsg;
	QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();
	QString imageUrl = map.value("imageUrl").toString();
	QString imageTitle = map.value("imageTitle").toString();
	QString webUrl = map.value("webUrl").toString();
	QString webTitle = map.value("webTitle").toString();

	QString strSend = QString("SendNotice(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",\"%6\",%7,%8,\"%9\");")
		.arg(strPath).arg("").arg(imageTitle).arg(webUrl).arg(webTitle).arg(QString(msgInfo.msgID))
		.arg(msgInfo.MessageState).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));

	m_vm->runJs(strSend);
	/////////////////图片方向
	QString strFilePath = QDir::tempPath() + "/" + QUuid::createUuid().toString() + ".jpg";
	HttpNetWork::HttpDownLoadFile *netWork = new HttpNetWork::HttpDownLoadFile;
	netWork->setData(QVariant::fromValue(msgInfo));
	netWork->setObjectName(strFilePath);

	connect(netWork, SIGNAL(sigDownFinished(bool)), this, SLOT(slotUpdateNotice(bool)));
	netWork->StartDownLoadFile(imageUrl, strFilePath);
	/////////////////
}

void IMPerChatStore::OnSendShareUrlMessage(QString strPath, MessageInfo msgInfo)
{
	QString msg = msgInfo.strMsg;
	QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();
	QString subject = map.value("subject").toString();
	QString text = map.value("text").toString();
	QString url = map.value("url").toString();
	QString imageUrl = map.value("imgUrl").toString();

	QString strSend = QString("SendShareUrl(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",\"%6\",%7,%8,\"%9\");")
		.arg(subject).arg(text).arg(url).arg(imageUrl)
		.arg(strPath).arg(QString(msgInfo.msgID))
		.arg(msgInfo.MessageState).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));

	m_vm->runJs(strSend);
}

void IMPerChatStore::OnSendLocationMessage(QString strPath, MessageInfo msgInfo)
{
	msgInfo.strMsg.remove("\n");
	msgInfo.strMsg.remove(QChar(32));
	msgInfo.strMsg = msgInfo.strMsg.replace("\"", "\\\"");

	QString strSend = QString("SendLocation(\"%1\", \"%2\", \"%3\", %4, %5, \"%6\");")
		.arg(strPath).arg(msgInfo.strMsg).arg(QString(msgInfo.msgID)).arg(msgInfo.MessageState).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));

	m_vm->runJs(strSend);
}

void IMPerChatStore::OnSendCommonMessage(QString strPath, MessageInfo msgInfo)
{
	QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();

	QString image = map.value("image").toString();
	QString smallIcon = map.value("smallIcon").toString();
	QString title = map.value("title").toString();
	QString content = map.value("content").toString();
	QString systemName = map.value("systemName").toString();
	QString userType = map.value("userType").toString();

	if (image.isEmpty())
	{
		if (userType == "user")
			image = "https://tc.ipcom.io/panserver/files/c64b5061-77dc-45af-b91b-e5e06d401347/download";
		if (userType == "group")
			image = "https://tc.ipcom.io/panserver/files/0bb98df6-cddd-444e-a36c-c50beac4dcd1/download";
	}

	QString strSend = QString("SendCommon('%1', '%2', '%3', '%4', '%5','%6', '%7', %8, %9, '%10');")
		.arg(image).arg(smallIcon).arg(title).arg(content).arg(systemName)
		.arg(strPath).arg(QString(msgInfo.msgID)).arg(msgInfo.MessageState).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));
	m_vm->runJs(strSend);
}

void IMPerChatStore::OnSendAudioMessage(QString strPath, MessageInfo msgInfo)
{
	QString msgID = QString(msgInfo.msgID);
	QString audioPath = msgInfo.strMsg;

	if (audioPath.endsWith(".amr"))
	{
		QString wavPath = audioPath;
		wavPath.replace(".amr", ".wav");

		QFile audioFile(wavPath);
		if (!audioFile.exists())
		{
#ifdef Q_OS_WIN
			AmrDec amrDec;
			amrDec.convertAmrToWav(audioPath, wavPath);
#else
			QString appPath = QDir::currentPath() + "/ffmpeg";
			QStringList arguments;
			arguments << "-i" << audioPath << wavPath;
			QProcess process(this);
			process.start(appPath, arguments);
			process.waitForFinished();
			process.close();
#endif
		}

		audioPath = wavPath;
	}

	int duration = CalWavLength(audioPath);//file.size() / BYTES_PER_SECOND;

	strPath.replace(" ", "%20");
	QString strSend = QString("SendAudio(\"%1\",\"%2\", %3, \"%4\",%5,%6, \"%7\");").arg(strPath).arg(audioPath).arg(duration).arg(msgID).arg(msgInfo.MessageState).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));
	m_vm->runJs(strSend);
}

void IMPerChatStore::OnSendVideoMessage(QString strPath, MessageInfo msgInfo)
{
	QString strIsLoading;//是否是正加载
	QString strVideoPicPath;//视频第一帧图
	QString strVideoPath;//视频路径

	QString strMsg = "";
	QString strPhotoImgPath = "";
	QString msgID = QString(msgInfo.msgID);
	if (msgInfo.strMsg == "load")
	{
		strMsg = "qrc:/html/Resources/html/load.gif";
		strPhotoImgPath = QString("<img width=30px height=30px src='") + strMsg + QString("' />");

		strIsLoading = "true";
	}
	else if (msgInfo.strMsg != "fail")
	{
#ifdef Q_OS_WIN
		msgInfo.strMsg.replace(" ", "%20");
#endif
		QString strPicPath = msgInfo.strMsg.left(msgInfo.strMsg.indexOf("."));
		strPicPath += ".png";
		VedioFrameOpera pVdo;
		pVdo.CreateVedioPicture(msgInfo.strMsg, strPicPath);
		strPhotoImgPath = QString("<img height:auto width:auto src='file:///%1' name = '%2' onload='movePlayIcon(this);loadpic()'/> ").arg(strPicPath).arg(msgInfo.strMsg);

		strIsLoading = "false";
		strVideoPicPath = "file:///" + strPicPath;
		strVideoPath = msgInfo.strMsg;
	}

	QString strSend = QString("SendVdo(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",%6,%7,\"%8\");").arg(strPath).arg(strIsLoading).arg(strVideoPicPath).arg(strVideoPath).arg(msgID).arg(msgInfo.MessageState).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));
	m_vm->runJs(strSend);
}

void IMPerChatStore::OnSendNotifyMessage(QString strPath, MessageInfo msgInfo)
{
	QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();

	QString content;
	if (map.value("type").toString() == "notification")
	{
		content = map.value("content").toString();
	}
	if (map.value("type").toString() == "receivedFile")
	{
		content = tr("Received file successfully!") + QString("\\\"%1\\\"").arg(map.value("fileName").toString());
	}

	QString strSend = QString("tipMessage(\"%1\");").arg(content);
	m_vm->runJs(strSend);
}

void IMPerChatStore::OnSendTransferMessage(QString strPath, MessageInfo msgInfo)
{
	UserInfo userInfo = gDataManager->getUserInfo();
	QString userID = QString::number(userInfo.nUserID);
	QJsonDocument json = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = json.toVariant().toMap();
	QString transfer;
	if (userID == map.value("sendIMUserId").toString())
	{
		//用户是发送者。
		transfer = "sender";
	}
	else
	{
		transfer = "receiver";
	}

	QString strSend = QString("RecTransfer(\"%1\",\"%2\",\"%3\",%4,\"%5\");")
		.arg(strPath).arg(transfer).arg(QString(msgInfo.msgID)).arg(msgInfo.integral).arg(QString::number(msgInfo.nFromUserID));
	m_vm->runJs(strSend);
}

void IMPerChatStore::slotInsertTextEditPic(QString strPath)
{
	m_vm->insertImageToTextEditSend(strPath);
}

void IMPerChatStore::slotSendTransmitMessage(MessageInfo msg)
{
	if (initFinished)  //界面初始化完毕
	{
		switch (msg.MessageChildType)
		{
		case MessageType::Message_TEXT:
			SendTextMessage(msg.strMsg, msg.strMsg);
			break;
		case MessageType::Message_PIC:
			SendPicture(msg.strMsg);
			break;
		case MessageType::Message_FILE:
			SendTransmitFile(msg);
			break;
		case MessageType::Message_VEDIO:
			SendVideo(msg);
			break;
		default:
			break;
		}
	}
	else
	{
		waitMessages.append(msg);
	}
}

void IMPerChatStore::SendTextMessage(QString strText, QString strHtmlText)
{
	if (gSocketMessage)
	{
		UserInfo userInfo = gDataManager->getUserInfo();
		int nUserID = userInfo.nUserID;

		QString toUserID = m_vm->getBuddyId();
		if (toUserID.isEmpty())
		{
			return;
		}

		MessageInfo msgInfo = gSocketMessage->SendTextMessage(nUserID, toUserID.toInt(), 0, (short)0, strText);
		slotProcessSendMessageInfo(userInfo.strUserAvatarLocal, msgInfo);//用最原始的文本消息

		MessageListInfo messageListInfo;
		messageListInfo.nBudyyID = this->objectName().toInt();
		messageListInfo.strLastMessage = msgInfo.strMsg;
		messageListInfo.nLastTime = msgInfo.ClientTime;
		messageListInfo.strBuddyName = m_vm->getNickName();
		BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(this->objectName());
		if (buddy.strNickName.isEmpty())
			buddy = gDataBaseOpera->DBGetGroupUserFromID(this->objectName());
		messageListInfo.strBuddyHeaderImage = buddy.strLocalAvatar;
		messageListInfo.messageType = msgInfo.MessageChildType;
		messageListInfo.nUnReadNum = 0;
		messageListInfo.isGroup = 0;

		gDataBaseOpera->DBInsertPerMessageListInfo(messageListInfo);
		//更新至消息列表，false代表个人消息
		QMap<QString, QVariant> params;
		params["SendStatus"] = MESSAGE_STATE_PRESEND;
		params["MsgId"] = QString(msgInfo.msgID);
		emit m_dispatcher->sigUpdateSelfMessage(false, QVariant::fromValue(buddy), msgInfo.strMsg, true, params);
	}
}


void IMPerChatStore::SendPicture(QString fileName)
{
	//插入时间线。
	slotInsertTimeLine(GETLOCALTIME);
	QFile file(fileName);
	if (!file.exists())
	{
		QJsonParseError jsonError;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(fileName.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QVariantMap result = jsonDocument.toVariant().toMap();
				qDebug() << "img:" << result["path"].toString();
				fileName = result["path"].toString();
			}
		}
	}

	QString filePathEscape = fileName;
	filePathEscape.replace(" ", "%20");
	filePathEscape.replace("#", "%23");

	QString strIsUserDefine = "false";
	QString strUserDefinePicPath = "";
	QString strPicPath = QString("file:///") + filePathEscape;

	QImage img;
	QByteArray arr;
	QFile filepic(fileName);
	if (filepic.open(QIODevice::ReadOnly))
	{//20181214wmc 手机表情包图片常存在后缀与实际不符的情况 此处直接load会打不开
		arr = filepic.readAll();
	}
	filepic.close();
	img.loadFromData(arr);
	UserInfo userInfo = gDataManager->getUserInfo();
	int nUserID = userInfo.nUserID;
	QString toUserID = m_vm->getBuddyId();
	if (toUserID.isEmpty())
	{
		return;
	}
	AppConfig configInfo = gDataManager->getAppConfigInfo();
	MessageInfo messageInfo = gSocketMessage->SendPicMessage(nUserID, toUserID.toInt(), 0, "");
	messageInfo.strMsg = fileName;
	gSocketMessage->DBInsertMessageInfo(messageInfo.nFromUserID, messageInfo.nToUserID, messageInfo);

	QString strPath = convertHeaderToHtmlSrc(userInfo.strUserAvatarLocal);

	if (!strPicPath.isEmpty())
	{
		strPicPath = GetSmallImg(strPicPath);
	}
	QString strSend = QString("SendPicture(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",%6,%7,\"%8\");").arg(strPath).arg(strIsUserDefine).arg(strUserDefinePicPath).arg(strPicPath).arg(QString(messageInfo.msgID)).arg(0).arg(messageInfo.integral).arg(QString::number(messageInfo.nFromUserID));

	m_vm->runJs(strSend);

	PicMessage *picMessage = new PicMessage;
	picMessage->nFromUserID = nUserID;
	picMessage->nToUserID = toUserID.toInt();
	picMessage->nDeliverType = 1;
	picMessage->strUpLoadUrl = configInfo.PanServerUploadURL;
	picMessage->strDownLoadUrl = configInfo.PanServerDownloadURL;
	picMessage->strPicPath = fileName;
	picMessage->nPicHeight = img.height();
	picMessage->nPicWidth = img.width();

	HttpNetWork::HttpUpLoadFile *httpUpLoad = new HttpNetWork::HttpUpLoadFile(this);
	m_iHttp++;
	httpUpLoad->setUserData(Qt::UserRole, picMessage);
	httpUpLoad->setData(QVariant::fromValue(messageInfo));

	connect(httpUpLoad, SIGNAL(sigUpLoadFinished(bool, QByteArray)), this, SLOT(slotUpPicReplyFinished(bool, QByteArray)));
	QVariantMap pargram;
	pargram.insert("parentId", "66662");
	pargram.insert("createUser", "6662");
	httpUpLoad->StartUpLoadFile(configInfo.PanServerUploadURL, fileName, pargram);
	m_listPic.push_back(fileName);

	//更新至消息列表，false代表个人消息
	QMap<QString, QVariant> params;
	params["SendStatus"] = MESSAGE_STATE_PRESEND;
	params["MsgId"] = QString(messageInfo.msgID);

	MessageListInfo messageListInfo;
	messageListInfo.nBudyyID = this->objectName().toInt();
	messageListInfo.strLastMessage = tr("[Image]");
	messageListInfo.nLastTime = messageInfo.ClientTime;
	messageListInfo.strBuddyName = m_vm->getNickName();
	BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(this->objectName());
	if (buddy.strNickName.isEmpty())
		buddy = gDataBaseOpera->DBGetGroupUserFromID(this->objectName());
	messageListInfo.strBuddyHeaderImage = buddy.strLocalAvatar;
	messageListInfo.messageType = messageInfo.MessageChildType;
	messageListInfo.nUnReadNum = 0;
	messageListInfo.isGroup = 0;
	gDataBaseOpera->DBInsertPerMessageListInfo(messageListInfo);
	emit m_dispatcher->sigUpdateSelfMessage(false, QVariant::fromValue(buddy), messageListInfo.strLastMessage, true, params);
}

/*发送图片 上传完图片 结果*/
void IMPerChatStore::slotUpPicReplyFinished(bool result, QByteArray strResult)
{
	m_iHttp--;
	HttpNetWork::HttpUpLoadFile *act = qobject_cast<HttpNetWork::HttpUpLoadFile*>(sender());
	PicMessage *picMessage = (PicMessage *)act->userData(Qt::UserRole);
	QVariant var = act->getData();
	MessageInfo msgInfo = var.value<MessageInfo>();

	if (result)
	{
		gSocketMessage->setUpPicReplyFinished(strResult, picMessage, msgInfo);

		MessageListInfo messageListInfo;
		messageListInfo.nBudyyID = this->objectName().toInt();
		messageListInfo.strLastMessage = tr("[Image]");
		messageListInfo.nLastTime = msgInfo.ClientTime;
		messageListInfo.strBuddyName = m_vm->getNickName();
		BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(this->objectName());
		if (buddy.strNickName.isEmpty())
			buddy = gDataBaseOpera->DBGetGroupUserFromID(this->objectName());
		messageListInfo.strBuddyHeaderImage = buddy.strLocalAvatar;
		messageListInfo.messageType = msgInfo.MessageChildType;
		messageListInfo.nUnReadNum = 0;
		messageListInfo.isGroup = 0;
		gDataBaseOpera->DBInsertPerMessageListInfo(messageListInfo);
		//更新至消息列表，false代表个人消息
		QMap<QString, QVariant> params;
		params["SendStatus"] = MESSAGE_STATE_SEND;
		params["MsgId"] = QString(msgInfo.msgID);
		emit m_dispatcher->sigUpdateSelfMessage(false, QVariant::fromValue(buddy), messageListInfo.strLastMessage, true, params);
	}
	else
	{
		qDebug() << "严重 ！！！ 发送失败标志出现 ： slotUpPicReplyFinished()";
		gSocketMessage->SetMessageState(msgInfo.msgID, MESSAGE_STATE_FAILURE);
		this->UpdateMessageStateInfo(msgInfo.msgID, MESSAGE_STATE_FAILURE, msgInfo.integral);
	}
}

void IMPerChatStore::UpdateMessageStateInfo(QByteArray msgID, int nState, int score)
{
	QString strSend = QString("SetMsgState(\"%1\",%2,%3)").arg(QString(msgID)).arg(nState).arg(score);
	m_vm->runJs(strSend);

	BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(this->objectName());
	if (buddy.strNickName.isEmpty())
		buddy = gDataBaseOpera->DBGetGroupUserFromID(this->objectName());

	//更新至消息列表，false代表个人消息
	QMap<QString, QVariant> params;
	params["SendStatus"] = nState;
	params["MsgId"] = QString(msgID);
	emit m_dispatcher->sigUpdateSelfMessage(false, QVariant::fromValue(buddy), "", true, params);
}

void IMPerChatStore::SendTransmitFile(MessageInfo msg)
{
	//插入时间线。
	slotInsertTimeLine(GETLOCALTIME);

	UserInfo userinfo = gDataManager->getUserInfo();
	int nUserID = userinfo.nUserID;

	QJsonDocument doc = QJsonDocument::fromJson(msg.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();

	QString fileName = map.value("FileName").toString();
	QString filePath = gDataBaseOpera->DBGetFileInfoLocalPath(msg.msgID);

	QFile transmitFile(filePath);
	if (transmitFile.exists())
	{
		QString toUserID = m_vm->getBuddyId();
		MessageInfo messageInfo = gSocketMessage->SendFileMessage(nUserID, toUserID.toInt(), 0, "");
		gDataBaseOpera->DBOnInsertFileInfo(QString(messageInfo.msgID), filePath, "");//将发送文件信息插入数据库

		FileTypeEx* fileType = new FileTypeEx(this);
		QString strPath = fileType->GetFilePic(filePath);

		QString strLiID = QString(messageInfo.msgID) + QString("send");
		QString strsize = gSocketMessage->ByteConversion(fileType->GetFileSize(filePath));
		delete fileType;
		strsize = QString("(") + strsize + QString(")");
		QString strHeadPath = convertHeaderToHtmlSrc(userinfo.strUserAvatarLocal);
		QString strSend = QString("SendFile(\"%1\",\"%2\",\"%3\",%4,\"%5\",\"%6\",\"%7\",\"%8\")").arg(strHeadPath).arg(fileName).arg(QString(messageInfo.msgID)).arg(0).arg(strPath).arg(strLiID).arg(strsize).arg(QString::number(messageInfo.nFromUserID));
		m_vm->runJs(strSend);

		AppConfig conf = gDataManager->getAppConfigInfo();

		FileMessage *fileMessage = new FileMessage;
		fileMessage->nFromUserID = nUserID;
		fileMessage->nToUserID = toUserID.toInt();
		fileMessage->nDeliverType = 0;
		fileMessage->strUpLoadUrl = conf.PanServerUploadURL;
		fileMessage->strDownLoadUrl = conf.PanServerDownloadURL;
		fileMessage->strFilePath = filePath;

		QFile upfile(filePath);
		fileMessage->FileName = fileName;
		fileMessage->FileType = fileName.split(".").last();
		fileMessage->FileSize = gSocketMessage->ByteConversion(upfile.size());

		MessageInfo msgInfo = messageInfo;

		QString newLiID = QString(msgInfo.msgID) + QString("send");
		QString changeLi = QString("ChangeLi(\"%1\")").arg(newLiID);


		QString cancleID = msgInfo.msgID + QString("can");
		QString cancle = QString("ChangeLi(\"%1\")").arg(cancleID);
		ChangeFileState(msgInfo.msgID, 2);


		QVariantMap resultMap;
		resultMap.insert("result", "success");
		resultMap.insert("fileid", map.value("FileId"));
		QByteArray result = QJsonDocument::fromVariant(resultMap).toJson();

		gSocketMessage->setUpFileReplyFinished(result, fileMessage, msgInfo);

		MessageListInfo messageListInfo;
		messageListInfo.nBudyyID = this->objectName().toInt();
		messageListInfo.strLastMessage = tr("[File]");
		messageListInfo.nLastTime = msgInfo.ClientTime;
		messageListInfo.strBuddyName = m_vm->getNickName();
		BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(this->objectName());
		if (buddy.strNickName.isEmpty())
			buddy = gDataBaseOpera->DBGetGroupUserFromID(this->objectName());
		messageListInfo.strBuddyHeaderImage = buddy.strLocalAvatar;
		messageListInfo.messageType = msgInfo.MessageChildType;
		messageListInfo.nUnReadNum = 0;
		messageListInfo.isGroup = 0;
		gDataBaseOpera->DBInsertPerMessageListInfo(messageListInfo);
		//更新至消息列表，false代表个人消息
		QMap<QString, QVariant> params;
		emit m_dispatcher->sigUpdateSelfMessage(false, QVariant::fromValue(buddy), messageListInfo.strLastMessage, true, params);
	}
	else
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("Local file does't exist，unable to forward"));
	}
}


void IMPerChatStore::SendVideo(MessageInfo msg)
{
	QString fileLocalPath = msg.strMsg;
	if (gSocketMessage)
	{
		UserInfo userInfo = gDataManager->getUserInfo();
		int nUserID = userInfo.nUserID;
		QString toUserID = m_vm->getBuddyId();
		if (toUserID.isEmpty())
		{
			return;
		}

		QString getFileHttpPath = gDataBaseOpera->DBGetFileInfoHttpPathByLocalPath(msg.strMsg);

		MessageInfo msgInfo = gSocketMessage->SendVideoMessage(nUserID, toUserID.toInt(), 0, Message_VEDIO, getFileHttpPath);

		/*查詢一遍本地路徑並更新數據庫*/
		msgInfo.strMsg = msg.strMsg;
		fileLocalPath = fileLocalPath.replace(gSettingsManager->getUserPath(), "");
		gSocketMessage->DBRestoreLocalPathInMessageInfo(msgInfo.msgID, fileLocalPath);

		slotProcessSendMessageInfo(userInfo.strUserAvatarLocal, msgInfo);//用最原始的文本消息


		MessageListInfo messageListInfo;
		messageListInfo.nBudyyID = this->objectName().toInt();
		messageListInfo.strLastMessage = msgInfo.strMsg;
		messageListInfo.nLastTime = msgInfo.ClientTime;
		messageListInfo.strBuddyName = m_vm->getNickName();
		BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(this->objectName());
		if (buddy.strNickName.isEmpty())
			buddy = gDataBaseOpera->DBGetGroupUserFromID(this->objectName());
		messageListInfo.strBuddyHeaderImage = buddy.strLocalAvatar;
		messageListInfo.messageType = msgInfo.MessageChildType;
		messageListInfo.nUnReadNum = 0;
		messageListInfo.isGroup = 0;
		gDataBaseOpera->DBInsertPerMessageListInfo(messageListInfo);
		//更新至消息列表，false代表个人消息
		QMap<QString, QVariant> params;
		params["SendStatus"] = MESSAGE_STATE_PRESEND;
		params["MsgId"] = QString(msgInfo.msgID);
		emit m_dispatcher->sigUpdateSelfMessage(false, QVariant::fromValue(buddy), msgInfo.strMsg, true, params);
	}
}

void IMPerChatStore::slotShareID(int type, QString contactID)
{
	QString title;
	QString AndroidShow;
	QString strShow;
	QString userType;
	QString systemName;
	QString content;
	QString image;

	QString smallIcon = "https://tc.ipcom.io/panserver/files/2c6a7eda-7995-4bd6-828b-4ba9b4d2c163/download";
	QString viewType = "display";
	int id = contactID.toInt();


	QVariantMap map;
	if (type == OpenPer)
	{
		BuddyInfo buddyInfo = gDataBaseOpera->DBGetBuddyInfoByID(contactID);
		title = buddyInfo.strNickName;
		AndroidShow = "com.efounder.chat.activity.JFUserInfoActivity";
		strShow = "EFUserDetailViewController";
		userType = "user";
		systemName = tr("Personal business card");
		content = tr("ID:") + contactID;
		if (buddyInfo.strHttpAvatar.isEmpty())
			image = "https://tc.ipcom.io/panserver/files/c64b5061-77dc-45af-b91b-e5e06d401347/download";
		else
			image = buddyInfo.strHttpAvatar;
	}
	if (type == OpenGroup)
	{
		GroupInfo groupInfo = gDataBaseOpera->DBGetGroupFromID(contactID);
		title = groupInfo.groupName;
		AndroidShow = "com.efounder.chat.activity.AddGroupUserInfoActivity";
		strShow = "GroupApplicantViewController";
		userType = "group";
		systemName = tr("Group business card");
		content = tr("Group ID:") + contactID;
		if (groupInfo.groupHttpHeadImage.isEmpty())
			image = "https://tc.ipcom.io/panserver/files/0bb98df6-cddd-444e-a36c-c50beac4dcd1/download";
		else
			image = groupInfo.groupHttpHeadImage;
	}

	map.insert("title", title);
	map.insert("smallIcon", smallIcon);
	map.insert("time", "");
	map.insert("AndroidShow", AndroidShow);
	map.insert("show", strShow);
	map.insert("userType", userType);
	map.insert("systemName", systemName);
	map.insert("content", content);
	map.insert("viewType", viewType);
	map.insert("id", id);
	map.insert("image", image);

	QString text = QJsonDocument::fromVariant(map).toJson();
	UserInfo userinfo = gDataManager->getUserInfo();
	MessageInfo msgInfo = gSocketMessage->SendTextMessage(userinfo.nUserID, objectName().toInt(), 0, (short)MessageType::Message_COMMON, text);

	slotProcessSendMessageInfo(userinfo.strUserAvatarLocal, msgInfo);

	MessageListInfo messageListInfo;
	messageListInfo.nBudyyID = this->objectName().toInt();
	messageListInfo.strLastMessage = QString("[%1]").arg(systemName);
	messageListInfo.nLastTime = msgInfo.ClientTime;
	messageListInfo.strBuddyName = m_vm->getNickName();
	BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(this->objectName());
	if (buddy.strNickName.isEmpty())
		buddy = gDataBaseOpera->DBGetGroupUserFromID(this->objectName());
	messageListInfo.strBuddyHeaderImage = buddy.strLocalAvatar;
	messageListInfo.messageType = msgInfo.MessageChildType;
	messageListInfo.nUnReadNum = 0;
	messageListInfo.isGroup = 0;
	gDataBaseOpera->DBInsertPerMessageListInfo(messageListInfo);
	//更新至消息列表，false代表个人消息
	QMap<QString, QVariant> params;
	emit m_dispatcher->sigUpdateSelfMessage(false, QVariant::fromValue(buddy), messageListInfo.strLastMessage, true, params);
}

void IMPerChatStore::slotRecvNotifyMessage(MessageInfo messageInfo)
{
	this->OnRecvNotifyMessage(messageInfo);
}

void IMPerChatStore::slotStartInputting()
{
	m_vm->startInputting();
}

void IMPerChatStore::slotEndInputting()
{
	m_vm->endInputting();
}

void IMPerChatStore::slotParseScreenCutMessage(QString strMsg)
{
	QJsonParseError jsonError;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(strMsg.toUtf8(), &jsonError);
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QVariantMap result = jsonDocument.toVariant().toMap();
			QString strCMD = result["CMD"].toString();

			uintptr_t widgetPtr = result["widgetPtr"].toUInt();

			if (widgetPtr != (uintptr_t)(this->parent()))
			{
				return;//不是发给该聊天窗口的，不处理消息
			}

			if (strCMD == "ScreenCancle")
			{
				slotScreenCanclePixMap();
			}
			else if (strCMD == "ScreenShotComplete")
			{
				slotSendScreenShotPic();
			}
		}
	}
}

void IMPerChatStore::slotScreenCanclePixMap()
{
#ifdef Q_OS_MAC
	OEScreenshot *act = qobject_cast<OEScreenshot*>(sender());
	if (act)
	{
		act->showNormal();
		act->close();
		emit m_dispatcher->sigShowNormalWindow();
	}
#endif
}

void IMPerChatStore::slotSendScreenShotPic()
{
#ifdef Q_OS_MAC
	OEScreenshot *act = qobject_cast<OEScreenshot*>(sender());
	if (act)
	{
		act->showNormal();
		act->close();
		emit m_dispatcher->sigShowNormalWindow();
	}
#endif
	QString imageID = QString::number(QDateTime::currentDateTime().toTime_t());
#ifdef Q_OS_WIN
	QString strPath = gSettingsManager->getUserPath() + "/Clipboard/" + imageID + ".png";
#else
	QString strPath = gSettingsManager->getUserPath() + "/Clipboard/" + imageID + ".png";
#endif
	QDir fileDir = QFileInfo(strPath).absoluteDir();
	QString strFileDir = QFileInfo(strPath).absolutePath();
	if (!fileDir.exists())
		fileDir.mkpath(strFileDir);

	QClipboard *board = QApplication::clipboard();
	const QMimeData *mimeData = board->mimeData();
	if (mimeData->hasImage())
	{
		QImage image = qvariant_cast<QImage>(mimeData->imageData());
		image.save(strPath, "PNG");
		slotInsertTextEditPic(strPath);
	}
}

void IMPerChatStore::slotUpdateHtmlBuddyHeaderImagePath(int nIMUserID, QString headerPath)
{
	m_vm->runJs(QString("window.UpdateHtmlBuddyHeaderImagePath(\"%1\",\"%2\")").arg(QString::number(nIMUserID), headerPath));
}

void IMPerChatStore::slotUpdateHtmlBuddyNickName(int nIMUserID, QString nickName, int nGroupID)
{
	if (nGroupID > -1)
	{
		return;
	}
	m_vm->runJs(QString("window.UpdateHtmlBuddyNickName(\"%1\",\"%2\")").arg(QString::number(nIMUserID), nickName));
}

void IMPerChatStore::slotGivePacket(QString data)
{
	UserInfo userinfo = gDataManager->getUserInfo();

	MessageInfo msgInfo = gSocketMessage->SendTextMessage(userinfo.nUserID, objectName().toInt(), 0, (short)MessageType::Message_REDBAG, data);

	slotProcessSendMessageInfo(userinfo.strUserAvatarLocal, msgInfo);

	//根据消息类型设置显示的内容。
	msgInfo.strMsg = tr("[Red Packet]");

	MessageListInfo messageListInfo;
	messageListInfo.nBudyyID = this->objectName().toInt();
	messageListInfo.strLastMessage = msgInfo.strMsg;
	messageListInfo.nLastTime = msgInfo.ClientTime;
	messageListInfo.strBuddyName = m_vm->getNickName();
	BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(this->objectName());
	if (buddy.strNickName.isEmpty())
		buddy = gDataBaseOpera->DBGetGroupUserFromID(this->objectName());
	messageListInfo.strBuddyHeaderImage = buddy.strLocalAvatar;
	messageListInfo.messageType = msgInfo.MessageChildType;
	messageListInfo.nUnReadNum = 0;
	messageListInfo.isGroup = 0;
	gDataBaseOpera->DBInsertPerMessageListInfo(messageListInfo);

	//更新左侧消息列表内容
	QMap<QString, QVariant> params;
	emit m_dispatcher->sigUpdateSelfMessage(false, QVariant::fromValue(buddy), messageListInfo.strLastMessage, true, params);
}


void IMPerChatStore::slotSendFile(QString fileName)
{
	//判断文件是否大于100M
	QFile file(fileName);
	qint64 iSize = file.size();
	if (iSize > 1024 * 1024 * 100)
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("File larger than 100M cannot be sent"));
		return;
	}
	//插入时间线。
	slotInsertTimeLine(GETLOCALTIME);
	QString FileName = fileName.mid(fileName.lastIndexOf("/") + 1);
	UserInfo userinfo = gDataManager->getUserInfo();

	int nUserID = userinfo.nUserID;
	if (m_vm->getBuddyId().isEmpty())
	{
		return;
	}

	QString toUserID = m_vm->getBuddyId();
	MessageInfo messageInfo = gSocketMessage->SendFileMessage(nUserID, toUserID.toInt(), 0, "");
	gDataBaseOpera->DBOnInsertFileInfo(QString(messageInfo.msgID), fileName, "");//将发送文件信息插入数据库
	messageInfo.strMsg = fileName;
	gSocketMessage->DBInsertMessageInfo(messageInfo.nFromUserID, messageInfo.nToUserID, messageInfo);

	FileTypeEx* fileType = new FileTypeEx(this);
	QString strPath = fileType->GetFilePic(fileName);

	QString strLiID = QString(messageInfo.msgID) + QString("send");
	QString strsize = gSocketMessage->ByteConversion(fileType->GetFileSize(fileName));
	delete fileType;
	strsize = QString("(") + strsize + QString(")");
	strPath.replace(" ", "%20");
	QString strHeadPath = convertHeaderToHtmlSrc(userinfo.strUserAvatarLocal);
	QString strend = QString("SendFile(\"%1\",\"%2\",\"%3\",%4,\"%5\",\"%6\",\"%7\",\"%8\")").arg(strHeadPath).arg(FileName).arg(QString(messageInfo.msgID)).arg(0).arg(strPath).arg(strLiID).arg(strsize).arg(QString::number(messageInfo.nFromUserID));
	m_vm->runJs(strend);

	AppConfig conf = gDataManager->getAppConfigInfo();

	FileMessage *fileMessage = new FileMessage;
	fileMessage->nFromUserID = nUserID;
	fileMessage->nToUserID = toUserID.toInt();
	fileMessage->nDeliverType = 0;
	fileMessage->strUpLoadUrl = conf.PanServerUploadURL;
	fileMessage->strDownLoadUrl = conf.PanServerDownloadURL;
	fileMessage->strFilePath = fileName;

	QFile upfile(fileName);
	int nIndex = fileName.lastIndexOf("/");
	QString strFileName = fileName.mid(nIndex + 1);
	nIndex = strFileName.lastIndexOf(".");
	fileMessage->FileName = strFileName;
	fileMessage->FileType = strFileName.mid(nIndex + 1, strFileName.length());
	fileMessage->FileSize = gSocketMessage->ByteConversion(upfile.size());
	HttpNetWork::HttpUpLoadFile *httpUpLoad = new HttpNetWork::HttpUpLoadFile(this);
	m_iHttp++;
	httpUpLoad->setUserData(Qt::UserRole, fileMessage);
	httpUpLoad->setData(QVariant::fromValue(messageInfo));

	connect(httpUpLoad, SIGNAL(sigUpLoadFinished(bool, QByteArray)), this, SLOT(slotUpFileReplyFinished(bool, QByteArray)));
	connect(httpUpLoad, SIGNAL(sigUpLoadProgress(qint64, qint64)), this, SLOT(doUpLoadFileProgress(qint64, qint64)));
	connect(m_dispatcher, SIGNAL(sigCancel(QString)), httpUpLoad, SLOT(slotLoadorDownLoadCancle(QString)));
	QVariantMap pargram;
	pargram.insert("parentId", "66662");
	pargram.insert("createUser", "6662");
	httpUpLoad->StartUpLoadFile(conf.PanServerUploadURL, fileName, pargram);


	MessageListInfo messageListInfo;
	messageListInfo.nBudyyID = this->objectName().toInt();
	messageListInfo.strLastMessage = tr("[File]");
	messageListInfo.nLastTime = QDateTime::currentMSecsSinceEpoch();
	messageListInfo.strBuddyName = m_vm->getNickName();
	BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(this->objectName());
	if (buddy.strNickName.isEmpty())
		buddy = gDataBaseOpera->DBGetGroupUserFromID(this->objectName());
	messageListInfo.strBuddyHeaderImage = buddy.strLocalAvatar;
	messageListInfo.messageType = MessageType::Message_FILE;
	messageListInfo.nUnReadNum = 0;
	messageListInfo.isGroup = 0;
	gDataBaseOpera->DBInsertPerMessageListInfo(messageListInfo);
	//更新至消息列表，false代表个人消息
	QMap<QString, QVariant> params;
	params["SendStatus"] = MESSAGE_STATE_PRESEND;
	params["MsgId"] = QString(messageInfo.msgID);
	emit m_dispatcher->sigUpdateSelfMessage(false, QVariant::fromValue(buddy), messageListInfo.strLastMessage, false, params);
}

void IMPerChatStore::doUpLoadFileProgress(qint64 sendnum, qint64 total)
{
	HttpNetWork::HttpUpLoadFile *requestHttp = qobject_cast<HttpNetWork::HttpUpLoadFile*>(sender());
	if (requestHttp)
	{
		QVariant var = requestHttp->getData();
		MessageInfo msgInfo = var.value<MessageInfo>();
		if (total > 0)
		{
			int num = 250 * sendnum / total;
			QString processID = msgInfo.msgID + QString("bar");
			QString strSend = QString("ProgressBar(\"%1\",%2)").arg(processID).arg(num);
			m_vm->runJs(strSend);
		}
		else {
			QString processID = msgInfo.msgID + QString("bar");
			QString strSend = QString("ProgressBar(\"%1\",%2)").arg(processID).arg(0);
			m_vm->runJs(strSend);
		}
	}
}

void IMPerChatStore::slotUpFileReplyFinished(bool bResult, QByteArray result)
{
	m_iHttp--;
	HttpNetWork::HttpUpLoadFile *requestHttp = qobject_cast<HttpNetWork::HttpUpLoadFile*>(sender());
	FileMessage *fileMessage = (FileMessage *)requestHttp->userData(Qt::UserRole);
	QVariant var = requestHttp->getData();
	MessageInfo msgInfo = var.value<MessageInfo>();

	if (bResult && requestHttp)
	{
		QString newLiID = QString(msgInfo.msgID) + QString("send");
		QString changeLi = QString("ChangeLi(\"%1\")").arg(newLiID);

		QString cancleID = msgInfo.msgID + QString("can");
		QString cancle = QString("ChangeLi(\"%1\")").arg(cancleID);

		ChangeFileState(msgInfo.msgID, 2);

		gSocketMessage->setUpFileReplyFinished(result, fileMessage, msgInfo);

		MessageListInfo messageListInfo;
		messageListInfo.nBudyyID = this->objectName().toInt();
		messageListInfo.strLastMessage = tr("[File]");
		messageListInfo.nLastTime = msgInfo.ClientTime;
		messageListInfo.strBuddyName = m_vm->getNickName();
		BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(this->objectName());
		if (buddy.strNickName.isEmpty())
			buddy = gDataBaseOpera->DBGetGroupUserFromID(this->objectName());
		messageListInfo.strBuddyHeaderImage = buddy.strLocalAvatar;
		messageListInfo.messageType = msgInfo.MessageChildType;
		messageListInfo.nUnReadNum = 0;
		messageListInfo.isGroup = 0;
		gDataBaseOpera->DBInsertPerMessageListInfo(messageListInfo);
		//更新至消息列表，false代表个人消息
		QMap<QString, QVariant> params;
		params["SendStatus"] = MESSAGE_STATE_SEND;
		params["MsgId"] = QString(msgInfo.msgID);
		emit m_dispatcher->sigUpdateSelfMessage(false, QVariant::fromValue(buddy), messageListInfo.strLastMessage, false, params);
	}
	else
	{
		qDebug() << "严重 ！！！ 发送失败标志出现 ： slotUpFileReplyFinished()";
		gSocketMessage->SetMessageState(msgInfo.msgID, MESSAGE_STATE_FAILURE);
		this->UpdateMessageStateInfo(msgInfo.msgID, MESSAGE_STATE_FAILURE, msgInfo.integral);

		QString newLiID = QString(msgInfo.msgID) + QString("send");
		QString changeLi = QString("ChangeLiFailed(\"%1\")").arg(newLiID);

		QString cancleID = msgInfo.msgID + QString("can");
		QString cancle = QString("ChangeLiFailed(\"%1\")").arg(cancleID);

		ChangeFileState(msgInfo.msgID, 1);

	}
}

void IMPerChatStore::slotSendSecretLetter(QString password, QString text, QString message)
{
	QString& letter = message;

	UserInfo userinfo = gDataManager->getUserInfo();

	MessageInfo msgInfo = gSocketMessage->SendTextMessage(userinfo.nUserID, objectName().toInt(), 0, (short)Message_SECRETLETTER, letter);

	slotProcessSendMessageInfo(userinfo.strUserAvatarLocal, msgInfo);

	//根据消息类型设置显示的内容。
	msgInfo.strMsg = tr("[Secret Message]");

	MessageListInfo messageListInfo;
	messageListInfo.nBudyyID = this->objectName().toInt();
	messageListInfo.strLastMessage = msgInfo.strMsg;
	messageListInfo.nLastTime = msgInfo.ClientTime;
	messageListInfo.strBuddyName = m_vm->getNickName();
	BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(this->objectName());
	if (buddy.strNickName.isEmpty())
		buddy = gDataBaseOpera->DBGetGroupUserFromID(this->objectName());
	messageListInfo.strBuddyHeaderImage = buddy.strLocalAvatar;
	messageListInfo.messageType = msgInfo.MessageChildType;
	messageListInfo.nUnReadNum = 0;
	messageListInfo.isGroup = 0;
	gDataBaseOpera->DBInsertPerMessageListInfo(messageListInfo);

	//更新左侧消息列表内容
	QMap<QString, QVariant> params;
	emit m_dispatcher->sigUpdateSelfMessage(false, QVariant::fromValue(buddy), messageListInfo.strLastMessage, true, params);
}

void IMPerChatStore::slotSendSecretImage(QString message)
{
	QString& letter = message;

	UserInfo userinfo = gDataManager->getUserInfo();

	MessageInfo msgInfo = gSocketMessage->SendTextMessage(userinfo.nUserID, objectName().toInt(), 0, (short)Message_SECRETIMAGE, letter);

	slotProcessSendMessageInfo(userinfo.strUserAvatarLocal, msgInfo);

	//根据消息类型设置显示的内容。
	msgInfo.strMsg = tr("[Secret Image]");

	MessageListInfo messageListInfo;
	messageListInfo.nBudyyID = this->objectName().toInt();
	messageListInfo.strLastMessage = msgInfo.strMsg;
	messageListInfo.nLastTime = msgInfo.ClientTime;
	messageListInfo.strBuddyName = m_vm->getNickName();
	BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(this->objectName());
	if (buddy.strNickName.isEmpty())
		buddy = gDataBaseOpera->DBGetGroupUserFromID(this->objectName());
	messageListInfo.strBuddyHeaderImage = buddy.strLocalAvatar;
	messageListInfo.messageType = msgInfo.MessageChildType;
	messageListInfo.nUnReadNum = 0;
	messageListInfo.isGroup = 0;
	gDataBaseOpera->DBInsertPerMessageListInfo(messageListInfo);

	//更新左侧消息列表内容
	QMap<QString, QVariant> params;
	emit m_dispatcher->sigUpdateSelfMessage(false, QVariant::fromValue(buddy), messageListInfo.strLastMessage, true, params);
}

void IMPerChatStore::slotSendSecretFile(QString message)
{
	QString& letter = message;

	UserInfo userinfo = gDataManager->getUserInfo();

	MessageInfo msgInfo = gSocketMessage->SendTextMessage(userinfo.nUserID, objectName().toInt(), 0, (short)Message_SECRETFILE, letter);

	slotProcessSendMessageInfo(userinfo.strUserAvatarLocal, msgInfo);

	//根据消息类型设置显示的内容。
	msgInfo.strMsg = tr("[Secret File]");

	MessageListInfo messageListInfo;
	messageListInfo.nBudyyID = this->objectName().toInt();
	messageListInfo.strLastMessage = msgInfo.strMsg;
	messageListInfo.nLastTime = msgInfo.ClientTime;
	messageListInfo.strBuddyName = m_vm->getNickName();
	BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(this->objectName());
	if (buddy.strNickName.isEmpty())
		buddy = gDataBaseOpera->DBGetGroupUserFromID(this->objectName());
	messageListInfo.strBuddyHeaderImage = buddy.strLocalAvatar;
	messageListInfo.messageType = msgInfo.MessageChildType;
	messageListInfo.nUnReadNum = 0;
	messageListInfo.isGroup = 0;
	gDataBaseOpera->DBInsertPerMessageListInfo(messageListInfo);

	//更新左侧消息列表内容
	QMap<QString, QVariant> params;
	emit m_dispatcher->sigUpdateSelfMessage(false, QVariant::fromValue(buddy), messageListInfo.strLastMessage, true, params);
}

void IMPerChatStore::slotSendNotice(QMap<QString, QString> mapData)
{
	UserInfo userInfo = gDataManager->getUserInfo();
	QString strPath = convertHeaderToHtmlSrc(userInfo.strUserAvatarLocal);

	QString imageUrl = mapData.value("imageUrl");
	QString imageTitle = mapData.value("imageTitle");
	QString webUrl = mapData.value("webUrl");
	QString webTitle = mapData.value("webTitle");
	int nUserID = userInfo.nUserID;
	if (m_vm->getBuddyId().isEmpty())
	{
		return;
	}

	QFile file(imageUrl);
	if (!file.exists())
	{
		QJsonParseError jsonError;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(imageUrl.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QVariantMap result = jsonDocument.toVariant().toMap();
				qDebug() << "img:" << result["path"].toString();
				imageUrl = result["path"].toString();
			}
		}
	}

	QString toUserID = m_vm->getBuddyId();
	AppConfig configInfo = gDataManager->getAppConfigInfo();
	MessageInfo messageInfo = gSocketMessage->SendNoteiceMessage(nUserID, toUserID.toInt(), 0, "");

	HttpNetWork::HttpUpLoadFile *httpUpLoad = new HttpNetWork::HttpUpLoadFile(this);
	mapData.insert("imageUrl", configInfo.PanServerDownloadURL);

	QString processID = messageInfo.msgID;
	mapData.insert("MsgId", processID);
	httpUpLoad->setData(QVariant::fromValue(mapData));

	connect(httpUpLoad, SIGNAL(sigUpLoadFinished(bool, QByteArray)), this, SLOT(slotUpNoticePicReplyFinished(bool, QByteArray)));
	QVariantMap pargram;
	pargram.insert("parentId", "66662");
	pargram.insert("createUser", "6662");
	httpUpLoad->StartUpLoadFile(configInfo.PanServerUploadURL, imageUrl, pargram);

#ifndef Q_OS_WIN
	imageUrl = QString("file:///") + imageUrl;
#endif
	//插入时间线。
	slotInsertTimeLine(GETLOCALTIME);
	QString strSend = QString("SendNotice(\"%1\",\"%2\",\"%3\",\"%4\",\"%5\",\"%6\",%7,%8,\"%9\");")
		.arg(strPath).arg(imageUrl).arg(imageTitle).arg(webUrl).arg(webTitle).arg(QString(messageInfo.msgID))
		.arg(messageInfo.MessageState).arg(messageInfo.integral).arg(QString::number(messageInfo.nFromUserID));
	m_vm->runJs(strSend);

	MessageListInfo messageListInfo;
	messageListInfo.nBudyyID = this->objectName().toInt();
	messageListInfo.strLastMessage = tr("[Announcement]");
	messageListInfo.nLastTime = QDateTime::currentMSecsSinceEpoch();
	messageListInfo.strBuddyName = m_vm->getNickName();
	BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(this->objectName());
	if (buddy.strNickName.isEmpty())
		buddy = gDataBaseOpera->DBGetGroupUserFromID(this->objectName());
	messageListInfo.strBuddyHeaderImage = buddy.strLocalAvatar;
	messageListInfo.messageType = MessageType::Message_NOTICE;
	messageListInfo.nUnReadNum = 0;
	messageListInfo.isGroup = 0;
	gDataBaseOpera->DBInsertPerMessageListInfo(messageListInfo);
	//更新至消息列表，false代表个人消息
	QMap<QString, QVariant> params;
	params["SendStatus"] = MESSAGE_STATE_PRESEND;
	params["MsgId"] = QString(messageInfo.msgID);
	emit m_dispatcher->sigUpdateSelfMessage(false, QVariant::fromValue(buddy), messageListInfo.strLastMessage, true, params);
}

/*发送图片 上传完图片 结果*/
void IMPerChatStore::slotUpNoticePicReplyFinished(bool result, QByteArray strResult)
{
	HttpNetWork::HttpUpLoadFile *act = qobject_cast<HttpNetWork::HttpUpLoadFile*>(sender());
	QVariant var = act->getData();
	QMap<QString, QString> mapData = var.value<QMap<QString, QString>>();
	MessageInfo msgInfo = gSocketMessage->DBGetMessageInfoWithMsgID(mapData.value("MsgId"));

	if (result)
	{
		gSocketMessage->setUpNoticePicReplyFinished(strResult, mapData, msgInfo);

		MessageListInfo messageListInfo;
		messageListInfo.nBudyyID = this->objectName().toInt();
		messageListInfo.strLastMessage = tr("[Announcement]");
		messageListInfo.nLastTime = msgInfo.ClientTime;
		messageListInfo.strBuddyName = m_vm->getNickName();
		BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(this->objectName());
		if (buddy.strNickName.isEmpty())
			buddy = gDataBaseOpera->DBGetGroupUserFromID(this->objectName());
		messageListInfo.strBuddyHeaderImage = buddy.strLocalAvatar;
		messageListInfo.messageType = msgInfo.MessageChildType;
		messageListInfo.nUnReadNum = 0;
		messageListInfo.isGroup = 0;
		gDataBaseOpera->DBInsertPerMessageListInfo(messageListInfo);
		//更新至消息列表，false代表个人消息
		QMap<QString, QVariant> params;
		params["SendStatus"] = MESSAGE_STATE_SEND;
		params["MsgId"] = QString(msgInfo.msgID);
		emit m_dispatcher->sigUpdateSelfMessage(false, QVariant::fromValue(buddy), messageListInfo.strLastMessage, true, params);
	}
	else
	{
		qDebug() << "严重 ！！！ 发送失败标志出现 ： slotUpNoticePicReplyFinished()";
		this->UpdateMessageStateInfo(msgInfo.msgID, MESSAGE_STATE_FAILURE, msgInfo.integral);
	}
}

void IMPerChatStore::slotZoomImg(QString strId)
{
	MessageInfo msgInfo = gSocketMessage->DBGetMessageInfoWithMsgID(strId);
	if (msgInfo.MessageState == MESSAGE_STATE_UNLOADED || msgInfo.MessageChildType != Message_PIC)
	{
		return;
	}
	QFile file(msgInfo.strMsg);
	if (!file.exists())
	{
		QJsonParseError jsonError;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QVariantMap result = jsonDocument.toVariant().toMap();
				msgInfo.strMsg = result["path"].toString();
			}
		}
	}
	emit m_dispatcher->sigOpenPic(msgInfo.strMsg, &m_listPic, m_view);
}

void IMPerChatStore::slotSaveFile(QString msgID)
{
	const QString DEFAULT_DIR_KEY("default_dir");
	QSettings MySettings;
	QString default_dir = MySettings.value(DEFAULT_DIR_KEY).toString();

	MessageInfo messageInfo = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);
	QJsonDocument jsonDocument = QJsonDocument::fromJson(messageInfo.strMsg.toUtf8());
	QVariantMap result = jsonDocument.toVariant().toMap();
	QString fileName = result["FileName"].toString();
	QString fileID = result["FileId"].toString();
	QString fileExt = result["FileType"].toString();

	QString strFilePath = QFileDialog::getSaveFileName(m_view, tr("Save as"), (default_dir.isEmpty() ? "" : default_dir + "/") + fileName);
	if (strFilePath == "")
	{
		return;
	}

	if (QFileInfo(strFilePath).suffix().toLower() != fileExt.toLower())
	{
		//无后缀，自动添加后缀
		strFilePath += "." + fileExt;
	}

	QFileInfo fileInfo(strFilePath);
	MySettings.setValue(DEFAULT_DIR_KEY, fileInfo.path());

	QString strUlID2 = QString(msgID) + QString("recv2");
	QString strUlID = QString(msgID) + QString("recv");
	QString coverFile = QString("coverFile(\"%1\")").arg(strUlID);

	m_vm->runJs(coverFile);

	AppConfig configInfo = gDataManager->getAppConfigInfo();
	QString httpUrl = QString(configInfo.PanServerDownloadURL) + fileID + QString("/download");

	HttpNetWork::HttpDownLoadFile *netWork = new HttpNetWork::HttpDownLoadFile;
	m_iHttp++;
	netWork->setData(QVariant::fromValue(messageInfo));
	netWork->setObjectName(strFilePath);
	connect(netWork, SIGNAL(sigDownFinished(bool)), this, SLOT(slotRequestHttpFileResult(bool)));
	connect(netWork, SIGNAL(sigDownFailed()), this, SLOT(slotDownFailed()));
	connect(netWork, SIGNAL(sigDownloadProgress(qint64, qint64)), this, SLOT(slotDownLoadFileProgress(qint64, qint64)));

	connect(m_dispatcher, SIGNAL(sigCancel(QString)), netWork, SLOT(slotLoadorDownLoadCancle(QString)));
	netWork->StartDownLoadFile(httpUrl, strFilePath);
}

void IMPerChatStore::slotDrags(QStringList list)
{
	foreach(QString file, list)
	{
		file = file.replace("\\", "/");
		this->slotSendFile(file);
	}
}

void IMPerChatStore::clearHtml()
{
	m_vm->runJs("clear()");
}

void IMPerChatStore::InitMessageInfo(QString strEndRowId/* = ""*/)
{
	//strEndRowId有值时，说明当前点击的是获取更多消息

	if (!strEndRowId.isEmpty())
	{
		m_vm->runJs("BeginShowMore()");
		m_foremostRowId = "";
		m_isShowingMore = true;
	}

	QString	strUserID = this->objectName();

	if (strEndRowId.isEmpty())
	{
		//清空
		clearHtml();
	}

	//获取好友头像
	UserInfo userInfo = gDataManager->getUserInfo();
	QString strUserHeadImage = userInfo.strUserAvatarLocal;

	QMap<QString, QList<MessageInfo> > mapTemp = gSocketMessage->DBGetRecnetMessage(userInfo.nUserID, strUserID.toInt(), strEndRowId);

	QMap<QString, QList<MessageInfo> >::iterator itor = mapTemp.begin();
	for (; itor != mapTemp.end(); ++itor)
	{
		if (itor.key() == strUserID)
		{
			QList<MessageInfo> msgList = itor.value();

			if (msgList.count() >= (strEndRowId.isEmpty() ? DB_MESSAGEINFO_INIT_MAX_NUM : DB_MESSAGEINFO_FETCH_MAX_NUM))
			{
				m_vm->runJs("AddShowMore()");
			}

			if (msgList.size() > 0)
			{
				m_foremostRowId = msgList[0].strRowId;
			}

			for (int i = 0; i < msgList.size(); i++)
			{
				if (msgList[i].MessageState == 7) {//将数据库中未读消息更新为已读
					gSocketMessage->SetMessageState(msgList[i].msgID, 6);
				}

				if (msgList[i].nFromUserID == userInfo.nUserID)
				{
					//说明我是发送方
					slotProcessSendMessageInfo(strUserHeadImage, msgList[i]);
				}
				else
				{
					MessageInfo message = msgList[i];

					//插入时间线。
					slotInsertTimeLine(message.ServerTime);

					BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(QString::number(message.nFromUserID));
					if (buddy.strNickName.isEmpty())
						buddy = gDataBaseOpera->DBGetGroupUserFromID(QString::number(message.nFromUserID));
					slotProcessRecvMessageInfo(message, buddy);
				}
			}
		}
	}

	initFinished = true;
	while (!waitMessages.isEmpty())
	{
		MessageInfo msg = waitMessages.first();
		slotSendTransmitMessage(msg);
		waitMessages.removeFirst();
	}

	m_vm->setTextEditSendFocus();

	if (!strEndRowId.isEmpty())
	{
		m_vm->runJs("EndShowMore()");
		m_isShowingMore = false;
	}
}

void IMPerChatStore::slotLoadMore()
{
	InitMessageInfo(this->m_foremostRowId);
}

void IMPerChatStore::slotWebEngineFinish(bool ok)
{
	if (ok)
	{
		//切换合适的语言
		m_vm->runJs(QString("switchI18NLocale('%1')").arg(gI18NLocale));

		//切换主题样式
		if (gThemeStyle == "White")
		{
			m_vm->runJs("changeStyle('dayStyle')");
		}
		else if (gThemeStyle == "Blue")
		{
			m_vm->runJs("changeStyle('nightStyle')");
		}
		

		InitMessageInfo();
	}
}

bool IMPerChatStore::IsDownloading()
{
	if (m_iHttp > 0)
	{
		return true;
	}
	return false;
}
