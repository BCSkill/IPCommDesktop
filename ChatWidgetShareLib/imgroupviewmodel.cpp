﻿#include "imgroupviewmodel.h"
#include "imgroupview.h"


IMGroupViewModel::IMGroupViewModel(QObject *parent) : QObject(parent)
{
	
}

void IMGroupViewModel::init()
{
	m_view = reinterpret_cast<IMGroupView*>(parent());
}

void IMGroupViewModel::setGroupNameAndGroupId(QString strGroupName, QString strGroupID)
{
	m_view->setGroupNameAndGroupId(strGroupName, strGroupID);
}