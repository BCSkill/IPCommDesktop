﻿#include "qwebengineviewdelegate.h"
#include "ui_qwebengineviewdelegate.h"
#include <QTimer>
#include <QResizeEvent>
#include "chatdatamanager.h"

extern QString gThemeStyle;

QWebEngineViewDelegate::QWebEngineViewDelegate(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::QWebEngineViewDelegate), m_webEngineView(NULL), m_webenginePageDelegate(NULL)
{
    ui->setupUi(this);

	//切换主题样式
	if (gThemeStyle == "White")
	{
		this->setStyleSheet("background-color:#ffffff;");
	}
	else if (gThemeStyle == "Blue")
	{
		this->setStyleSheet("background-color:#042439;");
	}
}

QWebEngineViewDelegate::~QWebEngineViewDelegate()
{
	if (m_webEngineView && ((QWebEngineViewDelegate *)(m_webEngineView->parent())) == this)
	{
		m_webEngineView->setParent(NULL);//取消父子关系，避免被删除，m_webEngineView由webenginemanager进行管理删除
	}
	

    delete ui;

	unbindChatId();
	if (m_webenginePageDelegate)
	{
		delete m_webenginePageDelegate;
		m_webenginePageDelegate = NULL;
	}
}

void QWebEngineViewDelegate::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);

	QWidget::paintEvent(event);
}


void QWebEngineViewDelegate::resizeEvent(QResizeEvent *event)
{
	if (m_webEngineView && ((QWebEngineViewDelegate *)(m_webEngineView->parent())) == this)
	{
		m_webEngineView->resize(event->size().width(), event->size().height());
	}
	
}

QString QWebEngineViewDelegate::chatId()
{
	return m_currentChatId;
}

void QWebEngineViewDelegate::adjustWebEngineView()
{
	m_webEngineView->setParent(this);
	m_webEngineView->move(0, 0);
	m_webEngineView->resize(this->width(), this->height());

	m_webEngineView->show();
}

void QWebEngineViewDelegate::switchChatIFrame(bool bAdjustView)
{
	if (m_currentChatId != ChatDataManager::self()->m_currentSelectedChatId)
	{
		return;
	}

	if (QWebEngineViewManager::instance()->m_settingFlagsMap[m_webEngineView->page()].isLoadChatManagerFinished)
	{
		qDebug() << "SwitchChatIFrame:" << m_currentChatId;
		m_webEngineView->page()->runJavaScript(QString("SwitchChatIFrame(\"%1\")").arg(m_currentChatId));
	}
	else
	{
		
	}

	if (bAdjustView)
	{
		adjustWebEngineView();
	}
	
}


void QWebEngineViewDelegate::hideAllIFrames()
{
	m_webEngineView->page()->runJavaScript(QString("HideAllIFrames()"));
}

void QWebEngineViewDelegate::addChatIFrame(const QString& chatId)
{
	m_webEngineView->page()->runJavaScript(QString("AddChatIFrame(\"%1\")").arg(chatId));
}

void QWebEngineViewDelegate::chatIFramePresent()
{
	hideAllIFrames();
	adjustWebEngineView();

	addChatIFrame(m_currentChatId);
}

void QWebEngineViewDelegate::delayChatIFramePresent()
{
	if (QWebEngineViewManager::instance()->m_settingFlagsMap[m_webEngineView->page()].isLoadChatManagerFinished)
	{
		chatIFramePresent();
	}
	else
	{
		QTimer::singleShot(30, this, [=] {
			delayChatIFramePresent();
		});
	}

}

void QWebEngineViewDelegate::slotLoadChatManagerFinished(bool ok)
{
	if (ok)
	{
		QWebEngineViewManager::instance()->m_settingFlagsMap[m_webEngineView->page()].isLoadChatManagerFinished = true;

		chatIFramePresent();
	}
	else
	{
		//加载失败
	}
	
}


void QWebEngineViewDelegate::slotOnChatIFrameLoad(QString chatId)
{
	if (m_currentChatId == chatId)
	{
		//注意：必须等iframe里的文件加载完毕才能触发loadFinished信号
		qDebug() << "slotOnChatIFrameLoad:" << chatId;

		switchChatIFrame();
		
		QWebEngineViewManager::instance()->m_settingFlagsMap[m_webEngineView->page()].isChatIFrameLoadFinished[m_currentChatId] = true;

		emit loadFinished(true);
	}
	else
	{
		//多IFRAME共用一个webengine时会走到这里
		
	}
	
}


QWebEngineView*  QWebEngineViewDelegate::bindChatId(const QString& chatId)
{
	if (m_currentChatId.isEmpty())
	{
		m_currentChatId = chatId;
		m_webEngineView = QWebEngineViewManager::instance()->addChatWindow(chatId);

		QObject::connect(m_webEngineView, SIGNAL(renderProcessTerminated(QWebEnginePage::RenderProcessTerminationStatus, int)), this, SIGNAL(renderProcessTerminated(QWebEnginePage::RenderProcessTerminationStatus, int)));

		//如果m_webEngineView是第一次创建，则执行相应的初始化操作
		if (!QWebEngineViewManager::instance()->m_settingFlagsMap[m_webEngineView->page()].isLoadChatManager)
		{
			QObject::connect(m_webEngineView, SIGNAL(loadFinished(bool)), this, SLOT(slotLoadChatManagerFinished(bool)));
		}
		else
		{
			if (QWebEngineViewManager::instance()->m_settingFlagsMap[m_webEngineView->page()].isLoadChatManagerFinished)
			{
				chatIFramePresent();//ChatManager.html正常情况下到这里时应该是提前加载完毕了
			}
			else
			{
				qDebug() << "ChatManager.html has not finished load!";
				delayChatIFramePresent();
			}
			
		}
		
	}
	else
	{
		qFatal("QWebEngineViewDelegate: cannot bindChatId twice!");//暂时限制只能绑定一次
	}

	return m_webEngineView;
}

void QWebEngineViewDelegate::unbindChatId()
{
	QWebEngineViewManager::instance()->removeChatWindow(m_currentChatId);

	if (m_webEngineView)
	{
		m_webEngineView->page()->runJavaScript(QString("RemoveChatIFrame(\"%1\")").arg(m_currentChatId));
	}
	
}

void QWebEngineViewDelegate::loadChatManager()
{
	if (!QWebEngineViewManager::instance()->m_settingFlagsMap[m_webEngineView->page()].isLoadChatManager)
	{
		QWebEngineViewManager::instance()->m_settingFlagsMap[m_webEngineView->page()].isLoadChatManager = true;
		m_webEngineView->page()->load(QUrl("qrc:/html/Resources/html/chatManager.html"));

		adjustWebEngineView();
	}
	
}

QWebEngineView* QWebEngineViewDelegate::engine()
{
	return m_webEngineView;
}

QWebEnginePageDelegate* QWebEngineViewDelegate::page()
{
	if (m_webenginePageDelegate == NULL)
	{
		m_webenginePageDelegate = new QWebEnginePageDelegate(m_webEngineView->page(), m_currentChatId);
	}

	return m_webenginePageDelegate;
}

void QWebEngineViewDelegate::triggerPageAction(QWebEnginePage::WebAction action, bool checked /*= false*/)
{
	m_webEngineView->triggerPageAction(action,checked);
}