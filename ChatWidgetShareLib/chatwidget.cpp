﻿#include "chatwidget.h"
#include "ui_chatwidget.h"
#include "QStringLiteralBak.h"

#ifdef Q_OS_MAC
#include "mycommonstyle.h"
#endif

ChatWidget::ChatWidget(QWidget *parent)
	: QSplitter(parent)
{
	ui = new Ui::ChatWidget();
	ui->setupUi(this);

	const int defaultLeftWidth = 280;
	const int handleWidth = 2;//splitter控件的handle宽度
	QList<int> widthList;
	widthList.append(defaultLeftWidth);
	widthList.append(900 - handleWidth - 4 - defaultLeftWidth);//这里的900为聊天窗口加载完后的最终大小 IMMainWidget->resize(900, 660); 减4是手动调整测试的，原因不明
	this->setSizes(widthList);

	this->setHandleWidth(1);
	
	//ui->separateLine->installEventFilter(this);
	ui->searchLineEdit->installEventFilter(this);
	ui->deviceWidget->installEventFilter(this);

#ifdef Q_OS_MAC
    ui->searchLineEdit->setStyle(new MyProxyStyle);
#endif

	QFile file(":/QSS/Resources/QSS/ChatWidgetShareLib/chatwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->searchLineEdit, SIGNAL(textEdited(QString)), this, SLOT(slotEditSearchLine(QString)));
	connect(ui->searchBtn, SIGNAL(clicked(bool)), this, SLOT(slotClickCancelSearch()));

	ui->searchList->hide();
	ui->deviceWidget->hide();
}

ChatWidget::~ChatWidget()
{
	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

MessageListView * ChatWidget::getMessageListView()
{
	return ui->messageWidget;
}

QStackedWidget * ChatWidget::getStackedWidget()
{
	return ui->chatStackedWidget;
}


void ChatWidget::slotMinSizeWidget()
{
	if (this->windowState() != Qt::WindowMinimized)
	{
		setWindowState(Qt::WindowMinimized);
	}
}

bool ChatWidget::eventFilter(QObject *obj, QEvent *e)
{
	/*if (obj == ui->separateLine)
	{
		if (e->type() == QEvent::MouseMove)
		{
            QMouseEvent *event = (QMouseEvent *)e;
            int w = ui->leftWidget->width() + event->x();
			if (w > 350)
				w = 350;
			if (w < 150)
				w = 150;
			ui->leftWidget->setFixedWidth(w);
            ui->messageWidget->setFixedWidth(w);
		}
	}*/
	if (obj == ui->deviceWidget && e->type() == QEvent::MouseButtonPress)
	{
		emit sigShowDeviceWidget();
	}

	if (obj == ui->searchLineEdit && e->type() == QEvent::KeyPress)
	{
		QKeyEvent *keyEvent = (QKeyEvent *)e;
		if (keyEvent->key() == Qt::Key_Up || keyEvent->key() == Qt::Key_Down || keyEvent->key() == Qt::Key_Return)
		{
			ui->searchList->onChangeItem(keyEvent);
		}
	}

	if (obj == ui->searchLineEdit && e->type() == QEvent::MouseButtonPress)
	{
		ui->searchLineEdit->selectAll();
		return true;
	}

	return QSplitter::eventFilter(obj, e);
}

SearchList * ChatWidget::getSearchList()
{
	return ui->searchList;
}

void ChatWidget::slotEditSearchLine(QString text)
{
	if (text.isEmpty())
	{
		ui->searchList->hide();
		ui->messageWidget->show();
	}
	else
	{
		if (ui->searchList->isHidden())
		{
			ui->messageWidget->hide();
			ui->searchList->show();
		}
		emit sigSearchText(text);
	}
}

void ChatWidget::slotClickCancelSearch()
{
	ui->searchLineEdit->clear();

	ui->searchList->hide();
	ui->messageWidget->show();
}

void ChatWidget::setOtherDeviceCount(int count)
{
	if (count <= 0)
		ui->deviceWidget->hide();
	else
	{
		QString text = tr("Account logged in on other ") + QString::number(count) + tr(" device(s)");
		ui->deviceLabel->setText(text);
		ui->deviceWidget->show();
	}
}