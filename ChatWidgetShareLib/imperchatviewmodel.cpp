﻿#include "imperchatviewmodel.h"
#include "imperchatview.h"
#include "qwebengineviewdelegate.h"
#include "childwidget/expresswidget.h"
#include "chatdatamanager.h"
#include "immainwidget.h"
#include <QTextImageFormat>
#include <QTextEdit>
#include <QTextBlock>


IMPerChatViewModel::IMPerChatViewModel(QObject *parent) : QObject(parent)
{
	m_view = reinterpret_cast<IMPerChatView*>(parent);
}

void IMPerChatViewModel::init()
{

}


void IMPerChatViewModel::runJs(const QString &scriptSource)
{
	m_view->getWebView()->page()->runJavaScript(scriptSource);
}

void IMPerChatViewModel::setTextEditSendFocus()
{
	m_view->getTextEditSend()->setFocus();
}

QString IMPerChatViewModel::getImagePathByDescription(QString strImgDescription)
{
	return ExpressWidget::GetImagePathByDescription(strImgDescription);
}

void IMPerChatViewModel::setNickNameAndBuddyId(QString strText, QString strBuddyID)
{
	m_strNickName = strText;
	m_strBuddyId = strBuddyID;

	emit sigSetNickNameAndBuddyId(strText, strBuddyID);
}

QString IMPerChatViewModel::getNickName()
{
	return m_strNickName;
}

QString IMPerChatViewModel::getBuddyId()
{
	return m_strBuddyId;
}

void IMPerChatViewModel::showByChatId(bool isNew)
{
	if (isNew)
	{
		//由于webengineview在不可见状态时并不渲染，导致残留上一个页面影响，所以此处对于新打开的页面改用延迟渲染
		if (!ChatDataManager::self()->m_isFirstShowChat)
		{
			QWebEngineView* view = m_view->getWebView()->engine();
			view->setParent(IMMainWidget::self());

			view->resize(1, 1);
			view->move(0, 0);
			view->raise();
			view->show();
			m_view->getWebView()->switchChatIFrame(false);
		}


		QTimer::singleShot(800, this, [=] {m_view->getWebView()->switchChatIFrame(); });
	}
	else
	{
		m_view->getWebView()->switchChatIFrame();
	}

	ChatDataManager::self()->m_isFirstShowChat = false;
}


void IMPerChatViewModel::insertImageToTextEditSend(QString strPath)
{
	QFile file(strPath);
	if (file.open(QIODevice::ReadOnly))
	{
		QByteArray byteArray = file.readAll();
		file.close();
		QImage image = QImage::fromData(byteArray);
		if (!image.isNull())
		{
			QTextImageFormat imageFormat;
			if (image.width() > 100 || image.height() > 100)
			{
				if (image.width() > image.height())
				{
					imageFormat.setWidth(100);
				}
				else
				{
					imageFormat.setHeight(100);
				}
			}

			imageFormat.setName(strPath);

			m_view->getTextEditSend()->textCursor().insertImage(imageFormat);
		}
	}
}


void IMPerChatViewModel::startInputting()
{
	m_view->startInputting();
}

void IMPerChatViewModel::endInputting()
{
	m_view->endInputting();
}

QString IMPerChatViewModel::getDraft()
{
	QTextEdit* textEdit = m_view->getTextEditSend();

	QString saveText = "";
	QString strText = textEdit->toPlainText();
	if (!strText.isEmpty())
	{
		QTextDocument *document = textEdit->document();
#ifdef Q_OS_WIN
		QTextBlock &currentBlock = document->begin();
#else
		QTextBlock currentBlock = document->begin();
#endif
		QTextBlock::iterator it;

		while (true)
		{
			for (it = currentBlock.begin(); !(it.atEnd());)
			{
				QTextFragment currentFragment = it.fragment();
				QTextImageFormat newImageFormat = currentFragment.charFormat().toImageFormat();
				if (newImageFormat.isValid()) {
					// 判断出这个fragment为image
					++it;

					//先判断是不是表情。
					QString name = newImageFormat.name();
					if (name.startsWith(":/expression/Resources"))   //表情。
					{
						int len = currentFragment.length();
						for (int i = 0; i < len; i++)//相同的多个表情会导致len>1，原因不明
						{
							QString strImgPath = name.remove(":/expression/Resources");
							QString str_img_description = ExpressWidget::GetDescriptionByImagePath(strImgPath);
							saveText += str_img_description;
						}
					}
					else
					{
						saveText += tr("[Image]");
					}
					continue;
				}
				if (currentFragment.isValid())
				{
					++it;
					QString text = currentFragment.text();
					saveText += text;
				}
			}

			currentBlock = currentBlock.next();
			if (!currentBlock.isValid())
			{
				break;
			}
		}
	}
	return saveText;
}

