﻿#include "qwebengineviewmanager.h"
#include "qwebengineviewdelegate.h"
#include <QObject>
#include <QTimer>


QWebEngineViewManager::QWebEngineViewManager()
{

}

QWebEngineViewManager::~QWebEngineViewManager()
{

}


QWebEngineViewManager* QWebEngineViewManager::instance()
{
	static QWebEngineViewManager m_instance;

	return &m_instance;
}

QWebEngineView* QWebEngineViewManager::generateWebEngineView()
{
	qDebug() << "QWebEngineViewManager::generateWebEngineView()";

	QWebEngineView* engine = new QWebEngineView();
	engine->setAcceptDrops(false);//禁止拖放文件
	engine->setContextMenuPolicy(Qt::NoContextMenu);

	connect(engine, SIGNAL(renderProcessTerminated(QWebEnginePage::RenderProcessTerminationStatus, int)), this, SLOT(slotWebViewRenderProcessTerminated(QWebEnginePage::RenderProcessTerminationStatus, int)));

	return engine;
}

QWebEngineView* QWebEngineViewManager::addChatWindow(const QString& chatId)
{
	qDebug() << " QWebEngineViewManager::addChatWindow:"<< chatId;

	if (m_engineStuVec.size() < MAX_ALLOWED_QTWEBENGINEPROCESS_NUM)
	{
		webEngineStu stu;
		stu.engine = generateWebEngineView();

		m_engineStuVec.push_back(stu);
	}

	qSort(m_engineStuVec.begin(), m_engineStuVec.end(), [](const webEngineStu &infoA, const webEngineStu &infoB) {return infoA.chatIds.size() < infoB.chatIds.size(); });

	m_engineStuVec[0].chatIds.insert(chatId);


	return m_engineStuVec[0].engine;

}

void QWebEngineViewManager::removeWebEngineView(int i)
{
	m_settingFlagsMap.remove(m_engineStuVec[i].engine->page());

	//没有相应的iframe了，释放qwebengineview,关闭QtWebEngineProcess.exe
	m_engineStuVec[i].engine->deleteLater();
	m_engineStuVec[i].engine = NULL;

	m_engineStuVec.remove(i);
}

void QWebEngineViewManager::removeChatWindow(const QString& chatId)
{
	for (int i = 0; i < m_engineStuVec.size(); i++)
	{
		if (m_engineStuVec[i].chatIds.contains(chatId))
		{
			m_engineStuVec[i].chatIds.remove(chatId);

			m_settingFlagsMap[m_engineStuVec[i].engine->page()].isChatIFrameLoadFinished[chatId] = false;

			if (m_engineStuVec[i].chatIds.isEmpty())
			{
				removeWebEngineView(i);
			}

			break;
		}
	}
}

void QWebEngineViewManager::slotWebViewRenderProcessTerminated(QWebEnginePage::RenderProcessTerminationStatus terminationStatus, int exitCode)
{
	if (terminationStatus != QWebEnginePage::NormalTerminationStatus)
	{
		//webengine进程异常退出
		QWebEngineView *webEngineView = qobject_cast<QWebEngineView*>(sender());

		for (int i = 0; i < m_engineStuVec.size(); i++)
		{
			if (m_engineStuVec[i].engine == webEngineView)
			{
				QTimer::singleShot(0, this, [=] {
					//直接调用会报奇怪错误，所以放在QTimer中。
					//尽快释放相关变量（尤其是m_engineStuVec），以防止进程崩溃后信号触发顺序问题导致的可能未有web进程创建的问题
					removeWebEngineView(i);
				});
				
				break;
			}
		}
	}
}