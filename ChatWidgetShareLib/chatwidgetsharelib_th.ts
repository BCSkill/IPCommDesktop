<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="th_TH">
<context>
    <name>AtWidget</name>
    <message>
        <location filename="SearchWidget/atwidget.ui" line="14"/>
        <source>AtWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="SearchWidget/atwidget.cpp" line="248"/>
        <source>@</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ChatDataManager</name>
    <message>
        <location filename="chatdatamanager.cpp" line="760"/>
        <location filename="chatdatamanager.cpp" line="824"/>
        <location filename="chatdatamanager.cpp" line="855"/>
        <source>Warning</source>
        <translation>การเตือน</translation>
    </message>
    <message>
        <location filename="chatdatamanager.cpp" line="760"/>
        <location filename="chatdatamanager.cpp" line="824"/>
        <source>There are files being transferred，contuinue to close ?</source>
        <translation>มีไฟล์ที่กำลังถ่ายโอนปิดต่อไปหรือไม่</translation>
    </message>
    <message>
        <location filename="chatdatamanager.cpp" line="855"/>
        <source>There are files being transferred，contuinue to quit ?</source>
        <translation>มีไฟล์ที่กำลังถ่ายโอนอยู่ต้องการออกจากต่อไปหรือไม่</translation>
    </message>
    <message>
        <location filename="chatdatamanager.cpp" line="1319"/>
        <source>The other party has successfully received your file</source>
        <translation>บุคคลอื่นได้รับไฟล์ของคุณเรียบร้อยแล้ว</translation>
    </message>
</context>
<context>
    <name>ChatWidget</name>
    <message>
        <location filename="chatwidget.ui" line="14"/>
        <source>ChatWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="chatwidget.ui" line="117"/>
        <location filename="chatwidget.ui" line="162"/>
        <location filename="chatwidget.ui" line="190"/>
        <source>Search</source>
        <translation>ค้นหา</translation>
    </message>
    <message>
        <location filename="chatwidget.ui" line="215"/>
        <source>View device list</source>
        <translation>ดูรายการอุปกรณ์</translation>
    </message>
    <message>
        <location filename="chatwidget.ui" line="272"/>
        <source>当前账户在其他1台设备登录</source>
        <translation></translation>
    </message>
    <message>
        <location filename="chatwidget.cpp" line="149"/>
        <source>Account logged in on other </source>
        <translation>บัญชีปัจจุบันอยู่ในอื่น ๆ</translation>
    </message>
    <message>
        <location filename="chatwidget.cpp" line="149"/>
        <source> device(s)</source>
        <translation>เข้าสู่ระบบอุปกรณ์</translation>
    </message>
</context>
<context>
    <name>ChooseUnitWidget</name>
    <message>
        <location filename="redPacketWidget/ChooseUnitWidget.ui" line="26"/>
        <source>ChooseUnitWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/ChooseUnitWidget.ui" line="77"/>
        <source>Choose red packet token</source>
        <translation>เลือกโทเค็นซองจดหมายสีแดง</translation>
    </message>
    <message>
        <location filename="redPacketWidget/ChooseUnitWidget.ui" line="195"/>
        <source>OK</source>
        <translation>ตกลง</translation>
    </message>
</context>
<context>
    <name>EnterPasswordWidget</name>
    <message>
        <location filename="secretWidget/enterpasswordwidget.ui" line="14"/>
        <source>EnterPasswordWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.ui" line="82"/>
        <source>Please enter password of the base:</source>
        <translation>กรุณาใส่รหัสผ่านของฐาน</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.ui" line="101"/>
        <source>Base password is used to decrypt the password of the message</source>
        <translation>รหัสผ่านพื้นฐานใช้ในการถอดรหัสรหัสผ่านของข้อความ</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.ui" line="126"/>
        <source>Please enter the base password</source>
        <translation>กรุณาใส่รหัสผ่านฐาน</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.ui" line="169"/>
        <source>OK</source>
        <translation>ตกลง</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="64"/>
        <source>Please enter the password</source>
        <translation>กรุณาใส่รหัสผ่าน</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="65"/>
        <source>Enter the password to view the content</source>
        <translation>ป้อนรหัสผ่านเพื่อดูเนื้อหา</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="144"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="149"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="159"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="171"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="178"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="202"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="209"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="242"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="251"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="319"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="144"/>
        <source>Decryption failure！Error code 113</source>
        <translation>การถอดรหัสล้มเหลวรหัสข้อผิดพลาด 113</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="149"/>
        <source>Password cannot be empty！</source>
        <translation>รหัสผ่านต้องไม่ว่างเปล่า</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="159"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="251"/>
        <source>Incorrect password！</source>
        <translation>รหัสผ่านผิดพลาด</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="171"/>
        <source>Decryption failure！Error code 137</source>
        <translation>การถอดรหัสล้มเหลวรหัสข้อผิดพลาด 137</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="178"/>
        <source>Decryption failure！Error code 144</source>
        <translation>การถอดรหัสล้มเหลวรหัสข้อผิดพลาด 144</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="202"/>
        <source>Please observe the surrounding environment before viewing the password to avoid leaking your password.
The password is only displayed once, please be sure to save the password</source>
        <translation>โปรดสังเกตสภาพแวดล้อมโดยรอบก่อนที่จะดูรหัสผ่านเพื่อหลีกเลี่ยงการรั่วไหลของรหัสผ่านของคุณ
รหัสผ่านจะแสดงเพียงครั้งเดียวโปรดแน่ใจว่าได้บันทึกรหัสผ่าน</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="209"/>
        <source>The password cannot be empty！</source>
        <translation>รหัสผ่านต้องไม่ว่างเปล่า</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="242"/>
        <source>The secret picture/file is empty，please download again！</source>
        <translation>รูปภาพ / ไฟล์ลับว่างเปล่าโปรดดาวน์โหลดอีกครั้ง</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="319"/>
        <source>Fail to download the secret picture/file!</source>
        <translation>ดาวน์โหลดรูปภาพ / ไฟล์ลับไม่สำเร็จ</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="329"/>
        <source>The password is:&quot;</source>
        <translation>The password is </translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="329"/>
        <source>&quot;,Please remember the password, as the system will not save the password</source>
        <translation> โปรดจำรหัสผ่านเนื่องจากระบบจะไม่บันทึกรหัสผ่าน</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="332"/>
        <source>View  password</source>
        <translation>ดูรหัสผ่าน</translation>
    </message>
</context>
<context>
    <name>ExpressWidget</name>
    <message>
        <location filename="childWidget/expresswidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="108"/>
        <source>[smile]</source>
        <translation>[ยิ้ม]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="109"/>
        <source>[twitch mouth]</source>
        <translation>[ปากกระตุก]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="110"/>
        <source>[salivate]</source>
        <translation>[น้ำลาย]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="111"/>
        <source>[staring blankly]</source>
        <translation>[จ้องมองอย่างว่างเปล่า]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="112"/>
        <source>[complacent]</source>
        <translation>[อิ่มเอมใจ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="113"/>
        <source>[shy]</source>
        <translation>[อาย]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="114"/>
        <source>[shut up]</source>
        <translation>[หุบปาก]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="115"/>
        <source>[sleep]</source>
        <translation>[นอน]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="116"/>
        <source>[cry]</source>
        <translation>[ร้องไห้]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="117"/>
        <source>[awkward]</source>
        <translation>[อึดอัด]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="118"/>
        <source>[angry]</source>
        <translation>[โกรธ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="119"/>
        <source>[naughty]</source>
        <translation>[ซน]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="120"/>
        <source>[grimace]</source>
        <translation>[แสยะ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="121"/>
        <source>[suprised]</source>
        <translation>[ประหลาดใจ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="122"/>
        <source>[sad]</source>
        <translation>[เศร้า]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="123"/>
        <source>[cool]</source>
        <translation>[เย็น]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="124"/>
        <source>[cold sweat]</source>
        <translation>[เหงื่อเย็น]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="125"/>
        <source>[crazy]</source>
        <translation>[บ้า]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="126"/>
        <source>[spit]</source>
        <translation>[น้ำลาย]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="127"/>
        <source>[titter]</source>
        <translation>[หัวร่อต่อกระซิก]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="128"/>
        <source>[supercilious look]</source>
        <translation>[รูปลักษณ์เยินยอ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="129"/>
        <source>[cute]</source>
        <translation>[น่ารัก]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="130"/>
        <source>[arrogance]</source>
        <translation>[หยิ่ง]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="131"/>
        <source>[hungry]</source>
        <translation>[หิว]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="132"/>
        <source>[sleepy]</source>
        <translation>[ง่วงนอน]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="133"/>
        <source>[terrified]</source>
        <translation>[กลัว]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="134"/>
        <source>[sweat]</source>
        <translation>[เหงื่อ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="135"/>
        <source>[smile fatuously]</source>
        <translation>[ยิ้มอย่างเย้ยหยัน]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="136"/>
        <source>[soldier]</source>
        <translation>[ทหาร]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="137"/>
        <source>[strive]</source>
        <translation>[มุ่งมั่น]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="138"/>
        <source>[doubting]</source>
        <translation>[สงสัย]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="139"/>
        <source>[hash]</source>
        <translation>[กัญชา]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="140"/>
        <source>[dizzy]</source>
        <translation>[วิงเวียน]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="141"/>
        <source>[pig&apos;s head]</source>
        <translation>[หัวหมู]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="142"/>
        <source>[skeleton]</source>
        <translation>[โครงกระดูก]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="143"/>
        <source>[unlucky]</source>
        <translation>[โชคร้าย]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="144"/>
        <source>[knock]</source>
        <translation>[เคาะ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="145"/>
        <source>[good bye]</source>
        <translation>[ลาก่อน]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="146"/>
        <source>[clap hands]</source>
        <translation>[ปรบมือ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="147"/>
        <source>[pick nose]</source>
        <translation>[เลือกจมูก]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="148"/>
        <source>[embarassed]</source>
        <translation>[ขึ้นไปข้างบน]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="149"/>
        <source>[snicker]</source>
        <translation>[ขำ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="150"/>
        <source>[left hem]</source>
        <translation>[มิ้มซ้าย]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="151"/>
        <source>[right hem]</source>
        <translation>[มิ้มขวา]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="152"/>
        <source>[yawn]</source>
        <translation>[อ้าปากค้าง]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="153"/>
        <source>[despise]</source>
        <translation>[ชัง]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="154"/>
        <source>[grievance]</source>
        <translation>[ข้องใจ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="155"/>
        <source>[about to weep]</source>
        <translation>[กำลังจะร้องไห้]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="156"/>
        <source>[insidious]</source>
        <translation>[ร้ายกาจ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="157"/>
        <source>[kiss]</source>
        <translation>[จูบ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="158"/>
        <source>[scared]</source>
        <translation>[กลัว]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="159"/>
        <source>[pitiful]</source>
        <translation>
ˈpitifəl
9/5000
[น่าสงสาร]
[Ǹā s̄ngs̄ār]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="160"/>
        <source>[kitchen knife]</source>
        <translation>[มีดครัว]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="161"/>
        <source>[watermelon]</source>
        <translation>[แตงโม]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="162"/>
        <source>[beer]</source>
        <translation>[เบียร์]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="163"/>
        <source>[ping pong]</source>
        <translation>[ปิงปอง]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="164"/>
        <source>[coffee]</source>
        <translation>[กาแฟ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="165"/>
        <source>[rice]</source>
        <translation>[ข้าว]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="166"/>
        <source>[rose]</source>
        <translation>[ดอกกุหลาบ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="167"/>
        <source>[withered]</source>
        <translation>[ลีบ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="168"/>
        <source>[love]</source>
        <translation>[ความรัก]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="169"/>
        <source>[loving heart]</source>
        <translation>[ใจรัก]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="170"/>
        <source>[heart break]</source>
        <translation>[หัวใจสลาย]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="171"/>
        <source>[cake]</source>
        <translation>[เค้ก]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="172"/>
        <source>[flash]</source>
        <translation>[แฟลช]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="173"/>
        <source>[bomb]</source>
        <translation>[ระเบิด]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="174"/>
        <source>[knife]</source>
        <translation>[ด]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="175"/>
        <source>[beetles]</source>
        <translation>[ด้วง]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="176"/>
        <source>[shit]</source>
        <translation>
SHit
6/5000
[อึ]
[Xụ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="177"/>
        <source>[moon]</source>
        <translation>[ดวงจันทร์]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="178"/>
        <source>[sun]</source>
        <translation>[อาทิตย์]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="179"/>
        <source>[gift]</source>
        <translation>[ของขวัญ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="180"/>
        <source>[embrace]</source>
        <translation>[โอบกอด]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="181"/>
        <source>[strong]</source>
        <translation>[แข็งแกร่ง]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="182"/>
        <source>[weak]</source>
        <translation>[อ่อนแอ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="183"/>
        <source>[shake hands]</source>
        <translation>[สัมผัสมือ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="184"/>
        <source>[hold fist salute]</source>
        <translation>[ถือกำปั้นทักทาย]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="185"/>
        <source>[seduce]</source>
        <translation>[เกลี้ยกล่อม]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="186"/>
        <source>[fist]</source>
        <translation>[กำปั้น]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="187"/>
        <source>[disappointed]</source>
        <translation>[ผิดหวัง]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="188"/>
        <source>[love you]</source>
        <translation>[รักคุณ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="189"/>
        <source>[NO]</source>
        <translation>[ไม่]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="190"/>
        <source>[OK]</source>
        <translation>[ตกลง]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="191"/>
        <source>[weep]</source>
        <translation>[ร้องไห้]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="192"/>
        <source>[curse]</source>
        <translation>[คำสาป]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="193"/>
        <source>[weep sweat]</source>
        <translation>[ร้องไห้เหงื่อ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="194"/>
        <source>[victory]</source>
        <translation>[ชัยชนะ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="195"/>
        <source>[basketball]</source>
        <translation>[บาสเกตบอล]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="196"/>
        <source>[soccer]</source>
        <translation>[ฟุตบอล]</translation>
    </message>
</context>
<context>
    <name>GiveRedPackWidget</name>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="26"/>
        <source>GiveRedPack</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="83"/>
        <source>Red Packet</source>
        <translation>แพ็คเก็ตสีแดง</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="105"/>
        <source>Close</source>
        <translation>ปิด</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="166"/>
        <source>Red Packet Token</source>
        <translation>โทเค็นแพ็คเก็ตสีแดง</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="195"/>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="382"/>
        <source>PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="251"/>
        <source>Escrow Balance</source>
        <translation>ยอดคงเหลือสัญญา</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="275"/>
        <source>0PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="294"/>
        <source>Transfer</source>
        <translation>โอน</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="338"/>
        <source>Amount Each</source>
        <translation>จำนวนเงินละ</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="363"/>
        <source>Set Amount</source>
        <translation>กำหนดจำนวนเงิน</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="416"/>
        <source>Message</source>
        <translation>ข่าวสาร</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="438"/>
        <source>Best wishes</source>
        <translation>ด้วยความปรารถนาดี</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="478"/>
        <source>Amount 0PWR</source>
        <translation>จำนวน 0PWR</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="518"/>
        <source>Red Packet History</source>
        <translation>ประวัติแพ็คเก็ตสีแดง</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="552"/>
        <source>Prepare Red Packet</source>
        <translation>เตรียม Red Packet</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="590"/>
        <source>Unclaimed red packet will be refunded after 24 hours</source>
        <translation>แพ็กเก็ตสีแดงที่ไม่มีการอ้างสิทธิ์จะได้รับคืนภายใน 24 ชั่วโมง</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="138"/>
        <source>Amount: </source>
        <translation>จำนวนเงิน:</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="146"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="180"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="189"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="195"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="213"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="218"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="228"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="146"/>
        <source>Failed to prepare the red packet!</source>
        <translation>ไม่สามารถเตรียมแพ็คเก็ตสีแดงได้!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="153"/>
        <source>Best wishes!</source>
        <translation>ด้วยความปรารถนาดี</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="180"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="189"/>
        <source>Amount cannot be empty!</source>
        <translation>จำนวนต้องไม่ว่างเปล่า</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="195"/>
        <source>The amount of the red packet cannot be greater than your balance!</source>
        <translation>จำนวนของแพ็กเก็ตสีแดงต้องไม่มากกว่ายอดคงเหลือของคุณ</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="201"/>
        <source>Please enter your login password</source>
        <translation>กรุณาใส่รหัสผ่านเข้าสู่ระบบของคุณ</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="213"/>
        <source>Password Validation Failed!</source>
        <translation>การตรวจสอบรหัสผ่านล้มเหลว</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="218"/>
        <source>Password cannot be empty!</source>
        <translation>รหัสผ่านต้องไม่ว่างเปล่า</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="228"/>
        <source>Incorrect Password!</source>
        <translation>รหัสผ่านผิดพลาด</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="235"/>
        <source>Best Wishes!</source>
        <translation>ด้วยความปรารถนาดี</translation>
    </message>
</context>
<context>
    <name>GroupAddBuddyWidget</name>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="20"/>
        <source>GroupAddBuddy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="161"/>
        <source>Select contact(s)</source>
        <translation>เลือกผู้ติดต่อ</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="239"/>
        <source>Search</source>
        <translation>ค้นหา</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="294"/>
        <source>Please select contact(s) you want to invite</source>
        <translation>โปรดเลือกผู้ติดต่อที่คุณต้องการเชิญ</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="367"/>
        <source>OK</source>
        <translation>ตกลง</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="398"/>
        <source>Cancel</source>
        <translation>ยกเลิก</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="197"/>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="447"/>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="645"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <source>This member has been in the tribe already!</source>
        <translation type="vanished">สมาชิกคนนี้อยู่ในเผ่าแล้ว</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="197"/>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="447"/>
        <source>This member has been in the group already!</source>
        <translation>สมาชิกคนนี้อยู่ในกลุ่มแล้ว!</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="266"/>
        <source>You have selected </source>
        <translation>คุณได้เลือก</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="266"/>
        <source> contact(s)</source>
        <translation>รายชื่อผู้ติดต่อ</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="269"/>
        <source>Please select contacts you want to invite</source>
        <translation>โปรดเลือกผู้ติดต่อที่คุณต้องการเชิญ</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="625"/>
        <source> invited </source>
        <translation>ได้รับเชิญ</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="625"/>
        <source> to join the group</source>
        <translation>เพื่อเข้าร่วมกลุ่ม</translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished">เพื่อเข้าร่วมเผ่า</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="645"/>
        <source>Added Successfully!</source>
        <translation>เพิ่มสำเร็จแล้ว</translation>
    </message>
</context>
<context>
    <name>GroupChatWidget</name>
    <message>
        <location filename="groupchatwidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="89"/>
        <source>Font</source>
        <translation>ตัวอักษร</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="95"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="120"/>
        <source>Expressions</source>
        <translation>การแสดงออก</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="148"/>
        <source>Vibration</source>
        <translation>การสั่นสะเทือน</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="176"/>
        <source>Picture</source>
        <translation>ภาพ</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="204"/>
        <source>Screen Cut</source>
        <translation>ตัดหน้าจอ</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="232"/>
        <source>File</source>
        <translation>ไฟล์</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="260"/>
        <source>Announcement</source>
        <translation>การประกาศ</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="288"/>
        <source>Mute All</source>
        <translation>ปิดเสียงทั้งหมด</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="319"/>
        <source>Red Packet</source>
        <translation>แพ็คเก็ตสีแดง</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="362"/>
        <source>Message Log</source>
        <translation>บันทึกข้อความ</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="389"/>
        <source>Send</source>
        <translation>ส่ง</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="392"/>
        <source>Alt+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="482"/>
        <source>Group Members</source>
        <translation>สมาชิกกลุ่ม</translation>
    </message>
    <message>
        <source>Tribe Members</source>
        <translation type="vanished">สมาชิกเผ่า</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="510"/>
        <source>Search</source>
        <translation>ค้นหา</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="547"/>
        <source>Add Member</source>
        <translation>เพิ่มสมาชิก</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="588"/>
        <source>Refresh</source>
        <translation>รีเฟรช</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="vanished">สำเนา</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation type="vanished">ตัด</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="vanished">แปะ</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">ลบ</translation>
    </message>
    <message>
        <source>Empty Filename! Open Document Failed!</source>
        <translation type="vanished">ชื่อไฟล์ว่างเปล่าเปิดเอกสารล้มเหลว</translation>
    </message>
    <message>
        <source>Notice</source>
        <translation type="vanished">แจ้งให้ทราบ</translation>
    </message>
    <message>
        <source>Empty file name or file doesn&apos;t exist!</source>
        <translation type="vanished">ไม่มีชื่อไฟล์หรือไฟล์เปล่า</translation>
    </message>
    <message>
        <source>Cannot send file larger than 100M in size</source>
        <translation type="vanished">ไม่สามารถส่งไฟล์ที่มีขนาดใหญ่กว่า 100M</translation>
    </message>
    <message>
        <source>[File]</source>
        <translation type="vanished">[ไฟล์]</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">เปิด</translation>
    </message>
    <message>
        <source>Image File(*.bmp;*.jpeg;*.jpg;*.png;*.gif)</source>
        <translation type="vanished">ไฟล์รูปภาพ (*. bmp; *. jpeg; *. jpg; *. png; *. gif)</translation>
    </message>
    <message>
        <source>[Image]</source>
        <translation type="vanished">[ภาพ]</translation>
    </message>
    <message>
        <source>[Red Packet]</source>
        <translation type="vanished">[แพ็คเก็ตสีแดง]</translation>
    </message>
    <message>
        <source>Best Wishes</source>
        <translation type="vanished">ด้วยความปรารถนาดี</translation>
    </message>
    <message>
        <source> joined the tribe</source>
        <translation type="vanished">เข้าร่วมเผ่า</translation>
    </message>
    <message>
        <source> invited </source>
        <translation type="vanished">ได้รับเชิญ</translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished">เพื่อเข้าร่วมเผ่า</translation>
    </message>
    <message>
        <source>The other party has successfully received your file</source>
        <translation type="vanished">บุคคลอื่นได้รับไฟล์ของคุณเรียบร้อยแล้ว</translation>
    </message>
    <message>
        <source>This file already exists, overwrite or not？</source>
        <translation type="vanished">ไฟล์นี้มีอยู่แล้วมันถูกเขียนทับหรือไม่?</translation>
    </message>
    <message>
        <source>Image File</source>
        <translation type="vanished">ไฟล์รูปภาพ</translation>
    </message>
    <message>
        <source>This file already exists, overwrite or not?</source>
        <translation type="vanished">ไฟล์นี้มีอยู่แล้วมันถูกเขียนทับหรือไม่?</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation type="vanished">บันทึกเป็น</translation>
    </message>
    <message>
        <source> Members %1</source>
        <translation type="vanished">สมาชิกเผ่า %1</translation>
    </message>
    <message>
        <source>Cancel Administrator Privileges</source>
        <translation type="vanished">ยกเลิกสิทธิ์ของผู้ดูแลระบบ</translation>
    </message>
    <message>
        <source>Set as Administrator</source>
        <translation type="vanished">ตั้งเป็นผู้ดูแลระบบ</translation>
    </message>
    <message>
        <source>View Profile</source>
        <translation type="vanished">ดูรายละเอียด</translation>
    </message>
    <message>
        <source>Set Tribal Name</source>
        <translation type="vanished">ตั้งชื่อเผ่า</translation>
    </message>
    <message>
        <source>Remove Tribe Member</source>
        <translation type="vanished">ลบสมาชิกเผ่า</translation>
    </message>
    <message>
        <source>Please enter your alias:</source>
        <translation type="vanished">โปรดป้อนชื่อแทนของคุณ:</translation>
    </message>
    <message>
        <source>Fail to check the red packet!</source>
        <translation type="vanished">ตรวจสอบแพ็กเก็ตสีแดงไม่สำเร็จ!</translation>
    </message>
    <message>
        <source>Add as friend</source>
        <translation type="vanished">เพิ่มเป็นเพื่อน</translation>
    </message>
    <message>
        <source>Adding as friend succeed!</source>
        <translation type="vanished">เพิ่มเพื่อนสู่ความสำเร็จ!</translation>
    </message>
    <message>
        <source>Adding as friend failed!</source>
        <translation type="vanished">การเพิ่มเพื่อนล้มเหลว!</translation>
    </message>
    <message>
        <source>Tribe has been silenced...</source>
        <translation type="vanished">สมาชิกทั้งหมดถูกแบน ...</translation>
    </message>
    <message>
        <source>Personal Business Card</source>
        <translation type="vanished">นามบัตรส่วนบุคคล</translation>
    </message>
    <message>
        <source>Personal ID:</source>
        <translation type="vanished">ID ส่วนบุคคล:</translation>
    </message>
    <message>
        <source>Tribe Business Card</source>
        <translation type="vanished">นามบัตรเผ่า</translation>
    </message>
    <message>
        <source>Tribe ID:</source>
        <translation type="vanished">ID ชนเผ่า:</translation>
    </message>
    <message>
        <source>Local file does&apos;t exist and cannot be forwarded</source>
        <translation type="vanished">ไม่มีไฟล์ในเครื่องและไม่สามารถส่งต่อได้</translation>
    </message>
    <message>
        <source>Resend</source>
        <translation type="vanished">ส่งซ้ำ</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation type="vanished">โหลด</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="vanished">ข้างหน้า</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="vanished">ชัดเจน</translation>
    </message>
    <message>
        <source>[Announcement]</source>
        <translation type="vanished">[ประกาศ]</translation>
    </message>
    <message>
        <source>Received the file successfully!</source>
        <translation type="vanished">คุณได้รับไฟล์ที่ส่งมาจากบุคคลอื่นเรียบร้อยแล้ว!</translation>
    </message>
</context>
<context>
    <name>GroupFileWidget</name>
    <message>
        <location filename="groupfilewidget.ui" line="14"/>
        <source>GroupFileWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="groupfilewidget.ui" line="89"/>
        <source>file</source>
        <translation>ไฟล์</translation>
    </message>
    <message>
        <location filename="groupfilewidget.ui" line="94"/>
        <source>upload time</source>
        <translation>เวลาอัพโหลด</translation>
    </message>
    <message>
        <location filename="groupfilewidget.ui" line="99"/>
        <source>size</source>
        <translation>ขนาด</translation>
    </message>
    <message>
        <location filename="groupfilewidget.ui" line="104"/>
        <source>uploaded by</source>
        <translation>อัปโหลดโดย</translation>
    </message>
    <message>
        <location filename="groupfilewidget.cpp" line="67"/>
        <source>%1 files in total</source>
        <translation>รวม %1 ไฟล์</translation>
    </message>
    <message>
        <location filename="groupfilewidget.cpp" line="129"/>
        <source>unknown</source>
        <translation>ไม่ทราบ</translation>
    </message>
    <message>
        <location filename="groupfilewidget.cpp" line="172"/>
        <source>open file</source>
        <oldsource>open</oldsource>
        <translation>เปิดไฟล์</translation>
    </message>
</context>
<context>
    <name>GroupPackWidget</name>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="26"/>
        <source>GroupPackWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="83"/>
        <source>Red Packet</source>
        <translation>แพ็คเก็ตสีแดง</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="105"/>
        <source>Close</source>
        <translation>ใกล้</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="156"/>
        <source>Random Red Packet</source>
        <translation>ซองจดหมายสีแดงแบบสุ่ม</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="210"/>
        <source>Identical Red Packet</source>
        <translation>ซองจดหมายสีแดงสามัญ</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="299"/>
        <source>Red Packet Token</source>
        <translation>โทเค็นซองจดหมายสีแดง</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="328"/>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="569"/>
        <source>PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="384"/>
        <source>Escrow Balance</source>
        <translation>ยอดคงเหลือสัญญา</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="408"/>
        <source>0PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="427"/>
        <source>Transfer</source>
        <translation>เข้าไป</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="480"/>
        <source>Total Amount</source>
        <translation>ยอดรวม</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="516"/>
        <source>拼</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="550"/>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="659"/>
        <source>Enter Number</source>
        <translation>กรอกจำนวนเงินทั้งหมด</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="603"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="273"/>
        <source>Each person gets a random amount</source>
        <translation>แต่ละคนจะได้รับจำนวนสุ่ม</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="637"/>
        <source>Number of Red Packets</source>
        <translation>จำนวนของแพ็กเก็ตสีแดง</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="687"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="260"/>
        <source>There are</source>
        <translation>เผ่าต่างๆ</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="718"/>
        <source>Message</source>
        <translation>ข่าวสาร</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="740"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="183"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="220"/>
        <source>Best Wishes</source>
        <translation>ด้วยความปรารถนาดี</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="786"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="277"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="291"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="317"/>
        <source>Amount 0PWR</source>
        <translation>จำนวน 0PWR</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="826"/>
        <source>Red Packet History</source>
        <translation>ประวัติแพ็คเก็ตสีแดง</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="860"/>
        <source>Prepare Red Packet</source>
        <translation>ยัดลงในซองจดหมายสีแดง</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="898"/>
        <source>Unclaimed red packet will be refunded after 24 hours</source>
        <translation>ซองจดหมายสีแดงที่ไม่มีการอ้างสิทธิ์จะได้รับคืนภายใน 24 ชั่วโมง</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="107"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="117"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="124"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="129"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="135"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="161"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="166"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="176"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="213"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="107"/>
        <source>The amount or the number of the red packet cannot be empty!</source>
        <translation>จำนวนหรือจำนวนของแพ็กเก็ตสีแดงต้องไม่ว่างเปล่า!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="117"/>
        <source>The amount or the number of the red packet cannot be 0 !</source>
        <translation>จำนวนหรือจำนวนของแพ็กเก็ตสีแดงต้องไม่เป็น 0!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="124"/>
        <source>The amount of the red packet cannot be greater than your balance!</source>
        <translation>จำนวนแพ็คเก็ตสีแดงไม่สามารถมากกว่ายอดคงเหลือของคุณ!</translation>
    </message>
    <message>
        <source>The number of the red packet cannot be greater than the number of people in the tribe!</source>
        <translation type="vanished">จำนวนของแพ็คเก็ตสีแดงไม่สามารถมากกว่าจำนวนของคนในเผ่า!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="129"/>
        <source>The number of the red packet cannot be greater than the number of people in the group!</source>
        <translation>จำนวนของแพ็กเก็ตสีแดงต้องไม่มากกว่าจำนวนคนในกลุ่ม!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="135"/>
        <source>The amount of each red packet cannot be smaller than 0.00001!</source>
        <translation>จำนวนของแต่ละแพ็กเก็ตสีแดงต้องไม่น้อยกว่า 0.00001!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="140"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="147"/>
        <source>Please enter your login password:</source>
        <translation>กรุณาใส่รหัสผ่านเข้าสู่ระบบของคุณ:</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="161"/>
        <source>Failed to verify the login password!</source>
        <translation>ไม่สามารถยืนยันรหัสผ่านการเข้าสู่ระบบ!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="166"/>
        <source>Login password cannot be empty!</source>
        <translation>รหัสผ่านเข้าสู่ระบบต้องไม่ว่างเปล่า!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="176"/>
        <source>Incorrect Password!</source>
        <translation>รหัสผ่านไม่ถูกต้อง!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="213"/>
        <source>Failed to prepare the red packet!</source>
        <translation>ไม่สามารถสร้างซองจดหมายสีแดง!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="260"/>
        <source>people in this group</source>
        <translation>คนในกลุ่มนี้</translation>
    </message>
    <message>
        <source>people in this tribe</source>
        <translation type="vanished">ในเผ่านี้</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="271"/>
        <source>Amount</source>
        <translation>ซองจดหมายสีแดงรวม</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="285"/>
        <source>Amount Each</source>
        <translation>จำนวนเงินละ</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="287"/>
        <source>Each person gets a identical amount</source>
        <translation>แต่ละคนได้รับจำนวนเท่ากัน</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="294"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="305"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="322"/>
        <source>Amount </source>
        <translation>ซองจดหมายสีแดงรวม</translation>
    </message>
</context>
<context>
    <name>GroupSearchWidget</name>
    <message>
        <location filename="childWidget/groupsearchwidget.ui" line="23"/>
        <source>GroupSearchWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childWidget/groupsearchwidget.ui" line="74"/>
        <source>Find Group Member</source>
        <translation>ค้นหาสมาชิกกลุ่ม</translation>
    </message>
    <message>
        <source>Find Tribe Member</source>
        <translation type="vanished">ค้นหาสมาชิกเผ่า</translation>
    </message>
</context>
<context>
    <name>GroupUserProfileWidget</name>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="14"/>
        <source>GroupUserProfile</source>
        <translation></translation>
    </message>
    <message>
        <source>Personal ID</source>
        <translation type="vanished">ID ส่วนบุคคล</translation>
    </message>
    <message>
        <source>Base ID</source>
        <translation type="vanished">ID ฐาน</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="111"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="188"/>
        <source>Acct.No</source>
        <translation>บัญชี</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="234"/>
        <source>Copy</source>
        <translation>สำเนา</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="336"/>
        <source>View Avatar</source>
        <translation>ดูรูป</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="630"/>
        <source>Add as friend</source>
        <translation>เพิ่มเป็นเพื่อน</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.cpp" line="25"/>
        <source>Close</source>
        <translation>ปิด</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.cpp" line="70"/>
        <source>Send</source>
        <translation>ส่ง</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.cpp" line="189"/>
        <source>Request sent successfully</source>
        <translation>ส่งคำขอเรียบร้อยแล้ว</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.cpp" line="193"/>
        <source>Request sent Failed</source>
        <translation>คำขอล้มเหลวในการส่ง</translation>
    </message>
</context>
<context>
    <name>GroupWidget</name>
    <message>
        <location filename="groupwidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="groupwidget.ui" line="83"/>
        <source>View the Details</source>
        <translation>ดูรายละเอียด</translation>
    </message>
    <message>
        <location filename="groupwidget.ui" line="133"/>
        <source>chat</source>
        <translation>พูดคุย</translation>
    </message>
    <message>
        <location filename="groupwidget.ui" line="176"/>
        <source>file</source>
        <translation>ไฟล์</translation>
    </message>
</context>
<context>
    <name>IMGroupChatStore</name>
    <message>
        <location filename="imgroupchatstore.cpp" line="495"/>
        <location filename="imgroupchatstore.cpp" line="1260"/>
        <location filename="imgroupchatstore.cpp" line="2109"/>
        <source>Best Wishes</source>
        <translation>ด้วยความปรารถนาดี</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="598"/>
        <location filename="imgroupchatstore.cpp" line="1413"/>
        <location filename="imgroupchatstore.cpp" line="2267"/>
        <source>The other party has successfully received your file</source>
        <translation>บุคคลอื่นได้รับไฟล์ของคุณเรียบร้อยแล้ว</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="933"/>
        <location filename="imgroupchatstore.cpp" line="1765"/>
        <location filename="imgroupchatstore.cpp" line="1809"/>
        <location filename="imgroupchatstore.cpp" line="2577"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="933"/>
        <source>This file already exists, overwrite or not?</source>
        <translation>ไฟล์นี้มีอยู่แล้วมันถูกเขียนทับหรือไม่?</translation>
    </message>
    <message>
        <source> joined the tribe</source>
        <translation type="vanished">เข้าร่วมเผ่า</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1409"/>
        <location filename="imgroupchatstore.cpp" line="2263"/>
        <location filename="imgroupchatstore.cpp" line="2986"/>
        <source> invited </source>
        <translation>ได้รับเชิญ</translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished">เพื่อเข้าร่วมเผ่า</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1623"/>
        <location filename="imgroupchatstore.cpp" line="1631"/>
        <location filename="imgroupchatstore.cpp" line="1650"/>
        <source>[Image]</source>
        <translation>[ภาพ]</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1750"/>
        <location filename="imgroupchatstore.cpp" line="1793"/>
        <location filename="imgroupchatstore.cpp" line="2641"/>
        <location filename="imgroupchatstore.cpp" line="2649"/>
        <location filename="imgroupchatstore.cpp" line="2696"/>
        <source>[File]</source>
        <translation>[ไฟล์]</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1765"/>
        <location filename="imgroupchatstore.cpp" line="1809"/>
        <source>Local file does&apos;t exist and cannot be forwarded</source>
        <translation>ไม่มีไฟล์ในเครื่องและไม่สามารถส่งต่อได้</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2430"/>
        <source>Personal Business Card</source>
        <translation>นามบัตรส่วนบุคคล</translation>
    </message>
    <message>
        <source>Personal ID:</source>
        <translation type="vanished">ID ส่วนบุคคล:</translation>
    </message>
    <message>
        <source>Tribe Business Card</source>
        <translation type="vanished">นามบัตรเผ่า</translation>
    </message>
    <message>
        <source>Tribe ID:</source>
        <translation type="vanished">ID ชนเผ่า:</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1407"/>
        <location filename="imgroupchatstore.cpp" line="2261"/>
        <location filename="imgroupchatstore.cpp" line="2984"/>
        <source> joined the group</source>
        <translation>เข้าร่วมกลุ่ม</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1409"/>
        <location filename="imgroupchatstore.cpp" line="2263"/>
        <location filename="imgroupchatstore.cpp" line="2986"/>
        <source> to join the group</source>
        <translation>เพื่อเข้าร่วมกลุ่ม</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2431"/>
        <source>ID:</source>
        <translation>ID:</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2444"/>
        <source>Group Business Card</source>
        <translation>นามบัตรกลุ่ม</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2445"/>
        <source>Group ID:</source>
        <translation>ID กลุ่ม:</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2551"/>
        <source>[Red Packet]</source>
        <translation>[แพ็คเก็ตสีแดง]</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2577"/>
        <source>Cannot send file larger than 100M in size</source>
        <translation>ไม่สามารถส่งไฟล์ที่มีขนาดใหญ่กว่า 100M</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2790"/>
        <location filename="imgroupchatstore.cpp" line="2798"/>
        <location filename="imgroupchatstore.cpp" line="2815"/>
        <source>[Announcement]</source>
        <translation>[ประกาศ]</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2913"/>
        <source>Save as</source>
        <translation>บันทึกเป็น</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2990"/>
        <source>Received the file successfully!</source>
        <translation>คุณได้รับไฟล์ที่ส่งมาจากบุคคลอื่นเรียบร้อยแล้ว!</translation>
    </message>
</context>
<context>
    <name>IMGroupChatView</name>
    <message>
        <location filename="imgroupchatview.cpp" line="137"/>
        <source> Members %1</source>
        <translation>สมาชิกเผ่า %1</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="356"/>
        <location filename="imgroupchatview.cpp" line="1751"/>
        <location filename="imgroupchatview.cpp" line="1758"/>
        <source>Copy</source>
        <translation>สำเนา</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="359"/>
        <source>Cut</source>
        <translation>ตัด</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="362"/>
        <source>Paste</source>
        <translation>แปะ</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="365"/>
        <source>Delete</source>
        <translation>ลบ</translation>
    </message>
    <message>
        <source>Tribe has been silenced...</source>
        <translation type="vanished">สมาชิกทั้งหมดถูกแบน ...</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="933"/>
        <source>Cancel Administrator Privileges</source>
        <translation>ยกเลิกสิทธิ์ของผู้ดูแลระบบ</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="939"/>
        <source>Set as Administrator</source>
        <translation>ตั้งเป็นผู้ดูแลระบบ</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="946"/>
        <source>View Profile</source>
        <translation>ดูรายละเอียด</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="951"/>
        <source>Set Remark Name</source>
        <oldsource>Set Group Name</oldsource>
        <translation>ตั้งชื่อหมายเหตุ</translation>
    </message>
    <message>
        <source>Remove Tribe Member</source>
        <translation type="vanished">ลบสมาชิกเผ่า</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="788"/>
        <source>Group has been silenced...</source>
        <translation>กลุ่มถูกปิดเสียง</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="958"/>
        <source>Remove Group Member</source>
        <translation>ลบสมาชิกกลุ่ม</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1014"/>
        <source>Please enter your alias:</source>
        <translation>โปรดป้อนชื่อแทนของคุณ:</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1303"/>
        <source>Open</source>
        <translation>เปิด</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1304"/>
        <source>Image File</source>
        <translation>ไฟล์รูปภาพ</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1521"/>
        <source>Send</source>
        <translation>ส่ง</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1521"/>
        <source>All(*.*)</source>
        <translation>ทั้งหมด (*. *)</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1652"/>
        <source>Empty Filename! Open Document Failed!</source>
        <translation>ชื่อไฟล์ว่างเปล่าเปิดเอกสารล้มเหลว</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1701"/>
        <location filename="imgroupchatview.cpp" line="2109"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1701"/>
        <source>Empty file name or file doesn&apos;t exist!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1768"/>
        <source>Resend</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1778"/>
        <source>Reload</source>
        <translation>โหลด</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1786"/>
        <source>Forward</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1797"/>
        <source>Clear</source>
        <translation>ชัดเจน</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="2109"/>
        <source>Fail to check the red packet!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="2246"/>
        <source>@</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>IMGroupChatViewModel</name>
    <message>
        <location filename="imgroupchatviewmodel.cpp" line="206"/>
        <source>[Image]</source>
        <translation>[ภาพ]</translation>
    </message>
</context>
<context>
    <name>IMPerChat</name>
    <message>
        <location filename="imperchat.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="92"/>
        <source>View the Detail</source>
        <translation>ดูรายละเอียด</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="228"/>
        <source>Screen Cut</source>
        <translation>จับภาพหน้าจอ</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="256"/>
        <source>Font</source>
        <translation>ตัวอักษร</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="284"/>
        <source>Expressions</source>
        <translation>การแสดงออก</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="312"/>
        <source>Vibration</source>
        <translation>การสั่นสะเทือน</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="340"/>
        <source>Picture</source>
        <translation>ภาพ</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="368"/>
        <source>File</source>
        <translation>ไฟล์</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="396"/>
        <source>Secret Message</source>
        <translation>ข้อความลับ</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="424"/>
        <source>Secret Picture</source>
        <translation>ภาพลับ</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="452"/>
        <source>Secret File</source>
        <translation>ไฟล์ลับ</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="480"/>
        <source>Announcement</source>
        <translation>การประกาศ</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="536"/>
        <source>ETH Transfer</source>
        <translation>การโอน ETH</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="564"/>
        <source>BTC Transfer</source>
        <translation>การโอน BTC</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="592"/>
        <source>EOS Transfer</source>
        <translation>การโอน EOS</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="620"/>
        <source>Red Packet</source>
        <translation>แพ็คเก็ตสีแดง</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="672"/>
        <source>Message Log</source>
        <translation>บันทึกข้อความ</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="696"/>
        <source>Send</source>
        <translation>ส่ง</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="699"/>
        <source>Alt+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="742"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;微软雅黑&apos;; font-size:12pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>Create Tribe</source>
        <translation type="vanished">สร้างเผ่า</translation>
    </message>
    <message>
        <source>Add as a friend</source>
        <translation type="vanished">เพิ่มเป็นเพื่อน</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="vanished">สำเนา</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation type="vanished">ตัด</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="vanished">แปะ</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">ลบ</translation>
    </message>
    <message>
        <source>Power Exchange</source>
        <translation type="vanished">การแลกเปลี่ยนพลังงาน</translation>
    </message>
    <message>
        <source>ETH Tranfer</source>
        <translation type="vanished">การโอน ETH</translation>
    </message>
    <message>
        <source>BTC Tranfer</source>
        <translation type="vanished">การโอน BTC</translation>
    </message>
    <message>
        <source>EOS Tranfer</source>
        <translation type="vanished">การโอน EOS</translation>
    </message>
    <message>
        <source>Notice</source>
        <translation type="vanished">แจ้งให้ทราบ</translation>
    </message>
    <message>
        <source>File larger than 100M cannot be sent</source>
        <translation type="vanished">ไฟล์ที่มีขนาดใหญ่กว่า 100M ไม่สามารถส่งได้</translation>
    </message>
    <message>
        <source>[File]</source>
        <translation type="vanished">[ไฟล์]</translation>
    </message>
    <message>
        <source>Empty file name or file doesn&apos;t exist!</source>
        <translation type="vanished">ไม่มีชื่อไฟล์หรือไฟล์เปล่า</translation>
    </message>
    <message>
        <source>Local file does&apos;t exist，unable to forward</source>
        <translation type="vanished">ไม่มีไฟล์ในตัวเครื่องไม่สามารถส่งต่อได้</translation>
    </message>
    <message>
        <source>[Image]</source>
        <translation type="vanished">[ภาพ]</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">เปิด</translation>
    </message>
    <message>
        <source>Image File(*.bmp;*.jpeg;*.jpg;*.png;*.gif)</source>
        <translation type="vanished">ไฟล์รูปภาพ (*. bmp; *. jpeg; *. jpg; *. png; *. gif)</translation>
    </message>
    <message>
        <source>Best Wishes</source>
        <translation type="vanished">ด้วยความปรารถนาดี</translation>
    </message>
    <message>
        <source>Received file successfully!</source>
        <translation type="vanished">ได้รับไฟล์สำเร็จ</translation>
    </message>
    <message>
        <source>The other party has successfully received your file</source>
        <translation type="vanished">บุคคลอื่นได้รับไฟล์ของคุณเรียบร้อยแล้ว</translation>
    </message>
    <message>
        <source>This file already exists, overwrite or not？</source>
        <translation type="vanished">ไฟล์นี้มีอยู่แล้วมันถูกเขียนทับหรือไม่?</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation type="vanished">บันทึกเป็น</translation>
    </message>
    <message>
        <source>Receive file successfully!</source>
        <translation type="vanished">ได้รับไฟล์สำเร็จ</translation>
    </message>
    <message>
        <source>[Secret Message]</source>
        <translation type="vanished">[ข้อความลับ]</translation>
    </message>
    <message>
        <source>[Secret Image]</source>
        <translation type="vanished">[ภาพลับ]</translation>
    </message>
    <message>
        <source>[Secret File]</source>
        <translation type="vanished">[ไฟล์ลับ]</translation>
    </message>
    <message>
        <source>[Red Packet]</source>
        <translation type="vanished">[แพ็คเก็ตสีแดง]</translation>
    </message>
    <message>
        <source>Personal business card</source>
        <translation type="vanished">นามบัตรส่วนบุคคล</translation>
    </message>
    <message>
        <source>Personal ID:</source>
        <translation type="vanished">ID ส่วนบุคคล:</translation>
    </message>
    <message>
        <source>Tribe business card</source>
        <translation type="vanished">นามบัตรเผ่า</translation>
    </message>
    <message>
        <source>Tribe ID:</source>
        <translation type="vanished">ID ชนเผ่า:</translation>
    </message>
    <message>
        <source>Fail to check the red packet!</source>
        <translation type="vanished">ดูซองจดหมายสีแดงล้มเหลว!</translation>
    </message>
    <message>
        <source>Resend</source>
        <translation type="vanished">ส่ง</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation type="vanished">โหลด</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="vanished">ส่งต่อ</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="vanished">ชัดเจน</translation>
    </message>
    <message>
        <source>[Announcement]</source>
        <translation type="vanished">[ประกาศ]</translation>
    </message>
    <message>
        <source>（Typing...）</source>
        <translation type="vanished">คุณกำลังพิมพ์ ...</translation>
    </message>
</context>
<context>
    <name>IMPerChatStore</name>
    <message>
        <location filename="imperchatstore.cpp" line="696"/>
        <location filename="imperchatstore.cpp" line="1379"/>
        <source>Best Wishes</source>
        <translation>ด้วยความปรารถนาดี</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="872"/>
        <source>The other party has successfully received your file</source>
        <translation>บุคคลอื่นได้รับไฟล์ของคุณเรียบร้อยแล้ว</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="923"/>
        <location filename="imperchatstore.cpp" line="1097"/>
        <location filename="imperchatstore.cpp" line="1917"/>
        <location filename="imperchatstore.cpp" line="2191"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="923"/>
        <source>This file already exists, overwrite or not?</source>
        <translation>ไฟล์นี้มีอยู่แล้วมันถูกเขียนทับหรือไม่?</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1039"/>
        <location filename="imperchatstore.cpp" line="1044"/>
        <location filename="imperchatstore.cpp" line="1057"/>
        <source>Receive file successfully!</source>
        <translation>ได้รับไฟล์สำเร็จ!</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1097"/>
        <source>File is in use, please check it first!</source>
        <translation>มีการใช้งานไฟล์โปรดตรวจสอบก่อน!</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1574"/>
        <source>Received file successfully!</source>
        <translation>ได้รับไฟล์สำเร็จ!</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1763"/>
        <location filename="imperchatstore.cpp" line="1792"/>
        <source>[Image]</source>
        <translation>[ภาพ]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1900"/>
        <location filename="imperchatstore.cpp" line="2256"/>
        <location filename="imperchatstore.cpp" line="2318"/>
        <source>[File]</source>
        <translation>[ไฟล์]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1917"/>
        <source>Local file does&apos;t exist，unable to forward</source>
        <translation>ไม่มีไฟล์ในตัวเครื่องไม่สามารถส่งต่อได้</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1991"/>
        <source>Personal business card</source>
        <translation>นามบัตรส่วนบุคคล</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1992"/>
        <source>ID:</source>
        <translation>ID:</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2005"/>
        <source>Group business card</source>
        <translation>นามบัตรกลุ่ม</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2006"/>
        <source>Group ID:</source>
        <translation>ID กลุ่ม:</translation>
    </message>
    <message>
        <source>Personal ID:</source>
        <translation type="vanished">ID ส่วนบุคคล:</translation>
    </message>
    <message>
        <source>Tribe business card</source>
        <translation type="vanished">นามบัตรเผ่า</translation>
    </message>
    <message>
        <source>Tribe ID:</source>
        <translation type="vanished">ID ชนเผ่า:</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2162"/>
        <source>[Red Packet]</source>
        <translation>[แพ็คเก็ตสีแดง]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2191"/>
        <source>File larger than 100M cannot be sent</source>
        <translation>ไฟล์ที่มีขนาดใหญ่กว่า 100M ไม่สามารถส่งได้</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2363"/>
        <source>[Secret Message]</source>
        <translation>[ข้อความลับ]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2395"/>
        <source>[Secret Image]</source>
        <translation>[ภาพลับ]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2427"/>
        <source>[Secret File]</source>
        <translation>[ไฟล์ลับ]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2508"/>
        <location filename="imperchatstore.cpp" line="2540"/>
        <source>[Announcement]</source>
        <translation>[ประกาศ]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2601"/>
        <source>Save as</source>
        <translation>บันทึกเป็น</translation>
    </message>
</context>
<context>
    <name>IMPerChatView</name>
    <message>
        <source>Create Tribe</source>
        <translation type="vanished">สร้างเผ่า</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="153"/>
        <source>Create Group</source>
        <translation>สร้างกลุ่ม</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="164"/>
        <source>Add as a friend</source>
        <translation>เพิ่มเป็นเพื่อน</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="221"/>
        <source>Power Exchange</source>
        <translation>การแลกเปลี่ยนพลังงาน</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="226"/>
        <source>ETH Tranfer</source>
        <translation>การโอน ETH</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="231"/>
        <source>BTC Tranfer</source>
        <translation>การโอน BTC</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="236"/>
        <source>EOS Tranfer</source>
        <translation>การโอน EOS</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="598"/>
        <location filename="imperchatview.cpp" line="1777"/>
        <location filename="imperchatview.cpp" line="1783"/>
        <source>Copy</source>
        <translation>สำเนา</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="601"/>
        <source>Cut</source>
        <translation>ตัด</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="604"/>
        <source>Paste</source>
        <translation>แปะ</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="607"/>
        <source>Delete</source>
        <translation>ลบ</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="751"/>
        <source>（Typing...）</source>
        <translation>คุณกำลังพิมพ์ ...</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="953"/>
        <source>Open</source>
        <translation>เปิด</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="954"/>
        <source>Image File</source>
        <translation>ไฟล์รูปภาพ</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1030"/>
        <source>Send</source>
        <translation>ส่ง</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1030"/>
        <source>All(*.*)</source>
        <translation>ทั้งหมด (*. *)</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1293"/>
        <location filename="imperchatview.cpp" line="1684"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1293"/>
        <source>Empty file name or file doesn&apos;t exist!</source>
        <translation>ไม่มีชื่อไฟล์หรือไฟล์เปล่า!</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1325"/>
        <source>fileName为空!OpenDocument Failed!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1684"/>
        <source>Fail to check the red packet!</source>
        <translation>ตรวจสอบแพ็กเก็ตสีแดงไม่สำเร็จ!</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1793"/>
        <source>Resend</source>
        <translation>ส่งซ้ำ</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1801"/>
        <source>Reload</source>
        <translation>โหลด</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1809"/>
        <source>Forward</source>
        <translation>ข้างหน้า</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1819"/>
        <source>Clear</source>
        <translation>ชัดเจน</translation>
    </message>
</context>
<context>
    <name>IMPerChatViewModel</name>
    <message>
        <location filename="imperchatviewmodel.cpp" line="166"/>
        <source>[Image]</source>
        <translation>[ภาพ]</translation>
    </message>
</context>
<context>
    <name>MessageListDispatcher</name>
    <message>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="160"/>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="171"/>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="197"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="160"/>
        <source>Are you sure to dismiss this group?</source>
        <translation>คุณแน่ใจที่จะยกเลิกกลุ่มนี้?</translation>
    </message>
    <message>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="171"/>
        <source>Are you sure to quit group?</source>
        <translation>คุณแน่ใจที่จะออกจากกลุ่ม?</translation>
    </message>
    <message>
        <source>Are you sure to dismiss this tribe?</source>
        <translation type="vanished">มุ่งมั่นที่จะสลายเผ่า?</translation>
    </message>
    <message>
        <source>Are you sure to quit tribe?</source>
        <translation type="vanished">Are you sure to quit tribe?</translation>
    </message>
    <message>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="197"/>
        <source>Network request failed!</source>
        <translation>คำขอเครือข่ายล้มเหลว!</translation>
    </message>
</context>
<context>
    <name>MessageListStore</name>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="148"/>
        <location filename="MessageWidget/messageliststore.cpp" line="725"/>
        <source> joined the group</source>
        <oldsource> joined the tribe</oldsource>
        <translation>เข้าร่วมกลุ่ม</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="150"/>
        <location filename="MessageWidget/messageliststore.cpp" line="727"/>
        <source> invited </source>
        <translation>ได้รับเชิญ</translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished">เพื่อเข้าร่วมเผ่า</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="156"/>
        <location filename="MessageWidget/messageliststore.cpp" line="558"/>
        <location filename="MessageWidget/messageliststore.cpp" line="733"/>
        <source>File received successfully </source>
        <translation>ได้รับไฟล์สำเร็จ</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="158"/>
        <location filename="MessageWidget/messageliststore.cpp" line="562"/>
        <location filename="MessageWidget/messageliststore.cpp" line="735"/>
        <source>The other party has successfully received your file </source>
        <translation>บุคคลอื่นได้รับไฟล์ของคุณเรียบร้อยแล้ว</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="195"/>
        <location filename="MessageWidget/messageliststore.cpp" line="198"/>
        <location filename="MessageWidget/messageliststore.cpp" line="537"/>
        <location filename="MessageWidget/messageliststore.cpp" line="785"/>
        <location filename="MessageWidget/messageliststore.cpp" line="788"/>
        <source>[Announcement]</source>
        <translation>[ประกาศ]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="209"/>
        <location filename="MessageWidget/messageliststore.cpp" line="212"/>
        <location filename="MessageWidget/messageliststore.cpp" line="544"/>
        <location filename="MessageWidget/messageliststore.cpp" line="798"/>
        <location filename="MessageWidget/messageliststore.cpp" line="801"/>
        <source>[Share]</source>
        <translation>[แบ่งปัน]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="319"/>
        <source>View Details</source>
        <translation>ดูรายละเอียด</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="323"/>
        <source>Close Chat</source>
        <translation>ปิดการแชท</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="327"/>
        <source>Close All Chats</source>
        <translation>ปิดการแชททั้งหมด</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="344"/>
        <source>Remove From Top</source>
        <translation>ลบจากด้านบน</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="352"/>
        <source>Set Message Top</source>
        <translation>ตั้งค่าข้อความด้านบน</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="360"/>
        <source>Cancle No-Disturbing</source>
        <translation>ยกเลิกไม่รบกวน</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="368"/>
        <source>Set No-Disturbing</source>
        <translation>ตั้งค่าไม่รบกวน</translation>
    </message>
    <message>
        <source>Dismiss Tribe</source>
        <translation type="vanished">การแบ่งแยกเผ่า</translation>
    </message>
    <message>
        <source>Quit Tribe</source>
        <translation type="vanished">ออกจากเผ่า</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="150"/>
        <location filename="MessageWidget/messageliststore.cpp" line="727"/>
        <source> to join the group</source>
        <translation>เพื่อเข้าร่วมกลุ่ม</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="386"/>
        <source>Dismiss Group</source>
        <translation>ปิดกลุ่ม</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="395"/>
        <source>Quit Group</source>
        <translation>ออกจากกลุ่ม</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="490"/>
        <source>Yesterday</source>
        <translation>เมื่อวาน</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="509"/>
        <source>[Image]</source>
        <translation>[ภาพ]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="511"/>
        <source>[Audio]</source>
        <translation>[เสียง]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="513"/>
        <source>[Video]</source>
        <translation>[วีดีโอ]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="515"/>
        <source>[File]</source>
        <translation>[ไฟล์]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="517"/>
        <source>This type of message is not supported for now</source>
        <translation>ไม่รองรับข้อความประเภทนี้ในขณะนี้</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="519"/>
        <source>[Tranfer]</source>
        <translation>[การโอนเงิน]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="521"/>
        <source>[Red Packet]</source>
        <translation>[แพ็คเก็ตสีแดง]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="523"/>
        <source>[Secret Message]</source>
        <translation>[ข้อความลับ]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="525"/>
        <source>[Secret Image]</source>
        <translation>[ภาพลับ]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="527"/>
        <source>[Secret File]</source>
        <translation>[ไฟล์ลับ]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="529"/>
        <source>[Location]</source>
        <translation>[สถานที่]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="531"/>
        <source>[Gravitational Waves]</source>
        <translation>[คลื่นความโน้มถ่วง]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="627"/>
        <location filename="MessageWidget/messageliststore.cpp" line="631"/>
        <location filename="MessageWidget/messageliststore.cpp" line="877"/>
        <location filename="MessageWidget/messageliststore.cpp" line="896"/>
        <location filename="MessageWidget/messageliststore.cpp" line="983"/>
        <location filename="MessageWidget/messageliststore.cpp" line="987"/>
        <location filename="MessageWidget/messageliststore.cpp" line="1223"/>
        <source>&lt;font color=&apos;#f7931e&apos; &gt;[I was @]&lt;/font&gt;</source>
        <translation>&lt;font color=&apos;#f7931e&apos; &gt;[มีคนพูดถึงฉัน]&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="1730"/>
        <source>&lt;font color=&apos;#f7931e&apos; &gt;[Draft]&lt;/font&gt;</source>
        <translation>&lt;font color=&apos;#f7931e&apos; &gt;[ร่าง]&lt;/font&gt;</translation>
    </message>
</context>
<context>
    <name>MessageLog</name>
    <message>
        <location filename="messagelog/messagelog.ui" line="26"/>
        <source>Message Log</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="330"/>
        <source>Messsage Log</source>
        <translation>บันทึกข้อความ</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="484"/>
        <source>Friends</source>
        <translation>เพื่อน</translation>
    </message>
    <message>
        <source>Tribe</source>
        <translation type="vanished">เผ่า</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="524"/>
        <source>Group</source>
        <translation>กลุ่ม</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="578"/>
        <source>🔎</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="598"/>
        <source>Search</source>
        <translation>ค้นหา</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="623"/>
        <source>Clear</source>
        <translation>ชัดเจน</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="697"/>
        <source>about:blank</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="811"/>
        <source>|&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="837"/>
        <source>&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="857"/>
        <source>&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="877"/>
        <source>&gt;|</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="45"/>
        <source>微软雅黑</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1068"/>
        <source>Best Wishes</source>
        <translation>ด้วยความปรารถนาดี</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1212"/>
        <location filename="messagelog/messagelog.cpp" line="1340"/>
        <location filename="messagelog/messagelog.cpp" line="1831"/>
        <location filename="messagelog/messagelog.cpp" line="1970"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1212"/>
        <source>Empty file name or file doesn&apos;t exist!</source>
        <translation>ไม่มีชื่อไฟล์หรือไฟล์เปล่า</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1244"/>
        <source>fileName为空!OpenDocument Failed!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1333"/>
        <source>Sorry</source>
        <translation>ขอโทษ</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1333"/>
        <source>There are no qualified message records</source>
        <translation>ไม่มีบันทึกข้อความที่ผ่านการรับรอง</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1340"/>
        <source>Unable to search empty content</source>
        <translation>ไม่สามารถค้นหาเนื้อหาที่ว่างเปล่า</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1831"/>
        <source>Failed to check the red packet!</source>
        <translation>ดูซองจดหมายสีแดงล้มเหลว!</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1970"/>
        <source>This file already exists, overwrite or not?</source>
        <translation>ไฟล์นี้มีอยู่แล้วมันถูกเขียนทับหรือไม่?</translation>
    </message>
    <message>
        <source>This file already exists, overwrite or not？</source>
        <translation type="vanished">ไฟล์นี้มีอยู่แล้วมันถูกเขียนทับหรือไม่?</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="2150"/>
        <source>Save as</source>
        <translation>บันทึกเป็น</translation>
    </message>
</context>
<context>
    <name>NoticeWidget</name>
    <message>
        <location filename="childWidget/noticewidget.ui" line="14"/>
        <source>NoticeWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.ui" line="111"/>
        <source>Announcement</source>
        <translation>การประกาศ</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.ui" line="204"/>
        <source>Click to Choose Image</source>
        <translation>คลิกเพื่อเลือกภาพ</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.ui" line="243"/>
        <source>Please enter the title of the web page</source>
        <translation>กรุณาใส่ชื่อของหน้าเว็บ</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.ui" line="271"/>
        <source>Please enter the link of the web page</source>
        <translation>กรุณาใส่ลิงค์ของหน้าเว็บ</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.ui" line="388"/>
        <source>Send</source>
        <translation>ส่ง</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="29"/>
        <source>Please enter the title of the image</source>
        <translation>กรุณาใส่ชื่อของภาพ</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="30"/>
        <location filename="childWidget/noticewidget.cpp" line="35"/>
        <source>微软雅黑</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="77"/>
        <source>Open</source>
        <translation>เปิด</translation>
    </message>
    <message>
        <source>Image File(*.bmp;*.jpeg;*.jpg;*.png;*.gif)</source>
        <translation type="vanished">ไฟล์รูปภาพ (*. bmp; *. jpeg; *. jpg; *. png; *. gif)</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="78"/>
        <source>Image File</source>
        <translation>ไฟล์รูปภาพ</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="137"/>
        <location filename="childWidget/noticewidget.cpp" line="145"/>
        <location filename="childWidget/noticewidget.cpp" line="150"/>
        <location filename="childWidget/noticewidget.cpp" line="155"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="137"/>
        <source>Please Choose Image!</source>
        <translation>กรุณาเลือกภาพ!</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="145"/>
        <source>Please enter the title of the image！</source>
        <translation>กรุณาใส่ชื่อของภาพ！</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="150"/>
        <source>Please enter the title of web page!</source>
        <translation>กรุณาใส่ชื่อของหน้าเว็บ!</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="155"/>
        <source>Please enter the URL！</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>OpenLetterWidget</name>
    <message>
        <location filename="secretWidget/openletterwidget.ui" line="14"/>
        <source>OpenLetterWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.ui" line="78"/>
        <source>Secret Message</source>
        <translation>ข้อความลับ</translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.ui" line="205"/>
        <source>Enter Password</source>
        <translation>ใส่รหัสผ่าน</translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.ui" line="255"/>
        <source>OK</source>
        <translation>ตกลง</translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.cpp" line="78"/>
        <location filename="secretWidget/openletterwidget.cpp" line="85"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.cpp" line="78"/>
        <source>The password cannot be empty!</source>
        <translation>รหัสผ่านต้องไม่ว่างเปล่า!</translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.cpp" line="85"/>
        <source>Incorrect password!</source>
        <translation>รหัสผ่านผิดพลาด!</translation>
    </message>
</context>
<context>
    <name>OpenPacketWidget</name>
    <message>
        <location filename="redPacketWidget/openPack.ui" line="32"/>
        <source>OpenPacket</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.ui" line="189"/>
        <source>我是头像</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.ui" line="228"/>
        <source>我是昵称</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.ui" line="267"/>
        <location filename="redPacketWidget/openPack.cpp" line="88"/>
        <source>A red packet was sent to you</source>
        <translation>แพ็คเก็ตสีแดงถูกส่งถึงคุณ</translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.ui" line="306"/>
        <source>Best Wishes</source>
        <translation>ด้วยความปรารถนาดี</translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.cpp" line="92"/>
        <source>Red racket with a identical amount</source>
        <translation>แร็กเก็ตสีแดงที่มีจำนวนเท่ากัน</translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.cpp" line="96"/>
        <source>Red packet with a random amount</source>
        <translation>แพ็คเก็ตสีแดงที่มีจำนวนสุ่ม</translation>
    </message>
</context>
<context>
    <name>PackUnitWidget</name>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="17"/>
        <source>PackUnitWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="63"/>
        <source>PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="91"/>
        <source>11.0000000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="110"/>
        <source>11.000000000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="130"/>
        <source>Available</source>
        <translation>ที่มีจำหน่าย</translation>
    </message>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="146"/>
        <source>Frozen</source>
        <translation>แช่แข็ง</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="59"/>
        <location filename="MessageWidget/messageliststore.cpp" line="63"/>
        <source>&lt;font color=&apos;#f7931e&apos; &gt;[I was @]&lt;/font&gt;</source>
        <translation>&lt;font color=&apos;#f7931e&apos; &gt;[มีคนพูดถึงฉัน]&lt;/font&gt;</translation>
    </message>
</context>
<context>
    <name>QWebEngineViewDelegate</name>
    <message>
        <location filename="qwebengineviewdelegate.ui" line="16"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>RedPackDetail</name>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="32"/>
        <source>redPackDetail</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="100"/>
        <source>Records</source>
        <translation>บันทึก</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="138"/>
        <source>Details</source>
        <translation>รายละเอียด</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="248"/>
        <source>我是头像</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="302"/>
        <source>我是昵称</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="343"/>
        <source>我是红包备注</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="416"/>
        <source>我是获得的红包金额</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="438"/>
        <source>我是红包单位</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="495"/>
        <source>我是多少个红包</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="527"/>
        <source>我是已领取红包</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.cpp" line="74"/>
        <source> red packets </source>
        <translation>แพ็คเก็ตสีแดง</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.cpp" line="79"/>
        <source>taken </source>
        <translation>มันได้รับแล้ว</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.cpp" line="103"/>
        <source>most lucky</source>
        <translation>ขอให้โชคดี</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.cpp" line="113"/>
        <source>微软雅黑</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>RedPackHistory</name>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="32"/>
        <source>RedPackHistory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="79"/>
        <source>Red Packet History</source>
        <translation>ประวัติแพ็คเก็ตสีแดง</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="190"/>
        <location filename="redPacketWidget/RedPackHistory.ui" line="428"/>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="184"/>
        <source>Received</source>
        <translation>ที่ได้รับ</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="260"/>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="199"/>
        <source>Sent</source>
        <translation>ส่ง</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="345"/>
        <source>我是头像</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="409"/>
        <source>我是昵称</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="495"/>
        <source>我是红包数量</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="517"/>
        <source>Packets</source>
        <translation>ซองจดหมายสีแดง</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="574"/>
        <source>Tokens received can be transfered into local wallet</source>
        <translation>สัญญาณที่ได้รับสามารถโอนไปยังกระเป๋าเงินท้องถิ่น</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="84"/>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="134"/>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="175"/>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="190"/>
        <source>微软雅黑</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="185"/>
        <source>Tokens been deposited in your accounts now, which can be transfered into local wallet</source>
        <translation>โทเค็นฝากเข้าบัญชีของคุณแล้วซึ่งสามารถโอนเข้ากระเป๋าเงินท้องถิ่นได้</translation>
    </message>
</context>
<context>
    <name>SearchList</name>
    <message>
        <location filename="SearchWidget/searchlist.ui" line="16"/>
        <source>SearchList</source>
        <translation></translation>
    </message>
    <message>
        <location filename="SearchWidget/searchlist.cpp" line="120"/>
        <location filename="SearchWidget/searchlist.cpp" line="147"/>
        <location filename="SearchWidget/searchlist.cpp" line="150"/>
        <location filename="SearchWidget/searchlist.cpp" line="167"/>
        <location filename="SearchWidget/searchlist.cpp" line="228"/>
        <location filename="SearchWidget/searchlist.cpp" line="248"/>
        <source>Friends</source>
        <translation>เพื่อน</translation>
    </message>
    <message>
        <location filename="SearchWidget/searchlist.cpp" line="129"/>
        <location filename="SearchWidget/searchlist.cpp" line="185"/>
        <location filename="SearchWidget/searchlist.cpp" line="188"/>
        <location filename="SearchWidget/searchlist.cpp" line="201"/>
        <source>Group</source>
        <translation>กลุ่ม</translation>
    </message>
    <message>
        <source>Tribe</source>
        <translation type="vanished">ชนเผ่า</translation>
    </message>
    <message>
        <location filename="SearchWidget/searchlist.cpp" line="225"/>
        <source>other friends</source>
        <translation>เพื่อนคนอื่น ๆ</translation>
    </message>
</context>
<context>
    <name>SecretFileWidget</name>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="14"/>
        <source>SecretFileWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="105"/>
        <source>Secret File</source>
        <translation>ไฟล์ลับ</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="228"/>
        <source>Enter Password</source>
        <translation>ใส่รหัสผ่าน</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="271"/>
        <source>Click &quot;add&quot; button to add secret file, double click file to cancel</source>
        <translation>คลิกปุ่ม &quot;เพิ่ม&quot; เพื่อเพิ่มไฟล์ลับดับเบิลคลิกไฟล์เพื่อยกเลิก</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="332"/>
        <source>Add</source>
        <translation>เพิ่ม</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="357"/>
        <source>Send</source>
        <translation>ส่ง</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="91"/>
        <source>Open File</source>
        <translation>เปิดไฟล์</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="91"/>
        <source>File (*.*)</source>
        <translation>ไฟล์ (*.*)</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="147"/>
        <location filename="secretWidget/secretfilewidget.cpp" line="151"/>
        <location filename="secretWidget/secretfilewidget.cpp" line="173"/>
        <location filename="secretWidget/secretfilewidget.cpp" line="211"/>
        <location filename="secretWidget/secretfilewidget.cpp" line="247"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="147"/>
        <source>No File Selected!</source>
        <translation>ไม่มีไฟล์ที่เลือก!</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="151"/>
        <source>Please Enter The Password!</source>
        <translation>กรุณาใส่รหัสผ่าน!</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="162"/>
        <source>Sending……</source>
        <translation>กำลังส่ง ...</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="173"/>
        <source>File does not exist!</source>
        <translation>ไม่มีไฟล์!</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="211"/>
        <source>Encryption Failed!</source>
        <translation>การเข้ารหัสล้มเหลว!</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="247"/>
        <source>Failed to upload secret ile!</source>
        <translation>ไม่สามารถอัปโหลดไฟล์ลับ ile ได้!</translation>
    </message>
</context>
<context>
    <name>SecretImageWidget</name>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="14"/>
        <source>SecretImageWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="93"/>
        <source>Secret Image</source>
        <translation>ภาพลับ</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="210"/>
        <source>Enter Password</source>
        <translation>ใส่รหัสผ่าน</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="343"/>
        <source>Click &quot;add&quot; button to add secret image, double click file to cancel</source>
        <translation>คลิกปุ่ม &quot;เพิ่ม&quot; เพื่อเพิ่มภาพความลับดับเบิลคลิกไฟล์เพื่อยกเลิก</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="404"/>
        <source>Add</source>
        <translation>เพิ่ม</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="429"/>
        <source>Send</source>
        <translation>ส่ง</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="91"/>
        <source>Open Image</source>
        <translation>เปิดภาพ</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="91"/>
        <source>Image File(*.bmp;*.jpeg;*.jpg;*.png)</source>
        <translation>ไฟล์รูปภาพ (*. bmp; *. jpeg; *. jpg; *. png)</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="110"/>
        <location filename="secretWidget/secretimagewidget.cpp" line="114"/>
        <location filename="secretWidget/secretimagewidget.cpp" line="136"/>
        <location filename="secretWidget/secretimagewidget.cpp" line="172"/>
        <location filename="secretWidget/secretimagewidget.cpp" line="208"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="110"/>
        <source>No Image Selected! </source>
        <translation>ไม่มีภาพที่เลือก!</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="114"/>
        <source>Please Enter The Password!</source>
        <translation>กรุณาใส่รหัสผ่าน!</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="125"/>
        <source>Sending……</source>
        <translation>กำลังส่ง ...</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="136"/>
        <source>Image does not exist!</source>
        <translation>ไม่มีรูปภาพ!</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="172"/>
        <source>Encryption Failed!</source>
        <translation>การเข้ารหัสล้มเหลว!</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="208"/>
        <source>Failed to upload secret file!</source>
        <translation>การอัพโหลดไฟล์ลับล้มเหลว!</translation>
    </message>
</context>
<context>
    <name>SecretLetterWidget</name>
    <message>
        <location filename="secretWidget/secretletterwidget.ui" line="14"/>
        <source>SecretLetterWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.ui" line="120"/>
        <source>Secret Message</source>
        <translation>ข้อความลับ</translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.ui" line="237"/>
        <source>Enter Password</source>
        <translation>ใส่รหัสผ่าน</translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.ui" line="259"/>
        <source>Enter Message</source>
        <translation>ข้อความอินพุต</translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.ui" line="284"/>
        <source>Send</source>
        <translation>ส่ง</translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.cpp" line="86"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.cpp" line="86"/>
        <source>The password or message cannot be empty!</source>
        <translation>รหัสผ่านหรือข้อความต้องไม่ว่างเปล่า!</translation>
    </message>
</context>
<context>
    <name>TransAccWidget</name>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="20"/>
        <source>TransAccWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="71"/>
        <location filename="redPacketWidget/TransAccWidget.ui" line="200"/>
        <location filename="redPacketWidget/TransAccWidget.ui" line="482"/>
        <source>PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="87"/>
        <source>Transfer</source>
        <translation>โอน</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="168"/>
        <source>Escrow Balance</source>
        <translation>ยอดคงเหลือสัญญา</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="253"/>
        <source>Address</source>
        <translation>ที่อยู่</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="279"/>
        <source>Hosted Address</source>
        <translation>ที่อยู่ที่จัดการ</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="307"/>
        <source>Amount</source>
        <translation>เงิน</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="338"/>
        <source>Enter Amount</source>
        <translation>ป้อนจำนวนเงินที่โอน</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="368"/>
        <source>Payment Address</source>
        <translation>ที่อยู่การชำระเงิน</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="443"/>
        <source>Cost of Miners</source>
        <translation>ค่าใช้จ่ายขุด</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="469"/>
        <location filename="redPacketWidget/TransAccWidget.ui" line="547"/>
        <source>0.0004</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="573"/>
        <source>0.006</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="624"/>
        <source>OK</source>
        <translation>ตกลง</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="32"/>
        <source>Wallet</source>
        <translation>กระเป๋าสตางค์</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="52"/>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="58"/>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="63"/>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="77"/>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="82"/>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="92"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="52"/>
        <source>Payment Address Cannot be Empty!</source>
        <translation>ที่อยู่การชำระเงินต้องไม่ว่างเปล่า!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="58"/>
        <source>Amount of Transfer Cannot be Empty!</source>
        <translation>จำนวนเงินโอนไม่สามารถว่างเปล่า!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="63"/>
        <source>The Amount of Tranfer is Greater Than</source>
        <translation>จำนวนเงินโอนมากกว่า</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="63"/>
        <source>Balance</source>
        <translation>สมดุล</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="69"/>
        <source>Please Enter Your Login Password:</source>
        <translation>กรุณาใส่รหัสผ่านเข้าสู่ระบบของคุณ:</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="77"/>
        <source>Failed to Verify the Login Password!</source>
        <translation>ไม่สามารถตรวจสอบรหัสผ่านเข้าสู่ระบบ!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="82"/>
        <source>Login Password Cannot be Empty!</source>
        <translation>รหัสผ่านเข้าสู่ระบบต้องไม่ว่างเปล่า!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="92"/>
        <source>Incorrect Password!</source>
        <translation>รหัสผ่านไม่ถูกต้อง!</translation>
    </message>
</context>
<context>
    <name>TransmitMessageWidget</name>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="14"/>
        <source>TransmitMessage</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="155"/>
        <source>Send to</source>
        <translation>ส่งถึง</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="230"/>
        <source>Search</source>
        <translation>ค้นหา</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="295"/>
        <source>Recently</source>
        <translation>เมื่อเร็ว ๆ นี้</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="374"/>
        <location filename="childWidget/transmitmessagewidget.cpp" line="494"/>
        <location filename="childWidget/transmitmessagewidget.cpp" line="497"/>
        <source>Friends</source>
        <translation>เพื่อน</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="408"/>
        <location filename="childWidget/transmitmessagewidget.cpp" line="529"/>
        <location filename="childWidget/transmitmessagewidget.cpp" line="532"/>
        <source>Groups</source>
        <translation>กลุ่ม</translation>
    </message>
    <message>
        <source>Tribes</source>
        <translation type="vanished">ชนเผ่า</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="513"/>
        <source>OK</source>
        <translation>ตกลง</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="544"/>
        <source>Cancel</source>
        <translation>ยกเลิก</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.cpp" line="390"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.cpp" line="390"/>
        <source>Please choose a friend or a group</source>
        <translation>กรุณาเลือกเพื่อนหรือกลุ่ม</translation>
    </message>
    <message>
        <source>Please choose a friend or a tribe</source>
        <translation type="vanished">กรุณาเลือกเพื่อนหรือเผ่า</translation>
    </message>
</context>
</TS>
