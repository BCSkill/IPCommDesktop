﻿#include "imgroupstore.h"
#include "imgroupview.h"
#include "imgroupdispatcher.h"
#include "imgroupviewmodel.h"


IMGroupStore::IMGroupStore(QObject *parent) : QObject(parent)
{
}


void IMGroupStore::init()
{
	m_view = reinterpret_cast<IMGroupView*>(parent());
	m_dispatcher = m_view->dispatcher();
	m_vm = m_view->vm();

	connect(m_dispatcher, SIGNAL(sigSetGroupNameAndGroupId(QString, QString)), this, SLOT(slotSetGroupNameAndGroupId(QString, QString)));
	
}

void IMGroupStore::slotSetGroupNameAndGroupId(QString strGroupName, QString strGroupID)
{
	m_vm->setGroupNameAndGroupId(strGroupName, strGroupID);
}