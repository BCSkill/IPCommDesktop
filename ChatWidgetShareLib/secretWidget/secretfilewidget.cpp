﻿#include "secretfilewidget.h"
#include "ui_secretfilewidget.h"
#include "QStringLiteralBak.h"

extern OPDataManager  *gOPDataManager;

SecretFileWidget::SecretFileWidget(QString userID, QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::SecretFileWidget();

	ui->setupUi(this);

	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog | Qt::Tool);
	setWindowModality(Qt::WindowModal);
	setAttribute(Qt::WA_DeleteOnClose);
	setAttribute(Qt::WA_TranslucentBackground);

	QFile file(":/QSS/Resources/QSS/ChatWidgetShareLib/secretfilewidget.ui.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->selectBtn, SIGNAL(clicked(bool)), this, SLOT(slotClickedSelectBtn()));
	connect(ui->sendBtn, SIGNAL(clicked(bool)), this, SLOT(slotClickedEnterBtn()));
	connect(ui->fileListWidget, SIGNAL(itemDoubleClicked(QListWidgetItem *)),
		this, SLOT(slotCancleFile(QListWidgetItem *)));
	connect(ui->closeBtn, SIGNAL(clicked(bool)), this, SLOT(close()));

	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigCommKey(QString, QString)), this, SLOT(slotCommKey(QString, QString)));
	connect(request, SIGNAL(sigCommKey(QString, QString)), request, SLOT(deleteLater()));
	request->getCommKey(userID, gOPDataManager->getAccessToken());
}

SecretFileWidget::~SecretFileWidget()
{
	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

void SecretFileWidget::show()
{
	QWidget::show();
	this->raise();
	this->activateWindow();
}

void SecretFileWidget::slotCommKey(QString publicKey, QString privateKey)
{
	this->publicKey = "-----BEGIN PUBLIC KEY-----\n";
	this->publicKey += publicKey;
	this->publicKey += "\n-----END PUBLIC KEY-----\n";
}

//鼠标事件的处理。
void SecretFileWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void SecretFileWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void SecretFileWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

void SecretFileWidget::slotCancleFile(QListWidgetItem *item)
{
	int row = ui->fileListWidget->row(item);
	ui->fileListWidget->takeItem(row);
}

void SecretFileWidget::slotClickedSelectBtn()
{
	QString strPath = QFileDialog::getOpenFileName(this, tr("Open File"), QDir::homePath(), tr("File (*.*)"));

	if (strPath.isEmpty())
	{

	}
	else
	{
		QFileInfo info(strPath);

		QString fileIcon = "qrc:/FileType/Resources/FileType/007.png";  //默认赋值一个未知图标。
		//根据不同的文件类型赋值不同的文件图标路径。
		//先获取扩展名。
		QString suffix = info.suffix().toLower();
		//罗列所有的文件类型。
		QStringList program, zip, text, image, audio, video;
		QStringList word, excel, ppt;
		program << "exe" << "bat" << "msi";
		zip << "rar" << "zip" << "7z";
		text << "txt" << "pdf" << "htm" << "html" << "xml";
		image << "png" << "ico" << "jpg" << "jpeg" << "bmp" << "gif";
		audio << "wav" << "mp3" << "wma" << "aac" << "flac";
		video << "avi" << "wmv" << "3gp" << "mp4" << "rmvb" << "mov" << "mkv";
		video << "rm" << "mpg" << "mpeg" << "ogg";
		word << "doc" << "docx";
		excel << "xls" << "xlsx";
		ppt << "ppt" << "pptx";
		if (program.contains(suffix))
			fileIcon = "qrc:/FileType/Resources/FileType/012.png";
		if (zip.contains(suffix))
			fileIcon = "qrc:/FileType/Resources/FileType/008.png";
		if (text.contains(suffix))
			fileIcon = "qrc:/FileType/Resources/FileType/010.png";
		if (image.contains(suffix))
			fileIcon = "qrc:/FileType/Resources/FileType/005.png";
		if (audio.contains(suffix))
			fileIcon = "qrc:/FileType/Resources/FileType/013.png";
		if (video.contains(suffix))
			fileIcon = "qrc:/FileType/Resources/FileType/004.png";
		if (word.contains(suffix))
			fileIcon = "qrc:/FileType/Resources/FileType/002.png";
		if (excel.contains(suffix))
			fileIcon = "qrc:/FileType/Resources/FileType/003.png";
		if (ppt.contains(suffix))
			fileIcon = "qrc:/FileType/Resources/FileType/001.png";

		QListWidgetItem *item = new QListWidgetItem(info.fileName());
		item->setIcon(QIcon(fileIcon));
		item->setData(Qt::UserRole, info.absoluteFilePath());
		ui->fileListWidget->addItem(item);
	}
}

void SecretFileWidget::slotClickedEnterBtn()
{
	if (ui->fileListWidget->count() == 0)
		IMessageBox::tip(this, tr("Notice"), tr("No File Selected!"));
	else
	{
		if (ui->passwordEdit->text().isEmpty())
			IMessageBox::tip(this, tr("Notice"), tr("Please Enter The Password!"));
		else
		{
			QListWidgetItem *item = ui->fileListWidget->item(0);
			if (item)
			{
				//获取要发送的文件名。
				QString fileName = item->text();
				QString filePath = item->data(Qt::UserRole).toString();

				//更新界面的提示显示。
				ui->tipLabel->setText(fileName + tr("Sending……"));

				QByteArray array;
				QFile file(filePath);
				if (file.open(QIODevice::ReadOnly))
				{
					array = file.readAll();
					file.close();
				}

				if (array.isEmpty())
					IMessageBox::tip(this, tr("Notice"), fileName + tr("File does not exist!"));
				else
				{
					//创建临时文件。
					QString uuid = QUuid::createUuid().toString();
					uuid.remove("{"); uuid.remove("}"); uuid.remove("-");
					QString tempFile = QDir::tempPath() + "/" + uuid + ".file";
					QFile temp(tempFile);
					if (temp.open(QIODevice::WriteOnly))
					{
						QString password = ui->passwordEdit->text();
						QByteArray data = gOPDataManager->encryptAES(array, password, false);
						temp.write(data);
						temp.close();

						//组织content.
						QString strWord = gOPDataManager->encryptRSA(password, publicKey);
						QVariantMap content;
						QFileInfo info(filePath);
						content.insert("password", strWord);
						content.insert("FileLocalPath", filePath);
						content.insert("FileSize", this->bytesToString(info.size()));
						content.insert("FileName", info.fileName());
						content.insert("FileType", info.suffix());
						content.insert("FileTime", info.lastModified().toString("yyyy-MM-dd hh:mm:ss"));
						content.insert("TempFile", tempFile);  //临时文件的位置，上传成功后删除。

						//上传密件。
						HttpNetWork::HttpUpLoadFile *upload = new HttpNetWork::HttpUpLoadFile;
						upload->setData(content);
						connect(upload, SIGNAL(sigUpLoadFinished(bool, QByteArray)), this, SLOT(slotUplaodFile(bool, QByteArray)));
						QVariantMap pargram;
						pargram.insert("parentId", "66662");
						pargram.insert("createUser", "6662");
						upload->StartUpLoadFile(gDataManager->getAppConfigInfo().PanServerUploadURL, tempFile, pargram);
					}
					else
					{
						IMessageBox::tip(this, tr("Notice"), tr("Encryption Failed!"));
					}
				}
			}
		}
	}
}

void SecretFileWidget::slotUplaodFile(bool success, QByteArray array)
{
	HttpNetWork::HttpUpLoadFile *upload = qobject_cast<HttpNetWork::HttpUpLoadFile*>(sender());
	QVariantMap content = upload->getData().toMap();

	if (success)
	{
		QVariantMap map = QJsonDocument::fromJson(array).toVariant().toMap();
		QString fileid = map["fileid"].toString();

		//密图url
		QString avatarURL = gDataManager->getAppConfigInfo().PanServerDownloadURL + fileid + "/download";
		content.insert("url", avatarURL);

		//生成之前删除临时文件，并去掉临时文件字段。
		QFile temp(content.value("TempFile").toString());
		temp.remove();
		content.take("TempFile");

		//生成密件的发送数据。 
		QJsonDocument doc = QJsonDocument::fromVariant(content);
		QString strContent = QString(doc.toJson());

		emit sigSecretFile(strContent);
	}
	else
	{
		QString fileName = content.value("FileName").toString();
		IMessageBox::tip(this, tr("Notice"), fileName + tr("Failed to upload secret ile!"));
	}

	//发送一个完毕以后，就删除一个列表项。
	if (ui->fileListWidget->count() > 0)  //安全起见先进行判断。
	{
		ui->fileListWidget->takeItem(0);

		if (ui->fileListWidget->count() > 0)  //如果还有的话，执行下一个的处理。
			slotClickedEnterBtn();
		else
		{
			this->close();                   //没有就退出。
		}
	}
}

QString SecretFileWidget::bytesToString(qint64 bytes)
{
	QString result;
	if (bytes < 1024)
	{
		int size = (int)bytes;
		result = QString::number(size) + QString("Bytes");
		return result;
	}
	else
	{
		bytes = bytes / 1024;
		if (bytes < 1024)
		{
			int size = (int)bytes;
			result = QString::number(size) + QString("KB");
			return result;
		}
		else
		{
			bytes = bytes / 1024;
			if (bytes < 1024)
			{
				int size = (int)bytes;
				result = QString::number(size) + QString("MB");
				return result;
			}
			else
			{
				bytes = bytes / 1024;
				int size = (int)bytes;
				result = QString::number(size) + QString("GB");
				return result;
			}
		}
	}
}



