﻿#include "secretimagewidget.h"
#include "ui_secretimagewidget.h"
#include "QStringLiteralBak.h"

extern OPDataManager  *gOPDataManager;

SecretImageWidget::SecretImageWidget(QString userID, QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::SecretImageWidget();

	ui->setupUi(this);

	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog | Qt::Tool);
	setWindowModality(Qt::WindowModal);
	setAttribute(Qt::WA_DeleteOnClose);
	setAttribute(Qt::WA_TranslucentBackground);

	QFile file(":/QSS/Resources/QSS/ChatWidgetShareLib/secretimagewidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->selectBtn, SIGNAL(clicked(bool)), this, SLOT(slotClickedSelectBtn()));
	connect(ui->sendBtn, SIGNAL(clicked(bool)), this, SLOT(slotClickedEnterBtn()));
	connect(ui->fileListWidget, SIGNAL(itemDoubleClicked(QListWidgetItem *)),
		this, SLOT(slotCancleFile(QListWidgetItem *)));
	connect(ui->closeBtn, SIGNAL(clicked(bool)), this, SLOT(close()));

	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigCommKey(QString, QString)), this, SLOT(slotCommKey(QString, QString)));
	connect(request, SIGNAL(sigCommKey(QString, QString)), request, SLOT(deleteLater()));
	request->getCommKey(userID, gOPDataManager->getAccessToken());
}

SecretImageWidget::~SecretImageWidget()
{
	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

void SecretImageWidget::show()
{
	QWidget::show();
	this->raise();
	this->activateWindow();
}

void SecretImageWidget::slotCommKey(QString publicKey, QString privateKey)
{
	this->publicKey = "-----BEGIN PUBLIC KEY-----\n";
	this->publicKey += publicKey;
	this->publicKey += "\n-----END PUBLIC KEY-----\n";
}

//鼠标事件的处理。
void SecretImageWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void SecretImageWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void SecretImageWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

void SecretImageWidget::slotCancleFile(QListWidgetItem *item)
{
	int row = ui->fileListWidget->row(item);
	ui->fileListWidget->takeItem(row);
}

void SecretImageWidget::slotClickedSelectBtn()
{
	QString strPath = QFileDialog::getOpenFileName(this, tr("Open Image"), QDir::homePath(), tr("Image File(*.bmp;*.jpeg;*.jpg;*.png)"));

	if (strPath.isEmpty())
	{

	}
	else
	{
		QFileInfo info(strPath);
		QListWidgetItem *item = new QListWidgetItem(info.fileName());
		item->setIcon(QIcon(strPath));
		item->setData(Qt::UserRole, info.absoluteFilePath());
		ui->fileListWidget->addItem(item);
	}
}

void SecretImageWidget::slotClickedEnterBtn()
{
	if (ui->fileListWidget->count() == 0)
		IMessageBox::tip(this, tr("Notice"), tr("No Image Selected! "));
	else
	{
		if (ui->passwordEdit->text().isEmpty())
			IMessageBox::tip(this, tr("Notice"), tr("Please Enter The Password!"));
		else
		{
			QListWidgetItem *item = ui->fileListWidget->item(0);
			if (item)
			{
				//获取要发送的文件名。
				QString fileName = item->text();
				QString filePath = item->data(Qt::UserRole).toString();

				//更新界面的提示显示。
				ui->tipLabel->setText(fileName + tr("Sending……"));

				QByteArray array;
				QFile file(filePath);
				if (file.open(QIODevice::ReadOnly))
				{
					array = file.readAll();
					file.close();
				}

				if (array.isEmpty())
					IMessageBox::tip(this, tr("Notice"), fileName + tr("Image does not exist!"));
				else
				{
					//创建临时文件。
					QString uuid = QUuid::createUuid().toString();
					uuid.remove("{"); uuid.remove("}"); uuid.remove("-");
					QString tempFile = QDir::tempPath() + "/" + uuid + ".file";
					QFile temp(tempFile);
					if (temp.open(QIODevice::WriteOnly))
					{
						QString password = ui->passwordEdit->text();
						QByteArray data = gOPDataManager->encryptAES(array, password, false);
						temp.write(data);
						temp.close();

						//组织content.
						QString strWord = gOPDataManager->encryptRSA(password, publicKey);
						QVariantMap content;
						QFileInfo info(filePath);

						content.insert("password", strWord);
						content.insert("isRawPic", "true");
						content.insert("path", info.absoluteFilePath());
						content.insert("TempFile", tempFile);  //临时文件的位置，上传成功后删除。

						//上传密件。
						HttpNetWork::HttpUpLoadFile *upload = new HttpNetWork::HttpUpLoadFile;
						upload->setData(content);
						connect(upload, SIGNAL(sigUpLoadFinished(bool, QByteArray)), this, SLOT(slotUplaodImage(bool, QByteArray)));
						QVariantMap pargram;
						pargram.insert("parentId", "66662");
						pargram.insert("createUser", "6662");
						upload->StartUpLoadFile(gDataManager->getAppConfigInfo().PanServerUploadURL, tempFile, pargram);
					}
					else
					{
						IMessageBox::tip(this, tr("Notice"), tr("Encryption Failed!"));
					}
				}
			}
		}
	}
}

void SecretImageWidget::slotUplaodImage(bool success, QByteArray array)
{
	HttpNetWork::HttpUpLoadFile *upload = qobject_cast<HttpNetWork::HttpUpLoadFile*>(sender());
	QVariantMap content = upload->getData().toMap();

	if (success)
	{
		QVariantMap map = QJsonDocument::fromJson(array).toVariant().toMap();
		QString fileid = map["fileid"].toString();

		//密图url
		QString avatarURL = gDataManager->getAppConfigInfo().PanServerDownloadURL + fileid + "/download";
		content.insert("url", avatarURL);

		//生成之前删除临时文件，并去掉临时文件字段。
		QFile temp(content.value("TempFile").toString());
		temp.remove();
		content.take("TempFile");

		//生成密件的发送数据。 
		QJsonDocument doc = QJsonDocument::fromVariant(content);
		QString strContent = QString(doc.toJson());

		emit sigSecretImage(strContent);
	}
	else
	{
		QString fileName = content.value("FileName").toString();
		IMessageBox::tip(this, tr("Notice"), fileName + tr("Failed to upload secret file!"));
	}

	//发送一个完毕以后，就删除一个列表项。
	if (ui->fileListWidget->count() > 0)  //安全起见先进行判断。
	{
		ui->fileListWidget->takeItem(0);

		if (ui->fileListWidget->count() > 0)  //如果还有的话，执行下一个的处理。
			slotClickedEnterBtn();
		else
		{
			this->close();                   //没有就退出。
		}
	}
}



