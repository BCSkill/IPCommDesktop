﻿#include "openletterwidget.h"
#include "ui_openletterwidget.h"
#include "QStringLiteralBak.h"

extern OPDataManager  *gOPDataManager;

OpenLetterWidget::OpenLetterWidget(QString data, QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::OpenLetterWidget();

	ui->setupUi(this);

	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog | Qt::Tool | Qt::WindowStaysOnTopHint);
	setWindowModality(Qt::WindowModal);
	setAttribute(Qt::WA_DeleteOnClose);
	setAttribute(Qt::WA_TranslucentBackground);

	QFile file(":/QSS/Resources/QSS/ChatWidgetShareLib/openletterwidget.ui.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	QJsonDocument doc = QJsonDocument::fromJson(data.toUtf8());
	QVariantMap map = doc.toVariant().toMap();
	this->letter = map.value("content").toString();

	connect(ui->enterBtn, SIGNAL(clicked(bool)), this, SLOT(slotClickedEnterBtn()));
	connect(ui->closeBtn, SIGNAL(clicked(bool)), this, SLOT(close()));
	connect(ui->closeBtn, SIGNAL(clicked(bool)), this, SIGNAL(sigClose()));
}

OpenLetterWidget::~OpenLetterWidget()
{
	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

void OpenLetterWidget::show()
{
	QWidget::show();
	this->raise();
	this->activateWindow();
}

//鼠标事件的处理。
void OpenLetterWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void OpenLetterWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void OpenLetterWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

void OpenLetterWidget::slotClickedEnterBtn()
{
	QString password = ui->passwordEdit->text();

	if (password.isEmpty())
		IMessageBox::tip(this, tr("Notice"), tr("The password cannot be empty!"));
	else
	{
		QString strContent = gOPDataManager->decryptAES(letter.toUtf8(), password);
		ui->letterEdit->setPlainText(strContent);
		
		if (strContent.isEmpty())
			IMessageBox::tip(this, tr("Notice"), tr("Incorrect password!"));
	}
}

void OpenLetterWidget::directOpen(QString word)
{
	ui->passwordEdit->setText(word);
	slotClickedEnterBtn();
}
