﻿#include "secretletterwidget.h"
#include "ui_secretletterwidget.h"
#include "QStringLiteralBak.h"

extern OPDataManager  *gOPDataManager;

SecretLetterWidget::SecretLetterWidget(QString userID, QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::SecretLetterWidget();

	ui->setupUi(this);

	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog | Qt::Tool);
	setWindowModality(Qt::WindowModal);
	setAttribute(Qt::WA_DeleteOnClose);
	setAttribute(Qt::WA_TranslucentBackground);

	QFile file(":/QSS/Resources/QSS/ChatWidgetShareLib/secretletterwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->enterBtn, SIGNAL(clicked(bool)), this, SLOT(slotClickedEnterBtn()));
	connect(ui->closeBtn, SIGNAL(clicked(bool)), this, SLOT(close()));

	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigCommKey(QString, QString)), this, SLOT(slotCommKey(QString, QString)));
	connect(request, SIGNAL(sigCommKey(QString, QString)), request, SLOT(deleteLater()));
	request->getCommKey(userID, gOPDataManager->getAccessToken());
}

void SecretLetterWidget::slotCommKey(QString publicKey, QString privateKey)
{
	this->publicKey = "-----BEGIN PUBLIC KEY-----\n";
	this->publicKey += publicKey;
	this->publicKey += "\n-----END PUBLIC KEY-----\n";
}

SecretLetterWidget::~SecretLetterWidget()
{
	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

void SecretLetterWidget::show()
{
	QWidget::show();
	this->raise();
	this->activateWindow();
}

//鼠标事件的处理。
void SecretLetterWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void SecretLetterWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void SecretLetterWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

void SecretLetterWidget::slotClickedEnterBtn()
{
	QString password = ui->passwordEdit->text();
	QString text = ui->letterEdit->toPlainText();

	if (password.isEmpty() || text.isEmpty())
		IMessageBox::tip(this, tr("Notice"), tr("The password or message cannot be empty!"));
	else
	{
		QString strContent = gOPDataManager->encryptAES(text.toUtf8(), password);
		QString strPassword = gOPDataManager->encryptRSA(password, publicKey);

		QVariantMap map;
		map.insert("content", strContent);
		map.insert("password", strPassword);
		QJsonDocument doc = QJsonDocument::fromVariant(map);
		QString letter(doc.toJson());

		emit sigSecretLetter(password, text, letter);
		this->close();
	}
}

