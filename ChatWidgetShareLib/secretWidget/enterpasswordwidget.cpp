﻿#include "enterpasswordwidget.h"
#include "ui_enterpasswordwidget.h"
#include "globalmanager.h"
#ifndef Q_OS_WIN
#include "inline_mac.h"
#endif
#include "QStringLiteralBak.h"

extern OPDataManager  *gOPDataManager;

EnterPasswordWidget::EnterPasswordWidget(MessageInfo msg, QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::EnterPasswordWidget();

	ui->setupUi(this);

	m_pPicWidget = NULL;

	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog | Qt::Tool | Qt::WindowStaysOnTopHint);
	setWindowModality(Qt::WindowModal);
	setAttribute(Qt::WA_DeleteOnClose);
	setAttribute(Qt::WA_TranslucentBackground);

	QFile file(":/QSS/Resources/QSS/ChatWidgetShareLib/enterpasswordwidget.ui.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	timer = new QTimer(this);

	connect(ui->enterBtn, SIGNAL(clicked(bool)), this, SLOT(slotClickedEnterBtn()));
	connect(ui->closeBtn, SIGNAL(clicked(bool)), this, SIGNAL(sigQuit()));
	connect(timer, SIGNAL(timeout()), this, SLOT(slotRemoveSecretFile()));
	ui->passwordEdit->setFocus();
	this->msgInfo = msg;
	UserInfo user = gDataManager->getUserInfo();
	if (msgInfo.strMsg.contains("password") && msg.nToUserID == user.nUserID)  //包含密码字段并且是用户接收的，就是第一次解密。
	{
		OPRequestShareLib *request = new OPRequestShareLib;
		connect(request, SIGNAL(sigCommKey(QString, QString)), this, SLOT(slotCommKey(QString, QString)));
		connect(request, SIGNAL(sigCommKey(QString, QString)), request, SLOT(deleteLater()));
		request->getCommKey(QString::number(msgInfo.nToUserID), gOPDataManager->getAccessToken());
		this->show();
	}
	else   //已经展示过密码了，那么久直接要求用户输密码。
	{
		QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		QString directWord = map.value("directWord").toString();
		if (msgInfo.MessageChildType == Message_SECRETLETTER)
		{
			this->hide();
			OpenLetterWidget *letterWidget = new OpenLetterWidget(msgInfo.strMsg);
			connect(letterWidget, SIGNAL(sigClose()), this, SIGNAL(sigQuit()));
			letterWidget->show();
			if (!directWord.isEmpty())
				letterWidget->directOpen(directWord);
		}
		else
		{
			ui->tipLabel->hide();
			ui->titleLabel->setText(tr("Please enter the password"));
			ui->passwordEdit->setPlaceholderText(tr("Enter the password to view the content"));
			if (directWord.isEmpty())
			    this->show();
			else
			{
				ui->passwordEdit->setText(directWord);
				slotClickedEnterBtn();
			}
		}
	}
}

EnterPasswordWidget::~EnterPasswordWidget()
{
	if (m_pPicWidget)
	{
		delete m_pPicWidget;
		m_pPicWidget = NULL;
	}

	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

void EnterPasswordWidget::show()
{
	QWidget::show();
	this->raise();
	this->activateWindow();
}

void EnterPasswordWidget::slotCommKey(QString publicKey, QString privateKey)
{
	this->cipherKey = privateKey;
}

//鼠标事件的处理。
void EnterPasswordWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void EnterPasswordWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void EnterPasswordWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

void EnterPasswordWidget::slotEnterPassword(QString word)
{
	ui->passwordEdit->setText(word);
	slotClickedEnterBtn();
}


void EnterPasswordWidget::slotClickedEnterBtn()
{
	UserInfo user = gDataManager->getUserInfo();
	if (msgInfo.strMsg.contains("password") && msgInfo.nToUserID == user.nUserID)
	{
		QString userPWD = gDataManager->getUserInfo().strLoginPWD;
		QString password = ui->passwordEdit->text();

		if (userPWD.isEmpty())
		{
			IMessageBox::tip(this, tr("Notice"), tr("Decryption failure！Error code 113"));
			return;
		}
		if (password.isEmpty())
		{
			IMessageBox::tip(this, tr("Notice"), tr("Password cannot be empty！"));
			return;
		}

		//进行计算。
		QByteArray array = QCryptographicHash::hash(password.toUtf8(), QCryptographicHash::Sha1);
		QString string = array.toHex();

		if (string.toLower() != userPWD.toLower())
		{
			IMessageBox::tip(this, tr("Notice"), tr("Incorrect password！"));
			return;
		}
#ifdef Q_OS_WIN
		QString keyPath = gSettingsManager->getUserPath() + "/wallet/" + QString::number(user.nUserID) + "//pwr.key";
#else
		QString keyPath = getResourcePath() + "/wallet//" + QString::number(user.nUserID) + "//pwr.key";
#endif
		//解密成功，获取私钥。
		QString walletKey = gOPDataManager->Decryption(userPWD, keyPath);
		if (walletKey.isEmpty())
		{
			IMessageBox::tip(this, tr("Notice"), tr("Decryption failure！Error code 137"));
			return;
		}
		//获取原文的rsa私钥。
		QString sourceKey = gOPDataManager->decryptAES(cipherKey.toUtf8(), walletKey);
		if (sourceKey.isEmpty())
		{
			IMessageBox::tip(this, tr("Notice"), tr("Decryption failure！Error code 144"));
			return;
		}
		QString privateKey = "-----BEGIN RSA PRIVATE KEY-----\n";
		privateKey += sourceKey;
		privateKey += "\n-----END RSA PRIVATE KEY-----\n";

		QString strMsg = msgInfo.strMsg;
		QVariantMap map = QJsonDocument::fromJson(strMsg.toUtf8()).toVariant().toMap();
		QString mapWord = map.value("password").toString();
		QString strWord = gOPDataManager->decryptRSA(mapWord, privateKey);

		//修改消息内容，把已经删除密码的消息存入数据库。
		map.take("password");
		QByteArray newByte = QJsonDocument::fromVariant(map).toJson();
		msgInfo.strMsg = newByte;
		gSocketMessage->DBUpdateMessageContent(msgInfo.msgID, msgInfo.strMsg);
		//添加直接打开的标志。
		map.insert("directWord", strWord);
		QByteArray directByte = QJsonDocument::fromVariant(map).toJson();
		msgInfo.strMsg = directByte;

		IMessageBox *box = new IMessageBox;
		connect(box, SIGNAL(sigClose()), this, SLOT(slotShowPassTip()));
		box->init(tr("Notice"), tr("Please observe the surrounding environment before viewing the password to avoid leaking your password.\nThe password is only displayed once, please be sure to save the password"));
	}
	else
	{
		QString password = ui->passwordEdit->text();

		if (password.isEmpty())
			IMessageBox::tip(this, tr("Notice"), tr("The password cannot be empty！"));
		else
		{
			QString strMsg = msgInfo.strMsg;
			QVariantMap map = QJsonDocument::fromJson(strMsg.toUtf8()).toVariant().toMap();
			QString url = map.value("url").toString();

			//下载密件。
			QString tempFile = QDir::tempPath() + "/secret.file";

			HttpNetWork::HttpDownLoadFile *http = new HttpNetWork::HttpDownLoadFile;
			connect(http, SIGNAL(sigDownFinished(bool)), this, SLOT(slotDownloadFile(bool)));
			http->StartDownLoadFile(url, tempFile);
		}
	}
}

void EnterPasswordWidget::slotDownloadFile(bool success)
{
	if (success)
	{
		QString tempFile = QDir::tempPath() + "/secret.file";
		QFile file(tempFile);
		QByteArray secretByte;
		if (file.open(QIODevice::ReadOnly))
		{
			secretByte = file.readAll();
			file.close();
			file.remove();   //读取完加密版的文件以后，就将该文件删除。
		}

		if (secretByte.isEmpty())
		{
			IMessageBox::tip(this, tr("Notice"), tr("The secret picture/file is empty，please download again！"));
			return;
		}

		QString password = ui->passwordEdit->text();
		QByteArray sourceByte = gOPDataManager->decryptAES(secretByte, password, false);

		if (sourceByte.isEmpty())
		{
			IMessageBox::tip(this, tr("Notice"), tr("Incorrect password！"));
			return;
		}

		//创建临时文件。
		QString uuid = QUuid::createUuid().toString();
		uuid.remove("{"); uuid.remove("}"); uuid.remove("-");
		QString secretFile = QDir::tempPath() + "/" + uuid;
		if (msgInfo.MessageChildType == Message_SECRETIMAGE)
		{
			secretFile += ".png";
		}
		else
		{
			secretFile = QDir::tempPath() + "/";
			QString strMsg = msgInfo.strMsg;
			QVariantMap map = QJsonDocument::fromJson(strMsg.toUtf8()).toVariant().toMap();
			secretFile = secretFile + map.value("FileName").toString();
		}
// 		else
// 		{
// 			QString strMsg = msgInfo.strMsg;
// 			QVariantMap map = QJsonDocument::fromJson(strMsg.toUtf8()).toVariant().toMap();
// 			secretFile = secretFile + "." + map.value("FileType").toString();
// 		}
		//


		QFile sourceFile(secretFile);
		if (sourceFile.open(QIODevice::WriteOnly))
		{
			sourceFile.write(sourceByte);
			sourceFile.close();
		}

		QFileInfo info(sourceFile);
		//保存解密后的密件的路径。
		secretFilePath = info.absoluteFilePath();

		if (msgInfo.MessageChildType == Message_SECRETIMAGE)
		{
			//20180828wmc 屏蔽原有图片浏览窗口使用新窗口
// 			ZoomImg *zoom = new ZoomImg;
// 			zoom->OpenImg(secretFilePath);
			if (m_pPicWidget)
			{
				m_pPicWidget->close();
			}
				
            m_pPicWidget = new PicWidget(this);
			connect(m_pPicWidget, SIGNAL(sigPicClose()), this, SLOT(slotPicClose()));
			m_pPicWidget->setPath(secretFilePath, this,1);
			m_pPicWidget->show();
			//emit sigOpenPic(secretFilePath, NULL, this);
		}
		if (msgInfo.MessageChildType == Message_SECRETFILE)
		{
			QString url = "file:///" + secretFilePath;
			//展示文件内容。
			QDesktopServices::openUrl(QUrl(url));
		}

		//启动定时器并隐藏界面。
		timer->start(60000);
		this->hide();
	}
	else
	{
		IMessageBox::tip(this, tr("Notice"), tr("Fail to download the secret picture/file!"));
	}
}

void EnterPasswordWidget::slotShowPassTip()
{
	this->hide();

	QVariantMap map = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8()).toVariant().toMap();
	QString strWord = map.value("directWord").toString();
	QString showTip = tr("The password is:\"") + strWord + tr("\",Please remember the password, as the system will not save the password");
	IMessageBox *box = new IMessageBox;
	connect(box, SIGNAL(sigClose()), this, SLOT(slotOpenPassWidget()));
	box->init(tr("View  password"), showTip);
}

void EnterPasswordWidget::slotOpenPassWidget()
{
	EnterPasswordWidget *enterWidget = new EnterPasswordWidget(msgInfo);
}

void EnterPasswordWidget::slotRemoveSecretFile()
{
	QFile file(secretFilePath);

	if (file.exists())
		file.remove();
	else
	{
		timer->stop();
		emit this->sigQuit();
	}
}

void EnterPasswordWidget::slotPicClose()
{
	m_pPicWidget = NULL;
}

