﻿#include <QTimer>
#include <QFile>
#include <QMenu>
#include <QTextEdit>
#include <QTextBlock>
#include <QFileDialog>
#include <QClipboard>
#include <QMimeData>
#include <QTextDocumentFragment>
#include <QProcess>
#include <QDesktopServices>

#include "imperchatview.h"
#include "imperchatdispatcher.h"
#include "imperchatstore.h"
#include "imperchatviewmodel.h"
#include "ui_imperchat.h"
#include "imbuddy.h"
#include "immainwidget.h"
#include "childwidget/expresswidget.h"
#include "qlabelheader.h"
#include "settingsmanager.h"

#include "redPacketWidget/giveredpackwidget.h"
#include "redPacketWidget/RedPackDetail.h"
#include "redPacketWidget/openPack.h"

#include "secretWidget/secretletterwidget.h"
#include "secretWidget/secretimagewidget.h"
#include "secretWidget/secretfilewidget.h"
#include "secretWidget/openletterwidget.h"
#include "secretWidget/enterpasswordwidget.h"

#include "childWidget/noticewidget.h"
#include "profilemanager.h"
#include "opdatebasesharelib.h"
#include "imvideoplayer.h"
#include "oescreenshot.h"
#include "chatdatamanager.h"

#ifdef Q_OS_WIN
#include <tchar.h>
#endif

extern SettingsManager* gSettingsManager;
extern OPDatebaseShareLib *gOPDataBaseOpera;

extern QString gThemeStyle;

IMPerChatView::IMPerChatView(QString strBuddyId, QWidget *parent) : QWidget(parent)
, mExpressWidget(NULL), mChatHeaderImage(NULL), sendTip(NULL)
, m_bWalletBtn(false), m_WalletMenu(NULL), openPacketWidget(NULL)
, videoWidget(NULL), detailWidget(NULL), enterWidget(NULL)
, m_TimerBtn(NULL), m_TimerLeave(NULL), m_TimerInputting(NULL), m_TimerUserInputting(NULL)
, m_bTimeOut(false), m_bWalletMenu(false), copying(false), giveWidget(NULL)
{
	ui = new Ui::IMPerChat();
	ui->setupUi(this);

	this->setObjectName(strBuddyId);

	m_strBuddyId = strBuddyId;

	m_vm = new IMPerChatViewModel(this);
	m_dispatcher = new IMPerChatDispatcher(this);
	m_store = new IMPerChatStore(this);

	m_vm->init();
	m_dispatcher->init();
	m_store->init();

	initConnectSignals();

	initSelf();

	initInputStatusTimer();
	initWebView();
	initTextEditSend();

	initTextEditSendContextMenu();
	initWalletMenu();
}

IMPerChatView::~IMPerChatView()
{
	delete ui;
}

IMPerChatDispatcher* IMPerChatView::dispatcher()
{
	return m_dispatcher;
}

IMPerChatStore* IMPerChatView::store()
{
	return m_store;
}

IMPerChatViewModel* IMPerChatView::vm()
{
	return m_vm;
}


WId IMPerChatView::winId() const
{
	return (WId)this;
}

QWebEngineViewDelegate * IMPerChatView::getWebView()
{
	return ui->mWebView;
}

QPointer<ExpressWidget> IMPerChatView::getExpressWidget()
{
	if (mExpressWidget == NULL)
	{
		mExpressWidget = new ExpressWidget(this);
		rectExpressWidget = QRect(QPoint(0, ui->toolWidget->y() - mExpressWidget->height()), mExpressWidget->size());

		mExpressWidget->disconnect();
		connect(mExpressWidget, SIGNAL(sigExpressImagePath(QString)), this, SLOT(slotInsertExpressNormalImagePath(QString)));
	}

	return mExpressWidget;
}

void IMPerChatView::initConnectSignals()
{
	connect(m_vm, SIGNAL(sigSetNickNameAndBuddyId(QString, QString)),SLOT(slotSetNickNameAndBuddyId(QString, QString)));
}

void IMPerChatView::initSelf()
{
	QFile file(":/QSS/Resources/QSS/ChatWidgetShareLib/imperchat.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	this->setAcceptDrops(true);
	setAttribute(Qt::WA_DeleteOnClose, true);

	ui->mLabelPerNickChat->installEventFilter(this);

	ui->mPButtonPerFont->hide();
	ui->mPButtonPerShake->hide();

	if (m_store->isFriend(m_strBuddyId))
	{
		ui->addGroupBtn->setStyleSheet("QPushButton#addGroupBtn{border-image: url(:/PerChat/Resources/person/addusergroup.png);margin: 3px;}");
		ui->addGroupBtn->setToolTip(tr("Create Group"));
	}
	else
	{
		if (m_store->isSpecialBuddy(m_strBuddyId))
		{
			ui->addGroupBtn->hide();
		}
		else
		{
			ui->addGroupBtn->setStyleSheet("QPushButton#addGroupBtn{border-image: url(:/PerChat/Resources/person/addBuddy.png);margin: 3px;}");
			ui->addGroupBtn->setToolTip(tr("Add as a friend"));
		}
	}


#ifdef Q_OS_WIN
	if (IMMainWidget::self())
	{
		connect(IMMainWidget::self(), SIGNAL(sigParseScreenCutMessage(QString)), m_dispatcher, SIGNAL(sigParseScreenCutMessage(QString)));
	}
#endif

	if (IMMainWidget::self())
	{
		//更新HTML里的头像
		connect(IMMainWidget::self(), SIGNAL(sigUpdateBuddyHeaderImagePath(int, QString)), m_dispatcher, SIGNAL(sigUpdateHtmlBuddyHeaderImagePath(int, QString)));

		//更新HTML里的昵称
		connect(IMMainWidget::self(), SIGNAL(sigUpdateBuddyNickName(int, QString, int)), m_dispatcher, SIGNAL(sigUpdateHtmlBuddyNickName(int, QString, int)));
	}


	if (!mChatHeaderImage)
		mChatHeaderImage = new QLabelHeader(this);
	mChatHeaderImage->hide();
	mChatHeaderImage->setStyleSheet("background-color: rgba(0, 0, 0, 0);");

	connect(ui->mPButtonPerFont, SIGNAL(clicked()), this, SLOT(slotClickedSetFont()));
	connect(ui->mPButtonPerExpress, SIGNAL(clicked()), this, SLOT(slotSelectExpress()));
	connect(ui->mPButtonPerSend, SIGNAL(clicked()), this, SLOT(slotSendMessage()));

	connect(ui->mPButtonPerShake, SIGNAL(clicked()), this, SLOT(slotClickedShake()));
	connect(ui->mPButtonPerRedPacket, SIGNAL(clicked()), this, SLOT(slotClickedRedPacket()));
	connect(ui->mPButtonPerPicture, SIGNAL(clicked()), this, SLOT(slotClickedPicture()));
	connect(ui->mPButtonPerCutPicture, SIGNAL(clicked()), this, SLOT(slotClickedCutPicture()));
	connect(ui->mPButtonPerLog, SIGNAL(clicked()), this, SLOT(slotOpenPerLog()));
	connect(ui->mPButtonPerFile, SIGNAL(clicked()), this, SLOT(slotPerFile()));

	connect(ui->mPButtonSecretLetter, SIGNAL(clicked()), this, SLOT(slotClickedSendLetter()));
	connect(ui->mPButtonSecretImage, SIGNAL(clicked()), this, SLOT(slotClickedSecretImage()));
	connect(ui->mPButtonSecretFile, SIGNAL(clicked()), this, SLOT(slotClickedSecretFile()));

	connect(ui->addGroupBtn, SIGNAL(clicked()), this, SLOT(slotClickedAddGroup()));

	connect(ui->mPButtonNotice, SIGNAL(clicked()), this, SLOT(slotClickedNotice()));//通告

	sendTip = new QLabel(this);
	QPixmap tipImage(":/PerChat/Resources/person/sendtip.png");
	sendTip->resize(tipImage.size());
	sendTip->setPixmap(tipImage);
	sendTip->hide();
}

void IMPerChatView::initWalletMenu()
{
	m_WalletMenu = new QMenu(this);

	ActPower = new QAction(tr("Power Exchange"), this);
	ActPower->setIcon(QIcon(":/PerChat/Resources/person/Transfer.png"));

	connect(ActPower, SIGNAL(triggered()), this, SLOT(slotClickedTransfer()));

	ActETH = new QAction(tr("ETH Tranfer"), this);
	ActETH->setIcon(QIcon(":/PerChat/Resources/person/ETH.png"));

	connect(ActETH, SIGNAL(triggered()), this, SLOT(slotClickedETH()));

	ActBTC = new QAction(tr("BTC Tranfer"), this);
	ActBTC->setIcon(QIcon(":/PerChat/Resources/person/BTC.png"));

	connect(ActBTC, SIGNAL(triggered()), this, SLOT(slotClickedBTC()));

	ActEOS = new QAction(tr("EOS Tranfer"), this);
	ActEOS->setIcon(QIcon(":/PerChat/Resources/person/EOS.png"));

	connect(ActEOS, SIGNAL(triggered()), this, SLOT(slotClickedEOS()));
	m_WalletMenu->addAction(ActEOS);
	m_WalletMenu->addAction(ActBTC);
	m_WalletMenu->addAction(ActETH);
	m_WalletMenu->addAction(ActPower);

	QFile style(":/QSS/Resources/QSS/ChatWidgetShareLib/imperchatviewwalletmenu.qss");
	style.open(QFile::ReadOnly);
	QString sheet = QLatin1String(style.readAll());
	m_WalletMenu->setStyleSheet(sheet);
	style.close();
	//m_WalletMenu->setStyleSheet("QMenu{background-color: #72a4d6;color: #042439;padding-left:5px;}QMenu::item{padding:5px;padding-left:25px;padding-right:15px;}QMenu::item:selected{color: white;border:none;padding:5px;padding-left:25px;padding-right:15px;}");
	
	ui->mPButtonBTC->hide();
	ui->mPButtonETH->hide();
	ui->mPButtonEOS->hide();

	ui->mPButtonTransfer->installEventFilter(this);
	m_WalletMenu->installEventFilter(this);
}

void IMPerChatView::initTextEditSend()
{
	ui->mTextEditPer->installEventFilter(this);
	ui->mTextEditPer->setFocus();

	ui->mTextEditPer->setAcceptDrops(false);
	ui->mTextEditPer->setContextMenuPolicy(Qt::NoContextMenu);
}

void IMPerChatView::initInputStatusTimer()
{
	//textedit输入框改变时发送正在输入消息
	m_TimerBtn = new QTimer(this);
	m_TimerLeave = new QTimer(this);
	m_TimerInputting = new QTimer(this);
	m_TimerUserInputting = new QTimer(this);
	m_bTimeOut = true;
	//延迟时间结束恢复标题
	connect(m_TimerInputting, SIGNAL(timeout()), this, SLOT(slotEndInputting()));
	//textedit输入框改变时发送正在输入消息
	connect(ui->mTextEditPer, SIGNAL(textChanged()), this, SLOT(slotUserInputting()));
	connect(m_TimerUserInputting, SIGNAL(timeout()), this, SLOT(slotInputTimeOut()));
}

void IMPerChatView::dragEnterEvent(QDragEnterEvent *event)
{
	if (event->mimeData()->hasFormat("text/uri-list"))
		event->acceptProposedAction();
}

void IMPerChatView::showEvent(QShowEvent *event)
{
	ui->mTextEditPer->setFocus();
}

void IMPerChatView::dropEvent(QDropEvent *event)
{
	QList<QUrl> urls = event->mimeData()->urls();
	if (urls.isEmpty())
		return;
	foreach(QUrl url, urls)
	{
		QString file_name = url.toLocalFile();

		if (QFileInfo(file_name).isFile())
		{
			m_store->slotSendFile(file_name);
		}
	}
}

//事件过滤器
bool IMPerChatView::eventFilter(QObject *obj, QEvent *e)

{
	if (obj == ui->mWebView && e->type() == QEvent::Drop)
	{
		QDropEvent *dropEvent = (QDropEvent *)e;
		this->dropEvent(dropEvent);
		return true;
	}
	if (obj == ui->mLabelPerNickChat && e->type() == QEvent::MouseButtonRelease)
	{
		emit profilemanager::getInstance()->sigCreatePerFile(this->objectName());
	}

	if (obj == ui->mWebView)
	{
		if (e->type() == QEvent::FocusIn)
		{
			OnCloseExpress();
		}
	}
	else if (obj == ui->mTextEditPer)
	{
		if (e->type() == QEvent::FocusIn)
		{
			OnCloseExpress();
		}
		if (e->type() == QEvent::KeyPress)
		{
			QKeyEvent *event = static_cast<QKeyEvent*>(e);
			if (gSettingsManager->getSendMeg())
			{
				if (event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter)
				{
					if (Qt::ControlModifier == (event->modifiers()&(~Qt::KeypadModifier)))
					{
						slotSendMessage();
						return true;
					}
				}
			}
			else
			{
				if ((event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter) && (event->modifiers() == 0 || event->modifiers() == Qt::KeypadModifier))
				{
					slotSendMessage();
					return true;
				}
			}
			if ((event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter) &&
				(Qt::ControlModifier == (event->modifiers()&(~Qt::KeypadModifier)) || Qt::ShiftModifier == (event->modifiers()&(~Qt::KeypadModifier))))
			{
				ui->mTextEditPer->textCursor().insertText("\n");
				return true;
			}
			if (event->key() == Qt::Key_V && (event->modifiers() == Qt::ControlModifier))
			{
				QClipboard *board = QApplication::clipboard();
				const QMimeData *mimeData = board->mimeData();

				if (mimeData->hasUrls())
				{
					QList<QUrl> urls = mimeData->urls();
					foreach(QUrl url, urls)
					{
						QString filePath = url.toLocalFile();
						if (filePath.isEmpty())
						{
							//本地文件不存在，说明可能是文本内容
							if (mimeData->hasText())
							{
								QString text = mimeData->text();
								ui->mTextEditPer->textCursor().insertText(text);
							}

							return true;
						}

						//如果是文件夹，则直接返回
						if (QFileInfo(filePath).isDir())
						{
							return true;
						}
#ifdef Q_OS_WIN
						filePath.remove("file:///");
#else
						filePath.remove("file://");
#endif
						QStringList types;
						types << ".png" << ".gif" << ".bmp" << ".jpg" << ".jpeg";
						bool hasSend = false;
						foreach(QString type, types)
						{
							if (filePath.toLower().endsWith(type))
							{
								QFile file(filePath);
								if (file.open(QIODevice::ReadOnly))
								{
									QByteArray byteArray = file.readAll();
									file.close();
									QImage image = QImage::fromData(byteArray);
									if (!image.isNull())
									{
										QTextImageFormat imageFormat;

										if (image.width() > 100 || image.height() > 100)
										{
											if (image.width() > image.height())
											{
												imageFormat.setWidth(100);
											}
											else
											{
												imageFormat.setHeight(100);
											}
										}

										imageFormat.setName(filePath);
										ui->mTextEditPer->textCursor().insertImage(imageFormat);
										hasSend = true;
									}
								}
								break;
							}
						}
						if (!hasSend)
							m_store->slotSendFile(filePath);
					}
					return true;
				}
				if (mimeData->hasHtml())
				{
					QString html = mimeData->html();

					QTextDocument doc;
					doc.setHtml(html);
					QString strTmp = doc.toPlainText();
					QChar qc = 65532;
					strTmp.replace(qc, "");
					if (!strTmp.isEmpty())
					{
						ui->mTextEditPer->textCursor().insertText(strTmp);
					}
					else
					{
						QTextDocumentFragment fragment;
						fragment = QTextDocumentFragment::fromHtml(html);
						ui->mTextEditPer->textCursor().insertFragment(fragment);
					}
					return true;
				}
				if (mimeData->hasImage())
				{
					QImage image = qvariant_cast<QImage>(mimeData->imageData());

					QString imageID = QString::number(QDateTime::currentDateTime().toTime_t());
#ifdef Q_OS_WIN
					QString strPath = gSettingsManager->getUserPath() + "/Clipboard/" + imageID + ".png";
#else
					QString strPath = gSettingsManager->getUserPath() + "/Clipboard/" + imageID + ".png";
#endif
					QDir fileDir = QFileInfo(strPath).absoluteDir();
					QString strFileDir = QFileInfo(strPath).absolutePath();
					if (!fileDir.exists())
						fileDir.mkpath(strFileDir);

					image.save(strPath, "PNG");
					m_vm->insertImageToTextEditSend(strPath);

					return true;
				}
				if (mimeData->hasText())
				{
					QString text = mimeData->text();
					ui->mTextEditPer->textCursor().insertText(text);

					return true;
				}
			}

			if (event->key() == Qt::Key_Up || event->key() == Qt::Key_Down)
			{
				QString text = ui->mTextEditPer->toPlainText();
				QString strText = ui->mTextEditPer->toPlainText();
				if (!text.contains("\n") && strText.isEmpty())
					emit m_dispatcher->sigKeyUpDown(event);
			}
		}
		if (e->type() == QEvent::KeyRelease)
		{
			QKeyEvent *event = static_cast<QKeyEvent*>(e);
			if (event->key() == Qt::Key_Control)
				copying = false;
		}
	}
	if (obj == ui->mPButtonTransfer && e->type() == QEvent::Enter)
	{
		m_bWalletBtn = true;
		connect(m_TimerBtn, SIGNAL(timeout()), this, SLOT(slotWalletMenu()));
		m_TimerBtn->start(500);
	}
	if (obj == ui->mPButtonTransfer && e->type() == QEvent::Leave)
	{
		m_TimerBtn->stop();
		m_bWalletBtn = false;
	}
	if (obj == m_WalletMenu && e->type() == QEvent::Enter)
	{
		m_TimerLeave->stop();
		m_bWalletMenu = true;
	}
	if (obj == m_WalletMenu && e->type() == QEvent::Leave)
	{
		m_bWalletMenu = false;
		connect(m_TimerLeave, SIGNAL(timeout()), this, SLOT(slotCloseWalletLeave()));
		m_TimerLeave->start(500);
	}

	return QWidget::eventFilter(obj, e);
}

void IMPerChatView::slotCloseWalletLeave()
{
	QPoint p1 = ui->mPButtonTransfer->mapToGlobal(QPoint(0, 0));
	QRect rect1 = ui->mPButtonTransfer->rect();
	int ix = rect1.width();
	int iy = rect1.height();
	rect1.setX(p1.x());
	rect1.setY(p1.y());
	rect1.setWidth(ix);
	rect1.setHeight(iy);
	QPoint pos1 = cursor().pos();
	if (!rect1.contains(pos1))
	{
		if (!m_bWalletMenu)
		{
			m_TimerLeave->stop();
			m_bWalletBtn = false;
			m_WalletMenu->close();
		}
	}
}

void IMPerChatView::slotWalletMenu()
{
	m_TimerBtn->stop();
	if (m_bWalletBtn)
	{
		m_bWalletBtn = true;
		QPoint p1 = ui->mPButtonTransfer->mapToGlobal(QPoint(0, 0));
		QSize size1 = m_WalletMenu->sizeHint();
		p1.setX(p1.x() - size1.width() / 2 + 20);
		p1.setY(p1.y() - size1.height());
		connect(m_TimerLeave, SIGNAL(timeout()), this, SLOT(slotCloseWalletLeave()));
		m_TimerLeave->start(500);
		m_WalletMenu->exec(p1);
	}
}

void IMPerChatView::slotUserInputting()
{
	if (m_bTimeOut == true)
	{
		m_bTimeOut = false;
		emit m_dispatcher->sigUserInputting(1);//1代表正在输入
		m_TimerUserInputting->stop();
		m_TimerUserInputting->start(4000);
	}
}

void IMPerChatView::slotEndInputting()
{
	m_TimerInputting->stop();
	ui->mLabelPerNickChat->setText(m_vm->getNickName());
	ResizeNameLabel(m_vm->getNickName());
}

void IMPerChatView::slotInputTimeOut()
{
	m_bTimeOut = true;
}

void IMPerChatView::initTextEditSendContextMenu()
{
	m_Menu = new QMenu(this);

	ActCopy = new QAction(tr("Copy"), this);
	connect(ActCopy, SIGNAL(triggered()), this, SLOT(slotCopy()));

	ActCut = new QAction(tr("Cut"), this);
	connect(ActCut, SIGNAL(triggered()), this, SLOT(slotCut()));

	ActPaste = new QAction(tr("Paste"), this);
	connect(ActPaste, SIGNAL(triggered()), this, SLOT(slotPaste()));

	ActDel = new QAction(tr("Delete"), this);
	connect(ActDel, SIGNAL(triggered()), this, SLOT(slotDel()));

	QFile style(":/QSS/Resources/QSS/ChatWidgetShareLib/imperchatviewmenu.qss");
	style.open(QFile::ReadOnly);
	QString sheet = QLatin1String(style.readAll());
	m_Menu->setStyleSheet(sheet);
	style.close();
	//m_Menu->setStyleSheet("QMenu{background-color: #72a4d6;color: #042439;}QMenu::item{padding:4px;padding-left:20px;padding-right:30px;}QMenu::item:selected{color: white;border:none;padding:4px;padding-left:20px;padding-right:30px;}");
}

void IMPerChatView::initWebView()
{
	ui->mWebView->bindChatId(m_strBuddyId);//绑定chat id

	QWebChannel * pWebChannel = QWebEngineViewManager::instance()->m_settingFlagsMap[ui->mWebView->engine()->page()].bindWebChannel;
	WebObjectShareLib* bindChatManagerObject = QWebEngineViewManager::instance()->m_settingFlagsMap[ui->mWebView->engine()->page()].bindChatManagerObject;

	if (!pWebChannel)
	{
		pWebChannel = new QWebChannel();

		bindChatManagerObject = new WebObjectShareLib();

		pWebChannel->registerObject("chat_manager", bindChatManagerObject);

		ui->mWebView->page()->setWebChannel(pWebChannel);

		QWebEngineViewManager::instance()->m_settingFlagsMap[ui->mWebView->engine()->page()].bindWebChannel = pWebChannel;
		QWebEngineViewManager::instance()->m_settingFlagsMap[ui->mWebView->engine()->page()].bindChatManagerObject = bindChatManagerObject;
	}

	connect(bindChatManagerObject, SIGNAL(sigOnChatIFrameLoad(QString)), ui->mWebView, SLOT(slotOnChatIFrameLoad(QString)));//IFRAME加载完成事件绑定

	WebObjectShareLib* pWebObject = new WebObjectShareLib(this);
	pWebChannel->registerObject(QString("chat_") + ui->mWebView->chatId(), pWebObject);

	ui->mWebView->page()->setWebChannel(pWebChannel);

	connect(pWebObject, SIGNAL(sigZoomImg(QString)), m_dispatcher, SIGNAL(sigZoomImg(QString)));
	connect(pWebObject, SIGNAL(sigVideoPlay(QString)), this, SLOT(slotVideoPlay(QString)));
	connect(pWebObject, SIGNAL(sigOpenFile(QString)), this, SLOT(slotOpenFile(QString)));
	connect(pWebObject, SIGNAL(sigOpenDocument(QString)), this, SLOT(slotOpenDocument(QString)));
	connect(pWebObject, SIGNAL(sigCancleLoadorDownLoad(QString)), this, SLOT(slotCancelLoadorDownLoad(QString)));
	connect(pWebObject, SIGNAL(sigSendFileByID(QString)), this, SLOT(slotSendFileByID(QString)));
	connect(pWebObject, SIGNAL(sigGetFile(QString)), m_dispatcher, SIGNAL(sigGetFile(QString)));
	connect(pWebObject, SIGNAL(sigSaveFile(QString)), m_dispatcher, SIGNAL(sigSaveFile(QString)));
	connect(pWebObject, SIGNAL(sigDrag(QStringList)), m_dispatcher, SIGNAL(sigDrags(QStringList)));
	connect(pWebObject, SIGNAL(sigOpenUrl(QString)), this, SLOT(slotOpenLink(QString)));
	connect(pWebObject, SIGNAL(sigOpenSecret(QString)), this, SLOT(slotOpenSecret(QString)));
	connect(pWebObject, SIGNAL(sigMsgID(QString)), this, SLOT(slotMsgID(QString)));
	connect(pWebObject, SIGNAL(sigTransmit(QString)), m_dispatcher, SIGNAL(sigTransmit(QString)));
	connect(pWebObject, SIGNAL(sigPopUpMenu(bool, bool, QString)), this, SLOT(slotPopUpMenu(bool, bool, QString)));
	connect(pWebObject, SIGNAL(sigClickUserHeader(QString)), this, SLOT(slotClickUserHeader(QString)));
	connect(pWebObject, SIGNAL(sigShowMore()), m_dispatcher, SIGNAL(sigLoadMore()));


	if (gThemeStyle == "White")
	{
		ui->mWebView->page()->setBackgroundColor(QColor("#ffffff"));
	}
	else if (gThemeStyle == "Blue")
	{
		ui->mWebView->page()->setBackgroundColor(QColor("#042439"));
	}

	

	connect(ui->mWebView, SIGNAL(renderProcessTerminated(QWebEnginePage::RenderProcessTerminationStatus, int)), this, SLOT(slotWebViewRenderProcessTerminated(QWebEnginePage::RenderProcessTerminationStatus, int)));
	connect(ui->mWebView, SIGNAL(loadFinished(bool)), m_dispatcher, SIGNAL(sigWebEngineFinish(bool)));

	ui->mWebView->loadChatManager();

	ui->mWebView->installEventFilter(this);

}

void IMPerChatView::ExpressHide(QRect rect, QPoint pos)
{
	if (rect.contains(pos))
	{
		OnCloseExpress();
	}
}

void IMPerChatView::mousePressEvent(QMouseEvent *event)
{
	QRect rect = QRect(ui->mWebView->pos() + this->pos(), ui->mWebView->size());
	ExpressHide(rect, event->pos());
	rect = QRect(ui->titleWidget->pos() + this->pos(), ui->titleWidget->size());
	ExpressHide(rect, event->pos());
	rect = QRect(ui->toolWidget->pos() + this->pos(), ui->toolWidget->size());
	ExpressHide(rect, event->pos());
	rect = QRect(ui->mLabelPerNickChat->pos() + this->pos(), ui->mLabelPerNickChat->size());
	ExpressHide(rect, event->pos());
	if (!rectExpressWidget.contains(event->pos()))
	{
		OnCloseExpress();
	}
	QPoint eventPos = event->pos();
	QPoint TestPos = ui->mTextEditPer->pos();
	QRect TextRect = ui->mTextEditPer->rect();
	if ((eventPos.x() >= TestPos.x() && eventPos.x() <= TestPos.x() + TextRect.width()) &&
		(eventPos.y() >= TestPos.y() && eventPos.y() <= TestPos.y() + TextRect.height()))
	{
		m_Menu->clear();
		QString strText = ui->mTextEditPer->textCursor().selectedText();
		if (strText != "")
		{
			m_Menu->addAction(ActCopy);
			m_Menu->addAction(ActCut);
			m_Menu->addAction(ActPaste);
			m_Menu->addAction(ActDel);
			m_Menu->exec(event->globalPos());
		}
		else
		{
			m_Menu->addAction(ActPaste);
			m_Menu->exec(event->globalPos());
		}
	}
	return QWidget::mousePressEvent(event);
}

void IMPerChatView::slotSetNickNameAndBuddyId(QString strText, QString strBuddyID)
{
	ui->mLabelPerNickChat->setText(strText);
	ui->mLabelPerNickChat->setObjectName(strBuddyID);
	QFont font;
	font.setFamily(QString::fromUtf8("\345\276\256\350\275\257\351\233\205\351\273\221"));
	font.setPointSize(16);
	QFontMetrics fm(font);
	int str_width = fm.width(strText);
	ui->mLabelPerNickChat->setFixedSize(str_width + 8, ui->mLabelPerNickChat->height());
}

QTextEdit* IMPerChatView::getTextEditSend()
{
	return ui->mTextEditPer;
}

void IMPerChatView::startInputting()
{
	m_TimerInputting->stop();
	QString strNewName = m_vm->getNickName() + (tr("（Typing...）"));
	ui->mLabelPerNickChat->setText(strNewName);
	ResizeNameLabel(strNewName);
	m_TimerInputting->start(5000);
}

void IMPerChatView::endInputting()
{
	m_TimerInputting->stop();
	ui->mLabelPerNickChat->setText(m_vm->getNickName());
	ResizeNameLabel(m_vm->getNickName());
}

void IMPerChatView::ResizeNameLabel(QString strNewName)
{
	QFont font;
	font.setFamily(QString::fromUtf8("\345\276\256\350\275\257\351\233\205\351\273\221"));
	font.setPointSize(16);
	QFontMetrics fm(font);
	int str_width = fm.width(strNewName);
	ui->mLabelPerNickChat->setFixedSize(str_width + 8, ui->mLabelPerNickChat->height());
}

void IMPerChatView::OnCloseExpress()
{
	if (mExpressWidget != NULL)
		mExpressWidget->deleteLater();
	mbExpress_widget_is_showing = false;    // wxd add   更新表情窗口状态为隐藏
}


void IMPerChatView::slotInsertExpressNormalImagePath(QString strImgPath)
{
	OnCloseExpress();

	QTextImageFormat imageFormat;
	imageFormat.setName(":/expression/Resources" + strImgPath);
	ui->mTextEditPer->textCursor().insertImage(imageFormat);

	ui->mTextEditPer->setFocus();
}

void IMPerChatView::slotClickedSetFont()
{
	//不做处理
}

void IMPerChatView::slotClickedShake()
{
	//不做处理
}

void IMPerChatView::slotSelectExpress()
{
	if (getExpressWidget() != NULL)
	{
		if (!mbExpress_widget_is_showing)
		{
			mExpressWidget->showNormalEmotion(QPoint());   // 不能在 ExpressWidget 类里面 move, 因为GroupChatWidget是父窗口, 子窗口内部无法根据父窗口的位置移动 wxd note
			mExpressWidget->move(QPoint(0, ui->toolWidget->y() - mExpressWidget->height()));
			mExpressWidget->show();
			mbExpress_widget_is_showing = true;
		}
		else
		{
			mExpressWidget->hide();
			mbExpress_widget_is_showing = false;
		}
	}
}


void IMPerChatView::slotSendMessage()
{
	OnCloseExpress();
	QString strText = ui->mTextEditPer->toPlainText();

	if (strText.isEmpty())
	{
		if (sendTip->isHidden())
		{
			QTimer *timer = new QTimer(this);
			connect(timer, SIGNAL(timeout()), sendTip, SLOT(hide()));
			int x = ui->toolWidget->x() + ui->mPButtonPerSend->pos().x() - 140;
			int y = ui->toolWidget->y() + ui->mPButtonPerSend->pos().y() + 30;
			sendTip->move(x, y);
			sendTip->show();
			timer->setSingleShot(true);
			timer->start(3000);
		}
		return;
	}
	else
	{
		QTextDocument *document = ui->mTextEditPer->document();
#ifdef Q_OS_WIN
		QTextBlock &currentBlock = document->begin();
#else
		QTextBlock currentBlock = document->begin();
#endif
		QTextBlock::iterator it;

		QString saveText;
		QString html;
		while (true)
		{
			for (it = currentBlock.begin(); !(it.atEnd());)
			{
				QTextFragment currentFragment = it.fragment();
				QTextImageFormat newImageFormat = currentFragment.charFormat().toImageFormat();
				if (newImageFormat.isValid()) {
					// 判断出这个fragment为image
					++it;

					//先判断是不是表情。
					QString name = newImageFormat.name();
					if (name.startsWith(":/expression/Resources"))   //表情。
					{
						int len = currentFragment.length();
						for (int i = 0; i < len; i++)//相同的多个表情会导致len>1，原因不明
						{
							QString strImgPath = name.remove(":/expression/Resources");
							QString qrc_path("<img src='qrc:/expression/Resources");
							qrc_path = qrc_path + strImgPath + "'/>";
							//发送表情
							QString str_img_description = ExpressWidget::GetDescriptionByImagePath(strImgPath);
							saveText += str_img_description;
							html += qrc_path;
						}
					}
					else
					{
						if (!saveText.isEmpty())
						{
							m_store->SendTextMessage(saveText, html);
							saveText.clear();
							html.clear();
						}
						m_store->SendPicture(newImageFormat.name());
					}

					continue;
				}


				if (currentFragment.isValid())
				{
					++it;
					QString text = currentFragment.text();
					saveText += text;
					html += text;
				}

			}

			currentBlock = currentBlock.next();
			if (!currentBlock.isValid())
			{
				if (!saveText.isEmpty())
					m_store->SendTextMessage(saveText, html);
				break;
			}
			else
			{
				if (!saveText.isEmpty())
				{
					saveText.append("\n");
					html.append("\n");
				}
			}
		}

	}

	ui->mTextEditPer->clear();
}

void IMPerChatView::slotClickedRedPacket()
{
	if (giveWidget)
		giveWidget->close();

	giveWidget = new GiveRedPackWidget;
	connect(giveWidget, SIGNAL(sigClose()), this, SLOT(slotCloseGivePackWidget()));
	connect(giveWidget, SIGNAL(sigHostingCharge(int, QString, QString, QString)), m_dispatcher, SIGNAL(sigHostingCharge(int, QString, QString, QString)));
	connect(giveWidget, SIGNAL(sigGivePacketData(QString)), m_dispatcher, SIGNAL(sigGivePacket(QString)));
	BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(this->objectName());
	giveWidget->setObjectName(buddy.strUserName);
	giveWidget->show();
}

void IMPerChatView::slotCloseGivePackWidget()
{
	if (giveWidget)
		giveWidget->close();

	giveWidget = NULL;
}

void IMPerChatView::slotClickedPicture()
{
	QString fileName = QFileDialog::getOpenFileName(this,
		tr("Open"), ".",
		tr("Image File") + QStringLiteral("(*.bmp; *.jpeg; *.jpg; *.png; *.gif)"));
	if (!fileName.isEmpty())
	{
		m_vm->insertImageToTextEditSend(fileName);
	}
	OnCloseExpress();
	ui->mTextEditPer->setFocus();
}

void IMPerChatView::slotClickedCutPicture()
{
	OnCloseExpress();
#ifdef Q_OS_WIN
	TCHAR szAppPath[MAX_PATH] = { 0 };
	_stprintf(szAppPath, L"0x%x,%d %s", IMMainWidget::self()->winId(), winId(), gSettingsManager->getLanguage().toStdWString().c_str());
	ShellExecute(HWND(this->winId()), L"open", L"ScreenShotTool.exe", szAppPath, NULL, SW_SHOW);
#else
	OEScreenshot *screenCut = OEScreenshot::Instance();
	connect(screenCut, SIGNAL(sigScreenComplete()), this, SLOT(slotSendScreenShotPic()));
	connect(screenCut, SIGNAL(sigScreenCanclePixMap()), this, SLOT(slotScreenCanclePixMap()));
#endif
	ui->mTextEditPer->setFocus();
}

void IMPerChatView::slotScreenCanclePixMap()
{
#ifdef Q_OS_MAC
	OEScreenshot *act = qobject_cast<OEScreenshot*>(sender());
	if (act)
	{
		act->showNormal();
		act->close();
        emit m_dispatcher->sigShowNormalWindow();
	}
#endif
}

void IMPerChatView::slotSendScreenShotPic()
{
#ifdef Q_OS_MAC
	OEScreenshot *act = qobject_cast<OEScreenshot*>(sender());
	if (act)
	{
		act->showNormal();
		act->close();
        emit m_dispatcher->sigShowNormalWindow();
	}
#endif
	QString imageID = QString::number(QDateTime::currentDateTime().toTime_t());
#ifdef Q_OS_WIN
	QString strPath = gSettingsManager->getUserPath() + "/Clipboard/" + imageID + ".png";
#else
	QString strPath = gSettingsManager->getUserPath() + "/Clipboard/" + imageID + ".png";
#endif
	QDir fileDir = QFileInfo(strPath).absoluteDir();
	QString strFileDir = QFileInfo(strPath).absolutePath();
	if (!fileDir.exists())
		fileDir.mkpath(strFileDir);

	QClipboard *board = QApplication::clipboard();
	const QMimeData *mimeData = board->mimeData();
	if (mimeData->hasImage())
	{
		QImage image = qvariant_cast<QImage>(mimeData->imageData());
		image.save(strPath, "PNG");
		m_vm->insertImageToTextEditSend(strPath);
	}
}

void IMPerChatView::slotOpenPerLog()
{
	emit m_dispatcher->sigOpenPerLog(objectName());
}

void IMPerChatView::slotPerFile()
{
	QString strFileName = QFileDialog::getOpenFileName(this, tr("Send"), ".", tr("All(*.*)"));

	if (!strFileName.isEmpty())
		emit m_dispatcher->sigSendFile(strFileName);
}

void IMPerChatView::slotClickedSendLetter()
{
	SecretLetterWidget *letterWidget = new SecretLetterWidget(this->objectName());
	connect(letterWidget, SIGNAL(sigSecretLetter(QString, QString, QString)), m_dispatcher, SIGNAL(sigSendSecretLetter(QString, QString, QString)));
	letterWidget->show();
}

void IMPerChatView::slotClickedSecretImage()
{
	SecretImageWidget *imageWidget = new SecretImageWidget(this->objectName());
	connect(imageWidget, SIGNAL(sigSecretImage(QString)), m_dispatcher, SIGNAL(sigSendSecretImage(QString)));
	imageWidget->show();
}

void IMPerChatView::slotClickedSecretFile()
{
	SecretFileWidget *fileWidget = new SecretFileWidget(this->objectName());
	connect(fileWidget, SIGNAL(sigSecretFile(QString)), m_dispatcher, SIGNAL(sigSendSecretFile(QString)));
	fileWidget->show();
}

void IMPerChatView::slotClickedAddGroup()
{
	QString buddyID = this->objectName();
	if (gDataBaseOpera->DBJudgeFriendIsHaveByID(buddyID))
		emit m_dispatcher->sigAddChatGroup(buddyID);
	else
	{
		emit profilemanager::getInstance()->sigCreatePerFile(this->objectName());
	}
}

void IMPerChatView::slotClickedNotice()
{
	NoticeWidget *pWidget = new NoticeWidget(this->objectName());
	connect(pWidget, SIGNAL(sigSendNotice(QMap<QString, QString>)), m_dispatcher, SIGNAL(sigSendNotice(QMap<QString, QString>)));
	pWidget->show();
}


void IMPerChatView::slotClickedTransfer()
{
	QString toUserID = ui->mLabelPerNickChat->objectName();
	AddressInfo eAddress = gOPDataBaseOpera->DBGetAddressInfo(toUserID);
	m_bWalletBtn = false;
	m_WalletMenu->close();
	emit m_dispatcher->sigTransferWindow(toUserID, eAddress.ethAddress);
}

void IMPerChatView::slotClickedETH()
{
	QString toUserID = ui->mLabelPerNickChat->objectName();
	AddressInfo eAddress = gOPDataBaseOpera->DBGetAddressInfo(toUserID);
	m_bWalletBtn = false;
	m_WalletMenu->close();
	emit m_dispatcher->sigETHWindow(toUserID, eAddress.ethAddress);
}

void IMPerChatView::slotClickedBTC()
{
	QString toUserID = ui->mLabelPerNickChat->objectName();
	AddressInfo eAddress = gOPDataBaseOpera->DBGetAddressInfo(toUserID);
	m_bWalletBtn = false;
	m_WalletMenu->close();
	emit m_dispatcher->sigBTCWindow(toUserID, eAddress.ethAddress);
}

void IMPerChatView::slotClickedEOS()
{
	QString toUserID = ui->mLabelPerNickChat->objectName();
	AddressInfo eAddress = gOPDataBaseOpera->DBGetAddressInfo(toUserID);
	m_bWalletBtn = false;
	m_WalletMenu->close();
	emit m_dispatcher->sigEOSWindow(toUserID, eAddress.ethAddress);
}

void IMPerChatView::slotCopy()
{
	QClipboard*clipBoard = QApplication::clipboard();
	clipBoard->setText(ui->mTextEditPer->textCursor().selectedText());
}

void IMPerChatView::slotCut()
{
	QClipboard*clipBoard = QApplication::clipboard();
	clipBoard->setText(ui->mTextEditPer->textCursor().selectedText());
	ui->mTextEditPer->textCursor().deleteChar();
}

void IMPerChatView::slotPaste()
{
	QClipboard *board = QApplication::clipboard();
	const QMimeData *mimeData = board->mimeData();
	if (mimeData->hasUrls())
	{
		QList<QUrl> urls = mimeData->urls();
		foreach(QUrl url, urls)
		{
			QString filePath = url.toLocalFile();
			if (filePath.isEmpty())
			{
				//本地文件不存在，说明可能是文本内容
				if (mimeData->hasText())
				{
					QString text = mimeData->text();
					ui->mTextEditPer->textCursor().insertText(text);
				}
				return;
			}

			//如果是文件夹，则直接返回
			if (QFileInfo(filePath).isDir())
			{
				return;
			}

#ifdef Q_OS_WIN
			filePath.remove("file:///");
#else
			filePath.remove("file://");
#endif
			QStringList types;
			types << ".png" << ".gif" << ".bmp" << ".jpg" << ".jpeg";
			bool hasSend = false;
			foreach(QString type, types)
			{
				if (filePath.toLower().endsWith(type))
				{
					QFile file(filePath);
					if (file.open(QIODevice::ReadOnly))
					{
						QByteArray byteArray = file.readAll();
						file.close();
						QImage image = QImage::fromData(byteArray);
						if (!image.isNull())
						{
							QTextImageFormat imageFormat;

							if (image.width() > 100 || image.height() > 100)
							{
								if (image.width() > image.height())
								{
									imageFormat.setWidth(100);
								}
								else
								{
									imageFormat.setHeight(100);
								}
							}

							QImage *image = new QImage;
							imageFormat.setName(filePath);
							ui->mTextEditPer->textCursor().insertImage(imageFormat);
							hasSend = true;
						}
					}
					break;
				}
			}

			if (!hasSend)
				emit m_dispatcher->sigSendFile(filePath);
		}
		return;
	}
	else if (mimeData->hasHtml())
	{
		QString html = mimeData->html();

		QTextDocument doc;
		doc.setHtml(html);
		QString strTmp = doc.toPlainText();
		QChar qc = 65532;
		strTmp.replace(qc, "");
		if (!strTmp.isEmpty())
		{
			ui->mTextEditPer->textCursor().insertText(strTmp);
		}
		else
		{
			QTextDocumentFragment fragment;
			fragment = QTextDocumentFragment::fromHtml(html);
			ui->mTextEditPer->textCursor().insertFragment(fragment);
		}

	}
	else if (mimeData->hasImage())
	{
		QImage image = qvariant_cast<QImage>(mimeData->imageData());

		QString imageID = QString::number(QDateTime::currentDateTime().toTime_t());
#ifdef Q_OS_WIN
		QString strPath = gSettingsManager->getUserPath() + "/Clipboard/" + imageID + ".png";
#else
		QString strPath = gSettingsManager->getUserPath() + "/Clipboard/" + imageID + ".png";
#endif
		QDir fileDir = QFileInfo(strPath).absoluteDir();
		QString strFileDir = QFileInfo(strPath).absolutePath();
		if (!fileDir.exists())
			fileDir.mkpath(strFileDir);

		image.save(strPath, "PNG");
		m_vm->insertImageToTextEditSend(strPath);
	}
	else
	{
		QClipboard*clipBoard = QApplication::clipboard();
		ui->mTextEditPer->insertPlainText(clipBoard->text());
	}
}

void IMPerChatView::slotDel()
{
	ui->mTextEditPer->textCursor().deleteChar();
}

void IMPerChatView::slotVideoPlay(QString mediaPath)
{
	if (videoWidget)
		videoWidget->close();
	videoWidget = new IMVideoPlayer;

	connect(videoWidget, SIGNAL(sigClose()), this, SLOT(slotCloseVideoWidget()));
	videoWidget->videoPlay(mediaPath);
}

void IMPerChatView::slotCloseVideoWidget()
{
	videoWidget->close();
	videoWidget = NULL;
}

void IMPerChatView::slotOpenFile(QString msgID)
{
	QString fileName = gDataBaseOpera->DBGetFileInfoLocalPath(msgID);

	if (fileName == "")
	{
		MessageInfo tmpifno = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);
		QJsonParseError jsonError;
		QString strFileName;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(tmpifno.strMsg.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QVariantMap result = jsonDocument.toVariant().toMap();
				strFileName = result["FileName"].toString();
			}
		}
		UserInfo userInfo = gDataManager->getUserInfo();
		QString currentPath = gSettingsManager->getUserPath();
		fileName = currentPath + QString("/files/") + QString("%1").arg(userInfo.nUserID) + QString("/") + strFileName;
	}
	QFile file(fileName);
	if (fileName == "" || !file.exists())
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("Empty file name or file doesn't exist!"));
	}
	else
	{
		QDesktopServices bs;
		bs.openUrl(QUrl::fromLocalFile(fileName));
	}
}

void IMPerChatView::slotOpenDocument(QString msgID)
{
	QString fileName = gDataBaseOpera->DBGetFileInfoLocalPath(msgID);
	if (fileName == "")
	{
		MessageInfo tmpifno = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);
		QJsonParseError jsonError;
		QString strFileName;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(tmpifno.strMsg.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QVariantMap result = jsonDocument.toVariant().toMap();
				strFileName = result["FileName"].toString();
			}
		}
		UserInfo userInfo = gDataManager->getUserInfo();
		QString currentPath = gSettingsManager->getUserPath();
		fileName = currentPath + QString("/files/") + QString("%1").arg(userInfo.nUserID) + QString("/") + strFileName;
	}
	if (fileName == "")
	{
		qDebug() << tr("fileName为空!OpenDocument Failed!") << endl;
	}
	else
	{
#ifdef Q_OS_WIN
		//windows系统实现定位到文件功能
		const QString explorer = "explorer";
		QStringList param;
		if (!QFileInfo(fileName).isDir())
			param << QLatin1String("/select,");
		param << QDir::toNativeSeparators(fileName);
		QProcess::startDetached(explorer, param);
#else
		QString filePath = fileName.left(fileName.lastIndexOf("/"));
		QDesktopServices::openUrl(QUrl::fromLocalFile(filePath));
#endif

	}
}

void IMPerChatView::slotCancelLoadorDownLoad(QString msgId)
{
	emit m_dispatcher->sigCancel(msgId);
}

void IMPerChatView::slotSendFileByID(QString msgID)
{
	UserInfo userInfo = gDataManager->getUserInfo();
	QString fileName = gDataBaseOpera->DBGetFileInfoLocalPath(msgID);
	QString strend = QString("DeleteByMegId(\"%1\")").arg(msgID);
	ui->mWebView->page()->runJavaScript(strend);
	gSocketMessage->DeleteByMsgId(msgID);
	emit m_dispatcher->sigSendFile(fileName);
}

void IMPerChatView::slotOpenLink(QString link)
{
	if (link.contains("."))
	{
#ifndef Q_OS_WIN
		QUrl url(link);
		QString strlink = "";
		QString strScheme = url.scheme();
		if (strScheme.isEmpty())
			strlink = "http://" + link;
		else
			strlink = link;
		QDesktopServices::openUrl(QUrl(strlink));
#else
		QDesktopServices::openUrl(QUrl::fromLocalFile(link));
#endif
	}
	else
	{
		slotOpenSecret(link);
	}
}

void IMPerChatView::slotOpenSecret(QString msgID)
{
	UserInfo userInfo = gDataManager->getUserInfo();
	QMap<QString, QList<MessageInfo> > mapTemp = gSocketMessage->DBGetAllMessage();
	QMap<QString, QList<MessageInfo> >::iterator itor = mapTemp.begin();
	for (; itor != mapTemp.end(); ++itor)
	{
		for (int i = 0; i < itor.value().size(); i++)
		{
			if (itor.value()[i].msgID == msgID.toUtf8())
			{
				//已经找到指定的消息。
				MessageInfo message = itor.value()[i];
				if (itor.value()[i].nFromUserID == userInfo.nUserID)
				{
					//用户发送的。
					if (message.MessageChildType == Message_SECRETLETTER)
					{
						OpenLetterWidget *openWidget = new OpenLetterWidget(message.strMsg);
						openWidget->show();
					}
					if (message.MessageChildType == Message_SECRETIMAGE || message.MessageChildType == Message_SECRETFILE)
					{
						if (enterWidget)
							enterWidget->close();

						enterWidget = new EnterPasswordWidget(message);
						connect(enterWidget, SIGNAL(sigQuit()), this, SLOT(slotCloseEnterWidget()));//图片浏览
					}
				}
				else
				{
					//对方发送的。
					if (enterWidget)
						enterWidget->close();

					enterWidget = new EnterPasswordWidget(message);
					connect(enterWidget, SIGNAL(sigQuit()), this, SLOT(slotCloseEnterWidget()));//图片浏览

				}
			}
		}
	}
}

void IMPerChatView::slotCloseEnterWidget()
{
	enterWidget->close();
	enterWidget = NULL;
}

void IMPerChatView::slotMsgID(QString msgID)
{
	MessageInfo msgInfo = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);

	if (msgInfo.MessageChildType == MessageType::Message_TRANSFER)
	{
		QJsonDocument json = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
		QVariantMap map = json.toVariant().toMap();
		RecordInfo record;

		UserInfo userInfo = gDataManager->getUserInfo();
		QString userID = QString::number(userInfo.nUserID);
		if (userID == map.value("sendIMUserId").toString())
		{
			//用户是发送者。
			record.status = "sender";
		}
		else
		{
			record.status = "receiver";
		}
		record.age = map.value("tradingTime").toString();
		record.from = map.value("sendWalletAddress").toString();
		record.to = map.value("receiveWalletAddress").toString();
		record.txHash = map.value("tradingID").toString();
		record.value = map.value("money").toString();
		record.txFee = map.value("txFee").toString();
		record.block = map.value("block").toString().toInt();
		record.sendId = map.value("sendIMUserId").toString();
		record.receiveId = map.value("receiveIMUserId").toString();

		emit m_dispatcher->sigTransferRecord(record);
	}
	if (msgInfo.MessageChildType == MessageType::Message_COMMON)
	{
		QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		QString userType = map.value("userType").toString();
		int id = map.value("id").toInt();
		if (userType == "user")
		{
			emit profilemanager::getInstance()->sigCreatePerFile(QString::number(id));
		}
		if (userType == "group")
		{
			GroupInfo group = gDataBaseOpera->DBGetGroupFromID(QString::number(id));
			emit profilemanager::getInstance()->sigCreateGrpFile(QString::number(id));
		}
	}
	if (msgInfo.MessageChildType == MessageType::Message_REDBAG)
	{
		UserInfo user = gDataManager->getUserInfo();

		QJsonDocument json = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
		QVariantMap map = json.toVariant().toMap();
		QString invalid = map.value("invalid").toString();

		if (msgInfo.nToUserID == user.nUserID)  //接收红包的处理。
		{
			if (invalid == "true")
			{
				//显示红包详情页面。
				if (detailWidget)
					detailWidget->close();
				detailWidget = new RedPackDetail;
				connect(detailWidget, SIGNAL(sigClose()), this, SLOT(slotCloseDetailWidget()));
				//红包发送者信息
				double coin = map.value("data").toDouble();
				if (coin < 0)
					coin = map.value("coin").toDouble();

				BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(QString::number(msgInfo.nFromUserID));
				if (buddy.strNote.isEmpty())
					buddy.strNote = buddy.strNickName;
				detailWidget->setMyInfo(buddy.strLocalAvatar, buddy.strNote, QString::number(coin, 'f', 5));

				//红包金额和领取状态信息
				QVariantMap packet = map.value("redPacket").toMap();
				QString remarks = packet.value("remarks").toString();
				double totalMoney = packet.value("totalMoney").toDouble();
				int totalSize = packet.value("totalSize").toInt();
				int remainSize = packet.value("remainSize").toInt();
				int trusteeshipCoinId = packet.value("trusteeshipCoinId").toInt();
				QString assetUnit;
				if (trusteeshipCoinId == 1)
					assetUnit = "PWR";
				detailWidget->setPackInfo(remarks, QString::number(totalMoney, 'f', 5), assetUnit, totalSize, totalSize - remainSize);

				//红包领取记录信息
				QVariantList list = map.value("list").toList();
				foreach(QVariant varRecord, list)
				{
					QVariantMap record = varRecord.toMap();
					QDateTime dateTime = QDateTime::fromTime_t(record.value("time").toLongLong() / 1000);
					double money = record.value("coin").toDouble();
					detailWidget->addPerson(user.strUserAvatarLocal, user.strUserNickName,
						dateTime.date().toString("yyyy-MM-dd"), dateTime.time().toString("hh:mm:ss"),
						QString::number(money, 'f', 5), assetUnit);
				}

				detailWidget->show();
			}
			else
			{
				QString packetId = map.value("packetId").toString();

				OPRequestShareLib *request = new OPRequestShareLib;
				connect(request, SIGNAL(sigPacketStatus(bool)), this, SLOT(slotPacketStatus(bool)));
				request->setObjectName(msgID);
				request->readRedBagStatus(QString::number(user.nUserID),
					packetId);
			}
		}
		else
		{
			int trusteeshipCoinId;
			QString packetId;

			if (invalid == "true")  //已经领取/失效
			{
				QVariantMap redPacket = map.value("redPacket").toMap();
				trusteeshipCoinId = redPacket.value("trusteeshipCoinId").toInt();
				packetId = redPacket.value("packetId").toString();
			}
			else                    //尚未领取
			{
				trusteeshipCoinId = map.value("trusteeshipCoinId").toInt();
				packetId = map.value("packetId").toString();
			}

			UserInfo user = gDataManager->getUserInfo();
			OPRequestShareLib *request = new OPRequestShareLib;
			request->setObjectName(msgID);
			connect(request, SIGNAL(sigGetPacketResult(QString)), this, SLOT(slotGetPacketResult(QString)));
			request->getRedBag(user.strAccountName, QString::number(user.nUserID),
				QString::number(trusteeshipCoinId), packetId);
		}
	}
}

void IMPerChatView::slotGetPacketResult(QString result)
{
	if (!result.isEmpty())
	{
		//获得msgID。
		OPRequestShareLib *request = qobject_cast<OPRequestShareLib *>(sender());
		QString msgID = request->objectName();

		//获取用户信息，消息结构体。
		UserInfo user = gDataManager->getUserInfo();
		MessageInfo msgInfo = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);

		//给消息体增加已经失效的标志，并写入数据库。
		QJsonDocument json = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = json.toVariant().toMap();
		map.insert("invalid", "true");
		QJsonDocument doc = QJsonDocument::fromVariant(map);
		gSocketMessage->DBUpdateMessageContent(msgID.toUtf8(), doc.toJson());

		if (msgInfo.nToUserID == user.nUserID)  //接收红包的处理
		{
			//设置红包为已打开状态。
			QString strSend = QString("setColor('%1');").arg(msgID);
			ui->mWebView->page()->runJavaScript(strSend);

			//显示红包详情页面。
			if (detailWidget)
				detailWidget->close();
			detailWidget = new RedPackDetail;
			connect(detailWidget, SIGNAL(sigClose()), this, SLOT(slotCloseDetailWidget()));

			//红包发送者信息
			double coin = map.value("data").toDouble();
			if (coin < 0)
				coin = map.value("coin").toDouble();

			BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(QString::number(msgInfo.nFromUserID));
			if (buddy.strNote.isEmpty())
				buddy.strNote = buddy.strNickName;
			detailWidget->setMyInfo(buddy.strLocalAvatar, buddy.strNote, QString::number(coin, 'f', 5));

			//红包金额和领取状态信息
			QVariantMap packet = map.value("redPacket").toMap();
			QString remarks = packet.value("remarks").toString();
			double totalMoney = packet.value("totalMoney").toDouble();
			int totalSize = packet.value("totalSize").toInt();
			int remainSize = packet.value("remainSize").toInt();
			int trusteeshipCoinId = packet.value("trusteeshipCoinId").toInt();
			QString assetUnit;
			if (trusteeshipCoinId == 1)
				assetUnit = "PWR";
			detailWidget->setPackInfo(remarks, QString::number(totalMoney, 'f', 5), assetUnit, totalSize, totalSize - remainSize);

			//红包领取记录信息
			QVariantList list = map.value("list").toList();
			foreach(QVariant varRecord, list)
			{
				QVariantMap record = varRecord.toMap();
				QDateTime dateTime = QDateTime::fromTime_t(record.value("time").toLongLong() / 1000);
				double money = record.value("coin").toDouble();
				detailWidget->addPerson(user.strUserAvatarLocal, user.strUserNickName,
					dateTime.date().toString("yyyy-MM-dd"), dateTime.time().toString("hh:mm:ss"),
					QString::number(money, 'f', 5), assetUnit);
			}

			detailWidget->show();
		}
		else    //发送红包的处理。
		{
			//显示红包详情页面。
			if (detailWidget)
				detailWidget->close();
			detailWidget = new RedPackDetail;
			connect(detailWidget, SIGNAL(sigClose()), this, SLOT(slotCloseDetailWidget()));

			//红包发送者信息
			detailWidget->setMyInfo(user.strUserAvatarLocal, user.strUserNickName, "0");

			//红包金额和领取状态信息
			QVariantMap packet = map.value("redPacket").toMap();
			QString remarks = packet.value("remarks").toString();
			double totalMoney = packet.value("totalMoney").toDouble();
			int totalSize = packet.value("totalSize").toInt();
			int remainSize = packet.value("remainSize").toInt();
			int trusteeshipCoinId = packet.value("trusteeshipCoinId").toInt();
			QString assetUnit;
			if (trusteeshipCoinId == 1)
				assetUnit = "PWR";
			detailWidget->setPackInfo(remarks, QString::number(totalMoney, 'f', 5), assetUnit, totalSize, totalSize - remainSize);

			//红包领取记录信息
			QVariantList list = map.value("list").toList();
			foreach(QVariant varRecord, list)
			{
				QVariantMap record = varRecord.toMap();
				QString imUserId = record.value("imUserId").toString();
				QString nickName = record.value("nickName").toString();
				QDateTime dateTime = QDateTime::fromTime_t(record.value("time").toLongLong() / 1000);
				double money = record.value("coin").toDouble();
				BuddyInfo recordBuddy = gDataBaseOpera->DBGetBuddyInfoByID(imUserId);
				detailWidget->addPerson(recordBuddy.strLocalAvatar, nickName,
					dateTime.date().toString("yyyy-MM-dd"), dateTime.time().toString("hh:mm:ss"),
					QString::number(money, 'f', 5), assetUnit);
			}

			detailWidget->show();
		}
	}
	else
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("Fail to check the red packet!"));
	}
}

void IMPerChatView::slotPacketStatus(bool valid)
{
	OPRequestShareLib *request = qobject_cast<OPRequestShareLib *>(sender());
	QString msgID = request->objectName();
	MessageInfo msgInfo = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);
	UserInfo user = gDataManager->getUserInfo();
	if (valid)
	{
		if (openPacketWidget)
			openPacketWidget->close();
		openPacketWidget = new OpenPacketWidget;
		connect(openPacketWidget, SIGNAL(sigClose()), this, SLOT(slotClosePacketWidget()));
		connect(openPacketWidget, SIGNAL(sigOpenPacket(QString)), this, SLOT(slotOpenPacket(QString)));

		BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(QString::number(msgInfo.nFromUserID));
		QString name = buddy.strNote;
		if (buddy.strNote.isEmpty())
			name = buddy.strNickName;

		QJsonDocument json = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
		QVariantMap map = json.toVariant().toMap();
		QString remarks = map.value("leaveMessage").toString();

		openPacketWidget->setInfo(buddy.strLocalAvatar, name, msgID, remarks, commonPack);
		openPacketWidget->show();
	}
	else
	{
		QJsonDocument json = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
		QVariantMap map = json.toVariant().toMap();
		int trusteeshipCoinId = map.value("trusteeshipCoinId").toInt();
		QString packetId = map.value("packetId").toString();
		UserInfo user = gDataManager->getUserInfo();

		OPRequestShareLib *request = new OPRequestShareLib;
		request->setObjectName(msgID);
		connect(request, SIGNAL(sigGetPacketResult(QString)), this, SLOT(slotGetPacketResult(QString)));
		request->getRedBag(user.strAccountName, QString::number(user.nUserID),
			QString::number(trusteeshipCoinId), packetId);
	}
}

void IMPerChatView::slotClosePacketWidget()
{
	openPacketWidget->close();
	openPacketWidget = NULL;
}

void IMPerChatView::slotCloseDetailWidget()
{
	detailWidget->close();
	detailWidget = NULL;
}

void IMPerChatView::slotOpenPacket(QString msgID)
{
	slotClosePacketWidget();

	MessageInfo msgInfo = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);
	QJsonDocument json = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = json.toVariant().toMap();
	int trusteeshipCoinId = map.value("trusteeshipCoinId").toInt();
	QString packetId = map.value("packetId").toString();
	UserInfo user = gDataManager->getUserInfo();

	OPRequestShareLib *request = new OPRequestShareLib;
	request->setObjectName(msgID);
	connect(request, SIGNAL(sigGetPacketResult(QString)), this, SLOT(slotGetPacketResult(QString)));
	request->getRedBag(user.strAccountName, QString::number(user.nUserID),
		QString::number(trusteeshipCoinId), packetId);
}

void IMPerChatView::slotWebViewCopy()
{
	ui->mWebView->triggerPageAction(QWebEnginePage::Copy);
}


void IMPerChatView::slotPopUpMenu(bool isShowCopy, bool isShowTransmit, QString msgId)
{
	QMenu *popMenu = new QMenu(this);
	UserInfo userInfo = gDataManager->getUserInfo();
	MessageInfo tmpifno = gSocketMessage->DBGetMessageInfoWithMsgID(msgId);

	popMenu->setStyleSheet("QMenu::item{padding-left: 5px;padding-right: 5px;}");
	//文本消息选中状态

	if (isShowCopy)
	{
		QAction *menuItem_textCopy = new QAction(tr("Copy"), this);
		connect(menuItem_textCopy, SIGNAL(triggered()), this, SLOT(slotWebViewCopy()));
		popMenu->addAction(menuItem_textCopy);
	}
	else if ((tmpifno.MessageChildType == 1) || (tmpifno.MessageChildType == 5))
	{
		QAction *menuItem_picCopy = new QAction(tr("Copy"), this);
		QVariant v = qVariantFromValue(msgId);

		menuItem_picCopy->setData(v);
		connect(menuItem_picCopy, SIGNAL(triggered()), this, SLOT(slotFileViewCopy()));
		popMenu->addAction(menuItem_picCopy);
	}

	if (tmpifno.MessageState == MESSAGE_STATE_FAILURE)
	{
		QAction *menuItem_resend = new QAction(tr("Resend"), this);
		QVariant v = qVariantFromValue(msgId);
		menuItem_resend->setData(v);
		connect(menuItem_resend, SIGNAL(triggered()), this, SLOT(slotReSend()));
		popMenu->addAction(menuItem_resend);
	}
	else if (tmpifno.MessageState == MESSAGE_STATE_UNLOADED)
	{
		QAction *menuItem_resend = new QAction(tr("Reload"), this);
		QVariant v = qVariantFromValue(msgId);
		menuItem_resend->setData(v);
		connect(menuItem_resend, SIGNAL(triggered()), this, SLOT(slotReLoad()));
		popMenu->addAction(menuItem_resend);
	}
	else if (isShowTransmit)
	{
		QAction *menuItem_transmit = new QAction(tr("Forward"), this);
		QVariant v = qVariantFromValue(msgId);
		menuItem_transmit->setData(v);

		connect(menuItem_transmit, SIGNAL(triggered()), this, SLOT(slotWebViewTransmit()));
		popMenu->addAction(menuItem_transmit);
	}

	if (true)
	{
		QAction *menuItem_clear = new QAction(tr("Clear"), this);
		connect(menuItem_clear, SIGNAL(triggered()), this, SLOT(slotWebViewClear()));
		popMenu->addAction(menuItem_clear);
	}
	if (popMenu->actions().count() > 0)
		popMenu->exec(QCursor::pos());
	delete popMenu;
}

void IMPerChatView::slotWebViewClear()
{
	ui->mWebView->page()->runJavaScript("clear_only()");
}


void IMPerChatView::slotWebViewTransmit()
{
	QAction *act = qobject_cast<QAction *>(sender());
	QVariant v = act->data();
	QString msgId = v.value<QString>();

	emit m_dispatcher->sigTransmit(msgId);
}

void IMPerChatView::slotReLoad()
{
	QAction *act = qobject_cast<QAction *>(sender());
	QVariant v = act->data();
	QString msgId = v.value<QString>();
	MessageInfo tmpifno = gSocketMessage->DBGetMessageInfoWithMsgID(msgId);
	gSocketMessage->DBMsgReLoad(tmpifno);
}

void IMPerChatView::slotReSend()
{
	QAction *act = qobject_cast<QAction *>(sender());
	QVariant v = act->data();
	QString msgId = v.value<QString>();
	MessageInfo tmpifno = gSocketMessage->DBGetMessageInfoWithMsgID(msgId);

	QString strend = QString("DeleteByMegId(\"%1\")").arg(msgId);
	ui->mWebView->page()->runJavaScript(strend);
	QString strMsg = tmpifno.strMsg;
	gSocketMessage->DeleteByMsgId(msgId);

	if (tmpifno.MessageChildType == Message_TEXT)
	{
		m_store->SendTextMessage(strMsg, strMsg);
	}
	else if (tmpifno.MessageChildType == Message_PIC)
	{
		m_store->SendPicture(strMsg);
	}
	else if (tmpifno.MessageChildType == Message_FILE)
	{
		m_store->slotSendFile(strMsg);
	}
}

void IMPerChatView::slotFileViewCopy()
{
	QAction *act = qobject_cast<QAction *>(sender());
	QVariant v = act->data();
	QString msgId = v.value<QString>();


    //UserInfo userInfo = gDataManager->getUserInfo();
	MessageInfo tmpifno = gSocketMessage->DBGetMessageInfoWithMsgID(msgId);
	QString filePath;
	if (tmpifno.MessageChildType == 1)
	{
		MessageInfo message = gSocketMessage->DBGetMessageInfoWithMsgID(msgId);
		filePath = message.strMsg;
	}
	else if (tmpifno.MessageChildType == 5)
	{
		filePath = gDataBaseOpera->DBGetFileInfoLocalPath(msgId);
	}

#ifdef Q_OS_WIN
	QString winFilePath = filePath.replace("/", "\\");
#else
    QString winFilePath = filePath;
#endif
	QMimeData *mimeData = new QMimeData;
	QList<QUrl> ourls;
	QUrl url = QUrl::fromLocalFile(winFilePath);
	ourls << url;

	mimeData->setUrls(ourls);
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setMimeData(mimeData);
}


void IMPerChatView::slotClickUserHeader(QString msgID)
{
	MessageInfo messageInfo = gSocketMessage->DBGetMessageInfoWithMsgID(msgID);
	QString groupUserID = QString::number(messageInfo.nFromUserID);
	if (!groupUserID.isEmpty())
	{
		emit profilemanager::getInstance()->sigCreatePerFile(groupUserID);
	}
}

void IMPerChatView::slotWebViewRenderProcessTerminated(QWebEnginePage::RenderProcessTerminationStatus terminationStatus, int exitCode)
{
	switch (terminationStatus)
	{
	case QWebEnginePage::NormalTerminationStatus:
		qDebug() << "webView: NormalTerminationStatus ";
		break;
	case QWebEnginePage::AbnormalTerminationStatus:
		qDebug() << "webView: AbnormalTerminationStatus ";
		break;
	case QWebEnginePage::CrashedTerminationStatus:
		qDebug() << "webView: CrashedTerminationStatus ";
		break;
	case QWebEnginePage::KilledTerminationStatus:
		qDebug() << "webView: KilledTerminationStatus ";
		break;
	default:
		break;
	}

	if (terminationStatus != QWebEnginePage::NormalTerminationStatus)
	{
		QTimer::singleShot(0, this, [=] {

			if (ChatDataManager::self())
			{
				emit ChatDataManager::self()->sigWebEngineViewTermination(ui->mWebView->chatId());
			}

		});

	}
}
