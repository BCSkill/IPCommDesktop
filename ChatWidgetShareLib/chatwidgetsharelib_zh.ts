<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AtWidget</name>
    <message>
        <location filename="SearchWidget/atwidget.ui" line="14"/>
        <source>AtWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="SearchWidget/atwidget.cpp" line="248"/>
        <source>@</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ChatDataManager</name>
    <message>
        <source>对方已经成功接收您的文件</source>
        <translation type="vanished">对方已经成功接收您的文件</translation>
    </message>
    <message>
        <source> joined the tribe</source>
        <translation type="vanished">加入了部落</translation>
    </message>
    <message>
        <source> invited </source>
        <translation type="vanished">邀请</translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished">加入了部落</translation>
    </message>
    <message>
        <source>[Announcement]</source>
        <translation type="obsolete">[通告]</translation>
    </message>
    <message>
        <source>[Share]</source>
        <translation type="obsolete">[分享]</translation>
    </message>
    <message>
        <source>&lt;font color=&apos;#f7931e&apos; &gt;[I was @]&lt;/font&gt;</source>
        <oldsource>&lt;font color=&apos;#f7931e&apos; &gt;I was @&lt;/font&gt;</oldsource>
        <translation type="obsolete">&lt;font color=&apos;#f7931e&apos; &gt;[有人@我]&lt;/font&gt;</translation>
    </message>
    <message>
        <source>File received successfully </source>
        <translation type="vanished">您已成功接收对方发送的文件！</translation>
    </message>
    <message>
        <source>The other party has successfully received your file </source>
        <translation type="vanished">对方已经成功接收您的文件！</translation>
    </message>
    <message>
        <location filename="chatdatamanager.cpp" line="760"/>
        <location filename="chatdatamanager.cpp" line="824"/>
        <location filename="chatdatamanager.cpp" line="855"/>
        <source>Warning</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="chatdatamanager.cpp" line="760"/>
        <location filename="chatdatamanager.cpp" line="824"/>
        <source>There are files being transferred，contuinue to close ?</source>
        <translation>有文件正在传输，是否关闭？</translation>
    </message>
    <message>
        <location filename="chatdatamanager.cpp" line="855"/>
        <source>There are files being transferred，contuinue to quit ?</source>
        <translation>有文件正在传输，是否退出？</translation>
    </message>
    <message>
        <source>Download task is running，contuinue to close ?</source>
        <translation type="vanished">下载任务正在运行，继续关闭？</translation>
    </message>
    <message>
        <source>Download task is running，contuinue to Quit ?</source>
        <translation type="vanished">下载任务正在运行，继续退出？</translation>
    </message>
    <message>
        <location filename="chatdatamanager.cpp" line="1319"/>
        <source>The other party has successfully received your file</source>
        <oldsource>The opposite side has successfully received your file</oldsource>
        <translation>对方已经成功接收您的文件</translation>
    </message>
</context>
<context>
    <name>ChatWidget</name>
    <message>
        <location filename="chatwidget.ui" line="14"/>
        <source>ChatWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="chatwidget.ui" line="117"/>
        <location filename="chatwidget.ui" line="162"/>
        <location filename="chatwidget.ui" line="190"/>
        <source>Search</source>
        <oldsource>search</oldsource>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="chatwidget.ui" line="215"/>
        <source>View device list</source>
        <translation>查看设备列表</translation>
    </message>
    <message>
        <location filename="chatwidget.ui" line="272"/>
        <source>当前账户在其他1台设备登录</source>
        <translation></translation>
    </message>
    <message>
        <location filename="chatwidget.cpp" line="149"/>
        <source>Account logged in on other </source>
        <oldsource>Current account is logged in on other</oldsource>
        <translation>当前账户在其他</translation>
    </message>
    <message>
        <location filename="chatwidget.cpp" line="149"/>
        <source> device(s)</source>
        <translation>台设备登录</translation>
    </message>
</context>
<context>
    <name>ChooseUnitWidget</name>
    <message>
        <location filename="redPacketWidget/ChooseUnitWidget.ui" line="26"/>
        <source>ChooseUnitWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/ChooseUnitWidget.ui" line="77"/>
        <source>Choose red packet token</source>
        <oldsource>Select red packet token</oldsource>
        <translation>选择红包代币</translation>
    </message>
    <message>
        <location filename="redPacketWidget/ChooseUnitWidget.ui" line="195"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>EnterPasswordWidget</name>
    <message>
        <location filename="secretWidget/enterpasswordwidget.ui" line="14"/>
        <source>EnterPasswordWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.ui" line="82"/>
        <source>Please enter password of the base:</source>
        <oldsource>Please enter your base password:</oldsource>
        <translation>请输入基地密码：</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.ui" line="101"/>
        <source>Base password is used to decrypt the password of the message</source>
        <oldsource>The base code is used to decrypt the message code</oldsource>
        <translation>基地密码用于解密消息密码</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.ui" line="126"/>
        <source>Please enter the base password</source>
        <oldsource>Please enter your base password</oldsource>
        <translation>请输入密码</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.ui" line="169"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="64"/>
        <source>Please enter the password</source>
        <translation>请输入密码</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="65"/>
        <source>Enter the password to view the content</source>
        <translation>输入密码查看内容</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="144"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="149"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="159"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="171"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="178"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="202"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="209"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="242"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="251"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="319"/>
        <source>Notice</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="144"/>
        <source>Decryption failure！Error code 113</source>
        <translation>解密失败！错误码113</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="149"/>
        <source>Password cannot be empty！</source>
        <translation>密码不能为空！</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="202"/>
        <source>Please observe the surrounding environment before viewing the password to avoid leaking your password.
The password is only displayed once, please be sure to save the password</source>
        <translation>查看密码前请注意周围环境，避免泄漏密码。
密码只提示一次，请务必保存好密码</translation>
    </message>
    <message>
        <source>Please observe the surrounding environment before viewing the password to avoid leaking your password.
The password is only diaplayed once, please be sure to save the password</source>
        <translation type="vanished">查看密码前请注意周围环境，避免泄漏密码。
密码只提示一次，请务必保存好密码</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="319"/>
        <source>Fail to download the secret picture/file!</source>
        <translation>密图/密件下载失败！</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="329"/>
        <source>&quot;,Please remember the password, as the system will not save the password</source>
        <translation>&quot;，请牢记密码，系统不会保存该密码</translation>
    </message>
    <message>
        <source>&quot;,Please remember the password, the system will not save the password</source>
        <translation type="vanished">请牢记，系统不会保存该密码</translation>
    </message>
    <message>
        <source>The login password cannot be empty！</source>
        <translation type="vanished">登录密码不能为空！</translation>
    </message>
    <message>
        <source>Incorrect login password！</source>
        <translation type="vanished">登录密码错误！</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="171"/>
        <source>Decryption failure！Error code 137</source>
        <translation>解密失败！错误码137</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="178"/>
        <source>Decryption failure！Error code 144</source>
        <translation>解密失败！错误码144</translation>
    </message>
    <message>
        <source>Check your surroundings when entering to avoid leaking the password.
The password is prompted only once,Be sure to keep your password</source>
        <translation type="vanished">查看密码前请注意周围环境，避免泄漏密码。
密码只提示一次，请务必保存好密码</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="209"/>
        <source>The password cannot be empty！</source>
        <translation>密码不能为空！</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="242"/>
        <source>The secret picture/file is empty，please download again！</source>
        <translation>密图/密件为空，请重新下载！</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="159"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="251"/>
        <source>Incorrect password！</source>
        <translation>密码输入错误！</translation>
    </message>
    <message>
        <source>fail to download the secret picture/file!</source>
        <translation type="vanished">密图/密件下载失败！</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="329"/>
        <source>The password is:&quot;</source>
        <translation>密码为：&quot;</translation>
    </message>
    <message>
        <source>&quot;,Please keep it in mind that the password won&apos;t be saved in system</source>
        <translation type="vanished">&quot;，请牢记，系统不会保存该密码</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="332"/>
        <source>View  password</source>
        <oldsource>View password</oldsource>
        <translation>查看密码</translation>
    </message>
</context>
<context>
    <name>ExpressWidget</name>
    <message>
        <location filename="childWidget/expresswidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="108"/>
        <source>[smile]</source>
        <translation>微笑</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="109"/>
        <source>[twitch mouth]</source>
        <translation>撇嘴</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="110"/>
        <source>[salivate]</source>
        <translation>色</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="111"/>
        <source>[staring blankly]</source>
        <translation>白眼</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="112"/>
        <source>[complacent]</source>
        <translation>得意</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="113"/>
        <source>[shy]</source>
        <translation>害羞</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="114"/>
        <source>[shut up]</source>
        <translation>闭嘴</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="115"/>
        <source>[sleep]</source>
        <translation>睡</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="116"/>
        <source>[cry]</source>
        <translation>大哭</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="117"/>
        <source>[awkward]</source>
        <translation>尴尬</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="118"/>
        <source>[angry]</source>
        <translation>发怒</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="119"/>
        <source>[naughty]</source>
        <translation>调皮</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="120"/>
        <source>[grimace]</source>
        <translation>呲牙</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="121"/>
        <source>[suprised]</source>
        <translation>惊讶</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="122"/>
        <source>[sad]</source>
        <translation>睡</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="123"/>
        <source>[cool]</source>
        <translation>酷</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="124"/>
        <source>[cold sweat]</source>
        <translation>冷汗</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="125"/>
        <source>[crazy]</source>
        <translation>抓狂</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="126"/>
        <source>[spit]</source>
        <translation>吐</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="127"/>
        <source>[titter]</source>
        <translation>偷笑</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="128"/>
        <source>[supercilious look]</source>
        <translation>白眼</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="129"/>
        <source>[cute]</source>
        <translation>可爱</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="130"/>
        <source>[arrogance]</source>
        <translation>傲慢</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="131"/>
        <source>[hungry]</source>
        <translation>饥饿</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="132"/>
        <source>[sleepy]</source>
        <translation>困</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="133"/>
        <source>[terrified]</source>
        <translation>惊恐</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="134"/>
        <source>[sweat]</source>
        <translation>流汗</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="135"/>
        <source>[smile fatuously]</source>
        <translation>憨笑</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="136"/>
        <source>[soldier]</source>
        <translation>大兵</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="137"/>
        <source>[strive]</source>
        <translation>奋斗</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="138"/>
        <source>[doubting]</source>
        <translation>疑问</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="139"/>
        <source>[hash]</source>
        <translation>嘘</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="140"/>
        <source>[dizzy]</source>
        <translation>晕</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="141"/>
        <source>[pig&apos;s head]</source>
        <translation>猪头</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="142"/>
        <source>[skeleton]</source>
        <translation>骷髅</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="143"/>
        <source>[unlucky]</source>
        <translation>衰</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="144"/>
        <source>[knock]</source>
        <translation>敲打</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="145"/>
        <source>[good bye]</source>
        <translation>再见</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="146"/>
        <source>[clap hands]</source>
        <oldsource>[clap hand]</oldsource>
        <translation>鼓掌</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="147"/>
        <source>[pick nose]</source>
        <translation>抠鼻</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="148"/>
        <source>[embarassed]</source>
        <translation>糗大了</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="149"/>
        <source>[snicker]</source>
        <translation>坏笑</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="150"/>
        <source>[left hem]</source>
        <translation>左哼哼</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="151"/>
        <source>[right hem]</source>
        <translation>右哼哼</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="152"/>
        <source>[yawn]</source>
        <translation>哈欠</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="153"/>
        <source>[despise]</source>
        <translation>鄙视</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="154"/>
        <source>[grievance]</source>
        <translation>委屈</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="155"/>
        <source>[about to weep]</source>
        <translation>快哭了</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="156"/>
        <source>[insidious]</source>
        <translation>阴险</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="157"/>
        <source>[kiss]</source>
        <translation>亲亲</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="158"/>
        <source>[scared]</source>
        <translation>吓</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="159"/>
        <source>[pitiful]</source>
        <translation>可怜</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="160"/>
        <source>[kitchen knife]</source>
        <translation>菜刀</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="161"/>
        <source>[watermelon]</source>
        <translation>西瓜</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="162"/>
        <source>[beer]</source>
        <translation>啤酒</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="163"/>
        <source>[ping pong]</source>
        <translation>兵乓球</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="164"/>
        <source>[coffee]</source>
        <translation>咖啡</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="165"/>
        <source>[rice]</source>
        <translation>饭</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="166"/>
        <source>[rose]</source>
        <translation>玫瑰</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="167"/>
        <source>[withered]</source>
        <translation>凋谢</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="168"/>
        <source>[love]</source>
        <translation>示爱</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="169"/>
        <source>[loving heart]</source>
        <translation>爱心</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="170"/>
        <source>[heart break]</source>
        <translation>心碎</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="171"/>
        <source>[cake]</source>
        <translation>蛋糕</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="172"/>
        <source>[flash]</source>
        <translation>闪电</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="173"/>
        <source>[bomb]</source>
        <translation>炸弹</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="174"/>
        <source>[knife]</source>
        <translation>刀</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="175"/>
        <source>[beetles]</source>
        <translation>瓢虫</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="176"/>
        <source>[shit]</source>
        <translation>便便</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="177"/>
        <source>[moon]</source>
        <translation>月亮</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="178"/>
        <source>[sun]</source>
        <translation>太阳</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="179"/>
        <source>[gift]</source>
        <translation>礼物</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="180"/>
        <source>[embrace]</source>
        <translation>拥抱</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="181"/>
        <source>[strong]</source>
        <translation>强</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="182"/>
        <source>[weak]</source>
        <translation>弱</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="183"/>
        <source>[shake hands]</source>
        <translation>握手</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="184"/>
        <source>[hold fist salute]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="185"/>
        <source>[seduce]</source>
        <translation>勾引</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="186"/>
        <source>[fist]</source>
        <translation>拳头</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="187"/>
        <source>[disappointed]</source>
        <translation>差</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="188"/>
        <source>[love you]</source>
        <translation>爱你</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="189"/>
        <source>[NO]</source>
        <translation>NO</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="190"/>
        <source>[OK]</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="191"/>
        <source>[weep]</source>
        <translation>流泪</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="192"/>
        <source>[curse]</source>
        <translation>咒骂</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="193"/>
        <source>[weep sweat]</source>
        <translation>擦汗</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="194"/>
        <source>[victory]</source>
        <translation>胜利</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="195"/>
        <source>[basketball]</source>
        <translation>篮球</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="196"/>
        <source>[soccer]</source>
        <translation>足球</translation>
    </message>
</context>
<context>
    <name>GiveRedPackWidget</name>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="26"/>
        <source>GiveRedPack</source>
        <oldsource>GiveRedPackWidget</oldsource>
        <translation>发红包</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="83"/>
        <source>Red Packet</source>
        <oldsource>Send Red Packet</oldsource>
        <translation>发红包</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="105"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="166"/>
        <source>Red Packet Token</source>
        <oldsource>Red packet token</oldsource>
        <translation>红包代币</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="251"/>
        <source>Escrow Balance</source>
        <oldsource>Balance</oldsource>
        <translation>托管余额</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="294"/>
        <source>Transfer</source>
        <oldsource>Transfer in</oldsource>
        <translation>转入</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="338"/>
        <source>Amount Each</source>
        <oldsource>Amount each</oldsource>
        <translation>单个红包币数</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="363"/>
        <source>Set Amount</source>
        <translation>填写币数</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="438"/>
        <source>Best wishes</source>
        <translation>恭喜发财，大吉大利</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="518"/>
        <source>Red Packet History</source>
        <translation>红包记录</translation>
    </message>
    <message>
        <source>Enter Number</source>
        <oldsource>Number of Currency</oldsource>
        <translation type="vanished">填写币数</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="416"/>
        <source>Message</source>
        <translation>留言</translation>
    </message>
    <message>
        <source>OpenPlanet,Best Regards</source>
        <oldsource>OpenPlanet,Best regards</oldsource>
        <translation type="vanished">星际通讯，恭喜发财</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="478"/>
        <source>Amount 0PWR</source>
        <oldsource>Red Packet Amount 0PWR</oldsource>
        <translation>红包总额 0PWR</translation>
    </message>
    <message>
        <source>Records</source>
        <translation type="vanished">红包记录</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="552"/>
        <source>Prepare Red Packet</source>
        <oldsource>Prepare red packet</oldsource>
        <translation>塞进红包</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="590"/>
        <source>Unclaimed red packet will be refunded after 24 hours</source>
        <oldsource>Currency in the unreceived red packet will be refunded after 24 hours</oldsource>
        <translation>未领取的红包，将于24小时后发起退款</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="195"/>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="382"/>
        <source>PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="275"/>
        <source>0PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="138"/>
        <source>Amount: </source>
        <oldsource>Red Packet Amount: </oldsource>
        <translation>塞币总额: </translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="146"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="180"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="189"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="195"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="213"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="218"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="228"/>
        <source>Notice</source>
        <translation>注意</translation>
    </message>
    <message>
        <source>fail to prepare the red packet!</source>
        <translation type="vanished">红包生成失败！</translation>
    </message>
    <message>
        <source>OpenPlanet, Best Regards!</source>
        <oldsource>OpenPlanet, Best regards!</oldsource>
        <translation type="vanished">星际通讯，恭喜发财！</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="180"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="189"/>
        <source>Amount cannot be empty!</source>
        <translation>输入金额不能不能为空!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="146"/>
        <source>Failed to prepare the red packet!</source>
        <translation>红包生成失败！</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="153"/>
        <source>Best wishes!</source>
        <translation>恭喜发财，大吉大利</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="195"/>
        <source>The amount of the red packet cannot be greater than your balance!</source>
        <translation>红包金额不能大于您的托管余额!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="201"/>
        <source>Please enter your login password</source>
        <translation>请输入登录密码</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="213"/>
        <source>Password Validation Failed!</source>
        <translation>验证登录密码失败！</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="218"/>
        <source>Password cannot be empty!</source>
        <translation>密码不能为空！</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="228"/>
        <source>Incorrect Password!</source>
        <translation>登录密码错误！</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="235"/>
        <source>Best Wishes!</source>
        <translation>恭喜发财，大吉大利</translation>
    </message>
    <message>
        <source>The login password cannot be empty!</source>
        <translation type="vanished">登录密码不能为空！</translation>
    </message>
</context>
<context>
    <name>GroupAddBuddyWidget</name>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="20"/>
        <source>GroupAddBuddy</source>
        <oldsource>GroupAddBuddyWidget</oldsource>
        <translation>添加部落成员</translation>
    </message>
    <message>
        <source>Invite</source>
        <translation type="obsolete">选择联系人</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="161"/>
        <source>Select contact(s)</source>
        <translation>选择联系人</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="239"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="294"/>
        <source>Please select contact(s) you want to invite</source>
        <translation>请勾选需要添加的联系人</translation>
    </message>
    <message>
        <source>This member has been in the tribe already!</source>
        <translation type="vanished">本部落已经包含该成员！</translation>
    </message>
    <message>
        <source>contact(s)</source>
        <translation type="vanished">个联系人</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="197"/>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="447"/>
        <source>This member has been in the group already!</source>
        <translation>本群组已经包含该成员！</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="266"/>
        <source>You have selected </source>
        <translation>已经选择了</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="266"/>
        <source> contact(s)</source>
        <translation>个联系人</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="269"/>
        <source>Please select contacts you want to invite</source>
        <oldsource>Please check the friends you want to invite</oldsource>
        <translation>请勾选需要添加的联系人</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="625"/>
        <source> to join the group</source>
        <translation>加入了群组</translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished">加入了部落</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="367"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="398"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="197"/>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="447"/>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="645"/>
        <source>Notice</source>
        <translation>注意</translation>
    </message>
    <message>
        <source>This member is in the tribe already!</source>
        <translation type="vanished">本部落已经包含该成员！</translation>
    </message>
    <message>
        <source>friend(s)</source>
        <translation type="vanished">个联系人</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="625"/>
        <source> invited </source>
        <oldsource>invites</oldsource>
        <translation>邀请</translation>
    </message>
    <message>
        <source>into the tribe</source>
        <translation type="vanished">加入了部落</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="645"/>
        <source>Added Successfully!</source>
        <translation>添加成功！</translation>
    </message>
</context>
<context>
    <name>GroupChatWidget</name>
    <message>
        <location filename="groupchatwidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="89"/>
        <source>Font</source>
        <translation>字体</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="120"/>
        <source>Expressions</source>
        <translation>表情</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="148"/>
        <source>Vibration</source>
        <translation>震动</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="176"/>
        <source>Picture</source>
        <translation>图片</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="204"/>
        <source>Screen Cut</source>
        <translation>截图</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="232"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="260"/>
        <source>Announcement</source>
        <translation>通告</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="288"/>
        <source>Mute All</source>
        <translation>全员禁言</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="319"/>
        <source>Red Packet</source>
        <translation>红包</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="362"/>
        <source>Message Log</source>
        <translation>消息记录</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="389"/>
        <source>Send</source>
        <translation>发送</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="482"/>
        <source>Group Members</source>
        <oldsource>Tribe Members</oldsource>
        <translation>群组成员</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="510"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="547"/>
        <source>Add Member</source>
        <translation>添加成员</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="588"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="95"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="vanished">复制</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation type="vanished">剪切</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="vanished">粘贴</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <source>Notice</source>
        <translation type="vanished">注意</translation>
    </message>
    <message>
        <source>Cannot send File whose size is greater than 100M</source>
        <translation type="vanished">无法发送大于100M的文件</translation>
    </message>
    <message>
        <source>[File]</source>
        <translation type="vanished">[文件]</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">打开</translation>
    </message>
    <message>
        <source>Image File</source>
        <translation type="vanished">图像文件</translation>
    </message>
    <message>
        <source>[Image]</source>
        <translation type="vanished">[图片]</translation>
    </message>
    <message>
        <source>[Red Packet]</source>
        <translation type="vanished">[红包]</translation>
    </message>
    <message>
        <source>OpenPlanet, Best Regards</source>
        <translation type="vanished">星际通讯，恭喜发财</translation>
    </message>
    <message>
        <source>The other party has successfully received your file</source>
        <oldsource>The opposite side has successfully received your file</oldsource>
        <translation type="vanished">对方已经成功接收您的文件</translation>
    </message>
    <message>
        <source>This file already exists, overwrite or not？</source>
        <translation type="vanished">该文件已存在，是否覆盖？</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation type="vanished">另存为</translation>
    </message>
    <message>
        <source>Cancel Administrator Privileges</source>
        <translation type="vanished">取消管理员</translation>
    </message>
    <message>
        <source>Set as Administrator</source>
        <translation type="vanished">设为管理员</translation>
    </message>
    <message>
        <source>View Profile</source>
        <translation type="vanished">查看资料</translation>
    </message>
    <message>
        <source>Set Tribal Name</source>
        <oldsource>Set Alias in Tribe</oldsource>
        <translation type="vanished">设置备注名</translation>
    </message>
    <message>
        <source>Remove Tribe Member</source>
        <translation type="vanished">移除部落成员</translation>
    </message>
    <message>
        <source>Please enter your alias:</source>
        <translation type="vanished">请输入备注名称：</translation>
    </message>
    <message>
        <source>Fail to check the red packet!</source>
        <translation type="vanished">查看红包失败！</translation>
    </message>
    <message>
        <source>Add as friend</source>
        <oldsource>Add as a Friend</oldsource>
        <translation type="vanished">请求好友</translation>
    </message>
    <message>
        <source>Adding as friend succeed!</source>
        <translation type="vanished">添加好友成功！</translation>
    </message>
    <message>
        <source>Adding as friend failed!</source>
        <translation type="vanished">添加好友失败！</translation>
    </message>
    <message>
        <source>Tribe has been silenced...</source>
        <oldsource>Tribe has been muted...</oldsource>
        <translation type="vanished">全员禁言中...</translation>
    </message>
    <message>
        <source>Personal Business Card</source>
        <translation type="vanished">个人名片</translation>
    </message>
    <message>
        <source>Personal ID:</source>
        <translation type="vanished">星际ID：</translation>
    </message>
    <message>
        <source>Tribe Business Card</source>
        <translation type="vanished">部落名片</translation>
    </message>
    <message>
        <source>Tribe ID:</source>
        <translation type="vanished">部落ID：</translation>
    </message>
    <message>
        <source>Local file does&apos;t exist and cannot be forwarded</source>
        <translation type="vanished">本地文件不存在，无法转发</translation>
    </message>
    <message>
        <source>Local file does&apos;t exist，unable to forward</source>
        <translation type="vanished">本地文件不存在，无法转发</translation>
    </message>
    <message>
        <source>Resend</source>
        <translation type="vanished">重发</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation type="vanished">重新加载</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="vanished">转发</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="vanished">清屏</translation>
    </message>
    <message>
        <source>[Announcement]</source>
        <translation type="vanished">[通告]</translation>
    </message>
    <message>
        <source>Received the file successfully!</source>
        <oldsource>Receive file successfully!</oldsource>
        <translation type="vanished">您已成功接收对方发送的文件！</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="392"/>
        <source>Alt+S</source>
        <translation></translation>
    </message>
    <message>
        <source>Empty Filename! Open Document Failed!</source>
        <oldsource>fileName为空!OpenDocument Failed!</oldsource>
        <translation type="vanished">文件名为空！打开文件失败！</translation>
    </message>
    <message>
        <source>Empty file name or file doesn&apos;t exist!</source>
        <translation type="vanished">文件名为空或文件不存在！</translation>
    </message>
    <message>
        <source>Cannot send file larger than 100M in size</source>
        <translation type="vanished">无法发送大于100M的文件</translation>
    </message>
    <message>
        <source>Image File(*.bmp;*.jpeg;*.jpg;*.png;*.gif)</source>
        <oldsource>图像文件(*.bmp;*.jpeg;*.jpg;*.png;*.gif)</oldsource>
        <translation type="vanished">图像文件(*.bmp;*.jpeg;*.jpg;*.png;*.gif)</translation>
    </message>
    <message>
        <source>Best Wishes</source>
        <translation type="vanished">星际通讯，恭喜发财</translation>
    </message>
    <message>
        <source> joined the tribe</source>
        <translation type="vanished">加入了部落</translation>
    </message>
    <message>
        <source> invited </source>
        <translation type="vanished">邀请</translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished">加入了部落</translation>
    </message>
    <message>
        <source>This file already exists, overwrite or not?</source>
        <translation type="vanished">该文件已存在，是否覆盖？</translation>
    </message>
    <message>
        <source> Members %1</source>
        <translation type="vanished"> 部落成员 %1</translation>
    </message>
</context>
<context>
    <name>GroupFileWidget</name>
    <message>
        <location filename="groupfilewidget.ui" line="14"/>
        <source>GroupFileWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="groupfilewidget.ui" line="89"/>
        <source>file</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="groupfilewidget.ui" line="94"/>
        <source>upload time</source>
        <translation>上传时间</translation>
    </message>
    <message>
        <location filename="groupfilewidget.ui" line="99"/>
        <source>size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="groupfilewidget.ui" line="104"/>
        <source>uploaded by</source>
        <translation>上传者</translation>
    </message>
    <message>
        <location filename="groupfilewidget.cpp" line="67"/>
        <source>%1 files in total</source>
        <translation>共%1个文件</translation>
    </message>
    <message>
        <location filename="groupfilewidget.cpp" line="129"/>
        <source>unknown</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="groupfilewidget.cpp" line="172"/>
        <source>open file</source>
        <oldsource>open</oldsource>
        <translation>打开文件</translation>
    </message>
</context>
<context>
    <name>GroupPackWidget</name>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="26"/>
        <source>GroupPackWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="83"/>
        <source>Red Packet</source>
        <oldsource>Send Red Packet</oldsource>
        <translation>发红包</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="105"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="156"/>
        <source>Random Red Packet</source>
        <translation>拼手气红包</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="210"/>
        <source>Identical Red Packet</source>
        <oldsource>identical Red Packet</oldsource>
        <translation>普通红包</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="299"/>
        <source>Red Packet Token</source>
        <translation>红包代币</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="384"/>
        <source>Escrow Balance</source>
        <oldsource>Balance</oldsource>
        <translation>托管余额</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="427"/>
        <source>Transfer</source>
        <oldsource>Transfer in</oldsource>
        <translation>转入</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="480"/>
        <source>Total Amount</source>
        <translation>总红包币数</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="550"/>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="659"/>
        <source>Enter Number</source>
        <oldsource>Number of Currency</oldsource>
        <translation>填写币数</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="603"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="273"/>
        <source>Each person gets a random amount</source>
        <translation>每人抽到的金额随机</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="740"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="183"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="220"/>
        <source>Best Wishes</source>
        <translation>恭喜发财，大吉大利</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="826"/>
        <source>Red Packet History</source>
        <translation>红包记录</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="637"/>
        <source>Number of Red Packets</source>
        <translation>填写个数</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="687"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="260"/>
        <source>There are</source>
        <translation>本部落共</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="718"/>
        <source>Message</source>
        <translation>留言</translation>
    </message>
    <message>
        <source>OpenPlanet,Best Regards</source>
        <translation type="vanished">星际通讯，恭喜发财</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="786"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="277"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="291"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="317"/>
        <source>Amount 0PWR</source>
        <translation>红包总额0PWR</translation>
    </message>
    <message>
        <source>Records</source>
        <translation type="vanished">红包记录</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="860"/>
        <source>Prepare Red Packet</source>
        <translation>塞进红包</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="898"/>
        <source>Unclaimed red packet will be refunded after 24 hours</source>
        <oldsource>Currency in the unreceived red packet will be refunded after 24 hours</oldsource>
        <translation>未领取的红包，将于24小时后发起退款</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="328"/>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="569"/>
        <source>PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="408"/>
        <source>0PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="516"/>
        <source>拼</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="107"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="117"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="124"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="129"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="135"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="161"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="166"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="176"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="213"/>
        <source>Notice</source>
        <translation>注意</translation>
    </message>
    <message>
        <source>The amount and the number of the red packet cannot be empty!</source>
        <translation type="vanished">红包金额、红包个数都不能为空！</translation>
    </message>
    <message>
        <source>The amount and the number of the red packet cannot be 0!</source>
        <translation type="vanished">红包金额、红包个数都不能为0！</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="124"/>
        <source>The amount of the red packet cannot be greater than your balance!</source>
        <translation>红包金额不能大于您的托管余额!</translation>
    </message>
    <message>
        <source>The number of the red packet cannot be greater than the number of people in the tribe!</source>
        <translation type="vanished">红包个数不能大于部落总人数！</translation>
    </message>
    <message>
        <source>the amount of each red packet cannot be smaller than 0.00001!</source>
        <translation type="vanished">单个红包金额不能小于0.00001！</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="140"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="147"/>
        <source>Please enter your login password:</source>
        <translation>请输入登录密码：</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="161"/>
        <source>Failed to verify the login password!</source>
        <translation>验证登录密码失败！</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="166"/>
        <source>Login password cannot be empty!</source>
        <translation>登录密码不能为空！</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="176"/>
        <source>Incorrect Password!</source>
        <translation>登录密码错误！</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="260"/>
        <source>people in this group</source>
        <translation>群组成员</translation>
    </message>
    <message>
        <source>OpenPlanet, Best Regards</source>
        <translation type="vanished">星际通讯，恭喜发财</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="213"/>
        <source>Failed to prepare the red packet!</source>
        <translation>红包生成失败！</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="107"/>
        <source>The amount or the number of the red packet cannot be empty!</source>
        <translation>红包金额、红包个数都不能为空！</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="117"/>
        <source>The amount or the number of the red packet cannot be 0 !</source>
        <translation>红包金额、红包个数都不能为0！</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="129"/>
        <source>The number of the red packet cannot be greater than the number of people in the group!</source>
        <translation>红包个数不能大于部落总人数！</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="135"/>
        <source>The amount of each red packet cannot be smaller than 0.00001!</source>
        <translation>单个红包金额不能小于0.00001！</translation>
    </message>
    <message>
        <source>people in this tribe</source>
        <translation type="vanished">人</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="271"/>
        <source>Amount</source>
        <translation>红包总额</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="285"/>
        <source>Amount Each</source>
        <translation>单个红包币数</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="287"/>
        <source>Each person gets a identical amount</source>
        <oldsource>each person gets a identical amount</oldsource>
        <translation>每人抽到的金额固定</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="294"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="305"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="322"/>
        <source>Amount </source>
        <oldsource>Total Amount </oldsource>
        <translation>红包总额 </translation>
    </message>
</context>
<context>
    <name>GroupSearchWidget</name>
    <message>
        <location filename="childWidget/groupsearchwidget.ui" line="23"/>
        <source>GroupSearchWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childWidget/groupsearchwidget.ui" line="74"/>
        <source>Find Group Member</source>
        <oldsource>Find Tribe Member</oldsource>
        <translation>查找群组成员</translation>
    </message>
</context>
<context>
    <name>GroupUserProfileWidget</name>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="14"/>
        <source>GroupUserProfile</source>
        <oldsource>GroupUserProfileWidget</oldsource>
        <translation>部落成员资料</translation>
    </message>
    <message>
        <source>Personal ID</source>
        <translation type="vanished">星际ID</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="111"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="188"/>
        <source>Acct.No</source>
        <translation>账户</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="234"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="336"/>
        <source>View Avatar</source>
        <translation>查看头像</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="630"/>
        <source>Add as friend</source>
        <oldsource>Add as a friend</oldsource>
        <translation>加为好友</translation>
    </message>
    <message>
        <source>Base ID</source>
        <oldsource>基地ID</oldsource>
        <translation type="vanished">基地ID</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.cpp" line="25"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.cpp" line="70"/>
        <source>Send</source>
        <translation>发送消息</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.cpp" line="189"/>
        <source>Request sent successfully</source>
        <translation>请求发送成功</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.cpp" line="193"/>
        <source>Request sent Failed</source>
        <translation>请求发送失败</translation>
    </message>
</context>
<context>
    <name>GroupWidget</name>
    <message>
        <location filename="groupwidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="groupwidget.ui" line="83"/>
        <source>View the Details</source>
        <translation>查看资料</translation>
    </message>
    <message>
        <location filename="groupwidget.ui" line="133"/>
        <source>chat</source>
        <translation>聊天</translation>
    </message>
    <message>
        <location filename="groupwidget.ui" line="176"/>
        <source>file</source>
        <translation>文件</translation>
    </message>
</context>
<context>
    <name>IMGroupChatStore</name>
    <message>
        <location filename="imgroupchatstore.cpp" line="495"/>
        <location filename="imgroupchatstore.cpp" line="1260"/>
        <location filename="imgroupchatstore.cpp" line="2109"/>
        <source>Best Wishes</source>
        <translation>恭喜发财，大吉大利</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="598"/>
        <location filename="imgroupchatstore.cpp" line="1413"/>
        <location filename="imgroupchatstore.cpp" line="2267"/>
        <source>The other party has successfully received your file</source>
        <translation>对方已经成功接收您的文件</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="933"/>
        <location filename="imgroupchatstore.cpp" line="1765"/>
        <location filename="imgroupchatstore.cpp" line="1809"/>
        <location filename="imgroupchatstore.cpp" line="2577"/>
        <source>Notice</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="933"/>
        <source>This file already exists, overwrite or not?</source>
        <translation>该文件已存在，是否覆盖？</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1407"/>
        <location filename="imgroupchatstore.cpp" line="2261"/>
        <location filename="imgroupchatstore.cpp" line="2984"/>
        <source> joined the group</source>
        <oldsource> joined the tribe</oldsource>
        <translation>加入了群组</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1409"/>
        <location filename="imgroupchatstore.cpp" line="2263"/>
        <location filename="imgroupchatstore.cpp" line="2986"/>
        <source> invited </source>
        <translation>邀请</translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished">加入了部落</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1623"/>
        <location filename="imgroupchatstore.cpp" line="1631"/>
        <location filename="imgroupchatstore.cpp" line="1650"/>
        <source>[Image]</source>
        <translation>[图片]</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1750"/>
        <location filename="imgroupchatstore.cpp" line="1793"/>
        <location filename="imgroupchatstore.cpp" line="2641"/>
        <location filename="imgroupchatstore.cpp" line="2649"/>
        <location filename="imgroupchatstore.cpp" line="2696"/>
        <source>[File]</source>
        <translation>[文件]</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1765"/>
        <location filename="imgroupchatstore.cpp" line="1809"/>
        <source>Local file does&apos;t exist and cannot be forwarded</source>
        <translation>本地文件不存在，无法转发</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2430"/>
        <source>Personal Business Card</source>
        <translation>个人名片</translation>
    </message>
    <message>
        <source>Personal ID:</source>
        <translation type="vanished">星际ID：</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1409"/>
        <location filename="imgroupchatstore.cpp" line="2263"/>
        <location filename="imgroupchatstore.cpp" line="2986"/>
        <source> to join the group</source>
        <translation>加入了群组</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2431"/>
        <source>ID:</source>
        <translation>ID:</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2444"/>
        <source>Group Business Card</source>
        <oldsource>Tribe Business Card</oldsource>
        <translation>群组名片</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2445"/>
        <source>Group ID:</source>
        <oldsource>Tribe ID:</oldsource>
        <translation>群组ID：</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2551"/>
        <source>[Red Packet]</source>
        <translation>[红包]</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2577"/>
        <source>Cannot send file larger than 100M in size</source>
        <translation>无法发送大于100M的文件</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2790"/>
        <location filename="imgroupchatstore.cpp" line="2798"/>
        <location filename="imgroupchatstore.cpp" line="2815"/>
        <source>[Announcement]</source>
        <translation>[通告]</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2913"/>
        <source>Save as</source>
        <translation>另存为</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2990"/>
        <source>Received the file successfully!</source>
        <translation>您已成功接收对方发送的文件！</translation>
    </message>
</context>
<context>
    <name>IMGroupChatView</name>
    <message>
        <location filename="imgroupchatview.cpp" line="137"/>
        <source> Members %1</source>
        <translation> 部落成员 %1</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="356"/>
        <location filename="imgroupchatview.cpp" line="1751"/>
        <location filename="imgroupchatview.cpp" line="1758"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="359"/>
        <source>Cut</source>
        <translation>剪切</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="362"/>
        <source>Paste</source>
        <translation>粘贴</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="365"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <source>Tribe has been silenced...</source>
        <translation type="vanished">全员禁言中...</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="933"/>
        <source>Cancel Administrator Privileges</source>
        <translation>取消管理员</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="939"/>
        <source>Set as Administrator</source>
        <translation>设为管理员</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="946"/>
        <source>View Profile</source>
        <translation>查看资料</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="951"/>
        <source>Set Remark Name</source>
        <oldsource>Set Group Name</oldsource>
        <translation>设置备注名</translation>
    </message>
    <message>
        <source>Remove Tribe Member</source>
        <translation type="vanished">移除部落成员</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="788"/>
        <source>Group has been silenced...</source>
        <translation>全员禁言中...</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="958"/>
        <source>Remove Group Member</source>
        <translation>移除群组成员</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1014"/>
        <source>Please enter your alias:</source>
        <translation>请输入备注名称：</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1303"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1304"/>
        <source>Image File</source>
        <translation>图像文件</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1521"/>
        <source>Send</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1521"/>
        <source>All(*.*)</source>
        <translation>所有文件(*.*)</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1652"/>
        <source>Empty Filename! Open Document Failed!</source>
        <translation>文件名为空！打开文件失败！</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1701"/>
        <location filename="imgroupchatview.cpp" line="2109"/>
        <source>Notice</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1701"/>
        <source>Empty file name or file doesn&apos;t exist!</source>
        <translation>文件名为空或文件不存在！</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1768"/>
        <source>Resend</source>
        <translation>重发</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1778"/>
        <source>Reload</source>
        <translation>重新加载</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1786"/>
        <source>Forward</source>
        <translation>转发</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1797"/>
        <source>Clear</source>
        <translation>清屏</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="2109"/>
        <source>Fail to check the red packet!</source>
        <translation>查看红包失败！</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="2246"/>
        <source>@</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>IMGroupChatViewModel</name>
    <message>
        <location filename="imgroupchatviewmodel.cpp" line="206"/>
        <source>[Image]</source>
        <translation>[图片]</translation>
    </message>
</context>
<context>
    <name>IMPerChat</name>
    <message>
        <location filename="imperchat.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="92"/>
        <source>View the Detail</source>
        <translation>查看资料</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="228"/>
        <source>Screen Cut</source>
        <translation>截图</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="256"/>
        <source>Font</source>
        <translation>字体</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="284"/>
        <source>Expressions</source>
        <translation>表情</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="312"/>
        <source>Vibration</source>
        <translation>震动</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="340"/>
        <source>Picture</source>
        <translation>图片</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="368"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="396"/>
        <source>Secret Message</source>
        <translation>密信</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="424"/>
        <source>Secret Picture</source>
        <translation>密图</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="452"/>
        <source>Secret File</source>
        <translation>密件</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="480"/>
        <source>Announcement</source>
        <translation>通告</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="536"/>
        <source>ETH Transfer</source>
        <translation>ETH转账</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="564"/>
        <source>BTC Transfer</source>
        <translation>BTC转账</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="592"/>
        <source>EOS Transfer</source>
        <translation>EOS转账</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="620"/>
        <source>Red Packet</source>
        <translation>红包</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="672"/>
        <source>Message Log</source>
        <translation>消息记录</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="696"/>
        <source>Send</source>
        <translation>发送</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="699"/>
        <source>Alt+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="742"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;微软雅黑&apos;; font-size:12pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>Image File(*.bmp;*.jpeg;*.jpg;*.png;*.gif)</source>
        <oldsource>图像文件(*.bmp;*.jpeg;*.jpg;*.png;*.gif)</oldsource>
        <translation type="vanished">图像文件(*.bmp;*.jpeg;*.jpg;*.png;*.gif)</translation>
    </message>
    <message>
        <source>Create Tribe</source>
        <translation type="vanished">创建部落</translation>
    </message>
    <message>
        <source>Add as a friend</source>
        <translation type="vanished">加为好友</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="vanished">复制</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation type="vanished">剪切</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <source>Power Exchange</source>
        <translation type="vanished">能量交换</translation>
    </message>
    <message>
        <source>ETH Tranfer</source>
        <translation type="vanished">ETH转账</translation>
    </message>
    <message>
        <source>BTC Tranfer</source>
        <translation type="vanished">BTC转账</translation>
    </message>
    <message>
        <source>EOS Tranfer</source>
        <translation type="vanished">EOS转账</translation>
    </message>
    <message>
        <source>Notice</source>
        <translation type="vanished">注意</translation>
    </message>
    <message>
        <source>Cannot send File whose size is greater than 100M</source>
        <translation type="vanished">无法发送大于100M的文件</translation>
    </message>
    <message>
        <source>[File]</source>
        <translation type="vanished">[文件]</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">打开</translation>
    </message>
    <message>
        <source>[Image]</source>
        <translation type="vanished">[图片]</translation>
    </message>
    <message>
        <source>OpenPlanet, Best Regards</source>
        <translation type="vanished">星际通讯，恭喜发财</translation>
    </message>
    <message>
        <source>Receive file successfully!</source>
        <translation type="vanished">您已成功接收对方发送的文件！</translation>
    </message>
    <message>
        <source>The other party has successfully received your file</source>
        <oldsource>The opposite side has successfully received your file</oldsource>
        <translation type="vanished">对方已经成功接收您的文件</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="vanished">粘贴</translation>
    </message>
    <message>
        <source>File larger than 100M cannot be sent</source>
        <translation type="vanished">无法发送大于100M的文件</translation>
    </message>
    <message>
        <source>Empty file name or file doesn&apos;t exist!</source>
        <translation type="vanished">文件名为空或文件不存在！</translation>
    </message>
    <message>
        <source>Best Wishes</source>
        <translation type="vanished">星际通讯，恭喜发财</translation>
    </message>
    <message>
        <source>Received file successfully!</source>
        <translation type="vanished">您已成功接收对方发送的文件！</translation>
    </message>
    <message>
        <source>This file already exists, overwrite or not？</source>
        <translation type="vanished">该文件已存在，是否覆盖？</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation type="vanished">另存为</translation>
    </message>
    <message>
        <source>[Secret Message]</source>
        <translation type="vanished">[密信]</translation>
    </message>
    <message>
        <source>[Secret Image]</source>
        <translation type="vanished">[密图]</translation>
    </message>
    <message>
        <source>[Secret File]</source>
        <translation type="vanished">[密件]</translation>
    </message>
    <message>
        <source>[Red Packet]</source>
        <translation type="vanished">[红包]</translation>
    </message>
    <message>
        <source>Personal business card</source>
        <translation type="vanished">个人名片</translation>
    </message>
    <message>
        <source>Personal ID:</source>
        <translation type="vanished">星际ID：</translation>
    </message>
    <message>
        <source>Tribe business card</source>
        <translation type="vanished">部落名片</translation>
    </message>
    <message>
        <source>Tribe ID:</source>
        <translation type="vanished">部落ID：</translation>
    </message>
    <message>
        <source>Fail to check the red packet!</source>
        <translation type="vanished">查看红包失败！</translation>
    </message>
    <message>
        <source>Local file does&apos;t exist，unable to forward</source>
        <translation type="vanished">本地文件不存在，无法转发</translation>
    </message>
    <message>
        <source>Resend</source>
        <translation type="vanished">重发</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation type="vanished">重新加载</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="vanished">转发</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="vanished">清屏</translation>
    </message>
    <message>
        <source>[Announcement]</source>
        <translation type="vanished">[通告]</translation>
    </message>
    <message>
        <source>（Typing...）</source>
        <translation type="vanished">(对方正在输入...)</translation>
    </message>
</context>
<context>
    <name>IMPerChatStore</name>
    <message>
        <location filename="imperchatstore.cpp" line="696"/>
        <location filename="imperchatstore.cpp" line="1379"/>
        <source>Best Wishes</source>
        <translation>恭喜发财，大吉大利</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="872"/>
        <source>The other party has successfully received your file</source>
        <translation>对方已经成功接收您的文件</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="923"/>
        <location filename="imperchatstore.cpp" line="1097"/>
        <location filename="imperchatstore.cpp" line="1917"/>
        <location filename="imperchatstore.cpp" line="2191"/>
        <source>Notice</source>
        <translation>注意</translation>
    </message>
    <message>
        <source>This file already exists, overwrite or not？</source>
        <translation type="vanished">该文件已存在，是否覆盖？</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="923"/>
        <source>This file already exists, overwrite or not?</source>
        <translation>该文件已存在，是否覆盖？</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1039"/>
        <location filename="imperchatstore.cpp" line="1044"/>
        <location filename="imperchatstore.cpp" line="1057"/>
        <source>Receive file successfully!</source>
        <translation>您已成功接收对方发送的文件！</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1097"/>
        <source>File is in use, please check it first!</source>
        <translation>文件正在使用中，请检查！</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1574"/>
        <source>Received file successfully!</source>
        <translation>您已成功接收对方发送的文件！</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1763"/>
        <location filename="imperchatstore.cpp" line="1792"/>
        <source>[Image]</source>
        <translation>[图片]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1900"/>
        <location filename="imperchatstore.cpp" line="2256"/>
        <location filename="imperchatstore.cpp" line="2318"/>
        <source>[File]</source>
        <translation>[文件]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1917"/>
        <source>Local file does&apos;t exist，unable to forward</source>
        <translation>本地文件不存在，无法转发</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1991"/>
        <source>Personal business card</source>
        <translation>个人名片</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1992"/>
        <source>ID:</source>
        <translation>ID:</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2005"/>
        <source>Group business card</source>
        <translation>群组名片</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2006"/>
        <source>Group ID:</source>
        <translation>群组ID：</translation>
    </message>
    <message>
        <source>Personal ID:</source>
        <translation type="vanished">星际ID：</translation>
    </message>
    <message>
        <source>Tribe business card</source>
        <translation type="vanished">部落名片</translation>
    </message>
    <message>
        <source>Tribe ID:</source>
        <translation type="vanished">部落ID：</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2162"/>
        <source>[Red Packet]</source>
        <translation>[红包]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2191"/>
        <source>File larger than 100M cannot be sent</source>
        <translation>无法发送大于100M的文件</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2363"/>
        <source>[Secret Message]</source>
        <translation>[密信]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2395"/>
        <source>[Secret Image]</source>
        <translation>[密图]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2427"/>
        <source>[Secret File]</source>
        <translation>[密件]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2508"/>
        <location filename="imperchatstore.cpp" line="2540"/>
        <source>[Announcement]</source>
        <translation>[通告]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2601"/>
        <source>Save as</source>
        <translation>另存为</translation>
    </message>
</context>
<context>
    <name>IMPerChatView</name>
    <message>
        <source>Create Tribe</source>
        <translation type="vanished">创建部落</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="153"/>
        <source>Create Group</source>
        <translation>创建群组</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="164"/>
        <source>Add as a friend</source>
        <translation>加为好友</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="221"/>
        <source>Power Exchange</source>
        <translation>能量交换</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="226"/>
        <source>ETH Tranfer</source>
        <translation>ETH转账</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="231"/>
        <source>BTC Tranfer</source>
        <translation>BTC转账</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="236"/>
        <source>EOS Tranfer</source>
        <translation>EOS转账</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="598"/>
        <location filename="imperchatview.cpp" line="1777"/>
        <location filename="imperchatview.cpp" line="1783"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="601"/>
        <source>Cut</source>
        <translation>剪切</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="604"/>
        <source>Paste</source>
        <translation>粘贴</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="607"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="751"/>
        <source>（Typing...）</source>
        <translation>(对方正在输入...)</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="953"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="954"/>
        <source>Image File</source>
        <translation>图像文件</translation>
    </message>
    <message>
        <source>Image File(*.bmp;*.jpeg;*.jpg;*.png;*.gif)</source>
        <translation type="vanished">图像文件(*.bmp;*.jpeg;*.jpg;*.png;*.gif)</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1030"/>
        <source>Send</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1030"/>
        <source>All(*.*)</source>
        <translation>所有文件(*.*)</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1293"/>
        <location filename="imperchatview.cpp" line="1684"/>
        <source>Notice</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1293"/>
        <source>Empty file name or file doesn&apos;t exist!</source>
        <translation>文件名为空或文件不存在！</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1325"/>
        <source>fileName为空!OpenDocument Failed!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1684"/>
        <source>Fail to check the red packet!</source>
        <translation>查看红包失败！</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1793"/>
        <source>Resend</source>
        <translation>重发</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1801"/>
        <source>Reload</source>
        <translation>重新加载</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1809"/>
        <source>Forward</source>
        <translation>转发</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1819"/>
        <source>Clear</source>
        <translation>清屏</translation>
    </message>
</context>
<context>
    <name>IMPerChatViewModel</name>
    <message>
        <location filename="imperchatviewmodel.cpp" line="166"/>
        <source>[Image]</source>
        <translation>[图片]</translation>
    </message>
</context>
<context>
    <name>MessageList</name>
    <message>
        <source>&lt;font color=&apos;#f7931e&apos; &gt;[I was @]&lt;/font&gt;</source>
        <oldsource>&lt;font color=&apos;#f7931e&apos; &gt;[有人@我]&lt;/font&gt;</oldsource>
        <translation type="vanished">&lt;font color=&apos;#f7931e&apos; &gt;[有人@我]&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Yesterday</source>
        <translation type="vanished">昨天</translation>
    </message>
    <message>
        <source>  Sticky on Top</source>
        <translation type="vanished">  消息置顶</translation>
    </message>
    <message>
        <source>View Details</source>
        <oldsource>  View Details</oldsource>
        <translation type="vanished">查看资料</translation>
    </message>
    <message>
        <source>Close Chat</source>
        <oldsource>  Close Chat</oldsource>
        <translation type="vanished">关闭会话</translation>
    </message>
    <message>
        <source>Close All Chats</source>
        <oldsource>Close all Chats</oldsource>
        <translation type="vanished">关闭全部会话</translation>
    </message>
    <message>
        <source>Remove From Top</source>
        <oldsource>Remove from Top</oldsource>
        <translation type="vanished">取消消息置顶</translation>
    </message>
    <message>
        <source>Set Message Top</source>
        <oldsource>Set Messaege Top</oldsource>
        <translation type="vanished">消息置顶</translation>
    </message>
    <message>
        <source>Cancle No-Disturbing</source>
        <oldsource>Set Message No Disturbing</oldsource>
        <translation type="vanished">开启消息提醒</translation>
    </message>
    <message>
        <source>Set No-Disturbing</source>
        <oldsource>Cancle Message No Disturbing</oldsource>
        <translation type="vanished">消息免打扰</translation>
    </message>
    <message>
        <source>Dismiss Tribe</source>
        <oldsource>   Dismiss Tribe</oldsource>
        <translation type="vanished">解散部落</translation>
    </message>
    <message>
        <source>Quit Tribe</source>
        <oldsource>  Quit Tribe</oldsource>
        <translation type="vanished">退出部落</translation>
    </message>
    <message>
        <source>[Image]</source>
        <translation type="vanished">[图片]</translation>
    </message>
    <message>
        <source>[Audio]</source>
        <translation type="vanished">[音频]</translation>
    </message>
    <message>
        <source>[Video]</source>
        <translation type="vanished">[视频]</translation>
    </message>
    <message>
        <source>[File]</source>
        <translation type="vanished">[文件]</translation>
    </message>
    <message>
        <source>This type of message is not supported for now</source>
        <translation type="vanished">该消息类型暂不支持</translation>
    </message>
    <message>
        <source>[Tranfer]</source>
        <translation type="vanished">[转账]</translation>
    </message>
    <message>
        <source>[Red Packet]</source>
        <translation type="vanished">[红包]</translation>
    </message>
    <message>
        <source>[Secret Message]</source>
        <translation type="vanished">[密信]</translation>
    </message>
    <message>
        <source>[Secret Image]</source>
        <translation type="vanished">[密图]</translation>
    </message>
    <message>
        <source>[Secret File]</source>
        <translation type="vanished">[密件]</translation>
    </message>
    <message>
        <source>[Announcement]</source>
        <translation type="vanished">[通告]</translation>
    </message>
    <message>
        <source> joined the tribe</source>
        <translation type="obsolete">加入了部落</translation>
    </message>
    <message>
        <source>[Share]</source>
        <translation type="vanished">[分享]</translation>
    </message>
    <message>
        <source>[Location]</source>
        <translation type="vanished">[位置]</translation>
    </message>
    <message>
        <source>[Gravitational Waves]</source>
        <translation type="vanished">[引力波]</translation>
    </message>
    <message>
        <source> invited </source>
        <translation type="vanished">邀请</translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished">加入了部落</translation>
    </message>
    <message>
        <source>File received successfully</source>
        <oldsource>File receive successed</oldsource>
        <translation type="vanished">您已成功接收文件</translation>
    </message>
    <message>
        <source>Network request failed!</source>
        <translation type="vanished">网络请求失败！</translation>
    </message>
    <message>
        <source>The other party has successfully received your file!</source>
        <oldsource>The ohther party has successfully received your file!</oldsource>
        <translation type="vanished">对方已经成功接收您的文件！</translation>
    </message>
    <message>
        <source>Notice</source>
        <translation type="vanished">注意</translation>
    </message>
    <message>
        <source>Are you sure to dismiss this tribe?</source>
        <translation type="vanished">确定要解散此部落？</translation>
    </message>
    <message>
        <source>Are you sure to quit tribe?</source>
        <translation type="vanished">确定要退出此部落？</translation>
    </message>
    <message>
        <source>File received successfully </source>
        <translation type="vanished">您已成功接收对方发送的文件！</translation>
    </message>
    <message>
        <source>The other party has successfully received your file </source>
        <translation type="vanished">对方已经成功接收您的文件！</translation>
    </message>
    <message>
        <source>&lt;font color=&apos;#f7931e&apos; &gt;[Draft]&lt;/font&gt;</source>
        <oldsource>&lt;font color=&apos;#f7931e&apos; &gt;[草稿]&lt;/font&gt;</oldsource>
        <translation type="vanished">&lt;font color=&apos;#f7931e&apos; &gt;[草稿]&lt;/font&gt;</translation>
    </message>
</context>
<context>
    <name>MessageListDispatcher</name>
    <message>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="160"/>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="171"/>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="197"/>
        <source>Notice</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="160"/>
        <source>Are you sure to dismiss this group?</source>
        <translation>确定要解散此群组？</translation>
    </message>
    <message>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="171"/>
        <source>Are you sure to quit group?</source>
        <translation>确定要退出此群组？</translation>
    </message>
    <message>
        <source>Are you sure to dismiss this tribe?</source>
        <translation type="vanished">确定要解散此部落？</translation>
    </message>
    <message>
        <source>Are you sure to quit tribe?</source>
        <translation type="vanished">确定要退出此部落？</translation>
    </message>
    <message>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="197"/>
        <source>Network request failed!</source>
        <translation>网络请求失败！</translation>
    </message>
</context>
<context>
    <name>MessageListStore</name>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="148"/>
        <location filename="MessageWidget/messageliststore.cpp" line="725"/>
        <source> joined the group</source>
        <oldsource> joined the tribe</oldsource>
        <translation>加入了群组</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="150"/>
        <location filename="MessageWidget/messageliststore.cpp" line="727"/>
        <source> invited </source>
        <translation>邀请</translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished">加入了部落</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="150"/>
        <location filename="MessageWidget/messageliststore.cpp" line="727"/>
        <source> to join the group</source>
        <translation>加入了群组</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="156"/>
        <location filename="MessageWidget/messageliststore.cpp" line="558"/>
        <location filename="MessageWidget/messageliststore.cpp" line="733"/>
        <source>File received successfully </source>
        <translation>您已成功接收对方发送的文件！</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="158"/>
        <location filename="MessageWidget/messageliststore.cpp" line="562"/>
        <location filename="MessageWidget/messageliststore.cpp" line="735"/>
        <source>The other party has successfully received your file </source>
        <translation>对方已经成功接收您的文件！</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="195"/>
        <location filename="MessageWidget/messageliststore.cpp" line="198"/>
        <location filename="MessageWidget/messageliststore.cpp" line="537"/>
        <location filename="MessageWidget/messageliststore.cpp" line="785"/>
        <location filename="MessageWidget/messageliststore.cpp" line="788"/>
        <source>[Announcement]</source>
        <translation>[通告]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="209"/>
        <location filename="MessageWidget/messageliststore.cpp" line="212"/>
        <location filename="MessageWidget/messageliststore.cpp" line="544"/>
        <location filename="MessageWidget/messageliststore.cpp" line="798"/>
        <location filename="MessageWidget/messageliststore.cpp" line="801"/>
        <source>[Share]</source>
        <translation>[分享]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="319"/>
        <source>View Details</source>
        <translation>查看资料</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="323"/>
        <source>Close Chat</source>
        <translation>关闭会话</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="327"/>
        <source>Close All Chats</source>
        <translation>关闭全部会话</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="344"/>
        <source>Remove From Top</source>
        <translation>取消消息置顶</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="352"/>
        <source>Set Message Top</source>
        <translation>消息置顶</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="360"/>
        <source>Cancle No-Disturbing</source>
        <translation>开启消息提醒</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="368"/>
        <source>Set No-Disturbing</source>
        <translation>消息免打扰</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="386"/>
        <source>Dismiss Group</source>
        <oldsource>Dismiss Tribe</oldsource>
        <translation>解散群组</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="395"/>
        <source>Quit Group</source>
        <oldsource>Quit Tribe</oldsource>
        <translation>退出群组</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="490"/>
        <source>Yesterday</source>
        <translation>昨天</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="509"/>
        <source>[Image]</source>
        <translation>[图片]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="511"/>
        <source>[Audio]</source>
        <translation>[音频]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="513"/>
        <source>[Video]</source>
        <translation>[视频]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="515"/>
        <source>[File]</source>
        <translation>[文件]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="517"/>
        <source>This type of message is not supported for now</source>
        <translation>该消息类型暂不支持</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="519"/>
        <source>[Tranfer]</source>
        <translation>[转账]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="521"/>
        <source>[Red Packet]</source>
        <translation>[红包]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="523"/>
        <source>[Secret Message]</source>
        <translation>[密信]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="525"/>
        <source>[Secret Image]</source>
        <translation>[密图]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="527"/>
        <source>[Secret File]</source>
        <translation>[密件]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="529"/>
        <source>[Location]</source>
        <translation>[位置]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="531"/>
        <source>[Gravitational Waves]</source>
        <translation>[引力波]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="627"/>
        <location filename="MessageWidget/messageliststore.cpp" line="631"/>
        <location filename="MessageWidget/messageliststore.cpp" line="877"/>
        <location filename="MessageWidget/messageliststore.cpp" line="896"/>
        <location filename="MessageWidget/messageliststore.cpp" line="983"/>
        <location filename="MessageWidget/messageliststore.cpp" line="987"/>
        <location filename="MessageWidget/messageliststore.cpp" line="1223"/>
        <source>&lt;font color=&apos;#f7931e&apos; &gt;[I was @]&lt;/font&gt;</source>
        <translation>&lt;font color=&apos;#f7931e&apos; &gt;[有人@我]&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="1730"/>
        <source>&lt;font color=&apos;#f7931e&apos; &gt;[Draft]&lt;/font&gt;</source>
        <translation>&lt;font color=&apos;#f7931e&apos; &gt;[草稿]&lt;/font&gt;</translation>
    </message>
</context>
<context>
    <name>MessageLog</name>
    <message>
        <location filename="messagelog/messagelog.ui" line="26"/>
        <source>Message Log</source>
        <translation>消息记录</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="330"/>
        <source>Messsage Log</source>
        <translation>消息记录</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="484"/>
        <source>Friends</source>
        <translation>好友</translation>
    </message>
    <message>
        <source>Tribe</source>
        <translation type="vanished">部落</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="524"/>
        <source>Group</source>
        <translation>群组</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="578"/>
        <source>🔎</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="598"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="623"/>
        <source>Clear</source>
        <translation>清空</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="697"/>
        <source>about:blank</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="811"/>
        <source>|&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="837"/>
        <source>&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="857"/>
        <source>&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="877"/>
        <source>&gt;|</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="45"/>
        <source>微软雅黑</source>
        <translation></translation>
    </message>
    <message>
        <source>OpenPlanet,Best Regards</source>
        <translation type="vanished">星际通讯，恭喜发财</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1068"/>
        <source>Best Wishes</source>
        <translation>恭喜发财，大吉大利</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1212"/>
        <source>Empty file name or file doesn&apos;t exist!</source>
        <translation>文件名为空或文件不存在！</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1244"/>
        <source>fileName为空!OpenDocument Failed!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1333"/>
        <source>Sorry</source>
        <translation>抱歉</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1333"/>
        <source>There are no qualified message records</source>
        <translation>没有符合条件的消息记录</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1970"/>
        <source>This file already exists, overwrite or not?</source>
        <translation>该文件已存在，是否覆盖？</translation>
    </message>
    <message>
        <source>This file already exists, overwrite or not？</source>
        <translation type="vanished">该文件已存在，是否覆盖？</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="2150"/>
        <source>Save as</source>
        <translation>另存为</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1212"/>
        <location filename="messagelog/messagelog.cpp" line="1340"/>
        <location filename="messagelog/messagelog.cpp" line="1831"/>
        <location filename="messagelog/messagelog.cpp" line="1970"/>
        <source>Notice</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1340"/>
        <source>Unable to search empty content</source>
        <translation>搜索内容不能为空</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1831"/>
        <source>Failed to check the red packet!</source>
        <translation>查看红包失败！</translation>
    </message>
</context>
<context>
    <name>NoticeWidget</name>
    <message>
        <location filename="childWidget/noticewidget.ui" line="14"/>
        <source>NoticeWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.ui" line="111"/>
        <source>Announcement</source>
        <translation>通告</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.ui" line="204"/>
        <source>Click to Choose Image</source>
        <translation>点击选择图片</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.ui" line="243"/>
        <source>Please enter the title of the web page</source>
        <translation>请输入网页标题</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.ui" line="271"/>
        <source>Please enter the link of the web page</source>
        <translation>请输入网页链接</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.ui" line="388"/>
        <source>Send</source>
        <translation>发送</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="29"/>
        <source>Please enter the title of the image</source>
        <translation>请输入图片标题</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="30"/>
        <location filename="childWidget/noticewidget.cpp" line="35"/>
        <source>微软雅黑</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="77"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="78"/>
        <source>Image File</source>
        <translation>图像文件</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="137"/>
        <location filename="childWidget/noticewidget.cpp" line="145"/>
        <location filename="childWidget/noticewidget.cpp" line="150"/>
        <location filename="childWidget/noticewidget.cpp" line="155"/>
        <source>Notice</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="137"/>
        <source>Please Choose Image!</source>
        <translation>请选择图片！</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="145"/>
        <source>Please enter the title of the image！</source>
        <translation>请输入图片标题！</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="150"/>
        <source>Please enter the title of web page!</source>
        <translation>请输入网址标题！</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="155"/>
        <source>Please enter the URL！</source>
        <translation>请输入网址！</translation>
    </message>
    <message>
        <source>Image File(*.bmp;*.jpeg;*.jpg;*.png;*.gif)</source>
        <oldsource>图像文件(*.bmp;*.jpeg;*.jpg;*.png;*.gif)</oldsource>
        <translation type="vanished">图像文件(*.bmp;*.jpeg;*.jpg;*.png;*.gif)</translation>
    </message>
</context>
<context>
    <name>OpenLetterWidget</name>
    <message>
        <location filename="secretWidget/openletterwidget.ui" line="14"/>
        <source>OpenLetterWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.ui" line="78"/>
        <source>Secret Message</source>
        <translation>密信</translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.ui" line="205"/>
        <source>Enter Password</source>
        <translation>输入密码</translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.ui" line="255"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.cpp" line="78"/>
        <location filename="secretWidget/openletterwidget.cpp" line="85"/>
        <source>Notice</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.cpp" line="78"/>
        <source>The password cannot be empty!</source>
        <translation>密码不能为空！</translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.cpp" line="85"/>
        <source>Incorrect password!</source>
        <translation>密码输入错误！</translation>
    </message>
</context>
<context>
    <name>OpenPacketWidget</name>
    <message>
        <location filename="redPacketWidget/openPack.ui" line="32"/>
        <source>OpenPacket</source>
        <oldsource>OpenPacketWidget</oldsource>
        <translation>打开红包</translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.ui" line="189"/>
        <source>我是头像</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.ui" line="228"/>
        <source>我是昵称</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.ui" line="267"/>
        <location filename="redPacketWidget/openPack.cpp" line="88"/>
        <source>A red packet was sent to you</source>
        <oldsource>Send a Red Packet to You</oldsource>
        <translation>给你发了一个红包</translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.ui" line="306"/>
        <source>Best Wishes</source>
        <oldsource>Best Regards</oldsource>
        <translation>恭喜发财，大吉大利</translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.cpp" line="92"/>
        <source>Red racket with a identical amount</source>
        <oldsource>red racket with a identical amount</oldsource>
        <translation>红包金额固定</translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.cpp" line="96"/>
        <source>Red packet with a random amount</source>
        <oldsource>red packet with a random amount</oldsource>
        <translation>红包金额随机</translation>
    </message>
</context>
<context>
    <name>PackUnitWidget</name>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="17"/>
        <source>PackUnitWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="63"/>
        <source>PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="91"/>
        <source>11.0000000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="110"/>
        <source>11.000000000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="130"/>
        <source>Available</source>
        <translation>可用</translation>
    </message>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="146"/>
        <source>Frozen</source>
        <translation>冻结</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="59"/>
        <location filename="MessageWidget/messageliststore.cpp" line="63"/>
        <source>&lt;font color=&apos;#f7931e&apos; &gt;[I was @]&lt;/font&gt;</source>
        <oldsource>&lt;font color=&apos;#f7931e&apos; &gt;[有人@我]&lt;/font&gt;</oldsource>
        <translation>&lt;font color=&apos;#f7931e&apos; &gt;[有人@我]&lt;/font&gt;</translation>
    </message>
</context>
<context>
    <name>QWebEngineViewDelegate</name>
    <message>
        <location filename="qwebengineviewdelegate.ui" line="16"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>RedPackDetail</name>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="32"/>
        <source>redPackDetail</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="100"/>
        <source>Records</source>
        <translation>红包记录</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="138"/>
        <source>Details</source>
        <translation>红包详情</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="248"/>
        <source>我是头像</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="302"/>
        <source>我是昵称</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="343"/>
        <source>我是红包备注</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="416"/>
        <source>我是获得的红包金额</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="438"/>
        <source>我是红包单位</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="495"/>
        <source>我是多少个红包</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="527"/>
        <source>我是已领取红包</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.cpp" line="74"/>
        <source> red packets </source>
        <translation>个红包共</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.cpp" line="79"/>
        <source>taken </source>
        <translation>已领取</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.cpp" line="103"/>
        <source>most lucky</source>
        <translation>手气最佳</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.cpp" line="113"/>
        <source>微软雅黑</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>RedPackHistory</name>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="32"/>
        <source>RedPackHistory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="79"/>
        <source>Red Packet History</source>
        <translation>红包记录</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="190"/>
        <location filename="redPacketWidget/RedPackHistory.ui" line="428"/>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="184"/>
        <source>Received</source>
        <translation>我收到的</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="260"/>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="199"/>
        <source>Sent</source>
        <translation>我发出的</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="517"/>
        <source>Packets</source>
        <translation>个红包</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="574"/>
        <source>Tokens received can be transfered into local wallet</source>
        <translation>收到的红包已存入您的托管账户，可转出到本地钱包</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="345"/>
        <source>我是头像</source>
        <translation></translation>
    </message>
    <message>
        <source>Records</source>
        <translation type="vanished">红包记录</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="409"/>
        <source>我是昵称</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="495"/>
        <source>我是红包数量</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="84"/>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="134"/>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="175"/>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="190"/>
        <source>微软雅黑</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="185"/>
        <source>Tokens been deposited in your accounts now, which can be transfered into local wallet</source>
        <translation>收到的红包已存入您的托管账户，可转出到本地钱包</translation>
    </message>
</context>
<context>
    <name>SearchList</name>
    <message>
        <location filename="SearchWidget/searchlist.ui" line="16"/>
        <source>SearchList</source>
        <translation></translation>
    </message>
    <message>
        <location filename="SearchWidget/searchlist.cpp" line="120"/>
        <location filename="SearchWidget/searchlist.cpp" line="147"/>
        <location filename="SearchWidget/searchlist.cpp" line="150"/>
        <location filename="SearchWidget/searchlist.cpp" line="167"/>
        <location filename="SearchWidget/searchlist.cpp" line="228"/>
        <location filename="SearchWidget/searchlist.cpp" line="248"/>
        <source>Friends</source>
        <translation>好友</translation>
    </message>
    <message>
        <location filename="SearchWidget/searchlist.cpp" line="129"/>
        <location filename="SearchWidget/searchlist.cpp" line="185"/>
        <location filename="SearchWidget/searchlist.cpp" line="188"/>
        <location filename="SearchWidget/searchlist.cpp" line="201"/>
        <source>Group</source>
        <translation>群组</translation>
    </message>
    <message>
        <source>Tribe</source>
        <translation type="vanished">部落</translation>
    </message>
    <message>
        <location filename="SearchWidget/searchlist.cpp" line="225"/>
        <source>other friends</source>
        <translation>其他朋友</translation>
    </message>
</context>
<context>
    <name>SecretFileWidget</name>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="14"/>
        <source>SecretFileWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="105"/>
        <source>Secret File</source>
        <translation>密件</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="228"/>
        <source>Enter Password</source>
        <translation>输入密码</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="271"/>
        <source>Click &quot;add&quot; button to add secret file, double click file to cancel</source>
        <translation>点击“添加”添加密件，双击密件取消</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="332"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="357"/>
        <source>Send</source>
        <translation>发送</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="91"/>
        <source>Open File</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="91"/>
        <source>File (*.*)</source>
        <translation>文件 (*.*)</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="147"/>
        <location filename="secretWidget/secretfilewidget.cpp" line="151"/>
        <location filename="secretWidget/secretfilewidget.cpp" line="173"/>
        <location filename="secretWidget/secretfilewidget.cpp" line="211"/>
        <location filename="secretWidget/secretfilewidget.cpp" line="247"/>
        <source>Notice</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="147"/>
        <source>No File Selected!</source>
        <translation>未选择文件！</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="173"/>
        <source>File does not exist!</source>
        <translation>文件不存在！</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="247"/>
        <source>Failed to upload secret ile!</source>
        <translation></translation>
    </message>
    <message>
        <source>No File Choosen!</source>
        <translation type="vanished">未选择文件！</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="151"/>
        <source>Please Enter The Password!</source>
        <translation>未输入密码！</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="162"/>
        <source>Sending……</source>
        <translation>正在发送……</translation>
    </message>
    <message>
        <source>File Not Exists!</source>
        <translation type="vanished">文件不存在！</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="211"/>
        <source>Encryption Failed!</source>
        <translation>加密失败！</translation>
    </message>
    <message>
        <source>Failed to Upload Secret File!</source>
        <translation type="vanished">密件上传失败！</translation>
    </message>
</context>
<context>
    <name>SecretImageWidget</name>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="14"/>
        <source>SecretImageWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="93"/>
        <source>Secret Image</source>
        <translation>密图</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="210"/>
        <source>Enter Password</source>
        <translation>输入密码</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="343"/>
        <source>Click &quot;add&quot; button to add secret image, double click file to cancel</source>
        <translation>点击“添加”添加密图，双击密件取消</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="404"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="429"/>
        <source>Send</source>
        <translation>发送</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="91"/>
        <source>Open Image</source>
        <translation>打开图片</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="91"/>
        <source>Image File(*.bmp;*.jpeg;*.jpg;*.png)</source>
        <translation>图片文件(*.bmp;*.jpeg;*.jpg;*.png)</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="110"/>
        <location filename="secretWidget/secretimagewidget.cpp" line="114"/>
        <location filename="secretWidget/secretimagewidget.cpp" line="136"/>
        <location filename="secretWidget/secretimagewidget.cpp" line="172"/>
        <location filename="secretWidget/secretimagewidget.cpp" line="208"/>
        <source>Notice</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="110"/>
        <source>No Image Selected! </source>
        <translation>未选择图片！ </translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="136"/>
        <source>Image does not exist!</source>
        <translation>图片不存在！</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="208"/>
        <source>Failed to upload secret file!</source>
        <translation>密图上传失败!</translation>
    </message>
    <message>
        <source>No Image Choosen! </source>
        <translation type="vanished">未选择图片！ </translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="114"/>
        <source>Please Enter The Password!</source>
        <translation>未输入密码！</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="125"/>
        <source>Sending……</source>
        <translation>正在发送……</translation>
    </message>
    <message>
        <source>Image Not Exists!</source>
        <translation type="vanished">图片不存在！</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="172"/>
        <source>Encryption Failed!</source>
        <translation>加密失败！</translation>
    </message>
    <message>
        <source>Failed to Upload Secret File!</source>
        <translation type="vanished">密件上传失败！</translation>
    </message>
</context>
<context>
    <name>SecretLetterWidget</name>
    <message>
        <location filename="secretWidget/secretletterwidget.ui" line="14"/>
        <source>SecretLetterWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.ui" line="120"/>
        <source>Secret Message</source>
        <translation>密信</translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.ui" line="237"/>
        <source>Enter Password</source>
        <translation>输入密码</translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.ui" line="259"/>
        <source>Enter Message</source>
        <translation>输入密信</translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.ui" line="284"/>
        <source>Send</source>
        <translation>发送</translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.cpp" line="86"/>
        <source>Notice</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.cpp" line="86"/>
        <source>The password or message cannot be empty!</source>
        <translation>密码和密信都不能为空！</translation>
    </message>
    <message>
        <source>The password and the message cannot be empty!</source>
        <translation type="vanished">密码和密信都不能为空！</translation>
    </message>
</context>
<context>
    <name>TransAccWidget</name>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="20"/>
        <source>TransAccWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="71"/>
        <location filename="redPacketWidget/TransAccWidget.ui" line="200"/>
        <location filename="redPacketWidget/TransAccWidget.ui" line="482"/>
        <source>PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="87"/>
        <source>Transfer</source>
        <translation>转账</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="63"/>
        <source>Balance</source>
        <translation>托管余额</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="168"/>
        <source>Escrow Balance</source>
        <translation>托管余额</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="253"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="279"/>
        <source>Hosted Address</source>
        <oldsource>Host Address</oldsource>
        <translation>托管地址</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="307"/>
        <source>Amount</source>
        <translation>金额</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="338"/>
        <source>Enter Amount</source>
        <translation>请输入转账金额</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="368"/>
        <source>Payment Address</source>
        <translation>付款地址</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="443"/>
        <source>Cost of Miners</source>
        <translation>矿工费用</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="624"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="32"/>
        <source>Wallet</source>
        <translation>钱包</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="52"/>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="58"/>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="63"/>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="77"/>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="82"/>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="92"/>
        <source>Notice</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="52"/>
        <source>Payment Address Cannot be Empty!</source>
        <translation>付款地址不能为空！</translation>
    </message>
    <message>
        <source>Amount of Transfer Cannot be Empty</source>
        <translation type="vanished">转账金额不能为空！</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="58"/>
        <source>Amount of Transfer Cannot be Empty!</source>
        <translation>转账金额不能为空！</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="63"/>
        <source>The Amount of Tranfer is Greater Than</source>
        <translation>转账金额超过</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="69"/>
        <source>Please Enter Your Login Password:</source>
        <translation>请输入登录密码：</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="77"/>
        <source>Failed to Verify the Login Password!</source>
        <translation>验证登录密码失败！</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="82"/>
        <source>Login Password Cannot be Empty!</source>
        <translation>登录密码不能为空！</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="92"/>
        <source>Incorrect Password!</source>
        <translation>登录密码错误！</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="469"/>
        <location filename="redPacketWidget/TransAccWidget.ui" line="547"/>
        <source>0.0004</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="573"/>
        <source>0.006</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TransmitMessageWidget</name>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="14"/>
        <source>TransmitMessage</source>
        <oldsource>TransmitMessageWidget</oldsource>
        <translation>转发消息</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="155"/>
        <source>Send to</source>
        <translation>发送到</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="230"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="295"/>
        <source>Recently</source>
        <translation>最近聊天</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="374"/>
        <location filename="childWidget/transmitmessagewidget.cpp" line="494"/>
        <location filename="childWidget/transmitmessagewidget.cpp" line="497"/>
        <source>Friends</source>
        <translation>好友</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="408"/>
        <location filename="childWidget/transmitmessagewidget.cpp" line="529"/>
        <location filename="childWidget/transmitmessagewidget.cpp" line="532"/>
        <source>Groups</source>
        <translation>群组</translation>
    </message>
    <message>
        <source>Tribes</source>
        <translation type="vanished">部落</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="513"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="544"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.cpp" line="390"/>
        <source>Notice</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.cpp" line="390"/>
        <source>Please choose a friend or a group</source>
        <translation>请选择转发的目标好友或者群组</translation>
    </message>
    <message>
        <source>Please choose a friend or a tribe</source>
        <translation type="vanished">请选择转发的目标好友或者部落</translation>
    </message>
</context>
</TS>
