﻿#include "qwebenginepagedelegate.h"
#include <QWebChannel>
#include "qwebengineviewmanager.h"
#include "QStringLiteralBak.h"

QWebEnginePageDelegate::QWebEnginePageDelegate(QWebEnginePage* page, const QString& chatId)
	: m_page(page),m_chatId(chatId)
{
	
}

QWebEnginePageDelegate::~QWebEnginePageDelegate()
{
	
}

void QWebEnginePageDelegate::setBackgroundColor(const QColor &color)
{
	if (!QWebEngineViewManager::instance()->m_settingFlagsMap[m_page].isSetBackgroundColor)
	{
		QWebEngineViewManager::instance()->m_settingFlagsMap[m_page].isSetBackgroundColor = true;
		m_page->setBackgroundColor(color);
	}
	else
	{
		//DO NOTHING
	}

	
}

void QWebEnginePageDelegate::setWebChannel(QWebChannel *channel)
{
	if (!QWebEngineViewManager::instance()->m_settingFlagsMap[m_page].isSetWebChannel)
	{
		QWebEngineViewManager::instance()->m_settingFlagsMap[m_page].isSetWebChannel = true;
		m_page->setWebChannel(channel);
	}
	else
	{
		//因为复用的原因，已经设置过WebChannel无须再设置
	}
}

void QWebEnginePageDelegate::runJavaScript(const QString &scriptSource)
{
	//此处需要进入到iframe里进行调用
	QString prefix = QString("ChatIFrame(\"%1\").").arg(m_chatId);
	m_page->runJavaScript(prefix + scriptSource);
}