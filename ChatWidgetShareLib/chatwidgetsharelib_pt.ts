<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>AtWidget</name>
    <message>
        <location filename="SearchWidget/atwidget.ui" line="14"/>
        <source>AtWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="SearchWidget/atwidget.cpp" line="248"/>
        <source>@</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ChatDataManager</name>
    <message>
        <location filename="chatdatamanager.cpp" line="760"/>
        <location filename="chatdatamanager.cpp" line="824"/>
        <location filename="chatdatamanager.cpp" line="855"/>
        <source>Warning</source>
        <translation>Aviso</translation>
    </message>
    <message>
        <location filename="chatdatamanager.cpp" line="760"/>
        <location filename="chatdatamanager.cpp" line="824"/>
        <source>There are files being transferred，contuinue to close ?</source>
        <translation>Existem arquivos sendo transferidos, continuam a fechar?</translation>
    </message>
    <message>
        <location filename="chatdatamanager.cpp" line="855"/>
        <source>There are files being transferred，contuinue to quit ?</source>
        <translation>Existem arquivos sendo transferidos, continuam a sair?</translation>
    </message>
    <message>
        <source>Download task is running，contuinue to close ?</source>
        <translation type="vanished">Tarefa de download está em execução, continue a fechar?</translation>
    </message>
    <message>
        <source>Download task is running，contuinue to Quit ?</source>
        <translation type="vanished">Tarefa de download está em execução, continue a sair?</translation>
    </message>
    <message>
        <location filename="chatdatamanager.cpp" line="1319"/>
        <source>The other party has successfully received your file</source>
        <translation>A outra parte recebeu com sucesso seu arquivo</translation>
    </message>
</context>
<context>
    <name>ChatWidget</name>
    <message>
        <location filename="chatwidget.ui" line="14"/>
        <source>ChatWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="chatwidget.ui" line="117"/>
        <location filename="chatwidget.ui" line="162"/>
        <location filename="chatwidget.ui" line="190"/>
        <source>Search</source>
        <translation>Procurar</translation>
    </message>
    <message>
        <location filename="chatwidget.ui" line="215"/>
        <source>View device list</source>
        <translation>Ver lista de dispositivos</translation>
    </message>
    <message>
        <location filename="chatwidget.ui" line="272"/>
        <source>当前账户在其他1台设备登录</source>
        <translation></translation>
    </message>
    <message>
        <location filename="chatwidget.cpp" line="149"/>
        <source>Account logged in on other </source>
        <translation>Conta conectada </translation>
    </message>
    <message>
        <location filename="chatwidget.cpp" line="149"/>
        <source> device(s)</source>
        <translation> Terminal(s)</translation>
    </message>
</context>
<context>
    <name>ChooseUnitWidget</name>
    <message>
        <location filename="redPacketWidget/ChooseUnitWidget.ui" line="26"/>
        <source>ChooseUnitWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/ChooseUnitWidget.ui" line="77"/>
        <source>Choose red packet token</source>
        <translation>Escolha o token de pacote vermelho</translation>
    </message>
    <message>
        <location filename="redPacketWidget/ChooseUnitWidget.ui" line="195"/>
        <source>OK</source>
        <translation>Está bem</translation>
    </message>
</context>
<context>
    <name>EnterPasswordWidget</name>
    <message>
        <location filename="secretWidget/enterpasswordwidget.ui" line="14"/>
        <source>EnterPasswordWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.ui" line="82"/>
        <source>Please enter password of the base:</source>
        <translation>Por favor, digite a senha da base:</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.ui" line="101"/>
        <source>Base password is used to decrypt the password of the message</source>
        <translation>A senha básica é usada para descriptografar a senha da mensagem</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.ui" line="126"/>
        <source>Please enter the base password</source>
        <translation>Por favor, digite a senha base</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.ui" line="169"/>
        <source>OK</source>
        <translation>Está bem</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="64"/>
        <source>Please enter the password</source>
        <translation>Por favor insira a senha</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="65"/>
        <source>Enter the password to view the content</source>
        <translation>Digite a senha para ver o conteúdo</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="144"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="149"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="159"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="171"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="178"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="202"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="209"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="242"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="251"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="319"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="144"/>
        <source>Decryption failure！Error code 113</source>
        <translation>Falha de descriptografia！ Código de erro 113</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="149"/>
        <source>Password cannot be empty！</source>
        <translation>A senha não pode estar vazia！</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="159"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="251"/>
        <source>Incorrect password！</source>
        <translation>Senha incorreta!</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="171"/>
        <source>Decryption failure！Error code 137</source>
        <translation>Falha de descriptografia！ Código de erro 137</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="178"/>
        <source>Decryption failure！Error code 144</source>
        <translation>Falha de descriptografia！ Código de erro 144</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="202"/>
        <source>Please observe the surrounding environment before viewing the password to avoid leaking your password.
The password is only displayed once, please be sure to save the password</source>
        <translation>Por favor, observe o ambiente ao redor antes de visualizar a senha para evitar vazar sua senha.
A senha é exibida apenas uma vez, por favor, certifique-se de salvar a senha</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="209"/>
        <source>The password cannot be empty！</source>
        <translation>A senha não pode estar vazia！</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="242"/>
        <source>The secret picture/file is empty，please download again！</source>
        <translation>A imagem secreta / arquivo está vazio ， faça o download novamente！</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="319"/>
        <source>Fail to download the secret picture/file!</source>
        <translation>Falha ao baixar a imagem / arquivo secreto!</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="329"/>
        <source>The password is:&quot;</source>
        <translation>A senha é: &quot;</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="329"/>
        <source>&quot;,Please remember the password, as the system will not save the password</source>
        <translation>&quot;, Por favor, lembre-se da senha, pois o sistema não salvará a senha</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="332"/>
        <source>View  password</source>
        <translation>Ver senha</translation>
    </message>
</context>
<context>
    <name>ExpressWidget</name>
    <message>
        <location filename="childWidget/expresswidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="108"/>
        <source>[smile]</source>
        <translation>[sorriso]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="109"/>
        <source>[twitch mouth]</source>
        <translation>[boca de contração]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="110"/>
        <source>[salivate]</source>
        <translation>[salivar]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="111"/>
        <source>[staring blankly]</source>
        <translation>[olhando inexpressivamente]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="112"/>
        <source>[complacent]</source>
        <translation>[complacente]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="113"/>
        <source>[shy]</source>
        <translation>[tímida]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="114"/>
        <source>[shut up]</source>
        <translation>[Cale-se]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="115"/>
        <source>[sleep]</source>
        <translation>[dormir]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="116"/>
        <source>[cry]</source>
        <translation>[choro]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="117"/>
        <source>[awkward]</source>
        <translation>[estranho]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="118"/>
        <source>[angry]</source>
        <translation>[bravo]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="119"/>
        <source>[naughty]</source>
        <translation>[danadinho]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="120"/>
        <source>[grimace]</source>
        <translation>[careta]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="121"/>
        <source>[suprised]</source>
        <translation>[surpreso]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="122"/>
        <source>[sad]</source>
        <translation>[triste]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="123"/>
        <source>[cool]</source>
        <translation>[legal]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="124"/>
        <source>[cold sweat]</source>
        <translation>[suor frio]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="125"/>
        <source>[crazy]</source>
        <translation>[louco]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="126"/>
        <source>[spit]</source>
        <translation>[cuspir]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="127"/>
        <source>[titter]</source>
        <translation>[titter]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="128"/>
        <source>[supercilious look]</source>
        <translation>[olhar arrogante]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="129"/>
        <source>[cute]</source>
        <translation>[fofo]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="130"/>
        <source>[arrogance]</source>
        <translation>[arrogância]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="131"/>
        <source>[hungry]</source>
        <translation>[com fome]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="132"/>
        <source>[sleepy]</source>
        <translation>[sonolento]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="133"/>
        <source>[terrified]</source>
        <translation>[aterrorizado]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="134"/>
        <source>[sweat]</source>
        <translation>[suor]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="135"/>
        <source>[smile fatuously]</source>
        <translation>[sorriso estupidamente]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="136"/>
        <source>[soldier]</source>
        <translation>[soldado]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="137"/>
        <source>[strive]</source>
        <translation>[esforçar]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="138"/>
        <source>[doubting]</source>
        <translation>[duvidando]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="139"/>
        <source>[hash]</source>
        <translation>[jogo da velha]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="140"/>
        <source>[dizzy]</source>
        <translation>[tonto]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="141"/>
        <source>[pig&apos;s head]</source>
        <translation>[cabeça de porco]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="142"/>
        <source>[skeleton]</source>
        <translation>[esqueleto]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="143"/>
        <source>[unlucky]</source>
        <translation>[azarado]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="144"/>
        <source>[knock]</source>
        <translation>[batido]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="145"/>
        <source>[good bye]</source>
        <translation>[Tchau]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="146"/>
        <source>[clap hands]</source>
        <translation>[bater palmas]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="147"/>
        <source>[pick nose]</source>
        <translation>[escolha o nariz]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="148"/>
        <source>[embarassed]</source>
        <translation>[embaraçado]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="149"/>
        <source>[snicker]</source>
        <translation>[Sorriso]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="150"/>
        <source>[left hem]</source>
        <translation>[hem esquerda]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="151"/>
        <source>[right hem]</source>
        <translation>[hem direita]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="152"/>
        <source>[yawn]</source>
        <translation>[bocejar]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="153"/>
        <source>[despise]</source>
        <translation>[desprezo]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="154"/>
        <source>[grievance]</source>
        <translation>[reclamação]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="155"/>
        <source>[about to weep]</source>
        <translation>[prestes a chorar]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="156"/>
        <source>[insidious]</source>
        <translation>[insidioso]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="157"/>
        <source>[kiss]</source>
        <translation>[beijo]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="158"/>
        <source>[scared]</source>
        <translation>[assustado]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="159"/>
        <source>[pitiful]</source>
        <translation>[lamentável]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="160"/>
        <source>[kitchen knife]</source>
        <translation>[faca de cozinha]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="161"/>
        <source>[watermelon]</source>
        <translation>[Melancia]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="162"/>
        <source>[beer]</source>
        <translation>[Cerveja]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="163"/>
        <source>[ping pong]</source>
        <translation>[pingue-pongue]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="164"/>
        <source>[coffee]</source>
        <translation>[café]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="165"/>
        <source>[rice]</source>
        <translation>[arroz]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="166"/>
        <source>[rose]</source>
        <translation>[rosa]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="167"/>
        <source>[withered]</source>
        <translation>[murchado]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="168"/>
        <source>[love]</source>
        <translation>[amor]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="169"/>
        <source>[loving heart]</source>
        <translation>[coração apaixonado]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="170"/>
        <source>[heart break]</source>
        <translation>[desgosto]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="171"/>
        <source>[cake]</source>
        <translation>[bolo]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="172"/>
        <source>[flash]</source>
        <translation>[instantâneo]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="173"/>
        <source>[bomb]</source>
        <translation>[bombear]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="174"/>
        <source>[knife]</source>
        <translation>[faca]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="175"/>
        <source>[beetles]</source>
        <translation>[besouros]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="176"/>
        <source>[shit]</source>
        <translation>[merda]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="177"/>
        <source>[moon]</source>
        <translation>[lua]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="178"/>
        <source>[sun]</source>
        <translation>[Dom]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="179"/>
        <source>[gift]</source>
        <translation>[presente]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="180"/>
        <source>[embrace]</source>
        <translation>[abraço]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="181"/>
        <source>[strong]</source>
        <translation>[Forte]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="182"/>
        <source>[weak]</source>
        <translation>[fraco]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="183"/>
        <source>[shake hands]</source>
        <translation>[apertar as mãos]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="184"/>
        <source>[hold fist salute]</source>
        <translation>[segure a saudação do punho]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="185"/>
        <source>[seduce]</source>
        <translation>[seduzir]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="186"/>
        <source>[fist]</source>
        <translation>[punho]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="187"/>
        <source>[disappointed]</source>
        <translation>[desapontado]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="188"/>
        <source>[love you]</source>
        <translation>[vos amo]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="189"/>
        <source>[NO]</source>
        <translation>[NÃO]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="190"/>
        <source>[OK]</source>
        <translation>[ESTÁ BEM]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="191"/>
        <source>[weep]</source>
        <translation>[chorar]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="192"/>
        <source>[curse]</source>
        <translation>[maldição]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="193"/>
        <source>[weep sweat]</source>
        <translation>[chore o suor]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="194"/>
        <source>[victory]</source>
        <translation>[vitória]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="195"/>
        <source>[basketball]</source>
        <translation>[basquetebol]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="196"/>
        <source>[soccer]</source>
        <translation>[futebol]</translation>
    </message>
</context>
<context>
    <name>GiveRedPackWidget</name>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="26"/>
        <source>GiveRedPack</source>
        <oldsource>GiveRedPackWidget</oldsource>
        <translation>Dar pacote vermelho</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="83"/>
        <source>Red Packet</source>
        <translation>Pacote vermelho</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="105"/>
        <source>Close</source>
        <translation>Perto</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="166"/>
        <source>Red Packet Token</source>
        <translation>Token de Pacote Vermelho</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="195"/>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="382"/>
        <source>PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="251"/>
        <source>Escrow Balance</source>
        <translation>Saldo de Custódia</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="275"/>
        <source>0PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="294"/>
        <source>Transfer</source>
        <translation>Transferir</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="338"/>
        <source>Amount Each</source>
        <translation>Quantidade Cada</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="363"/>
        <source>Set Amount</source>
        <translation>Montante montante</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="416"/>
        <source>Message</source>
        <translation>Mensagem</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="438"/>
        <source>Best wishes</source>
        <translation>Muitas felicidades</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="478"/>
        <source>Amount 0PWR</source>
        <translation>Montante 0PWR</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="518"/>
        <source>Red Packet History</source>
        <translation>História do Pacote Vermelho</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="552"/>
        <source>Prepare Red Packet</source>
        <translation>Prepare o pacote vermelho</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="590"/>
        <source>Unclaimed red packet will be refunded after 24 hours</source>
        <translation>Pacote vermelho não reclamado será devolvido após 24 horas</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="138"/>
        <source>Amount: </source>
        <translation>Montante: </translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="146"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="180"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="189"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="195"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="213"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="218"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="228"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="146"/>
        <source>Failed to prepare the red packet!</source>
        <translation>Falha ao preparar o pacote vermelho!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="153"/>
        <source>Best wishes!</source>
        <translation>Muitas felicidades!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="180"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="189"/>
        <source>Amount cannot be empty!</source>
        <translation>O valor não pode estar vazio!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="195"/>
        <source>The amount of the red packet cannot be greater than your balance!</source>
        <translation>A quantidade do pacote vermelho não pode ser maior que seu saldo!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="201"/>
        <source>Please enter your login password</source>
        <translation>Por favor, digite sua senha de login</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="213"/>
        <source>Password Validation Failed!</source>
        <translation>Validação de senha falhou!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="218"/>
        <source>Password cannot be empty!</source>
        <translation>A senha não pode estar vazia!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="228"/>
        <source>Incorrect Password!</source>
        <translation>Senha incorreta!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="235"/>
        <source>Best Wishes!</source>
        <translation>Muitas felicidades!</translation>
    </message>
</context>
<context>
    <name>GroupAddBuddyWidget</name>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="20"/>
        <source>GroupAddBuddy</source>
        <oldsource>GroupAddBuddyWidget</oldsource>
        <translation>Grupo Adicionar amigo</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="161"/>
        <source>Select contact(s)</source>
        <translation>Selecione contato</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="239"/>
        <source>Search</source>
        <translation>Procurar</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="294"/>
        <source>Please select contact(s) you want to invite</source>
        <translation>Por favor selecione contato (s) que você deseja convidar</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="367"/>
        <source>OK</source>
        <translation>Está bem</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="398"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="197"/>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="447"/>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="645"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <source>This member has been in the tribe already!</source>
        <translation type="vanished">Este membro já está na tribo!</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="197"/>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="447"/>
        <source>This member has been in the group already!</source>
        <translation>Este membro já está no grupo!</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="266"/>
        <source>You have selected </source>
        <translation>Você selecionou</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="266"/>
        <source> contact(s)</source>
        <translation>contato</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="269"/>
        <source>Please select contacts you want to invite</source>
        <translation>Por favor, selecione os contatos que você deseja convidar</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="625"/>
        <source> invited </source>
        <translation>convidamos</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="625"/>
        <source> to join the group</source>
        <translation>para se juntar ao grupo</translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished">para se juntar à tribo</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="645"/>
        <source>Added Successfully!</source>
        <translation>Adicionado com sucesso!</translation>
    </message>
</context>
<context>
    <name>GroupChatWidget</name>
    <message>
        <location filename="groupchatwidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="89"/>
        <source>Font</source>
        <translation>Fonte</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="95"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="120"/>
        <source>Expressions</source>
        <translation>Expressões</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="148"/>
        <source>Vibration</source>
        <translation>Vibração</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="176"/>
        <source>Picture</source>
        <translation>Cenário</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="204"/>
        <source>Screen Cut</source>
        <translation>Corte de tela</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="232"/>
        <source>File</source>
        <translation>Arquivo</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="260"/>
        <source>Announcement</source>
        <translation>Anúncio</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="288"/>
        <source>Mute All</source>
        <translation>Silencie tudo</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="319"/>
        <source>Red Packet</source>
        <translation>Pacote vermelho</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="362"/>
        <source>Message Log</source>
        <translation>Log de mensagens</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="389"/>
        <source>Send</source>
        <translation>Mandar</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="392"/>
        <source>Alt+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="482"/>
        <source>Group Members</source>
        <translation>Membros do grupo</translation>
    </message>
    <message>
        <source>Tribe Members</source>
        <translation type="vanished">Membros da tribo</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="510"/>
        <source>Search</source>
        <translation>Procurar</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="547"/>
        <source>Add Member</source>
        <translation>Adicionar membro</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="588"/>
        <source>Refresh</source>
        <translation>Atualizar</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="vanished">cópia de</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation type="vanished">Cortar</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="vanished">Colar</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Excluir</translation>
    </message>
    <message>
        <source>Empty Filename! Open Document Failed!</source>
        <translation type="vanished">Nome de arquivo vazio! Documento aberto falhou!</translation>
    </message>
    <message>
        <source>Notice</source>
        <translation type="vanished">Aviso prévio</translation>
    </message>
    <message>
        <source>Empty file name or file doesn&apos;t exist!</source>
        <translation type="vanished">Nome de arquivo vazio ou arquivo não existe!</translation>
    </message>
    <message>
        <source>Cannot send file larger than 100M in size</source>
        <translation type="vanished">Não é possível enviar arquivos com tamanho maior que 100 M</translation>
    </message>
    <message>
        <source>[File]</source>
        <translation type="vanished">[Arquivo]</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Abrir</translation>
    </message>
    <message>
        <source>Image File(*.bmp;*.jpeg;*.jpg;*.png;*.gif)</source>
        <translation type="vanished">Arquivo de imagem(*.bmp;*.jpeg;*.jpg;*.png;*.gif)</translation>
    </message>
    <message>
        <source>Image File</source>
        <translation type="vanished">Arquivo de imagem</translation>
    </message>
    <message>
        <source>[Image]</source>
        <translation type="vanished">[Imagem]</translation>
    </message>
    <message>
        <source>[Red Packet]</source>
        <translation type="vanished">[Pacote vermelho]</translation>
    </message>
    <message>
        <source>Best Wishes</source>
        <translation type="vanished">Muitas felicidades</translation>
    </message>
    <message>
        <source> joined the tribe</source>
        <translation type="vanished">juntou-se a tribo</translation>
    </message>
    <message>
        <source> invited </source>
        <translation type="vanished">convidamos</translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished">para se juntar à tribo</translation>
    </message>
    <message>
        <source>The other party has successfully received your file</source>
        <translation type="vanished">A outra parte recebeu com sucesso seu arquivo</translation>
    </message>
    <message>
        <source>This file already exists, overwrite or not？</source>
        <translation type="vanished">Este arquivo já existe, sobrescrever ou não？</translation>
    </message>
    <message>
        <source>This file already exists, overwrite or not?</source>
        <translation type="vanished">Este arquivo já existe, sobrescrever ou não？</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation type="vanished">Salvar como</translation>
    </message>
    <message>
        <source> Members %1</source>
        <translation type="vanished">Membro tribal %1</translation>
    </message>
    <message>
        <source>Cancel Administrator Privileges</source>
        <translation type="vanished">Cancelar privilégios de administrador</translation>
    </message>
    <message>
        <source>Set as Administrator</source>
        <translation type="vanished">Definir como administrador</translation>
    </message>
    <message>
        <source>View Profile</source>
        <translation type="vanished">Ver Perfil</translation>
    </message>
    <message>
        <source>Set Tribal Name</source>
        <translation type="vanished">Definir nome tribal</translation>
    </message>
    <message>
        <source>Remove Tribe Member</source>
        <translation type="vanished">Remover membro da tribo</translation>
    </message>
    <message>
        <source>Please enter your alias:</source>
        <translation type="vanished">Por favor, digite seu alias:</translation>
    </message>
    <message>
        <source>Fail to check the red packet!</source>
        <translation type="vanished">Falha ao verificar o pacote vermelho!</translation>
    </message>
    <message>
        <source>Add as friend</source>
        <translation type="vanished">Adicionar como amigo</translation>
    </message>
    <message>
        <source>Adding as friend succeed!</source>
        <translation type="vanished">Adicionando como amigo sucesso!</translation>
    </message>
    <message>
        <source>Adding as friend failed!</source>
        <translation type="vanished">Adicionando como amigo falhou!</translation>
    </message>
    <message>
        <source>Tribe has been silenced...</source>
        <translation type="vanished">Tribo foi silenciada ...</translation>
    </message>
    <message>
        <source>Personal Business Card</source>
        <translation type="vanished">Cartão pessoal</translation>
    </message>
    <message>
        <source>Personal ID:</source>
        <translation type="vanished">ID pessoal:</translation>
    </message>
    <message>
        <source>Tribe Business Card</source>
        <translation type="vanished">Cartão de visita do tribo</translation>
    </message>
    <message>
        <source>Tribe ID:</source>
        <translation type="vanished">ID da Tribo:</translation>
    </message>
    <message>
        <source>Local file does&apos;t exist and cannot be forwarded</source>
        <translation type="vanished">Arquivo local não existe e não pode ser encaminhado</translation>
    </message>
    <message>
        <source>Resend</source>
        <translation type="vanished">Reenviar</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation type="vanished">Recarregar</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="vanished">Encaminhar</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="vanished">Claro</translation>
    </message>
    <message>
        <source>[Announcement]</source>
        <translation type="vanished">[Anúncio]</translation>
    </message>
    <message>
        <source>Received the file successfully!</source>
        <translation type="vanished">Recebeu o arquivo com sucesso!</translation>
    </message>
</context>
<context>
    <name>GroupFileWidget</name>
    <message>
        <location filename="groupfilewidget.ui" line="14"/>
        <source>GroupFileWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="groupfilewidget.ui" line="89"/>
        <source>file</source>
        <translation>Arquivo</translation>
    </message>
    <message>
        <location filename="groupfilewidget.ui" line="94"/>
        <source>upload time</source>
        <translation>tempo de upload</translation>
    </message>
    <message>
        <location filename="groupfilewidget.ui" line="99"/>
        <source>size</source>
        <translation>Tamanho</translation>
    </message>
    <message>
        <location filename="groupfilewidget.ui" line="104"/>
        <source>uploaded by</source>
        <translation>Enviado por</translation>
    </message>
    <message>
        <location filename="groupfilewidget.cpp" line="67"/>
        <source>%1 files in total</source>
        <translation>%1 arquivos no total</translation>
    </message>
    <message>
        <location filename="groupfilewidget.cpp" line="129"/>
        <source>unknown</source>
        <translation>desconhecido</translation>
    </message>
    <message>
        <location filename="groupfilewidget.cpp" line="172"/>
        <source>open file</source>
        <oldsource>open</oldsource>
        <translation>Arquivo aberto</translation>
    </message>
</context>
<context>
    <name>GroupPackWidget</name>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="26"/>
        <source>GroupPackWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="83"/>
        <source>Red Packet</source>
        <translation>Pacote Vermelho</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="105"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="156"/>
        <source>Random Red Packet</source>
        <translation>Pacote vermelho aleatório</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="210"/>
        <source>Identical Red Packet</source>
        <translation>Pacote Vermelho Idêntico</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="299"/>
        <source>Red Packet Token</source>
        <translation>Token de Pacote Vermelho</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="328"/>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="569"/>
        <source>PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="384"/>
        <source>Escrow Balance</source>
        <translation>Saldo de Custódia</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="408"/>
        <source>0PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="427"/>
        <source>Transfer</source>
        <translation>Transferir</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="480"/>
        <source>Total Amount</source>
        <translation>Valor Total</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="516"/>
        <source>拼</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="550"/>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="659"/>
        <source>Enter Number</source>
        <translation>Digite o número</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="603"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="273"/>
        <source>Each person gets a random amount</source>
        <translation>Cada pessoa recebe um valor aleatório</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="637"/>
        <source>Number of Red Packets</source>
        <translation>Número de pacotes vermelhos</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="687"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="260"/>
        <source>There are</source>
        <translation>Existem</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="718"/>
        <source>Message</source>
        <translation>Mensagem</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="740"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="183"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="220"/>
        <source>Best Wishes</source>
        <translation>Felicidades</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="786"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="277"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="291"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="317"/>
        <source>Amount 0PWR</source>
        <translation>Quantidade 0PWR</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="826"/>
        <source>Red Packet History</source>
        <translation>História do Pacote Vermelho</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="860"/>
        <source>Prepare Red Packet</source>
        <translation>Prepare o pacote vermelho</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="898"/>
        <source>Unclaimed red packet will be refunded after 24 hours</source>
        <translation>Pacote vermelho não reclamado será devolvido após 24 horas</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="107"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="117"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="124"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="129"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="135"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="161"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="166"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="176"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="213"/>
        <source>Notice</source>
        <translation>Aviso</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="107"/>
        <source>The amount or the number of the red packet cannot be empty!</source>
        <translation>A quantidade ou o número do pacote vermelho não pode estar vazio!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="117"/>
        <source>The amount or the number of the red packet cannot be 0 !</source>
        <translation>A quantidade ou o número do pacote vermelho não pode ser 0!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="124"/>
        <source>The amount of the red packet cannot be greater than your balance!</source>
        <translation>A quantidade do pacote vermelho não pode ser maior que seu saldo!</translation>
    </message>
    <message>
        <source>The number of the red packet cannot be greater than the number of people in the tribe!</source>
        <translation type="vanished">O número do pacote vermelho não pode ser maior que o número de pessoas da tribo!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="129"/>
        <source>The number of the red packet cannot be greater than the number of people in the group!</source>
        <translation>O número do pacote vermelho não pode ser maior que o número de pessoas no grupo!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="135"/>
        <source>The amount of each red packet cannot be smaller than 0.00001!</source>
        <translation>A quantidade de cada pacote vermelho não pode ser menor que 0,00001!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="140"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="147"/>
        <source>Please enter your login password:</source>
        <translation>Por favor, digite sua senha de login:</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="161"/>
        <source>Failed to verify the login password!</source>
        <translation>Falha ao verificar a senha de login!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="166"/>
        <source>Login password cannot be empty!</source>
        <translation>A senha de login não pode estar vazia!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="176"/>
        <source>Incorrect Password!</source>
        <translation>Senha incorreta!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="213"/>
        <source>Failed to prepare the red packet!</source>
        <translation>Falha ao preparar o pacote vermelho!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="260"/>
        <source>people in this group</source>
        <translation>pessoas neste grupo</translation>
    </message>
    <message>
        <source>people in this tribe</source>
        <translation type="vanished">pessoas nesta tribo</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="271"/>
        <source>Amount</source>
        <translation>Montante</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="285"/>
        <source>Amount Each</source>
        <translation>Quantidade Cada</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="287"/>
        <source>Each person gets a identical amount</source>
        <translation>Cada pessoa recebe uma quantia idêntica</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="294"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="305"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="322"/>
        <source>Amount </source>
        <translation>Montante </translation>
    </message>
</context>
<context>
    <name>GroupSearchWidget</name>
    <message>
        <location filename="childWidget/groupsearchwidget.ui" line="23"/>
        <source>GroupSearchWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childWidget/groupsearchwidget.ui" line="74"/>
        <source>Find Group Member</source>
        <translation>Encontrar membro do grupo</translation>
    </message>
    <message>
        <source>Find Tribe Member</source>
        <translation type="vanished">Encontre membros da tribo</translation>
    </message>
</context>
<context>
    <name>GroupUserProfileWidget</name>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="14"/>
        <source>GroupUserProfile</source>
        <oldsource>GroupUserProfileWidget</oldsource>
        <translation>Perfil do usuário do grupo</translation>
    </message>
    <message>
        <source>Personal ID</source>
        <translation type="vanished">ID pessoal</translation>
    </message>
    <message>
        <source>Base ID</source>
        <translation type="vanished">ID base</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="111"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="188"/>
        <source>Acct.No</source>
        <translation>Conta</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="234"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="336"/>
        <source>View Avatar</source>
        <translation>Visualizar Avatar</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="630"/>
        <source>Add as friend</source>
        <translation>Adicionar como amigo</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.cpp" line="25"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.cpp" line="70"/>
        <source>Send</source>
        <translation>Enviar</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.cpp" line="189"/>
        <source>Request sent successfully</source>
        <translation>Pedido enviado com sucesso</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.cpp" line="193"/>
        <source>Request sent Failed</source>
        <translation>Pedido enviado com falha</translation>
    </message>
</context>
<context>
    <name>GroupWidget</name>
    <message>
        <location filename="groupwidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="groupwidget.ui" line="83"/>
        <source>View the Details</source>
        <translation>Visualizar informações</translation>
    </message>
    <message>
        <location filename="groupwidget.ui" line="133"/>
        <source>chat</source>
        <translation>bate-papo</translation>
    </message>
    <message>
        <location filename="groupwidget.ui" line="176"/>
        <source>file</source>
        <translation>Arquivo</translation>
    </message>
</context>
<context>
    <name>IMGroupChatStore</name>
    <message>
        <location filename="imgroupchatstore.cpp" line="495"/>
        <location filename="imgroupchatstore.cpp" line="1260"/>
        <location filename="imgroupchatstore.cpp" line="2109"/>
        <source>Best Wishes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="598"/>
        <location filename="imgroupchatstore.cpp" line="1413"/>
        <location filename="imgroupchatstore.cpp" line="2267"/>
        <source>The other party has successfully received your file</source>
        <translation>A outra parte recebeu com sucesso seu arquivo</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="933"/>
        <location filename="imgroupchatstore.cpp" line="1765"/>
        <location filename="imgroupchatstore.cpp" line="1809"/>
        <location filename="imgroupchatstore.cpp" line="2577"/>
        <source>Notice</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="933"/>
        <source>This file already exists, overwrite or not?</source>
        <translation>Este arquivo já existe, sobrescrever ou não？</translation>
    </message>
    <message>
        <source> joined the tribe</source>
        <translation type="vanished">juntou-se a tribo</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1409"/>
        <location filename="imgroupchatstore.cpp" line="2263"/>
        <location filename="imgroupchatstore.cpp" line="2986"/>
        <source> invited </source>
        <translation>convidamos</translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished">para se juntar à tribo</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1623"/>
        <location filename="imgroupchatstore.cpp" line="1631"/>
        <location filename="imgroupchatstore.cpp" line="1650"/>
        <source>[Image]</source>
        <translation>[Imagem]</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1750"/>
        <location filename="imgroupchatstore.cpp" line="1793"/>
        <location filename="imgroupchatstore.cpp" line="2641"/>
        <location filename="imgroupchatstore.cpp" line="2649"/>
        <location filename="imgroupchatstore.cpp" line="2696"/>
        <source>[File]</source>
        <translation>[Arquivo]</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1765"/>
        <location filename="imgroupchatstore.cpp" line="1809"/>
        <source>Local file does&apos;t exist and cannot be forwarded</source>
        <translation>Arquivo local não existe e não pode ser encaminhado</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2430"/>
        <source>Personal Business Card</source>
        <translation>Cartão pessoal</translation>
    </message>
    <message>
        <source>Personal ID:</source>
        <translation type="vanished">ID pessoal:</translation>
    </message>
    <message>
        <source>Tribe Business Card</source>
        <translation type="vanished">Cartão de visita do tribo</translation>
    </message>
    <message>
        <source>Tribe ID:</source>
        <translation type="vanished">ID da Tribo:</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1407"/>
        <location filename="imgroupchatstore.cpp" line="2261"/>
        <location filename="imgroupchatstore.cpp" line="2984"/>
        <source> joined the group</source>
        <translation>Juntou-se ao grupo</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1409"/>
        <location filename="imgroupchatstore.cpp" line="2263"/>
        <location filename="imgroupchatstore.cpp" line="2986"/>
        <source> to join the group</source>
        <translation>Para se juntar ao grupo</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2431"/>
        <source>ID:</source>
        <translation>ID:</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2444"/>
        <source>Group Business Card</source>
        <translation>Cartão do grupo</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2445"/>
        <source>Group ID:</source>
        <translation>ID do grupo:</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2551"/>
        <source>[Red Packet]</source>
        <translation>[Pacote vermelho]</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2577"/>
        <source>Cannot send file larger than 100M in size</source>
        <translation>Não é possível enviar arquivos com tamanho maior que 100 M</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2790"/>
        <location filename="imgroupchatstore.cpp" line="2798"/>
        <location filename="imgroupchatstore.cpp" line="2815"/>
        <source>[Announcement]</source>
        <translation>[Anúncio]</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2913"/>
        <source>Save as</source>
        <translation>Salvar como</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2990"/>
        <source>Received the file successfully!</source>
        <translation>Recebeu o arquivo com sucesso!</translation>
    </message>
</context>
<context>
    <name>IMGroupChatView</name>
    <message>
        <location filename="imgroupchatview.cpp" line="137"/>
        <source> Members %1</source>
        <translation>Membro tribal %1</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="356"/>
        <location filename="imgroupchatview.cpp" line="1751"/>
        <location filename="imgroupchatview.cpp" line="1758"/>
        <source>Copy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="359"/>
        <source>Cut</source>
        <translation>Cortar</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="362"/>
        <source>Paste</source>
        <translation>Colar</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="365"/>
        <source>Delete</source>
        <translation>Excluir</translation>
    </message>
    <message>
        <source>Tribe has been silenced...</source>
        <translation type="vanished">Tribo foi silenciada ...</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="933"/>
        <source>Cancel Administrator Privileges</source>
        <translation>Cancelar privilégios de administrador</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="939"/>
        <source>Set as Administrator</source>
        <translation>Definir como administrador</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="946"/>
        <source>View Profile</source>
        <translation>Ver Perfil</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="951"/>
        <source>Set Remark Name</source>
        <oldsource>Set Group Name</oldsource>
        <translation>Definir nome da observação</translation>
    </message>
    <message>
        <source>Remove Tribe Member</source>
        <translation type="vanished">Remover membro da tribo</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="788"/>
        <source>Group has been silenced...</source>
        <translation>Grupo foi silenciado</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="958"/>
        <source>Remove Group Member</source>
        <translation>Remover membro do grupo</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1014"/>
        <source>Please enter your alias:</source>
        <translation>Por favor, digite seu alias:</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1303"/>
        <source>Open</source>
        <translation>Abrir</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1304"/>
        <source>Image File</source>
        <translation>Arquivo de imagem</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1521"/>
        <source>Send</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1521"/>
        <source>All(*.*)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1652"/>
        <source>Empty Filename! Open Document Failed!</source>
        <translation>Nome de arquivo vazio! Documento aberto falhou!</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1701"/>
        <location filename="imgroupchatview.cpp" line="2109"/>
        <source>Notice</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1701"/>
        <source>Empty file name or file doesn&apos;t exist!</source>
        <translation>Nome de arquivo vazio ou arquivo não existe!</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1768"/>
        <source>Resend</source>
        <translation>Reenviar</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1778"/>
        <source>Reload</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1786"/>
        <source>Forward</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1797"/>
        <source>Clear</source>
        <translation>Claro</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="2109"/>
        <source>Fail to check the red packet!</source>
        <translation>Falha ao verificar o pacote vermelho!</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="2246"/>
        <source>@</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>IMGroupChatViewModel</name>
    <message>
        <location filename="imgroupchatviewmodel.cpp" line="206"/>
        <source>[Image]</source>
        <translation>[Imagem]</translation>
    </message>
</context>
<context>
    <name>IMPerChat</name>
    <message>
        <location filename="imperchat.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="92"/>
        <source>View the Detail</source>
        <translation>Visualizar informações</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="228"/>
        <source>Screen Cut</source>
        <translation>Corte de tela</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="256"/>
        <source>Font</source>
        <translation>Font</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="284"/>
        <source>Expressions</source>
        <translation>Expressão</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="312"/>
        <source>Vibration</source>
        <translation>Vibração</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="340"/>
        <source>Picture</source>
        <translation>Cenário</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="368"/>
        <source>File</source>
        <translation>Arquivo</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="396"/>
        <source>Secret Message</source>
        <translation>Mensagem Secreta</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="424"/>
        <source>Secret Picture</source>
        <translation>Imagem Secreta</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="452"/>
        <source>Secret File</source>
        <translation>Arquivo Secreto</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="480"/>
        <source>Announcement</source>
        <translation>Anúncio</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="536"/>
        <source>ETH Transfer</source>
        <translation>Transferência ETH</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="564"/>
        <source>BTC Transfer</source>
        <translation>Transferência BTC</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="592"/>
        <source>EOS Transfer</source>
        <translation>Transferência EOS</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="620"/>
        <source>Red Packet</source>
        <translation>Pacote vermelho</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="672"/>
        <source>Message Log</source>
        <translation>Log de mensagens</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="696"/>
        <source>Send</source>
        <translation>Mandar</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="699"/>
        <source>Alt+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="742"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;微软雅黑&apos;; font-size:12pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>Create Tribe</source>
        <translation type="vanished">Criar tribo</translation>
    </message>
    <message>
        <source>Add as a friend</source>
        <translation type="vanished">Adicionar como amigo</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="vanished">cópia de</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation type="vanished">Cortar</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="vanished">Colar</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Excluir</translation>
    </message>
    <message>
        <source>Power Exchange</source>
        <translation type="vanished">Troca de poder</translation>
    </message>
    <message>
        <source>ETH Tranfer</source>
        <translation type="vanished">Transferência ETH</translation>
    </message>
    <message>
        <source>BTC Tranfer</source>
        <translation type="vanished">Transferência BTC</translation>
    </message>
    <message>
        <source>EOS Tranfer</source>
        <translation type="vanished">Transferência EOS</translation>
    </message>
    <message>
        <source>Notice</source>
        <translation type="vanished">Aviso prévio</translation>
    </message>
    <message>
        <source>File larger than 100M cannot be sent</source>
        <translation type="vanished">Arquivo maior que 100M não pode ser enviado</translation>
    </message>
    <message>
        <source>[File]</source>
        <translation type="vanished">[Arquivo]</translation>
    </message>
    <message>
        <source>Empty file name or file doesn&apos;t exist!</source>
        <translation type="vanished">Nome de arquivo vazio ou arquivo não existe!</translation>
    </message>
    <message>
        <source>Local file does&apos;t exist，unable to forward</source>
        <translation type="vanished">Arquivo local não existe, incapaz de encaminhar</translation>
    </message>
    <message>
        <source>[Image]</source>
        <translation type="vanished">[Imagem]</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Abrir</translation>
    </message>
    <message>
        <source>Image File(*.bmp;*.jpeg;*.jpg;*.png;*.gif)</source>
        <translation type="vanished">Arquivo de Imagem (*.Bmp; *.Jpeg; *.Jpg; *.Png; *.Gif)</translation>
    </message>
    <message>
        <source>Best Wishes</source>
        <translation type="vanished">Muitas felicidades</translation>
    </message>
    <message>
        <source>Received file successfully!</source>
        <translation type="vanished">Arquivo recebido com sucesso!</translation>
    </message>
    <message>
        <source>The other party has successfully received your file</source>
        <translation type="vanished">A outra parte recebeu com sucesso seu arquivo</translation>
    </message>
    <message>
        <source>This file already exists, overwrite or not？</source>
        <translation type="vanished">Este arquivo já existe, sobrescrever ou não？</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation type="vanished">Salvar como</translation>
    </message>
    <message>
        <source>Receive file successfully!</source>
        <translation type="vanished">Receber arquivo com sucesso!</translation>
    </message>
    <message>
        <source>[Secret Message]</source>
        <translation type="vanished">[Mensagem Secreta]</translation>
    </message>
    <message>
        <source>[Secret Image]</source>
        <translation type="vanished">[Imagem Secreta]</translation>
    </message>
    <message>
        <source>[Secret File]</source>
        <translation type="vanished">[Arquivo Secreto]</translation>
    </message>
    <message>
        <source>[Red Packet]</source>
        <translation type="vanished">[Pacote vermelho]</translation>
    </message>
    <message>
        <source>Personal business card</source>
        <translation type="vanished">Cartão pessoal</translation>
    </message>
    <message>
        <source>Personal ID:</source>
        <translation type="vanished">ID pessoal:</translation>
    </message>
    <message>
        <source>Tribe business card</source>
        <translation type="vanished">Cartão de visita da tribo</translation>
    </message>
    <message>
        <source>Tribe ID:</source>
        <translation type="vanished">ID da Tribo:</translation>
    </message>
    <message>
        <source>Fail to check the red packet!</source>
        <translation type="vanished">Falha ao verificar o pacote vermelho!</translation>
    </message>
    <message>
        <source>Resend</source>
        <translation type="vanished">Reenviar</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation type="vanished">recarregar</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="vanished">frente</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="vanished">Claro</translation>
    </message>
    <message>
        <source>[Announcement]</source>
        <translation type="vanished">[Anúncio]</translation>
    </message>
    <message>
        <source>（Typing...）</source>
        <translation type="vanished">(Digitando...)</translation>
    </message>
</context>
<context>
    <name>IMPerChatStore</name>
    <message>
        <location filename="imperchatstore.cpp" line="696"/>
        <location filename="imperchatstore.cpp" line="1379"/>
        <source>Best Wishes</source>
        <translation>Arquivo está em uso, por favor, verifique primeiro</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="872"/>
        <source>The other party has successfully received your file</source>
        <translation>A outra parte recebeu com sucesso seu arquivo</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="923"/>
        <location filename="imperchatstore.cpp" line="1097"/>
        <location filename="imperchatstore.cpp" line="1917"/>
        <location filename="imperchatstore.cpp" line="2191"/>
        <source>Notice</source>
        <translation></translation>
    </message>
    <message>
        <source>This file already exists, overwrite or not？</source>
        <translation type="vanished">Este arquivo já existe, sobrescrever ou não？</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="923"/>
        <source>This file already exists, overwrite or not?</source>
        <translation>Este arquivo já existe, sobrescrever ou não？</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1039"/>
        <location filename="imperchatstore.cpp" line="1044"/>
        <location filename="imperchatstore.cpp" line="1057"/>
        <source>Receive file successfully!</source>
        <translation>Receber arquivo com sucesso!</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1097"/>
        <source>File is in use, please check it first!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1574"/>
        <source>Received file successfully!</source>
        <translation>Arquivo recebido com sucesso!</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1763"/>
        <location filename="imperchatstore.cpp" line="1792"/>
        <source>[Image]</source>
        <translation>[Imagem]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1900"/>
        <location filename="imperchatstore.cpp" line="2256"/>
        <location filename="imperchatstore.cpp" line="2318"/>
        <source>[File]</source>
        <translation>[Arquivo]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1917"/>
        <source>Local file does&apos;t exist，unable to forward</source>
        <translation>Arquivo local não existe, incapaz de encaminhar</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1991"/>
        <source>Personal business card</source>
        <translation>Cartão pessoal</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1992"/>
        <source>ID:</source>
        <translation>ID:</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2005"/>
        <source>Group business card</source>
        <translation>Cartão de visita do grupo</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2006"/>
        <source>Group ID:</source>
        <translation>ID do grupo:</translation>
    </message>
    <message>
        <source>Personal ID:</source>
        <translation type="vanished">ID pessoal:</translation>
    </message>
    <message>
        <source>Tribe business card</source>
        <translation type="vanished">Cartão de visita da tribo</translation>
    </message>
    <message>
        <source>Tribe ID:</source>
        <translation type="vanished">ID da Tribo:</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2162"/>
        <source>[Red Packet]</source>
        <translation>[Pacote vermelho]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2191"/>
        <source>File larger than 100M cannot be sent</source>
        <translation>Arquivo maior que 100M não pode ser enviado</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2363"/>
        <source>[Secret Message]</source>
        <translation>[Mensagem Secreta]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2395"/>
        <source>[Secret Image]</source>
        <translation>[Imagem Secreta]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2427"/>
        <source>[Secret File]</source>
        <translation>[Arquivo Secreto]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2508"/>
        <location filename="imperchatstore.cpp" line="2540"/>
        <source>[Announcement]</source>
        <translation>[Anúncio]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2601"/>
        <source>Save as</source>
        <translation>Salvar como</translation>
    </message>
</context>
<context>
    <name>IMPerChatView</name>
    <message>
        <source>Create Tribe</source>
        <translation type="vanished">Criar tribo</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="153"/>
        <source>Create Group</source>
        <translation>Criar grupo</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="164"/>
        <source>Add as a friend</source>
        <translation>Adicionar como amigo</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="221"/>
        <source>Power Exchange</source>
        <translation>Troca de poder</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="226"/>
        <source>ETH Tranfer</source>
        <translation>Transferência ETH</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="231"/>
        <source>BTC Tranfer</source>
        <translation>Transferência BTC</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="236"/>
        <source>EOS Tranfer</source>
        <translation>Transferência EOS</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="598"/>
        <location filename="imperchatview.cpp" line="1777"/>
        <location filename="imperchatview.cpp" line="1783"/>
        <source>Copy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="601"/>
        <source>Cut</source>
        <translation>Cortar</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="604"/>
        <source>Paste</source>
        <translation>Colar</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="607"/>
        <source>Delete</source>
        <translation>Excluir</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="751"/>
        <source>（Typing...）</source>
        <translation>(Digitando...)</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="953"/>
        <source>Open</source>
        <translation>Abrir</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="954"/>
        <source>Image File</source>
        <translation>Arquivo de imagem</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1030"/>
        <source>Send</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1030"/>
        <source>All(*.*)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1293"/>
        <location filename="imperchatview.cpp" line="1684"/>
        <source>Notice</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1293"/>
        <source>Empty file name or file doesn&apos;t exist!</source>
        <translation>Nome de arquivo vazio ou arquivo não existe!</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1325"/>
        <source>fileName为空!OpenDocument Failed!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1684"/>
        <source>Fail to check the red packet!</source>
        <translation>Falha ao verificar o pacote vermelho!</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1793"/>
        <source>Resend</source>
        <translation>Reenviar</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1801"/>
        <source>Reload</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1809"/>
        <source>Forward</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1819"/>
        <source>Clear</source>
        <translation>Claro</translation>
    </message>
</context>
<context>
    <name>IMPerChatViewModel</name>
    <message>
        <location filename="imperchatviewmodel.cpp" line="166"/>
        <source>[Image]</source>
        <translation>[Imagem]</translation>
    </message>
</context>
<context>
    <name>MessageListDispatcher</name>
    <message>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="160"/>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="171"/>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="197"/>
        <source>Notice</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="160"/>
        <source>Are you sure to dismiss this group?</source>
        <translation>Você tem certeza de dispensar este grupo?</translation>
    </message>
    <message>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="171"/>
        <source>Are you sure to quit group?</source>
        <translation>Tem certeza de sair do grupo?</translation>
    </message>
    <message>
        <source>Are you sure to dismiss this tribe?</source>
        <translation type="vanished">Você tem certeza de dispensar esta tribo?</translation>
    </message>
    <message>
        <source>Are you sure to quit tribe?</source>
        <translation type="vanished">Você tem certeza de sair da tribo?</translation>
    </message>
    <message>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="197"/>
        <source>Network request failed!</source>
        <translation>Pedido de rede falhou!</translation>
    </message>
</context>
<context>
    <name>MessageListStore</name>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="148"/>
        <location filename="MessageWidget/messageliststore.cpp" line="725"/>
        <source> joined the group</source>
        <oldsource> joined the tribe</oldsource>
        <translation>Juntou-se ao grupo</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="150"/>
        <location filename="MessageWidget/messageliststore.cpp" line="727"/>
        <source> invited </source>
        <translation>convidamos</translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished">para se juntar à tribo</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="156"/>
        <location filename="MessageWidget/messageliststore.cpp" line="558"/>
        <location filename="MessageWidget/messageliststore.cpp" line="733"/>
        <source>File received successfully </source>
        <translation>Arquivo recebido com sucesso</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="158"/>
        <location filename="MessageWidget/messageliststore.cpp" line="562"/>
        <location filename="MessageWidget/messageliststore.cpp" line="735"/>
        <source>The other party has successfully received your file </source>
        <translation>A outra parte recebeu com sucesso seu arquivo</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="195"/>
        <location filename="MessageWidget/messageliststore.cpp" line="198"/>
        <location filename="MessageWidget/messageliststore.cpp" line="537"/>
        <location filename="MessageWidget/messageliststore.cpp" line="785"/>
        <location filename="MessageWidget/messageliststore.cpp" line="788"/>
        <source>[Announcement]</source>
        <translation>[Anúncio]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="209"/>
        <location filename="MessageWidget/messageliststore.cpp" line="212"/>
        <location filename="MessageWidget/messageliststore.cpp" line="544"/>
        <location filename="MessageWidget/messageliststore.cpp" line="798"/>
        <location filename="MessageWidget/messageliststore.cpp" line="801"/>
        <source>[Share]</source>
        <translation>[Compartilhar]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="319"/>
        <source>View Details</source>
        <translation>Ver detalhes</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="323"/>
        <source>Close Chat</source>
        <translation>Fechar Chat</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="327"/>
        <source>Close All Chats</source>
        <translation>Fechar todos os chats</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="344"/>
        <source>Remove From Top</source>
        <translation>Remover do topo</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="352"/>
        <source>Set Message Top</source>
        <translation>Definir o topo da mensagem</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="360"/>
        <source>Cancle No-Disturbing</source>
        <translation>Cancelar sem perturbar</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="368"/>
        <source>Set No-Disturbing</source>
        <translation>Definir sem perturbação</translation>
    </message>
    <message>
        <source>Dismiss Tribe</source>
        <translation type="vanished">Dispensar a tribo</translation>
    </message>
    <message>
        <source>Quit Tribe</source>
        <translation type="vanished">Pare de tribo</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="150"/>
        <location filename="MessageWidget/messageliststore.cpp" line="727"/>
        <source> to join the group</source>
        <translation>Para se juntar ao grupo</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="386"/>
        <source>Dismiss Group</source>
        <translation>Dispensar Grupo</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="395"/>
        <source>Quit Group</source>
        <translation>Sair do grupo</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="490"/>
        <source>Yesterday</source>
        <translation>Ontem</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="509"/>
        <source>[Image]</source>
        <translation>[Imagem]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="511"/>
        <source>[Audio]</source>
        <translation>[Áudio]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="513"/>
        <source>[Video]</source>
        <translation>[Vídeo]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="515"/>
        <source>[File]</source>
        <translation>[Arquivo]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="517"/>
        <source>This type of message is not supported for now</source>
        <translation>Este tipo de mensagem não é suportado por enquanto</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="519"/>
        <source>[Tranfer]</source>
        <translation>[Transferir]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="521"/>
        <source>[Red Packet]</source>
        <translation>[Pacote vermelho]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="523"/>
        <source>[Secret Message]</source>
        <translation>[Mensagem Secreta]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="525"/>
        <source>[Secret Image]</source>
        <translation>[Imagem Secreta]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="527"/>
        <source>[Secret File]</source>
        <translation>[Arquivo Secreto]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="529"/>
        <source>[Location]</source>
        <translation>[Localização]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="531"/>
        <source>[Gravitational Waves]</source>
        <translation>[Ondas gravitacionais]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="627"/>
        <location filename="MessageWidget/messageliststore.cpp" line="631"/>
        <location filename="MessageWidget/messageliststore.cpp" line="877"/>
        <location filename="MessageWidget/messageliststore.cpp" line="896"/>
        <location filename="MessageWidget/messageliststore.cpp" line="983"/>
        <location filename="MessageWidget/messageliststore.cpp" line="987"/>
        <location filename="MessageWidget/messageliststore.cpp" line="1223"/>
        <source>&lt;font color=&apos;#f7931e&apos; &gt;[I was @]&lt;/font&gt;</source>
        <translation>&lt;font color=&apos;#f7931e&apos; &gt;[Eu fui @]&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="1730"/>
        <source>&lt;font color=&apos;#f7931e&apos; &gt;[Draft]&lt;/font&gt;</source>
        <translation>&lt;font color=&apos;#f7931e&apos; &gt;[Rascunho]&lt;/font&gt;</translation>
    </message>
</context>
<context>
    <name>MessageLog</name>
    <message>
        <location filename="messagelog/messagelog.ui" line="26"/>
        <source>Message Log</source>
        <translation>Log de mensagens</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="330"/>
        <source>Messsage Log</source>
        <translation>Log de mensagens</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="484"/>
        <source>Friends</source>
        <translation>Amigo</translation>
    </message>
    <message>
        <source>Tribe</source>
        <translation type="vanished">Tribo</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="524"/>
        <source>Group</source>
        <translation>Grupo</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="578"/>
        <source>🔎</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="598"/>
        <source>Search</source>
        <translation>Procurar</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="623"/>
        <source>Clear</source>
        <translation>Claro</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="697"/>
        <source>about:blank</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="811"/>
        <source>|&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="837"/>
        <source>&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="857"/>
        <source>&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="877"/>
        <source>&gt;|</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="45"/>
        <source>微软雅黑</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1068"/>
        <source>Best Wishes</source>
        <translation>Muitas felicidades</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1212"/>
        <location filename="messagelog/messagelog.cpp" line="1340"/>
        <location filename="messagelog/messagelog.cpp" line="1831"/>
        <location filename="messagelog/messagelog.cpp" line="1970"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1212"/>
        <source>Empty file name or file doesn&apos;t exist!</source>
        <translation>Nome de arquivo vazio ou arquivo não existe!</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1244"/>
        <source>fileName为空!OpenDocument Failed!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1333"/>
        <source>Sorry</source>
        <translation>Desculpa</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1333"/>
        <source>There are no qualified message records</source>
        <translation>Não há registros de mensagens qualificados</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1340"/>
        <source>Unable to search empty content</source>
        <translation>Não é possível pesquisar conteúdo vazio</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1831"/>
        <source>Failed to check the red packet!</source>
        <translation>Falha ao verificar o pacote vermelho!</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1970"/>
        <source>This file already exists, overwrite or not?</source>
        <translation>Este arquivo já existe, sobrescrever ou não？</translation>
    </message>
    <message>
        <source>This file already exists, overwrite or not？</source>
        <translation type="vanished">Este arquivo já existe, sobrescrever ou não？</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="2150"/>
        <source>Save as</source>
        <translation>Salvar como</translation>
    </message>
</context>
<context>
    <name>NoticeWidget</name>
    <message>
        <location filename="childWidget/noticewidget.ui" line="14"/>
        <source>NoticeWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.ui" line="111"/>
        <source>Announcement</source>
        <translation>Anúncio</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.ui" line="204"/>
        <source>Click to Choose Image</source>
        <translation>Clique para Escolher Imagem</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.ui" line="243"/>
        <source>Please enter the title of the web page</source>
        <translation>Por favor insira o título da página web</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.ui" line="271"/>
        <source>Please enter the link of the web page</source>
        <translation>Por favor insira o link da página web</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.ui" line="388"/>
        <source>Send</source>
        <translation>Mandar</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="29"/>
        <source>Please enter the title of the image</source>
        <translation>Por favor insira o título da imagem</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="30"/>
        <location filename="childWidget/noticewidget.cpp" line="35"/>
        <source>微软雅黑</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="77"/>
        <source>Open</source>
        <translation>Abrir</translation>
    </message>
    <message>
        <source>Image File(*.bmp;*.jpeg;*.jpg;*.png;*.gif)</source>
        <translation type="vanished">Arquivo de Imagem (*. Bmp; *. Jpeg; *. Jpg; *. Png; *. Gif)</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="78"/>
        <source>Image File</source>
        <translation>Arquivo de imagem</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="137"/>
        <location filename="childWidget/noticewidget.cpp" line="145"/>
        <location filename="childWidget/noticewidget.cpp" line="150"/>
        <location filename="childWidget/noticewidget.cpp" line="155"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="137"/>
        <source>Please Choose Image!</source>
        <translation>Por favor escolha a imagem!</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="145"/>
        <source>Please enter the title of the image！</source>
        <translation>Por favor insira o título da imagem！</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="150"/>
        <source>Please enter the title of web page!</source>
        <translation>Por favor insira o título da página web!</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="155"/>
        <source>Please enter the URL！</source>
        <translation>Por favor insira o URL！</translation>
    </message>
</context>
<context>
    <name>OpenLetterWidget</name>
    <message>
        <location filename="secretWidget/openletterwidget.ui" line="14"/>
        <source>OpenLetterWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.ui" line="78"/>
        <source>Secret Message</source>
        <translation>Mensagem Secreta</translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.ui" line="205"/>
        <source>Enter Password</source>
        <translation>Digite a senha</translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.ui" line="255"/>
        <source>OK</source>
        <translation>Está bem</translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.cpp" line="78"/>
        <location filename="secretWidget/openletterwidget.cpp" line="85"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.cpp" line="78"/>
        <source>The password cannot be empty!</source>
        <translation>A senha não pode estar vazia!</translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.cpp" line="85"/>
        <source>Incorrect password!</source>
        <translation>Senha incorreta!</translation>
    </message>
</context>
<context>
    <name>OpenPacketWidget</name>
    <message>
        <location filename="redPacketWidget/openPack.ui" line="32"/>
        <source>OpenPacket</source>
        <oldsource>OpenPacketWidget</oldsource>
        <translation>Pacote aberto</translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.ui" line="189"/>
        <source>我是头像</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.ui" line="228"/>
        <source>我是昵称</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.ui" line="267"/>
        <location filename="redPacketWidget/openPack.cpp" line="88"/>
        <source>A red packet was sent to you</source>
        <translation>Um pacote vermelho foi enviado para você</translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.ui" line="306"/>
        <source>Best Wishes</source>
        <translation>Muitas felicidades</translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.cpp" line="92"/>
        <source>Red racket with a identical amount</source>
        <translation>Raquete vermelha com uma quantidade idêntica</translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.cpp" line="96"/>
        <source>Red packet with a random amount</source>
        <translation>Pacote vermelho com uma quantidade aleatória</translation>
    </message>
</context>
<context>
    <name>PackUnitWidget</name>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="17"/>
        <source>PackUnitWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="63"/>
        <source>PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="91"/>
        <source>11.0000000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="110"/>
        <source>11.000000000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="130"/>
        <source>Available</source>
        <translation>acessível</translation>
    </message>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="146"/>
        <source>Frozen</source>
        <translation>Congelados</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="59"/>
        <location filename="MessageWidget/messageliststore.cpp" line="63"/>
        <source>&lt;font color=&apos;#f7931e&apos; &gt;[I was @]&lt;/font&gt;</source>
        <translation>&lt;font color=&apos;#f7931e&apos; &gt;[Eu fui @]&lt;/font&gt;</translation>
    </message>
</context>
<context>
    <name>QWebEngineViewDelegate</name>
    <message>
        <location filename="qwebengineviewdelegate.ui" line="16"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>RedPackDetail</name>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="32"/>
        <source>redPackDetail</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="100"/>
        <source>Records</source>
        <translation>Registros</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="138"/>
        <source>Details</source>
        <translation>Detalhes</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="248"/>
        <source>我是头像</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="302"/>
        <source>我是昵称</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="343"/>
        <source>我是红包备注</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="416"/>
        <source>我是获得的红包金额</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="438"/>
        <source>我是红包单位</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="495"/>
        <source>我是多少个红包</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="527"/>
        <source>我是已领取红包</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.cpp" line="74"/>
        <source> red packets </source>
        <translation>pacotes vermelhos</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.cpp" line="79"/>
        <source>taken </source>
        <translation>ocupado</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.cpp" line="103"/>
        <source>most lucky</source>
        <translation>mais sorte</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.cpp" line="113"/>
        <source>微软雅黑</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>RedPackHistory</name>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="32"/>
        <source>RedPackHistory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="79"/>
        <source>Red Packet History</source>
        <translation>História do Pacote Vermelho</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="190"/>
        <location filename="redPacketWidget/RedPackHistory.ui" line="428"/>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="184"/>
        <source>Received</source>
        <translation>Recebido</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="260"/>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="199"/>
        <source>Sent</source>
        <translation>Enviei</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="345"/>
        <source>我是头像</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="409"/>
        <source>我是昵称</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="495"/>
        <source>我是红包数量</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="517"/>
        <source>Packets</source>
        <translation>Pacotes</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="574"/>
        <source>Tokens received can be transfered into local wallet</source>
        <translation>Os tokens recebidos podem ser transferidos para a carteira local</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="84"/>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="134"/>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="175"/>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="190"/>
        <source>微软雅黑</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="185"/>
        <source>Tokens been deposited in your accounts now, which can be transfered into local wallet</source>
        <translation>Tokens foram depositados em suas contas agora, que podem ser transferidos para a carteira local</translation>
    </message>
</context>
<context>
    <name>SearchList</name>
    <message>
        <location filename="SearchWidget/searchlist.ui" line="16"/>
        <source>SearchList</source>
        <translation></translation>
    </message>
    <message>
        <location filename="SearchWidget/searchlist.cpp" line="120"/>
        <location filename="SearchWidget/searchlist.cpp" line="147"/>
        <location filename="SearchWidget/searchlist.cpp" line="150"/>
        <location filename="SearchWidget/searchlist.cpp" line="167"/>
        <location filename="SearchWidget/searchlist.cpp" line="228"/>
        <location filename="SearchWidget/searchlist.cpp" line="248"/>
        <source>Friends</source>
        <translation>Amigos</translation>
    </message>
    <message>
        <location filename="SearchWidget/searchlist.cpp" line="129"/>
        <location filename="SearchWidget/searchlist.cpp" line="185"/>
        <location filename="SearchWidget/searchlist.cpp" line="188"/>
        <location filename="SearchWidget/searchlist.cpp" line="201"/>
        <source>Group</source>
        <translation>Grupo</translation>
    </message>
    <message>
        <source>Tribe</source>
        <translation type="vanished">Tribo</translation>
    </message>
    <message>
        <location filename="SearchWidget/searchlist.cpp" line="225"/>
        <source>other friends</source>
        <translation>outros amigos</translation>
    </message>
</context>
<context>
    <name>SecretFileWidget</name>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="14"/>
        <source>SecretFileWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="105"/>
        <source>Secret File</source>
        <translation>Arquivo Secreto</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="228"/>
        <source>Enter Password</source>
        <translation>Digite a senha</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="271"/>
        <source>Click &quot;add&quot; button to add secret file, double click file to cancel</source>
        <translation>Clique no botão &quot;Adicionar&quot; para adicionar o arquivo secreto, clique duas vezes no arquivo para cancelar</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="332"/>
        <source>Add</source>
        <translation>Adicionar</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="357"/>
        <source>Send</source>
        <translation>Mandar</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="91"/>
        <source>Open File</source>
        <translation>Abrir arquivo</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="91"/>
        <source>File (*.*)</source>
        <translation>Arquivo (*.*)</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="147"/>
        <location filename="secretWidget/secretfilewidget.cpp" line="151"/>
        <location filename="secretWidget/secretfilewidget.cpp" line="173"/>
        <location filename="secretWidget/secretfilewidget.cpp" line="211"/>
        <location filename="secretWidget/secretfilewidget.cpp" line="247"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="147"/>
        <source>No File Selected!</source>
        <translation>Nenhum arquivo selecionado!</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="151"/>
        <source>Please Enter The Password!</source>
        <translation>Por favor insira a senha!</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="162"/>
        <source>Sending……</source>
        <translation>Enviando ...</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="173"/>
        <source>File does not exist!</source>
        <translation>Arquivo não existe!</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="211"/>
        <source>Encryption Failed!</source>
        <translation>Criptografia falhou!</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="247"/>
        <source>Failed to upload secret ile!</source>
        <translation>Falha ao carregar o arquivo secreto!</translation>
    </message>
</context>
<context>
    <name>SecretImageWidget</name>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="14"/>
        <source>SecretImageWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="93"/>
        <source>Secret Image</source>
        <translation>Imagem secreta</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="210"/>
        <source>Enter Password</source>
        <translation>Digite a senha</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="343"/>
        <source>Click &quot;add&quot; button to add secret image, double click file to cancel</source>
        <translation>Clique no botão &quot;Adicionar&quot; para adicionar imagem secreta, clique duas vezes no arquivo para cancelar</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="404"/>
        <source>Add</source>
        <translation>Adicionar</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="429"/>
        <source>Send</source>
        <translation>Mandar</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="91"/>
        <source>Open Image</source>
        <translation>Abrir imagem</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="91"/>
        <source>Image File(*.bmp;*.jpeg;*.jpg;*.png)</source>
        <translation>Arquivo de Imagem (*. Bmp; *. Jpeg; *. Jpg; *. Png)</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="110"/>
        <location filename="secretWidget/secretimagewidget.cpp" line="114"/>
        <location filename="secretWidget/secretimagewidget.cpp" line="136"/>
        <location filename="secretWidget/secretimagewidget.cpp" line="172"/>
        <location filename="secretWidget/secretimagewidget.cpp" line="208"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="110"/>
        <source>No Image Selected! </source>
        <translation>Nenhuma imagem selecionada!</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="114"/>
        <source>Please Enter The Password!</source>
        <translation>Por favor insira a senha!</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="125"/>
        <source>Sending……</source>
        <translation>Enviando ...</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="136"/>
        <source>Image does not exist!</source>
        <translation>A imagem não existe!</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="172"/>
        <source>Encryption Failed!</source>
        <translation>Criptografia falhou!</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="208"/>
        <source>Failed to upload secret file!</source>
        <translation>Falha ao carregar o arquivo secreto!</translation>
    </message>
</context>
<context>
    <name>SecretLetterWidget</name>
    <message>
        <location filename="secretWidget/secretletterwidget.ui" line="14"/>
        <source>SecretLetterWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.ui" line="120"/>
        <source>Secret Message</source>
        <translation>Mensagem Secreta</translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.ui" line="237"/>
        <source>Enter Password</source>
        <translation>Digite a senha</translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.ui" line="259"/>
        <source>Enter Message</source>
        <translation>Digite a mensagem</translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.ui" line="284"/>
        <source>Send</source>
        <translation>Mandar</translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.cpp" line="86"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.cpp" line="86"/>
        <source>The password or message cannot be empty!</source>
        <translation>A senha ou mensagem não pode estar vazia!</translation>
    </message>
</context>
<context>
    <name>TransAccWidget</name>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="20"/>
        <source>TransAccWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="71"/>
        <location filename="redPacketWidget/TransAccWidget.ui" line="200"/>
        <location filename="redPacketWidget/TransAccWidget.ui" line="482"/>
        <source>PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="87"/>
        <source>Transfer</source>
        <translation>Transferir</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="168"/>
        <source>Escrow Balance</source>
        <translation>Saldo de Custódia</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="253"/>
        <source>Address</source>
        <translation>Endereço</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="279"/>
        <source>Hosted Address</source>
        <translation>Endereço Hospedado</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="307"/>
        <source>Amount</source>
        <translation>Montante</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="338"/>
        <source>Enter Amount</source>
        <translation>Digite Montante</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="368"/>
        <source>Payment Address</source>
        <translation>Endereço de pagamento</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="443"/>
        <source>Cost of Miners</source>
        <translation>Custo dos Mineiros</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="469"/>
        <location filename="redPacketWidget/TransAccWidget.ui" line="547"/>
        <source>0.0004</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="573"/>
        <source>0.006</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="624"/>
        <source>OK</source>
        <translation>Está bem</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="32"/>
        <source>Wallet</source>
        <translation>Carteira</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="52"/>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="58"/>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="63"/>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="77"/>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="82"/>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="92"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="52"/>
        <source>Payment Address Cannot be Empty!</source>
        <translation>O endereço de pagamento não pode estar vazio!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="58"/>
        <source>Amount of Transfer Cannot be Empty!</source>
        <translation>Quantidade de transferência não pode estar vazia!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="63"/>
        <source>The Amount of Tranfer is Greater Than</source>
        <translation>A quantidade de transferência é maior que</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="63"/>
        <source>Balance</source>
        <translation>Equilibrar</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="69"/>
        <source>Please Enter Your Login Password:</source>
        <translation>Por favor digite sua senha de login:</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="77"/>
        <source>Failed to Verify the Login Password!</source>
        <translation>Falha ao verificar a senha de login!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="82"/>
        <source>Login Password Cannot be Empty!</source>
        <translation>Senha</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="92"/>
        <source>Incorrect Password!</source>
        <translation>Senha incorreta!</translation>
    </message>
</context>
<context>
    <name>TransmitMessageWidget</name>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="14"/>
        <source>TransmitMessage</source>
        <oldsource>TransmitMessageWidget</oldsource>
        <translation>Transmitir Mensagem</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="155"/>
        <source>Send to</source>
        <translation>Mandar</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="230"/>
        <source>Search</source>
        <translation>Procurar</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="295"/>
        <source>Recently</source>
        <translation>Recentemente</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="374"/>
        <location filename="childWidget/transmitmessagewidget.cpp" line="494"/>
        <location filename="childWidget/transmitmessagewidget.cpp" line="497"/>
        <source>Friends</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="408"/>
        <location filename="childWidget/transmitmessagewidget.cpp" line="529"/>
        <location filename="childWidget/transmitmessagewidget.cpp" line="532"/>
        <source>Groups</source>
        <translation>Grupos</translation>
    </message>
    <message>
        <source>Tribes</source>
        <translation type="vanished">Tribos</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="513"/>
        <source>OK</source>
        <translation>Está bem</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="544"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.cpp" line="390"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.cpp" line="390"/>
        <source>Please choose a friend or a group</source>
        <translation>Por favor escolha um amigo ou um grupo</translation>
    </message>
    <message>
        <source>Please choose a friend or a tribe</source>
        <translation type="vanished">Por favor, escolha um amigo ou uma tribo</translation>
    </message>
</context>
</TS>
