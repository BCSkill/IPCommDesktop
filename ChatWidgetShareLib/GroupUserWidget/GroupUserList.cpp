﻿#include "GroupUserList.h"
#include <QQmlContext>
#include <QQuickItem>
#include "MessageWidget/HeadProvider.h"
#include "immainwidget.h"
#include <QGuiApplication>

extern QString gThemeStyle;

GroupUserList::GroupUserList(QWidget *parent)
	: QQuickWidget(parent), m_pGroupUserListModel(new GroupUserListModel())
{
	this->setResizeMode(QQuickWidget::SizeRootObjectToView);

	HeadProvider *imageProvider = new HeadProvider(QQmlImageProviderBase::Image);
	this->engine()->addImageProvider(QLatin1String("HeadProvider"), imageProvider);

	this->rootContext()->setContextProperty("groupUserList", this);
	this->rootContext()->setContextProperty("groupUserListModel", m_pGroupUserListModel);
	this->setSource(QUrl("qrc:/GroupUserList/Resources/GroupUserWidget/GroupUserList.qml"));

	//切换主题样式
	if (gThemeStyle == "White")
	{
		this->changeStyle("dayStyle");
	}
	else if (gThemeStyle == "Blue")
	{
		this->changeStyle("nightStyle");
	}

	QString strColor = this->rootObject()->property("clearColor").toString();
	this->setClearColor(QColor(strColor));

	if (IMMainWidget::self())
	{
		connect(IMMainWidget::self(), SIGNAL(sigUpdateBuddyHeaderImagePath(int, QString)), this, SLOT(slotUpdateBuddyHeaderImagePath(int, QString)));
		//connect(IMMainWidget::self(), SIGNAL(sigUpdateBuddyNickName(int, QString)), this, SLOT(slotUpdateBuddyNickName(int, QString)));
	}
}


GroupUserList::~GroupUserList()
{
	if (m_pGroupUserListModel)
	{
		delete m_pGroupUserListModel;
		m_pGroupUserListModel = NULL;
	}

}

void GroupUserList::changeStyle(QString styleName)
{
	QObject* rootItem = rootObject();
	if (rootItem)
	{
		QVariant param = styleName;
		QMetaObject::invokeMethod(rootItem, "changeStyle", Q_ARG(QVariant, param));
	}
}

void GroupUserList::mouseReleaseEvent(QMouseEvent *event)
{
#ifdef Q_OS_MAC
	//MAC下触摸板经常拖动偶尔导致无法正常点击，所以发射信号使其正常
	if (event->button() == Qt::LeftButton)
	{
		emit qGuiApp->applicationStateChanged(Qt::ApplicationInactive);
		emit qGuiApp->applicationStateChanged(Qt::ApplicationActive);
	}
#endif

	QQuickWidget::mouseReleaseEvent(event);
}


void GroupUserList::clear()
{
	m_pGroupUserListModel->clear();
}

int GroupUserList::count()
{
	return m_pGroupUserListModel->rowCount();
}

QModelIndex GroupUserList::item(int i)
{
	return m_pGroupUserListModel->index(i);
}

QVariant GroupUserList::data(QModelIndex index, GroupUserListModel::GroupUserListItemRoles roleName)
{
	return m_pGroupUserListModel->data(index, roleName);
}

void GroupUserList::setData(const QModelIndex &index, const QVariant &value, GroupUserListModel::GroupUserListItemRoles roleName)
{
	m_pGroupUserListModel->setData(index, value, roleName);
}

void GroupUserList::takeItem(int row)
{
	m_pGroupUserListModel->takeItem(row);
}

void GroupUserList::insertItem(QString strUserID, QString strPicPath, QString strNickName,QString adminPicPath,bool bHasNote)
{
	//GroupUserItem*  item = new GroupUserItem(strUserID, strPicPath, strNickName, adminPicPath);
	GroupUserItem item(strUserID, strPicPath, strNickName, adminPicPath, bHasNote);

	m_pGroupUserListModel->pushBack(&item);
}

QModelIndex GroupUserList::currentItem()
{
	QObject* listViewMessageList = this->rootObject()->findChild<QObject*>("listViewGroupUser");
	int currentIndex = listViewMessageList->property("currentIndex").toInt();

	return m_pGroupUserListModel->index(currentIndex);
}


bool GroupUserList::IsValidImage(QString imgPath)
{
	QFile file(imgPath);
	if (file.open(QIODevice::ReadOnly))
	{
		QByteArray byteArray = file.readAll();
		file.close();
		QImage image = QImage::fromData(byteArray);

		if (image.isNull())
		{
			return false;
		}
	}
	else
	{
		return false;
	}

	return true;
}

void GroupUserList::slotUpdateBuddyHeaderImagePath(int ID, QString path)
{
	for (int i = 0; i < m_pGroupUserListModel->rowCount(); i++)
	{
		QString userId = m_pGroupUserListModel->data(m_pGroupUserListModel->index(i), GroupUserListModel::UserIdRole).toString();
		if (userId == QString::number(ID))
		{
			QString headUrl = "";
			if (IsValidImage(path))
			{
				headUrl = QStringLiteral("image://HeadProvider/") + path;
			}
			else
			{
				headUrl = QStringLiteral("image://HeadProvider/:/PerChat/Resources/person/temp.png");
			}

			m_pGroupUserListModel->setData(m_pGroupUserListModel->index(i), headUrl, GroupUserListModel::HeadUrlRole);
			break;
		}

	}
}