#include "GroupUserListModel.h"
#include <QDateTime>





GroupUserItem::GroupUserItem( const QString &userId, const QString &headUrl, const QString &nickName,const QString &adminPicPath, const bool &bHasNote)
	: m_userId(userId), m_headUrl(headUrl), m_nickName(nickName),m_adminUrl(adminPicPath),m_bHasNote(bHasNote)
{
}


QString GroupUserItem::userId() const
{
	return m_userId;
}

QString GroupUserItem::headUrl() const
{
	return m_headUrl;
}

QString GroupUserItem::nickName() const
{
	return m_nickName;
}

QString GroupUserItem::adminUrl() const
{
	return m_adminUrl;
}
bool GroupUserItem::hasNote() const
{
	return m_bHasNote;
}





void GroupUserItem::setUserId(QVariant val)
{
	m_userId = val.toString();
}

void GroupUserItem::setHeadUrl(QVariant val)
{
	m_headUrl = val.toString();
}

void GroupUserItem::setNickName(QVariant val)
{
	m_nickName = val.toString();
}

void GroupUserItem::setAdminUrl(QVariant val)
{
	m_adminUrl = val.toString();
}

void GroupUserItem::setHasNote(QVariant val)
{
	m_bHasNote = val.toBool();
}


/////////////////////////////////////////////////////////////

GroupUserListModel::GroupUserListModel(QObject *parent)
    : QAbstractListModel(parent)
{
	
}

void GroupUserListModel::pushFront(const GroupUserItem *item)
{
	beginInsertRows(QModelIndex(), 0, 0);
	m_items.push_front(*item);
	endInsertRows();
}

void GroupUserListModel::pushBack(const GroupUserItem *item)
{
	beginInsertRows(QModelIndex(), rowCount(), rowCount());
	m_items.push_back(*item);
	endInsertRows();
}

int GroupUserListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    // FIXME: Implement me!
	return m_items.count();
}

QVariant GroupUserListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

	if (index.row() < 0 || index.row() >= m_items.count())
		return QVariant();

	const GroupUserItem &item = m_items[index.row()];

	if (role == UserIdRole)
	{
		return item.userId();
	}
	else if (role == HeadUrlRole)
	{
		return item.headUrl();
	}
	else if (role == NickNameRole)
	{
		return item.nickName();
	}
	else if (role == AdminUrlRole)
	{
		return item.adminUrl();
	}
	else if (role == HasNoteRole)
	{
		return item.hasNote();
	}
	

	return QVariant();
}


bool GroupUserListModel::setData(const QModelIndex &index, const QVariant &value, int role/* = Qt::EditRole*/)
{
	if (index.row() < 0 || index.row() >= m_items.count())
		return false;

	GroupUserItem &item = m_items[index.row()];
	if (role == UserIdRole)
	{
		item.setUserId(value);
	}
	else if (role == HeadUrlRole)
	{
		item.setHeadUrl(value.toString() + "?" + QString::number(QDateTime::currentDateTime().toMSecsSinceEpoch()));
	}
	else if (role == NickNameRole)
	{
		item.setNickName(value);
	}
	else if (role == AdminUrlRole)
	{
		item.setAdminUrl(value);
	}
	else if (role == HasNoteRole)
	{
		item.setHasNote(value);
	}

	emit dataChanged(index, index);
	return true;
}

//![0]
QHash<int, QByteArray> GroupUserListModel::roleNames() const {
	QHash<int, QByteArray> roles;
	roles[UserIdRole] = "userId";
	roles[HeadUrlRole] = "headUrl";
	roles[NickNameRole] = "nickName";
	roles[AdminUrlRole] = "adminUrl";
	return roles;
}

void GroupUserListModel::clear()
{
	if (m_items.count() > 0)
	{
		beginRemoveRows(QModelIndex(), 0, m_items.count() - 1);
		m_items.clear();
		endRemoveRows();
	}
	
}

void GroupUserListModel::takeItem(int row)
{
	beginRemoveRows(QModelIndex(), row, row);
	m_items.removeAt(row);
	endRemoveRows();
}