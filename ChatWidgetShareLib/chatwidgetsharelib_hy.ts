<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hy_AM">
<context>
    <name>AtWidget</name>
    <message>
        <location filename="SearchWidget/atwidget.ui" line="14"/>
        <source>AtWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="SearchWidget/atwidget.cpp" line="248"/>
        <source>@</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ChatDataManager</name>
    <message>
        <source> joined the tribe</source>
        <translation type="vanished"> միացավ ցեղին</translation>
    </message>
    <message>
        <source> invited </source>
        <translation type="vanished"> Հրավերը </translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished"> միանալ ցեղին</translation>
    </message>
    <message>
        <source>File received successfully </source>
        <translation type="vanished">Ֆայլը Հաջող բեռնել </translation>
    </message>
    <message>
        <source>The other party has successfully received your file </source>
        <translation type="vanished">Մյուս կողմը հաջողությամբ ստացել է ձեր ֆայլը </translation>
    </message>
    <message>
        <source>[Announcement]</source>
        <translation type="vanished">[Հայտարարություն]</translation>
    </message>
    <message>
        <source>[Share]</source>
        <translation type="vanished">[Բաժնետոմս]</translation>
    </message>
    <message>
        <source>&lt;font color=&apos;#f7931e&apos; &gt;[I was @]&lt;/font&gt;</source>
        <translation type="vanished">&lt;font color=&apos;#f7931e&apos; &gt;[Ինչ-որ մեկը @ ինձ]&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="chatdatamanager.cpp" line="760"/>
        <location filename="chatdatamanager.cpp" line="824"/>
        <location filename="chatdatamanager.cpp" line="855"/>
        <source>Warning</source>
        <translation>Ուշադրություն</translation>
    </message>
    <message>
        <location filename="chatdatamanager.cpp" line="760"/>
        <location filename="chatdatamanager.cpp" line="824"/>
        <source>There are files being transferred，contuinue to close ?</source>
        <translation>Ներբեռնման խնդիրն աշխատում է, շարունակում է փակել？</translation>
    </message>
    <message>
        <location filename="chatdatamanager.cpp" line="855"/>
        <source>There are files being transferred，contuinue to quit ?</source>
        <translation>Ներբեռնման խնդիրն աշխատում է, շարունակեք դուրս գալ？</translation>
    </message>
    <message>
        <source>Download task is running，contuinue to close ?</source>
        <translation type="vanished">Ներբեռնման խնդիրն աշխատում է, շարունակում է փակել:</translation>
    </message>
    <message>
        <source>Download task is running，contuinue to Quit ?</source>
        <translation type="vanished">Ներբեռնման խնդիրն աշխատում է, շարունակեք դուրս գալ:</translation>
    </message>
    <message>
        <location filename="chatdatamanager.cpp" line="1319"/>
        <source>The other party has successfully received your file</source>
        <translation>Մյուս կողմը հաջողությամբ ստացել է ձեր ֆայլը</translation>
    </message>
</context>
<context>
    <name>ChatWidget</name>
    <message>
        <location filename="chatwidget.ui" line="14"/>
        <source>ChatWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="chatwidget.ui" line="117"/>
        <location filename="chatwidget.ui" line="162"/>
        <location filename="chatwidget.ui" line="190"/>
        <source>Search</source>
        <translation>Որոնում</translation>
    </message>
    <message>
        <location filename="chatwidget.ui" line="215"/>
        <source>View device list</source>
        <translation>Դիտեք սարքի ցանկը</translation>
    </message>
    <message>
        <location filename="chatwidget.ui" line="272"/>
        <source>当前账户在其他1台设备登录</source>
        <translation>Ընթացիկ հաշիվը մուտք է գործում մեկ այլ սարքի վրա</translation>
    </message>
    <message>
        <location filename="chatwidget.cpp" line="149"/>
        <source>Account logged in on other </source>
        <translation>Հաշիվը մուտք է գործում այլ </translation>
    </message>
    <message>
        <location filename="chatwidget.cpp" line="149"/>
        <source> device(s)</source>
        <translation>սարքը (ներ)</translation>
    </message>
</context>
<context>
    <name>ChooseUnitWidget</name>
    <message>
        <location filename="redPacketWidget/ChooseUnitWidget.ui" line="26"/>
        <source>ChooseUnitWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/ChooseUnitWidget.ui" line="77"/>
        <source>Choose red packet token</source>
        <translation>Ընտրեք նվեր գումար Արժույթը</translation>
    </message>
    <message>
        <location filename="redPacketWidget/ChooseUnitWidget.ui" line="195"/>
        <source>OK</source>
        <translation>Լավ</translation>
    </message>
</context>
<context>
    <name>EnterPasswordWidget</name>
    <message>
        <location filename="secretWidget/enterpasswordwidget.ui" line="14"/>
        <source>EnterPasswordWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.ui" line="82"/>
        <source>Please enter password of the base:</source>
        <translation>Խնդրում ենք մուտքագրել ձեր գաղտնաբառը:</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.ui" line="101"/>
        <source>Base password is used to decrypt the password of the message</source>
        <translation>հաղորդագրության գաղտնաբառի վերծանման համար</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.ui" line="126"/>
        <source>Please enter the base password</source>
        <translation>Գաղտնաբառ</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.ui" line="169"/>
        <source>OK</source>
        <translation>Հաստատեք</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="64"/>
        <source>Please enter the password</source>
        <translation>Մուտքագրեք գաղտնաբառը</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="65"/>
        <source>Enter the password to view the content</source>
        <translation>Մուտքագրեք ձեր մեկնաբանությունը</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="144"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="149"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="159"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="171"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="178"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="202"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="209"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="242"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="251"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="319"/>
        <source>Notice</source>
        <translation>Ծանուցում</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="144"/>
        <source>Decryption failure！Error code 113</source>
        <translation>Աղքատության չկատարումը: Error code 113</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="149"/>
        <source>Password cannot be empty！</source>
        <translation>Գաղտնաբառը չի կարող դատարկ լինել!</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="159"/>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="251"/>
        <source>Incorrect password！</source>
        <translation>Սխալ գաղտնաբառ!</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="171"/>
        <source>Decryption failure！Error code 137</source>
        <translation>Decryption ձախողումը: Error code 137</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="178"/>
        <source>Decryption failure！Error code 144</source>
        <translation>Decryption ձախողումը: Սխալ կոդը 144</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="202"/>
        <source>Please observe the surrounding environment before viewing the password to avoid leaking your password.
The password is only displayed once, please be sure to save the password</source>
        <translation>Խնդրում ենք դիտարկել շրջակա միջավայրը, նախքան դիտելու գաղտնաբառը, ձեր գաղտնաբառը բաց թողնելուց խուսափելու համար:
Գաղտնաբառը միայն մեկ անգամ ցուցադրվում է, համոզվեք, որ փրկեք գաղտնաբառը</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="209"/>
        <source>The password cannot be empty！</source>
        <translation>Գաղտնաբառը չի կարող դատարկ լինել!</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="242"/>
        <source>The secret picture/file is empty，please download again！</source>
        <translation>Գաղտնի պատկերը / ֆայլը դատարկ է, խնդրում ենք կրկին բեռնել!</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="319"/>
        <source>Fail to download the secret picture/file!</source>
        <translation>Չհաջողվեց բեռնել գաղտնի նկարը / ֆայլը!</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="329"/>
        <source>The password is:&quot;</source>
        <translation>Գաղտնաբառը `&quot;</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="329"/>
        <source>&quot;,Please remember the password, as the system will not save the password</source>
        <translation>&quot;Խնդրում ենք հիշել գաղտնաբառը, քանի որ համակարգը չի փրկի գաղտնաբառը</translation>
    </message>
    <message>
        <location filename="secretWidget/enterpasswordwidget.cpp" line="332"/>
        <source>View  password</source>
        <translation>Դիտել գաղտնաբառը</translation>
    </message>
</context>
<context>
    <name>ExpressWidget</name>
    <message>
        <location filename="childWidget/expresswidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="108"/>
        <source>[smile]</source>
        <translation>[ժպտում]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="109"/>
        <source>[twitch mouth]</source>
        <translation>[բերան բերան]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="110"/>
        <source>[salivate]</source>
        <translation>[ագահ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="111"/>
        <source>[staring blankly]</source>
        <translation>[դատարկ նայելով]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="112"/>
        <source>[complacent]</source>
        <translation>[համառ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="113"/>
        <source>[shy]</source>
        <translation>[ամաչկոտ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="114"/>
        <source>[shut up]</source>
        <translation>[լռիր]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="115"/>
        <source>[sleep]</source>
        <translation>[քնել]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="116"/>
        <source>[cry]</source>
        <translation>[լաց]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="117"/>
        <source>[awkward]</source>
        <translation>[անհարմար]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="118"/>
        <source>[angry]</source>
        <translation>[զայրացած]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="119"/>
        <source>[naughty]</source>
        <translation>[չարաճճի]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="120"/>
        <source>[grimace]</source>
        <translation>[Գրին]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="121"/>
        <source>[suprised]</source>
        <translation>[զարմացած]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="122"/>
        <source>[sad]</source>
        <translation>[տխուր]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="123"/>
        <source>[cool]</source>
        <translation>[զով]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="124"/>
        <source>[cold sweat]</source>
        <translation>[սառը քրտինք]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="125"/>
        <source>[crazy]</source>
        <translation>[խենթ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="126"/>
        <source>[spit]</source>
        <translation>[թքելը]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="127"/>
        <source>[titter]</source>
        <translation>[Քմծիծաղ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="128"/>
        <source>[supercilious look]</source>
        <translation>[գերազանց տեսք]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="129"/>
        <source>[cute]</source>
        <translation>[հաճելի]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="130"/>
        <source>[arrogance]</source>
        <translation>[ամբարտավանություն]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="131"/>
        <source>[hungry]</source>
        <translation>[սոված]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="132"/>
        <source>[sleepy]</source>
        <translation>[քնկոտ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="133"/>
        <source>[terrified]</source>
        <translation>[սարսափած]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="134"/>
        <source>[sweat]</source>
        <translation>[քրտինքը]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="135"/>
        <source>[smile fatuously]</source>
        <translation>[ժպտում fatuously]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="136"/>
        <source>[soldier]</source>
        <translation>[զինվոր]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="137"/>
        <source>[strive]</source>
        <translation>[ձգտում]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="138"/>
        <source>[doubting]</source>
        <translation>[կասկածներ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="139"/>
        <source>[hash]</source>
        <translation>[սուս մնա]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="140"/>
        <source>[dizzy]</source>
        <translation>[գլխապտույտ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="141"/>
        <source>[pig&apos;s head]</source>
        <translation>[խոզերի գլուխը]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="142"/>
        <source>[skeleton]</source>
        <translation>[կմախք]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="143"/>
        <source>[unlucky]</source>
        <translation>[անհաջող]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="144"/>
        <source>[knock]</source>
        <translation>[թակել]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="145"/>
        <source>[good bye]</source>
        <translation>[լավ բայ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="146"/>
        <source>[clap hands]</source>
        <translation>[կլեպ ձեռքեր]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="147"/>
        <source>[pick nose]</source>
        <translation>[ընտրեք քիթ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="148"/>
        <source>[embarassed]</source>
        <translation>[հուզված]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="149"/>
        <source>[snicker]</source>
        <translation>[Ծիծաղ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="150"/>
        <source>[left hem]</source>
        <translation>[ձախ հեմ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="151"/>
        <source>[right hem]</source>
        <translation>[աջ հեմ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="152"/>
        <source>[yawn]</source>
        <translation>[հորանջել]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="153"/>
        <source>[despise]</source>
        <translation>[արհամարհանք]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="154"/>
        <source>[grievance]</source>
        <translation>[բողոք]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="155"/>
        <source>[about to weep]</source>
        <translation>[լացից]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="156"/>
        <source>[insidious]</source>
        <translation>[նողկալի]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="157"/>
        <source>[kiss]</source>
        <translation>[համբույր]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="158"/>
        <source>[scared]</source>
        <translation>[վախեցած]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="159"/>
        <source>[pitiful]</source>
        <translation>[մռայլ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="160"/>
        <source>[kitchen knife]</source>
        <translation>[խոհանոցային դանակ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="161"/>
        <source>[watermelon]</source>
        <translation>[ձմերուկ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="162"/>
        <source>[beer]</source>
        <translation>[Գարեջուր]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="163"/>
        <source>[ping pong]</source>
        <translation>[սեղանի թենիս]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="164"/>
        <source>[coffee]</source>
        <translation>[սուրճ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="165"/>
        <source>[rice]</source>
        <translation>[բրինձ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="166"/>
        <source>[rose]</source>
        <translation>[վարդը]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="167"/>
        <source>[withered]</source>
        <translation>[հալածված]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="168"/>
        <source>[love]</source>
        <translation>[Սեր]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="169"/>
        <source>[loving heart]</source>
        <translation>[սիրառատ սիրտ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="170"/>
        <source>[heart break]</source>
        <translation>[սրտի ընդմիջում]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="171"/>
        <source>[cake]</source>
        <translation>[թխվածք]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="172"/>
        <source>[flash]</source>
        <translation>[կայծակ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="173"/>
        <source>[bomb]</source>
        <translation>[ռումբ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="174"/>
        <source>[knife]</source>
        <translation>[դանակ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="175"/>
        <source>[beetles]</source>
        <translation>[բզեզներ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="176"/>
        <source>[shit]</source>
        <translation>[բլիթ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="177"/>
        <source>[moon]</source>
        <translation>[լուսինը]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="178"/>
        <source>[sun]</source>
        <translation>[արեւը]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="179"/>
        <source>[gift]</source>
        <translation>[նվեր]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="180"/>
        <source>[embrace]</source>
        <translation>[ընդունում]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="181"/>
        <source>[strong]</source>
        <translation>[ուժեղ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="182"/>
        <source>[weak]</source>
        <translation>[թույլ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="183"/>
        <source>[shake hands]</source>
        <translation>[ձեռք սեղմել]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="184"/>
        <source>[hold fist salute]</source>
        <translation>[անցկացրեք առաջին բախտը]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="185"/>
        <source>[seduce]</source>
        <translation>[գայթակղություն]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="186"/>
        <source>[fist]</source>
        <translation>[բռունցք]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="187"/>
        <source>[disappointed]</source>
        <translation>[հիասթափված]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="188"/>
        <source>[love you]</source>
        <translation>[սիրում եմ քեզ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="189"/>
        <source>[NO]</source>
        <translation>[Ոչ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="190"/>
        <source>[OK]</source>
        <translation>[Լավ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="191"/>
        <source>[weep]</source>
        <translation>[լաց]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="192"/>
        <source>[curse]</source>
        <translation>[հայհոյել]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="193"/>
        <source>[weep sweat]</source>
        <translation>[լաց քրտինքը]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="194"/>
        <source>[victory]</source>
        <translation>[հաղթանակ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="195"/>
        <source>[basketball]</source>
        <translation>[բասկետբոլ]</translation>
    </message>
    <message>
        <location filename="childWidget/expresswidget.cpp" line="196"/>
        <source>[soccer]</source>
        <translation>[ֆուտբոլ]</translation>
    </message>
</context>
<context>
    <name>GiveRedPackWidget</name>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="26"/>
        <source>GiveRedPack</source>
        <oldsource>GiveRedPackWidget</oldsource>
        <translation>Կարմիր փաթեթը տվեք</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="83"/>
        <source>Red Packet</source>
        <translation>նվեր գումար</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="105"/>
        <source>Close</source>
        <translation>փակել</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="166"/>
        <source>Red Packet Token</source>
        <translation>նվեր գումար Արժույթը</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="195"/>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="382"/>
        <source>PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="251"/>
        <source>Escrow Balance</source>
        <translation>Կառավարվող հաշվեկշիռ</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="275"/>
        <source>0PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="294"/>
        <source>Transfer</source>
        <translation>Փոխանցում</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="338"/>
        <source>Amount Each</source>
        <translation>Բոլորը</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="363"/>
        <source>Set Amount</source>
        <translation>Սահմանեք գումարը</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="416"/>
        <source>Message</source>
        <translation>Հաղորդագրություն</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="438"/>
        <source>Best wishes</source>
        <translation>Հաջողություն</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="478"/>
        <source>Amount 0PWR</source>
        <translation>Գումարը 0PWR</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="518"/>
        <source>Red Packet History</source>
        <translation>նվեր գումար Գրառում</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="552"/>
        <source>Prepare Red Packet</source>
        <translation>Պատրաստել նվեր գումար</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.ui" line="590"/>
        <source>Unclaimed red packet will be refunded after 24 hours</source>
        <translation>Անընդունելի նվեր գումարները կվերադարձվեն 24 ժամվա ընթացքում</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="138"/>
        <source>Amount: </source>
        <translation>Գումար: </translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="146"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="180"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="189"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="195"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="213"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="218"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="228"/>
        <source>Notice</source>
        <translation>Ծանուցում</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="146"/>
        <source>Failed to prepare the red packet!</source>
        <translation>Չհաջողվեց պատրաստել նվեր գումար!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="153"/>
        <source>Best wishes!</source>
        <translation>Հաջողություն!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="180"/>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="189"/>
        <source>Amount cannot be empty!</source>
        <translation>Գումարը չի կարող դատարկ լինել!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="195"/>
        <source>The amount of the red packet cannot be greater than your balance!</source>
        <translation>Կարմիր փաթեթի գումարը չի կարող մեծ լինել, քան ձեր հաշվեկշիռը!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="201"/>
        <source>Please enter your login password</source>
        <translation>Մուտքագրեք ձեր մուտքի գաղտնաբառը</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="213"/>
        <source>Password Validation Failed!</source>
        <translation>Գաղտնաբառի վավերացումը չկատարվեց!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="218"/>
        <source>Password cannot be empty!</source>
        <translation>Գաղտնաբառը չի կարող դատարկ լինել!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="228"/>
        <source>Incorrect Password!</source>
        <translation>Սխալ գաղտնաբառ!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/giveredpackwidget.cpp" line="235"/>
        <source>Best Wishes!</source>
        <translation>Հաջողություն!</translation>
    </message>
</context>
<context>
    <name>GroupAddBuddyWidget</name>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="20"/>
        <source>GroupAddBuddy</source>
        <oldsource>GroupAddBuddyWidget</oldsource>
        <translation>Խումբ Ավելացնել Buddy</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="161"/>
        <source>Select contact(s)</source>
        <translation>Ընտրեք կոնտակտ (ներ)</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="239"/>
        <source>Search</source>
        <translation>Որոնում</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="294"/>
        <source>Please select contact(s) you want to invite</source>
        <translation>Խնդրում ենք ընտրել կոնտակտ (ներ) ը, ում ցանկանում եք հրավիրել</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="367"/>
        <source>OK</source>
        <translation>Լավ</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.ui" line="398"/>
        <source>Cancel</source>
        <translation>Չեղարկել</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="197"/>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="447"/>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="645"/>
        <source>Notice</source>
        <translation>Ծանուցում</translation>
    </message>
    <message>
        <source>This member has been in the tribe already!</source>
        <translation type="vanished">Այս անդամը արդեն ցեղում է եղել!</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="197"/>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="447"/>
        <source>This member has been in the group already!</source>
        <translation>Այս անդամն արդեն խմբում է եղել</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="266"/>
        <source>You have selected </source>
        <translation>Դուք ընտրել եք </translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="266"/>
        <source> contact(s)</source>
        <translation>կոնտակտ (ներ)</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="269"/>
        <source>Please select contacts you want to invite</source>
        <translation>Խնդրում ենք ընտրել կոնտակտներ, որոնք ցանկանում եք հրավիրել</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="625"/>
        <source> invited </source>
        <translation> հրավիրված </translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="625"/>
        <source> to join the group</source>
        <translation>միանալ խմբին</translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished"> միանալ ցեղին</translation>
    </message>
    <message>
        <location filename="childWidget/groupaddbuddywidget.cpp" line="645"/>
        <source>Added Successfully!</source>
        <translation>Հաջողությամբ է ավելացվել!</translation>
    </message>
</context>
<context>
    <name>GroupChatWidget</name>
    <message>
        <location filename="groupchatwidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="89"/>
        <source>Font</source>
        <translation>Տառատեսակի</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="95"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="120"/>
        <source>Expressions</source>
        <translation>Արտահայտությունները</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="148"/>
        <source>Vibration</source>
        <translation>Թրթռում</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="176"/>
        <source>Picture</source>
        <translation>Նկար</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="204"/>
        <source>Screen Cut</source>
        <translation>Էկրանի կտրում</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="232"/>
        <source>File</source>
        <translation>Ֆայլը</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="260"/>
        <source>Announcement</source>
        <translation>Հայտարարություն</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="288"/>
        <source>Mute All</source>
        <translation>Խլացնել բոլորը</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="319"/>
        <source>Red Packet</source>
        <translation>նվեր գումար</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="362"/>
        <source>Message Log</source>
        <translation>Պատմություն</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="389"/>
        <source>Send</source>
        <translation>Ուղարկել</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="392"/>
        <source>Alt+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="482"/>
        <source>Group Members</source>
        <translation>Խմբի անդամներ</translation>
    </message>
    <message>
        <source>Tribe Members</source>
        <translation type="vanished">Ցեղի անդամները</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="510"/>
        <source>Search</source>
        <translation>Որոնում</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="547"/>
        <source>Add Member</source>
        <translation>Ավելացնել անդամ</translation>
    </message>
    <message>
        <location filename="groupchatwidget.ui" line="588"/>
        <source>Refresh</source>
        <translation>Թարմացրեք</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="vanished">Պատճեն</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation type="vanished">Կտրել</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="vanished">Կպցնել</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Ջնջել</translation>
    </message>
    <message>
        <source>Empty Filename! Open Document Failed!</source>
        <translation type="vanished">Դատարկ ֆայլի անունը! Բաց փաստաթուղթը չհաջողվեց!</translation>
    </message>
    <message>
        <source>Notice</source>
        <translation type="vanished">注意</translation>
    </message>
    <message>
        <source>Empty file name or file doesn&apos;t exist!</source>
        <translation type="vanished">Դատարկ ֆայլի անունը կամ ֆայլը գոյություն չունի!</translation>
    </message>
    <message>
        <source>Cannot send file larger than 100M in size</source>
        <translation type="vanished">Հնարավոր չէ ուղարկել ֆայլը, քան 100M չափսով</translation>
    </message>
    <message>
        <source>[File]</source>
        <translation type="vanished">[Ֆայլ]</translation>
    </message>
    <message>
        <source>All(*.*)</source>
        <translation type="vanished">Բոլորը (*. *)</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Բացեք</translation>
    </message>
    <message>
        <source>Image File(*.bmp;*.jpeg;*.jpg;*.png;*.gif)</source>
        <translation type="vanished">Պատկեր Պատկեր (*.bmp;*.jpeg;*.jpg;*.png;*.gif)</translation>
    </message>
    <message>
        <source>Image File</source>
        <translation type="vanished">Պատկեր Պատկեր </translation>
    </message>
    <message>
        <source>[Image]</source>
        <translation type="vanished">[Պատկեր]</translation>
    </message>
    <message>
        <source>[Red Packet]</source>
        <translation type="vanished">[նվեր գումար]</translation>
    </message>
    <message>
        <source>Best Wishes</source>
        <translation type="vanished">Հաջողություն</translation>
    </message>
    <message>
        <source> joined the tribe</source>
        <translation type="vanished"> միացան ցեղին</translation>
    </message>
    <message>
        <source> invited </source>
        <translation type="vanished"> հրավիրված </translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished"> միանալ ցեղին</translation>
    </message>
    <message>
        <source>The other party has successfully received your file</source>
        <translation type="vanished">Մյուս կողմը հաջողությամբ ստացել է ձեր ֆայլը</translation>
    </message>
    <message>
        <source>This file already exists, overwrite or not？</source>
        <translation type="vanished">Այս ֆայլը արդեն գոյություն ունի, վերագրանցեք, թե ոչ?</translation>
    </message>
    <message>
        <source>This file already exists, overwrite or not?</source>
        <translation type="vanished">Այս ֆայլը արդեն գոյություն ունի, վերագրանցեք, թե ոչ?</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation type="vanished">Պահպանել որպես</translation>
    </message>
    <message>
        <source> Members %1</source>
        <translation type="vanished"> անդամներ %1</translation>
    </message>
    <message>
        <source>Cancel Administrator Privileges</source>
        <translation type="vanished">Հրաժարվել կառավարչի արտոնություններից</translation>
    </message>
    <message>
        <source>Set as Administrator</source>
        <translation type="vanished">Սահմանել որպես ադմինիստրատոր</translation>
    </message>
    <message>
        <source>View Profile</source>
        <translation type="vanished">Դիտել պրոֆիլը</translation>
    </message>
    <message>
        <source>Set Tribal Name</source>
        <translation type="vanished">Սահմանել ցեղի անունը</translation>
    </message>
    <message>
        <source>Remove Tribe Member</source>
        <translation type="vanished">Հեռացրեք ցեղի անդամին</translation>
    </message>
    <message>
        <source>Please enter your alias:</source>
        <translation type="vanished">Խնդրում ենք մուտքագրել ձեր alias:</translation>
    </message>
    <message>
        <source>Fail to check the red packet!</source>
        <translation type="vanished">Չհաջողվեց ստուգել կարմիր փաթեթը!</translation>
    </message>
    <message>
        <source>Add as friend</source>
        <translation type="vanished">Ավելացնել որպես ընկեր</translation>
    </message>
    <message>
        <source>Adding as friend succeed!</source>
        <translation type="vanished">Հաջող հավելում ընկերոջդ համար!</translation>
    </message>
    <message>
        <source>Adding as friend failed!</source>
        <translation type="vanished">Ավելացնել որպես ընկեր չհաջողվեց!</translation>
    </message>
    <message>
        <source>Tribe has been silenced...</source>
        <translation type="vanished">Ցեղը լռել է ...</translation>
    </message>
    <message>
        <source>Personal Business Card</source>
        <translation type="vanished">Անձնական բիզնես քարտ</translation>
    </message>
    <message>
        <source>Personal ID:</source>
        <translation type="vanished">Անձնական ID:</translation>
    </message>
    <message>
        <source>Tribe Business Card</source>
        <translation type="vanished">Ցեղ այցեքարտbul</translation>
    </message>
    <message>
        <source>Tribe ID:</source>
        <translation type="vanished">Ցեղի ID:</translation>
    </message>
    <message>
        <source>Local file does&apos;t exist and cannot be forwarded</source>
        <translation type="vanished">Տեղական ֆայլը գոյություն չունի եւ չի կարող փոխանցվել</translation>
    </message>
    <message>
        <source>Resend</source>
        <translation type="vanished">Վերսկսել</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="vanished">Փոխանցել</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="vanished">Մաքրել</translation>
    </message>
    <message>
        <source>[Announcement]</source>
        <translation type="vanished">[Հայտարարություն]</translation>
    </message>
    <message>
        <source>Received the file successfully!</source>
        <translation type="vanished">Ֆայլը հաջողությամբ ստացվեց!</translation>
    </message>
</context>
<context>
    <name>GroupFileWidget</name>
    <message>
        <location filename="groupfilewidget.ui" line="14"/>
        <source>GroupFileWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="groupfilewidget.ui" line="89"/>
        <source>file</source>
        <translation>ֆայլը</translation>
    </message>
    <message>
        <location filename="groupfilewidget.ui" line="94"/>
        <source>upload time</source>
        <translation>վերբեռնման ժամանակը</translation>
    </message>
    <message>
        <location filename="groupfilewidget.ui" line="99"/>
        <source>size</source>
        <translation>չափը</translation>
    </message>
    <message>
        <location filename="groupfilewidget.ui" line="104"/>
        <source>uploaded by</source>
        <translation>բեռնված</translation>
    </message>
    <message>
        <location filename="groupfilewidget.cpp" line="67"/>
        <source>%1 files in total</source>
        <translation>%1 ֆայլ ընդհանուր առմամբ</translation>
    </message>
    <message>
        <location filename="groupfilewidget.cpp" line="129"/>
        <source>unknown</source>
        <translation>անհայտ</translation>
    </message>
    <message>
        <location filename="groupfilewidget.cpp" line="172"/>
        <source>open file</source>
        <oldsource>open</oldsource>
        <translation>Բաց ֆայլը</translation>
    </message>
</context>
<context>
    <name>GroupPackWidget</name>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="26"/>
        <source>GroupPackWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="83"/>
        <source>Red Packet</source>
        <translation>նվեր գումար</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="105"/>
        <source>Close</source>
        <translation>փակել</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="156"/>
        <source>Random Red Packet</source>
        <translation>Պատահական նվեր գումար</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="210"/>
        <source>Identical Red Packet</source>
        <translation>Նույնական նվեր գումար</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="299"/>
        <source>Red Packet Token</source>
        <translation>նվեր դրամական նշան</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="328"/>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="569"/>
        <source>PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="384"/>
        <source>Escrow Balance</source>
        <translation>Կառավարվող հաշվեկշիռ</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="408"/>
        <source>0PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="427"/>
        <source>Transfer</source>
        <translation>Փոխանցում</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="480"/>
        <source>Total Amount</source>
        <translation>Ընդհանուր գումարը</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="516"/>
        <source>拼</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="550"/>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="659"/>
        <source>Enter Number</source>
        <translation>Քանակ</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="603"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="273"/>
        <source>Each person gets a random amount</source>
        <translation>Յուրաքանչյուր մարդ ստանում է նույնական գումար</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="637"/>
        <source>Number of Red Packets</source>
        <translation>Նվերի գումարի քանակը</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="687"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="260"/>
        <source>There are</source>
        <translation>Ընդամենը</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="718"/>
        <source>Message</source>
        <translation>Հաղորդագրություն թողնել</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="740"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="183"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="220"/>
        <source>Best Wishes</source>
        <translation>Հաջողություն</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="786"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="277"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="291"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="317"/>
        <source>Amount 0PWR</source>
        <translation>Գումարը 0PWR</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="826"/>
        <source>Red Packet History</source>
        <translation>նվեր գումար Գրառում</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="860"/>
        <source>Prepare Red Packet</source>
        <translation>Ուղարկել</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.ui" line="898"/>
        <source>Unclaimed red packet will be refunded after 24 hours</source>
        <translation>Անընդունելի նվեր գումարները կվերադարձվեն 24 ժամվա ընթացքում</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="107"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="117"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="124"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="129"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="135"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="161"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="166"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="176"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="213"/>
        <source>Notice</source>
        <translation>Ծանուցում</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="107"/>
        <source>The amount or the number of the red packet cannot be empty!</source>
        <translation>Կարմիր փաթեթի գումարը կամ համարը չի կարող դատարկ լինել!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="117"/>
        <source>The amount or the number of the red packet cannot be 0 !</source>
        <translation>Կարմիր փաթեթի գումարը կամ համարը չի կարող լինել 0!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="124"/>
        <source>The amount of the red packet cannot be greater than your balance!</source>
        <translation>Կարմիր փաթեթի գումարը չի կարող մեծ լինել, քան ձեր հաշվեկշիռը!</translation>
    </message>
    <message>
        <source>The number of the red packet cannot be greater than the number of people in the tribe!</source>
        <translation type="vanished">Կարմիր փաթեթի թիվը չի կարող ավելի մեծ լինել, քան ցեղի մարդկանց թիվը!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="129"/>
        <source>The number of the red packet cannot be greater than the number of people in the group!</source>
        <translation>Կարմիր փաթեթի թիվը չի կարող ավելի մեծ լինել, քան խմբի մեջ</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="135"/>
        <source>The amount of each red packet cannot be smaller than 0.00001!</source>
        <translation>Յուրաքանչյուր կարմիր փաթեթի գումարը չի կարող լինել ավելի փոքր, քան 0.00001!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="140"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="147"/>
        <source>Please enter your login password:</source>
        <translation>Մուտքագրեք ձեր մուտքի գաղտնաբառը:</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="161"/>
        <source>Failed to verify the login password!</source>
        <translation>Չհաջողվեց ստուգել մուտքի գաղտնաբառը!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="166"/>
        <source>Login password cannot be empty!</source>
        <translation>Մուտքագրված գաղտնաբառը չի կարող դատարկ լինել!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="176"/>
        <source>Incorrect Password!</source>
        <translation>Սխալ գաղտնաբառ!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="213"/>
        <source>Failed to prepare the red packet!</source>
        <translation>Չհաջողվեց պատրաստել նվեր գումար!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="260"/>
        <source>people in this group</source>
        <translation>այս խմբում</translation>
    </message>
    <message>
        <source>people in this tribe</source>
        <translation type="vanished">այս ցեղի մարդիկ</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="271"/>
        <source>Amount</source>
        <translation>Գումարը</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="285"/>
        <source>Amount Each</source>
        <translation>Յուրաքանչյուրը</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="287"/>
        <source>Each person gets a identical amount</source>
        <translation>Յուրաքանչյուր մարդ ստանում է նույնական գումար</translation>
    </message>
    <message>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="294"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="305"/>
        <location filename="redPacketWidget/GroupPackWidget.cpp" line="322"/>
        <source>Amount </source>
        <translation>Գումարը </translation>
    </message>
</context>
<context>
    <name>GroupSearchWidget</name>
    <message>
        <location filename="childWidget/groupsearchwidget.ui" line="23"/>
        <source>GroupSearchWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childWidget/groupsearchwidget.ui" line="74"/>
        <source>Find Group Member</source>
        <translation>Գտնել խմբի անդամ</translation>
    </message>
    <message>
        <source>Find Tribe Member</source>
        <translation type="vanished">Գտնել ցեղի անդամը</translation>
    </message>
</context>
<context>
    <name>GroupUserProfileWidget</name>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="14"/>
        <source>GroupUserProfile</source>
        <oldsource>GroupUserProfileWidget</oldsource>
        <translation>Խմբի անձնագիրը պրոֆիլը</translation>
    </message>
    <message>
        <source>Personal ID</source>
        <translation type="vanished">Անհատական ID</translation>
    </message>
    <message>
        <source>Base ID</source>
        <translation type="vanished">Հիմնական ID</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="111"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="188"/>
        <source>Acct.No</source>
        <translation>Հաշիվ</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="234"/>
        <source>Copy</source>
        <translation>Պատճեն</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="336"/>
        <source>View Avatar</source>
        <translation>Դիտել Անձնանշան</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.ui" line="630"/>
        <source>Add as friend</source>
        <translation>Ավելացնել որպես ընկեր</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.cpp" line="25"/>
        <source>Close</source>
        <translation>փակել</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.cpp" line="70"/>
        <source>Send</source>
        <translation>Ուղարկել</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.cpp" line="189"/>
        <source>Request sent successfully</source>
        <translation>Հարցումը հաջողությամբ ուղարկվեց</translation>
    </message>
    <message>
        <location filename="childWidget/groupuserprofilewidget.cpp" line="193"/>
        <source>Request sent Failed</source>
        <translation>Ուղարկման հարցում ձախողվեց</translation>
    </message>
</context>
<context>
    <name>GroupWidget</name>
    <message>
        <location filename="groupwidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="groupwidget.ui" line="83"/>
        <source>View the Details</source>
        <translation>Դիտեք մանրամասները</translation>
    </message>
    <message>
        <location filename="groupwidget.ui" line="133"/>
        <source>chat</source>
        <translation>զրուցել</translation>
    </message>
    <message>
        <location filename="groupwidget.ui" line="176"/>
        <source>file</source>
        <translation>ֆայլը</translation>
    </message>
</context>
<context>
    <name>IMGroupChatStore</name>
    <message>
        <location filename="imgroupchatstore.cpp" line="495"/>
        <location filename="imgroupchatstore.cpp" line="1260"/>
        <location filename="imgroupchatstore.cpp" line="2109"/>
        <source>Best Wishes</source>
        <translation>Հաջողություն</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="598"/>
        <location filename="imgroupchatstore.cpp" line="1413"/>
        <location filename="imgroupchatstore.cpp" line="2267"/>
        <source>The other party has successfully received your file</source>
        <translation>Մյուս կողմը հաջողությամբ ստացել է ձեր ֆայլը</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="933"/>
        <location filename="imgroupchatstore.cpp" line="1765"/>
        <location filename="imgroupchatstore.cpp" line="1809"/>
        <location filename="imgroupchatstore.cpp" line="2577"/>
        <source>Notice</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="933"/>
        <source>This file already exists, overwrite or not?</source>
        <translation>Այս ֆայլը արդեն գոյություն ունի, վերագրանցեք, թե ոչ?</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1409"/>
        <location filename="imgroupchatstore.cpp" line="2263"/>
        <location filename="imgroupchatstore.cpp" line="2986"/>
        <source> invited </source>
        <translation></translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished"> միանալ ցեղին</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1623"/>
        <location filename="imgroupchatstore.cpp" line="1631"/>
        <location filename="imgroupchatstore.cpp" line="1650"/>
        <source>[Image]</source>
        <translation>[Պատկեր]</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1750"/>
        <location filename="imgroupchatstore.cpp" line="1793"/>
        <location filename="imgroupchatstore.cpp" line="2641"/>
        <location filename="imgroupchatstore.cpp" line="2649"/>
        <location filename="imgroupchatstore.cpp" line="2696"/>
        <source>[File]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1765"/>
        <location filename="imgroupchatstore.cpp" line="1809"/>
        <source>Local file does&apos;t exist and cannot be forwarded</source>
        <translation>Տեղական ֆայլը գոյություն չունի եւ չի կարող փոխանցվել</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2430"/>
        <source>Personal Business Card</source>
        <translation>Անձնական բիզնես քարտ</translation>
    </message>
    <message>
        <source>Personal ID:</source>
        <translation type="vanished">Անձնական ID:</translation>
    </message>
    <message>
        <source>Tribe Business Card</source>
        <translation type="vanished">Ցեղ այցեքարտbul</translation>
    </message>
    <message>
        <source>Tribe ID:</source>
        <translation type="vanished">Ցեղի ID:</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1407"/>
        <location filename="imgroupchatstore.cpp" line="2261"/>
        <location filename="imgroupchatstore.cpp" line="2984"/>
        <source> joined the group</source>
        <translation>միացավ խմբին</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="1409"/>
        <location filename="imgroupchatstore.cpp" line="2263"/>
        <location filename="imgroupchatstore.cpp" line="2986"/>
        <source> to join the group</source>
        <translation>միանալ խմբին</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2431"/>
        <source>ID:</source>
        <translation>ID:</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2444"/>
        <source>Group Business Card</source>
        <translation>Խմբի բիզնես քարտ</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2445"/>
        <source>Group ID:</source>
        <translation>Խմբի ID:</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2551"/>
        <source>[Red Packet]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2577"/>
        <source>Cannot send file larger than 100M in size</source>
        <translation>Հնարավոր չէ ուղարկել ֆայլը, քան 100M չափսով</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2790"/>
        <location filename="imgroupchatstore.cpp" line="2798"/>
        <location filename="imgroupchatstore.cpp" line="2815"/>
        <source>[Announcement]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2913"/>
        <source>Save as</source>
        <translation>Պահպանել որպես</translation>
    </message>
    <message>
        <location filename="imgroupchatstore.cpp" line="2990"/>
        <source>Received the file successfully!</source>
        <translation>Ֆայլը հաջողությամբ ստացվեց!</translation>
    </message>
</context>
<context>
    <name>IMGroupChatView</name>
    <message>
        <location filename="imgroupchatview.cpp" line="137"/>
        <source> Members %1</source>
        <translation> անդամներ %1</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="356"/>
        <location filename="imgroupchatview.cpp" line="1751"/>
        <location filename="imgroupchatview.cpp" line="1758"/>
        <source>Copy</source>
        <translation>Պատճեն</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="359"/>
        <source>Cut</source>
        <translation>Կտրել</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="362"/>
        <source>Paste</source>
        <translation>Կպցնել</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="365"/>
        <source>Delete</source>
        <translation>Ջնջել</translation>
    </message>
    <message>
        <source>Tribe has been silenced...</source>
        <translation type="vanished">Ցեղը լռել է ...</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="933"/>
        <source>Cancel Administrator Privileges</source>
        <translation>Հրաժարվել կառավարչի արտոնություններից</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="939"/>
        <source>Set as Administrator</source>
        <translation>Սահմանել որպես ադմինիստրատոր</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="946"/>
        <source>View Profile</source>
        <translation>Դիտել պրոֆիլը</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="951"/>
        <source>Set Remark Name</source>
        <oldsource>Set Group Name</oldsource>
        <translation>Նշեք Նշանի անունը</translation>
    </message>
    <message>
        <source>Remove Tribe Member</source>
        <translation type="vanished">Հեռացրեք ցեղի անդամին</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="788"/>
        <source>Group has been silenced...</source>
        <translation>Խումբը արգելված է</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="958"/>
        <source>Remove Group Member</source>
        <translation>Հեռացրեք խմբի անդամ</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1014"/>
        <source>Please enter your alias:</source>
        <translation>Խնդրում ենք մուտքագրել ձեր alias:</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1303"/>
        <source>Open</source>
        <translation>Բացեք</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1304"/>
        <source>Image File</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1521"/>
        <source>Send</source>
        <translation>Ուղարկել</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1521"/>
        <source>All(*.*)</source>
        <translation>Բոլորը (*. *)</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1652"/>
        <source>Empty Filename! Open Document Failed!</source>
        <translation>Դատարկ ֆայլի անունը! Բաց փաստաթուղթը չհաջողվեց!</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1701"/>
        <location filename="imgroupchatview.cpp" line="2109"/>
        <source>Notice</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1701"/>
        <source>Empty file name or file doesn&apos;t exist!</source>
        <translation>Դատարկ ֆայլի անունը կամ ֆայլը գոյություն չունի!</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1768"/>
        <source>Resend</source>
        <translation>Վերսկսել</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1778"/>
        <source>Reload</source>
        <translation>Վերբեռնեք</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1786"/>
        <source>Forward</source>
        <translation>Փոխանցել</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="1797"/>
        <source>Clear</source>
        <translation>Մաքրել</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="2109"/>
        <source>Fail to check the red packet!</source>
        <translation>Չհաջողվեց ստուգել կարմիր փաթեթը!</translation>
    </message>
    <message>
        <location filename="imgroupchatview.cpp" line="2246"/>
        <source>@</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>IMGroupChatViewModel</name>
    <message>
        <location filename="imgroupchatviewmodel.cpp" line="206"/>
        <source>[Image]</source>
        <translation>[Պատկեր]</translation>
    </message>
</context>
<context>
    <name>IMPerChat</name>
    <message>
        <location filename="imperchat.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="92"/>
        <source>View the Detail</source>
        <translation>Դիտեք մանրամասնը</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="228"/>
        <source>Screen Cut</source>
        <translation>Էկրանի կտրում</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="256"/>
        <source>Font</source>
        <translation>Տառատեսակի</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="284"/>
        <source>Expressions</source>
        <translation>արտահայտությունը</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="312"/>
        <source>Vibration</source>
        <translation>Թրթռում</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="340"/>
        <source>Picture</source>
        <translation>Նկար</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="368"/>
        <source>File</source>
        <translation>Ֆայլը</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="396"/>
        <source>Secret Message</source>
        <translation>Գաղտնի նամակ</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="424"/>
        <source>Secret Picture</source>
        <translation>Գաղտնի նկար</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="452"/>
        <source>Secret File</source>
        <translation>Գաղտնի ֆայլը</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="480"/>
        <source>Announcement</source>
        <translation>Հայտարարություն</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="536"/>
        <source>ETH Transfer</source>
        <translation>ETH փոխանցում</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="564"/>
        <source>BTC Transfer</source>
        <translation>BTC փոխանցում</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="592"/>
        <source>EOS Transfer</source>
        <translation>EOS փոխանցում</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="620"/>
        <source>Red Packet</source>
        <translation>նվեր գումար</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="672"/>
        <source>Message Log</source>
        <translation>Պատմություն</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="696"/>
        <source>Send</source>
        <translation>Ուղարկել</translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="699"/>
        <source>Alt+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchat.ui" line="742"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;微软雅黑&apos;; font-size:12pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>Create Tribe</source>
        <translation type="vanished">Ստեղծեք ցեղը</translation>
    </message>
    <message>
        <source>Add as a friend</source>
        <translation type="vanished">Ավելացնել որպես ընկեր</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="vanished">Պատճեն</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation type="vanished">Կտրել</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="vanished">Կպցնել</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Ջնջել</translation>
    </message>
    <message>
        <source>Power Exchange</source>
        <translation type="vanished">Էներգետիկ փոխանակում</translation>
    </message>
    <message>
        <source>ETH Tranfer</source>
        <translation type="vanished">ETH փոխանցում</translation>
    </message>
    <message>
        <source>BTC Tranfer</source>
        <translation type="vanished">BTC փոխանցում</translation>
    </message>
    <message>
        <source>EOS Tranfer</source>
        <translation type="vanished">Eos փոխանցելու</translation>
    </message>
    <message>
        <source>Notice</source>
        <translation type="vanished">Ծանուցում</translation>
    </message>
    <message>
        <source>[File]</source>
        <translation type="vanished">[Ֆայլ]</translation>
    </message>
    <message>
        <source>Empty file name or file doesn&apos;t exist!</source>
        <translation type="vanished">Դատարկ ֆայլի անունը կամ ֆայլը գոյություն չունի!</translation>
    </message>
    <message>
        <source>Local file does&apos;t exist，unable to forward</source>
        <translation type="vanished">Տեղական ֆայլը գոյություն չունի, անհնար է առաջարկել</translation>
    </message>
    <message>
        <source>[Image]</source>
        <translation type="vanished">[Պատկեր]</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Բացեք</translation>
    </message>
    <message>
        <source>Image File(*.bmp;*.jpeg;*.jpg;*.png;*.gif)</source>
        <translation type="vanished">Գրաֆիկական ֆայլ(*.bmp;*.jpeg;*.jpg;*.png;*.gif)</translation>
    </message>
    <message>
        <source>Best Wishes</source>
        <translation type="vanished">Հաջողություն</translation>
    </message>
    <message>
        <source>Received file successfully!</source>
        <translation type="vanished">Ստացված ֆայլը հաջողությամբ!</translation>
    </message>
    <message>
        <source>The other party has successfully received your file</source>
        <translation type="vanished">Մյուս կողմը հաջողությամբ ստացել է ձեր ֆայլը</translation>
    </message>
    <message>
        <source>This file already exists, overwrite or not？</source>
        <translation type="vanished">Այս ֆայլը արդեն գոյություն ունի, վերագրանցեք, թե ոչ?</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation type="vanished">Պահպանել որպես</translation>
    </message>
    <message>
        <source>Receive file successfully!</source>
        <translation type="vanished">Ստացեք ֆայլը հաջողությամբ!</translation>
    </message>
    <message>
        <source>[Secret Message]</source>
        <translation type="vanished">[Գաղտնի նամակ]</translation>
    </message>
    <message>
        <source>[Secret Image]</source>
        <translation type="vanished">Գաղտնի նկար</translation>
    </message>
    <message>
        <source>[Secret File]</source>
        <translation type="vanished">[Secret file]</translation>
    </message>
    <message>
        <source>[Red Packet]</source>
        <translation type="vanished">[նվեր գումար]</translation>
    </message>
    <message>
        <source>Personal business card</source>
        <translation type="vanished">Անձնական այցեքարտ</translation>
    </message>
    <message>
        <source>Personal ID:</source>
        <translation type="vanished">Անձնական ID:</translation>
    </message>
    <message>
        <source>Tribe business card</source>
        <translation type="vanished">Ցանցային այցեքարտ</translation>
    </message>
    <message>
        <source>Tribe ID:</source>
        <translation type="vanished">Ցեղի ID:</translation>
    </message>
    <message>
        <source>All(*.*)</source>
        <translation type="vanished">Բոլորը (*. *)</translation>
    </message>
    <message>
        <source>Fail to check the red packet!</source>
        <translation type="vanished">Չհաջողվեց ստուգել կարմիր փաթեթը!</translation>
    </message>
    <message>
        <source>Resend</source>
        <translation type="vanished">Վերսկսել</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation type="vanished">Վերբեռնեք</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="vanished">Փոխանցել</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="vanished">Մաքրել</translation>
    </message>
    <message>
        <source>[Announcement]</source>
        <translation type="vanished">Հայտարարություն</translation>
    </message>
    <message>
        <source>（Typing...）</source>
        <translation type="vanished">(Տպել...)</translation>
    </message>
</context>
<context>
    <name>IMPerChatStore</name>
    <message>
        <location filename="imperchatstore.cpp" line="696"/>
        <location filename="imperchatstore.cpp" line="1379"/>
        <source>Best Wishes</source>
        <translation>Հաջողություն</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="872"/>
        <source>The other party has successfully received your file</source>
        <translation>Մյուս կողմը հաջողությամբ ստացել է ձեր ֆայլը</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="923"/>
        <location filename="imperchatstore.cpp" line="1097"/>
        <location filename="imperchatstore.cpp" line="1917"/>
        <location filename="imperchatstore.cpp" line="2191"/>
        <source>Notice</source>
        <translation></translation>
    </message>
    <message>
        <source>This file already exists, overwrite or not？</source>
        <translation type="vanished">Այս ֆայլը արդեն գոյություն ունի, վերագրանցեք, թե ոչ?</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="923"/>
        <source>This file already exists, overwrite or not?</source>
        <translation>Այս ֆայլը արդեն գոյություն ունի, վերագրանցեք, թե ոչ?</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1039"/>
        <location filename="imperchatstore.cpp" line="1044"/>
        <location filename="imperchatstore.cpp" line="1057"/>
        <source>Receive file successfully!</source>
        <translation>Ստացեք ֆայլը հաջողությամբ!</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1097"/>
        <source>File is in use, please check it first!</source>
        <translation>Ֆայլը օգտագործվում է, նախ ստուգեք այն</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1574"/>
        <source>Received file successfully!</source>
        <translation>Ստացված ֆայլը հաջողությամբ!</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1763"/>
        <location filename="imperchatstore.cpp" line="1792"/>
        <source>[Image]</source>
        <translation>[Պատկեր]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1900"/>
        <location filename="imperchatstore.cpp" line="2256"/>
        <location filename="imperchatstore.cpp" line="2318"/>
        <source>[File]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1917"/>
        <source>Local file does&apos;t exist，unable to forward</source>
        <translation>Տեղական ֆայլը գոյություն չունի, անհնար է առաջարկել</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1991"/>
        <source>Personal business card</source>
        <translation>Անձնական այցեքարտ</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="1992"/>
        <source>ID:</source>
        <translation>ID:</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2005"/>
        <source>Group business card</source>
        <translation>Խմբի բիզնես քարտ</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2006"/>
        <source>Group ID:</source>
        <translation>Խմբի ID:</translation>
    </message>
    <message>
        <source>Personal ID:</source>
        <translation type="vanished">Անձնական ID:</translation>
    </message>
    <message>
        <source>Tribe business card</source>
        <translation type="vanished">Ցանցային այցեքարտ</translation>
    </message>
    <message>
        <source>Tribe ID:</source>
        <translation type="vanished">Ցեղի ID:</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2162"/>
        <source>[Red Packet]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2191"/>
        <source>File larger than 100M cannot be sent</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2363"/>
        <source>[Secret Message]</source>
        <translation>[Գաղտնի նամակ]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2395"/>
        <source>[Secret Image]</source>
        <translation>[Գաղտնի պատկեր]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2427"/>
        <source>[Secret File]</source>
        <translation>[Գաղտնի Ֆայլ]</translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2508"/>
        <location filename="imperchatstore.cpp" line="2540"/>
        <source>[Announcement]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchatstore.cpp" line="2601"/>
        <source>Save as</source>
        <translation>Պահպանել որպես</translation>
    </message>
</context>
<context>
    <name>IMPerChatView</name>
    <message>
        <source>Create Tribe</source>
        <translation type="vanished">Ստեղծեք ցեղը</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="153"/>
        <source>Create Group</source>
        <translation>Ստեղծեք խումբ</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="164"/>
        <source>Add as a friend</source>
        <translation>Ավելացնել որպես ընկեր</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="221"/>
        <source>Power Exchange</source>
        <translation>Էներգետիկ փոխանակում</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="226"/>
        <source>ETH Tranfer</source>
        <translation>ETH փոխանցում</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="231"/>
        <source>BTC Tranfer</source>
        <translation>BTC փոխանցում</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="236"/>
        <source>EOS Tranfer</source>
        <translation>Eos փոխանցելու</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="598"/>
        <location filename="imperchatview.cpp" line="1777"/>
        <location filename="imperchatview.cpp" line="1783"/>
        <source>Copy</source>
        <translation>Պատճեն</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="601"/>
        <source>Cut</source>
        <translation>Կտրել</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="604"/>
        <source>Paste</source>
        <translation>Կպցնել</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="607"/>
        <source>Delete</source>
        <translation>Ջնջել</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="751"/>
        <source>（Typing...）</source>
        <translation>(Տպել...)</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="953"/>
        <source>Open</source>
        <translation>Բացեք</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="954"/>
        <source>Image File</source>
        <translation>պատկերի ֆայլը</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1030"/>
        <source>Send</source>
        <translation>Ուղարկել</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1030"/>
        <source>All(*.*)</source>
        <translation>Բոլորը (*. *)</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1293"/>
        <location filename="imperchatview.cpp" line="1684"/>
        <source>Notice</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1293"/>
        <source>Empty file name or file doesn&apos;t exist!</source>
        <translation>Դատարկ ֆայլի անունը կամ ֆայլը գոյություն չունի!</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1325"/>
        <source>fileName为空!OpenDocument Failed!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1684"/>
        <source>Fail to check the red packet!</source>
        <translation>Չհաջողվեց ստուգել կարմիր փաթեթը!</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1793"/>
        <source>Resend</source>
        <translation>Վերսկսել</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1801"/>
        <source>Reload</source>
        <translation>Վերբեռնեք</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1809"/>
        <source>Forward</source>
        <translation>Փոխանցել</translation>
    </message>
    <message>
        <location filename="imperchatview.cpp" line="1819"/>
        <source>Clear</source>
        <translation>Մաքրել</translation>
    </message>
</context>
<context>
    <name>IMPerChatViewModel</name>
    <message>
        <location filename="imperchatviewmodel.cpp" line="166"/>
        <source>[Image]</source>
        <translation>[Պատկեր]</translation>
    </message>
</context>
<context>
    <name>MessageList</name>
    <message>
        <source> joined the tribe</source>
        <translation type="vanished"> միացան ցեղին</translation>
    </message>
    <message>
        <source> invited </source>
        <translation type="vanished"> հրավիրված </translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished"> միանալ ցեղին</translation>
    </message>
    <message>
        <source>File received successfully </source>
        <translation type="vanished">Պատկերը հաջողությամբ ստացվեց </translation>
    </message>
    <message>
        <source>The other party has successfully received your file </source>
        <translation type="vanished">Մյուս կողմը հաջողությամբ ստացել է ձեր ֆայլը </translation>
    </message>
    <message>
        <source>[Announcement]</source>
        <translation type="vanished">[Հայտարարություն]</translation>
    </message>
    <message>
        <source>[Share]</source>
        <translation type="vanished">[Բաժնետոմս]</translation>
    </message>
    <message>
        <source>&lt;font color=&apos;#f7931e&apos; &gt;[I was @]&lt;/font&gt;</source>
        <translation type="vanished">&lt;font color=&apos;#f7931e&apos; &gt;[Ինչ-որ մեկը @ ինձ]&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Yesterday</source>
        <translation type="vanished">Երեկ</translation>
    </message>
    <message>
        <source>View Details</source>
        <translation type="vanished">Մանրամասնորեն</translation>
    </message>
    <message>
        <source>Close Chat</source>
        <translation type="vanished">Զրույցը փակելիս</translation>
    </message>
    <message>
        <source>Close All Chats</source>
        <translation type="vanished">Փակել բոլոր զրույցները</translation>
    </message>
    <message>
        <source>Remove From Top</source>
        <translation type="vanished">Հեռացնել վերեւից</translation>
    </message>
    <message>
        <source>Set Message Top</source>
        <translation type="vanished">Հաղորդագրության վերեւ</translation>
    </message>
    <message>
        <source>Cancle No-Disturbing</source>
        <translation type="vanished">Չեղարկել արգելքը</translation>
    </message>
    <message>
        <source>Set No-Disturbing</source>
        <translation type="vanished">Սահմանել արգելք</translation>
    </message>
    <message>
        <source>Dismiss Tribe</source>
        <translation type="vanished">Հեռացրեք շիրիմին</translation>
    </message>
    <message>
        <source>Quit Tribe</source>
        <translation type="vanished">Դուրս գալ ցեղին</translation>
    </message>
    <message>
        <source>[Image]</source>
        <translation type="vanished">[Պատկեր]</translation>
    </message>
    <message>
        <source>[Audio]</source>
        <translation type="vanished">Աուդիո</translation>
    </message>
    <message>
        <source>[Video]</source>
        <translation type="vanished">[տեսանյութ]</translation>
    </message>
    <message>
        <source>[File]</source>
        <translation type="vanished">[ֆայլը]</translation>
    </message>
    <message>
        <source>This type of message is not supported for now</source>
        <translation type="vanished">Այս հաղորդագրության տեսակը դեռեւս չի աջակցվում</translation>
    </message>
    <message>
        <source>[Tranfer]</source>
        <translation type="vanished">[Փոխանցում]</translation>
    </message>
    <message>
        <source>[Red Packet]</source>
        <translation type="vanished">[դրամական նվեր]</translation>
    </message>
    <message>
        <source>[Secret Message]</source>
        <translation type="vanished">[Գաղտնի նամակ]</translation>
    </message>
    <message>
        <source>[Secret Image]</source>
        <translation type="vanished">[Գաղտնի նկար]</translation>
    </message>
    <message>
        <source>[Secret File]</source>
        <translation type="vanished">[Գաղտնի ֆայլը]</translation>
    </message>
    <message>
        <source>[Location]</source>
        <translation type="vanished">[Որտեղից]</translation>
    </message>
    <message>
        <source>[Gravitational Waves]</source>
        <translation type="vanished">[Գրաֆիտացիոն ալիքներ]</translation>
    </message>
    <message>
        <source>Notice</source>
        <translation type="vanished">Ծանուցում</translation>
    </message>
    <message>
        <source>Are you sure to dismiss this tribe?</source>
        <translation type="vanished">Վստահ եք այս ցեղին ազատելու համար?</translation>
    </message>
    <message>
        <source>Are you sure to quit tribe?</source>
        <translation type="vanished">Վստահ եք հրաժարվել ցեղից?</translation>
    </message>
    <message>
        <source>Network request failed!</source>
        <translation type="vanished">Ցանցի պահանջը ձախողվեց!</translation>
    </message>
    <message>
        <source>&lt;font color=&apos;#f7931e&apos; &gt;[Draft]&lt;/font&gt;</source>
        <translation type="vanished">&lt;font color=&apos;#f7931e&apos; &gt;[Նախագիծ]&lt;/font&gt;</translation>
    </message>
</context>
<context>
    <name>MessageListDispatcher</name>
    <message>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="160"/>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="171"/>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="197"/>
        <source>Notice</source>
        <translation>Ծանուցում</translation>
    </message>
    <message>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="160"/>
        <source>Are you sure to dismiss this group?</source>
        <translation>Վստահ եք, որ ցանկանում եք լուծարել խումբը:</translation>
    </message>
    <message>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="171"/>
        <source>Are you sure to quit group?</source>
        <translation>Վստահ եք դուրս գալ խմբից:</translation>
    </message>
    <message>
        <source>Are you sure to dismiss this tribe?</source>
        <translation type="vanished">Վստահ եք այս ցեղին ազատելու համար?</translation>
    </message>
    <message>
        <source>Are you sure to quit tribe?</source>
        <translation type="vanished">Վստահ եք հրաժարվել ցեղից?</translation>
    </message>
    <message>
        <location filename="MessageWidget/messagelistdispatcher.cpp" line="197"/>
        <source>Network request failed!</source>
        <translation>Ցանցի պահանջը ձախողվեց!</translation>
    </message>
</context>
<context>
    <name>MessageListStore</name>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="148"/>
        <location filename="MessageWidget/messageliststore.cpp" line="725"/>
        <source> joined the group</source>
        <oldsource> joined the tribe</oldsource>
        <translation>Գրանցված խումբը</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="150"/>
        <location filename="MessageWidget/messageliststore.cpp" line="727"/>
        <source> invited </source>
        <translation> հրավիրված </translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished"> միանալ ցեղին</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="156"/>
        <location filename="MessageWidget/messageliststore.cpp" line="558"/>
        <location filename="MessageWidget/messageliststore.cpp" line="733"/>
        <source>File received successfully </source>
        <translation>Պատկերը հաջողությամբ ստացվեց </translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="158"/>
        <location filename="MessageWidget/messageliststore.cpp" line="562"/>
        <location filename="MessageWidget/messageliststore.cpp" line="735"/>
        <source>The other party has successfully received your file </source>
        <translation>Մյուս կողմը հաջողությամբ ստացել է ձեր ֆայլը </translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="195"/>
        <location filename="MessageWidget/messageliststore.cpp" line="198"/>
        <location filename="MessageWidget/messageliststore.cpp" line="537"/>
        <location filename="MessageWidget/messageliststore.cpp" line="785"/>
        <location filename="MessageWidget/messageliststore.cpp" line="788"/>
        <source>[Announcement]</source>
        <translation>[Հայտարարություն]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="209"/>
        <location filename="MessageWidget/messageliststore.cpp" line="212"/>
        <location filename="MessageWidget/messageliststore.cpp" line="544"/>
        <location filename="MessageWidget/messageliststore.cpp" line="798"/>
        <location filename="MessageWidget/messageliststore.cpp" line="801"/>
        <source>[Share]</source>
        <translation>[Բաժնետոմս]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="319"/>
        <source>View Details</source>
        <translation>Մանրամասնորեն</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="323"/>
        <source>Close Chat</source>
        <translation>Զրույցը փակելիս</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="327"/>
        <source>Close All Chats</source>
        <translation>Փակել բոլոր զրույցները</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="344"/>
        <source>Remove From Top</source>
        <translation>Հեռացնել վերեւից</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="352"/>
        <source>Set Message Top</source>
        <translation>Հաղորդագրության վերեւ</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="360"/>
        <source>Cancle No-Disturbing</source>
        <translation>Չեղարկել արգելքը</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="368"/>
        <source>Set No-Disturbing</source>
        <translation>Սահմանել արգելք</translation>
    </message>
    <message>
        <source>Dismiss Tribe</source>
        <translation type="vanished">Հեռացրեք շիրիմին</translation>
    </message>
    <message>
        <source>Quit Tribe</source>
        <translation type="vanished">Դուրս գալ ցեղին</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="150"/>
        <location filename="MessageWidget/messageliststore.cpp" line="727"/>
        <source> to join the group</source>
        <translation>Միանալ խմբին</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="386"/>
        <source>Dismiss Group</source>
        <translation>Խուսափեք խումբը</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="395"/>
        <source>Quit Group</source>
        <translation>Խուսափեք խումբից</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="490"/>
        <source>Yesterday</source>
        <translation>Երեկ</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="509"/>
        <source>[Image]</source>
        <translation>[Պատկեր]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="511"/>
        <source>[Audio]</source>
        <translation>Աուդիո</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="513"/>
        <source>[Video]</source>
        <translation>[տեսանյութ]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="515"/>
        <source>[File]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="517"/>
        <source>This type of message is not supported for now</source>
        <translation>Այս հաղորդագրության տեսակը դեռեւս չի աջակցվում</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="519"/>
        <source>[Tranfer]</source>
        <translation>[Փոխանցում]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="521"/>
        <source>[Red Packet]</source>
        <translation>[դրամական նվեր]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="523"/>
        <source>[Secret Message]</source>
        <translation>[Գաղտնի նամակ]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="525"/>
        <source>[Secret Image]</source>
        <translation>[Գաղտնի նկար]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="527"/>
        <source>[Secret File]</source>
        <translation>[Գաղտնի ֆայլը]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="529"/>
        <source>[Location]</source>
        <translation>[Որտեղից]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="531"/>
        <source>[Gravitational Waves]</source>
        <translation>[Գրաֆիտացիոն ալիքներ]</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="627"/>
        <location filename="MessageWidget/messageliststore.cpp" line="631"/>
        <location filename="MessageWidget/messageliststore.cpp" line="877"/>
        <location filename="MessageWidget/messageliststore.cpp" line="896"/>
        <location filename="MessageWidget/messageliststore.cpp" line="983"/>
        <location filename="MessageWidget/messageliststore.cpp" line="987"/>
        <location filename="MessageWidget/messageliststore.cpp" line="1223"/>
        <source>&lt;font color=&apos;#f7931e&apos; &gt;[I was @]&lt;/font&gt;</source>
        <translation>&lt;font color=&apos;#f7931e&apos; &gt;[Ինչ-որ մեկը @ ինձ]&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="1730"/>
        <source>&lt;font color=&apos;#f7931e&apos; &gt;[Draft]&lt;/font&gt;</source>
        <translation>&lt;font color=&apos;#f7931e&apos; &gt;[Նախագիծ]&lt;/font&gt;</translation>
    </message>
</context>
<context>
    <name>MessageLog</name>
    <message>
        <location filename="messagelog/messagelog.ui" line="26"/>
        <source>Message Log</source>
        <translation>Զրույցի պատմություն</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="330"/>
        <source>Messsage Log</source>
        <translation>Հաղորդագրություն գրառում</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="484"/>
        <source>Friends</source>
        <translation>Ընկերներ</translation>
    </message>
    <message>
        <source>Tribe</source>
        <translation type="vanished">Ցեղ</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="524"/>
        <source>Group</source>
        <translation>Խումբը</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="578"/>
        <source>🔎</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="598"/>
        <source>Search</source>
        <translation>Որոնում</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="623"/>
        <source>Clear</source>
        <translation>Մաքրել</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="697"/>
        <source>about:blank</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="811"/>
        <source>|&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="837"/>
        <source>&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="857"/>
        <source>&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.ui" line="877"/>
        <source>&gt;|</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="45"/>
        <source>微软雅黑</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1068"/>
        <source>Best Wishes</source>
        <translation>Հաջողություն</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1212"/>
        <location filename="messagelog/messagelog.cpp" line="1340"/>
        <location filename="messagelog/messagelog.cpp" line="1831"/>
        <location filename="messagelog/messagelog.cpp" line="1970"/>
        <source>Notice</source>
        <translation>Ծանուցում</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1212"/>
        <source>Empty file name or file doesn&apos;t exist!</source>
        <translation>Դատարկ ֆայլի անունը կամ ֆայլը գոյություն չունի!</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1244"/>
        <source>fileName为空!OpenDocument Failed!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1333"/>
        <source>Sorry</source>
        <translation>Ներողություն</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1333"/>
        <source>There are no qualified message records</source>
        <translation>Չկան որակյալ հաղորդագրություն գրառումներ</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1340"/>
        <source>Unable to search empty content</source>
        <translation>Հնարավոր չէ որոնել դատարկ բովանդակություն</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1831"/>
        <source>Failed to check the red packet!</source>
        <translation>Չհաջողվեց ստուգել կարմիր փաթեթը!</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="1970"/>
        <source>This file already exists, overwrite or not?</source>
        <translation>Այս ֆայլը արդեն գոյություն ունի, վերագրանցեք, թե ոչ?</translation>
    </message>
    <message>
        <source>This file already exists, overwrite or not？</source>
        <translation type="vanished">Այս ֆայլը արդեն գոյություն ունի, վերագրանցեք, թե ոչ?</translation>
    </message>
    <message>
        <location filename="messagelog/messagelog.cpp" line="2150"/>
        <source>Save as</source>
        <translation>Պահպանել որպես</translation>
    </message>
</context>
<context>
    <name>NoticeWidget</name>
    <message>
        <location filename="childWidget/noticewidget.ui" line="14"/>
        <source>NoticeWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.ui" line="111"/>
        <source>Announcement</source>
        <translation>Հայտարարություն</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.ui" line="204"/>
        <source>Click to Choose Image</source>
        <translation>Սեղմեք, ընտրեք Image</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.ui" line="243"/>
        <source>Please enter the title of the web page</source>
        <translation>Խնդրում ենք մուտքագրել վեբ էջի վերնագիրը</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.ui" line="271"/>
        <source>Please enter the link of the web page</source>
        <translation>Մուտքագրեք վեբ էջի հղումը</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.ui" line="388"/>
        <source>Send</source>
        <translation>Ուղարկել</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="29"/>
        <source>Please enter the title of the image</source>
        <translation>Խնդրում ենք մուտքագրել պատկերի անվանումը</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="30"/>
        <location filename="childWidget/noticewidget.cpp" line="35"/>
        <source>微软雅黑</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="77"/>
        <source>Open</source>
        <translation>Բացեք</translation>
    </message>
    <message>
        <source>Image File(*.bmp;*.jpeg;*.jpg;*.png;*.gif)</source>
        <translation type="vanished">պատկերի ֆայլը(*.bmp;*.jpeg;*.jpg;*.png;*.gif)</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="78"/>
        <source>Image File</source>
        <translation>պատկերի ֆայլը</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="137"/>
        <location filename="childWidget/noticewidget.cpp" line="145"/>
        <location filename="childWidget/noticewidget.cpp" line="150"/>
        <location filename="childWidget/noticewidget.cpp" line="155"/>
        <source>Notice</source>
        <translation>Ծանուցում</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="137"/>
        <source>Please Choose Image!</source>
        <translation>Խնդրում ենք ընտրել նկարը!</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="145"/>
        <source>Please enter the title of the image！</source>
        <translation>Խնդրում ենք մուտքագրել նկարը!</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="150"/>
        <source>Please enter the title of web page!</source>
        <translation>Խնդրում ենք մուտքագրել վեբ էջի վերնագիր!</translation>
    </message>
    <message>
        <location filename="childWidget/noticewidget.cpp" line="155"/>
        <source>Please enter the URL！</source>
        <translation>Խնդրում ենք մուտքագրել URL!</translation>
    </message>
</context>
<context>
    <name>OpenLetterWidget</name>
    <message>
        <location filename="secretWidget/openletterwidget.ui" line="14"/>
        <source>OpenLetterWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.ui" line="78"/>
        <source>Secret Message</source>
        <translation>Գաղտնի նամակ</translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.ui" line="205"/>
        <source>Enter Password</source>
        <translation>Գաղտնաբառ</translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.ui" line="255"/>
        <source>OK</source>
        <translation>Լավ</translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.cpp" line="78"/>
        <location filename="secretWidget/openletterwidget.cpp" line="85"/>
        <source>Notice</source>
        <translation>Ծանուցում</translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.cpp" line="78"/>
        <source>The password cannot be empty!</source>
        <translation>Գաղտնաբառը չի կարող դատարկ լինել!</translation>
    </message>
    <message>
        <location filename="secretWidget/openletterwidget.cpp" line="85"/>
        <source>Incorrect password!</source>
        <translation>Սխալ գաղտնաբառ!</translation>
    </message>
</context>
<context>
    <name>OpenPacketWidget</name>
    <message>
        <location filename="redPacketWidget/openPack.ui" line="32"/>
        <source>OpenPacket</source>
        <oldsource>OpenPacketWidget</oldsource>
        <translation>Բաց փաթեթը</translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.ui" line="189"/>
        <source>我是头像</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.ui" line="228"/>
        <source>我是昵称</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.ui" line="267"/>
        <location filename="redPacketWidget/openPack.cpp" line="88"/>
        <source>A red packet was sent to you</source>
        <translation>նվեր գումար ուղարկեց ձեզ</translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.ui" line="306"/>
        <source>Best Wishes</source>
        <translation>Հաջողություն</translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.cpp" line="92"/>
        <source>Red racket with a identical amount</source>
        <translatorcomment>նվեր գումար, պատահական քանակությամբ</translatorcomment>
        <translation>նվեր գումար, նույնական գումարով</translation>
    </message>
    <message>
        <location filename="redPacketWidget/openPack.cpp" line="96"/>
        <source>Red packet with a random amount</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PackUnitWidget</name>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="17"/>
        <source>PackUnitWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="63"/>
        <source>PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="91"/>
        <source>11.0000000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="110"/>
        <source>11.000000000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="130"/>
        <source>Available</source>
        <translation>Առկա է</translation>
    </message>
    <message>
        <location filename="redPacketWidget/PackUnitWidget.ui" line="146"/>
        <source>Frozen</source>
        <translation>Սառեցնելը</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="MessageWidget/messageliststore.cpp" line="59"/>
        <location filename="MessageWidget/messageliststore.cpp" line="63"/>
        <source>&lt;font color=&apos;#f7931e&apos; &gt;[I was @]&lt;/font&gt;</source>
        <translation>&lt;font color=&apos;#f7931e&apos; &gt;[Ինչ-որ մեկը @ ինձ]&lt;/font&gt;</translation>
    </message>
</context>
<context>
    <name>QWebEngineViewDelegate</name>
    <message>
        <location filename="qwebengineviewdelegate.ui" line="16"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>RedPackDetail</name>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="32"/>
        <source>redPackDetail</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="100"/>
        <source>Records</source>
        <translation>Ձայնագրություններ</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="138"/>
        <source>Details</source>
        <translation>Մանրամասները</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="248"/>
        <source>我是头像</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="302"/>
        <source>我是昵称</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="343"/>
        <source>我是红包备注</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="416"/>
        <source>我是获得的红包金额</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="438"/>
        <source>我是红包单位</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="495"/>
        <source>我是多少个红包</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.ui" line="527"/>
        <source>我是已领取红包</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.cpp" line="74"/>
        <source> red packets </source>
        <translation> նվեր գումար </translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.cpp" line="79"/>
        <source>taken </source>
        <translation>Ստացված </translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.cpp" line="103"/>
        <source>most lucky</source>
        <translation>Լավագույնը</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackDetail.cpp" line="113"/>
        <source>微软雅黑</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>RedPackHistory</name>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="32"/>
        <source>RedPackHistory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="79"/>
        <source>Red Packet History</source>
        <translation>գրառում</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="190"/>
        <location filename="redPacketWidget/RedPackHistory.ui" line="428"/>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="184"/>
        <source>Received</source>
        <translation>Ստացված</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="260"/>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="199"/>
        <source>Sent</source>
        <translation>Ուղարկված է</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="345"/>
        <source>我是头像</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="409"/>
        <source>我是昵称</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="495"/>
        <source>我是红包数量</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="517"/>
        <source>Packets</source>
        <translation>սորտեր</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.ui" line="574"/>
        <source>Tokens received can be transfered into local wallet</source>
        <translation>Ստացված տոկերը կարող է փոխանցվել տեղական դրամապանակին</translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="84"/>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="134"/>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="175"/>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="190"/>
        <source>微软雅黑</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/RedPackHistory.cpp" line="185"/>
        <source>Tokens been deposited in your accounts now, which can be transfered into local wallet</source>
        <translation>Տոկոսները պահվում են ձեր հաշիվներում, որոնք կարող են փոխանցվել տեղական դրամապանակին</translation>
    </message>
</context>
<context>
    <name>SearchList</name>
    <message>
        <location filename="SearchWidget/searchlist.ui" line="16"/>
        <source>SearchList</source>
        <translation></translation>
    </message>
    <message>
        <location filename="SearchWidget/searchlist.cpp" line="120"/>
        <location filename="SearchWidget/searchlist.cpp" line="147"/>
        <location filename="SearchWidget/searchlist.cpp" line="150"/>
        <location filename="SearchWidget/searchlist.cpp" line="167"/>
        <location filename="SearchWidget/searchlist.cpp" line="228"/>
        <location filename="SearchWidget/searchlist.cpp" line="248"/>
        <source>Friends</source>
        <translation>Ընկերներ</translation>
    </message>
    <message>
        <location filename="SearchWidget/searchlist.cpp" line="129"/>
        <location filename="SearchWidget/searchlist.cpp" line="185"/>
        <location filename="SearchWidget/searchlist.cpp" line="188"/>
        <location filename="SearchWidget/searchlist.cpp" line="201"/>
        <source>Group</source>
        <translation>Խումբը</translation>
    </message>
    <message>
        <source>Tribe</source>
        <translation type="vanished">Ցեղ</translation>
    </message>
    <message>
        <location filename="SearchWidget/searchlist.cpp" line="225"/>
        <source>other friends</source>
        <translation>Այլ ընկերներ</translation>
    </message>
</context>
<context>
    <name>SecretFileWidget</name>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="14"/>
        <source>SecretFileWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="105"/>
        <source>Secret File</source>
        <translation>գաղտնի ֆայլը</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="228"/>
        <source>Enter Password</source>
        <translation>գաղտնաբառը</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="271"/>
        <source>Click &quot;add&quot; button to add secret file, double click file to cancel</source>
        <translation>Սեղմեք «ավելացրեք» կոճակը, գաղտնի ֆայլը ավելացնելու համար, կրկնակի սեղմման ֆայլը չեղարկելու համար</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="332"/>
        <source>Add</source>
        <translation>Ավելացնել</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.ui" line="357"/>
        <source>Send</source>
        <translation>Ուղարկել</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="91"/>
        <source>Open File</source>
        <translation>Բացել ֆայլը</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="91"/>
        <source>File (*.*)</source>
        <translation>Ֆայլ (*. *)</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="147"/>
        <location filename="secretWidget/secretfilewidget.cpp" line="151"/>
        <location filename="secretWidget/secretfilewidget.cpp" line="173"/>
        <location filename="secretWidget/secretfilewidget.cpp" line="211"/>
        <location filename="secretWidget/secretfilewidget.cpp" line="247"/>
        <source>Notice</source>
        <translation>Ծանուցում</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="147"/>
        <source>No File Selected!</source>
        <translation>Ընտրված ֆայլ չկա!</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="151"/>
        <source>Please Enter The Password!</source>
        <translation>Մուտքագրեք գաղտնաբառը!</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="162"/>
        <source>Sending……</source>
        <translation>Ուղարկել……</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="173"/>
        <source>File does not exist!</source>
        <translation>Ֆայլը գոյություն չունի!</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="211"/>
        <source>Encryption Failed!</source>
        <translation>Կոդավորման չհաջողվեց!</translation>
    </message>
    <message>
        <location filename="secretWidget/secretfilewidget.cpp" line="247"/>
        <source>Failed to upload secret ile!</source>
        <translation>Չհաջողվեց բեռնել գաղտնի ֆայլը!</translation>
    </message>
</context>
<context>
    <name>SecretImageWidget</name>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="14"/>
        <source>SecretImageWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="93"/>
        <source>Secret Image</source>
        <translation>Գաղտնի նկար</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="210"/>
        <source>Enter Password</source>
        <translation>Մուտքագրեք գաղտնաբառը</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="343"/>
        <source>Click &quot;add&quot; button to add secret image, double click file to cancel</source>
        <translation>Սեղմեք «ավելացրեք» կոճակը, գաղտնի պատկերի, քաղցկեղի կրկնակի սեղմման ֆայլը</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="404"/>
        <source>Add</source>
        <translation>Ավելացնել</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.ui" line="429"/>
        <source>Send</source>
        <translation>Ուղարկել</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="91"/>
        <source>Open Image</source>
        <translation>Բաց նկարը</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="91"/>
        <source>Image File(*.bmp;*.jpeg;*.jpg;*.png)</source>
        <translation>պատկերի ֆայլը(*.bmp;*.jpeg;*.jpg;*.png)</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="110"/>
        <location filename="secretWidget/secretimagewidget.cpp" line="114"/>
        <location filename="secretWidget/secretimagewidget.cpp" line="136"/>
        <location filename="secretWidget/secretimagewidget.cpp" line="172"/>
        <location filename="secretWidget/secretimagewidget.cpp" line="208"/>
        <source>Notice</source>
        <translation>Ծանուցում</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="110"/>
        <source>No Image Selected! </source>
        <translation>Ոչ մի ընտրված նկար չկա! </translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="114"/>
        <source>Please Enter The Password!</source>
        <translation>Մուտքագրեք գաղտնաբառը!</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="125"/>
        <source>Sending……</source>
        <translation>Ուղարկել……</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="136"/>
        <source>Image does not exist!</source>
        <translation>Պատկերը գոյություն չունի!</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="172"/>
        <source>Encryption Failed!</source>
        <translation>Կոդավորման չհաջողվեց!</translation>
    </message>
    <message>
        <location filename="secretWidget/secretimagewidget.cpp" line="208"/>
        <source>Failed to upload secret file!</source>
        <translation>Չհաջողվեց բեռնել գաղտնի ֆայլը!</translation>
    </message>
</context>
<context>
    <name>SecretLetterWidget</name>
    <message>
        <location filename="secretWidget/secretletterwidget.ui" line="14"/>
        <source>SecretLetterWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.ui" line="120"/>
        <source>Secret Message</source>
        <translation>Գաղտնի նամակ</translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.ui" line="237"/>
        <source>Enter Password</source>
        <translation>Գաղտնաբառ</translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.ui" line="259"/>
        <source>Enter Message</source>
        <translation>Մուտքագրեք հաղորդագրություն</translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.ui" line="284"/>
        <source>Send</source>
        <translation>Ուղարկել</translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.cpp" line="86"/>
        <source>Notice</source>
        <translation>Ծանուցում</translation>
    </message>
    <message>
        <location filename="secretWidget/secretletterwidget.cpp" line="86"/>
        <source>The password or message cannot be empty!</source>
        <translation>Գաղտնաբառը կամ հաղորդագրությունը չի կարող դատարկ լինել!</translation>
    </message>
</context>
<context>
    <name>TransAccWidget</name>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="20"/>
        <source>TransAccWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="71"/>
        <location filename="redPacketWidget/TransAccWidget.ui" line="200"/>
        <location filename="redPacketWidget/TransAccWidget.ui" line="482"/>
        <source>PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="87"/>
        <source>Transfer</source>
        <translation>Փոխանցում</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="168"/>
        <source>Escrow Balance</source>
        <translation>Կառավարվող հաշվեկշիռ</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="253"/>
        <source>Address</source>
        <translation>Հասցե</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="279"/>
        <source>Hosted Address</source>
        <translation>Կառավարվող հասցեն</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="307"/>
        <source>Amount</source>
        <translation>Գումարը</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="338"/>
        <source>Enter Amount</source>
        <translation>Գումարը</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="368"/>
        <source>Payment Address</source>
        <translation>Վճարման հասցեն</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="443"/>
        <source>Cost of Miners</source>
        <translation>Հանքափորների արժեքը</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="469"/>
        <location filename="redPacketWidget/TransAccWidget.ui" line="547"/>
        <source>0.0004</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="573"/>
        <source>0.006</source>
        <translation></translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.ui" line="624"/>
        <source>OK</source>
        <translation>Լավ</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="32"/>
        <source>Wallet</source>
        <translation>Դրամապանակ</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="52"/>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="58"/>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="63"/>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="77"/>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="82"/>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="92"/>
        <source>Notice</source>
        <translation>Ծանուցում</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="52"/>
        <source>Payment Address Cannot be Empty!</source>
        <translation>Վճարման հասցեն չի կարող լինել Դատարկ!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="58"/>
        <source>Amount of Transfer Cannot be Empty!</source>
        <translation>Փոխանցման գումարը չի կարող դատարկ լինել!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="63"/>
        <source>The Amount of Tranfer is Greater Than</source>
        <translation>Փոխանցման գումարը ավելի մեծ է</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="63"/>
        <source>Balance</source>
        <translation>մնացորդը</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="69"/>
        <source>Please Enter Your Login Password:</source>
        <translation>Խնդրում ենք մուտքագրել Ձեր անունը Գաղտնաբառ:</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="77"/>
        <source>Failed to Verify the Login Password!</source>
        <translation>Չհաջողվեց հաստատել գաղտնաբառի գաղտնաբառը!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="82"/>
        <source>Login Password Cannot be Empty!</source>
        <translation>Անուն Գաղտնաբառ չի կարող լինել Դատարկ!</translation>
    </message>
    <message>
        <location filename="redPacketWidget/TransAccWidget.cpp" line="92"/>
        <source>Incorrect Password!</source>
        <translation>Սխալ գաղտնաբառ!</translation>
    </message>
</context>
<context>
    <name>TransmitMessageWidget</name>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="14"/>
        <source>TransmitMessage</source>
        <oldsource>TransmitMessageWidget</oldsource>
        <translation>Հաղորդագրության փոխանցում</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="155"/>
        <source>Send to</source>
        <translation>Ուղարկել</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="230"/>
        <source>Search</source>
        <translation>Որոնում</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="295"/>
        <source>Recently</source>
        <translation>Վերջերս</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="374"/>
        <location filename="childWidget/transmitmessagewidget.cpp" line="494"/>
        <location filename="childWidget/transmitmessagewidget.cpp" line="497"/>
        <source>Friends</source>
        <translation>Ընկերներ</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="408"/>
        <location filename="childWidget/transmitmessagewidget.cpp" line="529"/>
        <location filename="childWidget/transmitmessagewidget.cpp" line="532"/>
        <source>Groups</source>
        <translation>Խմբեր</translation>
    </message>
    <message>
        <source>Tribes</source>
        <translation type="vanished">Ցեղեր</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="513"/>
        <source>OK</source>
        <translation>Լավ</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.ui" line="544"/>
        <source>Cancel</source>
        <translation>Չեղարկել</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.cpp" line="390"/>
        <source>Notice</source>
        <translation>Ծանուցում</translation>
    </message>
    <message>
        <location filename="childWidget/transmitmessagewidget.cpp" line="390"/>
        <source>Please choose a friend or a group</source>
        <translation>Խնդրում ենք ընտրել ընկեր կամ խմբի</translation>
    </message>
    <message>
        <source>Please choose a friend or a tribe</source>
        <translation type="vanished">Խնդրում ենք ընտրել ընկեր կամ ցեղ</translation>
    </message>
</context>
</TS>
