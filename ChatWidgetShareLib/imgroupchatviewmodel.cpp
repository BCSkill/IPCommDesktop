﻿#include "imgroupchatviewmodel.h"
#include "imgroupchatview.h"
#include "qwebengineviewdelegate.h"
#include "childWidget/expresswidget.h"
#include "common.h"
#include "imdatamanagersharelib.h"
#include "imbuddy.h"
#include "imdatabaseoperainfo.h"
#include "chatdatamanager.h"
#include "immainwidget.h"
#include "SearchWidget/atwidget.h"
#include <QTextEdit>
#include <QTextBlock>

extern IMDataManagerShareLib *gDataManager;
extern IMDataBaseOperaInfo *gDataBaseOpera;


IMGroupChatViewModel::IMGroupChatViewModel(QObject *parent) : QObject(parent)
, m_groupMemberCount(0)
{
	m_view = reinterpret_cast<IMGroupChatView*>(parent);
}

void IMGroupChatViewModel::init()
{

}
void IMGroupChatViewModel::runJs(const QString &scriptSource)
{
	m_view->getWebView()->page()->runJavaScript(scriptSource);
}


int IMGroupChatViewModel::getGroupMemberCount()
{
	return m_groupMemberCount;
}

QString IMGroupChatViewModel::getGroupId()
{
	return m_view->getGroupId();
}

void IMGroupChatViewModel::updateGroupMemberCount(int member_count)
{
	m_groupMemberCount = member_count;
	m_view->doUpdateGroupMemberCount(member_count);
}

void IMGroupChatViewModel::removeMemberByUserId(QString strUserId)
{
	m_view->removeMemberByUserId(strUserId);
}

void IMGroupChatViewModel::insertItem(QString strUserID, QString strPicPath, QString strNickName, QString adminPicPath, bool bHasNote)
{
	m_view->insertItem(strUserID, strPicPath, strNickName, adminPicPath, bHasNote);
}

void IMGroupChatViewModel::updateGroupBuddyImagePath(int userID, QString imagePath)
{
	m_view->updateGroupBuddyImagePath(userID, imagePath);
}
void IMGroupChatViewModel::setNoSpeak(bool bEnable)
{
	if (bEnable)
	{
		UserInfo info = gDataManager->getUserInfo();
		BuddyInfo user = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(getGroupId(), QString::number(info.nUserID));
		if (user.nUserType == 0) //0是普通群员。
		{
			m_view->setNoSpeak(true);
		}
	}
	else
	{
		m_view->setNoSpeak(false);
	}

}

void IMGroupChatViewModel::insertGroupUserList()
{
	m_view->insertGroupUserList();
}

void IMGroupChatViewModel::startThread()
{
	m_view->startThread();
}

void IMGroupChatViewModel::showByChatId(bool isNew)
{
	if (isNew)
	{
		//由于webengineview在不可见状态时并不渲染，导致残留上一个页面影响，所以此处对于新打开的页面改用延迟渲染
		if (!ChatDataManager::self()->m_isFirstShowChat)
		{
			QWebEngineView* view = m_view->getWebView()->engine();
			view->setParent(IMMainWidget::self());

			view->resize(1, 1);
			view->move(0, 0);
			view->raise();
			view->show();

			m_view->getWebView()->switchChatIFrame(false);
		}

		QTimer::singleShot(800, this, [=] {m_view->getWebView()->switchChatIFrame(); });
	}
	else
	{
		m_view->getWebView()->switchChatIFrame();
	}

	ChatDataManager::self()->m_isFirstShowChat = false;
}

void IMGroupChatViewModel::insertImageToTextEditSend(QString strPath)
{
	QFile file(strPath);
	if (file.open(QIODevice::ReadOnly))
	{
		QByteArray byteArray = file.readAll();
		file.close();
		QImage image = QImage::fromData(byteArray);
		if (!image.isNull())
		{
			QTextImageFormat imageFormat;

			if (image.width() > 100 || image.height() > 100)
			{
				if (image.width() > image.height())
				{
					imageFormat.setWidth(100);
				}
				else
				{
					imageFormat.setHeight(100);
				}
			}

			imageFormat.setName(strPath);
			m_view->getTextEditSend()->textCursor().insertImage(imageFormat);
		}
	}
}

QString IMGroupChatViewModel::getDraft()
{
	QString saveText = "";
	QString strText = m_view->getTextEditSend()->toPlainText();
	if (!strText.isEmpty())
	{
		QTextDocument *document = m_view->getTextEditSend()->document();
#ifdef Q_OS_WIN
		QTextBlock &currentBlock = document->begin();
#else
		QTextBlock currentBlock = document->begin();
#endif
		QTextBlock::iterator it;

		while (true)
		{
			for (it = currentBlock.begin(); !(it.atEnd());)
			{
				QTextFragment currentFragment = it.fragment();
				QTextImageFormat newImageFormat = currentFragment.charFormat().toImageFormat();
				if (newImageFormat.isValid()) {
					// 判断出这个fragment为image
					++it;

					QString name = newImageFormat.name();
					//at字段。
					if (m_view->getAtWidget()->isAtGroupBuddyFormat(name))
					{
						saveText += "@";
						QFileInfo imageInfo(name);
						QString strId = imageInfo.baseName().remove("at");
						int buddyID = strId.toInt();

						//拼接@的字符串
						BuddyInfo buddy = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(getGroupId(), strId);
						if (buddy.strNote.isEmpty())
							saveText += buddy.strNickName;
						else
							saveText += buddy.strNote;
					}
					else
					{
						//判断是不是表情。
						if (name.startsWith(":/expression/Resources"))   //表情。
						{
							int len = currentFragment.length();
							for (int i = 0; i < len; i++)//相同的多个表情会导致len>1，原因不明
							{
								QString strImgPath = name.remove(":/expression/Resources");
								QString str_img_description = ExpressWidget::GetDescriptionByImagePath(strImgPath);
								saveText += str_img_description;
							}
						}
						else
						{
							saveText += tr("[Image]");
						}
					}
					continue;
				}
				if (currentFragment.isValid())
				{
					++it;
					QString text = currentFragment.text();
					saveText += text;
				}
			}

			currentBlock = currentBlock.next();
			if (!currentBlock.isValid())
			{
				break;
			}
		}
	}
	return saveText;
}

QVariant IMGroupChatViewModel::getValueByIndexAndType(int index, GroupUserListModel::GroupUserListItemRoles roleType)
{
	QModelIndex item = m_view->getGroupUserList()->item(index);

	return m_view->getGroupUserList()->data(item, roleType);
}