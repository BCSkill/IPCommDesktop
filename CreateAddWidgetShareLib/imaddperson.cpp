﻿#include "imaddperson.h"
#include "ui_imaddperson.h"
#include <QFile>
#include <QtMath>
#include "QStringLiteralBak.h"
#include "settingsmanager.h"

#ifdef Q_OS_LINUX
    #include <qdesktopwidget.h>
    #include <QStyle>
#endif

extern QString gThemeStyle;

IMAddPerson::IMAddPerson(QWidget *parent)
	: BaseWindow(parent)
{
	ui = new Ui::IMAddPerson();

	ui->setupUi(this);
	initTitleBar(); 
	this->setAttribute(Qt::WA_DeleteOnClose);
	this->setWindowFlags(Qt::FramelessWindowHint);		// 去掉标题栏 且不能移动
	setWindowIcon(QIcon(":/Login/Resources/login/system.ico"));

	QFile file(":/QSS/Resources/QSS/CreateAddWidgetShareLib/imaddperson.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

#ifdef Q_OS_WIN
 	shadow = new Shadow();
#endif
 #ifdef Q_OS_WIN
 	connect(m_titleBar->getCloseBtn(), SIGNAL(clicked()), shadow, SLOT(hide()));
 #endif

	//
	ui->mPButtonFindPerson->setChecked(true);
	connect(ui->mPButtonFindPerson, SIGNAL(clicked()), this, SLOT(doClickSearchPerson()));
	connect(ui->mPButtonFindGroup, SIGNAL(clicked()), this, SLOT(doClickSearchGroup()));
	connect(ui->mPButtonFindAppNo, SIGNAL(clicked()), this, SLOT(doClickSearchAppNo()));

	mSearchPerson = new IMSearchPerson(this);
	ui->stackedWidget->addWidget(mSearchPerson);

	mSearchGroup = new IMSearchGroup(this);
	ui->stackedWidget->addWidget(mSearchGroup);

	connect(m_titleBar, SIGNAL(sigButtonCloseClicked()), this, SLOT(doClickClose()));

	ui->mPButtonFindAppNo->hide();
	mSearchPerson->setItemFocus();
#ifdef Q_OS_LINUX
	setLinuxCenter();
#endif
}

IMAddPerson::~IMAddPerson()
{
#ifdef Q_OS_WIN
 	if (shadow)
 		delete shadow;
#endif
	if (mSearchPerson != NULL)
	{
		mSearchPerson->close();
	}

	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

void IMAddPerson::initTitleBar()
{
	m_titleBar->setTitleIcon(":/Login/Resources/login/system.ico");
	m_titleBar->setTitleContent(tr("Add friends"),10);

	if (gThemeStyle == "Blue")
	{
		m_titleBar->setBackgroundColor(24, 50, 87);
		m_titleBar->setTitleContentBKColor("color:white;font-family:\"Microsoft YaHei\"");
	}
	if (gThemeStyle == "White")
	{
		m_titleBar->setBackgroundColor(0, 102, 203);
		m_titleBar->setTitleContentBKColor("color:white;font-family:\"Microsoft YaHei\"");
	}

	m_titleBar->setButtonType(ONLY_CLOSE_BUTTON);
	m_titleBar->setTitleWidth(this->width());
}

void IMAddPerson::doClickSearchPerson()
{
	ui->stackedWidget->setCurrentIndex(0);
	mSearchPerson->setItemFocus();
}

void IMAddPerson::doClickSearchGroup()
{
	ui->stackedWidget->setCurrentIndex(1);
	mSearchGroup->setItemFocus();
}

void IMAddPerson::doClickSearchAppNo()
{
// 	ui->mPButtonFindAppNo->setStyleSheet("border-image:url(:/addPerson/Resources/add/nav_btnbg.png) 0 0 0 0 stretch stretch;");
// 	ui->mPButtonFindPerson->setStyleSheet("border-image:url() 0 0 0 0 stretch stretch;");
// 	ui->mPButtonFindGroup->setStyleSheet("border-image:url() 0 0 0 0 stretch stretch;");
}

void IMAddPerson::doClickClose()
{
	if (mSearchPerson != NULL)
	{
		mSearchPerson->ClearList();
	}
}

void IMAddPerson::changeEvent(QEvent * event)
{
#ifdef Q_OS_WIN
	if (event->type() == QEvent::WindowStateChange)
	{
		if (this->windowState() == Qt::WindowMaximized)
			shadow->hide();
		if (this->windowState() == Qt::WindowMinimized)
			shadow->hide();

		if (this->windowState() == Qt::WindowNoState)
			shadow->show();
	}
#endif
	QWidget::changeEvent(event);
}

void IMAddPerson::moveEvent(QMoveEvent *event)
{
#ifdef Q_OS_WIN
	shadow->move(event->pos().x() - 10, event->pos().y() - 10);
#endif
}

void IMAddPerson::resizeEvent(QResizeEvent* event)
{
#ifdef Q_OS_WIN
	shadow->resize(event->size().width() + 20, event->size().height() + 20);
#endif
}

void IMAddPerson::show()
{
	QWidget::show();
#ifdef Q_OS_WIN
	shadow->resize(width() + 20, height() + 20);
	shadow->move(x() - 10, y() - 10);
	shadow->show();
#endif
#ifdef Q_OS_WIN
	// 窗口被激活了，强制重绘  
	InvalidateRect((HWND)this->winId(), nullptr, FALSE);
#endif
}
void IMAddPerson::closeEvent(QCloseEvent * event)
{
	emit sigAddPersonClose();
	QWidget::closeEvent(event);
}

#ifdef Q_OS_LINUX
void IMAddPerson::setLinuxCenter()
{
    window()->setGeometry
    (
            QStyle::alignedRect
            (
                    Qt::LeftToRight,
                    Qt::AlignCenter,
                    window()->size(),
                    qApp->desktop()->availableGeometry()
            )
    );
}
#endif
