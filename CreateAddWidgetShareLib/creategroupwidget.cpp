﻿#include "creategroupwidget.h"
#include "ui_creategroupwidget.h"
#ifdef Q_OS_MAC
#include "mycommonstyle.h"
#endif
#include "QStringLiteralBak.h"

#ifdef Q_OS_LINUX
    #include <qdesktopwidget.h>
#endif

extern QString gThemeStyle;

CreateGroupWidget::CreateGroupWidget(QWidget *parent)
	: BaseWindow(parent), 
	m_nHttpRequestType(http_request_type::not_mine_request)
{
	ui = new Ui::CreateGroupWidget();

	ui->setupUi(this);
#ifdef Q_OS_WIN
	shadow = new Shadow();
    connect(ui->m_btnCancel, SIGNAL(clicked()), shadow, SLOT(hide()));
#endif
	initTitleBar(); 
	initChildWidgetLayout(); 
	initContactsList(); 
	initClassMember(); 
	this->setWindowFlags(Qt::FramelessWindowHint);		// 去掉标题栏 且不能移动
	this->setAttribute(Qt::WA_DeleteOnClose);			// 设置在关闭该窗口时 直接 delete该窗口的对象 即 close()走析构函数
	setWindowIcon(QIcon(":/Login/Resources/login/system.ico"));

	QFile file(":/QSS/Resources/QSS/CreateAddWidgetShareLib/creategroupwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->m_btnCancel, SIGNAL(clicked()), this, SIGNAL(sigClose()));   // 取消按钮响应
	connect(ui->m_btnConfirm, SIGNAL(clicked()), this, SLOT(OnConfirmBtnClicked())); // 确认按钮响应

	connect(ui->m_lineSearchText, SIGNAL(textEdited(QString)), this, SLOT(slotSearch(QString)));
	connect(ui->m_listSearch, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(slotClickSearchList(QListWidgetItem *)));
#ifdef Q_OS_MAC
    ui->m_lineGroupName->setStyle(new MyProxyStyle);
    ui->m_lineSearchText->setStyle(new MyProxyStyle);
    ui->m_listContactsList->setStyle(new MyProxyStyle);
    ui->m_listGroupMembers->setStyle(new MyProxyStyle);
    ui->m_listSearch->setStyle(new MyProxyStyle);
#endif
	ui->m_lineSearchText->installEventFilter(this);

	ui->m_listSearch->hide();
	ui->m_lineSearchText->setFocus();

	ui->m_lineGroupName->setMaxLength(20);


#ifdef  Q_OS_LINUX
	setLinuxCenter();
#endif
}
CreateGroupWidget::~CreateGroupWidget()
{
	cleanList(ui->m_listContactsList); 
	cleanList(ui->m_listGroupMembers); 
#ifdef Q_OS_WIN
	if (shadow)
		delete shadow;
#endif

	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

void CreateGroupWidget::changeEvent(QEvent * event)
{
	if (event->type() == QEvent::WindowStateChange)
	{
#ifdef Q_OS_WIN
		if (this->windowState() == Qt::WindowMaximized)
			shadow->hide();
		if (this->windowState() == Qt::WindowMinimized)
			shadow->hide();

		if (this->windowState() == Qt::WindowNoState)
			shadow->show();
#endif
	}

	QWidget::changeEvent(event);
}


void CreateGroupWidget::moveEvent(QMoveEvent *event)
{
#ifdef Q_OS_WIN
	shadow->move(event->pos().x() - 10, event->pos().y() - 10);
#endif
}

void CreateGroupWidget::resizeEvent(QResizeEvent* event)
{
#ifdef Q_OS_WIN
	shadow->resize(event->size().width() + 20, event->size().height() + 20);
#endif
}

void CreateGroupWidget::show()
{
	QWidget::show();
#ifdef Q_OS_WIN
	shadow->resize(width() + 20, height() + 20);
	shadow->move(x() - 10, y() - 10);
	shadow->show();
#endif
#ifdef Q_OS_WIN
	// 窗口被激活了，强制重绘  
	InvalidateRect((HWND)this->winId(), nullptr, FALSE);
#endif
}

// 初始化本类成员
void CreateGroupWidget::initClassMember()
{
	m_stUserInfo = gDataManager->getUserInfo();
	m_stGroupInfo.createTime = ""; 
	m_stGroupInfo.createUserId = ""; 
	m_stGroupInfo.groupId = ""; 
	m_stGroupInfo.groupName = ""; 
	m_stGroupInfo.groupHttpHeadImage = ""; 
	m_stGroupInfo.groupLoacalHeadImage = ""; 
	m_stGroupInfo.noSpeak = 0;
	m_stGroupInfo.msgPrompt = 0;
}

// 清空一个list
void CreateGroupWidget::cleanList(CFriendListWidgetBase* list)
{
	QListWidgetItem* cur_item = NULL;
	CFrientStyleWidget* buddy = NULL;
	for (unsigned int i = 0; i < list->count(); i++)
	{
		cur_item = list->item(i);
		if (cur_item)
		{
			list->removeItemWidget(cur_item); 
			delete cur_item; 
			--i; 
		}
	}
}

// 初始化标题栏
void CreateGroupWidget::initTitleBar()
{
	m_titleBar->setTitleIcon(":/Login/Resources/login/logo.png");
	m_titleBar->setTitleContent(tr("Create group"), 10);
	m_titleBar->setBackgroundColor(0, 102, 203);//0, 102, 203
	if (gThemeStyle == "Blue")
	{
		m_titleBar->setBackgroundColor(24, 50, 87);
		m_titleBar->setTitleContentBKColor("color:white;font-family:\"Microsoft YaHei\"");
	}
	if (gThemeStyle == "White")
	{
		m_titleBar->setBackgroundColor(0, 102, 203);
		m_titleBar->setTitleContentBKColor("color:white;font-family:\"Microsoft YaHei\"");
	}
	m_titleBar->setButtonType(ONLY_CLOSE_BUTTON);
	m_titleBar->setTitleWidth(this->width());
}

// 初始化子控件
// 功能:	1 窗口上1/5部分的label的背景色设置
//		2 去掉两个QListWidget的边框
//		3 在客户区中间部分画一条分割线
void CreateGroupWidget::initChildWidgetLayout()
{	
	// 去掉 QListWidget的边框
	ui->m_listContactsList->setFrameShape(QFrame::NoFrame);
	ui->m_listGroupMembers->setFrameShape(QFrame::NoFrame); 
	ui->m_listSearch->setFrameShape(QFrame::NoFrame);
}

// 初始化 ContactsList 
// 将好友列表插入到 ContactsList
void CreateGroupWidget::initContactsList()
{
	if (gDataBaseOpera)
	{
		QMap<QString, QList<BuddyInfo> > contacts_map = gDataBaseOpera->DB_GetBuddyInfo();
		QMap<QString, QList<BuddyInfo> >::iterator itor = contacts_map.begin();
		for (; itor != contacts_map.end(); ++itor)
		{
			// 创建 buddy_label 的list item   buddy_label -- A B C.....
			QString buddys_label = itor.key();
			if (buddys_label == "~")
				buddys_label = "#";
			if (buddys_label.isEmpty())
				continue;
			QListWidgetItem *newItem = new QListWidgetItem(buddys_label);
			newItem->setSizeHint(QSize(this->width(), 25));
			newItem->setData(Qt::UserRole, buddys_label);
			newItem->setFlags(Qt::ItemFlag::NoItemFlags);			// 给标签设置特殊的flag 供与正常item区别
			ui->m_listContactsList->insertItem(ui->m_listContactsList->count(), newItem);         //加到QListWidget中

			// 将 QList<BuddyInfo> 中的成员加入之后的list item 
			// buddys 为指向 QList<BuddyInfo>的指针
			QList<BuddyInfo>&  buddys = itor.value();
			for (int i = 0; i < buddys.size(); ++i)
			{
				int nFlags = 0;
				QString strAvatar = (buddys[i]).strLocalAvatar;
				QString strNickName = (buddys[i]).strNickName;
				QString strNote = (buddys[i]).strNote;
				QString strBuddyID = QString("%1").arg((buddys[i]).nUserId);

				if (!strNote.isEmpty())
				{
					insertBuddyToList(ui->m_listContactsList, buddys_label, strBuddyID, strAvatar, strNote);
					//ui->m_listContactsList->OnInsertContacts(itor.key(), strBuddyID, strAvatar, strNote); 
				}
				else
				{
					insertBuddyToList(ui->m_listContactsList, buddys_label, strBuddyID, strAvatar, strNickName);
					//ui->m_listContactsList->OnInsertContacts(itor.key(), strBuddyID, strAvatar, strNickName);
				}
			}
		}

		connect(ui->m_listContactsList, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(OnContactsListItemClicked(QListWidgetItem*)));
	}
}


// 向list添加一个被赋予 buddy对象的item
void CreateGroupWidget::insertBuddyToList(CFriendListWidgetBase* list, QString str_data, QString str_buddy_id, QString str_pic_path, QString str_nick_name, int n_height)
{
	CFrientStyleWidget *buddy = new CFrientStyleWidget(); 
	if (ui->m_listContactsList == list)
	{
		buddy->OnInitCreateGroupContactsList(str_buddy_id);
	}		
	else if (ui->m_listGroupMembers == list)
	{
		buddy->OnInitCreateGroupMemberList(str_buddy_id);
		connect(buddy, SIGNAL(sigRemoveBuddyFromCGMemberList(QString)), this, SLOT(OnMemberListItemRemoveBtnClicked(QString)));
	}
	else if (ui->m_listSearch == list)
	{
		buddy->OnInitCreateGroupContactsList(str_buddy_id);
		for (int i = 0; i < ui->m_listGroupMembers->count(); i++)
		{
			QListWidgetItem *item = ui->m_listGroupMembers->item(i);
			CFrientStyleWidget *widget = (CFrientStyleWidget *)ui->m_listGroupMembers->itemWidget(item);

			if (widget)
			{
				if (widget->objectName() == str_buddy_id)
					buddy->changeCGCheckBtnStatus();
			}
		}
	}
		
	buddy->OnSetPicPath(str_pic_path);
	buddy->OnSetNickNameText(str_nick_name);

	QListWidgetItem *newItem = new QListWidgetItem(); //创建一个newItem
	newItem->setSizeHint(QSize(list->width(), n_height)); 
	newItem->setData(Qt::UserRole, str_buddy_id);

	list->insertItem(list->count(), newItem); //将该newItem插入到后面
	list->setItemWidget(newItem, buddy); //将buddy赋给该newItem

	if (list == ui->m_listSearch && list->count() == 1)
	{
		newItem->setSelected(true);
		ui->m_listSearch->setCurrentItem(newItem);
	}
}

// list's item 被点击事件
// 改变 被点击的item的CheckBtn的状态
//	1 状态为选中 -- 添加一个buddy到MemberList
//  2 状态为未选中 -- 从MemberList中根据buddy->objectName()删除一个item
void CreateGroupWidget::OnContactsListItemClicked(QListWidgetItem* the_item)
{
	// 如果item时标签(buddy_label)的话 直接返回 不做任何处理
	if (the_item->flags() == Qt::ItemFlag::NoItemFlags)
		return; 

	CFrientStyleWidget *buddy = (CFrientStyleWidget*)ui->m_listContactsList->itemWidget(the_item);
	if (buddy == NULL)
	{
		qDebug() << "CreateGroupWidget::OnContactsListItemClicked空指针";
		return;
	}
	buddy->changeCGCheckBtnStatus(); 
	const bool btn_is_checked = true; 
	if (buddy->getCGCheckBtnStatus() == btn_is_checked)
	{
		doAddBuddyToGroupMemberList(buddy->objectName());
	}
	else
	{
		doRmBuddyInGroupMemberList(buddy->objectName()); 
	}
	ui->m_lineSearchText->selectAll();
	ui->m_lineSearchText->setFocus();
}

// MemberList's item 上的remove按钮点击事件
void CreateGroupWidget::OnMemberListItemRemoveBtnClicked(const QString& buddy_id)
{
	doRmBuddyInGroupMemberList(buddy_id); 
	CFriendListWidgetBase* list = ui->m_listContactsList; 
	QListWidgetItem* cur_item = NULL;
	CFrientStyleWidget* buddy = NULL;
	for (int i = 0; i < list->count(); i++)
	{
		cur_item = list->item(i);
		if (cur_item)
			buddy = (CFrientStyleWidget*)list->itemWidget(cur_item);
		if (buddy)
		{
			if (buddy->objectName().compare(buddy_id) == 0)
			{
				buddy->changeCGCheckBtnStatus(); 
			}
		}
	}

	if (ui->m_listSearch->isVisible())
	{
		for (int i = 0; i < ui->m_listSearch->count(); i++)
		{
			QListWidgetItem *item = ui->m_listSearch->item(i);
			CFrientStyleWidget *widget = (CFrientStyleWidget*)ui->m_listSearch->itemWidget(item);
			if (widget)
			{
				if (widget->objectName().compare(buddy_id) == 0)
				{
					widget->changeCGCheckBtnStatus();
				}
			}
		}
	}
	ui->m_lineSearchText->selectAll();
	ui->m_lineSearchText->setFocus();
}

// 向 ui->m_listGroupMembers 中添加一个item   设置 item.objectName() = buddy_id
void CreateGroupWidget::doAddBuddyToGroupMemberList(const QString& buddy_id)
{
	if (gDataBaseOpera)
	{
		QMap<QString, QList<BuddyInfo> > contacts_map = gDataBaseOpera->DB_GetBuddyInfo();
		QMap<QString, QList<BuddyInfo> >::iterator itor = contacts_map.begin();
		for (; itor != contacts_map.end(); ++itor)
		{
			// buddys_label -- A B C....
			QString buddys_label = itor.key();
			if (buddys_label == "~")
				buddys_label = "#";

			// buddys 为指向 QList<BuddyInfo>的指针
			QList<BuddyInfo>&  buddys = itor.value();
			for (int i = 0; i < buddys.size(); ++i)
			{
				QString strBuddyID = QString("%1").arg((buddys[i]).nUserId);
				if (strBuddyID.compare(buddy_id) != 0)
					continue; 

				QString strAvatar = (buddys[i]).strLocalAvatar; 
				QString strNickName = (buddys[i]).strNickName; 
				QString strNote = (buddys[i]).strNote; 
				
				if (!strNote.isEmpty())
				{
					insertBuddyToList(ui->m_listGroupMembers, buddys_label, strBuddyID, strAvatar, strNote);
					//ui->m_listContactsList->OnInsertContacts(itor.key(), strBuddyID, strAvatar, strNote); 
				}
				else
				{
					insertBuddyToList(ui->m_listGroupMembers, buddys_label, strBuddyID, strAvatar, strNickName);
					//ui->m_listContactsList->OnInsertContacts(itor.key(), strBuddyID, strAvatar, strNickName);
				}
				m_lstBuddysInfo.append(buddys[i]);        // 将成员添加到QList<BuddyInfo>
			}
		}
	}
	updateGroupMembersCountLabel(); 
}

// 从 ui->m_listGroupMembers 中删除一个item   根据 item.objectName() == buddy_id
void CreateGroupWidget::doRmBuddyInGroupMemberList(const QString& buddy_id)
{
	removeItemFromList(ui->m_listGroupMembers, buddy_id); 
	updateGroupMembersCountLabel(); 
	// 从 QList<BuddyInfo> 中删除对应buddy_id的项
	for (int i = 0; i < m_lstBuddysInfo.count(); ++i)
	{
		QString cur_buddy_id = QString::number(m_lstBuddysInfo.at(i).nUserId); 
		if (cur_buddy_id.compare(buddy_id) == 0)
		{
			m_lstBuddysInfo.removeAt(i); 
			return; 
		}
	}
}


// 从list中删除对应buddy_id的item
void CreateGroupWidget::removeItemFromList(CFriendListWidgetBase* list, const QString& buddy_id)
{
	QListWidgetItem* cur_item = NULL;
	CFrientStyleWidget* buddy = NULL;
	for (unsigned int i = 0; i < list->count(); i++)
	{
		cur_item = list->item(i);
		if (cur_item)
			buddy = (CFrientStyleWidget*)list->itemWidget(cur_item);
		if (buddy)
		{
			if (buddy->objectName().compare(buddy_id) == 0)
			{
				list->removeItemWidget(cur_item);
				delete cur_item;
				break;
			}
		}
	}
}

// 刷新新建部落的成员人数
// 每次有人数变动时 调用该函数
void CreateGroupWidget::updateGroupMembersCountLabel()
{
	QString str_info_show(""); 
	QString str_count = QString::number(ui->m_listGroupMembers->count()); 
	if (str_count.compare("0")==0)
		str_info_show = tr("Please tick the contacts you want to add"); 
	else
		str_info_show = str_count + tr(" contact has been selected"); 
	ui->m_lbShowGroupMembersCount->setText(str_info_show); 
}

// 确定按钮 点击事件
// 判断部落成员人数 如果不小于1人(不包含本人) 创建部落
void CreateGroupWidget::OnConfirmBtnClicked()
{
	if (ui->m_listGroupMembers->count() == 0)
	{
		IMessageBox::tip(this, tr("Warning"), tr("Group members cannot be empty!"));
	}
	else
	{
		ui->m_btnConfirm->setEnabled(false);
		QString str_group_name = ui->m_lineGroupName->text();
		if (str_group_name.isEmpty())
		{
			str_group_name = doCreateGroupName();
		}

		UserInfo user = gDataManager->getUserInfo();

		HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
		connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(OnGetHttpResult(bool, QString)));

		QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/createGroup"
			+ QString("?userId=%1&passWord=%2&groupName=%3").arg(user.nUserID).arg(user.strUserPWD).arg(str_group_name);

		m_nHttpRequestType = http_request_type::create_group;
		http->getHttpRequest(url);
	}
}

QString CreateGroupWidget::doCreateGroupName()
{
	QString groupName = gDataManager->getUserInfo().strUserName;
	CFriendListWidgetBase* group_members = ui->m_listGroupMembers;
	if (group_members)
	{
		for (int i = 0; i < group_members->count(); i++)
		{
			QListWidgetItem* cur_member = group_members->item(i);
			if (cur_member)
			{
				// 在 新建item时将buddy_id作为用户自定义数据保存在 item->setData(Qt::UserRole,buddy_id)
				QString buddy_id = cur_member->data(Qt::UserRole).toString();
				if (!buddy_id.isEmpty())
				{
					BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(buddy_id);
					if (!buddy.strNickName.isEmpty())
					    groupName.append(tr("、") + buddy.strNickName);
				}
			}
		} // 遍历 group_members
	}

	if (groupName.count() > 23)
		groupName = groupName.left(20) + tr("…");

	return groupName;
}

// 拿到创建部落的http结果
void CreateGroupWidget::OnGetHttpResult(bool success, const QString& result)
{
	if (m_nHttpRequestType == http_request_type::not_mine_request)
		return; 
	switch (m_nHttpRequestType)
	{
	case http_request_type::create_group:
	{
		if (success)
		{
			// 创建部落成功
			doParseGroupInfoFromJsonStr(result);			// 解析json 得到部落信息 保存到 m_stGroupInfo
			if (m_stGroupInfo.groupLoacalHeadImage.isEmpty()) // 给部落信息加上默认部落头像
				m_stGroupInfo.groupLoacalHeadImage = ":/GroupChat/Resources/groupchat/group.png";
			doAddBuddysInMemberListToGroup(m_stGroupInfo.groupId);   // 创建部落成功后 向部落添加成员
			return; 
		}
		else
		{
			IMessageBox::tip(this, tr("Warning"), tr("Group creation failed"));
			ui->m_btnConfirm->setEnabled(true);
			return;
		}
		m_nHttpRequestType = http_request_type::not_mine_request;     // 做完处理后改变 m_nHttpRequestType的值 防止其他请求的http结果也被该case处理
	}
		break;
	case http_request_type::add_buddys_to_group:
		if (success)
		{
			//更新数据库
			gDataBaseOpera->DBInsertGroupInfo(m_stGroupInfo);
			// 发送创建部落成功的信号 到 IMChatClient
			emit sigCreateGroupSuccess(m_stGroupInfo.groupId); 
		}
		else
		{
			// 创建部落失败
			QMessageBox::information(this, tr("Warning"), tr("Group creation failed"));
			ui->m_btnConfirm->setEnabled(true);
			return;
		}
		m_nHttpRequestType = http_request_type::not_mine_request; 
		break; 
	default:
		m_nHttpRequestType = http_request_type::not_mine_request; 
		break; 
	}
}

// 从 str_json 解析 部落信息到 m_stGroupInfo
void CreateGroupWidget::doParseGroupInfoFromJsonStr(const QString& str_json)
{
	QString str_value(""); 
	QJsonParseError jsonError;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(str_json.toUtf8(), &jsonError);
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QJsonObject obj = jsonDocument.object();

			if (obj["result"].toString() == "success")
			{
				QJsonValue groupValue = obj.take("group");    // group对应的值是 map
				if (groupValue.isUndefined())
					return; 
				QVariant groupValueVar = groupValue.toVariant(); // 拿到groupValue对应的值
				if (groupValue.isObject())
				{
					QVariantMap groupMap = groupValueVar.toMap();	// 将groupValue的值转为map
					m_stGroupInfo.groupId = groupMap["groupId"].toString(); 
					m_stGroupInfo.groupName = groupMap["groupName"].toString(); 
					m_stGroupInfo.createTime = groupMap["createTime"].toString();
					m_stGroupInfo.createUserId = groupMap["createUserId"].toString(); 
				}			
			}
		}
	}
}

// 将 MemberList 中的所有成员添加到新建的部落中
void CreateGroupWidget::doAddBuddysInMemberListToGroup(const QString& group_id)
{
	UserInfo user = gDataManager->getUserInfo();
	QString str_buddy_ids("");    // 部落成员的userid(对登录用户来说即是buddy_id)  用 ':' 隔开
	QString str_buddy_names;
	QStringList buddy_ids = doGetBuddysIdInMemberList(); 
	for (int i = 0; i < buddy_ids.count(); i++)
	{
		str_buddy_ids += buddy_ids[i]; 
		if (i + 1 < buddy_ids.count())
			str_buddy_ids += ";";

		BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(buddy_ids[i]);
		if (str_buddy_names.isEmpty())
			str_buddy_names.append(buddy.strNickName);
		else
			str_buddy_names.append(tr("、") + buddy.strNickName);
	}

	//发送邀请信息。
	QString content = user.strUserNickName + tr(" invited ") + str_buddy_names + tr(" to join the group");

	QVariantMap map;
	map.insert("type", "notification");
	map.insert("content", content);
	map.insert("inviterID", user.strUserNickName);
	map.insert("invitedID", str_buddy_names);
	map.insert("CMD", "addUserToGroup");

	MessageInfo msgInfo = gSocketMessage->SendTextMessage(user.nUserID, group_id.toInt(), 1, (short)MessageType::Message_NOTIFY, QJsonDocument::fromVariant(map).toJson());

	if (str_buddy_ids.isEmpty())
		qDebug("Create group success, but no buddy add in."); 

	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(OnGetHttpResult(bool, QString)));

	QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/addUserToGroup"
		+ QString("?userId=%1&passWord=%2&addUserId=%3&groupId=%4").arg(user.nUserID).arg(user.strUserPWD).arg(str_buddy_ids).arg(group_id);

	m_nHttpRequestType = http_request_type::add_buddys_to_group;
	http->getHttpRequest(url);
}

QStringList CreateGroupWidget::doGetBuddysIdInMemberList()
{
	QStringList buddy_ids; 
	CFriendListWidgetBase* group_members = ui->m_listGroupMembers; 
	if (group_members)
	{
		QListWidgetItem* cur_member = NULL; 
		for (int i = 0; i < group_members->count(); i++)
		{
			cur_member = group_members->item(i); 
			if (cur_member)
			{
				// 在 新建item时将buddy_id作为用户自定义数据保存在 item->setData(Qt::UserRole,buddy_id)
				QString buddy_id = cur_member->data(Qt::UserRole).toString(); 
				if (!buddy_id.isEmpty())
					buddy_ids.append(buddy_id); 
			}
		} // 遍历 group_members
	}
	return buddy_ids; 
}

void CreateGroupWidget::slotSearch(QString text)
{
	if (text.isEmpty())
	{
		ui->m_listSearch->hide();
	}
	else
	{
		ui->m_listSearch->clear();
		ui->m_listSearch->show();

		text = text.toLower();

		QList<BuddyInfo> buddyList = gDataBaseOpera->DBGetBuddyInfo();
		foreach(BuddyInfo buddy, buddyList)
		{
			if (buddy.BuddyType != 1)
				continue;

			PinYin pinyin;
			if (!buddy.strNote.isEmpty())
			{
				pinyin = getPinYin(buddy.strNote);
				if (buddy.strNote.toLower().contains(text) || pinyin.fullPinYin.contains(text) || pinyin.easyPinYin.contains(text))
				{
					this->insertBuddyToList(ui->m_listSearch, buddy.strPingYin, QString::number(buddy.nUserId), buddy.strLocalAvatar, buddy.strNote);
					continue;
				}
			}

			if (!buddy.strNickName.isEmpty())
			{
				pinyin = getPinYin(buddy.strNickName);
				if (buddy.strNickName.toLower().contains(text) || pinyin.fullPinYin.contains(text) || pinyin.easyPinYin.contains(text))
				{
					this->insertBuddyToList(ui->m_listSearch, buddy.strPingYin, QString::number(buddy.nUserId), buddy.strLocalAvatar, buddy.strNickName);
					continue;
				}
			}
		}
	}
}

void CreateGroupWidget::slotClickSearchList(QListWidgetItem *item)
{
	CFrientStyleWidget *buddy = (CFrientStyleWidget*)ui->m_listSearch->itemWidget(item);
	if (buddy == NULL)
	{
		qDebug() << "CreateGroupWidget::slotClickSearchList空指针";
		return;
	}

	buddy->changeCGCheckBtnStatus();
	const bool btn_is_checked = true;
	if (buddy->getCGCheckBtnStatus() == btn_is_checked)
	{
		doAddBuddyToGroupMemberList(buddy->objectName());
	}
	else
	{
		doRmBuddyInGroupMemberList(buddy->objectName());
	}
	for (int i = 0; i < ui->m_listContactsList->count(); i++)
	{
		QListWidgetItem *buddyItem = ui->m_listContactsList->item(i);
		if (buddyItem)
		{
			if (buddyItem->flags() == Qt::NoItemFlags)
				continue;

			CFrientStyleWidget *buddyWidget = (CFrientStyleWidget*)ui->m_listContactsList->itemWidget(buddyItem);
			if (buddyWidget != NULL)
			{
				if (buddyWidget->objectName() == buddy->objectName())
				{
					buddyWidget->changeCGCheckBtnStatus();
					break;
				}		
			}
		}
	}
	ui->m_lineSearchText->selectAll();
	ui->m_lineSearchText->setFocus();
}

bool CreateGroupWidget::eventFilter(QObject *obj, QEvent *e)
{
	if (obj == ui->m_lineSearchText && e->type() == QEvent::KeyPress)
	{
		QKeyEvent *event = (QKeyEvent *)e;
		if (event->key() == Qt::Key_Up)
		{
			if (ui->m_listSearch->count() > 0)
			{
				QListWidgetItem *item = NULL;
				for (int i = ui->m_listSearch->count() - 1; i >= 0; i--)
				{
					if (ui->m_listSearch->currentRow() < 0)
					{
						item = ui->m_listSearch->item(i);
						break;
					}

					if (ui->m_listSearch->currentRow() > i)
					{
						item = ui->m_listSearch->item(i);
						break;
					}
				}
				if (item)
				{
					item->setSelected(true);
					ui->m_listSearch->setCurrentItem(item);
				}
			}
		}
		if (event->key() == Qt::Key_Down)
		{
			if (ui->m_listSearch->count() > 0)
			{
				QListWidgetItem *item = NULL;
				for (int i = 0; i < ui->m_listSearch->count(); i++)
				{
					if (ui->m_listSearch->currentRow() < 0)
					{
						item = ui->m_listSearch->item(i);
						break;
					}

					if (ui->m_listSearch->currentRow() < i)
					{
						item = ui->m_listSearch->item(i);
						break;
					}
				}
				if (item)
				{
					item->setSelected(true);
					ui->m_listSearch->setCurrentItem(item);
				}
			}
		}

		if (event->key() == Qt::Key_Return)
		{
			QListWidgetItem *item = ui->m_listSearch->currentItem();
			if (item)
				slotClickSearchList(item);
		}
	}

	if (obj == ui->m_lineSearchText && e->type() == QEvent::MouseButtonPress)
	{
		ui->m_lineSearchText->selectAll();
		return true;
	}

	return QWidget::eventFilter(obj, e);
}

void CreateGroupWidget::selectContact(QString buddyID)
{
	for (int i = 0; i < ui->m_listContactsList->count(); i++)
	{
		QListWidgetItem *buddyItem = ui->m_listContactsList->item(i);
		if (buddyItem->flags() == Qt::NoItemFlags)
			continue;
		CFrientStyleWidget *buddyWidget = (CFrientStyleWidget*)ui->m_listContactsList->itemWidget(buddyItem);
		if (buddyWidget != NULL)
		{
			if (buddyWidget->objectName() == buddyID)
			{
				this->OnContactsListItemClicked(buddyItem);
				return;
			}
		}
	}
}
#ifdef Q_OS_LINUX
void CreateGroupWidget::setLinuxCenter()
{
    window()->setGeometry
    (
            QStyle::alignedRect
            (
                    Qt::LeftToRight,
                    Qt::AlignCenter,
                    window()->size(),
                    qApp->desktop()->availableGeometry()
            )
    );
}
#endif