﻿#include "imsearchperson.h"
#include "ui_imsearchperson.h"
#include "messagebox.h"
#include "leavemessagewidget.h"
#include "alphabeticalsortsharedlib.h"
#include "globalmanager.h"
#ifndef Q_OS_WIN
#include "mycommonstyle.h"
#include "inline_mac.h"
#endif
#include "QStringLiteralBak.h"
#include "profilemanager.h"

IMSearchPerson::IMSearchPerson(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::IMSearchPerson();
	ui->setupUi(this);

	m_pLoadWidget = NULL;

	ui->mPButtonSearchPerson->setText(tr("Search"));
	ui->mlineEditSearchPerson->setPlaceholderText(tr("NickName/PhoneNumber"));

	QFile file(":/QSS/Resources/QSS/CreateAddWidgetShareLib/imsearchperson.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->mPButtonSearchPerson, SIGNAL(clicked()), this, SLOT(doClickSearch()));
	connect(ui->mListWidgetSearchPerson, SIGNAL(sigAddPerson(QString,int)), this, SLOT(doAddPerson(QString)));
	//connect(ui->mlineEditSearchPerson, SIGNAL(returnPressed()), this, SLOT(doClickSearch()));
	connect(ui->mListWidgetSearchPerson, SIGNAL(sigOpenProfile(QString)), this, SLOT(slotOpenProfile(QString)));
#ifdef Q_OS_MAC
    ui->mlineEditSearchPerson->setStyle(new MyProxyStyle);
    ui->mListWidgetSearchPerson->setStyle(new MyProxyStyle);
#endif
}

IMSearchPerson::~IMSearchPerson()
{
	if (m_pLoadWidget)
	{
		delete m_pLoadWidget;
		m_pLoadWidget = NULL;
	}

	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

void IMSearchPerson::doClickSearch()
{
	m_pLoadWidget = new QLoadingWidget(this);
	if (m_pLoadWidget)
	{
		m_pLoadWidget->show();
	}
	ui->mListWidgetSearchPerson->clear();
	QString strData = ui->mlineEditSearchPerson->text();
	if (!strData.isEmpty())
	{
		OnSearchPerson(strData);
	}
	else
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("Search content is empty!"));
		//QMessageBox::information(this, tr("查找好友"), tr("输入搜索内容为空!"));
		if (m_pLoadWidget)
		{
			m_pLoadWidget->close();
			m_pLoadWidget = NULL;
		}
	}
}

void IMSearchPerson::OnSearchPerson(QString strData)
{
	mStrSaveSearchContent = strData;

	UserInfo userInfo = gDataManager->getUserInfo();

	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(doHttpSearchPersonResultInfo(bool, QString)));

	QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/user/search"
		+ QString("?userId=%1&passWord=%2&keyWord=%3").arg(userInfo.nUserID).arg(userInfo.strUserPWD).arg(strData);

	http->getHttpRequest(url);
}

void IMSearchPerson::doHttpSearchPersonResultInfo(bool bState, QString strResult)
{
	if (bState)
	{
		ParseSearchPersonResult(strResult);
	}
	else
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("Searching friend failed!"));
		//QMessageBox::information(this, tr("查找好友"), tr("查找好友失败!"));
	}
	if (m_pLoadWidget)
	{
		m_pLoadWidget->close();
		m_pLoadWidget = NULL;
	}

}

void IMSearchPerson::ParseSearchPersonResult(QString strResult)
{
	int nTemp = 0;
	UserInfo userInfo = gDataManager->getUserInfo();
	QString strID = QString("%1").arg(userInfo.nUserID);
	QJsonParseError jsonError;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(strResult.toUtf8(), &jsonError);
	ui->mListWidgetSearchPerson->m_iNum = 0;
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QJsonObject obj = jsonDocument.object();
			if (obj["result"].toString() == "success")
			{
				QVariant strTemp;
				QJsonValue userInfoValue;
				foreach(QString key, obj.keys())
				{
					userInfoValue = obj.take(key);
					strTemp = userInfoValue.toVariant();
					if (userInfoValue.isArray())
					{
						QVariantList userInfoList = strTemp.toList();
						for (int i = 0; i < userInfoList.size(); i++)
						{
							QVariantMap listData = userInfoList.at(i).toMap();
							QString strHttpAvatar = listData["avatar"].toString();
							QString strBuddyID = listData["userId"].toString();
							QString strNickName = listData["nickName"].toString();
							if (strBuddyID == strID)
							{
								//continue;20181124改为能查到自己
							}
							if (mStrSaveSearchContent == strBuddyID)
							{
								//QString strTemp = QString("%1(%2)").arg(strNickName).arg(strBuddyID);
								QString strTemp = QString("%1").arg(strNickName);
								ui->mListWidgetSearchPerson->OnInsertSearchBuddyList(strBuddyID, strHttpAvatar, strTemp);
								nTemp++;
							}
							else //if(mStrSaveSearchContent == strNickName)20181128 改为模糊查找
							{
								ui->mListWidgetSearchPerson->OnInsertSearchBuddyList(strBuddyID, strHttpAvatar, strNickName);
								nTemp++;
							}
						}
					}
				}
			}
			else
			{
				IMessageBox::tip(NULL, tr("Notice"), tr("Failed to get user information!"));
				//QMessageBox::information(this, tr("查找"), tr("解析好友信息失败!"));
			}
		}
	}
	if (nTemp == 0)
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("No matching users!"));
		//QMessageBox::information(this, tr("查找"), tr("没有找到符合搜索条件的用户!"));
	}
}

void IMSearchPerson::doAddPerson(QString strBuddyID)
{
	if (QString::number(gDataManager->getUserInfo().nUserID) == strBuddyID)
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("Can't add yourself as a friend!"));
		return;
	}
	/*首先判断是否有好友*/
	if (gDataManager)
	{
		if (!gDataBaseOpera->DBJudgeFriendIsHaveByID(strBuddyID))
		{
			m_strBuddyId = strBuddyID;

			LeaveMessageWidget* MegWidget = new LeaveMessageWidget(this);
			MegWidget->OnInitBuddyInfo(strBuddyID);
			connect(MegWidget, SIGNAL(sigApplySuccessed()), this, SLOT(slotApplySuccessed()));
			connect(MegWidget, SIGNAL(sigApplyFailed()), this, SLOT(slotApplyFailed()));
			MegWidget->show();
// 			HttpNetWork::HttpNetWorkShareLib *PersonInfo = new HttpNetWork::HttpNetWorkShareLib();
// 			connect(PersonInfo, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotTempFriend(bool, QString)));
// 			AppConfig configInfo = gDataManager->getAppConfigInfo();
// 			QString strRequest = configInfo.MessageServerAddress + "/IMServer/user/getOtherUserByUserId?otherUserId=" + strBuddyID;
// 			PersonInfo->getHttpRequest(strRequest);
		}
		else
		{
			IMessageBox::tip(NULL, tr("Notice"), tr("This user is already your friend!"));
			//QMessageBox::information(this, tr("请求好友"), tr("已经加入该好友!"));
		}
	}
}

void IMSearchPerson::doHttpAddPersonResultInfo(bool bResult,QString strResult)
{
	QJsonParseError jsonError;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(strResult.toUtf8(), &jsonError);
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QJsonObject obj = jsonDocument.object();
			if (obj["result"].toString() == "success")
			{
				IMessageBox::tip(NULL, tr("Notice"), tr("Friend apply succeed!"));
				//QMessageBox::information(this, tr("请求好友"), tr("请求好友成功!"));
				return;
			}
		}
	}
	IMessageBox::tip(NULL, tr("Notice"), tr("Friend apply failed!"));
	//QMessageBox::information(this, tr("请求好友"), tr("请求好友失败!"));
}

void IMSearchPerson::ClearList()
{
	ui->mListWidgetSearchPerson->clear();
	ui->mlineEditSearchPerson->clear();
}

void IMSearchPerson::setItemFocus()
{
	ui->mlineEditSearchPerson->setFocus();
}

void IMSearchPerson::slotDoAddPerson(QString strMessage,QString strOtherName)
{
	UserInfo userInfo = gDataManager->getUserInfo();

	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(doHttpAddPersonResultInfo(bool, QString)));

	QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/user/applyAddFriend"
		+ QString("?userId=%1&passWord=%2&friendUserId=%3&leaveMessage=%4").arg(userInfo.nUserID).arg(userInfo.strUserPWD).arg(m_strBuddyId).arg(strMessage);

	if (strOtherName != "")
	{
		m_stTmpInfo.strNote = strOtherName;
		gDataBaseOpera->DBInsertBuddyInfo(m_stTmpInfo);
	}

	http->getHttpRequest(url);
}

void IMSearchPerson::slotTempFriend(bool bResult, QString result)
{
	if (bResult)
	{
		QJsonParseError jsonError;
		BuddyInfo buddyInfo;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(result.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QVariantMap result = jsonDocument.toVariant().toMap();
				if (result["result"].toString() == "success")
				{
					QVariantMap usrInfo = result["user"].toMap();
					if (!usrInfo.isEmpty())
					{
						QString strPinYin;
						buddyInfo.strHttpAvatar = usrInfo["avatar"].toString();
						buddyInfo.strEmail = usrInfo["email"].toString();
						buddyInfo.strMobilePhone = usrInfo["mobilePhone"].toString();
						buddyInfo.strNickName = usrInfo["nickName"].toString();
						buddyInfo.strNote = usrInfo["note"].toString();
						buddyInfo.strPhone = usrInfo["phone"].toString();
						buddyInfo.strSex = usrInfo["sex"].toString();
						buddyInfo.strSign = usrInfo["sign"].toString();
						buddyInfo.nUserId = usrInfo["userId"].toInt();
						buddyInfo.strUserName = usrInfo["userName"].toString();
						buddyInfo.nUserType = usrInfo["userType"].toInt();
						buddyInfo.disableStrangers = usrInfo["disableStrangers"].toInt();
						buddyInfo.BuddyType = 0;
						buddyInfo.strLocalAvatar = gSettingsManager->getUserPath() + "/resource/header/" + usrInfo["userId"].toString() + ".jpg";
						//if (buddyInfo.strSex == "F")//默认头像
						//{
						//	buddyInfo.strDefaultAvatar = QString(":/IMChatClient/Resources/imchatclient/female.png");
						//}
						//else
						//{
						//	buddyInfo.strDefaultAvatar = QString(":/PerChat/Resources/person/temp.png");
						//}
						buddyInfo.strDefaultAvatar = QString(":/PerChat/Resources/person/temp.png");
						//根据名称获取首字母
						AlphabeticalSortSharedLib mAlphabeticalSort;
						if (!buddyInfo.strNote.isEmpty())
							strPinYin = mAlphabeticalSort.GetChineseSpell(buddyInfo.strNote);
						else
							strPinYin = mAlphabeticalSort.GetChineseSpell(buddyInfo.strNickName);
						buddyInfo.strPingYin = strPinYin;
						if (gDataBaseOpera)
						{
							gDataBaseOpera->DBInsertBuddyInfo(buddyInfo);
						}
						m_stTmpInfo = buddyInfo;
						LeaveMessageWidget* MegWidget = new LeaveMessageWidget(this);
						//MegWidget->OnInitBuddyInfo(buddyInfo);
						connect(MegWidget, SIGNAL(sigLeaveMessage(QString, QString)), this, SLOT(slotDoAddPerson(QString, QString)));
						MegWidget->show();
						return;
					}
				}
			}
		}
	}
}

QString IMSearchPerson::GetExeDir()
{
#ifdef Q_OS_WIN
    return QDir::currentPath();
#else
	return getResourcePath();
#endif
}

void IMSearchPerson::slotApplySuccessed()
{
	//IMessageBox::tip(this, tr("提示"), tr("请求好友成功！"));
}

void IMSearchPerson::slotApplyFailed()
{
	IMessageBox::tip(NULL, tr("Notice"), tr("Network anomaly!"));
}

void IMSearchPerson::slotOpenProfile(QString strid)
{
	emit profilemanager::getInstance()->sigCreatePerFile(strid);
}

void IMSearchPerson::keyPressEvent(QKeyEvent * event)
{
	if (event->key() == Qt::Key_Return)
	{
		doClickSearch();
	}
	return QWidget::keyPressEvent(event);
}