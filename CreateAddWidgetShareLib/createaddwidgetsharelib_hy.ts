<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hy_AM">
<context>
    <name>CreateGroupWidget</name>
    <message>
        <location filename="creategroupwidget.ui" line="20"/>
        <location filename="creategroupwidget.cpp" line="161"/>
        <source>Create group</source>
        <oldsource>Create tribe</oldsource>
        <translation>Ստեղծեք խումբ</translation>
    </message>
    <message>
        <source>the name of the tribe</source>
        <translation type="vanished">ցեղի անունը</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="62"/>
        <source>the name of the group</source>
        <translation>Խմբի անունը</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="84"/>
        <location filename="creategroupwidget.cpp" line="446"/>
        <source>Please tick the contacts you want to add</source>
        <translation>Խնդրում ենք ստուգել կոնտակտները</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="112"/>
        <source>Search for contacts</source>
        <translation>Կոնտակտների որոնում</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="174"/>
        <source>Cancel</source>
        <translation>Չեղարկել</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="199"/>
        <source>OK</source>
        <translation>լավ</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="240"/>
        <source>Group name:</source>
        <translation>Խմբի անունը:</translation>
    </message>
    <message>
        <source>Tribal name:</source>
        <translation type="vanished">Ցեղի անունը:</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="448"/>
        <source> contact has been selected</source>
        <translation> կոնտակտը ընտրվել է</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="458"/>
        <location filename="creategroupwidget.cpp" line="531"/>
        <location filename="creategroupwidget.cpp" line="549"/>
        <source>Warning</source>
        <translation>Ուշադրություն</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="458"/>
        <source>Group members cannot be empty!</source>
        <translation>Խմբի անդամները չեն կարող դատարկ լինել:</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="531"/>
        <location filename="creategroupwidget.cpp" line="549"/>
        <source>Group creation failed</source>
        <translation>Խմբի ստեղծումը ձախողվեց</translation>
    </message>
    <message>
        <source>Tribal members cannot be empty!</source>
        <translation type="vanished">Ցեղի անդամները չեն կարող դատարկ լինել!</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="499"/>
        <location filename="creategroupwidget.cpp" line="609"/>
        <source>、</source>
        <translation></translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="506"/>
        <source>…</source>
        <translation></translation>
    </message>
    <message>
        <source>Tribal creation failed</source>
        <translation type="vanished">Ցեղի ստեղծումը ձախողվեց</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="613"/>
        <source> invited </source>
        <translation> հրավիրված </translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="613"/>
        <source> to join the group</source>
        <translation>Միանալ խմբին</translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished"> միանալ ցեղին</translation>
    </message>
</context>
<context>
    <name>IMAddPerson</name>
    <message>
        <location filename="imaddperson.ui" line="14"/>
        <location filename="imaddperson.cpp" line="81"/>
        <source>Add friends</source>
        <translation>Ընկերներ ավելացրեք</translation>
    </message>
    <message>
        <location filename="imaddperson.ui" line="117"/>
        <source>Find friend</source>
        <translation>Օգտվողը</translation>
    </message>
    <message>
        <location filename="imaddperson.ui" line="170"/>
        <source>Find group</source>
        <translation>Գտնել խումբը</translation>
    </message>
    <message>
        <source>Find tribe</source>
        <translation type="vanished">Ցեղ</translation>
    </message>
    <message>
        <location filename="imaddperson.ui" line="32"/>
        <source>Application number
service number</source>
        <translation>Դիմումի համարը
սպասարկման համարը</translation>
    </message>
</context>
<context>
    <name>IMSearchGroup</name>
    <message>
        <location filename="imsearchgroup.ui" line="14"/>
        <source>IMSearchGroup</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="21"/>
        <source>Search</source>
        <translation>Որոնում</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="22"/>
        <source>Group ID/Group Name</source>
        <oldsource>Tribe ID/Tribe Name</oldsource>
        <translation>Խմբի ID / խմբի անունը</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="71"/>
        <location filename="imsearchgroup.cpp" line="104"/>
        <location filename="imsearchgroup.cpp" line="179"/>
        <location filename="imsearchgroup.cpp" line="184"/>
        <location filename="imsearchgroup.cpp" line="189"/>
        <location filename="imsearchgroup.cpp" line="194"/>
        <location filename="imsearchgroup.cpp" line="263"/>
        <location filename="imsearchgroup.cpp" line="282"/>
        <location filename="imsearchgroup.cpp" line="302"/>
        <location filename="imsearchgroup.cpp" line="317"/>
        <source>Notice</source>
        <translation>Ծանուցում</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="71"/>
        <source>Search content is empty!</source>
        <translation>Որոնում բովանդակությունը դատարկ է!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="104"/>
        <source>Searching group failed!</source>
        <translation>Որոնման խումբը ձախողվեց:</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="179"/>
        <location filename="imsearchgroup.cpp" line="184"/>
        <location filename="imsearchgroup.cpp" line="189"/>
        <source>Failed to get group information!</source>
        <translation>Չհաջողվեց խմբային տեղեկատվություն ստանալ:</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="263"/>
        <source>Has already joined this group!</source>
        <translation>Արդեն միացել է այս խմբին:</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="282"/>
        <source>Group apply succeed!</source>
        <translation>Խումբը հաջողության է հասնում:</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="302"/>
        <source>Group apply failed!</source>
        <translation>Խմբի կիրառումը ձախողվեց:</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="359"/>
        <source> joined the group</source>
        <translation>Գրանցված խումբը</translation>
    </message>
    <message>
        <source>Searching tribe failed!</source>
        <translation type="vanished">Որոնել ցեղը ձախողվեց!</translation>
    </message>
    <message>
        <source>Failed to get tribe information!</source>
        <translation type="vanished">Չհաջողվեց ստանալ ցեղային տեղեկատվություն!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="194"/>
        <source>No qualified users were found!</source>
        <translation>Ոչ մի որակյալ օգտվող չգտնվեց!</translation>
    </message>
    <message>
        <source>Has already joined this tribe!</source>
        <translation type="vanished">Արդեն միացել է այս ցեղին!</translation>
    </message>
    <message>
        <source>Tribe apply succeed!</source>
        <translation type="vanished">Ցեղ Հաջողակ կիրառումը!</translation>
    </message>
    <message>
        <source>Tribe apply failed!</source>
        <translation type="vanished">Ցեղ Դիմումը ձախողվեց!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="317"/>
        <source>Network anomaly!</source>
        <translation>Ցանցային անոմալիա!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="350"/>
        <source>:/GroupChat/Resources/groupchat/group.png</source>
        <translation></translation>
    </message>
    <message>
        <source> joined the tribe</source>
        <translation type="vanished"> միացան ցեղին</translation>
    </message>
</context>
<context>
    <name>IMSearchPerson</name>
    <message>
        <location filename="imsearchperson.ui" line="14"/>
        <source>IMSearchPerson</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="22"/>
        <source>Search</source>
        <translation>Որոնում</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="23"/>
        <source>NickName/PhoneNumber</source>
        <translation>մականունը/Հեռախոսահամար</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="71"/>
        <location filename="imsearchperson.cpp" line="104"/>
        <location filename="imsearchperson.cpp" line="167"/>
        <location filename="imsearchperson.cpp" line="174"/>
        <location filename="imsearchperson.cpp" line="183"/>
        <location filename="imsearchperson.cpp" line="206"/>
        <location filename="imsearchperson.cpp" line="223"/>
        <location filename="imsearchperson.cpp" line="229"/>
        <location filename="imsearchperson.cpp" line="344"/>
        <source>Notice</source>
        <translation>Ծանուցում</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="71"/>
        <source>Search content is empty!</source>
        <translation>Որոնում բովանդակությունը դատարկ է!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="104"/>
        <source>Searching friend failed!</source>
        <translation>Որոնման ընկերը ձախողվեց!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="167"/>
        <source>Failed to get user information!</source>
        <translation>Չհաջողվեց ստանալ օգտվողի տեղեկությունները!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="174"/>
        <source>No matching users!</source>
        <translation>Համօգտագործող օգտվողներ չկան!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="183"/>
        <source>Can&apos;t add yourself as a friend!</source>
        <translation>Չի կարող ավելացնել ձեր ընկերը!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="206"/>
        <source>This user is already your friend!</source>
        <translation>Այս օգտվողը արդեն ձեր ընկերն է!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="223"/>
        <source>Friend apply succeed!</source>
        <translation>Ընկերը դիմում է հաջողության!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="229"/>
        <source>Friend apply failed!</source>
        <translation>Ուղարկել ընկերը չհաջողվեց!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="344"/>
        <source>Network anomaly!</source>
        <translation>Ցանցային անոմալիա!</translation>
    </message>
</context>
<context>
    <name>LeaveMessageWidget</name>
    <message>
        <location filename="leavemessagewidget.ui" line="32"/>
        <source>Add Friends</source>
        <translation>Ավելացնել ընկերներ</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="175"/>
        <source>Add</source>
        <translation>Ավելացնել</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="464"/>
        <source>Verification Info</source>
        <translation>Ստուգման տեղեկություն</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="492"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;SimSun&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Microsoft YaHei&apos;; font-size:15px; font-weight:96;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="540"/>
        <source>Remarks</source>
        <translation>Նշումներ</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="626"/>
        <source>Cancel</source>
        <translation>Չեղարկել</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="673"/>
        <source>OK</source>
        <translation>լավ</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.cpp" line="136"/>
        <source>I am </source>
        <translation>ես եմ </translation>
    </message>
    <message>
        <location filename="leavemessagewidget.cpp" line="329"/>
        <source>ID:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="leavemessagewidget.cpp" line="406"/>
        <source>:/GroupChat/Resources/groupchat/group.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="leavemessagewidget.cpp" line="422"/>
        <source>Group ID </source>
        <translation>Խմբի ID</translation>
    </message>
    <message>
        <source>Tribe ID </source>
        <translation type="vanished">Ցեղի ID </translation>
    </message>
</context>
<context>
    <name>MyTitleBar</name>
    <message>
        <location filename="mytitlebar.cpp" line="55"/>
        <source>minimize</source>
        <translation>փոքրացնել</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="56"/>
        <source>restore</source>
        <translation>վերականգնել</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="57"/>
        <source>maximize</source>
        <translation>Ամբողջ էկրանով</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="58"/>
        <source>close</source>
        <translation>փակել</translation>
    </message>
</context>
</TS>
