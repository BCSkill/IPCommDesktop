<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>CreateGroupWidget</name>
    <message>
        <location filename="creategroupwidget.ui" line="20"/>
        <location filename="creategroupwidget.cpp" line="161"/>
        <source>Create group</source>
        <oldsource>Create tribe</oldsource>
        <translation>Criar grupo</translation>
    </message>
    <message>
        <source>the name of the tribe</source>
        <translation type="vanished">o nome da tribo</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="62"/>
        <source>the name of the group</source>
        <translation>O nome do grupo</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="84"/>
        <location filename="creategroupwidget.cpp" line="446"/>
        <source>Please tick the contacts you want to add</source>
        <translation>Por favor, marque os contatos 
que você deseja adicionar</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="112"/>
        <source>Search for contacts</source>
        <translation>Pesquisar contatos</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="174"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="199"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="240"/>
        <source>Group name:</source>
        <translation>Nome do grupo:</translation>
    </message>
    <message>
        <source>Tribal name:</source>
        <translation type="vanished">Nome tribal:</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="448"/>
        <source> contact has been selected</source>
        <translation> contato foi selecionado</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="458"/>
        <location filename="creategroupwidget.cpp" line="531"/>
        <location filename="creategroupwidget.cpp" line="549"/>
        <source>Warning</source>
        <translation>Aviso</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="458"/>
        <source>Group members cannot be empty!</source>
        <translation>Os membros do grupo não podem estar vazios!</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="531"/>
        <location filename="creategroupwidget.cpp" line="549"/>
        <source>Group creation failed</source>
        <translation>A criação do grupo falhou</translation>
    </message>
    <message>
        <source>Tribal members cannot be empty!</source>
        <translation type="vanished">Os membros tribais não podem estar vazios!</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="499"/>
        <location filename="creategroupwidget.cpp" line="609"/>
        <source>、</source>
        <translation></translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="506"/>
        <source>…</source>
        <translation></translation>
    </message>
    <message>
        <source>Tribal creation failed</source>
        <translation type="vanished">Criação tribal falhou</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="613"/>
        <source> invited </source>
        <translation> convidamos </translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="613"/>
        <source> to join the group</source>
        <translation>Para se juntar ao grupo</translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished"> para se juntar à tribo</translation>
    </message>
</context>
<context>
    <name>IMAddPerson</name>
    <message>
        <location filename="imaddperson.ui" line="14"/>
        <location filename="imaddperson.cpp" line="81"/>
        <source>Add friends</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imaddperson.ui" line="117"/>
        <source>Find friend</source>
        <translation>amigo</translation>
    </message>
    <message>
        <location filename="imaddperson.ui" line="170"/>
        <source>Find group</source>
        <translation>Encontre o grupo</translation>
    </message>
    <message>
        <source>Find tribe</source>
        <translation type="vanished">tribo</translation>
    </message>
    <message>
        <location filename="imaddperson.ui" line="32"/>
        <source>Application number
service number</source>
        <translation>Número de aplicação
número de serviço</translation>
    </message>
</context>
<context>
    <name>IMSearchGroup</name>
    <message>
        <location filename="imsearchgroup.ui" line="14"/>
        <source>IMSearchGroup</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="21"/>
        <source>Search</source>
        <translation>Procurar</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="22"/>
        <source>Group ID/Group Name</source>
        <oldsource>Tribe ID/Tribe Name</oldsource>
        <translation>ID do grupo / nome do grupo</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="71"/>
        <location filename="imsearchgroup.cpp" line="104"/>
        <location filename="imsearchgroup.cpp" line="179"/>
        <location filename="imsearchgroup.cpp" line="184"/>
        <location filename="imsearchgroup.cpp" line="189"/>
        <location filename="imsearchgroup.cpp" line="194"/>
        <location filename="imsearchgroup.cpp" line="263"/>
        <location filename="imsearchgroup.cpp" line="282"/>
        <location filename="imsearchgroup.cpp" line="302"/>
        <location filename="imsearchgroup.cpp" line="317"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="71"/>
        <source>Search content is empty!</source>
        <translation>O conteúdo da pesquisa está vazio!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="104"/>
        <source>Searching group failed!</source>
        <translation>Procurando grupo falhou!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="179"/>
        <location filename="imsearchgroup.cpp" line="184"/>
        <location filename="imsearchgroup.cpp" line="189"/>
        <source>Failed to get group information!</source>
        <translation>Falha ao obter informações do grupo!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="263"/>
        <source>Has already joined this group!</source>
        <translation>Já se juntou a este grupo!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="282"/>
        <source>Group apply succeed!</source>
        <translation>Grupo aplicar sucesso!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="302"/>
        <source>Group apply failed!</source>
        <translation>Grupo aplicar falhou!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="359"/>
        <source> joined the group</source>
        <translation>Juntou-se ao grupo</translation>
    </message>
    <message>
        <source>Searching tribe failed!</source>
        <translation type="vanished">Procurando pela tribo falhou!</translation>
    </message>
    <message>
        <source>Failed to get tribe information!</source>
        <translation type="vanished">Não conseguiu obter informações sobre a tribo!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="194"/>
        <source>No qualified users were found!</source>
        <translation>Nenhum usuário qualificado foi encontrado!</translation>
    </message>
    <message>
        <source>Has already joined this tribe!</source>
        <translation type="vanished">Já se juntou a esta tribo!</translation>
    </message>
    <message>
        <source>Tribe apply succeed!</source>
        <translation type="vanished">Tribo aplique bem sucedido!</translation>
    </message>
    <message>
        <source>Tribe apply failed!</source>
        <translation type="vanished">Tribe apply falhou!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="317"/>
        <source>Network anomaly!</source>
        <translation>Anomalia de rede!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="350"/>
        <source>:/GroupChat/Resources/groupchat/group.png</source>
        <translation></translation>
    </message>
    <message>
        <source> joined the tribe</source>
        <translation type="vanished"> juntou-se a tribo</translation>
    </message>
</context>
<context>
    <name>IMSearchPerson</name>
    <message>
        <location filename="imsearchperson.ui" line="14"/>
        <source>IMSearchPerson</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="22"/>
        <source>Search</source>
        <translation>Procurar</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="23"/>
        <source>NickName/PhoneNumber</source>
        <translation>NickName / Phone Number</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="71"/>
        <location filename="imsearchperson.cpp" line="104"/>
        <location filename="imsearchperson.cpp" line="167"/>
        <location filename="imsearchperson.cpp" line="174"/>
        <location filename="imsearchperson.cpp" line="183"/>
        <location filename="imsearchperson.cpp" line="206"/>
        <location filename="imsearchperson.cpp" line="223"/>
        <location filename="imsearchperson.cpp" line="229"/>
        <location filename="imsearchperson.cpp" line="344"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="71"/>
        <source>Search content is empty!</source>
        <translation>O conteúdo da pesquisa está vazio!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="104"/>
        <source>Searching friend failed!</source>
        <translation>Procurando amigo falhou!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="167"/>
        <source>Failed to get user information!</source>
        <translation>Falha ao obter informações do usuário!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="174"/>
        <source>No matching users!</source>
        <translation>Nenhum usuário correspondente！</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="183"/>
        <source>Can&apos;t add yourself as a friend!</source>
        <translation>Não se pode adicionar como amigo!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="206"/>
        <source>This user is already your friend!</source>
        <translation>Este usuário já é seu amigo!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="223"/>
        <source>Friend apply succeed!</source>
        <translation>Tribo aplique bem sucedido!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="229"/>
        <source>Friend apply failed!</source>
        <translation>Amigo aplicar falhou!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="344"/>
        <source>Network anomaly!</source>
        <translation>Anomalia de rede!</translation>
    </message>
</context>
<context>
    <name>LeaveMessageWidget</name>
    <message>
        <location filename="leavemessagewidget.ui" line="32"/>
        <source>Add Friends</source>
        <translation>Adicionar amigos</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="175"/>
        <source>Add</source>
        <translation>Adicionar</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="464"/>
        <source>Verification Info</source>
        <translation>Informação de verificação</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="492"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;SimSun&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Microsoft YaHei&apos;; font-size:15px; font-weight:96;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="540"/>
        <source>Remarks</source>
        <translation>Observações</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="626"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="673"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.cpp" line="136"/>
        <source>I am </source>
        <translation>eu sou </translation>
    </message>
    <message>
        <location filename="leavemessagewidget.cpp" line="329"/>
        <source>ID:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="leavemessagewidget.cpp" line="406"/>
        <source>:/GroupChat/Resources/groupchat/group.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="leavemessagewidget.cpp" line="422"/>
        <source>Group ID </source>
        <translation>ID do grupo</translation>
    </message>
    <message>
        <source>Tribe ID </source>
        <translation type="vanished">ID da tribo </translation>
    </message>
</context>
<context>
    <name>MyTitleBar</name>
    <message>
        <location filename="mytitlebar.cpp" line="55"/>
        <source>minimize</source>
        <translation>minimizar</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="56"/>
        <source>restore</source>
        <translation>restaurar</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="57"/>
        <source>maximize</source>
        <translation>maximizar</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="58"/>
        <source>close</source>
        <translation>perto</translation>
    </message>
</context>
</TS>
