<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="th_TH">
<context>
    <name>CreateGroupWidget</name>
    <message>
        <location filename="creategroupwidget.ui" line="20"/>
        <location filename="creategroupwidget.cpp" line="161"/>
        <source>Create group</source>
        <oldsource>Create tribe</oldsource>
        <translation>สร้างกลุ่ม</translation>
    </message>
    <message>
        <source>the name of the tribe</source>
        <translation type="vanished">กรุณาใส่ชื่อของเผ่า</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="62"/>
        <source>the name of the group</source>
        <translation>ชื่อของกลุ่ม</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="84"/>
        <location filename="creategroupwidget.cpp" line="446"/>
        <source>Please tick the contacts you want to add</source>
        <translation>โปรดเลือกผู้ติดต่อที่คุณต้องการเพิ่ม</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="112"/>
        <source>Search for contacts</source>
        <translation>ค้นหาผู้ติดต่อ</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="174"/>
        <source>Cancel</source>
        <translation>ยกเลิก</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="199"/>
        <source>OK</source>
        <translation>ตกลง</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="240"/>
        <source>Group name:</source>
        <translation>ชื่อกลุ่ม:</translation>
    </message>
    <message>
        <source>Tribal name:</source>
        <translation type="vanished">ชื่อเผ่า:</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="448"/>
        <source> contact has been selected</source>
        <translation> contact has been selected</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="458"/>
        <location filename="creategroupwidget.cpp" line="531"/>
        <location filename="creategroupwidget.cpp" line="549"/>
        <source>Warning</source>
        <translation>การเตือน</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="458"/>
        <source>Group members cannot be empty!</source>
        <translation>สมาชิกในกลุ่มต้องไม่ว่างเปล่า!</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="531"/>
        <location filename="creategroupwidget.cpp" line="549"/>
        <source>Group creation failed</source>
        <translation>การสร้างกลุ่มล้มเหลว</translation>
    </message>
    <message>
        <source>Tribal members cannot be empty!</source>
        <translation type="vanished">สมาชิกเผ่าไม่สามารถว่างเปล่าได้!</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="499"/>
        <location filename="creategroupwidget.cpp" line="609"/>
        <source>、</source>
        <translation></translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="506"/>
        <source>…</source>
        <translation></translation>
    </message>
    <message>
        <source>Tribal creation failed</source>
        <translation type="vanished">การสร้างเผ่าล้มเหลว</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="613"/>
        <source> invited </source>
        <translation>ได้รับเชิญ</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="613"/>
        <source> to join the group</source>
        <translation>เพื่อเข้าร่วมกลุ่ม</translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished">เพื่อเข้าร่วมเผ่า</translation>
    </message>
</context>
<context>
    <name>IMAddPerson</name>
    <message>
        <location filename="imaddperson.ui" line="14"/>
        <location filename="imaddperson.cpp" line="81"/>
        <source>Add friends</source>
        <translation>เพิ่มเพื่อน</translation>
    </message>
    <message>
        <location filename="imaddperson.ui" line="32"/>
        <source>Application number
service number</source>
        <translation>หมายเลขใบสมัคร
หมายเลขบริการ</translation>
    </message>
    <message>
        <location filename="imaddperson.ui" line="117"/>
        <source>Find friend</source>
        <translation>หาเพื่อน</translation>
    </message>
    <message>
        <location filename="imaddperson.ui" line="170"/>
        <source>Find group</source>
        <translation>ค้นหากลุ่ม</translation>
    </message>
    <message>
        <source>Find tribe</source>
        <translation type="vanished">ค้นหาเผ่า</translation>
    </message>
</context>
<context>
    <name>IMSearchGroup</name>
    <message>
        <location filename="imsearchgroup.ui" line="14"/>
        <source>IMSearchGroup</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="21"/>
        <source>Search</source>
        <translation>ค้นหา</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="22"/>
        <source>Group ID/Group Name</source>
        <oldsource>Tribe ID/Tribe Name</oldsource>
        <translation>ID กลุ่ม / ชื่อกลุ่ม</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="71"/>
        <location filename="imsearchgroup.cpp" line="104"/>
        <location filename="imsearchgroup.cpp" line="179"/>
        <location filename="imsearchgroup.cpp" line="184"/>
        <location filename="imsearchgroup.cpp" line="189"/>
        <location filename="imsearchgroup.cpp" line="194"/>
        <location filename="imsearchgroup.cpp" line="263"/>
        <location filename="imsearchgroup.cpp" line="282"/>
        <location filename="imsearchgroup.cpp" line="302"/>
        <location filename="imsearchgroup.cpp" line="317"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="71"/>
        <source>Search content is empty!</source>
        <translation>ค้นหาเนื้อหาว่างเปล่า!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="104"/>
        <source>Searching group failed!</source>
        <translation>การค้นหาล้มเหลว!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="179"/>
        <location filename="imsearchgroup.cpp" line="184"/>
        <location filename="imsearchgroup.cpp" line="189"/>
        <source>Failed to get group information!</source>
        <translation>ไม่สามารถรับข้อมูลกลุ่ม!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="263"/>
        <source>Has already joined this group!</source>
        <translation>เข้าร่วมกลุ่มนี้แล้ว!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="282"/>
        <source>Group apply succeed!</source>
        <translation>สมัครเป็นกลุ่มสำเร็จ!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="302"/>
        <source>Group apply failed!</source>
        <translation>การสมัครเป็นกลุ่มล้มเหลว!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="359"/>
        <source> joined the group</source>
        <translation>เข้าร่วมกลุ่ม</translation>
    </message>
    <message>
        <source>Failed to get tribe information!</source>
        <translation type="vanished">ไม่สามารถรับข้อมูลเกี่ยวกับเผ่า!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="194"/>
        <source>No qualified users were found!</source>
        <translation>ไม่มีเผ่าใดที่ตรงกับเกณฑ์การค้นหา!</translation>
    </message>
    <message>
        <source>Has already joined this tribe!</source>
        <translation type="vanished">เข้าร่วมเผ่าแล้ว!</translation>
    </message>
    <message>
        <source>Tribe apply succeed!</source>
        <translation type="vanished">แอปพลิเคชั่นเผ่าสำเร็จแล้ว!</translation>
    </message>
    <message>
        <source>Tribe apply failed!</source>
        <translation type="vanished">แอปพลิเคชันชนเผ่าล้มเหลว!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="317"/>
        <source>Network anomaly!</source>
        <translation>เครือข่ายผิดปกติ!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="350"/>
        <source>:/GroupChat/Resources/groupchat/group.png</source>
        <translation></translation>
    </message>
    <message>
        <source> joined the tribe</source>
        <translation type="vanished">เข้าร่วมเผ่า</translation>
    </message>
</context>
<context>
    <name>IMSearchPerson</name>
    <message>
        <location filename="imsearchperson.ui" line="14"/>
        <source>IMSearchPerson</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="22"/>
        <source>Search</source>
        <translation>ค้นหา</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="23"/>
        <source>NickName/PhoneNumber</source>
        <translation>ชื่อเล่น / หมายเลขโทรศัพท์</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="71"/>
        <location filename="imsearchperson.cpp" line="104"/>
        <location filename="imsearchperson.cpp" line="167"/>
        <location filename="imsearchperson.cpp" line="174"/>
        <location filename="imsearchperson.cpp" line="183"/>
        <location filename="imsearchperson.cpp" line="206"/>
        <location filename="imsearchperson.cpp" line="223"/>
        <location filename="imsearchperson.cpp" line="229"/>
        <location filename="imsearchperson.cpp" line="344"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="71"/>
        <source>Search content is empty!</source>
        <translation>ค้นหาเนื้อหาว่างเปล่า!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="104"/>
        <source>Searching friend failed!</source>
        <translation>ค้นหาล้มเหลว!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="167"/>
        <source>Failed to get user information!</source>
        <translation>ไม่สามารถรับข้อมูลผู้ใช้ได้!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="174"/>
        <source>No matching users!</source>
        <translation>ไม่มีผู้ใช้ที่ตรงกัน!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="183"/>
        <source>Can&apos;t add yourself as a friend!</source>
        <translation>ไม่สามารถเพิ่มตัวเองเป็นเพื่อนได้!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="206"/>
        <source>This user is already your friend!</source>
        <translation>เพิ่มเพื่อนคนนี้แล้ว!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="223"/>
        <source>Friend apply succeed!</source>
        <translation>สมัครสำเร็จ!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="229"/>
        <source>Friend apply failed!</source>
        <translation>การสมัครล้มเหลว!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="344"/>
        <source>Network anomaly!</source>
        <translation>เครือข่ายผิดปกติ!</translation>
    </message>
</context>
<context>
    <name>LeaveMessageWidget</name>
    <message>
        <location filename="leavemessagewidget.ui" line="32"/>
        <source>Add Friends</source>
        <translation>เพิ่มเพื่อน</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="175"/>
        <source>Add</source>
        <translation>เพิ่ม</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="464"/>
        <source>Verification Info</source>
        <translation>ข้อมูลการตรวจสอบ</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="492"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;SimSun&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Microsoft YaHei&apos;; font-size:15px; font-weight:96;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="540"/>
        <source>Remarks</source>
        <translation>ข้อคิดเห็น</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="626"/>
        <source>Cancel</source>
        <translation>ยกเลิก</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="673"/>
        <source>OK</source>
        <translation>ตกลง</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.cpp" line="136"/>
        <source>I am </source>
        <translation>ฉันคือ</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.cpp" line="329"/>
        <source>ID:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="leavemessagewidget.cpp" line="406"/>
        <source>:/GroupChat/Resources/groupchat/group.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="leavemessagewidget.cpp" line="422"/>
        <source>Group ID </source>
        <translation>รหัสกลุ่ม</translation>
    </message>
    <message>
        <source>Tribe ID </source>
        <translation type="vanished">รหัสชนเผ่า</translation>
    </message>
</context>
<context>
    <name>MyTitleBar</name>
    <message>
        <location filename="mytitlebar.cpp" line="55"/>
        <source>minimize</source>
        <translation>ลด</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="56"/>
        <source>restore</source>
        <translation>ฟื้นฟู</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="57"/>
        <source>maximize</source>
        <translation>เพิ่ม</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="58"/>
        <source>close</source>
        <translation>ใกล้</translation>
    </message>
</context>
</TS>
