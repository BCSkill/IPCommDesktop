# ----------------------------------------------------
# This file is generated by the Qt Visual Studio Tools.
# ------------------------------------------------------

# This is a reminder that you are using a generated .pro file.
# Remove it when you are finished editing this file.
message("You are running qmake on a generated .pro file. This may not work!")


HEADERS += ./basewindow.h \
    ./creategroupwidget.h \
    ./imaddperson.h \
    ./imsearchgroup.h \
    ./imsearchperson.h \
    ./mytitlebar.h \
    ./leavemessagewidget.h
SOURCES += ./basewindow.cpp \
    $$PWD/creategroupwidget.cpp \
    $$PWD/imaddperson.cpp \
    $$PWD/imsearchgroup.cpp \
    $$PWD/imsearchperson.cpp \
    $$PWD/mytitlebar.cpp \
    $$PWD/leavemessagewidget.cpp
FORMS += ./creategroupwidget.ui \
    ./imaddperson.ui \
    ./imsearchgroup.ui \
    ./imsearchperson.ui \
    ./leavemessagewidget.ui
