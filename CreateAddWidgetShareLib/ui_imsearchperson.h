/********************************************************************************
** Form generated from reading UI file 'imsearchperson.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_IMSEARCHPERSON_H
#define UI_IMSEARCHPERSON_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>
#include "cfriendtablewidgetbase.h"

QT_BEGIN_NAMESPACE

class Ui_IMSearchPerson
{
public:
    QLineEdit *mlineEditSearchPerson;
    QPushButton *mPButtonSearchPerson;
    QLabel *mLabelSearchPerson;
    QWidget *widget;
    CFriendTableWidgetBase *mListWidgetSearchPerson;

    void setupUi(QWidget *IMSearchPerson)
    {
        if (IMSearchPerson->objectName().isEmpty())
            IMSearchPerson->setObjectName(QStringLiteral("IMSearchPerson"));
        IMSearchPerson->resize(600, 385);
        IMSearchPerson->setStyleSheet(QStringLiteral(""));
        mlineEditSearchPerson = new QLineEdit(IMSearchPerson);
        mlineEditSearchPerson->setObjectName(QStringLiteral("mlineEditSearchPerson"));
        mlineEditSearchPerson->setGeometry(QRect(20, 15, 470, 32));
        mlineEditSearchPerson->setContextMenuPolicy(Qt::NoContextMenu);
        mlineEditSearchPerson->setStyleSheet(QStringLiteral(""));
        mPButtonSearchPerson = new QPushButton(IMSearchPerson);
        mPButtonSearchPerson->setObjectName(QStringLiteral("mPButtonSearchPerson"));
        mPButtonSearchPerson->setGeometry(QRect(500, 15, 72, 32));
        mPButtonSearchPerson->setCursor(QCursor(Qt::PointingHandCursor));
        mPButtonSearchPerson->setStyleSheet(QStringLiteral(""));
        mLabelSearchPerson = new QLabel(IMSearchPerson);
        mLabelSearchPerson->setObjectName(QStringLiteral("mLabelSearchPerson"));
        mLabelSearchPerson->setGeometry(QRect(0, 0, 600, 60));
        mLabelSearchPerson->setStyleSheet(QStringLiteral(""));
        widget = new QWidget(IMSearchPerson);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(0, 60, 600, 325));
        widget->setStyleSheet(QStringLiteral(""));
        mListWidgetSearchPerson = new CFriendTableWidgetBase(widget);
        mListWidgetSearchPerson->setObjectName(QStringLiteral("mListWidgetSearchPerson"));
        mListWidgetSearchPerson->setGeometry(QRect(20, 0, 580, 325));
        mListWidgetSearchPerson->setStyleSheet(QStringLiteral(""));
        mLabelSearchPerson->raise();
        mlineEditSearchPerson->raise();
        mPButtonSearchPerson->raise();
        widget->raise();

        retranslateUi(IMSearchPerson);

        QMetaObject::connectSlotsByName(IMSearchPerson);
    } // setupUi

    void retranslateUi(QWidget *IMSearchPerson)
    {
        IMSearchPerson->setWindowTitle(QApplication::translate("IMSearchPerson", "IMSearchPerson", nullptr));
        mlineEditSearchPerson->setText(QString());
        mPButtonSearchPerson->setText(QString());
        mLabelSearchPerson->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class IMSearchPerson: public Ui_IMSearchPerson {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_IMSEARCHPERSON_H
