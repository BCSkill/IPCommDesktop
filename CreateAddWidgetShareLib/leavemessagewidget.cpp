﻿#include "leavemessagewidget.h"
#include <QMouseEvent>
#include <qmath.h>
#include <QPainter>
#include "common.h"
#include "httpnetworksharelib.h"
#include "leavemessagewidget.h"
#include "alphabeticalsortsharedlib.h"
#include "QJsonParseError"
#include "messagebox.h"
#include <QMessageBox>
#include <QJsonDocument>
#include <QJsonObject>
#include "imbuddy.h"
#include "stdafx.h"
#include "globalmanager.h"
#include <QDir>
#include "ui_leavemessagewidget.h"
#ifndef Q_OS_WIN
#include "inline_mac.h"
#endif
#include "QStringLiteralBak.h"

LeaveMessageWidget::LeaveMessageWidget(QWidget *parent)
: QWidget(parent)
{
	ui = new Ui::LeaveMessageWidget();
	ui->setupUi(this);
	this->setAttribute(Qt::WA_DeleteOnClose);
	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog);
	setWindowModality(Qt::WindowModal);
	setWindowIcon(QIcon(":/Login/Resources/login/system.ico"));


	QFile file(":/QSS/Resources/QSS/CreateAddWidgetShareLib/leavemessagewidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	//this->setAttribute(Qt::WA_TranslucentBackground);
	//this->setContentsMargins(6, 6, 6, 6);
	//ui->NameLable->setAlignment(Qt::AlignHCenter);
	connect(ui->pushButtonOK, SIGNAL(clicked()), this, SLOT(slotOnOk()));
	connect(ui->pushButtonEsc, SIGNAL(clicked()), this, SLOT(slotOnEsc()));
	connect(ui->closeButton, SIGNAL(clicked()), this, SLOT(close()));
#ifdef Q_OS_WIN
	shadow = new Shadow();
#endif
}
LeaveMessageWidget::~LeaveMessageWidget()
{
#ifdef Q_OS_WIN
	if (shadow)
		delete shadow;
#endif

	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}
void LeaveMessageWidget::changeEvent(QEvent * event)
{
	if (event->type() == QEvent::WindowStateChange)
	{
#ifdef Q_OS_WIN
		if (this->windowState() == Qt::WindowMaximized)
			shadow->hide();
		if (this->windowState() == Qt::WindowMinimized)
			shadow->hide();

		if (this->windowState() == Qt::WindowNoState)
			shadow->show();
#endif
	}

	QWidget::changeEvent(event);
}


void LeaveMessageWidget::moveEvent(QMoveEvent *event)
{
#ifdef Q_OS_WIN
	shadow->move(event->pos().x() - 10, event->pos().y() - 10);
#endif
}

void LeaveMessageWidget::resizeEvent(QResizeEvent* event)
{
#ifdef Q_OS_WIN
	shadow->resize(event->size().width() + 20, event->size().height() + 20);
#endif
}

void LeaveMessageWidget::show()
{
	QWidget::show();
#ifdef Q_OS_WIN
	shadow->resize(width() + 20, height() + 20);
	shadow->move(x() - 10, y() - 10);
	shadow->show();
#endif
#ifdef Q_OS_WIN
	// 窗口被激活了，强制重绘  
	InvalidateRect((HWND)this->winId(), nullptr, FALSE);
#endif
}
// void LeaveMessageWidget::paintEvent(QPaintEvent * event)
// {
// 	QPainterPath path;
// 	path.setFillRule(Qt::WindingFill);
// 	path.addRect(6, 6, this->width() - 12, this->height() - 12);
// 
// 	QPainter painter(this);
// 	painter.setRenderHint(QPainter::Antialiasing, true);
// 	painter.fillPath(path, QBrush(Qt::white));
// 
// 	QColor color(0, 0, 0, 50);//, 50);
// 	for (int i = 0; i < 6; i++)
// 	{
// 		QPainterPath path;
// 		path.setFillRule(Qt::WindingFill);
// 		path.addRect(6 - i, 6 - i, this->width() - (6 - i) * 2, this->height() - (6 - i) * 2);
// 		color.setAlpha(110 - qSqrt(i) * 40);
// 		painter.setPen(color);
// 		painter.drawPath(path);
// 	}
// }
void LeaveMessageWidget::OnInitBuddyInfo(QString strBuddyID)
{
	iType = 0;
	m_strBuddyId = strBuddyID;
	UserInfo userInfo = gDataManager->getUserInfo();
	ui->MessageEdit->setPlainText(tr("I am ") + userInfo.strUserName);
	HttpNetWork::HttpNetWorkShareLib *PersonInfo = new HttpNetWork::HttpNetWorkShareLib();
	connect(PersonInfo, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotTempFriend(bool, QString)));
	AppConfig configInfo = gDataManager->getAppConfigInfo();
	QString strRequest = configInfo.MessageServerAddress + "/IMServer/user/getOtherUserByUserId?otherUserId=" + strBuddyID;
	PersonInfo->getHttpRequest(strRequest);
// 	OnSetPicPath(stInfo.strLocalAvatar, 0);
// 	ui->NameLable->setText(stInfo.strNickName);
}
void LeaveMessageWidget::OnInitGroupInfo(QString strGroupID)
{
	iType = 1;
	m_strGroupId = strGroupID;
	ui->OtherNameLabel->hide();
	ui->OtherNamelineEdit->hide();
	UserInfo userInfo = gDataManager->getUserInfo();
	HttpNetWork::HttpNetWorkShareLib *PersonInfo = new HttpNetWork::HttpNetWorkShareLib();
	connect(PersonInfo, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotTempGroup(bool, QString)));
	AppConfig configInfo = gDataManager->getAppConfigInfo();
	QString strResult = configInfo.MessageServerAddress + "/IMServer/group/getGroupByGroupId" + QString("?userId=") + QString::number(userInfo.nUserID) + "&passWord=" + userInfo.strUserPWD + "&groupId=" + strGroupID;
	PersonInfo->getHttpRequest(strResult);
}
void LeaveMessageWidget::slotOnOk()
{
 	QString strMessage = ui->MessageEdit->toPlainText();
 	QString strOtherName = ui->OtherNamelineEdit->text();
// 	emit sigLeaveMessage(strMessage, strOtherName);
	UserInfo userInfo = gDataManager->getUserInfo();
	if (iType == 0)
	{
		HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
		connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(doHttpAddPersonResultInfo(bool, QString)));

		QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/user/applyAddFriend"
			+ QString("?userId=%1&passWord=%2&friendUserId=%3&leaveMessage=%4").arg(userInfo.nUserID).arg(userInfo.strUserPWD).arg(m_strBuddyId).arg(strMessage);

		// 	QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/search"
		// 		+ QString("?userId=%1&passWord=%2&keyWord=%3").arg(userInfo.nUserID).arg(userInfo.strUserPWD).arg(strData);

		if (strOtherName != "")
		{
			m_stTmpInfo.strNote = strOtherName;
			gDataBaseOpera->DBInsertBuddyInfo(m_stTmpInfo);
		}

		http->getHttpRequest(url);
	}
	else
	{
		HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
		connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(doHttpAddPersonResultInfo(bool, QString)));

		QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/applyAddGroup"
			+ QString("?userId=%1&passWord=%2&groupId=%3&leaveMessage=%4").arg(userInfo.nUserID).arg(userInfo.strUserPWD).arg(m_strGroupId).arg(strMessage);

		// 	QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/search"
		// 		+ QString("?userId=%1&passWord=%2&keyWord=%3").arg(userInfo.nUserID).arg(userInfo.strUserPWD).arg(strData);

// 		if (strOtherName != "")
// 		{
// 			m_stTmpInfo.strNote = strOtherName;
// 			gDataBaseOpera->DBInsertBuddyInfo(m_stTmpInfo);
// 		}

		http->getHttpRequest(url);
	}
}
void LeaveMessageWidget::slotOnEsc()
{
	this->close();
}
//鼠标事件的处理。
void LeaveMessageWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	//dateSelector->hide();   //隐藏日期选择器。
	return QWidget::mousePressEvent(event);
}
void LeaveMessageWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void LeaveMessageWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

void LeaveMessageWidget::OnSetPicPath(QString strPath, int nType /*= 0*/)
{
	if (ui->HeaderLabel)
	{
		QByteArray bytearray;
		QFile file(strPath);
		if (file.open(QIODevice::ReadOnly) && file.size() != 0)
		{
			bytearray = file.readAll();

			if (bytearray.endsWith("temp"))
				ui->HeaderLabel->setTransform(false);
		}
		file.close();
		QPixmap pix;
		if (!pix.loadFromData(bytearray) || bytearray == "")
		{
			if (nType == 0)
			{
				pix.load(":/PerChat/Resources/person/temp.png");
			}
			else
			{
				pix.load(":/GroupChat/Resources/groupchat/group.png");
			}
		}
		ui->HeaderLabel->setAutoFillBackground(true);
		ui->HeaderLabel->setPixmap(pix);
		ui->HeaderLabel->setScaledContents(true);
		ui->HeaderLabel->setObjectName(strPath);
	}
}

void LeaveMessageWidget::slotTempFriend(bool bResult, QString result)
{
	if (bResult)
	{
		QJsonParseError jsonError;
		BuddyInfo buddyInfo;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(result.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QVariantMap result = jsonDocument.toVariant().toMap();
				if (result["result"].toString() == "success")
				{
					QVariantMap usrInfo = result["user"].toMap();
					if (!usrInfo.isEmpty())
					{
						QString strPinYin;
						buddyInfo.strHttpAvatar = usrInfo["avatar"].toString();
						buddyInfo.strEmail = usrInfo["email"].toString();
						buddyInfo.strMobilePhone = usrInfo["mobilePhone"].toString();
						buddyInfo.strNickName = usrInfo["nickName"].toString();
						buddyInfo.strNote = usrInfo["note"].toString();
						buddyInfo.strPhone = usrInfo["phone"].toString();
						buddyInfo.strSex = usrInfo["sex"].toString();
						buddyInfo.strSign = usrInfo["sign"].toString();
						buddyInfo.nUserId = usrInfo["userId"].toInt();
						buddyInfo.strUserName = usrInfo["userName"].toString();
						buddyInfo.nUserType = usrInfo["userType"].toInt();
						buddyInfo.disableStrangers = usrInfo["disableStrangers"].toInt();
						buddyInfo.BuddyType = 0;
						buddyInfo.strLocalAvatar = gSettingsManager->getUserPath() + "/resource/header/tmp/" + QString::number(gDataManager->getUserInfo().nUserID) + "/" + usrInfo["userId"].toString() + ".png";

						//if (buddyInfo.strSex == "F")//默认头像
						//{
						//	buddyInfo.strDefaultAvatar = QString(":/IMChatClient/Resources/imchatclient/female.png");
						//}
						//else
						//{
						//	buddyInfo.strDefaultAvatar = QString(":/PerChat/Resources/person/temp.png");
						//}
						buddyInfo.strDefaultAvatar = QString(":/PerChat/Resources/person/temp.png");
						//根据名称获取首字母
						AlphabeticalSortSharedLib mAlphabeticalSort;
						if (!buddyInfo.strNote.isEmpty())
							strPinYin = mAlphabeticalSort.GetChineseSpell(buddyInfo.strNote);
						else
							strPinYin = mAlphabeticalSort.GetChineseSpell(buddyInfo.strNickName);
						buddyInfo.strPingYin = strPinYin;
// 						if (gDataBaseOpera)
// 						{
// 							gDataBaseOpera->DBInsertBuddyInfo(buddyInfo);
// 						}
						m_stTmpInfo = buddyInfo;
						HttpNetWork::HttpDownLoadFile *down = new HttpNetWork::HttpDownLoadFile;
						connect(down, SIGNAL(sigDownFinished(bool)), this, SLOT(slotShareBuddyHeader(bool)));
						down->StartDownLoadFile(buddyInfo.strHttpAvatar, buddyInfo.strLocalAvatar);

						QString strTmpName = buddyInfo.strNickName;
						if (strTmpName.length() > 6)
						{
							strTmpName = strTmpName.mid(0, 6) + "..";
							ui->NameLable->setToolTip(buddyInfo.strNickName);
						}
						ui->NameLable->setText(strTmpName);
						ui->IdLabel->setText(tr("ID:") + QString::number(buddyInfo.nUserId));
						return;
					}
				}
			}
		}
	}
}
void LeaveMessageWidget::slotDoAddPerson(QString strMessage, QString strOtherName)
{

}
void LeaveMessageWidget::slotShareBuddyHeader(bool bSuc)
{
	if (bSuc)
	{
		OnSetPicPath(m_stTmpInfo.strLocalAvatar, 0);
	}
}
void LeaveMessageWidget::doHttpAddPersonResultInfo(bool bResult, QString strResult)
{
	QJsonParseError jsonError;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(strResult.toUtf8(), &jsonError);
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QJsonObject obj = jsonDocument.object();
			if (obj["result"].toString() == "success")
			{
				//IMessageBox::tip(this, tr("提示"), tr("请求好友成功！"));
				//QMessageBox::information(this, tr("请求好友"), tr("请求好友成功!"));
				emit sigApplySuccessed();
				this->close();
				return;
			}
		}
	}
	//IMessageBox::tip(this, tr("提示"), tr("请求好友失败！"));
	emit sigApplyFailed();
	this->close();
	//QMessageBox::information(this, tr("请求好友"), tr("请求好友失败!"));
}
QString LeaveMessageWidget::GetExeDir()
{
#ifdef Q_OS_WIN
    return QDir::currentPath();
#else
	return getResourcePath();
#endif
}

void LeaveMessageWidget::slotTempGroup(bool bResult, QString result)
{
	if (bResult)
	{
		QJsonParseError jsonError;
		GroupInfo groupInfo;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(result.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QVariantMap result = jsonDocument.toVariant().toMap();
				if (result["result"].toString() == "success")
				{
					QVariantMap resultInfo = result["group"].toMap();
					groupInfo.groupHttpHeadImage = resultInfo["avatar"].toString();
					groupInfo.createTime = resultInfo["createTime"].toString();
					groupInfo.createUserId = resultInfo["createUserId"].toString();
					groupInfo.groupId = resultInfo["groupId"].toString();
#ifdef Q_OS_WIN
					QString strPath = gSettingsManager->getUserPath();
					groupInfo.groupLoacalHeadImage = strPath + "/resource/header/groupheader/" + groupInfo.groupId + ".jpg";
#else
					groupInfo.groupLoacalHeadImage = getResourcePath() + "/resource/header/groupheader/" + groupInfo.groupId + ".jpg";
#endif
					groupInfo.groupDefaultAvatar = tr(":/GroupChat/Resources/groupchat/group.png");
					groupInfo.groupName = resultInfo["groupName"].toString();
					groupInfo.noSpeak = resultInfo["noSpeak"].toInt();
					groupInfo.groupType = resultInfo["groupType"].toInt();
					groupInfo.groupDesc = resultInfo["groupDesc"].toString();
					groupInfo.groupKey = resultInfo["groupKey"].toString();
					//gDataBaseOpera->DBInsertGroupInfo(groupInfo);
					m_stTmpGroupInfo = groupInfo;
					OnSetPicPath(groupInfo.groupLoacalHeadImage, 1);
					QString strTmpName = groupInfo.groupName;
					if (strTmpName.length() > 6)
					{
						strTmpName = strTmpName.mid(0, 6)+"..";
						ui->NameLable->setToolTip(groupInfo.groupName);
					}
					ui->NameLable->setText(strTmpName);
					ui->IdLabel->setText(tr("Group ID ") + groupInfo.groupId);
				}
			}
		}
	}
}
