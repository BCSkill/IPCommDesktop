<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>CreateGroupWidget</name>
    <message>
        <location filename="creategroupwidget.ui" line="20"/>
        <location filename="creategroupwidget.cpp" line="161"/>
        <source>Create group</source>
        <oldsource>Create tribe</oldsource>
        <translation>Создать группу</translation>
    </message>
    <message>
        <source>the name of the tribe</source>
        <translation type="vanished">название племени</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="62"/>
        <source>the name of the group</source>
        <translation>Название группы</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="84"/>
        <location filename="creategroupwidget.cpp" line="446"/>
        <source>Please tick the contacts you want to add</source>
        <translation>выберите друзей для добавления</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="112"/>
        <source>Search for contacts</source>
        <translation>Поиск контактов</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="174"/>
        <source>Cancel</source>
        <translation>отменить</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="199"/>
        <source>OK</source>
        <translation>Хорошо</translation>
    </message>
    <message>
        <location filename="creategroupwidget.ui" line="240"/>
        <source>Group name:</source>
        <translation>Название группы:</translation>
    </message>
    <message>
        <source>Tribal name:</source>
        <translation type="vanished">Название:</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="448"/>
        <source> contact has been selected</source>
        <translation>контакт был выбран</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="458"/>
        <location filename="creategroupwidget.cpp" line="531"/>
        <location filename="creategroupwidget.cpp" line="549"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="458"/>
        <source>Group members cannot be empty!</source>
        <translation>Члены группы не могут быть пустыми!</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="531"/>
        <location filename="creategroupwidget.cpp" line="549"/>
        <source>Group creation failed</source>
        <translation>Не удалось создать группу</translation>
    </message>
    <message>
        <source>Tribal members cannot be empty!</source>
        <translation type="vanished">Члены племени не могут быть пустыми!</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="499"/>
        <location filename="creategroupwidget.cpp" line="609"/>
        <source>、</source>
        <translation></translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="506"/>
        <source>…</source>
        <translation></translation>
    </message>
    <message>
        <source>Tribal creation failed</source>
        <translation type="vanished">v</translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="613"/>
        <source> invited </source>
        <translation> приглашенный </translation>
    </message>
    <message>
        <location filename="creategroupwidget.cpp" line="613"/>
        <source> to join the group</source>
        <translation>Присоединиться к группе</translation>
    </message>
    <message>
        <source> to join the tribe</source>
        <translation type="vanished"> присоединиться к племени</translation>
    </message>
</context>
<context>
    <name>IMAddPerson</name>
    <message>
        <location filename="imaddperson.ui" line="14"/>
        <location filename="imaddperson.cpp" line="81"/>
        <source>Add friends</source>
        <translation>добавлять</translation>
    </message>
    <message>
        <location filename="imaddperson.ui" line="117"/>
        <source>Find friend</source>
        <translation>Найти друга</translation>
    </message>
    <message>
        <location filename="imaddperson.ui" line="170"/>
        <source>Find group</source>
        <translation>Найти группу</translation>
    </message>
    <message>
        <source>Find tribe</source>
        <translation type="vanished">Найти племя</translation>
    </message>
    <message>
        <location filename="imaddperson.ui" line="32"/>
        <source>Application number
service number</source>
        <translation>Номер приложения
сервисный номер</translation>
    </message>
</context>
<context>
    <name>IMSearchGroup</name>
    <message>
        <location filename="imsearchgroup.ui" line="14"/>
        <source>IMSearchGroup</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="21"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="22"/>
        <source>Group ID/Group Name</source>
        <oldsource>Tribe ID/Tribe Name</oldsource>
        <translation>ID группы / Название группы</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="71"/>
        <location filename="imsearchgroup.cpp" line="104"/>
        <location filename="imsearchgroup.cpp" line="179"/>
        <location filename="imsearchgroup.cpp" line="184"/>
        <location filename="imsearchgroup.cpp" line="189"/>
        <location filename="imsearchgroup.cpp" line="194"/>
        <location filename="imsearchgroup.cpp" line="263"/>
        <location filename="imsearchgroup.cpp" line="282"/>
        <location filename="imsearchgroup.cpp" line="302"/>
        <location filename="imsearchgroup.cpp" line="317"/>
        <source>Notice</source>
        <translation>уведомление</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="71"/>
        <source>Search content is empty!</source>
        <translation>Поиск контента пуст!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="104"/>
        <source>Searching group failed!</source>
        <translation>Поиск группы не удался!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="179"/>
        <location filename="imsearchgroup.cpp" line="184"/>
        <location filename="imsearchgroup.cpp" line="189"/>
        <source>Failed to get group information!</source>
        <translation>Не удалось получить информацию о группе!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="263"/>
        <source>Has already joined this group!</source>
        <translation>Уже вступил в эту группу!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="282"/>
        <source>Group apply succeed!</source>
        <translation>Подать заявку успешно!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="302"/>
        <source>Group apply failed!</source>
        <translation>Подать заявку на группу не удалось!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="359"/>
        <source> joined the group</source>
        <translation>Вступил в группу</translation>
    </message>
    <message>
        <source>Searching tribe failed!</source>
        <translation type="vanished">Поиск племени не удался!</translation>
    </message>
    <message>
        <source>Failed to get tribe information!</source>
        <translation type="vanished">Не удалось получить информацию о племени!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="194"/>
        <source>No qualified users were found!</source>
        <translation>Не найдено ни одного квалифицированного пользователя!</translation>
    </message>
    <message>
        <source>Has already joined this tribe!</source>
        <translation type="vanished">Уже присоединился к этому племени!</translation>
    </message>
    <message>
        <source>Tribe apply succeed!</source>
        <translation type="vanished">Племя применять успешно!</translation>
    </message>
    <message>
        <source>Tribe apply failed!</source>
        <translation type="vanished">Племя подать заявку не удалось!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="317"/>
        <source>Network anomaly!</source>
        <translation>Сетевая аномалия!</translation>
    </message>
    <message>
        <location filename="imsearchgroup.cpp" line="350"/>
        <source>:/GroupChat/Resources/groupchat/group.png</source>
        <translation></translation>
    </message>
    <message>
        <source> joined the tribe</source>
        <translation type="vanished"> присоединился к племени</translation>
    </message>
</context>
<context>
    <name>IMSearchPerson</name>
    <message>
        <location filename="imsearchperson.ui" line="14"/>
        <source>IMSearchPerson</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="22"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="23"/>
        <source>NickName/PhoneNumber</source>
        <translation>Ник / Номер телефона</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="71"/>
        <location filename="imsearchperson.cpp" line="104"/>
        <location filename="imsearchperson.cpp" line="167"/>
        <location filename="imsearchperson.cpp" line="174"/>
        <location filename="imsearchperson.cpp" line="183"/>
        <location filename="imsearchperson.cpp" line="206"/>
        <location filename="imsearchperson.cpp" line="223"/>
        <location filename="imsearchperson.cpp" line="229"/>
        <location filename="imsearchperson.cpp" line="344"/>
        <source>Notice</source>
        <translation>уведомление</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="71"/>
        <source>Search content is empty!</source>
        <translation>Поиск контента пуст!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="104"/>
        <source>Searching friend failed!</source>
        <translation>Поиск друга не удался!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="167"/>
        <source>Failed to get user information!</source>
        <translation>Не удалось получить информацию о пользователе!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="174"/>
        <source>No matching users!</source>
        <translation>Нет подходящих пользователей!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="183"/>
        <source>Can&apos;t add yourself as a friend!</source>
        <translation>Не могу добавить себя в друзья!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="206"/>
        <source>This user is already your friend!</source>
        <translation>Этот пользователь уже твой друг!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="223"/>
        <source>Friend apply succeed!</source>
        <translation>Подать заявку успешно!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="229"/>
        <source>Friend apply failed!</source>
        <translation>Подать заявку другу не удалось!</translation>
    </message>
    <message>
        <location filename="imsearchperson.cpp" line="344"/>
        <source>Network anomaly!</source>
        <translation>Сетевая аномалия!</translation>
    </message>
</context>
<context>
    <name>LeaveMessageWidget</name>
    <message>
        <location filename="leavemessagewidget.ui" line="32"/>
        <source>Add Friends</source>
        <translation>Добавить друзей</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="175"/>
        <source>Add</source>
        <translation>добавлять</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="464"/>
        <source>Verification Info</source>
        <translation>Информация о проверке</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="492"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;SimSun&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Microsoft YaHei&apos;; font-size:15px; font-weight:96;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="540"/>
        <source>Remarks</source>
        <translation>замечания</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="626"/>
        <source>Cancel</source>
        <translation>отменить</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.ui" line="673"/>
        <source>OK</source>
        <translation>Хорошо</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.cpp" line="136"/>
        <source>I am </source>
        <translation>я </translation>
    </message>
    <message>
        <location filename="leavemessagewidget.cpp" line="329"/>
        <source>ID:</source>
        <translation>ID:</translation>
    </message>
    <message>
        <location filename="leavemessagewidget.cpp" line="406"/>
        <source>:/GroupChat/Resources/groupchat/group.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="leavemessagewidget.cpp" line="422"/>
        <source>Group ID </source>
        <translation>ID группы</translation>
    </message>
    <message>
        <source>Tribe ID </source>
        <translation type="vanished">ID племени</translation>
    </message>
</context>
<context>
    <name>MyTitleBar</name>
    <message>
        <location filename="mytitlebar.cpp" line="55"/>
        <source>minimize</source>
        <translation>минимизировать</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="56"/>
        <source>restore</source>
        <translation>восстановить</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="57"/>
        <source>maximize</source>
        <translation>максимизировать</translation>
    </message>
    <message>
        <location filename="mytitlebar.cpp" line="58"/>
        <source>close</source>
        <translation>близко</translation>
    </message>
</context>
</TS>
