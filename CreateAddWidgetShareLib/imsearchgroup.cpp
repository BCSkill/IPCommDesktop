﻿#include "imsearchgroup.h"
#include "ui_imsearchgroup.h"
#include "messagebox.h"
#include "leavemessagewidget.h"
#ifndef Q_OS_WIN
#include "mycommonstyle.h"
#endif
#include "QStringLiteralBak.h"
#include "profilemanager.h"
#include "immainwidget.h"
#include "imtranstype.h"
#include "define.h"
#include "globalmanager.h"

IMSearchGroup::IMSearchGroup(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::IMSearchGroup();

	ui->setupUi(this);
	ui->mPButtonSearchGroup->setText(tr("Search"));
	ui->mlineEditSearchGroup->setPlaceholderText(tr("Group ID/Group Name"));
	m_pLoadWidget = NULL;

	QFile file(":/QSS/Resources/QSS/CreateAddWidgetShareLib/imsearchgroup.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->mPButtonSearchGroup, SIGNAL(clicked()), this, SLOT(doClickSearchGroup()));
	//connect(ui->mlineEditSearchGroup, SIGNAL(returnPressed()), this, SLOT(doClickSearchGroup()));
	connect(ui->tableWidget, SIGNAL(sigAddPerson(QString,int)), this, SLOT(doAddGroup(QString,int)));
	connect(ui->tableWidget, SIGNAL(sigOpenProfile(QString)), this, SLOT(slotOpenGrpProfile(QString)));
#ifndef Q_OS_WIN
    ui->mlineEditSearchGroup->setStyle(new MyProxyStyle);
    ui->tableWidget->setStyle(new MyProxyStyle);
#endif
}

IMSearchGroup::~IMSearchGroup()
{
	if (m_pLoadWidget)
	{
		delete m_pLoadWidget;
		m_pLoadWidget = NULL;
	}

	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

void IMSearchGroup::doClickSearchGroup()
{
	m_pLoadWidget = new QLoadingWidget(this);
	if (m_pLoadWidget)
	{
		m_pLoadWidget->show();
	}
	ui->tableWidget->clear();
	QString strData = ui->mlineEditSearchGroup->text();
	if (!strData.isEmpty())
	{
		OnSearchGroup(strData);
	}
	else
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("Search content is empty!"));
		//QMessageBox::information(this, tr("查找部落"), tr("输入搜索内容为空!"));
		if (m_pLoadWidget)
		{
			m_pLoadWidget->close();
			m_pLoadWidget = NULL;
		}
	}
}

void IMSearchGroup::OnSearchGroup(QString strData)
{
	mStrSaveSearchContent = strData;

	UserInfo userInfo = gDataManager->getUserInfo();

	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(doHttpSearchGroupResultInfo(bool, QString)));

	QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/search"
		+ QString("?userId=%1&passWord=%2&keyWord=%3").arg(userInfo.nUserID).arg(userInfo.strUserPWD).arg(strData);

	http->getHttpRequest(url);
}

void IMSearchGroup::doHttpSearchGroupResultInfo(bool bState, QString strResult)
{
	if (bState)
	{
		ParseSearchGroupResult(strResult);
	}
	else
	{
		IMessageBox::tip(this, tr("Notice"), tr("Searching group failed!"));
		//QMessageBox::information(this, tr("查找部落"), tr("查找部落失败!"));
	}
	if (m_pLoadWidget)
	{
		m_pLoadWidget->close();
		m_pLoadWidget = NULL;
	}
}

void IMSearchGroup::ParseSearchGroupResult(QString strResult)
{
	int nTemp = 0;
	QJsonParseError jsonError;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(strResult.toUtf8(), &jsonError);
	ui->tableWidget->m_iNum = 0;
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QJsonObject obj = jsonDocument.object();
			if (obj["result"].toString() == "success")
			{
				QVariant strTemp;
				QJsonValue userInfoValue;

				foreach(QString key, obj.keys())
				{
					userInfoValue = obj.take(key);
					strTemp = userInfoValue.toVariant();
					if (userInfoValue.isArray())
					{
						QVariantList userInfoList = strTemp.toList();
						for (int i = 0; i < userInfoList.size(); i++)
						{
							GroupInfo groupInfo;
							QVariantMap listData = userInfoList.at(i).toMap();
							QString strCreateTime = listData["createTime"].toString();
							QString strUserID = listData["createUserId"].toString();
							QString strGroupID = listData["groupId"].toString();
							QString strGroupName = listData["groupName"].toString();
							QString strHttpAvatar = listData["avatar"].toString();
							int iType = listData["groupType"].toInt();
							if (mStrSaveSearchContent == strGroupID)
							{
								//QString strTemp = QString("%1(%2)").arg(strGroupName).arg(strGroupID);
								//ui->tableWidget->OnInsertSearchGroupList(strGroupID, strHttpAvatar, strTemp);
								//nTemp++;
							}
							else //if (mStrSaveSearchContent == strGroupName)20181128 改为模糊查找
							{
								//ui->tableWidget->OnInsertSearchGroupList(strGroupID, strHttpAvatar, strGroupName);
								//nTemp++;
							}
							//////
							nTemp++;
							UserInfo userInfo = gDataManager->getUserInfo();
							HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
							http->setProperty("GroupID", strGroupID);
							http->setProperty("Avatar", strHttpAvatar);
							http->setProperty("Name", strGroupName);
							http->setProperty("Type", iType);
							connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(doHttpUserListResult(bool, QString)));

							QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/getUserListByGroupId"
								+ QString("?userId=%1&passWord=%2&groupId=%3").arg(userInfo.nUserID).arg(userInfo.strUserPWD).arg(strGroupID);

							http->getHttpRequest(url);
							//////
						}
					}
				}
			}
			else
			{
				IMessageBox::tip(NULL, tr("Notice"), tr("Failed to get group information!"));
				//QMessageBox::information(this, tr("查找部落"), tr("解析部落信息失败!"));
			}
		}
		else
			IMessageBox::tip(NULL, tr("Notice"), tr("Failed to get group information!"));
			//QMessageBox::information(this, tr("查找部落"), tr("解析部落信息失败!"));
	}
	else
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("Failed to get group information!"));
		//QMessageBox::information(this, tr("查找部落"), tr("解析部落信息失败!"));
	}
	if (nTemp == 0)
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("No qualified users were found!"));
	}
}

void IMSearchGroup::doHttpUserListResult(bool bstat, QString strRes)
{
	int iGrpSize;
	QJsonParseError jsonError;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(strRes.toUtf8(), &jsonError);
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QJsonObject obj = jsonDocument.object();
			if (obj["result"].toString() == "success")
			{
				QJsonValue userInfoValue = obj.take("users");
				QVariant strTemp = userInfoValue.toVariant();
				QVariantList userInfoList = strTemp.toList();
				iGrpSize = userInfoList.size();
				//QSize tsize = strTemp.Size();
				//iGrpSize = strTemp.toSize();
			}
		}
	}
	HttpNetWork::HttpNetWorkShareLib* http = qobject_cast<HttpNetWork::HttpNetWorkShareLib*>(sender());
	QVariant val = http->property("GroupID");
	QString strGroupID = val.value<QString>();
	val = http->property("Avatar");
	QString strHttpAvatar = val.value<QString>();
	val = http->property("Name");
	QString strGroupName = val.value<QString>();
	val = http->property("Type");
	int iType = val.value<int>();

	ui->tableWidget->OnInsertSearchGroupList(strGroupID, strHttpAvatar, strGroupName, iType,iGrpSize);
	//nTemp++;
}

void IMSearchGroup::doAddGroup(QString strGroupID,int iType)
{
	if (gDataBaseOpera)
	{
		if (!gDataBaseOpera->DBJudgeGroupIsHaveByID(strGroupID))
		{
			if (iType == 0)
			{
				UserInfo userInfo = gDataManager->getUserInfo();

				LeaveMessageWidget* MegWidget = new LeaveMessageWidget(this);
				MegWidget->OnInitGroupInfo(strGroupID);
				connect(MegWidget, SIGNAL(sigApplySuccessed()), this, SLOT(slotApplySuccessed()));
				connect(MegWidget, SIGNAL(sigApplyFailed()), this, SLOT(slotApplyFailed()));
				MegWidget->show();
			}
			else if(iType == 1)
			{
				UserInfo userInfo = gDataManager->getUserInfo();
				HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
				http->setProperty("GroupId", strGroupID);
				connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(doHttpAddGroupResultInfo(bool, QString)));
				QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/applyAddGroup"
					+ QString("?userId=%1&passWord=%2&groupId=%3").arg(userInfo.nUserID).arg(userInfo.strUserPWD).arg(strGroupID);
				http->getHttpRequest(url);

			}
		}
		else
		{
			IMessageBox::tip(NULL, tr("Notice"), tr("Has already joined this group!"));
			//QMessageBox::information(this, tr("请求部落"), tr("已经加入该部落!"));
		}
	}
}

void IMSearchGroup::doHttpAddGroupResultInfo(bool bResult,QString strResult)
{
	if (bResult)
	{
		QJsonParseError jsonError;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(strResult.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QJsonObject obj = jsonDocument.object();
				if (obj["result"].toString() == "success")
				{
					IMessageBox::tip(NULL, tr("Notice"), tr("Group apply succeed!"));
					//QMessageBox::information(this, tr("请求部落"), tr("请求部落成功!"));
					HttpNetWork::HttpNetWorkShareLib * act = qobject_cast<HttpNetWork::HttpNetWorkShareLib *>(sender());
					QString strGroupId = act->property("GroupId").toString();

					if (!gDataBaseOpera->DBJudgeGroupIsHaveByID(strGroupId))
					{
						HttpNetWork::HttpNetWorkShareLib *PersonInfo = new HttpNetWork::HttpNetWorkShareLib();
						connect(PersonInfo, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotRequestGroupInfoFinished(bool, QString)));
						UserInfo userInfo = gDataManager->getUserInfo();
						AppConfig appConf = gDataManager->getAppConfigInfo();
						PersonInfo->setProperty("GroupId", strGroupId);
						QString strResult = appConf.MessageServerAddress + HTTP_GETGROUPINFOBYGROUPID + QString("?userId=") + QString::number(userInfo.nUserID) + "&passWord=" + userInfo.strUserPWD + "&groupId=" + strGroupId;
						PersonInfo->getHttpRequest(strResult);
					}
					return;
				}
			}
		}
	}
	QMessageBox::information(this, tr("Notice"), tr("Group apply failed!"));
}

void IMSearchGroup::setItemFocus()
{
	ui->mlineEditSearchGroup->setFocus();
}

void IMSearchGroup::slotApplySuccessed()
{
	//IMessageBox::tip(this, tr("提示"), tr("请求成功！"));
}

void IMSearchGroup::slotApplyFailed()
{
	IMessageBox::tip(NULL, tr("Notice"), tr("Network anomaly!"));
}

void IMSearchGroup::slotOpenGrpProfile(QString strid)
{
	emit profilemanager::getInstance()->sigCreateGrpFile(strid);
}

void IMSearchGroup::slotRequestGroupInfoFinished(bool bResult, QString result)
{
	if (bResult)
	{
		QJsonParseError jsonError;
		GroupInfo groupInfo;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(result.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QVariantMap result = jsonDocument.toVariant().toMap();
				if (result["result"].toString() == "success")
				{
					QVariantMap resultInfo = result["group"].toMap();
					groupInfo.groupHttpHeadImage = resultInfo["avatar"].toString();
					groupInfo.createTime = resultInfo["createTime"].toString();
					groupInfo.createUserId = resultInfo["createUserId"].toString();
					groupInfo.groupId = resultInfo["groupId"].toString();
#ifdef Q_OS_WIN
					QString strPath = gSettingsManager->getUserPath();
					groupInfo.groupLoacalHeadImage = strPath + "/resource/header/groupheader/" + groupInfo.groupId + ".jpg";
#else
					groupInfo.groupLoacalHeadImage = gSettingsManager->getUserPath() + "/resource/header/groupheader/" + groupInfo.groupId + ".jpg";
#endif
					groupInfo.groupDefaultAvatar = tr(":/GroupChat/Resources/groupchat/group.png");
					groupInfo.groupName = resultInfo["groupName"].toString();
					groupInfo.noSpeak = resultInfo["noSpeak"].toInt();
					groupInfo.groupType = resultInfo["groupType"].toInt();
					groupInfo.groupDesc = resultInfo["groupDesc"].toString();
					groupInfo.groupKey = resultInfo["groupKey"].toString();
					gDataBaseOpera->DBInsertGroupInfo(groupInfo);

					UserInfo userInfo = gDataManager->getUserInfo();
					QString strNote = tr(" joined the group");
					strNote = userInfo.strUserNickName + strNote;
					QVariantMap map;
					map.insert("type", "notification");
					map.insert("content", strNote);
					emit IMMainWidget::self()->sigTipMessage(GroupMessage, groupInfo.groupId, QJsonDocument::fromVariant(map).toJson());

				}
			}
		}
	}
}

void IMSearchGroup::keyPressEvent(QKeyEvent * event)
{
	if (event->key() == Qt::Key_Return)
	{
		doClickSearchGroup();
	}
	return QWidget::keyPressEvent(event);
}