﻿#include "imdatabaseoperainfo.h"
#include <QDateTime>
#include <QDebug>
#include "QStringLiteralBak.h"
#include "globalmanager.h"
#include <QDir>
#ifndef Q_OS_WIN
#include "inline_mac.h"
#endif
#include "messageliststore.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonParseError>
#include <QJsonValue>

IMDataBaseOperaInfo::IMDataBaseOperaInfo(QObject *parent) : QObject(parent)
{
	mDateBase = NULL;
}

IMDataBaseOperaInfo::~IMDataBaseOperaInfo()
{

}

void IMDataBaseOperaInfo::ConnectDB(QString strDBPath, QString strDBName)
{
	if (mDateBase == NULL) mDateBase = new SqlLiteShareLib();
	if (mDateBase->CreateDataFile(strDBPath))
	{
		bool bErro = false;
		//#ifdef _DEBUG
		bErro = mDateBase->OnConnectDB(strDBPath, strDBName);
		//#else
		//	bErro = mSqlUtil->OnConnectDB(strDBPath, strDBName, SQLITE_PASSWORD);
		//#endif // DEBUG
		if (bErro)
		{
			if (!mDateBase->isExistTable("BUDDYINFO"))
			{
				//创建好友表
				QString strSql = "create table BUDDYINFO(BuddyID int primary key,UserName varchar(30),NickName varchar(30),Note varchar(30),Sign varchar(100),Sex varchar(10),Email varchar(50),MobilePhone varchar(30),Phone varchar(30),AvatarHttp varchar(100),AvatarDefault varchar(100),AvatarLocal varchar(100),UserType int,PingYin varchar(10),BuddyType int,updateTime varchar(30), disableStrangers int, msgPrompt int);";
				mDateBase->ExecuSql(strSql);
			}
			else
			{

				//BuddyType 类型为 是否是好友 1是好友 0是非好友
				QString strSql = QString::fromLocal8Bit("select * from sqlite_master where name='BUDDYINFO' and sql like '%BuddyType%'");
				QSqlQuery query = mDateBase->ExecuQuery(strSql);
				if (!query.next())
				{
					strSql = QString("ALTER TABLE BUDDYINFO ADD COLUMN BuddyType varchar");
					mDateBase->ExecuSql(strSql);
				}

				strSql = QString::fromLocal8Bit("select * from sqlite_master where name='BUDDYINFO' and sql like '%AvatarDefault%'");
				query = mDateBase->ExecuQuery(strSql);
				if (!query.next())
				{
					strSql = QString("ALTER TABLE BUDDYINFO ADD COLUMN AvatarDefault varchar");
					mDateBase->ExecuSql(strSql);
				}

				strSql = QString::fromLocal8Bit("select * from sqlite_master where name='BUDDYINFO' and sql like '%disableStrangers%'");
				query = mDateBase->ExecuQuery(strSql);
				if (!query.next())
				{
					strSql = QString("ALTER TABLE BUDDYINFO ADD COLUMN disableStrangers int");
					mDateBase->ExecuSql(strSql);
				}

				strSql = QString::fromLocal8Bit("select * from sqlite_master where name='BUDDYINFO' and sql like '%msgPrompt%'");
				query = mDateBase->ExecuQuery(strSql);
				if (!query.next())
				{
					strSql = QString("ALTER TABLE BUDDYINFO ADD COLUMN msgPrompt int");
					mDateBase->ExecuSql(strSql);
				}
			}
			if (!mDateBase->isExistTable("GROUPINFO"))
			{
				//部落信息
				QString strSql = "create table GROUPINFO(GroupID int primary key,GroupName varchar(100),CreateUserID int,CreateTime varchar(100), GroupDesc varchar(200), GroupKey varchar(30), GroupHttpAvatar varchar(100),GroupAvatar varchar(100),GroupDefaultAvatar varchar(100),updateTime varchar(30), noSpeak int, groupType int, msgPrompt int)";
				mDateBase->ExecuSql(strSql);
			}
			else
			{
				QString strSql = QString::fromLocal8Bit("select * from sqlite_master where name='GROUPINFO' and sql like '%GroupHttpAvatar%'");
				QSqlQuery query = mDateBase->ExecuQuery(strSql);
				if (!query.next())
				{
					strSql = QString("ALTER TABLE GROUPINFO ADD COLUMN GroupHttpAvatar varchar");
					mDateBase->ExecuSql(strSql);
				}

				strSql = QString::fromLocal8Bit("select * from sqlite_master where name='GROUPINFO' and sql like '%GroupDefaultAvatar%'");
				query = mDateBase->ExecuQuery(strSql);
				if (!query.next())
				{
					strSql = QString("ALTER TABLE GROUPINFO ADD COLUMN GroupDefaultAvatar varchar");
					mDateBase->ExecuSql(strSql);
				}
				strSql = QString::fromLocal8Bit("select * from sqlite_master where name='GROUPINFO' and sql like '%noSpeak%'");
				query = mDateBase->ExecuQuery(strSql);
				if (!query.next())
				{
					strSql = QString("ALTER TABLE GROUPINFO ADD COLUMN noSpeak int");
					mDateBase->ExecuSql(strSql);
				}
				strSql = QString::fromLocal8Bit("select * from sqlite_master where name='GROUPINFO' and sql like '%groupType%'");
				query = mDateBase->ExecuQuery(strSql);
				if (!query.next())
				{
					strSql = QString("ALTER TABLE GROUPINFO ADD COLUMN groupType int");
					mDateBase->ExecuSql(strSql);
				}
				strSql = QString::fromLocal8Bit("select * from sqlite_master where name='GROUPINFO' and sql like '%GroupDesc%'");
				query = mDateBase->ExecuQuery(strSql);
				if (!query.next())
				{
					strSql = QString("ALTER TABLE GROUPINFO ADD COLUMN GroupDesc varchar(200)");
					mDateBase->ExecuSql(strSql);
				}
				strSql = QString::fromLocal8Bit("select * from sqlite_master where name='GROUPINFO' and sql like '%GroupKey%'");
				query = mDateBase->ExecuQuery(strSql);
				if (!query.next())
				{
					strSql = QString("ALTER TABLE GROUPINFO ADD COLUMN GroupKey varchar(30)");
					mDateBase->ExecuSql(strSql);
				}
				strSql = QString::fromLocal8Bit("select * from sqlite_master where name='GROUPINFO' and sql like '%msgPrompt%'");
				query = mDateBase->ExecuQuery(strSql);
				if (!query.next())
				{
					strSql = QString("ALTER TABLE GROUPINFO ADD COLUMN msgPrompt int");
					mDateBase->ExecuSql(strSql);
				}
			}
			if (!mDateBase->isExistTable("GROUPUSERINFO"))
			{
				//部落成员信息
				QString strSql = "create table GROUPUSERINFO(GroupUserID int,GroupID int,UserName varchar(30),NickName varchar(30),Note varchar(30),Sign varchar(100),Sex varchar(10),Email varchar(50),MobilePhone varchar(30),Phone varchar(30),AvatarHttp varchar(100),AvatarLocal varchar(100),AvatarDefault varchar(100),UserType int,updateTime varchar(30), disableStrangers int)";
				mDateBase->ExecuSql(strSql);
			}
			else {
				QString strSql = "create table t2(GroupUserID int,GroupID int,UserName varchar(30),NickName varchar(30),Note varchar(30),Sign varchar(100),Sex varchar(10),Email varchar(50),MobilePhone varchar(30),Phone varchar(30),AvatarHttp varchar(100),AvatarLocal varchar(100),AvatarDefault varchar(100),UserType int,updateTime varchar(30))";
				mDateBase->ExecuSql(strSql);
				strSql = "insert into t2(GroupUserID,GroupID,UserName,NickName,Note,Sign,Sex,Email,MobilePhone,Phone,AvatarHttp,AvatarLocal,AvatarDefault,UserType,updateTime) \
						 						 						 						 						 select GroupUserID,GroupID,UserName,NickName,Note,Sign,Sex,Email,MobilePhone,Phone,AvatarHttp,AvatarLocal,AvatarDefault,UserType,updateTime from GROUPUSERINFO";
				mDateBase->ExecuSql(strSql);
				strSql = "drop table GROUPUSERINFO";
				mDateBase->ExecuSql(strSql);

				strSql = "create table GROUPUSERINFO as select * from t2";
				mDateBase->ExecuSql(strSql);

				strSql = "drop table t2";

				mDateBase->ExecuSql(strSql);


				strSql = QString::fromLocal8Bit("select * from sqlite_master where name='GROUPUSERINFO' and sql like '%AvatarDefault%'");
				QSqlQuery query = mDateBase->ExecuQuery(strSql);
				if (!query.next())
				{
					strSql = QString("ALTER TABLE GROUPUSERINFO ADD COLUMN AvatarDefault varchar");
					mDateBase->ExecuSql(strSql);
				}

				//添加是否允许陌生人消息的字段。
				QString strsql2 = "ALTER TABLE GROUPUSERINFO ADD disableStrangers int";
				mDateBase->ExecuSql(strsql2);
			}
			if (!mDateBase->isExistTable("LOCALFILE"))
			{
				/*文件下载信息表*/
				QString strSql = "create table LOCALFILE(MSGID varchar(200),FileHttpPath varchar(200),FileLocalPath varchar(200))";
				mDateBase->ExecuSql(strSql);
			}

			if (!mDateBase->isExistTable("MESSAGELIST"))
			{
				QString strSql = "create table MESSAGELIST(BUDDYID int,BUDDYNAME varchar(100),BUDDYHEADERIMAGE varchar(100),LASTMESSAGE varchar(200),LastMsgJson varchar(200),LASTMESSAGETIME int,UNREADMESSGAENUM int,IsGroup int,messageType int,updateTime varchar(30),MsgTopOrder int,MsgPrompt int)";
				mDateBase->ExecuSql(strSql);
			}
			else
			{
				QString strSql = QString::fromLocal8Bit("select * from sqlite_master where name='MESSAGELIST' and sql like '%LastMsgJson%'");
				QSqlQuery query = mDateBase->ExecuQuery(strSql);
				if (!query.next())
				{
					//将LASTMESSAGE=>LastMsgJson
					/*[
					{
					"type": 1

					},
					{
					"type": 0,
					"msg" : "有什么事吗？"
					}
					]*/

					//1.添加LastMsgJson字段

					strSql = QString("ALTER TABLE MESSAGELIST ADD COLUMN LastMsgJson varchar(200)");
					mDateBase->ExecuSql(strSql);


					//2.转换LASTMESSAGE到LastMsgJson
					QList<MessageListInfo> listMessageListInfo;
					if (mDateBase)
					{
						QString strSql = QString("select * from MESSAGELIST");
						QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
						while (resultQuery.next())
						{
							int nBudyyID = resultQuery.value("BUDDYID").toInt();
							QString strLastMessage = resultQuery.value("LASTMESSAGE").toString();
							QJsonArray jsonArr;

							QJsonObject object;
							object.insert("type", 0);
							object.insert("msg", strLastMessage);

							jsonArr.append(object);

							QJsonDocument document;
							document.setArray(jsonArr);
							QString strJson = (QString)document.toJson();

							QString strSql = QString("update MESSAGELIST set LastMsgJson='%1' where BUDDYID=%2").arg(strJson.replace("\'", "\'\'"), QString::number(nBudyyID));
							mDateBase->ExecuQuery(strSql);

						}
					}
				}

				//新增消息置顶顺序标志
				strSql = QString::fromLocal8Bit("select * from sqlite_master where name='MESSAGELIST' and sql like '%MsgTopOrder%'");
				query = mDateBase->ExecuQuery(strSql);
				if (!query.next())
				{
					strSql = QString("ALTER TABLE MESSAGELIST ADD COLUMN MsgTopOrder int");
					mDateBase->ExecuSql(strSql);
				}

				strSql = QString::fromLocal8Bit("select * from sqlite_master where name='MESSAGELIST' and sql like '%MsgPrompt%'");
				query = mDateBase->ExecuQuery(strSql);
				if (!query.next())
				{
					strSql = QString("ALTER TABLE MESSAGELIST ADD COLUMN MsgPrompt int");
					mDateBase->ExecuSql(strSql);
				}
			}

			if (!mDateBase->isExistTable("APPLYMESSAGE"))
			{
				QString strsql = "create table APPLYMESSAGE(ApplyId int, ApplyName varchar(100), UserId int, GroupId int, GroupName varchar(100), ApplyType int, Agree int)";
				mDateBase->ExecuSql(strsql);
			}
			if (!mDateBase->isExistTable("ORGTREE"))
			{
				QString strsql = "CREATE TABLE ORGTREE (MC varchar(100), BH varchar(100), JS int, MX varchar(100), TYPE varchar(100), GROUP_ID int, USER_ID int, PARENT_BH varchar(100))";
				mDateBase->ExecuSql(strsql);
			}
			if (!mDateBase->isExistTable("ADDAPPLYMESSAGE"))
			{
				QString strsql = "CREATE TABLE ADDAPPLYMESSAGE (MessageId varchar(100), Type int, Id int, Message varchar(200),State int ,Date varchar(100),GroupId varchar(100),Read int)";
				mDateBase->ExecuSql(strsql);
			}
			else
			{
				QString strSql = QString::fromLocal8Bit("select * from sqlite_master where name='ADDAPPLYMESSAGE' and sql like '%Read%'");
				QSqlQuery query = mDateBase->ExecuQuery(strSql);
				if (!query.next())
				{
					strSql = QString("ALTER TABLE ADDAPPLYMESSAGE ADD COLUMN Read int");
					mDateBase->ExecuSql(strSql);
					strSql = QString("update ADDAPPLYMESSAGE set Read=1");
					mDateBase->ExecuSql(strSql);
				}
			}
		}
		else
		{
			qDebug() << QStringLiteralBak("连接数据库失败!");
		}
	}
}

QList<BuddyInfo> IMDataBaseOperaInfo::DBGetBuddyInfo()
{
	QList<BuddyInfo> mlistBuddyInfo;
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QString strSql = QString("select * from BUDDYINFO");
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
		while (resultQuery.next())
		{
			BuddyInfo buddyInfo;
			buddyInfo.strHttpAvatar = resultQuery.value("AvatarHttp").toString();
			buddyInfo.strLocalAvatar = resultQuery.value("AvatarLocal").toString();
			buddyInfo.strEmail = resultQuery.value("Email").toString();
			buddyInfo.strMobilePhone = resultQuery.value("MobilePhone").toString();
			buddyInfo.strNickName = resultQuery.value("NickName").toString();
			buddyInfo.strNote = resultQuery.value("Note").toString();
			buddyInfo.strPhone = resultQuery.value("Phone").toString();
			buddyInfo.strSex = resultQuery.value("Sex").toString();
			buddyInfo.strSign = resultQuery.value("Sign").toString();
			buddyInfo.nUserId = resultQuery.value("BuddyID").toInt();
			buddyInfo.strUserName = resultQuery.value("UserName").toString();
			buddyInfo.nUserType = resultQuery.value("UserType").toInt();
			buddyInfo.strPingYin = resultQuery.value("PingYin").toString();
			buddyInfo.BuddyType = resultQuery.value("BuddyType").toInt();
			buddyInfo.strDefaultAvatar = resultQuery.value("AvatarDefault").toString();
			buddyInfo.disableStrangers = resultQuery.value("disableStrangers").toInt();
			buddyInfo.msgPrompt = resultQuery.value("MsgPrompt").toInt();
			buddyInfo.strLocalAvatar = gSettingsManager->getUserPath() + buddyInfo.strLocalAvatar;
			mlistBuddyInfo.append(buddyInfo);
		}
	}
	return mlistBuddyInfo;
}

void IMDataBaseOperaInfo::DBInsertGroupInfo(GroupInfo groupInfo)
{
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QString strSql;
		//先存储部落信息
		strSql = QString("delete from GROUPINFO where GroupID = %1").arg(groupInfo.groupId);
		mDateBase->ExecuSql(strSql);
		groupInfo.groupLoacalHeadImage = groupInfo.groupLoacalHeadImage.replace(gSettingsManager->getUserPath(), "");//兼容路径
		QDateTime time = QDateTime::currentDateTime();//获取系统现在的时间
		QString str = time.toString("yyyy-MM-dd hh:mm:ss"); //设置显示格式
		strSql = QString("insert into GROUPINFO(GroupID,GroupName,CreateUserID,CreateTime,"
			"GroupAvatar,GroupHttpAvatar,GroupDefaultAvatar,updateTime,noSpeak,groupType,GroupDesc,GroupKey, msgPrompt) values(%1,'%2',%3,'%4','%5','%6','%7','%8', %9, %10, '%11', '%12', %13)")
			.arg(groupInfo.groupId).arg(groupInfo.groupName.replace("\'", "\'\'")).arg(groupInfo.createUserId)
			.arg(groupInfo.createTime).arg(groupInfo.groupLoacalHeadImage).arg(groupInfo.groupHttpHeadImage)
			.arg(groupInfo.groupDefaultAvatar).arg(str).arg(groupInfo.noSpeak).arg(groupInfo.groupType)
			.arg(groupInfo.groupDesc.replace("\'", "\'\'")).arg(groupInfo.groupKey).arg(groupInfo.msgPrompt);
		mDateBase->ExecuSql(strSql);
	}
}

//插入部落好友信息
void IMDataBaseOperaInfo::DBInsertGroupBuddyInfo(QString strGroupID, BuddyInfo buddyInfo)
{
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QString strSql;
		strSql = QString("delete from GROUPUSERINFO where GroupID = %1 and GroupUserID = %2").arg(strGroupID).arg(buddyInfo.nUserId);
		mDateBase->ExecuSql(strSql);
		buddyInfo.strLocalAvatar = buddyInfo.strLocalAvatar.replace(gSettingsManager->getUserPath(), "");
		QDateTime time = QDateTime::currentDateTime();//获取系统现在的时间
		QString strTime = time.toString("yyyy-MM-dd hh:mm:ss"); //设置显示格式
		strSql = QString("insert into GROUPUSERINFO(GroupUserID,GroupID,UserName,NickName\
		,Note,Sign,Sex,Email,MobilePhone,Phone,AvatarHttp,AvatarLocal,AvatarDefault,UserType\
		,updateTime,disableStrangers) values(%1,%2,'%3','%4','%5','%6','%7','%8','%9',\
		'%10','%11','%12','%13',%14,'%15',%16)").arg(buddyInfo.nUserId).arg(
	strGroupID).arg(buddyInfo.strUserName).arg(buddyInfo.strNickName.replace("\'", "\'\'"))
			.arg(buddyInfo.strNote.replace("\'", "\'\'")).arg(buddyInfo.strSign.replace("\'", "\'\'")).arg(buddyInfo.strSex)
			.arg(buddyInfo.strEmail).arg(buddyInfo.strMobilePhone).arg(buddyInfo.strPhone)
			.arg(buddyInfo.strHttpAvatar).arg(buddyInfo.strLocalAvatar).arg(buddyInfo.strDefaultAvatar).arg(buddyInfo.nUserType)
			.arg(strTime).arg(buddyInfo.disableStrangers);
		mDateBase->ExecuSql(strSql);
	}
}

void IMDataBaseOperaInfo::DBUpdateAllGroupUserInfo(BuddyInfo buddyInfo)
{
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		buddyInfo.strLocalAvatar = buddyInfo.strLocalAvatar.replace(gSettingsManager->getUserPath(), "");
		QDateTime time = QDateTime::currentDateTime();//获取系统现在的时间
		QString strTime = time.toString("yyyy-MM-dd hh:mm:ss"); //设置显示格式

		QString strSql = QString("update GROUPUSERINFO set UserName='%1', NickName='%2'\
			, Sign='%3', Sex='%4', Email='%5', MobilePhone='%6', Phone='%7', AvatarHttp='%8', AvatarLocal='%9', AvatarDefault='%10'\
			, updateTime='%11', disableStrangers=%12 where GroupUserID=%13")
			.arg(buddyInfo.strUserName).arg(buddyInfo.strNickName.replace("\'", "\'\'"))
			.arg(buddyInfo.strSign.replace("\'", "\'\'")).arg(buddyInfo.strSex)
			.arg(buddyInfo.strEmail).arg(buddyInfo.strMobilePhone).arg(buddyInfo.strPhone)
			.arg(buddyInfo.strHttpAvatar).arg(buddyInfo.strLocalAvatar).arg(buddyInfo.strDefaultAvatar)
			.arg(strTime).arg(buddyInfo.disableStrangers).arg(buddyInfo.nUserId);

		mDateBase->ExecuSql(strSql);
	}
}

void IMDataBaseOperaInfo::DBUpdateAllGroupNickName(int UserID, QString nickName)
{
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QString strSql = QString("update GROUPUSERINFO set NickName='%1' where GroupUserID=%2").arg(nickName).arg(UserID);
		mDateBase->ExecuSql(strSql);
	}
}

void IMDataBaseOperaInfo::DBUpdateGroupUserNote(int GroupId, int UserID, QString strNote)
{
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QString strsql = QString("update GROUPUSERINFO set Note='%1' where GroupUserID=%2 and GroupID=%3").arg(strNote).arg(UserID).arg(GroupId);
		mDateBase->ExecuSql(strsql);
	}
}

//获取部落信息
QMap<GroupInfo, QList<BuddyInfo> > IMDataBaseOperaInfo::DBGetGroupInfo()
{
	QMap<GroupInfo, QList<BuddyInfo> > groupInfoTemp;
	QString strSql = QString("select * from GROUPINFO");
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
		while (resultQuery.next())
		{
			GroupInfo groupInfo;
			groupInfo.groupId = resultQuery.value("GroupID").toString();
			groupInfo.groupName = resultQuery.value("GroupName").toString();
			groupInfo.createTime = resultQuery.value("CreateTime").toString();
			groupInfo.createUserId = resultQuery.value("CreateUserID").toString();
			groupInfo.groupLoacalHeadImage = resultQuery.value("GroupAvatar").toString();
			groupInfo.groupHttpHeadImage = resultQuery.value("GroupHttpAvatar").toString();
			groupInfo.groupDefaultAvatar = resultQuery.value("GroupDefaultAvatar").toString();
			groupInfo.noSpeak = resultQuery.value("noSpeak").toInt();
			groupInfo.groupType = resultQuery.value("groupType").toInt();
			groupInfo.groupDesc = resultQuery.value("GroupDesc").toString();
			groupInfo.groupKey = resultQuery.value("GroupKey").toString();
			groupInfo.msgPrompt = resultQuery.value("msgPrompt").toInt();
			groupInfo.groupLoacalHeadImage = gSettingsManager->getUserPath() + groupInfo.groupLoacalHeadImage;

			QList<BuddyInfo > buddyInfo;
			groupInfoTemp.insert(groupInfo, buddyInfo);

			qDebug() << "groupInfo.msgPrompt : " << groupInfo.msgPrompt;
		}
	}
	QMap<GroupInfo, QList<BuddyInfo> >::iterator itor = groupInfoTemp.begin();
	for (; itor != groupInfoTemp.end(); ++itor)
	{
		strSql = QString("select * from GROUPUSERINFO where GroupID = %1").arg(itor.key().groupId);
		QSqlQuery result = mDateBase->ExecuQuery(strSql);
		while (result.next())
		{
			BuddyInfo buddy;
			buddy.nUserId = result.value("GroupUserID").toInt();
			buddy.nUserType = result.value("UserType").toInt();
			buddy.strEmail = result.value("Email").toString();
			buddy.strHttpAvatar = result.value("AvatarHttp").toString();
			buddy.strLocalAvatar = result.value("AvatarLocal").toString();
			buddy.strMobilePhone = result.value("MobilePhone").toString();
			buddy.strNickName = result.value("NickName").toString();
			buddy.strNote = result.value("Note").toString();
			buddy.strPhone = result.value("Phone").toString();
			buddy.strSex = result.value("Sex").toString();
			buddy.strSign = result.value("Sign").toString();
			buddy.strUserName = result.value("UserName").toString();
			buddy.strDefaultAvatar = result.value("AvatarDefault").toString();
			buddy.disableStrangers = result.value("disableStrangers").toInt();
			buddy.strLocalAvatar = gSettingsManager->getUserPath() + buddy.strLocalAvatar;
			itor.value().append(buddy);
		}
	}
	return groupInfoTemp;
}

GroupInfo IMDataBaseOperaInfo::DBGetGroupFromID(QString strGroupID)
{
	GroupInfo groupInfo;
	groupInfo.groupId = "";
	QString strSql = QString("select * from GROUPINFO where GroupID = %1").arg(strGroupID);
	QSqlQuery result = mDateBase->ExecuQuery(strSql);
	while (result.next())
	{
		groupInfo.createTime = result.value("CreateTime").toString();
		groupInfo.createUserId = result.value("CreateUserId").toString();
		groupInfo.groupId = result.value("GroupID").toString();
		groupInfo.groupLoacalHeadImage = result.value("GroupAvatar").toString();
		groupInfo.groupDefaultAvatar = result.value("GroupDefaultAvatar").toString();
		groupInfo.groupHttpHeadImage = result.value("GroupHttpAvatar").toString();
		groupInfo.groupName = result.value("GroupName").toString();
		groupInfo.noSpeak = result.value("noSpeak").toInt();
		groupInfo.groupType = result.value("groupType").toInt();
		groupInfo.groupDesc = result.value("GroupDesc").toString();
		groupInfo.groupKey = result.value("GroupKey").toString();
		groupInfo.msgPrompt = result.value("msgPrompt").toInt();
		groupInfo.groupLoacalHeadImage = gSettingsManager->getUserPath() + groupInfo.groupLoacalHeadImage;
	}
	return groupInfo;
}

QList<BuddyInfo> IMDataBaseOperaInfo::DBGetGroupBuddyInfoFromID(QString strGroupID)
{
	QList<BuddyInfo> listBuddyInfo;
	QString strSql = QString("select * from GROUPUSERINFO where GroupID = %1 ORDER BY UserType DESC").arg(strGroupID);
	QSqlQuery result = mDateBase->ExecuQuery(strSql);
	while (result.next())
	{
		BuddyInfo buddy;
		buddy.nUserId = result.value("GroupUserID").toInt();
		buddy.nUserType = result.value("UserType").toInt();
		buddy.strEmail = result.value("Email").toString();
		buddy.strHttpAvatar = result.value("AvatarHttp").toString();
		buddy.strLocalAvatar = result.value("AvatarLocal").toString();
		buddy.strMobilePhone = result.value("MobilePhone").toString();
		buddy.strNickName = result.value("NickName").toString();
		buddy.strNote = result.value("Note").toString();
		buddy.strPhone = result.value("Phone").toString();
		buddy.strSex = result.value("Sex").toString();
		buddy.strSign = result.value("Sign").toString();
		buddy.strUserName = result.value("UserName").toString();
		buddy.strDefaultAvatar = result.value("AvatarDefault").toString();
		buddy.disableStrangers = result.value("disableStrangers").toInt();
		buddy.strLocalAvatar = gSettingsManager->getUserPath() + buddy.strLocalAvatar;
		listBuddyInfo.append(buddy);
	}
	return listBuddyInfo;
}

void IMDataBaseOperaInfo::DBUpdateGroupInfo(GroupInfo groupInfo)
{
	QString strSql;
	strSql = QString("delete from GROUPINFO where GroupID = %1").arg(groupInfo.groupId);
	mDateBase->ExecuSql(strSql);
	groupInfo.groupLoacalHeadImage = groupInfo.groupLoacalHeadImage.replace(gSettingsManager->getUserPath(), "");
	QDateTime time = QDateTime::currentDateTime();//获取系统现在的时间
	QString str = time.toString("yyyy-MM-dd hh:mm:ss"); //设置显示格式
	strSql = QString("insert into GROUPINFO(GroupID,GroupName,CreateUserID,CreateTime,"
		"GroupAvatar,GroupDefaultAvatar,updateTime,noSpeak,groupType,GroupDesc,GroupKey,msgPrompt) values(%1,'%2',%3,'%4','%5','%6','%7',%8,%9, '%10', '%11', %12)").arg(groupInfo.groupId).arg(groupInfo.groupName.replace("\'", "\'\'")).arg(groupInfo.createUserId)
		.arg(groupInfo.createTime).arg(groupInfo.groupLoacalHeadImage).arg(groupInfo.groupDefaultAvatar).arg(str).arg(groupInfo.noSpeak).arg(groupInfo.groupType).arg(groupInfo.groupDesc.replace("\'", "\'\'")).arg(groupInfo.groupKey).arg(groupInfo.msgPrompt);
	mDateBase->ExecuSql(strSql);
}

BuddyInfo IMDataBaseOperaInfo::DBGetBuddyInfoByID(QString nUserID)
{
	BuddyInfo buddy;
	buddy.nUserId = -1;
	QString strSql = QString("select * from BUDDYINFO where BuddyID = %1").arg(nUserID);
	QSqlQuery result = mDateBase->ExecuQuery(strSql);
	while (result.next())
	{
		buddy.nUserId = result.value("BuddyID").toInt();
		buddy.nUserType = result.value("UserType").toInt();
		buddy.strEmail = result.value("Email").toString();
		buddy.strHttpAvatar = result.value("AvatarHttp").toString();
		buddy.strLocalAvatar = result.value("AvatarLocal").toString();
		buddy.strMobilePhone = result.value("MobilePhone").toString();
		buddy.strNickName = result.value("NickName").toString();
		buddy.strNote = result.value("Note").toString();
		buddy.strPhone = result.value("Phone").toString();
		buddy.strSex = result.value("Sex").toString();
		buddy.strSign = result.value("Sign").toString();
		buddy.strUserName = result.value("UserName").toString();
		buddy.strPingYin = result.value("PingYin").toString();
		buddy.BuddyType = result.value("BuddyType").toInt();
		buddy.strDefaultAvatar = result.value("AvatarDefault").toString();
		buddy.disableStrangers = result.value("disableStrangers").toInt();
		buddy.msgPrompt = result.value("MsgPrompt").toInt();
		buddy.strLocalAvatar = gSettingsManager->getUserPath() + buddy.strLocalAvatar;
	}
	return buddy;
}

BuddyInfo IMDataBaseOperaInfo::DBGetGroupUserFromID(QString nUserID)
{
	BuddyInfo buddy;
	buddy.nUserId = -1;
	QString strSql = QString("select * from GROUPUSERINFO where GroupUserID = %1").arg(nUserID);
	QSqlQuery result = mDateBase->ExecuQuery(strSql);
	while (result.next())
	{
		buddy.nUserId = result.value("GroupUserID").toInt();
		buddy.nUserType = result.value("UserType").toInt();
		buddy.strEmail = result.value("Email").toString();
		buddy.strHttpAvatar = result.value("AvatarHttp").toString();
		buddy.strLocalAvatar = result.value("AvatarLocal").toString();
		buddy.strMobilePhone = result.value("MobilePhone").toString();
		buddy.strNickName = result.value("NickName").toString();
		buddy.strNote = result.value("Note").toString();
		buddy.strPhone = result.value("Phone").toString();
		buddy.strSex = result.value("Sex").toString();
		buddy.strSign = result.value("Sign").toString();
		buddy.strUserName = result.value("UserName").toString();
		buddy.strDefaultAvatar = result.value("AvatarDefault").toString();
		buddy.disableStrangers = result.value("disableStrangers").toInt();
		buddy.strLocalAvatar = gSettingsManager->getUserPath() + buddy.strLocalAvatar;
	}
	return buddy;
}

QList<BuddyInfo> IMDataBaseOperaInfo::DBSearchBuddyInfo()
{
	QList<BuddyInfo> buddyList;
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QString strSql = QString("select * from buddyInfo where BuddyType=1)");
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
		while (resultQuery.next())
		{
			BuddyInfo buddyInfo;
			buddyInfo.strHttpAvatar = resultQuery.value("AvatarHttp").toString();
			buddyInfo.strLocalAvatar = resultQuery.value("AvatarLocal").toString();
			buddyInfo.strEmail = resultQuery.value("Email").toString();
			buddyInfo.strMobilePhone = resultQuery.value("MobilePhone").toString();
			buddyInfo.strNickName = resultQuery.value("NickName").toString();
			buddyInfo.strNote = resultQuery.value("Note").toString();
			buddyInfo.strPhone = resultQuery.value("Phone").toString();
			buddyInfo.strSex = resultQuery.value("Sex").toString();
			buddyInfo.strSign = resultQuery.value("Sign").toString();
			buddyInfo.nUserId = resultQuery.value("BuddyID").toInt();
			buddyInfo.strUserName = resultQuery.value("UserName").toString();
			buddyInfo.nUserType = resultQuery.value("UserType").toInt();
			buddyInfo.strPingYin = resultQuery.value("PingYin").toString();
			buddyInfo.BuddyType = resultQuery.value("BuddyType").toInt();
			buddyInfo.strDefaultAvatar = resultQuery.value("AvatarDefault").toString();
			buddyInfo.disableStrangers = resultQuery.value("disableStrangers").toInt();
			buddyInfo.strLocalAvatar = gSettingsManager->getUserPath() + buddyInfo.strLocalAvatar;
			buddyList.append(buddyInfo);
		}
	}
	return buddyList;
}

QList<GroupInfo> IMDataBaseOperaInfo::DBSearchGroupInfo()
{
	QList<GroupInfo> groupList;
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QString strSql = QString("select * from groupinfo");
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
		while (resultQuery.next())
		{
			GroupInfo groupInfo;
			groupInfo.groupId = QString::number(resultQuery.value("GroupID").toInt());
			groupInfo.createUserId = QString::number(resultQuery.value("CreateUserID").toInt());
			groupInfo.groupName = resultQuery.value("GroupName").toString();
			groupInfo.groupLoacalHeadImage = resultQuery.value("GroupAvatar").toString();
			groupInfo.groupDefaultAvatar = resultQuery.value("GroupDefaultAvatar").toString();
			groupInfo.noSpeak = resultQuery.value("noSpeak").toInt();
			groupInfo.groupType = resultQuery.value("groupType").toInt();
			groupInfo.groupDesc = resultQuery.value("GroupDesc").toString();
			groupInfo.groupKey = resultQuery.value("GroupKey").toString();
			groupInfo.groupLoacalHeadImage = gSettingsManager->getUserPath() + groupInfo.groupLoacalHeadImage;
			groupList.append(groupInfo);
		}
	}
	return groupList;
}

QList<BuddyInfo> IMDataBaseOperaInfo::DBSearchGroupUser(int groupID, QString key)
{
	QList<BuddyInfo> buddyList;
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QString strSql = QString("select * from groupuserinfo where groupid=%1 and (nickname like '%%2%'or note like '%%3%')").arg(groupID).arg(key).arg(key);
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
		while (resultQuery.next())
		{
			BuddyInfo buddyInfo;
			buddyInfo.strLocalAvatar = resultQuery.value("AvatarLocal").toString();
			buddyInfo.strNickName = resultQuery.value("NickName").toString();
			buddyInfo.strNote = resultQuery.value("Note").toString();
			buddyInfo.nUserId = resultQuery.value("GroupUserID").toInt();
			buddyInfo.strDefaultAvatar = resultQuery.value("AvatarDefault").toString();
			buddyInfo.disableStrangers = resultQuery.value("disableStrangers").toInt();
			buddyInfo.strLocalAvatar = gSettingsManager->getUserPath() + buddyInfo.strLocalAvatar;
			buddyList.append(buddyInfo);
		}
	}
	return buddyList;
}

void IMDataBaseOperaInfo::DBUpdateGroupBuddyHeaderImg(QString strBuddyID, QString strGroupID, QString strPath)
{
	strPath = strPath.replace(gSettingsManager->getUserPath(), "");
	QString strSql = QString("update GROUPUSERINFO set AvatarLocal = '%1' where GroupUserID = %2 and GroupID = %3").arg(strPath).arg(strBuddyID.toInt()).arg(strGroupID.toInt());
	mDateBase->ExecuQuery(strSql);
}

void IMDataBaseOperaInfo::DBDeleteBuddyInfoByID(QString strBuddyID)
{
	QString strSql = QString("delete from BUDDYINFO where BuddyID = %1").arg(strBuddyID);
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		mDateBase->ExecuSql(strSql);
	}
}

void IMDataBaseOperaInfo::DBDeleteGroupUserByID(QString strBuddyID, QString strUserID)
{
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		//删除部落成员表
		QString strSql = QString("delete from GROUPUSERINFO where GroupID = %1 and GroupUserID = %2").arg(strBuddyID).arg(strUserID);
		mDateBase->ExecuSql(strSql);
	}
}

void IMDataBaseOperaInfo::DBDeleteGroupInfoByID(QString strBuddyID)
{
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QString strSql = QString("delete from GROUPINFO where GroupID = %1").arg(strBuddyID);
		mDateBase->ExecuSql(strSql);
		//删除部落成员表
		strSql = QString("delete from GROUPUSERINFO where GroupID = %1").arg(strBuddyID);
		mDateBase->ExecuSql(strSql);
	}
}

QList<ApplyMessage> IMDataBaseOperaInfo::DBGetApplyMessageList()
{
	QList<ApplyMessage> applyMessageList;
	QString strSql = QString("select * from APPLYMESSAGE");
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
		while (resultQuery.next())
		{
			ApplyMessage applyMessage;
			applyMessage.applyId = resultQuery.value("ApplyId").toInt();
			applyMessage.applyName = resultQuery.value("ApplyName").toString();
			applyMessage.userId = resultQuery.value("UserId").toInt();
			applyMessage.groupId = resultQuery.value("GroupId").toInt();
			applyMessage.groupName = resultQuery.value("GroupName").toString();
			applyMessage.applyType = resultQuery.value("ApplyType").toInt();
			applyMessage.agree = resultQuery.value("Agree").toInt();
			applyMessageList.append(applyMessage);
		}
	}
	return applyMessageList;
}

void IMDataBaseOperaInfo::DBInsertApplyMessage(ApplyMessage apply)
{
	QString strSql = QString("insert into APPLYMESSAGE(ApplyId,ApplyName,UserId,GroupId,GroupName,ApplyType,Agree) "
		"values(%1,'%2',%3,%4,'%5',%6,%7)")
		.arg(apply.applyId).arg(apply.applyName).arg(apply.userId).arg(apply.groupId).arg(apply.groupName)
		.arg(apply.applyType).arg(apply.agree);
	mDateBase->ExecuSql(strSql);
}

void IMDataBaseOperaInfo::DBApplyMessageRead(QString applyId, QString groupId)
{
	QString strSql;
	if (groupId.isEmpty())   //不含部落id，这是申请加好友的。
		strSql = QString("update APPLYMESSAGE set Agree=1 where ApplyId=%1 and ApplyType=0").arg(applyId.toInt());
	else                     //申请加群的。
		strSql = QString("update APPLYMESSAGE set Agree=1 where ApplyId=%1 and GroupId=%2 and ApplyType=1").arg(applyId.toInt()).arg(groupId.toInt());

	mDateBase->ExecuSql(strSql);
}

void IMDataBaseOperaInfo::DBInsertOrgTree(OrgStruct orgStruct)
{
	QString strSql = QString("insert into ORGTREE(MC,BH,JS,MX,TYPE,GROUP_ID,USER_ID,PARENT_BH)"
		"values(%1,'%2','%3',%4,'%5','%6',%7,%8,'%9')")
		.arg(orgStruct.mc).arg(orgStruct.bh).arg(orgStruct.js).arg(orgStruct.mx).arg(orgStruct.type)
		.arg(orgStruct.groupID).arg(orgStruct.userID).arg(orgStruct.parent_bh);
	mDateBase->ExecuSql(strSql);
}

QList<OrgStruct> IMDataBaseOperaInfo::DBGetOrgTree(QString bh)
{
	QList<OrgStruct> orgList;
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		if (bh.isEmpty())
		{
			QString strSql = QString("select * from ORGTREE where JS=1 and TYPE='ZZJG'");
			QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
			while (resultQuery.next())
			{
				OrgStruct orgStruct;
				orgStruct.bh = resultQuery.value("BH").toString();
				orgStruct.js = resultQuery.value("JS").toInt();
				orgStruct.mc = resultQuery.value("MC").toString();
				orgStruct.mx = resultQuery.value("MX").toString();
				orgStruct.type = resultQuery.value("TYPE").toString();

				orgList.append(orgStruct);
			}
		}
		else
		{
			QString strSql = QString("select * from ORGTREE where PARENT_BH='%1'").arg(bh);
			QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
			while (resultQuery.next())
			{
				OrgStruct orgStruct;
				orgStruct.bh = resultQuery.value("BH").toString();
				orgStruct.js = resultQuery.value("JS").toInt();
				orgStruct.mc = resultQuery.value("MC").toString();
				orgStruct.mx = resultQuery.value("MX").toString();
				orgStruct.parent_bh = resultQuery.value("PARENT_BH").toString();
				orgStruct.type = resultQuery.value("TYPE").toString();
				orgStruct.userID = resultQuery.value("USER_ID").toInt();
				orgStruct.groupID = resultQuery.value("GROUP_ID").toInt();
				orgList.append(orgStruct);
			}
		}
	}
	return orgList;
}

void IMDataBaseOperaInfo::DisConnectDB()
{
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		//	mDateBase->CloseDataBase();
		delete mDateBase;
		mDateBase = NULL;
	}
}

void IMDataBaseOperaInfo::DBOnInsertFileInfo(QString strMsgID, QString strFileLocalPath, QString strFileHttpPath)
{
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QString strSql = QString("insert into LOCALFILE(MSGID,FileHttpPath,FileLocalPath) values('%1','%2','%3')").arg(strMsgID).arg(strFileHttpPath).arg(strFileLocalPath.replace("\'", "\'\'"));
		mDateBase->ExecuSql(strSql);
	}
}

QString IMDataBaseOperaInfo::DBGetFileInfoLocalPath(QString strMsgID)
{
	QString strFileLocalPath = "";
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QString strSql = QString("select FileLocalPath from LOCALFILE where MSGID = '%1'").arg(strMsgID);
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
		while (resultQuery.next())
		{
			strFileLocalPath = resultQuery.value("FileLocalPath").toString();
		}
	}
	return strFileLocalPath;
}

QString IMDataBaseOperaInfo::DBGetFileInfoHttpPathByLocalPath(QString strLocalPath)
{
	QString strFileHttpPath = "";
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QString strSql = QString("select FileHttpPath from LOCALFILE where FileLocalPath = '%1'").arg(strLocalPath);
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
		while (resultQuery.next())
		{
			strFileHttpPath = resultQuery.value("FileHttpPath").toString();
		}
	}
	return strFileHttpPath;
}

QString IMDataBaseOperaInfo::DBGetFileLocalPathByHttpPath(QString strHttpPath)
{
	QString strFileLocalPath = "";
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QString strSql = QString("select FileLocalPath from LOCALFILE where FileHttpPath = '%1'").arg(strHttpPath);
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
		while (resultQuery.next())
		{
			strFileLocalPath = resultQuery.value("FileLocalPath").toString();
		}
	}
	return strFileLocalPath;
}


//插入单个好友信息
void IMDataBaseOperaInfo::DBInsertBuddyInfo(BuddyInfo buddyInfo)
{
	QString strSql = QString("delete from BUDDYINFO where BuddyID = %1").arg(buddyInfo.nUserId);
	mDateBase->ExecuSql(strSql);
	buddyInfo.strLocalAvatar = buddyInfo.strLocalAvatar.replace(gSettingsManager->getUserPath(), "");
	QDateTime time = QDateTime::currentDateTime();//获取系统现在的时间
	QString str = time.toString("yyyy-MM-dd hh:mm:ss"); //设置显示格式
	strSql = QString("insert into BUDDYINFO(BuddyID,UserName,NickName,Note,Sign,Sex"
		",Email,MobilePhone,Phone,AvatarHttp,AvatarLocal,AvatarDefault,UserType,PingYin,updateTime,BuddyType,disableStrangers,msgPrompt) "
		"values(%1,'%2','%3','%4','%5','%6','%7','%8','%9','%10','%11','%12',%13,'%14','%15',%16,%17,%18)")
		.arg(buddyInfo.nUserId).arg(buddyInfo.strUserName).arg(buddyInfo.strNickName.replace("\'", "\'\'")).arg(buddyInfo.strNote.replace("\'", "\'\'"))
		.arg(buddyInfo.strSign.replace("\'", "\'\'")).arg(buddyInfo.strSex).arg(buddyInfo.strEmail).arg(buddyInfo.strMobilePhone)
		.arg(buddyInfo.strPhone).arg(buddyInfo.strHttpAvatar).arg(buddyInfo.strLocalAvatar).arg(buddyInfo.strDefaultAvatar).arg(buddyInfo.nUserType)
		.arg(buddyInfo.strPingYin).arg(str).arg(buddyInfo.BuddyType).arg(buddyInfo.disableStrangers).arg(buddyInfo.msgPrompt);
	mDateBase->ExecuSql(strSql);
}

void IMDataBaseOperaInfo::DBInsertBuddyInfo(QList<BuddyInfo> listBuddyInfo)
{
	for (int i = 0; i < listBuddyInfo.size(); i++)
	{
		DBInsertBuddyInfo(listBuddyInfo[i]);
	}
}

//个人消息列表中数据
void IMDataBaseOperaInfo::DBInsertPerMessageListInfo(MessageListInfo messagelist)
{
	//检查下，如果strLastMessageJson为空，则用strLastMessage来构造出strLastMessageJson
	if (messagelist.strLastMessageJson.isEmpty())
	{

		QJsonArray jsonArr;

		QJsonObject object;
		object.insert("type", 0);
		object.insert("msg", messagelist.strLastMessage);

		jsonArr.append(object);

		QJsonDocument document;
		document.setArray(jsonArr);
		QString strJson = (QString)document.toJson();

		messagelist.strLastMessageJson = strJson;
	}

	//检查下，如果数据库MESSAGELIST字段LastMsgJson当前存储的记录中有不为0的条目，则保留这些条目(这些条目可能为[有人@我]，[新公告]等等)
	//,这些条目要与当前的strLastMessageJson中的条目去重并合并起来
	//目前简化处理，只处理@成员

	bool isPrevExistAt = false;


	QString strSql = QString("select * from MESSAGELIST where BUDDYID = %1").arg(messagelist.nBudyyID);
	QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
	while (resultQuery.next())
	{
		QString strLastMsgJson = resultQuery.value("LastMsgJson").toString();

		QJsonParseError jsonError;
		QJsonDocument doucment = QJsonDocument::fromJson(strLastMsgJson.toUtf8(), &jsonError);  // 转化为 JSON 文档
		if (!doucment.isNull() && (jsonError.error == QJsonParseError::NoError))
		{
			QJsonArray jsonArr = doucment.array();

			for (int i = 0; i < jsonArr.count(); i++)
			{
				QJsonObject object = jsonArr.at(i).toObject();
				if (object.value("type").toInt() == 1)//@成员
				{
					isPrevExistAt = true;
				}
			}
		}
	}


	//判断即将要存数据表字段中的messagelist.strLastMessageJson是否有@，没有前面追加上
	bool isCurrentExistAt = false;

	QJsonParseError jsonError;
	QJsonDocument doucment = QJsonDocument::fromJson(messagelist.strLastMessageJson.toUtf8(), &jsonError);  // 转化为 JSON 文档
	if (!doucment.isNull() && (jsonError.error == QJsonParseError::NoError))
	{
		QJsonArray jsonArr = doucment.array();

		for (int i = 0; i < jsonArr.count(); i++)
		{
			QJsonObject object = jsonArr.at(i).toObject();
			if (object.value("type").toInt() == 1)//@成员
			{
				isCurrentExistAt = true;
			}

		}


		//以前的表记录有@，现在的没@，则现在的要前面追加上@
		if (isPrevExistAt)
		{
			if (!isCurrentExistAt)
			{
				QJsonObject object;
				object.insert("type", 1);
				jsonArr.push_front(object);

				QJsonDocument document;
				document.setArray(jsonArr);
				QString strJson = (QString)document.toJson();

				messagelist.strLastMessageJson = strJson;
			}
		}


	}



	if (mDateBase)
	{
		QString strSql = QString("select MsgTopOrder from MESSAGELIST where BUDDYID = %1").arg(messagelist.nBudyyID);
		resultQuery = mDateBase->ExecuQuery(strSql);
		int msgTopOrder = 0;
		while (resultQuery.next())
		{
			msgTopOrder = resultQuery.value("MsgTopOrder").toInt();
		}
		strSql = QString("delete from MESSAGELIST where BUDDYID = %1").arg(messagelist.nBudyyID);
		mDateBase->ExecuSql(strSql);
		messagelist.strBuddyHeaderImage = messagelist.strBuddyHeaderImage.replace(gSettingsManager->getUserPath(), "");

		QDateTime time = QDateTime::currentDateTime();//获取系统现在的时间
		QString str = time.toString("yyyy-MM-dd hh:mm:ss"); //设置显示格式
		strSql = QString("insert into MESSAGELIST(BUDDYID,BUDDYNAME,BUDDYHEADERIMAGE,LastMsgJson,LASTMESSAGETIME,UNREADMESSGAENUM,IsGroup,messageType,updateTime,MsgTopOrder) values(%1,'%2','%3','%4',%5,%6,0,%7,'%8',%9)")
			.arg(messagelist.nBudyyID)
			.arg(messagelist.strBuddyName.replace("\'", "\'\'"))
			.arg(messagelist.strBuddyHeaderImage)
			.arg(messagelist.strLastMessageJson.replace("\'", "\'\'"))
			.arg(messagelist.nLastTime)
			.arg(messagelist.nUnReadNum)
			.arg(messagelist.messageType)
			.arg(str)
			.arg(msgTopOrder);
		mDateBase->ExecuSql(strSql);
	}
}

//部落消息列表中数据
void IMDataBaseOperaInfo::DBInsertGroupMessageListInfo(MessageListInfo messagelist)
{
	//检查下，如果strLastMessageJson为空，则用strLastMessage来构造出strLastMessageJson
	if (messagelist.strLastMessageJson.isEmpty())
	{
		QJsonArray jsonArr;
		QJsonObject object;
		object.insert("type", 0);
		object.insert("msg", messagelist.strLastMessage);
		jsonArr.append(object);

		QJsonDocument document;
		document.setArray(jsonArr);
		QString strJson = (QString)document.toJson();

		messagelist.strLastMessageJson = strJson;
	}

	//检查下，如果数据库MESSAGELIST字段LastMsgJson当前存储的记录中有不为0的条目，则保留这些条目(这些条目可能为[有人@我]，[新公告]等等)
	//,这些条目要与当前的strLastMessageJson中的条目去重并合并起来
	//目前简化处理，只处理@成员

	bool isPrevExistAt = false;


	QString strSql = QString("select * from MESSAGELIST where BUDDYID = %1").arg(messagelist.nBudyyID);
	QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
	while (resultQuery.next())
	{
		QString strLastMsgJson = resultQuery.value("LastMsgJson").toString();

		QJsonParseError jsonError;
		QJsonDocument doucment = QJsonDocument::fromJson(strLastMsgJson.toUtf8(), &jsonError);  // 转化为 JSON 文档
		if (!doucment.isNull() && (jsonError.error == QJsonParseError::NoError))
		{
			QJsonArray jsonArr = doucment.array();

			for (int i = 0; i < jsonArr.count(); i++)
			{
				QJsonObject object = jsonArr.at(i).toObject();
				if (object.value("type").toInt() == 1)//@成员
				{
					isPrevExistAt = true;
				}
			}
		}
	}


	//判断即将要存数据表字段中的messagelist.strLastMessageJson是否有@，没有前面追加上
	bool isCurrentExistAt = false;

	QJsonParseError jsonError;
	QJsonDocument doucment = QJsonDocument::fromJson(messagelist.strLastMessageJson.toUtf8(), &jsonError);  // 转化为 JSON 文档
	if (!doucment.isNull() && (jsonError.error == QJsonParseError::NoError))
	{
		QJsonArray jsonArr = doucment.array();

		for (int i = 0; i < jsonArr.count(); i++)
		{
			QJsonObject object = jsonArr.at(i).toObject();
			if (object.value("type").toInt() == 1)//@成员
			{
				isCurrentExistAt = true;
			}

		}


		//以前的表记录有@，现在的没@，则现在的要前面追加上@
		if (isPrevExistAt)
		{
			if (!isCurrentExistAt)
			{
				QJsonObject object;
				object.insert("type", 1);
				jsonArr.push_front(object);

				QJsonDocument document;
				document.setArray(jsonArr);
				QString strJson = (QString)document.toJson();

				messagelist.strLastMessageJson = strJson;
			}
		}


	}


	if (mDateBase)
	{
		QString strSql = QString("delete from MESSAGELIST where BUDDYID = %1").arg(messagelist.nBudyyID);
		mDateBase->ExecuSql(strSql);
		messagelist.strBuddyHeaderImage = messagelist.strBuddyHeaderImage.replace(gSettingsManager->getUserPath(), "");
		QDateTime time = QDateTime::currentDateTime();//获取系统现在的时间
		QString str = time.toString("yyyy-MM-dd hh:mm:ss"); //设置显示格式
		strSql = QString("insert into MESSAGELIST(BUDDYID,BUDDYNAME,BUDDYHEADERIMAGE,LastMsgJson,LASTMESSAGETIME,UNREADMESSGAENUM,IsGroup,messageType,updateTime, MsgTopOrder) values(%1,'%2','%3','%4',%5,%6,1,%7,'%8',%9)")
			.arg(messagelist.nBudyyID)
			.arg(messagelist.strBuddyName.replace("\'", "\'\'"))
			.arg(messagelist.strBuddyHeaderImage)
			.arg(messagelist.strLastMessageJson.replace("\'", "\'\'"))
			.arg(messagelist.nLastTime)
			.arg(messagelist.nUnReadNum)
			.arg(messagelist.messageType)
			.arg(str)
			.arg(messagelist.msgTopOrder);
		bool flag = mDateBase->ExecuSql(strSql);
	}
}

//设置消息已读
void IMDataBaseOperaInfo::DBUpdateMessageListInfo(int nBuddyID)
{
	if (mDateBase)
	{
		QString strSql = QString("update MESSAGELIST set UNREADMESSGAENUM=0 where BUDDYID=%1").arg(nBuddyID);
		mDateBase->ExecuSql(strSql);
	}
}

void IMDataBaseOperaInfo::DBDeleteMessageByID(QString userID)
{
	if (mDateBase)
	{
		QString strSql = QString("delete from messagelist where BUDDYID=%1").arg(userID);
		mDateBase->ExecuSql(strSql);
	}
}

void IMDataBaseOperaInfo::DBClearAllMessageList()
{
	if (mDateBase)
	{
		QString strSql = QString("delete from messagelist");
		mDateBase->ExecuSql(strSql);
	}
}

bool IMDataBaseOperaInfo::DBJudgeBuddyIsHaveByID(QString strUserID)
{
	if (mDateBase)
	{
		QString strSql = QString("select count(*) from BUDDYINFO where BuddyID = %1").arg(strUserID.toInt());
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
		if (resultQuery.first())
		{
			int nSize = resultQuery.value("count(*)").toInt();
			if (nSize == 0)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}
	return false;
}

bool IMDataBaseOperaInfo::DBJudgeFriendIsHaveByID(QString strUserID)
{
	if (mDateBase)
	{
		QString strSql = QString("select count(*) from BUDDYINFO where BuddyID = %1 and BuddyType=1").arg(strUserID.toInt());
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
		if (resultQuery.first())
		{
			int nSize = resultQuery.value("count(*)").toInt();
			if (nSize == 0)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}
	return false;
}




QList<MessageListInfo> IMDataBaseOperaInfo::DBGetALLMessageListInfo()
{
	QList<MessageListInfo> listMessageListInfo;
	if (mDateBase)
	{
		QString strSql = QString("select * from MESSAGELIST");
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
		while (resultQuery.next())
		{
			MessageListInfo messagelistinfo;
			messagelistinfo.nBudyyID = resultQuery.value("BUDDYID").toInt();
			messagelistinfo.strBuddyName = resultQuery.value("BUDDYNAME").toString();
			messagelistinfo.strBuddyHeaderImage = resultQuery.value("BUDDYHEADERIMAGE").toString();
			messagelistinfo.nBudyyID = resultQuery.value("BUDDYID").toInt();
			messagelistinfo.nLastTime = resultQuery.value("LASTMESSAGETIME").toLongLong();
			messagelistinfo.nUnReadNum = resultQuery.value("UNREADMESSGAENUM").toInt();

			messagelistinfo.strLastMessageJson = resultQuery.value("LastMsgJson").toString();

			messagelistinfo.strLastMessage = MessageListStore::instance()->msgJsonToDesc(messagelistinfo.strLastMessageJson);

			messagelistinfo.isGroup = resultQuery.value("IsGroup").toInt();
			messagelistinfo.messageType = resultQuery.value("messageType").toInt();
			messagelistinfo.strBuddyHeaderImage = gSettingsManager->getUserPath() + messagelistinfo.strBuddyHeaderImage;
			messagelistinfo.msgTopOrder = resultQuery.value("msgTopOrder").toInt();
			listMessageListInfo.append(messagelistinfo);
		}
	}
	return listMessageListInfo;
}

bool IMDataBaseOperaInfo::DBJudgeBuddyIsEmpty()
{
	if (mDateBase)
	{
		QString strSql = QString("select count(*) from BUDDYINFO");
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
		if (resultQuery.first())
		{
			int nSize = resultQuery.value("count(*)").toInt();
			if (nSize == 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	return false;
}

bool IMDataBaseOperaInfo::DBJudgeGroupIsHaveByID(QString strGroupID)
{
	if (mDateBase)
	{
		QString strSql = QString("select count(*) from  GROUPINFO where GroupID = %1").arg(strGroupID.toInt());
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
		if (resultQuery.first())
		{
			int nSize = resultQuery.value("count(*)").toInt();
			if (nSize == 0)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}
	return false;
}

bool IMDataBaseOperaInfo::DBJudgetGroupBuddyIsEmpty(QString strGroupID)
{
	if (mDateBase)
	{
		QString strSql = QString("select count(*) from  GROUPUSERINFO where GroupID = %1").arg(strGroupID.toInt());
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
		if (resultQuery.first())
		{
			int nSize = resultQuery.value("count(*)").toInt();
			if (nSize == 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	return false;
}

bool IMDataBaseOperaInfo::DBJudgeGroupIsHaveBuddy(QString strGroupID, QString strBuddyID)
{
	if (mDateBase)
	{
		QString strSql = QString("select count(*) from  GROUPUSERINFO where GroupID = %1 and GroupUserID = %2").arg(strGroupID.toInt()).arg(strBuddyID.toInt());
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
		if (resultQuery.first())
		{
			int nSize = resultQuery.value("count(*)").toInt();
			if (nSize == 0)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}
	return false;
}

QString IMDataBaseOperaInfo::DBGetLastMsgJsonInMessageList(QString strBuddyID)
{
	QString strSql = QString("select * from MESSAGELIST where BUDDYID = %1").arg(strBuddyID);
	QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
	if (resultQuery.next())
	{
		QString strLastMsgJson = resultQuery.value("LastMsgJson").toString();

		return strLastMsgJson;
	}

	return "";
}

void IMDataBaseOperaInfo::DBUpdateLastMsgJsonInMessageList(QString strBuddyOrGroupID, QString lastMsgJson, int msgType)
{
	QString strSql = QString("update MESSAGELIST set LastMsgJson='%1', messageType = %2  where BUDDYID=%3").arg(lastMsgJson.replace("\'", "\'\'")).arg( msgType ).arg(strBuddyOrGroupID);
	mDateBase->ExecuQuery(strSql);
}
void IMDataBaseOperaInfo::DBUpdateMsgTopOrderInMessageList(int strID, int topOrder)
{
	QString strSql = QString("update MESSAGELIST set MsgTopOrder = %1 where BUDDYID = %2").arg(topOrder).arg(strID);
	if (!mDateBase->ExecuSql(strSql))
	{
		qDebug() << "SQL DBUpdateMsgTopOrderInMessageList ERROR...";
	}
}
void IMDataBaseOperaInfo::DBGetMaximumTopOrderInMessageList(int *maxValue, int *minValue)
{
	int maxOrder = 0;
	int minOrder = 0;
	QSqlQuery resultQuery;
	QString strSql1 = QString("select max(MsgTopOrder) as MaxOrder, min(MsgTopOrder) as MinOrder from MESSAGELIST");
	resultQuery = mDateBase->ExecuQuery(strSql1);
	while (resultQuery.next())
	{
		maxOrder = resultQuery.value("MaxOrder").toInt();
		minOrder = resultQuery.value("MinOrder").toInt();
	}
	*maxValue = maxOrder;
	*minValue = minOrder;
}

int IMDataBaseOperaInfo::DBGetMsgTopOrderInMessageList(int id)
{
	int msgTopOrder = 0;
	QSqlQuery resultQuery;
	QString strSql1 = QString("select MsgTopOrder from MESSAGELIST where BUDDYID = %1").arg(id);

	resultQuery = mDateBase->ExecuQuery(strSql1);
	while (resultQuery.next())
	{
		msgTopOrder = resultQuery.value("MsgTopOrder").toInt();
	}
	return msgTopOrder;
}

void IMDataBaseOperaInfo::DBDeleteOneRecordInMessageList(int id)
{
	QString strSql = QString("delete from MESSAGELIST where BUDDYID = %1").arg(id);
	bool returnFlag = mDateBase->ExecuSql(strSql);
	if (!returnFlag)
		qDebug() << "SQL RUNNING ERROR on DBDeleteOneRecordInMessageList()";
}


void IMDataBaseOperaInfo::DBSetMsgPromptInInfoTables(int id, int tableType)
{
	QString strSql;
	if (tableType == 0)
	{
		strSql = QString("update BUDDYINFO set msgPrompt = 0 where BuddyID = %1").arg(id);
	}
	else if (tableType == 1)
	{
		strSql = QString("update GROUPINFO set msgPrompt = 0 where GroupID = %1").arg(id);
	}
	bool flag = mDateBase->ExecuSql(strSql);
	if (!flag)
		qDebug() << "SQL RUNNING ERROR on DBSetMsgNoPromptInInfoTables() 1";


	strSql = QString("update MESSAGELIST set msgPrompt = 0 where BUDDYID = %1").arg(id);
	flag = mDateBase->ExecuSql(strSql);

	if (!flag)
		qDebug() << "SQL RUNNING ERROR on DBSetMsgPromptInInfoTables() 2";
}
void IMDataBaseOperaInfo::DBCancleMsgPromptInInfoTables(int id, int tableType)
{
	QString strSql;
	if (tableType == 0)
	{
		strSql = QString("update BUDDYINFO set msgPrompt = 1 where BuddyID = %1").arg(id);
	}
	else if (tableType == 1)
	{
		strSql = QString("update GROUPINFO set msgPrompt = 1 where GroupID = %1").arg(id);
	}
	bool flag = mDateBase->ExecuSql(strSql);
	if (!flag)
		qDebug() << "SQL RUNNING ERROR on DBCancleMsgNoPromptInInfoTables() step1";

	strSql = QString("update MESSAGELIST set msgPrompt = 1 where BUDDYID = %1").arg(id);
	flag = mDateBase->ExecuSql(strSql);

	if (!flag)
		qDebug() << "SQL RUNNING ERROR on DBCancleMsgNoPromptInInfoTables() step2";
}

int IMDataBaseOperaInfo::DBGetMsgPromptFlagInInfoTables(int id, int tableType)
{
	QString strSql;
	int promptFlag = 0;

	strSql = QString("select msgPrompt from MESSAGELIST where BuddyID = %1").arg(id);
	QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
	while (resultQuery.next())
	{
		promptFlag = resultQuery.value("MsgPrompt").toInt();
	}

	if (promptFlag == 0)
	{
		if (tableType == 0)
		{
			strSql = QString("select msgPrompt from BUDDYINFO where BuddyID = %1").arg(id);
		}
		else if (tableType == 1)
		{
			strSql = QString("select msgPrompt from GROUPINFO where GroupID = %1").arg(id);
		}
		resultQuery = mDateBase->ExecuQuery(strSql);
		while (resultQuery.next())
		{
			promptFlag = resultQuery.value("msgPrompt").toInt();
		}
	}
	return promptFlag;
}


//void IMDataBaseOperaInfo::DBSetMsgPromptInMessageList(int id, int tableType)
//{
//	QString strSql = QString("update MESSAGELIST set msgPrompt = 0 where BUDDYID = %1").arg(id);
//	bool flag = mDateBase->ExecuSql(strSql);
//	if (!flag)
//		qDebug() << "SQL RUNNING ERROR on DBSetMsgPromptInMessageList()";
//}
//void IMDataBaseOperaInfo::DBCancleMsgPromptInMessageList(int id, int tableType)
//{
//	QString strSql = QString("update MESSAGELIST set msgPrompt = 1 where BUDDYID = %1").arg(id);
//	bool flag = mDateBase->ExecuSql(strSql);
//	if (!flag)
//		qDebug() << "SQL RUNNING ERROR on DBCancleMsgPromptInMessageList()";
//}
//



BuddyInfo IMDataBaseOperaInfo::DBGetGroupBuddyInfoWithGroupBuddyID(QString strGroupID, QString strBuddyID)
{
	BuddyInfo buddy;
	buddy.nUserType = 0;
	QString strSql = QString("select * from GROUPUSERINFO where GroupUserID = %1 and GroupID = %2").arg(strBuddyID.toInt()).arg(strGroupID.toInt());
	QSqlQuery result = mDateBase->ExecuQuery(strSql);
	while (result.next())
	{
		buddy.nUserId = result.value("GroupUserID").toInt();
		buddy.nUserType = result.value("UserType").toInt();
		buddy.strEmail = result.value("Email").toString();
		buddy.strHttpAvatar = result.value("AvatarHttp").toString();
		buddy.strLocalAvatar = result.value("AvatarLocal").toString();
		buddy.strMobilePhone = result.value("MobilePhone").toString();
		buddy.strNickName = result.value("NickName").toString();
		buddy.strNote = result.value("Note").toString();
		buddy.strPhone = result.value("Phone").toString();
		buddy.strSex = result.value("Sex").toString();
		buddy.strSign = result.value("Sign").toString();
		buddy.strUserName = result.value("UserName").toString();
		buddy.strDefaultAvatar = result.value("AvatarDefault").toString();
		buddy.disableStrangers = result.value("disableStrangers").toInt();
		buddy.strLocalAvatar = gSettingsManager->getUserPath() + buddy.strLocalAvatar;
	}
	return buddy;
}

QList<GroupInfo> IMDataBaseOperaInfo::DBGetAllGroupInfo()
{
	QList<GroupInfo > listGroupInfo;
	QString strSql = QString("select * from GROUPINFO ");
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
		while (resultQuery.next())
		{
			GroupInfo groupInfo;
			groupInfo.groupId = resultQuery.value("GroupID").toString();
			groupInfo.groupName = resultQuery.value("GroupName").toString();
			groupInfo.createTime = resultQuery.value("CreateTime").toString();
			groupInfo.createUserId = resultQuery.value("CreateUserID").toString();
			groupInfo.groupLoacalHeadImage = resultQuery.value("GroupAvatar").toString();
			groupInfo.groupDefaultAvatar = resultQuery.value("GroupDefaultAvatar").toString();	
			groupInfo.groupHttpHeadImage = resultQuery.value("GroupHttpAvatar").toString();
			groupInfo.noSpeak = resultQuery.value("noSpeak").toInt();
			groupInfo.groupType = resultQuery.value("groupType").toInt();
			groupInfo.groupDesc = resultQuery.value("GroupDesc").toString();
			groupInfo.groupKey = resultQuery.value("GroupKey").toString();
			groupInfo.msgPrompt = resultQuery.value("msgPrompt").toInt();
			groupInfo.groupLoacalHeadImage = gSettingsManager->getUserPath() + groupInfo.groupLoacalHeadImage;
			listGroupInfo.append(groupInfo);
		}
	}
	return listGroupInfo;
}

bool IMDataBaseOperaInfo::DBJudgetGroupIsEmpty()
{
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QString strSql = QString("select count(*) from GROUPINFO");
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
		if (resultQuery.first())
		{
			int nSize = resultQuery.value("count(*)").toInt();
			if (nSize == 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	return false;
}

QMap<QString, QList<BuddyInfo>> IMDataBaseOperaInfo::DB_GetBuddyInfo()
{
	{
		QMap<QString, QList<BuddyInfo > > mBuddyInfo;
		if (mDateBase != NULL && mDateBase->IsOpen())
		{
			QString strSql = QString("select * from BUDDYINFO");
			QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
			while (resultQuery.next())
			{
				BuddyInfo buddyInfo;
				buddyInfo.strHttpAvatar = resultQuery.value("AvatarHttp").toString();
				buddyInfo.strLocalAvatar = resultQuery.value("AvatarLocal").toString();
				buddyInfo.strEmail = resultQuery.value("Email").toString();
				buddyInfo.strMobilePhone = resultQuery.value("MobilePhone").toString();
				buddyInfo.strNickName = resultQuery.value("NickName").toString();
				buddyInfo.strNote = resultQuery.value("Note").toString();
				buddyInfo.strPhone = resultQuery.value("Phone").toString();
				buddyInfo.strSex = resultQuery.value("Sex").toString();
				buddyInfo.strSign = resultQuery.value("Sign").toString();
				buddyInfo.nUserId = resultQuery.value("BuddyID").toInt();
				buddyInfo.strUserName = resultQuery.value("UserName").toString();
				buddyInfo.nUserType = resultQuery.value("UserType").toInt();
				buddyInfo.strPingYin = resultQuery.value("PingYin").toString();
				buddyInfo.BuddyType = resultQuery.value("BuddyType").toInt();
				buddyInfo.disableStrangers = resultQuery.value("disableStrangers").toInt();
				buddyInfo.msgPrompt = resultQuery.value("MsgPrompt").toInt();
				buddyInfo.strLocalAvatar = gSettingsManager->getUserPath() + buddyInfo.strLocalAvatar;
				if (buddyInfo.BuddyType == 0 || buddyInfo.nUserId <= 0)
					continue;

				int nFlags = 0;
				QMap<QString, QList<BuddyInfo> >::iterator itor = mBuddyInfo.begin();
				for (; itor != mBuddyInfo.end(); ++itor)
				{
					QChar letter;
					if (buddyInfo.strPingYin.isEmpty())
					{
						letter = '~';
					}
					else
					{
						int c = (int)buddyInfo.strPingYin.at(0).toLatin1();
						if (!(65 <= c&&c <= 90))
							letter = '~';	//使用特殊字符ASCII码127使#项在MAP中保持末尾存放
						else
							letter = c;
					}
					QString strTemp = letter;
					if (strTemp == itor.key())
					{
						nFlags = 1;
						itor.value().append(buddyInfo);
						break;
					}
				}
				if (nFlags == 0)
				{
					QChar letter;
					if (buddyInfo.strPingYin.isEmpty())
					{
						letter = '~';
					}
					else
					{
						int c = (int)buddyInfo.strPingYin.at(0).toLatin1();
						if (!(65 <= c&&c <= 90))
						{
							//使用特殊字符ASCII码126使#项在MAP中保持末尾存放
							letter = '~';
						}
						else
							letter = c;
					}
					buddyInfo.strPingYin = (QString)letter;

					QList<BuddyInfo> vBuddyInfo;
					vBuddyInfo.append(buddyInfo);
					mBuddyInfo.insert(buddyInfo.strPingYin, vBuddyInfo);
				}
			}
		}
		return mBuddyInfo;
	}
}

void IMDataBaseOperaInfo::DBDeleteGroupBuddyByID(QString strGroupID)
{
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		//删除部落成员表
		QString strSql = QString("delete from GROUPUSERINFO where GroupID = %1").arg(strGroupID);
		mDateBase->ExecuSql(strSql);
	}
}

void IMDataBaseOperaInfo::DBInsertAddApplyMessage(AddApplyMessage Info)
{
	QString strTmp = Info.strMessage.replace("\'", "\'\'");
	QString strSql = QString("insert into ADDAPPLYMESSAGE(MessageId,Type,Id,Message,State,Date,GroupId,Read) "
		"values('%1',%2,%3,'%4',%5,%6,'%7',%8)")
		.arg(Info.strMegId).arg(Info.iType).arg(Info.iId).arg(strTmp).arg(Info.iState).arg(Info.strDate).arg(Info.strGroupId).arg(Info.iRead);
	mDateBase->ExecuSql(strSql);
}

void IMDataBaseOperaInfo::DBUpdateAddApplyMessage(AddApplyMessage Info)
{
	QString strTmp = Info.strMessage.replace("\'", "\'\'");
	QString strSql = QString("update ADDAPPLYMESSAGE set MessageId = '%1' , State = %2 , Date = '%3',Message = '%4',Read = %5 where Type = %6 and Id = %7 and GroupId = '%8'")
		.arg(Info.strMegId).arg(Info.iState).arg(Info.strDate).arg(Info.strMessage).arg(Info.iRead).arg(Info.iType).arg(Info.iId).arg(Info.strGroupId);
	mDateBase->ExecuSql(strSql);
}

bool IMDataBaseOperaInfo::DBFindAddApplyMessage(int iType, int iId, QString strGroupId, AddApplyMessage& info)
{
	QString strSql = QString("select * from ADDAPPLYMESSAGE where Type = %1 and Id = %2 and GroupId = '%3'").arg(iType).arg(iId).arg(strGroupId);
	AddApplyMessage applyMessage;
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
		if (resultQuery.next())
		{
			info.strMegId = resultQuery.value("MessageId").toString();
			info.iType = resultQuery.value("Type").toInt();
			info.iId = resultQuery.value("Id").toInt();
			info.strMessage = resultQuery.value("Message").toString();
			info.iState = resultQuery.value("State").toInt();
			info.strDate = resultQuery.value("Date").toString();
			info.strGroupId = resultQuery.value("GroupId").toString();
			info.iRead = resultQuery.value("Read").toInt();
			return true;
		}
	}
	return false;
}

bool IMDataBaseOperaInfo::DBFindByIdAddApplyMessage(int iId, AddApplyMessage& info)
{
	QString strSql = QString("select * from ADDAPPLYMESSAGE where Id = %1 order by Date desc limit 1").arg(iId);
	AddApplyMessage applyMessage;
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
		if (resultQuery.next())
		{
			info.strMegId = resultQuery.value("MessageId").toString();
			info.iType = resultQuery.value("Type").toInt();
			info.iId = resultQuery.value("Id").toInt();
			info.strMessage = resultQuery.value("Message").toString();
			info.iState = resultQuery.value("State").toInt();
			info.strDate = resultQuery.value("Date").toString();
			info.strGroupId = resultQuery.value("GroupId").toString();
			info.iRead = resultQuery.value("Read").toInt();
			return true;
		}
	}
	return false;
}


bool IMDataBaseOperaInfo::DBFindAddApplyMessageByMegId(QString strMegID, AddApplyMessage& info)
{
	QString strSql = QString("select * from ADDAPPLYMESSAGE where MessageId = '%1'").arg(strMegID);
	AddApplyMessage applyMessage;
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
		if (resultQuery.next())
		{
			info.strMegId = resultQuery.value("MessageId").toString();
			info.iType = resultQuery.value("Type").toInt();
			info.iId = resultQuery.value("Id").toInt();
			info.strMessage = resultQuery.value("Message").toString();
			info.iState = resultQuery.value("State").toInt();
			info.strDate = resultQuery.value("Date").toString();
			info.strGroupId = resultQuery.value("GroupId").toString();
			info.iRead = resultQuery.value("Read").toInt();
			return true;
		}
	}
	return false;
}
QList<AddApplyMessage> IMDataBaseOperaInfo::DBFindALLAddApplyMessage()
{
	QList<AddApplyMessage> applyMessageList;
	QString strSql = QString("select * from ADDAPPLYMESSAGE where State = 0");
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
		while (resultQuery.next())
		{
			AddApplyMessage applyMessage;
			applyMessage.strMegId = resultQuery.value("MessageId").toString();
			applyMessage.iType = resultQuery.value("Type").toInt();
			applyMessage.iId = resultQuery.value("Id").toInt();
			applyMessage.strMessage = resultQuery.value("Message").toString();
			applyMessage.iState = resultQuery.value("State").toInt();
			applyMessage.strDate = resultQuery.value("Date").toString();
			applyMessage.strGroupId = resultQuery.value("GroupId").toString();
			applyMessage.iRead = resultQuery.value("Read").toInt();
			applyMessageList.append(applyMessage);
		}
	}
	return applyMessageList;
}
QList<AddApplyMessage> IMDataBaseOperaInfo::DBFindALLAddApplyMessageDealt()
{
	QList<AddApplyMessage> applyMessageList;
	QDateTime local(QDateTime::currentDateTime());
	local = local.addDays(-7);//显示七天内
	QString localTime = local.toString("yyyyMMddhhmmss");
	QString strSql = QString("select * from ADDAPPLYMESSAGE where State > 0 and Date > %1 order by Date desc").arg(localTime);
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
		while (resultQuery.next())
		{
			AddApplyMessage applyMessage;
			applyMessage.strMegId = resultQuery.value("MessageId").toString();
			applyMessage.iType = resultQuery.value("Type").toInt();
			applyMessage.iId = resultQuery.value("Id").toInt();
			applyMessage.strMessage = resultQuery.value("Message").toString();
			applyMessage.iState = resultQuery.value("State").toInt();
			applyMessage.strDate = resultQuery.value("Date").toString();
			applyMessage.strGroupId = resultQuery.value("GroupId").toString();
			applyMessage.iRead = resultQuery.value("Read").toInt();
			applyMessageList.append(applyMessage);
		}
	}
	if (applyMessageList.size() == 0)//七天内没有 则显示最近三条
	{
		QString strSql = QString("select * from ADDAPPLYMESSAGE where State > 0 order by Dae desc LIMIT 0,3");
		if (mDateBase != NULL && mDateBase->IsOpen())
		{
			QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
			while (resultQuery.next())
			{
				AddApplyMessage applyMessage;
				applyMessage.strMegId = resultQuery.value("MessageId").toString();
				applyMessage.iType = resultQuery.value("Type").toInt();
				applyMessage.iId = resultQuery.value("Id").toInt();
				applyMessage.strMessage = resultQuery.value("Message").toString();
				applyMessage.iState = resultQuery.value("State").toInt();
				applyMessage.strDate = resultQuery.value("Date").toString();
				applyMessage.strGroupId = resultQuery.value("GroupId").toString();
				applyMessage.iRead = resultQuery.value("Read").toInt();
				applyMessageList.append(applyMessage);
			}
		}
	}
	return applyMessageList;
}

void IMDataBaseOperaInfo::DBUpdateAddApplyRead()
{
	QString strSql = QString("update ADDAPPLYMESSAGE set Read = 1;");
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QSqlQuery resultQuery = mDateBase->ExecuQuery(strSql);
	}
}

void IMDataBaseOperaInfo::ChangeOldHeaderPath()//替换固定路径
{
#ifdef Q_OS_WIN
	QString strExePath = QDir::currentPath();
#else
	QString strExePath = getResourcePath();
#endif

	QString strSql1 = QString(QString("update GROUPINFO set  GroupAvatar =replace(GroupAvatar,'%1','')").arg(strExePath));
	QString strSql2 = QString(QString("update BUDDYINFO set  AvatarLocal =replace(AvatarLocal,'%1','')").arg(strExePath));
	QString strSql3 = QString(QString("update GROUPUSERINFO set  AvatarLocal =replace(AvatarLocal,'%1','')").arg(strExePath));
	QString strSql4 = QString(QString("update MESSAGELIST set  BUDDYHEADERIMAGE =replace(BUDDYHEADERIMAGE,'%1','')").arg(strExePath));
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		mDateBase->ExecuQuery(strSql1);
		mDateBase->ExecuQuery(strSql2);
		mDateBase->ExecuQuery(strSql3);
		mDateBase->ExecuQuery(strSql4);
	}
}

void IMDataBaseOperaInfo::ClearUserData()
{
	QString strSql1 = QString(QString("delete from LOCALFILE"));
	QString strSql2 = QString(QString("delete from ADDAPPLYMESSAGE"));
	QString strSql3 = QString(QString("delete from BUDDYINFO where BuddyType=0"));
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		mDateBase->ExecuQuery(strSql1);
		mDateBase->ExecuQuery(strSql2);
		mDateBase->ExecuQuery(strSql3);
	}
}

QList<BuddyInfo> IMDataBaseOperaInfo::DBGetAllGroupUserInfo()
{
	QList<BuddyInfo> listBuddyInfo;
	QString strSql = QString("select * from GROUPUSERINFO group by GroupUserID");
	if (mDateBase != NULL && mDateBase->IsOpen())
	{
		QSqlQuery result = mDateBase->ExecuQuery(strSql);
		while (result.next())
		{
			BuddyInfo buddy;
			buddy.nUserId = result.value("GroupUserID").toInt();
			buddy.nUserType = result.value("UserType").toInt();
			buddy.strEmail = result.value("Email").toString();
			buddy.strHttpAvatar = result.value("AvatarHttp").toString();
			buddy.strLocalAvatar = result.value("AvatarLocal").toString();
			buddy.strMobilePhone = result.value("MobilePhone").toString();
			buddy.strNickName = result.value("NickName").toString();
			buddy.strNote = result.value("Note").toString();
			buddy.strPhone = result.value("Phone").toString();
			buddy.strSex = result.value("Sex").toString();
			buddy.strSign = result.value("Sign").toString();
			buddy.strUserName = result.value("UserName").toString();
			buddy.strDefaultAvatar = result.value("AvatarDefault").toString();
			buddy.disableStrangers = result.value("disableStrangers").toInt();
			buddy.strLocalAvatar = gSettingsManager->getUserPath() + buddy.strLocalAvatar;
			listBuddyInfo.append(buddy);
		}
	}
	return listBuddyInfo;
}