<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>OEScreen</name>
    <message>
        <location filename="oescreenshot.cpp" line="1584"/>
        <source>save Picture</source>
        <translation>Salvar foto</translation>
    </message>
</context>
<context>
    <name>ScreenControlWidget</name>
    <message>
        <location filename="screencontrolwidget.ui" line="35"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="399"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="80"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="401"/>
        <source>Rectangle</source>
        <translation>Retângulo</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="147"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="405"/>
        <source>Circle</source>
        <translation>Círculo</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="213"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="409"/>
        <source>Arrow</source>
        <translation>Seta</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="279"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="413"/>
        <source>Brush</source>
        <translation>Escova</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="336"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="417"/>
        <source>Mosaic</source>
        <translation>mosaico</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="391"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="421"/>
        <source>Text</source>
        <translation>Texto</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="496"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="425"/>
        <source>Revert</source>
        <translation>Reverter</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="546"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="429"/>
        <source>Save</source>
        <translation>salvar</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="636"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="433"/>
        <source>Exit</source>
        <translation>Saída</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="696"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="437"/>
        <source>Complete</source>
        <translation>Completo</translation>
    </message>
</context>
<context>
    <name>ScreenPenSettingWidget</name>
    <message>
        <location filename="screenpensettingwidget.ui" line="17"/>
        <source>ScreenPenSettingWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="screenpensettingwidget.ui" line="900"/>
        <source>Degree</source>
        <translation>Grau</translation>
    </message>
</context>
</TS>
