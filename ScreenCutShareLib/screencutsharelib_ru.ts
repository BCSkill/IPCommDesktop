<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>OEScreen</name>
    <message>
        <location filename="oescreenshot.cpp" line="1584"/>
        <source>save Picture</source>
        <translation>сохранить изображение</translation>
    </message>
</context>
<context>
    <name>ScreenControlWidget</name>
    <message>
        <location filename="screencontrolwidget.ui" line="35"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="399"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="80"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="401"/>
        <source>Rectangle</source>
        <translation>Прямоугольник</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="147"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="405"/>
        <source>Circle</source>
        <translation>Круг</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="213"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="409"/>
        <source>Arrow</source>
        <translation>Стрела</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="279"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="413"/>
        <source>Brush</source>
        <translation>Щетка</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="336"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="417"/>
        <source>Mosaic</source>
        <translation>мозаика</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="391"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="421"/>
        <source>Text</source>
        <translation>Текст</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="496"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="425"/>
        <source>Revert</source>
        <translation>возвращаться</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="546"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="429"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="636"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="433"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="696"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="437"/>
        <source>Complete</source>
        <translation>полный</translation>
    </message>
</context>
<context>
    <name>ScreenPenSettingWidget</name>
    <message>
        <location filename="screenpensettingwidget.ui" line="17"/>
        <source>ScreenPenSettingWidget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="screenpensettingwidget.ui" line="900"/>
        <source>Degree</source>
        <translation>степень</translation>
    </message>
</context>
</TS>
