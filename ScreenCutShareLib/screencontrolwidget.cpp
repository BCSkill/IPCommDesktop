﻿#include "screencontrolwidget.h"
#include <QPoint>
#include <QDebug>
#include <QApplication>
#include <QClipboard>
#include <qfile.h>
#include "oescreenshot.h"
#include "ui_screencontrolwidget.h"

ScreenControlWidget::ScreenControlWidget(QWidget *parent)
: QWidget(parent), ui(new Ui::ScreenControlWidget)
{
	ui->setupUi(this);
	initUI();

	isDrawLine = false;
	isDrawArrow = false;;
	isDrawRound = false;;
	isDrawText = false;;
	isDrawRectang = false;;
	isDrawMasic = false;
	setWindowFlags(Qt::FramelessWindowHint);

	QFile styleFile(":/screencut/Resources/screencut/screencontrolwidget.qss");
	styleFile.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(styleFile.readAll());
	setStyleSheet(styleSheet);
	styleFile.close();
	
	connectSignalAndSlot();
}

ScreenControlWidget::~ScreenControlWidget()
{
	delete ui;
}

void ScreenControlWidget::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);

	QWidget::paintEvent(event);
}

void ScreenControlWidget::initUI()
{
// 	ui->cancelBtn->setStyleSheet("QToolButton{padding: 1px;image: url(:/screencut/Resources/screencut/cancle.png);border:1px;}QToolButton:hover{padding: 0px;image: url(:/screencut/Resources/screencut/cancle.png);border:1px solid grey;}");
// 	ui->finishBtn->setStyleSheet("QToolButton{border-image: url(:/screencut/Resources/screencut/Finish.png);border:1px;}");
// 	ui->rectangleBtn->setStyleSheet("QToolButton{border-image: url(:/screencut/Resources/screencut/Rect.png);border:1px;}");
// 	ui->drawRoundBtn->setStyleSheet("QToolButton{border-image: url(:/screencut/Resources/screencut/Round.png);border:1px;}");
// 	ui->arrowBtn->setStyleSheet("QToolButton{border-image: url(:/screencut/Resources/screencut/Arrow.png);border:1px;}");
// 	ui->drawLineBtn->setStyleSheet("QToolButton{border-image: url(:/screencut/Resources/screencut/Line.png);border:1px;}");
// 	ui->textEditBtn->setStyleSheet("QToolButton{border-image: url(:/screencut/Resources/screencut/Edit.png);border:1px;}");
// 	ui->revertBtn->setStyleSheet("QToolButton{border-image: url(:/screencut/Resources/screencut/revert.png);border:1px;}");
// 	ui->saveBtn->setStyleSheet("QToolButton{border-image: url(:/screencut/Resources/screencut/save.png);border:1px;}");
}

void ScreenControlWidget::connectSignalAndSlot()
{
	connect(ui->cancelBtn, SIGNAL(clicked()), this, SLOT(slotCancelBtnClicked()));
	connect(ui->finishBtn, SIGNAL(clicked()), this, SLOT(slotFinishBtnClicked()));
	connect(ui->rectangleBtn, SIGNAL(clicked()), this, SLOT(slotRectangleBtnClicked()));
	connect(ui->drawRoundBtn, SIGNAL(clicked()), this, SLOT(slotDrawRoundBtnClicked()));
	connect(ui->arrowBtn, SIGNAL(clicked()), this, SLOT(slotArrowBtnClicked()));
	connect(ui->drawLineBtn, SIGNAL(clicked()), this, SLOT(slotDrawLineBtnClicked()));
	connect(ui->textEditBtn, SIGNAL(clicked()), this, SLOT(slotEditBtnClicked()));
	connect(ui->masicButton, SIGNAL(clicked()), this, SLOT(slotMasicBtnClicked()));
	

	connect(ui->revertBtn, SIGNAL(clicked()), this, SLOT(slotRevertBtnClicked()));
	connect(ui->saveBtn, SIGNAL(clicked()), this, SLOT(slotSaveBtnClicked()));
}

//保存Screen类的引用
void ScreenControlWidget::setScreenQuote(OEScreenshot* screen)
{
	this->screen = screen;
}

//取消按钮
void ScreenControlWidget::slotCancelBtnClicked()
{
	if (screen)
	{
        screen->CancleScreenCut();
	}
}

//完成按钮
void ScreenControlWidget::slotFinishBtnClicked()
{
	if (screen)
	{
		screen->SaveCutPicture();
	}
}

//添加字按钮
void ScreenControlWidget::slotEditBtnClicked()
{
	if (screen)
	{
		if (isDrawText)
		{
			screen->DrawTextEnable(false);
			isDrawText = false;
			ui->textEditBtn->setChecked(false);
		}
		else
		{
			screen->DrawTextEnable(true);
			isDrawText = true;
			ui->rectangleBtn->setChecked(false);
			ui->drawRoundBtn->setChecked(false);
			ui->arrowBtn->setChecked(false);
			ui->drawLineBtn->setChecked(false);
			ui->textEditBtn->setChecked(true);
			ui->masicButton->setChecked(false);
		}
	}
}

//绘制矩形
void ScreenControlWidget::slotRectangleBtnClicked()
{
	if (screen)
	{
		if (isDrawRectang)
		{
			screen->DrawRectangEnable(false);
			isDrawRectang = false;
			ui->rectangleBtn->setChecked(false);
		}
		else
		{
			screen->DrawRectangEnable(true);
			isDrawLine = false;
			isDrawRectang = true;
			isDrawRound = false;
			isDrawArrow = false;
			isDrawText = false;
			isDrawMasic = false;
			ui->rectangleBtn->setChecked(true);
			ui->drawRoundBtn->setChecked(false);
			ui->arrowBtn->setChecked(false);
			ui->drawLineBtn->setChecked(false);
			ui->textEditBtn->setChecked(false);
			ui->masicButton->setChecked(false);
		}
	}
}

//绘制圆
void ScreenControlWidget::slotDrawRoundBtnClicked()
{
	if (screen)
	{
		if (isDrawRound)
		{
			screen->DrawRoundEnable(false);
			isDrawRound = false;
			ui->drawRoundBtn->setChecked(false);
		}
		else
		{
			screen->DrawRoundEnable(true);
			isDrawLine = false;
			isDrawRectang = false;
			isDrawRound = true;
			isDrawArrow = false;
			isDrawText = false;
			isDrawMasic = false;
			ui->rectangleBtn->setChecked(false);
			ui->drawRoundBtn->setChecked(true);
			ui->arrowBtn->setChecked(false);
			ui->drawLineBtn->setChecked(false);
			ui->textEditBtn->setChecked(false);
			ui->masicButton->setChecked(false);
		}
	}
}

//绘制箭头
void ScreenControlWidget::slotArrowBtnClicked()
{
	if (screen)
	{
		if (isDrawArrow)
		{
			screen->DrawArrowEnable(false);
			isDrawArrow = false;
			ui->arrowBtn->setChecked(false);
		}
		else
		{
			screen->DrawArrowEnable(true);
			isDrawLine = false;
			isDrawRectang = false;
			isDrawRound = false;
			isDrawArrow = true;
			isDrawText = false;
			isDrawMasic = false;
			ui->rectangleBtn->setChecked(false);
			ui->drawRoundBtn->setChecked(false);
			ui->arrowBtn->setChecked(true);
			ui->drawLineBtn->setChecked(false);
			ui->textEditBtn->setChecked(false);
			ui->masicButton->setChecked(false);
		}
	}
}

//绘制线条
void ScreenControlWidget::slotDrawLineBtnClicked()
{
	if (screen)
	{
		if (isDrawLine)
		{
			screen->DrawLineEnable(false);
			isDrawLine = false;
			ui->drawLineBtn->setChecked(false);
		}
		else
		{
			screen->DrawLineEnable(true);
			isDrawLine = true;
			isDrawRectang = false;
			isDrawRound = false;
			isDrawArrow = false;
			isDrawText = false;
			isDrawMasic = false;
			ui->rectangleBtn->setChecked(false);
			ui->drawRoundBtn->setChecked(false);
			ui->arrowBtn->setChecked(false);
			ui->drawLineBtn->setChecked(true);
			ui->textEditBtn->setChecked(false);
			ui->masicButton->setChecked(false);
		}
	}
}

void ScreenControlWidget::slotMasicBtnClicked()
{
	if (screen)
	{
		if (isDrawMasic)
		{
			screen->DrawMasicEnable(false);
			isDrawMasic = false;
			ui->masicButton->setChecked(false);
		}
		else
		{
			screen->DrawMasicEnable(true);
			isDrawLine = false;
			isDrawRectang = false;
			isDrawRound = false;
			isDrawArrow = false;
			isDrawText = false;
			isDrawMasic = true;
			ui->rectangleBtn->setChecked(false);
			ui->drawRoundBtn->setChecked(false);
			ui->arrowBtn->setChecked(false);
			ui->drawLineBtn->setChecked(false);
			ui->textEditBtn->setChecked(false);
			ui->masicButton->setChecked(true);
		}
	}
}

//撤销
void ScreenControlWidget::slotRevertBtnClicked()
{
	screen->RevertPaints();
}

//保存
void ScreenControlWidget::slotSaveBtnClicked()
{
	screen->SaveAsPic();
}
