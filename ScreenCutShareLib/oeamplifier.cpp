﻿#include "oeamplifier.h"

#include <QPixmap>
#include <QPainter>

#ifndef QT_NO_DEBUG
#include <QDebug>
#endif

#include "oecommonhelper.h"

OEAmplifier::OEAmplifier(std::shared_ptr<QPixmap> originPainting,
                         QWidget *parent) :QWidget(parent),
                         originPainting_(originPainting)
{
    setWindowFlags(Qt::FramelessWindowHint|Qt::WindowSystemMenuHint);
    setMouseTracking(true);

    sideLength_ = 122 * OECommonHelper::getWindowHeightMultiplyingPower();
    imageHeight_ = 90 * OECommonHelper::getWindowHeightMultiplyingPower();
    setFixedSize(sideLength_,sideLength_);

    hide();
}

void OEAmplifier::onSizeChange(int w, int h) 
{
    screenSize_ = QSize(w, h);
}

void OEAmplifier::onPostionChange(int x, int y) 
{
    cursorPoint_ = QPoint(x, y);
    raise();
    int dest_x = x + 4;
    int dest_y = y + 26;

    const QSize& parent_size = parentWidget()->size();
    if (dest_y + height() > parent_size.height()) {
        dest_y = y - 26 - height();
    }
    if (dest_x + width() > parent_size.width()) {
        dest_x = x - 4 - width();
    }

    move(dest_x, dest_y);
}


/// 绘制鼠标拖拽时选区矩形的右下顶点的放大图;
void OEAmplifier::paintEvent(QPaintEvent *) {
    QPainter painter(this);

    /// 绘制背景
    painter.fillRect(rect(), QColor(0, 0, 0, 160));

    QPixmap endPointImage;
    /// 绘制放大图;
    const QSize& parent_size = parentWidget()->size();
   
    if ((cursorPoint_.x() + 15 < parent_size.width() && cursorPoint_.x() - 15 > 0)
      && (cursorPoint_.y() + 11 < parent_size.height() && cursorPoint_.y() - 11 > 0))
	{
        endPointImage = originPainting_->copy(QRect(cursorPoint_.x() - 15,cursorPoint_.y() - 11, 30, 22)).scaled(sideLength_, imageHeight_);
        painter.drawPixmap(0,0, endPointImage);
    }
    else
	{
        endPointImage = originPainting_->copy(QRect(cursorPoint_.x() - 15,cursorPoint_.y() - 11, 30, 22));
    }

    /// 绘制十字
    painter.setPen(QPen(QColor(0, 180, 255 , 180), 4));
    // 竖线;
    painter.drawLine(QPoint(sideLength_ >> 1, 0), QPoint(sideLength_ >> 1, imageHeight_ - 4));
    // 横线;
    painter.drawLine(QPoint(0, imageHeight_ >> 1), QPoint(sideLength_, imageHeight_ >> 1));

    /// 绘制大图内边框
    painter.setPen(QPen(Qt::white, 2));
    painter.drawRect(2,2,width()-4, imageHeight_-4);

    /// 绘制外边框
    painter.setPen(QPen(Qt::black, 1));
    painter.drawRect(0,0,width()-1,height()-1);

    /// 当前选中矩形的宽高信息;
    QString select_screen_info = QString("%1×%2").arg(screenSize_.width()).arg(screenSize_.height());

    /// 当前鼠标像素值的RGB信息
    QImage image = originPainting_->toImage();
    QColor cursor_pixel = image.pixel(cursorPoint_);
    QString select_pt_rgb = QString("RGB:(%1,%2,%3)").arg(cursor_pixel.red()).arg(cursor_pixel.green()).arg(cursor_pixel.blue());

    /// 绘制坐标轴相关数据
	QRect rect;
	OECommonHelper::getSmallestWindowFromCursor(rect);
	QString strRect = QString::number(rect.width()) + " X "+QString::number(rect.height());
    painter.setPen(Qt::white);
	QFont font;
	font.setFamily("Microsoft YaHei");
	int iSize = 16;
	while (iSize>0)
	{
		font.setPointSize(iSize);
		QFontMetrics fm(font);
		QRect rec = fm.boundingRect("RGB(255,255,255)");
		if (rec.width() > width()-10 && rec.height()>12)
		{
			iSize--;
		}
		else
		{
			break;
		}
	}
	if (iSize > 0)
	{
		painter.setFont(font);
		painter.drawText(QPoint(4, imageHeight_ + 14), strRect);
		painter.drawText(QPoint(4, imageHeight_ + 27), select_pt_rgb);
	}
}
