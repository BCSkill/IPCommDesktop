﻿#include "oescreenshot.h"
#include <QDesktopWidget>
#include <QApplication>
#include <QMouseEvent>
#include <QFileDialog>
#include <QClipboard>
#include <QDateTime>
#include <QPainter>
#include <QScreen>
#include <QCursor>
#include <QMutex>
#include <QMenu>
#include <QPen>
#include <QUuid>
#include <math.h>
#include <QJsonDocument>
#include <QJsonObject>
#include <QLabel>
#ifndef QT_NO_DEBUG
#include <QDebug>
#endif

#ifdef Q_OS_WIN
#include <windows.h>
#endif
#include "oeamplifier.h"
#include "oecommonhelper.h"
#include "QStringLiteralBak.h"
#include "masicWidget.h"

/// 鼠标按钮图片的十六进制数据
static const unsigned char uc_mouse_image[] = {
    0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A,  0x00, 0x00, 0x00, 0x0D, 0x49, 0x48, 0x44, 0x52
    ,0x00, 0x00, 0x00, 0x1D, 0x00, 0x00, 0x00, 0x2D,  0x08, 0x06, 0x00, 0x00, 0x00, 0x52, 0xE9, 0x60
    ,0xA2, 0x00, 0x00, 0x00, 0x09, 0x70, 0x48, 0x59,  0x73, 0x00, 0x00, 0x0B, 0x13, 0x00, 0x00, 0x0B
    ,0x13, 0x01, 0x00, 0x9A, 0x9C, 0x18, 0x00, 0x00,  0x01, 0x40, 0x49, 0x44, 0x41, 0x54, 0x58, 0x85
    ,0xED, 0xD5, 0x21, 0x6E, 0xC3, 0x30, 0x14, 0xC6,  0xF1, 0xFF, 0x9B, 0xC6, 0x36, 0x30, 0x38, 0xA9
    ,0x05, 0x01, 0x05, 0x81, 0x05, 0x03, 0x39, 0xCA,  0x60, 0x8F, 0xD2, 0x03, 0xEC, 0x10, 0x3B, 0x46
    ,0xC1, 0xC0, 0xC6, 0x0A, 0x3B, 0x96, 0xB1, 0x80,  0x82, 0xC1, 0x56, 0x2A, 0xFF, 0x06, 0xE2, 0x36
    ,0x75, 0x9A, 0xB4, 0xCA, 0xEC, 0x4E, 0x9A, 0xE4,  0x2F, 0xB2, 0x42, 0x22, 0xFF, 0xF2, 0xFC, 0x9C
    ,0x18, 0x52, 0x52, 0x52, 0x52, 0x52, 0x52, 0x52,  0x52, 0x52, 0x52, 0xFE, 0x55, 0xE4, 0xC6, 0xA0
    ,0xDC, 0xC4, 0x71, 0x87, 0xC1, 0xC1, 0x68, 0x01,  0xCC, 0x06, 0xC2, 0x51, 0xD0, 0x29, 0xB0, 0x18
    ,0x00, 0xDF, 0xC6, 0x40, 0x33, 0x37, 0x84, 0x30,  0x4C, 0x80, 0x85, 0xCE, 0x7B, 0x2E, 0x2A, 0x91
    ,0x84, 0x24, 0xBE, 0x25, 0xDE, 0x25, 0x5E, 0x2F,  0x6E, 0xAE, 0xD0, 0x37, 0x92, 0x10, 0xF0, 0x09
    ,0x54, 0x40, 0xE9, 0xEE, 0x15, 0xC6, 0xA2, 0x77,  0xFE, 0xE0, 0xE5, 0x85, 0x8F, 0x16, 0x58, 0xDF
    ,0x35, 0x06, 0x5B, 0xD3, 0xB9, 0xD4, 0x11, 0xD0,  0xA5, 0x8F, 0xDE, 0x57, 0x75, 0x83, 0x73, 0x50
    ,0x06, 0xF6, 0x72, 0x0A, 0x47, 0x40, 0x57, 0x0D,  0x38, 0xDE, 0xC0, 0x04, 0x6F, 0x68, 0x05, 0x36
    ,0xF5, 0xE1, 0x08, 0x3D, 0xCD, 0xEA, 0xEA, 0x5A,  0xD8, 0xBE, 0x5A, 0x46, 0xB0, 0x05, 0x1E, 0xAC
    ,0xF1, 0xC2, 0xD1, 0xCC, 0x01, 0x6D, 0x74, 0x02,  0xDB, 0x3B, 0xBF, 0xD3, 0x73, 0x07, 0x87, 0x2F
    ,0xEF, 0x53, 0x07, 0x38, 0x82, 0x2F, 0xF6, 0xFB,  0xB8, 0x81, 0x73, 0x41, 0x69, 0x28, 0x3A, 0x7A
    ,0x5C, 0xDD, 0x73, 0xCF, 0x3A, 0x86, 0xA3, 0x05,  0x87, 0xEA, 0xCC, 0x60, 0xA1, 0x06, 0x75, 0x89
    ,0xFE, 0x77, 0x92, 0x76, 0x68, 0x23, 0xEF, 0x88,  0xD3, 0x4C, 0xA8, 0x10, 0x7A, 0xD4, 0xEF, 0x8E
    ,0xBE, 0x8B, 0x68, 0x79, 0x3A, 0xB1, 0x72, 0xE1,  0xAE, 0xBC, 0x13, 0x0D, 0xDE, 0xBD, 0x3D, 0xF3
    ,0x08, 0x15, 0xD4, 0xDF, 0x4C, 0x06, 0x36, 0xF7,  0x9E, 0x09, 0xED, 0xE9, 0x99, 0x97, 0x3E, 0x42
    ,0xFF, 0x30, 0x42, 0x4B, 0xA1, 0x8D, 0xD8, 0xE9,  0x2A, 0xBD, 0xED, 0x41, 0x25, 0x2A, 0x89, 0x37
    ,0x1F, 0xBD, 0xEA, 0x61, 0x8B, 0x5F, 0xDD, 0xC1,  0xFA, 0x01, 0xD8, 0xA3, 0x8F, 0xFB, 0xCA, 0x70
    ,0x16, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45,  0x4E, 0x44, 0xAE, 0x42, 0x60, 0x82
};



OEScreenshot * OEScreenshot::self_ = nullptr;
bool OEScreenshot::isActivity_ = false;
bool OEScreen::isInit_ = false;
HWND mhWnd = NULL;
QString m_thisParm;

OEScreenshot::OEScreenshot(QWidget *parent) : QWidget(parent),
    isLeftPressed_ (false), backgroundScreen_(nullptr),
	originPainting_(nullptr), screenTool_(nullptr), m_pControlWidget(NULL), control(NULL), m_pPenSettingWidget(NULL)
{
    /// 初始化鼠标
    initCursor();
    /// 截取屏幕信息
    initGlobalScreen();
    /// 初始化鼠标放大器
    initAmplifier();
    /// 初始化大小感知器
    initMeasureWidget();
    /// 全屏窗口
    showFullScreen();
    /// 窗口与显示屏对齐
    setGeometry(getScreenRect());
    /// 霸道置顶
    onEgoistic();
    /// 开启鼠标实时追踪
    setMouseTracking(true);
    /// 更新鼠标的位置
    emit cursorPosChange(cursor().pos().x(), cursor().pos().y());
    /// 更新鼠标区域窗口
    updateMouse();
    /// 展示窗口
    show();
}

OEScreenshot::~OEScreenshot(void) 
{
}

OEScreenshot *OEScreenshot::Instance(HWND hWnd, QString thisParam)
{
	mhWnd = hWnd;
	m_thisParm = thisParam;
    if (!isActivity_ && self_) 
	{
        destroy();
    }
    static QMutex mutex;
    if (!self_) 
	{
       QMutexLocker locker(&mutex);
       if (!self_)
	   {
           isActivity_ = true;
           self_ = new OEScreenshot;
       }
    }
    return self_;
}

void OEScreenshot::destroy(void)
{
    if (!isActivity_ && self_) 
	{
        delete self_;
        self_ = nullptr;
    }
}

void OEScreenshot::hideEvent(QHideEvent *) 
{
    isActivity_ = false;
}

void OEScreenshot::closeEvent(QCloseEvent *) 
{
	if (m_pControlWidget)
	{
		m_pControlWidget->close();
	}
	if (m_pPenSettingWidget)
	{
		m_pPenSettingWidget->close();
	}
    isActivity_ = false;
}

void OEScreenshot::mouseDoubleClickEvent(QMouseEvent *) 
{
    emit doubleClick();
}

void OEScreenshot::initAmplifier(std::shared_ptr<QPixmap> originPainting) 
{
    std::shared_ptr<QPixmap>  temp_pm = originPainting;
    if (temp_pm == nullptr) {
        temp_pm = originPainting_;
    }
    amplifierTool_.reset(new OEAmplifier(temp_pm, this));
    connect(this,SIGNAL(cursorPosChange(int,int)),
            amplifierTool_.get(), SLOT(onPostionChange(int,int)));
    amplifierTool_->show();
    amplifierTool_->raise();
}

void OEScreenshot::initMeasureWidget(void)
{
    rectTool_.reset(new OERect(this));
    rectTool_->raise();
}

const QRect &OEScreenshot::getScreenRect(void) 
{
    if (!desktopRect_.isEmpty()) 
	{
        return desktopRect_;
    }
    desktopRect_ = QRect(QApplication::desktop()->pos(),
          QApplication::desktop()->size());
    return desktopRect_;
}

std::shared_ptr<QPixmap> OEScreenshot::initGlobalScreen(void)
{
    if (backgroundScreen_.get() != nullptr)
	{
        return backgroundScreen_;
    }
    std::shared_ptr<QPixmap> temp_screen = getGlobalScreen();
    QPixmap temp_dim_pix(temp_screen->width(), temp_screen->height());
    temp_dim_pix.fill((QColor(0, 0, 0, 160)));
    backgroundScreen_.reset(new QPixmap(*temp_screen));
    QPainter p(backgroundScreen_.get());
    p.drawPixmap(0, 0, temp_dim_pix);

    return backgroundScreen_;
}

std::shared_ptr<QPixmap> OEScreenshot::getGlobalScreen(void) 
{
    if (originPainting_.get() == nullptr)
	{
        /// 截取当前桌面，作为截屏的背景图
        QScreen *screen = QGuiApplication::primaryScreen();
        const QRect& temp_rect = getScreenRect();
        originPainting_.reset(new QPixmap(screen->grabWindow(0, temp_rect.x(),temp_rect.y(), temp_rect.width(),temp_rect.height())));
    }
    return originPainting_;
}

void OEScreenshot::onEgoistic(void)
{
    /// 窗口置顶
#ifdef Q_OS_WIN32
    SetWindowPos((HWND)this->winId(),HWND_TOPMOST,this->pos().x(),this->pos().y(),this->width(),this->height(),SWP_SHOWWINDOW);
#else
    Qt::WindowFlags flags = windowFlags();
    flags |= Qt::WindowStaysOnTopHint;
    setWindowFlags(flags);
#endif
}

void OEScreenshot::initCursor(const QString& ico) 
{
    QPixmap pixmap;
    if (ico.isEmpty()) 
	{
        pixmap.loadFromData(uc_mouse_image, sizeof(uc_mouse_image));
    }
    else {
        pixmap.load(ico);
    }
    QCursor cursor;
    cursor = QCursor(pixmap, 15, 23);
    setCursor(cursor);
}

std::shared_ptr<OEScreen> OEScreenshot::createScreen(const QPoint &pos)
{
    if (screenTool_.get() == nullptr) 
	{
        /// 创建截图器
        screenTool_.reset(new OEScreen(originPainting_, pos, this));
        /// 建立信号连接
        connect (this, SIGNAL(cursorPosChange(int,int)),screenTool_.get(),SLOT(onMouseChange(int,int)));
        /// 建立主界面双击保存信号关联
      //  connect (this, SIGNAL(doubleClick()),screenTool_.get(),SLOT(onSaveScreen()));
        /// 建立截图器大小关联
        connect(screenTool_.get(), SIGNAL(sizeChange(int,int)),rectTool_.get(), SLOT(onSizeChange(int,int)));
        connect(screenTool_.get(), SIGNAL(sizeChange(int,int)), amplifierTool_.get(), SLOT(onSizeChange(int,int)));
        /// 建立截图器与感知器的位置关联
        connect(screenTool_.get(), SIGNAL(postionChange(int,int)),rectTool_.get(), SLOT(onPostionChange(int,int)));

		connect(screenTool_.get(), SIGNAL(sizeChange(int, int)), this, SLOT(locationChange(int, int)));
		connect(screenTool_.get(), SIGNAL(postionChange(int, int)), this, SLOT(locationChange(int, int)));

        connect(screenTool_.get(), SIGNAL(sigSaveScreenPicture()), this, SLOT(slotScreenComplete()));

        /// 获得截图器当前起始位置
        startPoint_ = pos;
        isLeftPressed_ = true;
    }
    return screenTool_;
}

void OEScreenshot::slotScreenComplete()
{
	QJsonObject json;
	json.insert("CMD", "ScreenShotComplete");
	json.insert("widgetPtr", m_thisParm);//要传递到的widget指针
	QString strJSON = QString(QJsonDocument(json).toJson());
	SendCutPictureMessage(strJSON);
}

void OEScreenshot::SendCutPictureMessage(QString strJSON)
{
	QByteArray  ByteArray = strJSON.toUtf8();
	const char *strMsg = ByteArray.constData();
	COPYDATASTRUCT copyData = { 0 };
	copyData.dwData = 1;                                                                                                        //标志发送CString类型
	copyData.cbData = strlen(strMsg);
	copyData.lpData = (void*)strMsg;
	HWND hWnd = (HWND)winId();

	if (mhWnd)   //窗口句柄有可能传空，传空时只保存剪贴板，不发送窗口消息。
	    ::SendMessage(mhWnd, WM_COPYDATA, (WPARAM)hWnd, (LPARAM)&copyData);
}

void OEScreenshot::locationChange(int x, int y)
{
	int w = screenTool_->x() + screenTool_->width();
	int h = screenTool_->y() + screenTool_->height();

	QPoint pos = QWidget::mapToGlobal(screenTool_->pos());
	QRect deskRt = QApplication::desktop()->availableGeometry(pos);
	double g_nActScreenW = deskRt.width();
	double g_nActScreenH = deskRt.height();

	if (h+68<= g_nActScreenH)
	{
		if (m_pControlWidget&&m_pPenSettingWidget)
		{
			m_pControlWidget->move(w - m_pControlWidget->width(), h + 2);
			m_pPenSettingWidget->move(w - m_pControlWidget->width(), h + 32);
		}
	}
	else if (screenTool_->y() - 68 > 0)
	{
		if (m_pControlWidget&&m_pPenSettingWidget)
		{
			m_pControlWidget->move(w - m_pControlWidget->width(), screenTool_->y() - 32);
			m_pPenSettingWidget->move(w - m_pControlWidget->width(), screenTool_->y() - 70);
		}
	}
	else
	{
		if (m_pControlWidget&&m_pPenSettingWidget)
		{
			m_pControlWidget->move(w - m_pControlWidget->width(), screenTool_->y());
			m_pPenSettingWidget->move(w - m_pControlWidget->width(), screenTool_->y() + 30);
		}
	}
}

void OEScreenshot::destroyScreen()
{
    if (screenTool_.get() != nullptr) 
	{
        //disconnect (this, SIGNAL(doubleClick()),screenTool_.get(),SLOT(onSaveScreen()));
        disconnect(screenTool_.get(), SIGNAL(sizeChange(int,int)),rectTool_.get(), SLOT(onSizeChange(int,int)));
        disconnect(screenTool_.get(), SIGNAL(postionChange(int,int)),rectTool_.get(), SLOT(onPostionChange(int,int)));
 
        screenTool_.reset();
        screenTool_ = nullptr;
		m_pControlWidget->hide();
		m_pPenSettingWidget->hide();
        isLeftPressed_ = false;
        update();
        return;
    }
}

void OEScreenshot::SaveCutPicture()
{
	if (screenTool_.get())
	{
		screenTool_->onSaveScreen();
	}
}

void OEScreenshot::DrawRectangEnable(bool bState)
{
	if (screenTool_.get())
	{
		screenTool_->setDrawRectangState(bState);
	}
	if (bState)
	{
		m_pPenSettingWidget->setType(0);
		m_pPenSettingWidget->show();
	}
	else
	{
		m_pPenSettingWidget->hide();
	}

}

void OEScreenshot::DrawLineEnable(bool bState)
{
	if (screenTool_.get())
	{
		screenTool_->setDrawLineState(bState);
	}
	if (bState)
	{
		m_pPenSettingWidget->setType(0);
		m_pPenSettingWidget->show();
	}
	else
	{
		m_pPenSettingWidget->hide();
	}
}

void OEScreenshot::DrawMasicEnable(bool bState)
{
	if (screenTool_.get())
	{
		screenTool_->setDrawMasicState(bState);
	}
	if (bState)
	{
		m_pPenSettingWidget->setType(2);
		m_pPenSettingWidget->show();
	}
	else
	{
		m_pPenSettingWidget->hide();
	}
}

void OEScreenshot::DrawArrowEnable(bool bState)
{
	if (screenTool_.get())
	{
		screenTool_->setDrawArrowState(bState);
	}
	if (bState)
	{
		m_pPenSettingWidget->setType(0);
		m_pPenSettingWidget->show();
	}
	else
	{
		m_pPenSettingWidget->hide();
	}
}

void OEScreenshot::DrawRoundEnable(bool bState)
{
	if (screenTool_.get())
	{
		screenTool_->setDrawRoundState(bState);
	}
	if (bState)
	{
		m_pPenSettingWidget->setType(0);
		m_pPenSettingWidget->show();
	}
	else
	{
		m_pPenSettingWidget->hide();
	}
}

void OEScreenshot::DrawTextEnable(bool bState)
{
	if (screenTool_.get())
	{
		screenTool_->setDrawTextState(bState);
	}
	if (bState)
	{
		m_pPenSettingWidget->setType(1);
		m_pPenSettingWidget->show();
	}
	else
	{
		m_pPenSettingWidget->hide();
	}
}

void OEScreenshot::CancleScreenCut()
{
	QJsonObject json;
	json.insert("CMD", "ScreenCancle");
	json.insert("widgetPtr", m_thisParm);//要传递到的widget指针
	QString strJSON = QString(QJsonDocument(json).toJson());
	SendCutPictureMessage(strJSON);
    emit sigScreenCanclePixMap();
    close();
}

void OEScreenshot::RevertPaints()
{
	if (screenTool_.get())
	{
		screenTool_->RevertPaints();
	}
}

void OEScreenshot::SaveAsPic()
{
	if (screenTool_.get())
	{
		screenTool_->onSaveAsScreen();
	}
}

void OEScreenshot::mousePressEvent(QMouseEvent *e) 
{
    if (e->button() == Qt::LeftButton) 
	{
        createScreen(e->pos());
        return ;
    }
}

void OEScreenshot::mouseReleaseEvent(QMouseEvent *e)
{
    if (e->button() == Qt::RightButton) 
	{
        if (screenTool_.get() != nullptr) 
		{
            rectTool_->hide();
            amplifierTool_->onPostionChange(e->x(), e->y());
            amplifierTool_->show();
            updateMouse();
            return destroyScreen();
        }
        close();
        return ;
    }
    else if (isLeftPressed_ == true && e->button() == Qt::LeftButton) 
	{
        if (startPoint_ == e->pos() && !windowRect_.isEmpty())
		{
            screenTool_->setGeometry(windowRect_);
            screenTool_->show();
            windowRect_ = {};
        }
        disconnect (this, SIGNAL(cursorPosChange(int,int)),screenTool_.get(),SLOT(onMouseChange(int,int)));
        amplifierTool_->hide();
        disconnect (screenTool_.get(), SIGNAL(sizeChange(int,int)),amplifierTool_.get(),SLOT(onSizeChange(int,int)));
		
		if (!m_pControlWidget)
		{
			m_pControlWidget = new ScreenControlWidget(this);
			//m_pControlWidget->setStyleSheet("QWidget{background-color: #eaecf0;}");
			m_pControlWidget->setScreenQuote(this);
		}
		if (!m_pPenSettingWidget)
		{
			m_pPenSettingWidget = new ScreenPenSettingWidget(this);
			connect(m_pPenSettingWidget, SIGNAL(sigSetColor(QColor)), this, SLOT(setPenColor(QColor)));
			connect(m_pPenSettingWidget, SIGNAL(sigSetLine(int)), this, SLOT(setPenLine(int)));
			connect(m_pPenSettingWidget, SIGNAL(sigTextSizeChanged(QString)), this, SLOT(setTextSize(QString)));
			connect(m_pPenSettingWidget, SIGNAL(sigMasicChanged(int)), this, SLOT(setMasicSize(int)));
		}
		m_pControlWidget->show();
		
		locationChange(0,0);//参数无意义

        isLeftPressed_ = false;
    }
    QWidget::mouseReleaseEvent(e);
}

void OEScreenshot::mouseMoveEvent(QMouseEvent *e)
{
    emit cursorPosChange(e->x(), e->y());

    if (isLeftPressed_)
	{
		amplifierTool_->raise();
        windowRect_ = {};
        update();
    }
    else if (isLeftPressed_ == false && false == OEScreen::state())
	{
        onEgoistic();
        updateMouse();
    }
// 	if (m_pControlWidget != NULL)
// 	{
// 		QPoint p1 = m_pControlWidget->mapToGlobal(QPoint(0, 0));
// 		QRect rect1 = m_pControlWidget->rect();
// 		int ix = rect1.width();
// 		int iy = rect1.height();
// 		rect1.setX(p1.x());
// 		rect1.setY(p1.y());
// 		rect1.setWidth(ix);
// 		rect1.setHeight(iy);
// 		QPoint pos1 = cursor().pos();
// 		if (rect1.contains(pos1))
// 		{
// 			setCursor(QCursor(Qt::ArrowCursor));
// 		}
// 		else
// 		{
// 			initCursor();
// 		}
// 	}
// 
// 	else if (m_pPenSettingWidget != NULL)
// 	{
// 		QPoint p2 = m_pPenSettingWidget->mapToGlobal(QPoint(0, 0));
// 		QRect rect2 = m_pPenSettingWidget->rect();
// 		int ix2 = rect2.width();
// 		int iy2 = rect2.height();
// 		rect2.setX(p2.x());
// 		rect2.setY(p2.y());
// 		rect2.setWidth(ix2);
// 		rect2.setHeight(iy2);
// 		QPoint pos2 = cursor().pos();
// 		if (rect2.contains(pos2))
// 		{
// 			setCursor(QCursor(Qt::ArrowCursor));
// 		}
// 		else
// 		{
// 			initCursor();
// 		}
// 	}
// 	else
// 	{
// 		initCursor();
// 	}
    QWidget::mouseMoveEvent(e);
}

void OEScreenshot::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.drawPixmap(0,0,desktopRect_.width(),desktopRect_.height(), *backgroundScreen_);
    if (!windowRect_.isEmpty()) 
	{
        QPen pen = painter.pen();
        pen.setColor(QColor(0,175,255));
        pen.setWidth(5);
		pen.setJoinStyle(Qt::PenJoinStyle::MiterJoin);
        painter.setPen(pen);
        painter.drawRect(windowRect_.x(),windowRect_.y(),windowRect_.width(),windowRect_.height());
        painter.drawPixmap(QPoint(windowRect_.x(),windowRect_.y()),*originPainting_, windowRect_);
    }
}

void OEScreenshot::updateMouse(void) 
{
    OECommonHelper::getSmallestWindowFromCursor(windowRect_);
    QPoint temp_pt = mapFromGlobal(QPoint(windowRect_.x(), windowRect_.y()));
    windowRect_ = QRect(temp_pt.x(), temp_pt.y(),
                        windowRect_.width(), windowRect_.height());
#ifdef Q_OS_WIN
    ::EnableWindow((HWND)winId(), TRUE);
#endif
    amplifierTool_->onSizeChange(windowRect_.width(),windowRect_.height());
    emit findChildWind(windowRect_);
    update();
}

void OEScreenshot::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape) 
	{
		CancleScreenCut();
    }
    else 
	{
        e->ignore();
    }
}


OERect::OERect(QWidget *parent) : QWidget(parent)
{
    setFixedSize(95 * OECommonHelper::getWindowHeightMultiplyingPower(),20 * OECommonHelper::getWindowHeightMultiplyingPower());
    backgroundPixmap_.reset(new QPixmap(width(),height()));
    backgroundPixmap_->fill((QColor(8, 8, 8, 160)));
    hide();
}

void OERect::paintEvent(QPaintEvent *) 
{
    QPainter painter(this);
    painter.drawPixmap(rect(),*backgroundPixmap_);
    painter.setPen(QPen(QColor(Qt::white)));
    painter.drawText(rect(), Qt::AlignCenter, info_);
}

void OERect::onPostionChange(int x, int y) 
{
    if (x < 0) x = 0;
    if (y < 0) y = 0;
    const int& ry = y - height() - 1;
    if (ry < 0) 
	{
        this->raise();
    }
    move(x, ((ry < 0) ? y : ry));
    show();
}

void OERect::onSizeChange(int w, int h) 
{
    info_ = QString("%1 X %2").arg(w).arg(h);
}

///////////////////////////////////////////////////////////
OEScreen::OEScreen(std::shared_ptr<QPixmap> originPainting, QPoint pos, QWidget *parent)
	: QWidget(parent), direction_(NONE), originPoint_(pos),
	isPressed_(false), originPainting_(originPainting)
{
	isdrawline = false;
	isdrawrectangle = false;
	isdrawround = false;
	istextedit = false;
	isdrawarrow = false;
	ismasic = false;

	m_PenColor  = QColor(255,0,0);
	m_PenLine = 4;
	m_TextSize = 9;
	m_MasicSize = 10;
	m_iMasicGroup = 0;
	m_pMasic = new masicWidget();
	m_originImg = originPainting_->toImage();
	m_bNeedReMasic = true;

	m_plaintextedit = new QTextEdit(this);
	m_plaintextedit->hide();
	m_plaintextedit->resize(70, 70);
	QPalette p1 = m_plaintextedit->palette();
	QBrush br = QBrush(QColor(255, 0, 0, 0));
	p1.setBrush(QPalette::Base, br);
	m_plaintextedit->setPalette(p1);
	m_plaintextedit->setStyleSheet("QTextEdit{ border: 1px solid #dadada;}");
	connect(m_plaintextedit, SIGNAL(textChanged()), this, SLOT(slotTextChanged()));


    /// 开启鼠标实时追踪
    setMouseTracking(true);
    /// 默认隐藏
    hide();
}

OEScreen::~OEScreen()
{ 
	isInit_ = false; 
	if (m_plaintextedit)
	{
		delete m_plaintextedit;
		m_plaintextedit = NULL;
	}
	if (m_pMasic)
	{
		delete m_pMasic;
		m_pMasic = NULL;
	}
}

OEScreen::DIRECTION OEScreen::getRegion(const QPoint &cursor) 
{
    if (!isInit_) 
	{
        return NONE;
    }
    OEScreen::DIRECTION ret_dir = NONE;

    QPoint pt_lu = mapToParent(rect().topLeft());

    QPoint pt_rl = mapToParent(rect().bottomRight());

    int x = cursor.x();
    int y = cursor.y();
	if (isdrawarrow || isdrawline || isdrawrectangle || isdrawround)
	{
		this->setCursor(QCursor(Qt::CrossCursor));
	}
	else if (ismasic)
	{
		QPixmap pixCur;
		if (m_PenLine == 1)
		{
			pixCur.load(":/screencut/Resources/screencut/cur12.png");
		}
		else if (m_PenLine == 4)
		{
			pixCur.load(":/screencut/Resources/screencut/cur24.png");
		}
		else if (m_PenLine == 9)
		{
			pixCur.load(":/screencut/Resources/screencut/cur48.png");
		}
		setCursor(pixCur);
	}
	else if (istextedit)
	{
		this->setCursor(QCursor(Qt::IBeamCursor));
	}
	else
	{
		if (pt_lu.x() + PADDING_ >= x && pt_lu.x() <= x && pt_lu.y() + PADDING_ >= y && pt_lu.y() <= y)
		{
			ret_dir = LEFTUPPER;
			this->setCursor(QCursor(Qt::SizeFDiagCursor));
		}
		else if (x >= pt_rl.x() - PADDING_ && x <= pt_rl.x() && y >= pt_rl.y() - PADDING_ && y <= pt_rl.y())
		{
			ret_dir = RIGHTLOWER;
			this->setCursor(QCursor(Qt::SizeFDiagCursor));
		}
		else if (x <= pt_lu.x() + PADDING_ && x >= pt_lu.x() && y >= pt_rl.y() - PADDING_ && y <= pt_rl.y())
		{
			ret_dir = LEFTLOWER;
			this->setCursor(QCursor(Qt::SizeBDiagCursor));
		}
		else if (x <= pt_rl.x() && x >= pt_rl.x() - PADDING_ && y >= pt_lu.y() && y <= pt_lu.y() + PADDING_)
		{
			ret_dir = RIGHTUPPER;
			this->setCursor(QCursor(Qt::SizeBDiagCursor));
		}
		else if (x <= pt_lu.x() + PADDING_&& x >= pt_lu.x())
		{
			ret_dir = LEFT;
			this->setCursor(QCursor(Qt::SizeHorCursor));
		}
		else if (x <= pt_rl.x() && x >= pt_rl.x() - PADDING_)
		{
			ret_dir = RIGHT;
			this->setCursor(QCursor(Qt::SizeHorCursor));
		}
		else if (y >= pt_lu.y() && y <= pt_lu.y() + PADDING_)
		{
			ret_dir = UPPER;
			this->setCursor(QCursor(Qt::SizeVerCursor));
		}
		else if (y <= pt_rl.y() && y >= pt_rl.y() - PADDING_)
		{
			ret_dir = LOWER;
			this->setCursor(QCursor(Qt::SizeVerCursor));
		}
		else
		{
			ret_dir = NONE;
			this->setCursor(QCursor(Qt::SizeAllCursor));
		}
	}
    return ret_dir;
}


void OEScreen::contextMenuEvent(QContextMenuEvent *) 
{
    /// 在鼠标位置弹射出菜单栏
  //  menu_->exec(cursor().pos());
}

void OEScreen::mouseDoubleClickEvent(QMouseEvent *e)
{
    if (e->button() == Qt::LeftButton) 
	{
		onSaveScreen();
        e->accept();
    }
}

void OEScreen::mousePressEvent(QMouseEvent *e)
{
    if (e->button() == Qt::LeftButton) 
	{
        isPressed_ = true;
		m_iLineGroup = 0;
		m_iMasicGroup = 0;
        if(direction_ != NONE) 
		{
            this->mouseGrabber();
        }
		startPoint = e->pos();
		endPoint = e->pos();
		if (isdrawrectangle)
		{
			myRectangle *rectangle = new myRectangle;
			rectangle->startPoint = startPoint;
			rectangle->endPoint = endPoint;
			mRectangles.push_back(rectangle);
			mRectanglesColor.push_back(m_PenColor);
			mRectanglesL.push_back(m_PenLine);
			m_vecPaintsNum.push_back(Type_Rect);
		}
		else if (isdrawround)
		{
			myRound *round = new myRound;
			round->startPoint = startPoint;
			round->endPoint = endPoint;
			mRounds.push_back(round);
			mRoundsColor.push_back(m_PenColor);
			mRoundsL.push_back(m_PenLine);
			m_vecPaintsNum.push_back(Type_Round);
		}
		else if (isdrawarrow)
		{
			myArrow *arrow = new myArrow;
			arrow->startPoint = startPoint;
			arrow->endPoint = endPoint;
			mArrows.push_back(arrow);
			mArrowsColor.push_back(m_PenColor);
			mArrowsL.push_back(m_PenLine);
			m_vecPaintsNum.push_back(Type_Arrow);
		}
		if (istextedit)
		{
			if (m_plaintextedit->toPlainText().size())
			{
				myText *text = new myText;
				text->mRect = QRect(QPoint(m_plaintextedit->x(), m_plaintextedit->y()), QSize(m_plaintextedit->width(), m_plaintextedit->height()));
				text->mText = m_plaintextedit->toPlainText();
				mTexts.push_back(text);
				mTextsSize.push_back(m_TextSize);
				mTextsColor.push_back(m_PenColor);
				m_vecPaintsNum.push_back(Type_Text);
				update();
			}
			QFont font;
			font.setFamily("Microsoft YaHei");
			font.setPointSize(m_TextSize);
			m_plaintextedit->setFont(font);
			m_plaintextedit->setTextColor(m_PenColor);
			m_plaintextedit->move(startPoint);
			m_plaintextedit->show();
			m_plaintextedit->setFocus();
			m_plaintextedit->clear();
		}
		movePos_ = e->globalPos() - pos();
    }
}

void OEScreen::mouseReleaseEvent(QMouseEvent * e) 
{
    if (e->button() == Qt::LeftButton) 
	{
        isPressed_ = false;
		if (isdrawline)
		{
			m_vecLineGroup.push_back(m_iLineGroup);
			m_vecPaintsNum.push_back(Type_Line);
			m_bNeedReMasic = true;
		}
		if (isdrawrectangle)
		{
			endPoint = e->pos();
			mRectangles.last()->endPoint = endPoint;
			m_bNeedReMasic = true;
			update();
		}
		else if (isdrawround)
		{
			endPoint = e->pos();
			mRounds.last()->endPoint = endPoint;
			m_bNeedReMasic = true;
			update();
		}
		else if (isdrawarrow)
		{
			endPoint = e->pos();
			mArrows.last()->endPoint = endPoint;
			m_bNeedReMasic = true;
			update();
		}
		else if (ismasic)
		{
			m_vecMasicGroup.push_back(m_iMasicGroup);
			m_vecPaintsNum.push_back(Type_Masic);
			update();
		}
		else
		{
			if (direction_ != NONE)
			{
				setCursor(QCursor(Qt::SizeAllCursor));
			}
		}
    }
}

void OEScreen::mouseMoveEvent(QMouseEvent * e) 
{
    QPoint gloPoint = mapToParent(e->pos());
    QPoint pt_lu = mapToParent(rect().topLeft());
    QPoint pt_ll = mapToParent(rect().bottomLeft());
    QPoint pt_rl = mapToParent(rect().bottomRight());
    QPoint pt_ru = mapToParent(rect().topRight());
	if (isPressed_)
	{
		if (isdrawline)
		{
			setCursor(QCursor(Qt::CrossCursor));
			endPoint = e->pos();
			myLine *line = new myLine;
			line->startPoint = startPoint;
			line->endPoint = endPoint;
			mLines.push_back(line);
			mLinesColor.push_back(m_PenColor);
			mLinesL.push_back(m_PenLine);
			m_iLineGroup++;
			startPoint = endPoint;
			update();
		}
		else if (ismasic)
		{
			m_iMasicGroup++;
			QPixmap pixCur;
			if (m_PenLine == 1)
			{
				pixCur.load(":/screencut/Resources/screencut/cur12.png");
			}
			else if (m_PenLine == 4)
			{
				pixCur.load(":/screencut/Resources/screencut/cur24.png");
			}
			else if (m_PenLine == 9)
			{
				pixCur.load(":/screencut/Resources/screencut/cur48.png");
			}
			setCursor(pixCur);
			endPoint = e->pos();

			m_pMasic->resize(this->size());
			m_pMasic->move(this->pos());
			// 		QPainter painter1(&mWidget);
			// 		painter1.setPen(pen);
			// 		painter1.drawLine(line->startPoint, line->endPoint);
			m_pMasic->DrawRange(startPoint, endPoint);
			//mWidget.show();
			//QImage image = QPixmap::grabWidget(m_pMasic).toImage();
			//image.save("C:/Users/wangmanchun/Desktop/3.jpg", "JPG");
			//mWidget.close();
			startPoint = endPoint;
			update();
		}
		else if (isdrawrectangle)
		{
			setCursor(QCursor(Qt::CrossCursor));
			endPoint = e->pos();
			mRectangles.last()->endPoint = endPoint;
			update();
		}
		else if (isdrawround)
		{
			setCursor(QCursor(Qt::CrossCursor));
			endPoint = e->pos();
			mRounds.last()->endPoint = endPoint;
			update();
		}
		else if (isdrawarrow)
		{
			setCursor(QCursor(Qt::CrossCursor));
			endPoint = e->pos();
			mArrows.last()->endPoint = endPoint;
			update();
		}
		else
		{
			if (direction_ != NONE)
			{
				const int& global_x = gloPoint.x();
				/// 鼠标进行拖拉拽
				switch (direction_) {
				case LEFT:
					return onMouseChange(global_x, pt_ll.y() + 1);
				case RIGHT:
					return onMouseChange(global_x, pt_rl.y() + 1);
				case UPPER:
					return onMouseChange(pt_lu.x(), gloPoint.y());
				case LOWER:
					return onMouseChange(pt_rl.x() + 1, gloPoint.y());
				case LEFTUPPER:
				case RIGHTUPPER:
				case LEFTLOWER:
				case RIGHTLOWER:
					return onMouseChange(global_x, gloPoint.y());
				default:
					break;
				}
			}
			else
			{
				QRect deskRt = QApplication::desktop()->availableGeometry();
				double g_nActScreenW = deskRt.width();
				double g_nActScreenH = deskRt.height();
				QPoint ePoint = e->globalPos() - movePos_;
				if (ePoint.x() < 0 || ePoint.x() + rect().width() > g_nActScreenW||
					ePoint.y() < 0 || ePoint.y() + rect().height() > g_nActScreenH)
				{
					setCursor(QCursor(Qt::SizeAllCursor));
					//move(e->globalPos() - movePos_);
					movePos_ = e->globalPos() - pos();
				}
				else
				{
					setCursor(QCursor(Qt::SizeAllCursor));
					move(e->globalPos() - movePos_);
					movePos_ = e->globalPos() - pos();
				}
			}
		}
	}
	else
	{
		/// 检查鼠标鼠标方向
		direction_ = getRegion(gloPoint);

		/// 根据方位判断拖拉对应支点
		switch (direction_)
		{
		case NONE:
		case RIGHT:
		case RIGHTLOWER:
			originPoint_ = pt_lu;
			break;
		case RIGHTUPPER:
			originPoint_ = pt_ll;
			break;
		case LEFT:
		case LEFTLOWER:
			originPoint_ = pt_ru;
			break;
		case LEFTUPPER:
		case UPPER:
			originPoint_ = pt_rl;
			break;
		case LOWER:
			originPoint_ = pt_lu;
			break;
		}
	}
    currentRect_ = geometry();
}

void OEScreen::moveEvent(QMoveEvent *)
{
    emit postionChange(x(), y());
}

void OEScreen::resizeEvent(QResizeEvent *)
{
    listMarker_.clear();

    listMarker_.push_back(QPoint(0, 0));
    listMarker_.push_back(QPoint(width(), 0));
    listMarker_.push_back(QPoint(0, height()));
    listMarker_.push_back(QPoint(width(), height()));

    listMarker_.push_back(QPoint((width() >> 1), 0));
    listMarker_.push_back(QPoint((width() >> 1), height()));
    listMarker_.push_back(QPoint(0, (height() >> 1)));
    listMarker_.push_back(QPoint(width(), (height() >> 1)));

    emit sizeChange(width(), height());
}

void OEScreen::showEvent(QShowEvent *) 
{
    isInit_ = true;
}

void OEScreen::hideEvent(QHideEvent *)
{
    currentRect_ = {};
    movePos_ = {};
    originPoint_ = {};
    isInit_ = false;
}

void OEScreen::enterEvent(QEvent *e)
{
   // setCursor(Qt::SizeAllCursor);
    QWidget::enterEvent(e);
}

void OEScreen::leaveEvent(QEvent *e)
{
    //setCursor(Qt::ArrowCursor);
    QWidget::leaveEvent(e);
}

void OEScreen::closeEvent(QCloseEvent *)
{
    isInit_ = false;
}

void OEScreen::paintEvent(QPaintEvent *) 
{
    QPainter painter(this);

	//绘制捕捉区域边框
	QPen pen(QColor(0, 174, 255), 2);
    painter.setPen(pen);
    painter.drawRect(rect());

//     pen.setWidth(m_PenLine);
//     pen.setColor(m_PenColor);
//     painter.setPen(pen);
   // painter.drawPoints(listMarker_);
	painter.drawPixmap(QPoint(0, 0), *originPainting_, currentRect_);
	if (m_vecMasicGroup.size() || m_iMasicGroup>0)
	{
		QImage image = QPixmap::grabWidget(m_pMasic).toImage();
		QPoint startPos = m_pMasic->pos();
		int iw = image.width();
		int ih = image.height();
		QImage imgDraw = m_originImg;
		for (int i = 0; i < iw; i++)
		{
			for (int j = 0; j < ih; j++)
			{
				int iTmp[3];
				QColor rgb = image.pixel(i, j);
				rgb.getRgb(&iTmp[0], &iTmp[1], &iTmp[2]);
				if (iTmp[0] == 0 && iTmp[1] == 174 && iTmp[2] == 255)
				{
					QColor rgbM = m_masicImg.pixel(i+ startPos.x(), j+ startPos.y());
					imgDraw.setPixelColor(QPoint(i + startPos.x(), j + startPos.y()), rgbM);
				}
			}
		}
		//*originPainting_ = originPainting_->fromImage(imgO);
		QPixmap tmpPix;
		tmpPix = tmpPix.fromImage(imgDraw);
		QRect marect(0, 0, iw, ih);
		painter.drawPixmap(QPoint(0, 0), tmpPix, currentRect_);
	}

	for (int i = 0; i < mLines.size(); i++)
	{
		pen.setWidth(mLinesL[i]);
		pen.setColor(mLinesColor[i]);
		painter.setPen(pen);
		myLine *line = mLines[i];
		painter.drawLine(line->startPoint, line->endPoint);
	}
	for (int j = 0; j < mRectangles.size(); j++)
	{
		pen.setWidth(mRectanglesL[j]);
		pen.setColor(mRectanglesColor[j]);
		painter.setPen(pen);
		myRectangle *rectangle = mRectangles[j];
		int x1 = rectangle->startPoint.x() < rectangle->endPoint.x() ? rectangle->startPoint.x() : rectangle->endPoint.x();
		int y1 = rectangle->startPoint.y() < rectangle->endPoint.y() ? rectangle->startPoint.y() : rectangle->endPoint.y();
		painter.drawRect(x1, y1, abs(rectangle->endPoint.x() - rectangle->startPoint.x()), abs(rectangle->endPoint.y() - rectangle->startPoint.y()));

	}
	for (int k = 0; k < mRounds.size(); k++)
	{
		pen.setWidth(mRoundsL[k]);
		pen.setColor(mRoundsColor[k]);
		painter.setPen(pen);
		myRound *round = mRounds[k];
		int x2 = round->startPoint.x() < round->endPoint.x() ? round->startPoint.x() : round->endPoint.x();
		int y2 = round->startPoint.y() < round->endPoint.y() ? round->startPoint.y() : round->endPoint.y();
		painter.drawEllipse(x2, y2, abs(round->endPoint.x() - round->startPoint.x()), abs(round->endPoint.y() - round->startPoint.y()));
	}
	for (int m = 0; m < mArrows.size(); m++)
	{
		pen.setWidth(mArrowsL[m]);
		pen.setColor(mArrowsColor[m]);
		pen.setJoinStyle(Qt::PenJoinStyle::MiterJoin);
		painter.setPen(pen);
		myArrow *arrow = mArrows[m];
		drawarrow(arrow->startPoint, arrow->endPoint, painter);
	}
	for (int l = 0; l < mTexts.size(); l++)
	{
		myText *text = mTexts[l];
		QRect rect = text->mRect;
		QFont font;
		font.setFamily("Microsoft YaHei");
		font.setPointSize(mTextsSize[l]);
		pen.setColor(mTextsColor[l]);
		painter.setPen(pen);
		painter.setFont(font);
		painter.drawText(rect, Qt::TextWrapAnywhere, text->mText);
	}
}

void OEScreen::drawarrow(QPoint startpoint, QPoint endpoint, QPainter &p)
{
	p.setRenderHint(QPainter::Antialiasing, true);
	QPen drawtrianglepen = p.pen();
	p.setBrush(drawtrianglepen.color());
	double par = drawtrianglepen.width()*8;
	drawtrianglepen.setWidth(1);
	p.setPen(drawtrianglepen);
	double slopy = atan2((endpoint.y() - startpoint.y()), (endpoint.x() - startpoint.x()));
	double cosy = cos(slopy);
	double siny = sin(slopy);
	QPoint point1 = QPoint(endpoint.x() + int(-par*cosy - (par / 2.0*siny)), endpoint.y() + int(-par*siny + (par / 2.0*cosy)));
	QPoint point2 = QPoint(endpoint.x() + int(-par*cosy + (par / 2.0*siny)), endpoint.y() - int(par / 2.0*cosy + par*siny));
	QPoint points[3] = { endpoint, point1, point2 };

	p.drawPolygon(points, 3);
	int offsetx = int(par*siny / 3);
	int offsety = int(par*cosy / 3);
	QPoint point3, point4;
	point3 = QPoint(endpoint.x() + int(-par*cosy - (par / 2.0*siny)) + offsetx, endpoint.y() + int(-par*siny + (par / 2.0*cosy)) - offsety);
	point4 = QPoint(endpoint.x() + int(-par*cosy + (par / 2.0*siny) - offsetx), endpoint.y() - int(par / 2.0*cosy + par*siny) + offsety);
	QPoint arrbodypoints[3] = { startpoint, point3, point4 };
	p.drawPolygon(arrbodypoints, 3);
	p.setRenderHint(QPainter::Antialiasing, true);
}

const QString OEScreen::getFileName(void)
{
    QString file_name = "";
    return file_name;
}

void OEScreen::onSaveScreenOther(void) 
{
	setTextEditToVector();
	if (m_plaintextedit)
	{
		m_plaintextedit->hide();
	}

	QImage image = QPixmap::grabWidget(this).toImage();
	/*
	QString strGuid = QUuid::createUuid().toString();
	strGuid.replace("{", "");
	strGuid.replace("}", "");
	strGuid.replace("-", "");

	QString strPath = QDir::currentPath() + "/ScreenShot/" + strGuid + ".jpg";
	QDir fileDir = QFileInfo(strPath).absoluteDir();
	QString strFileDir = QFileInfo(strPath).absolutePath();
	if (!fileDir.exists()) 
	{
		fileDir.mkpath(strFileDir);
	}
	
	image.save(strPath, "jpg");
	*/

	QClipboard *board = QApplication::clipboard();
	board->setImage(image);

	emit sigSaveScreenPicture();
	quitScreenshot();
}

void OEScreen::onSaveScreen(void)
{
	onSaveScreenOther();
}

void OEScreen::quitScreenshot(void)
{
    close();
    parentWidget()->close();
}

void OEScreen::onMouseChange(int x, int y)
{
    show();
    if (x < 0 || y < 0)
	{
        return;
    }
    const int& rx = (x >= originPoint_.x()) ? originPoint_.x() : x;
    const int& ry = (y >= originPoint_.y()) ? originPoint_.y() : y;
    const int& rw = abs(x - originPoint_.x());
    const int& rh = abs(y - originPoint_.y());

    currentRect_ = QRect(rx, ry, rw, rh);

    this->setGeometry(currentRect_);

    parentWidget()->update();
}

void OEScreen::setDrawRectangState(bool bState)
{
	setTextEditToVector();
	isdrawline = false;
	isdrawrectangle = bState;
	isdrawround = false;
	istextedit = false;
	isdrawarrow = false;
	ismasic = false;
	if (m_plaintextedit)
	{
		m_plaintextedit->hide();
	}
}

void OEScreen::setDrawLineState(bool bState)
{
	setTextEditToVector();
	isdrawline = bState;
	isdrawrectangle = false;
	isdrawround = false;
	istextedit = false;
	isdrawarrow = false;
	ismasic = false;
	if (m_plaintextedit)
	{
		m_plaintextedit->hide();
	}
}

void OEScreen::setDrawMasicState(bool bState)
{
	//setTextEditToVector();
	ismasic = bState;
	isdrawline = false;
	isdrawrectangle = false;
	isdrawround = false;
	istextedit = false;
	isdrawarrow = false;
	if (m_plaintextedit)
	{
		m_plaintextedit->hide();
	}

	if (bState)
	{
// 		QImage imageOr = QPixmap::grabWidget(this).toImage();
// 		m_originImg = imageOr;
 		generalMasicImg();
// 		m_bNeedReMasic = false;
	}

}

void OEScreen::generalMasicImg()
{
	int iw = m_originImg.width();
	int ih = m_originImg.height();
	m_masicImg = m_originImg;
	//QPixmap 
// 	QPixmap masicPix(iw, ih);
// 	QImage img = originPainting_->toImage();
	int iK = m_MasicSize;
	int i = 0;
	int j = 0;
	while (i < iw - 1)
	{
		int ii = i + iK>iw ? iw : i + iK;
		j = 0;
		while (j < ih - 1)
		{
			int jj = j + iK>ih ? ih : j + iK;
			double dRGB[3] = { 0,0,0 };
			for (int l = i; l < ii; l++)
			{
				for (int ll = j; ll < jj; ll++)
				{
					int iTmpRGB[3] = { 0,0,0 };
					QColor rgb = m_originImg.pixel(l, ll);
					rgb.getRgb(&iTmpRGB[0], &iTmpRGB[1], &iTmpRGB[2]);
					for (int q = 0; q < 3; q++)
					{
						double dV = iTmpRGB[q];
						if (dV > 20)
						{
							int a = 1;
							a++;
						}
						dRGB[q] += dV / (ii - i) / (jj - j);
					}
				}
			}
			for (int l = i; l < ii; l++)
			{
				for (int ll = j; ll < jj; ll++)
				{
					QColor ecolor(dRGB[0], dRGB[1], dRGB[2], 255);
					m_masicImg.setPixelColor(QPoint(l, ll), ecolor);
				}
			}
			j = jj;
		}
		i = ii;
	}
}

void OEScreen::setDrawArrowState(bool bState)
{
	setTextEditToVector();
	isdrawline = false;
	isdrawrectangle = false;
	isdrawround = false;
	istextedit = false;
	ismasic = false;
	isdrawarrow = bState;
	if (m_plaintextedit)
	{
		m_plaintextedit->hide();
	}
}

void OEScreen::setDrawRoundState(bool bState)
{
	setTextEditToVector();
	isdrawline = false;
	isdrawrectangle = false;
	isdrawround = bState;
	istextedit = false;
	ismasic = false;
	isdrawarrow = false;
	if (m_plaintextedit)
	{
		m_plaintextedit->hide();
	}
}

void OEScreen::setDrawTextState(bool bState)
{
	setTextEditToVector();
	isdrawline = false;
	isdrawrectangle = false;
	isdrawround = false;
	ismasic = false;
	istextedit = bState;
	isdrawarrow = false;
	if (m_plaintextedit)
	{
		m_plaintextedit->hide();
	}
}

void OEScreen::slotTextChanged()
{
	QFont wordfont;
	wordfont.setFamily("Microsoft YaHei");
	wordfont.setPointSize(m_TextSize);
	QFontMetrics fm(wordfont);
	QLabel *label1 = new QLabel();
	QString str = m_plaintextedit->toPlainText();
	if (str.isEmpty())
	{
		str = "1";
	}
	int len = 0;
	QStringList lst;
	lst = str.split("\n");//lst={"one","two","three","four","five","six"}
	for (int i = 0; i < lst.size(); i++)
	{
		label1->setText(lst[i]);
		QRect rec = fm.boundingRect(label1->text());
		if (len < rec.width())
		{
			len = rec.width();
		}
	}
	m_plaintextedit->resize(len + 30, m_plaintextedit->document()->size().rheight() + 10);
}

void OEScreen::setTextEditToVector()
{
	if (istextedit&&m_plaintextedit->toPlainText().size())
	{
		myText *text = new myText;
		text->mRect = QRect(QPoint(m_plaintextedit->x(), m_plaintextedit->y()), QSize(m_plaintextedit->width(), m_plaintextedit->height()));
		text->mText = m_plaintextedit->toPlainText();
		mTexts.push_back(text);
		mTextsSize.push_back(m_TextSize);
		mTextsColor.push_back(m_PenColor);
		update();
	}
}

void OEScreenshot::setPenColor(QColor eColor)
{
	screenTool_->m_PenColor = eColor;
	screenTool_->changeTextEdit();
}

void OEScreenshot::setPenLine(int iLine)
{
	screenTool_->m_PenLine = iLine;
	screenTool_->m_pMasic->setMasicSize(iLine * 5);
}

void OEScreenshot::setTextSize(QString strIndex)
{
	screenTool_->m_TextSize = strIndex.toInt();
	screenTool_->changeTextEdit();
}

void OEScreenshot::setMasicSize(int iSize)
{
	screenTool_->m_MasicSize = iSize+5;
	screenTool_->changeMasicSize();
}

void OEScreen::onSaveAsScreen()
{
	setTextEditToVector();
	if (m_plaintextedit)
	{
		m_plaintextedit->hide();
	}
	QImage image = QPixmap::grabWidget(this).toImage();
	QString strGuid = QUuid::createUuid().toString();
	strGuid.replace("{", "");
	strGuid.replace("}", "");
	strGuid.replace("-", "");
	QString strType = ".jpg";

	setWindowIcon(QIcon(":/screencut/Resources/screencut/white.png"));
	QString filename = QFileDialog::getSaveFileName(this,
		tr("save Picture"),
		strGuid + strType,
		strType);
	if (filename.isEmpty())
	{
		return;
	}
	if (QFileInfo(filename).suffix().toLower()!="jpg")  //若后缀为空自动添加png后缀  
	{
		filename.append(".jpg");
	}
	QDir fileDir = QFileInfo(filename).absoluteDir();
	QString strFileDir = QFileInfo(filename).absolutePath();
	if (!fileDir.exists())
	{
	fileDir.mkpath(strFileDir);
	}
	image.save(filename, "jpg");
	quitScreenshot();
}

void OEScreen::RevertPaints()
{
	if (m_vecPaintsNum.size()==0)
	{
		return;
	}
	int iType = m_vecPaintsNum.back();
	m_vecPaintsNum.pop_back();
	switch (iType)
	{
	case Type_Rect:
		mRectangles.pop_back();
		mRectanglesColor.pop_back();
		mRectanglesL.pop_back();
		break;
	case Type_Round:
		mRounds.pop_back();
		mRoundsColor.pop_back();
		mRoundsL.pop_back();
		break;
	case Type_Arrow:
		mArrows.pop_back();
		mArrowsColor.pop_back();
		mArrowsL.pop_back();
		break;
	case Type_Line:
		for (int i = 0; i < m_vecLineGroup.back(); i++)
		{
			mLines.pop_back();
			mLinesColor.pop_back();
			mLinesL.pop_back();
		}
		m_vecLineGroup.pop_back();
		break;
	case Type_Text:
		mTexts.pop_back();
		mTextsSize.pop_back();
		mTextsColor.pop_back();
		break;
	case Type_Masic:
		m_pMasic->revertMasic(m_vecMasicGroup.back());
		m_vecMasicGroup.pop_back();
	default:
		break;
	}
	update();
}

extern "C" void OEScreenshot(HWND hWnd)
{
	OEScreenshot::Instance(hWnd,m_thisParm);
}

void OEScreen::changeTextEdit()
{
	QFont font;
	font.setFamily("Microsoft YaHei");
	font.setPointSize(m_TextSize);
	m_plaintextedit->setFont(font);
	m_plaintextedit->setTextColor(m_PenColor);
	QString str = m_plaintextedit->toPlainText();
	m_plaintextedit->clear();
	m_plaintextedit->setText(str);
}

void OEScreen::changeMasicSize()
{
	//m_pMasic->setMasicSize(m_MasicSize +5);
	generalMasicImg();
	update();
}
