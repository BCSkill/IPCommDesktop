<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>OEScreen</name>
    <message>
        <location filename="oescreenshot.cpp" line="1584"/>
        <source>save Picture</source>
        <translation>保存图片</translation>
    </message>
</context>
<context>
    <name>ScreenControlWidget</name>
    <message>
        <location filename="screencontrolwidget.ui" line="35"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="399"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="80"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="401"/>
        <source>Rectangle</source>
        <oldsource>Draw rectangle</oldsource>
        <translation>画矩形</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="147"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="405"/>
        <source>Circle</source>
        <oldsource>Draw a circle</oldsource>
        <translation>画圆</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="213"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="409"/>
        <source>Arrow</source>
        <oldsource>Draw arrow</oldsource>
        <translation>画箭头</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="279"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="413"/>
        <source>Brush</source>
        <oldsource>brush</oldsource>
        <translation>画笔</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="336"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="417"/>
        <source>Mosaic</source>
        <translation>马赛克</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="391"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="421"/>
        <source>Text</source>
        <oldsource>Add text</oldsource>
        <translation>添加文字</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="496"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="425"/>
        <source>Revert</source>
        <translation>撤销</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">撤销</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="546"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="429"/>
        <source>Save</source>
        <oldsource>save</oldsource>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="636"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="433"/>
        <source>Exit</source>
        <translation>退出截图</translation>
    </message>
    <message>
        <source>Exit screenshot</source>
        <translation type="vanished">退出截图</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="696"/>
        <location filename="GeneratedFiles/ui_screencontrolwidget.h" line="437"/>
        <source>Complete</source>
        <oldsource>Complete screenshot</oldsource>
        <translation>完成截图</translation>
    </message>
</context>
<context>
    <name>ScreenPenSettingWidget</name>
    <message>
        <location filename="screenpensettingwidget.ui" line="17"/>
        <source>ScreenPenSettingWidget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="screenpensettingwidget.ui" line="900"/>
        <source>Degree</source>
        <translation>模糊度</translation>
    </message>
    <message>
        <source>Ambiguity</source>
        <translation type="vanished">模糊度</translation>
    </message>
</context>
</TS>
