﻿
#include "oecommonhelper.h"

#include <QFile>
#include <QTranslator>
#include <QApplication>
#include <QWidget>
#include <QDesktopWidget>
#include <QWindow>
#ifdef Q_OS_WIN
#include <windows.h>
#endif
#ifndef QT_NO_DEBUG
#include <QDebug>
#endif


#define WINDOW_BASESIZE_WIDTH (1920)
#define WINDOW_BASESIZE_HEIGHT (1080)

float OECommonHelper::widthMultiplyingPower_ = 0;
float OECommonHelper::heightMultiplyingPower_ = 0;

void OECommonHelper::setStyle(const QString &style) 
{
    QFile qss(style);
    qss.open(QFile::ReadOnly);
    qApp->setStyleSheet(qss.readAll());
    qss.close();
}

void OECommonHelper::setLanguagePack(const QString &language) 
{
    // 加载中文包
    QTranslator translator;
    translator.load(language);
    qApp->installTranslator(&translator);
}

void OECommonHelper::moveCenter(QWidget *widget, QRect parentRect) 
{
    if (parentRect.isEmpty()) 
	{
        parentRect = QApplication::desktop()->rect();
    }
    widget->move (((parentRect.width() - widget->width()) >> 1),((parentRect.height() - widget->height()) >> 1));
}

const float &OECommonHelper::getWindowWidthMultiplyingPower() 
{
    if (widthMultiplyingPower_ == 0) 
	{
        upWindowSizeMultiplyingPower();
    }
    return widthMultiplyingPower_;
}

const float & OECommonHelper::getWindowHeightMultiplyingPower() 
{
    if (heightMultiplyingPower_ == 0) 
	{
        upWindowSizeMultiplyingPower();
    }
    return heightMultiplyingPower_;
}

void OECommonHelper::upWindowSizeMultiplyingPower() 
{
   QSize temp_size = QApplication::desktop()->size();
   widthMultiplyingPower_ = (float)temp_size.width() / (float)WINDOW_BASESIZE_WIDTH;
   heightMultiplyingPower_ = (float)temp_size.height() / (float)WINDOW_BASESIZE_HEIGHT;
}

bool OECommonHelper::getSmallestWindowFromCursor(QRect& out_rect) 
{
	HWND hd = GetDesktopWindow();        //得到桌面窗口
	hd = GetWindow(hd, GW_CHILD);        //得到屏幕上第一个子窗口
	QVector<HWND> vecH;
	POINT pt;
	::GetCursorPos(&pt);
	HWND myhd = ::ChildWindowFromPointEx(::GetDesktopWindow(), pt, CWP_ALL);
	while (hd != NULL)                    //循环得到所有的子窗口
	{
		if (::IsWindow(hd) && ::IsWindowVisible(hd))//&& myhd != hd)
		{
			vecH.push_back(hd);
		}
		hd = GetNextWindow(hd, GW_HWNDNEXT);
	}
	HWND hdIn = ::GetDesktopWindow();
	for (int i = vecH.size() - 1; i > 0; i--)
	{
		RECT temp_window;
		HWND Tmphd = vecH[i];
		::GetWindowRect(Tmphd, &temp_window);
		if (pt.x<temp_window.right&&
			pt.x>temp_window.left&&
			pt.y<temp_window.bottom&&
			pt.y>temp_window.top)//判断鼠标在rect内
		{
			hdIn = Tmphd;
		}
	}
	RECT temp_window;
	::GetWindowRect(hdIn, &temp_window);
	out_rect.setRect(temp_window.left, temp_window.top, temp_window.right - temp_window.left, temp_window.bottom - temp_window.top);
	if (out_rect.x() < 0)
	{
		int tmp = out_rect.x();
		out_rect.setX(0);
		out_rect.setWidth(out_rect.width());// +tmp);
	}
	if (out_rect.y() < 0)
	{
		int tmp = out_rect.y();
		out_rect.setY(0);
		out_rect.setHeight(out_rect.height());// +tmp);
	}
	QRect deskRt = QApplication::desktop()->screenGeometry();
	if (out_rect.width() + out_rect.x() > deskRt.width())
	{
		out_rect.setWidth(deskRt.width()- out_rect.x());
	}
	if (out_rect.height() + out_rect.y() > deskRt.height())
	{
		out_rect.setHeight(deskRt.height() - out_rect.y());
	}
	return true;
	

// #ifdef Q_OS_WIN
// 	HWND hwnd;
//     POINT pt;
//     // 获得当前鼠标位置
//     ::GetCursorPos(&pt);
//     // 获得当前位置桌面上的子窗口
// 	hwnd = ::ChildWindowFromPointEx(::GetDesktopWindow(), pt, CWP_SKIPINVISIBLE | CWP_SKIPTRANSPARENT);// CWP_SKIPDISABLED | CWP_SKIPINVISIBLE);
//     if (hwnd != NULL)
// 	{
//         HWND temp_hwnd;
//         temp_hwnd = hwnd;
//         while (true)
// 		{
//             ::GetCursorPos(&pt);
//             ::ScreenToClient(temp_hwnd, &pt);
//             temp_hwnd = ::ChildWindowFromPointEx(temp_hwnd, pt, CWP_SKIPINVISIBLE);
//             if (temp_hwnd == NULL || temp_hwnd == hwnd)
//                 break;
//             hwnd = temp_hwnd;
//         }
// 		int i = (int)hwnd;
// 		qDebug() << i;
//         RECT temp_window;
//         ::GetWindowRect(hwnd, &temp_window);
//         out_rect.setRect(temp_window.left,temp_window.top,temp_window.right - temp_window.left, temp_window.bottom - temp_window.top);
//         return true;
//     }
// #endif
//     return false;
}

bool OECommonHelper::getCurrentWindowFromCursor(QRect &out_rect)
{
#ifdef Q_OS_WIN
    HWND hwnd;
    POINT pt;
    // 获得当前鼠标位置
    ::GetCursorPos(&pt);
    // 获得当前位置桌面上的子窗口
    hwnd = ::ChildWindowFromPointEx(::GetDesktopWindow(), pt,CWP_SKIPDISABLED | CWP_SKIPINVISIBLE);
    if (hwnd != NULL) 
	{
        RECT temp_window;
        ::GetWindowRect(hwnd, &temp_window);
        out_rect.setRect(temp_window.left, temp_window.top,temp_window.right - temp_window.left, temp_window.bottom - temp_window.top);
        return true;
    }
#endif
    return false;
}
