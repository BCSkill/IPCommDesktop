<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="th_TH">
<context>
    <name>OEScreen</name>
    <message>
        <location filename="oescreenshot.cpp" line="1584"/>
        <source>save Picture</source>
        <translation>บันทึกภาพ</translation>
    </message>
</context>
<context>
    <name>ScreenControlWidget</name>
    <message>
        <location filename="screencontrolwidget.ui" line="35"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="80"/>
        <source>Rectangle</source>
        <translation>สี่เหลี่ยมผืนผ้า</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="147"/>
        <source>Circle</source>
        <translation>วงกลม</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="213"/>
        <source>Arrow</source>
        <translation>ลูกศร</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="279"/>
        <source>Brush</source>
        <translation>แปรง</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="336"/>
        <source>Mosaic</source>
        <translation>กระเบื้องโมเสค</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="391"/>
        <source>Text</source>
        <translation>ข้อความ</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="496"/>
        <source>Revert</source>
        <translation>คืนกลับ</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="546"/>
        <source>Save</source>
        <translation>บันทึก</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="636"/>
        <source>Exit</source>
        <translation>ทางออก</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="696"/>
        <source>Complete</source>
        <translation>สมบูรณ์</translation>
    </message>
</context>
<context>
    <name>ScreenPenSettingWidget</name>
    <message>
        <location filename="screenpensettingwidget.ui" line="17"/>
        <source>ScreenPenSettingWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="screenpensettingwidget.ui" line="900"/>
        <source>Degree</source>
        <translation>ความคลุมเครือ</translation>
    </message>
</context>
</TS>
