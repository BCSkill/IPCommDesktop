<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>NewMsgNotifyShareLib</name>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="66"/>
        <location filename="NewMsgNotifyShareLib.cpp" line="151"/>
        <source>image://NotifyImgProvider/</source>
        <translation></translation>
    </message>
    <message>
        <source>Tribe Apply</source>
        <translation type="vanished">Племя Применить</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="97"/>
        <source>Group Apply</source>
        <translation>Группа Применить</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="98"/>
        <source>Buddy Apply</source>
        <translation>Приятель Применить</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="140"/>
        <location filename="NewMsgNotifyShareLib.cpp" line="301"/>
        <source>&lt;font color=&apos;#f7931e&apos; &gt;[I was @]&lt;/font&gt;</source>
        <translation>&lt;font color=&apos;#f7931e&apos; &gt;[Я был @]&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="210"/>
        <source>The other party has successfully received your file </source>
        <translation>Другой участник успешно получил ваш файл </translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="236"/>
        <source>[Announcement]</source>
        <translation>[Объявление]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="244"/>
        <source>[Share]</source>
        <translation>[Поделиться]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="263"/>
        <source>[Image]</source>
        <translation>[Образ]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="265"/>
        <source>[Audio]</source>
        <translation>[аудио]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="267"/>
        <source>[Video]</source>
        <translation>[видео]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="269"/>
        <source>[File]</source>
        <translation>[файл]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="271"/>
        <source>This type of message is not supported for now</source>
        <translation>Этот тип сообщения пока не поддерживается</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="273"/>
        <source>[Tranfer]</source>
        <translation>[Перечислить]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="275"/>
        <source>[Red Packet]</source>
        <translation>[Красный пакет]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="277"/>
        <source>[Secret Message]</source>
        <translation>[Секретное сообщение]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="279"/>
        <source>[Secret Image]</source>
        <translation>[Секретное изображение]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="281"/>
        <source>[Secret File]</source>
        <translation>[Секретный файл]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="283"/>
        <source>[Location]</source>
        <translation>[Место нахождения]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="285"/>
        <source>[Gravitational Waves]</source>
        <translation>[Гравитационные волны]</translation>
    </message>
</context>
<context>
    <name>NewNotifyGui</name>
    <message>
        <location filename="NewNotifyGui.ui" line="32"/>
        <location filename="GeneratedFiles/ui_NewNotifyGui.h" line="91"/>
        <source>NewNotifyGui</source>
        <translation></translation>
    </message>
    <message>
        <location filename="NewNotifyGui.ui" line="124"/>
        <location filename="GeneratedFiles/ui_NewNotifyGui.h" line="92"/>
        <location filename="NewNotifyGui.cpp" line="90"/>
        <source>New Message</source>
        <translation>Новое сообщение</translation>
    </message>
    <message>
        <location filename="NewNotifyGui.ui" line="150"/>
        <location filename="GeneratedFiles/ui_NewNotifyGui.h" line="93"/>
        <source>Ignore</source>
        <translation>игнорировать</translation>
    </message>
</context>
</TS>
