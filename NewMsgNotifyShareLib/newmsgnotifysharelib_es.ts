<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>NewMsgNotifyShareLib</name>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="66"/>
        <location filename="NewMsgNotifyShareLib.cpp" line="151"/>
        <source>image://NotifyImgProvider/</source>
        <translation></translation>
    </message>
    <message>
        <source>Tribe Apply</source>
        <translation type="vanished">Tribe Apply</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="97"/>
        <source>Group Apply</source>
        <translation>Grupo aplicar</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="98"/>
        <source>Buddy Apply</source>
        <translation>Buddy Apply</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="140"/>
        <location filename="NewMsgNotifyShareLib.cpp" line="301"/>
        <source>&lt;font color=&apos;#f7931e&apos; &gt;[I was @]&lt;/font&gt;</source>
        <translation>&lt;font color=&apos;#f7931e&apos; &gt;[Yo era @]&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="210"/>
        <source>The other party has successfully received your file </source>
        <translation>La otra parte ha recibido con éxito su archivo </translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="236"/>
        <source>[Announcement]</source>
        <translation>[Anuncio]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="244"/>
        <source>[Share]</source>
        <translation>[Compartir]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="263"/>
        <source>[Image]</source>
        <translation>[Imagen]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="265"/>
        <source>[Audio]</source>
        <translation>[Audio]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="267"/>
        <source>[Video]</source>
        <translation>[Vídeo]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="269"/>
        <source>[File]</source>
        <translation>[Expediente]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="271"/>
        <source>This type of message is not supported for now</source>
        <translation>Este tipo de mensaje no es compatible por ahora</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="273"/>
        <source>[Tranfer]</source>
        <translation>[Transferir]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="275"/>
        <source>[Red Packet]</source>
        <translation>[Paquete rojo]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="277"/>
        <source>[Secret Message]</source>
        <translation>[Mensaje secreto]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="279"/>
        <source>[Secret Image]</source>
        <translation>[Imagen secreta]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="281"/>
        <source>[Secret File]</source>
        <translation>[Archivo Secreto]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="283"/>
        <source>[Location]</source>
        <translation>[Ubicación]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="285"/>
        <source>[Gravitational Waves]</source>
        <translation>[Ondas gravitacionales]</translation>
    </message>
</context>
<context>
    <name>NewNotifyGui</name>
    <message>
        <location filename="NewNotifyGui.ui" line="32"/>
        <location filename="GeneratedFiles/ui_NewNotifyGui.h" line="91"/>
        <source>NewNotifyGui</source>
        <translation></translation>
    </message>
    <message>
        <location filename="NewNotifyGui.ui" line="124"/>
        <location filename="GeneratedFiles/ui_NewNotifyGui.h" line="92"/>
        <location filename="NewNotifyGui.cpp" line="90"/>
        <source>New Message</source>
        <translation>Nuevo mensaje</translation>
    </message>
    <message>
        <location filename="NewNotifyGui.ui" line="150"/>
        <location filename="GeneratedFiles/ui_NewNotifyGui.h" line="93"/>
        <source>Ignore</source>
        <translation>Ignorar</translation>
    </message>
</context>
</TS>
