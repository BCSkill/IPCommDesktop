<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>NewMsgNotifyShareLib</name>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="66"/>
        <location filename="NewMsgNotifyShareLib.cpp" line="151"/>
        <source>image://NotifyImgProvider/</source>
        <translation></translation>
    </message>
    <message>
        <source>Tribe Apply</source>
        <translation type="vanished">部落申请</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="97"/>
        <source>Group Apply</source>
        <translation>群组申请</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="98"/>
        <source>Buddy Apply</source>
        <translation>好友申请</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="210"/>
        <source>The other party has successfully received your file </source>
        <translation>对方已成功接收您的文件 </translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="236"/>
        <source>[Announcement]</source>
        <translation>[通告]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="244"/>
        <source>[Share]</source>
        <translation>[分享]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="263"/>
        <source>[Image]</source>
        <translation>[图片]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="265"/>
        <source>[Audio]</source>
        <translation>[音频]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="267"/>
        <source>[Video]</source>
        <translation>[视频]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="269"/>
        <source>[File]</source>
        <translation>[文件]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="271"/>
        <source>This type of message is not supported for now</source>
        <translation>该消息类型暂不支持</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="273"/>
        <source>[Tranfer]</source>
        <translation>[转账]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="275"/>
        <source>[Red Packet]</source>
        <translation>[红包]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="277"/>
        <source>[Secret Message]</source>
        <translation>[密信]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="279"/>
        <source>[Secret Image]</source>
        <translation>[密图]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="281"/>
        <source>[Secret File]</source>
        <translation>[密件]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="283"/>
        <source>[Location]</source>
        <translation>[位置]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="285"/>
        <source>[Gravitational Waves]</source>
        <translation>[引力波]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="140"/>
        <location filename="NewMsgNotifyShareLib.cpp" line="301"/>
        <source>&lt;font color=&apos;#f7931e&apos; &gt;[I was @]&lt;/font&gt;</source>
        <translation>&lt;font color=&apos;#f7931e&apos; &gt;[有人@我]&lt;/font&gt;</translation>
    </message>
</context>
<context>
    <name>NewNotifyGui</name>
    <message>
        <location filename="NewNotifyGui.ui" line="32"/>
        <location filename="GeneratedFiles/ui_NewNotifyGui.h" line="91"/>
        <source>NewNotifyGui</source>
        <translation></translation>
    </message>
    <message>
        <location filename="NewNotifyGui.ui" line="124"/>
        <location filename="GeneratedFiles/ui_NewNotifyGui.h" line="92"/>
        <location filename="NewNotifyGui.cpp" line="90"/>
        <source>New Message</source>
        <translation>新消息</translation>
    </message>
    <message>
        <location filename="NewNotifyGui.ui" line="150"/>
        <location filename="GeneratedFiles/ui_NewNotifyGui.h" line="93"/>
        <source>Ignore</source>
        <translation>忽略全部</translation>
    </message>
</context>
</TS>
