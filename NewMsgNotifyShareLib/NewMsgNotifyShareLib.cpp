﻿
#include "NewMsgNotifyShareLib.h"
#include <qdebug.h>
#include <QtQml>
#include <QQuickItem>
#include <QTextCodec>

extern QString gThemeStyle;

NewMsgNotifyShareLib::NewMsgNotifyShareLib(QWidget *parent)
	: QQuickWidget(parent), m_msgNotifyModel(new MessageNotifyModel())
{

	QTextCodec::setCodecForLocale(QTextCodec::codecForName("gbk"));

	this->setResizeMode(QQuickWidget::SizeRootObjectToView);
	NotifyImgProvider *notifyImgProvider = new NotifyImgProvider(QQmlImageProviderBase::Image);
	this->engine()->addImageProvider(QLatin1String("NotifyImgProvider"), notifyImgProvider);

	this->rootContext()->setContextProperty("messageNotify", this);
	this->rootContext()->setContextProperty("messageNotifyModel", m_msgNotifyModel);
	this->setSource(QUrl("qrc:/MessageNotify/Resources/MessageNotify/MessageNotify.qml"));


	//切换主题样式
	if (gThemeStyle == "White")
	{
		this->changeStyle("dayStyle");
	}
	else if (gThemeStyle == "Blue")
	{
		this->changeStyle("nightStyle");
	}

	QString strColor = this->rootObject()->property("clearColor").toString();
	this->setClearColor(QColor(strColor));

	connect(this, SIGNAL(sigDoClickItem(QString)), SLOT(slotDoClickItem(QString)));
}
NewMsgNotifyShareLib::~NewMsgNotifyShareLib()
{
	this->m_msgNotifyModel->deleteLater();
}

void NewMsgNotifyShareLib::changeStyle(QString styleName)
{
	QObject* rootItem = rootObject();
	if (rootItem)
	{
		QVariant param = styleName;
		QMetaObject::invokeMethod(rootItem, "changeStyle", Q_ARG(QVariant, param));
	}
}

void NewMsgNotifyShareLib::OnInsertMessage(
	int msgCount,
	QString strUserID,
	QString strPicPath,
	QString strNickName,
	QString lastMsg,
	int perMsgCount,
	int msgType,
	int funcType
)// msgCount字段暂不用
{
	strPicPath = tr("image://NotifyImgProvider/") + strPicPath;
	MessageNotifyItem item(msgCount, strUserID, strPicPath, strNickName, lastMsg, perMsgCount, msgType, funcType);
	m_msgNotifyModel->pushFront(&item);
	emit sigAdjustStyle(m_msgNotifyModel);
}

MessageNotifyModel* NewMsgNotifyShareLib::getModel()
{
	return m_msgNotifyModel;
}

void NewMsgNotifyShareLib::slotGetIconFlashStatus()
{
}

void NewMsgNotifyShareLib::dealMsgFlash(int funcType, QString imgPath, MessageInfo *messageInfo)
{
	//默认头像设置
	if (!IsValidImage(imgPath))
	{
		if (messageInfo->MsgDeliverType == 0)//个人
		{
			imgPath = QStringLiteral(":/PerChat/Resources/person/temp.png");
		}
		else if (messageInfo->MsgDeliverType == 1) //群组
		{
			imgPath = QStringLiteral(":/GroupChat/Resources/groupchat/group.png");
		}
	}

	//部落好友申请处理
	QString groupApply = tr("Group Apply");
	QString buddyApply = tr("Buddy Apply");
	if (funcType == NewApply)
	{
		AddApplyMessage applyMessage;
		bool ret = gDataBaseOpera->DBFindByIdAddApplyMessage(messageInfo->nFromUserID, applyMessage);
		if (!ret)
			return;
		messageInfo->strMsg = applyMessage.strMessage;
		//好友申请
		if (applyMessage.iType == 0)
			this->OnInsertMessage(0, QString::number(messageInfo->nFromUserID), imgPath, groupApply, messageInfo->strMsg, 1, messageInfo->MessageChildType, funcType);
		//部落申请
		else if (applyMessage.iType == 1)
			this->OnInsertMessage(0, QString::number(messageInfo->nToUserID), imgPath, buddyApply, messageInfo->strMsg, 1, messageInfo->MessageChildType, funcType);
		return;
	}

	//屏蔽第一段load消息
	if ((messageInfo->strMsg == "load" && messageInfo->MessageChildType == Message_PIC) ||
		(messageInfo->strMsg == "load" && messageInfo->MessageChildType == Message_VEDIO))
	{
		return;
	}

	//国际化翻译处理
	msgHandleForINTL(messageInfo);

	bool msgIsExistFlag = false;
	for (int i = 0; i < m_msgNotifyModel->rowCount(); i++)
	{
		int userId = m_msgNotifyModel->data(m_msgNotifyModel->index(i), MessageNotifyModel::UserIdRole).toInt();
		int groupOrBuddyId = 0;
		groupOrBuddyId = messageInfo->MsgDeliverType == 0 ? messageInfo->nFromUserID : messageInfo->nToUserID;
		if (userId == groupOrBuddyId)
		{
			QString msgStr;
			int functionType = m_msgNotifyModel->data(m_msgNotifyModel->index(i), MessageNotifyModel::FuncTypeRole).toInt();
			int msgType = m_msgNotifyModel->data(m_msgNotifyModel->index(i), MessageNotifyModel::MsgTypeRole).toInt();
			if (functionType == NewApply) //跳过申请消息 单独显示
				break;
			if (functionType == Message_AT && messageInfo->MessageChildType != Message_AT)//处理AT逻辑
			{
				msgStr = tr("<font color=\'#f7931e\' >[I was @]</font>") + messageInfo->strMsg;
				messageInfo->MessageChildType = Message_AT;
			}
			else
			{
				msgStr = messageInfo->strMsg;
			}
			int msgCount = m_msgNotifyModel->data(m_msgNotifyModel->index(i), MessageNotifyModel::PerMsgCountRole).toInt();
			msgCount++;
			m_msgNotifyModel->setData(m_msgNotifyModel->index(i), msgStr, MessageNotifyModel::LastMsgRole);
			m_msgNotifyModel->setData(m_msgNotifyModel->index(i), msgCount, MessageNotifyModel::PerMsgCountRole);
			m_msgNotifyModel->setData(m_msgNotifyModel->index(i), tr("image://NotifyImgProvider/") + imgPath, MessageNotifyModel::HeadUrlRole);
			m_msgNotifyModel->setData(m_msgNotifyModel->index(i), messageInfo->MessageChildType, MessageNotifyModel::MsgTypeRole);
			m_msgNotifyModel->setData(m_msgNotifyModel->index(i), funcType, MessageNotifyModel::FuncTypeRole);
			msgIsExistFlag = true;
			emit sigAdjustStyle(m_msgNotifyModel);
		}
	}
	if (!msgIsExistFlag)
	{
		if (messageInfo->MsgDeliverType == 0)
		{
			BuddyInfo info = gDataBaseOpera->DBGetBuddyInfoByID(QString::number(messageInfo->nFromUserID));
			if (info.nUserId == -1)
				info = gDataBaseOpera->DBGetGroupUserFromID(QString::number(messageInfo->nFromUserID));
			QString noteName = (info.strNote == NULL || info.strNote == "") ? info.strNickName : info.strNote;
			this->OnInsertMessage(0, QString::number(messageInfo->nFromUserID), imgPath, noteName, messageInfo->strMsg, 1, messageInfo->MessageChildType, funcType);
		}
		if (messageInfo->MsgDeliverType == 1)
		{
			GroupInfo info = gDataBaseOpera->DBGetGroupFromID(QString::number(messageInfo->nToUserID));
			this->OnInsertMessage(0, QString::number(messageInfo->nToUserID), imgPath, info.groupName, messageInfo->strMsg, 1, messageInfo->MessageChildType, funcType);
		}
	}
}

void NewMsgNotifyShareLib::slotDoClickItem(QString userId)
{
	for (int i = 0; i < m_msgNotifyModel->rowCount(); i++)
	{
		QString currId = m_msgNotifyModel->data(m_msgNotifyModel->index(i), MessageNotifyModel::UserIdRole).toString();
		if (userId == currId)
		{
			int funcType = m_msgNotifyModel->data(m_msgNotifyModel->index(i), MessageNotifyModel::FuncTypeRole).toInt();
			emit sigItemOfMsgBoxClicked(currId, funcType);
		}
	}
	this->m_msgNotifyModel->clear();
	//emit sigHideWidget();
}
void NewMsgNotifyShareLib::slotIgnoreBtnClicked()
{
	this->m_msgNotifyModel->clear();
}

void NewMsgNotifyShareLib::msgHandleForINTL(MessageInfo *messageInfo)
{
	QString content;
	if (messageInfo->MessageChildType == Message_NOTIFY)
	{
		if (messageInfo->strMsg != NULL)
		{
			QJsonDocument doc = QJsonDocument::fromJson(messageInfo->strMsg.toUtf8());
			QVariantMap map = doc.toVariant().toMap();
			if (map.value("type").toString() == "notification")
			{
				content = map.value("content").toString();
			}
			if (map.value("type").toString() == "receivedFile")
			{
				content = tr("The other party has successfully received your file ") + QString("\"%1\"").arg(map.value("fileName").toString());
			}
		}
	}
	else
	{
		//获取发言人名字
		QString buddyName;
		QString fUserbUFF, toUserBuff, lastMsgInInfo;
		if (messageInfo->MsgDeliverType == 1)
		{
			BuddyInfo buddyInfo = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(QString::number(messageInfo->nToUserID), QString::number(messageInfo->nFromUserID));
			buddyName = (buddyInfo.strNote == "" || buddyInfo.strNote == NULL) ? buddyInfo.strNickName : buddyInfo.strNote;
		}
		switch (messageInfo->MessageChildType)
		{
		case Message_TEXT:
		{
			content = messageInfo->strMsg;
			break;
		}
		case Message_NOTICE:
		{
			QJsonDocument doc = QJsonDocument::fromJson(lastMsgInInfo.toUtf8());
			QVariantMap map = doc.toVariant().toMap();
			QString webTitle = map.value("webTitle").toString();
			content = tr("[Announcement]") + webTitle;
			break;
		}
		case Message_URL:
		{
			QJsonDocument doc = QJsonDocument::fromJson(lastMsgInInfo.toUtf8());
			QVariantMap map = doc.toVariant().toMap();
			QString webTitle = map.value("subject").toString();
			content = tr("[Share]") + webTitle;
			break;
		}
		case Message_COMMON:
		{
			QString msgTitle;
			QJsonDocument doc = QJsonDocument::fromJson(lastMsgInInfo.toUtf8());
			QVariantMap map = doc.toVariant().toMap();
			if (map.isEmpty())
				msgTitle = lastMsgInInfo;
			else
			{
				QString systemName = map.value("systemName").toString();
				msgTitle = QString("[%1]").arg(systemName);
			}
			content = msgTitle;
			break;
		}
		case Message_PIC:
			content = tr("[Image]");	break;
		case Message_AUDIO:
			content = tr("[Audio]");	break;
		case Message_VEDIO:
			content = tr("[Video]");	break;
		case Message_FILE:
			content = tr("[File]");		break;
		case Message_AD:
			content = tr("This type of message is not supported for now");	break;
		case Message_TRANSFER:
			content = tr("[Tranfer]");	break;
		case Message_REDBAG:
			content = tr("[Red Packet]");	break;
		case Message_SECRETLETTER:
			content = tr("[Secret Message]");	break;
		case Message_SECRETIMAGE:
			content = tr("[Secret Image]");	break;
		case Message_SECRETFILE:
			content = tr("[Secret File]");	break;
		case Message_LOCATION:
			content = tr("[Location]");	break;
		case Message_GW:
			content = tr("[Gravitational Waves]");	break;
		}
		if (messageInfo->MsgDeliverType == 1)
			content = buddyName + ":" + content;


		if (messageInfo->MessageChildType == Message_AT)
		{
			BuddyInfo buddyInfo = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(QString::number(messageInfo->nToUserID), QString::number(messageInfo->nFromUserID));
			buddyName = (buddyInfo.strNote == "" || buddyInfo.strNote == NULL) ? buddyInfo.strNickName : buddyInfo.strNote;

			QString lastMsgInInfo = messageInfo->strMsg;
			QJsonDocument doc = QJsonDocument::fromJson(lastMsgInInfo.toUtf8());
			QVariantMap map = doc.toVariant().toMap();

			QString atStr = map.value("content").toString();
			content = tr("<font color=\'#f7931e\' >[I was @]</font>") + buddyName + ":" + atStr;
		}
	}
	messageInfo->strMsg = content;
}


bool NewMsgNotifyShareLib::IsValidImage(QString imgPath)
{
	QFile file(imgPath);
	if (file.open(QIODevice::ReadOnly))
	{
		QByteArray byteArray = file.readAll();
		file.close();
		QImage image = QImage::fromData(byteArray);

		if (image.isNull())
		{
			return false;
		}
	}
	else
	{
		return false;
	}

	return true;
}
