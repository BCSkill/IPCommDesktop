﻿#include "NotifyImgProvider.h"
#include <QFile>
#include <QImageReader>
#include <qdebug.h>

NotifyImgProvider::NotifyImgProvider(ImageType type, Flags flags) :
	QQuickImageProvider(type, flags)
{

}

NotifyImgProvider::~NotifyImgProvider()
{

}

QImage NotifyImgProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
	QImageReader reader;

	reader.setDecideFormatFromContent(true);

	QStringList id_list = id.split('?');

	QString file;
	if (id_list.count() > 1)
	{
		file = id_list[0];
	}
	else
	{
		file = id_list[0];
	}
	reader.setFileName(file);

	size->setWidth(reader.size().width());
	size->setHeight(reader.size().height());

	reader.setScaledSize(requestedSize);

	return reader.read();
}
