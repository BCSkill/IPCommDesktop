﻿#include "NewNotifyGui.h"
#include "ui_NewNotifyGui.h"
#include <qdebug.h>

#include <stdio.h>
#include <windows.h>
#include <wininet.h>
#include <qtimer.h>
#include <QFile>
#define ITEM_HEIGHT 50

NewNotifyGui::NewNotifyGui(QWidget *parent)
	: QWidget(parent)
{
	//this->hide();

	ui = new Ui::NewNotifyGui();
	this->setWindowFlags(Qt::FramelessWindowHint | Qt::Tool);
	this->setAttribute(Qt::WA_TranslucentBackground);
	ui->setupUi(this);
	this->initStyle();
	//捕捉鼠标非点击状态移动事件设置
	this->setMouseTracking(true);
	ui->stackedWidget->setMouseTracking(true);
	ui->headWidget->setMouseTracking(true);
	ui->tailWidget->setMouseTracking(true);


	timer = new QTimer(this);
	timer->setSingleShot(true);
	timer->setTimerType(Qt::PreciseTimer);
	connect(timer, SIGNAL(timeout()), this, SLOT(slotHideWindow()));
	connect(ui->ignoreButton, SIGNAL(clicked()), this, SLOT(slotClickButton()));
}

NewNotifyGui::~NewNotifyGui()
{
	if(ui)
	{
		delete ui;
		ui = NULL;
	}
	notifyModel->deleteLater();
}

void NewNotifyGui::initStyle()
{
	QFile style(":/QSS/Resources/QSS/NewMsgNotifyShareLib/newnotifygui.qss");
	style.open(QFile::ReadOnly);
	QString sheet = QLatin1String(style.readAll());
	setStyleSheet(sheet);
	style.close();

	ui->headWidget->setAutoFillBackground(true);
	//ui->headWidget->setStyleSheet("background-color: #fafafa;border-top-left-radius:4px; border-top-right-radius:4px;");
	ui->tailWidget->setAutoFillBackground(true);
	//ui->tailWidget->setStyleSheet("background-color: #fafafa; border-bottom-left-radius:4px; border-bottom-right-radius:4px");
	//ui->newMsgLabel->setStyleSheet("color: #000000; font: 14 'Microsoft YaHei'");

// 	QString buttonStyle("\
// 		QPushButton#ignoreButton	\
// 	{ color: #6A82A5; font: 12 'Microsoft YaHei'; text-align: right}	\
// 		QPushButton#ignoreButton:hover	\
// 	{ color: #999999; font: 12 'Microsoft YaHei' }	\
// 		QPushButton#ignoreButton:pressed	\
// 	{ color: #999999; font: 12 'Microsoft YaHei' }");
// 	ui->ignoreButton->setStyleSheet(buttonStyle);
}

void NewNotifyGui::addQmlWidget(QWidget *qw)
{
	ui->stackedWidget->addWidget(qw);
	ui->stackedWidget->setCurrentWidget(qw);
}

void NewNotifyGui::slotAdjustStyle(MessageNotifyModel *notifyModel)
{
	this->notifyModel = notifyModel;
	ui->stackedWidget->setFixedHeight(ITEM_HEIGHT*(notifyModel->rowCount()));
	int windowHeight = ui->headWidget->height() + ui->stackedWidget->height() + ui->tailWidget->height();
	int tailToTopHeight = windowHeight - ui->tailWidget->height();
	ui->tailWidget->setGeometry(0, tailToTopHeight, ui->tailWidget->width(), ui->tailWidget->height());
	this->setFixedHeight(windowHeight);

	msgCounts = 0;
	for (int i = 0; i < notifyModel->rowCount(); i++)
	{
		msgCounts += notifyModel->data(notifyModel->index(i), MessageNotifyModel::PerMsgCountRole).toInt();
	}
	QString headStr = tr("New Message") + " (" + QString::number(msgCounts) + ")";
	//ui->newMsgLabel->setStyleSheet("color: #000000; font: 14 'Microsoft YaHei'");
	ui->newMsgLabel->setText(headStr);

	QPoint currPoint = handleWinPos(sysTrayPos);
	this->move(currPoint.x(), currPoint.y());
	QPoint leftTopPos = this->pos();
	QPoint rightBottomPos = QPoint(leftTopPos.x() + ui->headWidget->width(),
		leftTopPos.y() + ui->headWidget->height() + ui->stackedWidget->height() + ui->tailWidget->height());
	QRect rect(leftTopPos, rightBottomPos);
	emit sigSendBoxRect(rect);
}

void NewNotifyGui::slotMouseSuspendIn(QPoint point)
{
	timer->stop();

	if(point != QPoint(0, 0))
		sysTrayPos = point;
	//处理坐标
	QPoint currPoint =  handleWinPos(sysTrayPos);

	this->move(currPoint.x(), currPoint.y());
	if (!this->isVisible() && notifyModel->rowCount() > 0)
	{
		//设置窗口显示最前 否则会被任务栏覆盖
		this->setWindowFlags(this->windowFlags() | Qt::WindowStaysOnTopHint);
		this->showNormal();
		this->show();
	}
	QPoint leftTopPos = this->pos();
	QPoint rightBottomPos = QPoint(leftTopPos.x() + ui->headWidget->width(),
		leftTopPos.y() + ui->headWidget->height() + ui->stackedWidget->height() + ui->tailWidget->height());
	QRect rect(leftTopPos, rightBottomPos);
	emit sigSendBoxRect(rect);
}
void NewNotifyGui::slotMouseSuspendOut(QPoint point, int time)
{
	if (time != 0)
		timer->start(time * 1000);
	else
		this->slotHideWindow();
}

void NewNotifyGui::slotClickButton()
{
	this->notifyModel->clear();
	if (this->isVisible())
		this->hide();
	emit sigIgnoreBtnClicked();
}
	
void NewNotifyGui::slotHideWindow()
{
	if (this->isVisible())
	{
		this->hide();
		//重置矩形框
		QPoint leftTopPos(0, 0);
		QPoint rightBottomPos(1, 1);
		QRect rect(leftTopPos, rightBottomPos);
		emit sigSendBoxRect(rect);
	}
}
QPoint NewNotifyGui::handleWinPos(QPoint inPoint)
{
	int window_Width, window_Height, relative_Width, relative_Height;//屏幕宽度/屏幕相对宽度
	int caption_Height;	//字幕栏宽/高度
	int taskBar_Height;	//任务栏宽/高度
	int pro_Lenth, pro_Height; //消息盒子宽/高度
	int range_WindowToPro_X, range_PointToBottom_Y; //消息盒子左上角距离
	int pointX, pointY;

	window_Width = GetSystemMetrics(SM_CXSCREEN); //屏幕宽度(整体宽度)
	window_Height = GetSystemMetrics(SM_CYSCREEN); //屏幕高度(整体高度)
	relative_Width = GetSystemMetrics(SM_CXFULLSCREEN); //相对显示宽度 
	relative_Height = GetSystemMetrics(SM_CYFULLSCREEN); //相对显示高度 
	caption_Height = GetSystemMetrics(SM_CYCAPTION); //字幕栏高度
	pro_Lenth = this->width();
	pro_Height = this->height();

	APPBARDATA taskbar = { 0 };
	taskbar.cbSize = sizeof(APPBARDATA);
	SHAppBarMessage(ABM_GETTASKBARPOS, &taskbar);

	switch (taskbar.uEdge)
	{
		case ABE_BOTTOM:
		{
			taskBar_Height = window_Height - relative_Height - caption_Height; 
			range_WindowToPro_X = window_Width - inPoint.x() - pro_Lenth / 2;
			range_PointToBottom_Y = window_Height - inPoint.y();

			pointX = range_WindowToPro_X < 0 ?
				window_Width - pro_Lenth : inPoint.x() - pro_Lenth / 2;
			pointY = range_PointToBottom_Y > taskBar_Height ?
				inPoint.y() - this->height() : relative_Height + caption_Height - this->height();

			QPoint outPoint(pointX, pointY);
			return outPoint;
		}
		case ABE_TOP:
		{
			taskBar_Height = window_Height - relative_Height - caption_Height; 
			range_WindowToPro_X = window_Width - inPoint.x() - pro_Lenth / 2;

			pointX = range_WindowToPro_X < 0 ?
				window_Width - pro_Lenth : inPoint.x() - pro_Lenth / 2;
			pointY = taskBar_Height;
			QPoint outPoint(pointX, pointY);
			return outPoint;
		}
		case ABE_LEFT:
		{
			taskBar_Height = window_Width - relative_Width;
			pointX = taskBar_Height;
			pointY = inPoint.y() - pro_Height/2;
			range_PointToBottom_Y = window_Height - inPoint.y() - pro_Height / 2;
			pointY = range_PointToBottom_Y <= 0 ? window_Height - pro_Height : pointY;

			QPoint outPoint(pointX, pointY);
			return outPoint;
		}
		case ABE_RIGHT:
		{
			pointX = relative_Width - pro_Lenth;
			pointY = inPoint.y() - pro_Height / 2;
			range_PointToBottom_Y = window_Height - inPoint.y() - pro_Height / 2;
			pointY = range_PointToBottom_Y <= 0 ? window_Height - pro_Height : pointY;

			QPoint outPoint(pointX, pointY);
			return outPoint;
		}
		default:
		{
			return QPoint(0, 0);
			break;
		}
	}
}

//void NewNotifyGui::mousePressEvent(QMouseEvent *event)
//{
//}
//
//void NewNotifyGui::mouseMoveEvent(QMouseEvent *event)
//{
//}
