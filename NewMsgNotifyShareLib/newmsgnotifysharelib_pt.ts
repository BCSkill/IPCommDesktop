<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>NewMsgNotifyShareLib</name>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="66"/>
        <location filename="NewMsgNotifyShareLib.cpp" line="151"/>
        <source>image://NotifyImgProvider/</source>
        <translation></translation>
    </message>
    <message>
        <source>Tribe Apply</source>
        <translation type="vanished">Tribo Aplicar</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="97"/>
        <source>Group Apply</source>
        <translation>Grupo Aplicar</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="98"/>
        <source>Buddy Apply</source>
        <translation>Amigo Aplica</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="140"/>
        <location filename="NewMsgNotifyShareLib.cpp" line="301"/>
        <source>&lt;font color=&apos;#f7931e&apos; &gt;[I was @]&lt;/font&gt;</source>
        <translation>&lt;font color=&apos;#f7931e&apos; &gt;[Eu fui @]&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="210"/>
        <source>The other party has successfully received your file </source>
        <translation>A outra parte recebeu com sucesso seu arquivo </translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="236"/>
        <source>[Announcement]</source>
        <translation>[Anúncio]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="244"/>
        <source>[Share]</source>
        <translation>[Share]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="263"/>
        <source>[Image]</source>
        <translation>[Imagem]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="265"/>
        <source>[Audio]</source>
        <translation>[Áudio]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="267"/>
        <source>[Video]</source>
        <translation>[Vídeo]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="269"/>
        <source>[File]</source>
        <translation>[Arquivo]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="271"/>
        <source>This type of message is not supported for now</source>
        <translation>Este tipo de mensagem não é suportado por enquanto</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="273"/>
        <source>[Tranfer]</source>
        <translation>[Transferir]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="275"/>
        <source>[Red Packet]</source>
        <translation>[Pacote vermelho]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="277"/>
        <source>[Secret Message]</source>
        <translation>[Mensagem Secreta]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="279"/>
        <source>[Secret Image]</source>
        <translation>[Imagem Secreta]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="281"/>
        <source>[Secret File]</source>
        <translation>[Arquivo Secreto]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="283"/>
        <source>[Location]</source>
        <translation>[Localização]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="285"/>
        <source>[Gravitational Waves]</source>
        <translation>[Ondas gravitacionais]</translation>
    </message>
</context>
<context>
    <name>NewNotifyGui</name>
    <message>
        <location filename="NewNotifyGui.ui" line="32"/>
        <location filename="GeneratedFiles/ui_NewNotifyGui.h" line="91"/>
        <source>NewNotifyGui</source>
        <translation></translation>
    </message>
    <message>
        <location filename="NewNotifyGui.ui" line="124"/>
        <location filename="GeneratedFiles/ui_NewNotifyGui.h" line="92"/>
        <location filename="NewNotifyGui.cpp" line="90"/>
        <source>New Message</source>
        <translation>Nova mensagem</translation>
    </message>
    <message>
        <location filename="NewNotifyGui.ui" line="150"/>
        <location filename="GeneratedFiles/ui_NewNotifyGui.h" line="93"/>
        <source>Ignore</source>
        <translation>Ignorar</translation>
    </message>
</context>
</TS>
