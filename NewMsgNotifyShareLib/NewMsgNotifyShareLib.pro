QT+=core gui widgets quick quickwidgets quickcontrols2 sql
include(shared.pri)
include(NewMsgNotifyShareLib.pri)

INCLUDEPATH += ../SharedLib_mac/common \
    ../SharedLib_mac/IMDataBaseOperaInfo/include \
    ../SharedLib_mac/SqlLiteShareLib/include
