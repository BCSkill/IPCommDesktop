﻿#include "MessageNotifyModel.h"
#include <QDateTime>

MessageNotifyItem::MessageNotifyItem(const int &msgCount, const QString &userId, const QString &headUrl, const QString &nickName, const QString &lastMsg, const int &perMsgCount, const int &msgType, const int &funcType)
	: m_msgCount(msgCount), m_userId(userId), m_headUrl(headUrl), m_nickName(nickName), m_lastMsg(lastMsg), m_perMsgCount(perMsgCount), m_msgType(msgType), m_funcType(funcType)
{
}

int MessageNotifyItem::msgCount() const
{
	return m_msgCount;
}
QString MessageNotifyItem::userId() const
{
	return m_userId;
}
QString MessageNotifyItem::headUrl() const
{
	return m_headUrl;
}
QString MessageNotifyItem::nickName() const
{
	return m_nickName;
}
QString MessageNotifyItem::lastMsg() const
{
	return m_lastMsg;
}
int MessageNotifyItem::perMsgCount() const
{
	return m_perMsgCount;
}
int MessageNotifyItem::msgType() const
{
	return m_msgType;
}
int MessageNotifyItem::funcType() const
{
	return m_funcType;
}



void MessageNotifyItem::setMsgCount(QVariant val)
{
	m_msgCount = val.toInt();
}
void MessageNotifyItem::setUserId(QVariant val)
{
	m_userId = val.toString();
}
void MessageNotifyItem::setHeadUrl(QVariant val)
{
	m_headUrl = val.toString();
}
void MessageNotifyItem::setNickName(QVariant val)
{
	m_nickName = val.toString();
}
void MessageNotifyItem::setLastMsg(QVariant val)
{
	m_lastMsg = val.toString();
}
void MessageNotifyItem::setPerMsgCount(QVariant val)
{
	m_perMsgCount = val.toInt();
}
void MessageNotifyItem::setMsgType(QVariant val)
{
	m_msgType = val.toInt();
}
void MessageNotifyItem::setFuncType(QVariant val)
{
	m_msgType = val.toInt();
}





MessageNotifyModel::MessageNotifyModel(QObject *parent)
	: QAbstractListModel(parent)
{
}

void MessageNotifyModel::pushFront(const MessageNotifyItem *item)
{
	beginInsertRows(QModelIndex(), 0, 0);
	m_items.push_front(*item);
	endInsertRows();
}

void MessageNotifyModel::pushBack(const MessageNotifyItem *item)
{
	beginInsertRows(QModelIndex(), rowCount(), rowCount());
	m_items.push_back(*item);
	endInsertRows();
}
void MessageNotifyModel::insertItem(const MessageNotifyItem *item, const int index)
{
	beginInsertRows(QModelIndex(), index, index);
	m_items.insert(index, *item);
	endInsertRows();
}


int MessageNotifyModel::rowCount(const QModelIndex & parent) const {
	Q_UNUSED(parent);
	return m_items.count();
}


QVariant MessageNotifyModel::data(const QModelIndex & index, int role) const {
	if (index.row() < 0 || index.row() >= m_items.count())
		return QVariant();

	const MessageNotifyItem &item = m_items[index.row()];


	if (role == UserIdRole)
	{
		return item.userId();
	}
	else if (role == HeadUrlRole)
	{
		return item.headUrl();
	}
	else if (role == NickNameRole)
	{
		return item.nickName();
	}
	else if (role == LastMsgRole)
	{
		return item.lastMsg();
	}
	else if (role == PerMsgCountRole)
	{
		return item.perMsgCount();
	}
	else if (role == MsgCountRole)
	{
		return item.msgCount();
	}
	else if (role == MsgTypeRole)
	{
		return item.msgType();
	}
	else if (role == FuncTypeRole)
	{
		return item.funcType();
	}

	return QVariant();
}

bool MessageNotifyModel::setData(const QModelIndex &index, const QVariant &value, int role/* = Qt::EditRole*/)
{
	if (index.row() < 0 || index.row() >= m_items.count())
		return false;

	MessageNotifyItem &item = m_items[index.row()];
	if (role == MsgCountRole)
	{
		item.setMsgCount(value);
	}
	else if (role == UserIdRole)
	{
		item.setUserId(value);
	}
	else if (role == HeadUrlRole)
	{
		item.setHeadUrl(value.toString() + "?" + QString::number(QDateTime::currentDateTime().toMSecsSinceEpoch()));
	}
	else if (role == NickNameRole)
	{
		item.setNickName(value);
	}
	else if (role == LastMsgRole)
	{
		item.setLastMsg(value);
	}
	else if (role == PerMsgCountRole)
	{
		item.setPerMsgCount(value);
	}
	else if (role == MsgTypeRole)
	{
		item.setMsgType(value);
	}
	else if (role == FuncTypeRole)
	{
		item.setFuncType(value);
	}

	emit dataChanged(index, index);
	return true;
}

//![0]
QHash<int, QByteArray> MessageNotifyModel::roleNames() const {
	QHash<int, QByteArray> roles;
	roles[MsgCountRole] = "msgCount";
	roles[UserIdRole] = "userId";
	roles[HeadUrlRole] = "headUrl";
	roles[NickNameRole] = "nickName";
	roles[LastMsgRole] = "lastMsg";
	roles[PerMsgCountRole] = "perMsgCount";
	roles[MsgTypeRole] = "msgType";
	roles[FuncTypeRole] = "funcType";
	return roles;
}

void MessageNotifyModel::clear()
{
	if (m_items.count() > 0)
	{
		beginRemoveRows(QModelIndex(), 0, m_items.count() - 1);
		m_items.clear();
		endRemoveRows();
	}

}

void MessageNotifyModel::takeItem(int row)
{
	beginRemoveRows(QModelIndex(), row, row);
	m_items.removeAt(row);
	endRemoveRows();
}

QString MessageNotifyModel::currentSelectedUserId()
{
	return  m_currentSelectedUserId;
}

void MessageNotifyModel::setCurrentSelectedUserId(QString userId)
{
	m_currentSelectedUserId = userId;
}