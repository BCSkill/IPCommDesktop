<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hy_AM">
<context>
    <name>NewMsgNotifyShareLib</name>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="66"/>
        <location filename="NewMsgNotifyShareLib.cpp" line="151"/>
        <source>image://NotifyImgProvider/</source>
        <translation></translation>
    </message>
    <message>
        <source>Tribe Apply</source>
        <translation type="vanished">Ցեղ Ցանկում</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="97"/>
        <source>Group Apply</source>
        <translation>Խմբի կիրառումը</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="98"/>
        <source>Buddy Apply</source>
        <translation>Ընկերության հայցը</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="140"/>
        <location filename="NewMsgNotifyShareLib.cpp" line="301"/>
        <source>&lt;font color=&apos;#f7931e&apos; &gt;[I was @]&lt;/font&gt;</source>
        <translation>&lt;font color=&apos;#f7931e&apos; &gt;[Ես @]&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="210"/>
        <source>The other party has successfully received your file </source>
        <translation>Մյուս կողմը հաջողությամբ ստացել է ձեր ֆայլը </translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="236"/>
        <source>[Announcement]</source>
        <translation>[Հայտարարություն]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="244"/>
        <source>[Share]</source>
        <translation>[Բաժնետոմս]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="263"/>
        <source>[Image]</source>
        <translation>[նկարը]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="265"/>
        <source>[Audio]</source>
        <translation>[Աուդիո]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="267"/>
        <source>[Video]</source>
        <translation>[Տեսանյութ]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="269"/>
        <source>[File]</source>
        <translation>[Ֆայլ]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="271"/>
        <source>This type of message is not supported for now</source>
        <translation>Այս հաղորդագրության տեսակը դեռեւս չի աջակցվում</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="273"/>
        <source>[Tranfer]</source>
        <translation>[Փոխանցում]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="275"/>
        <source>[Red Packet]</source>
        <translation>[Կարմիր փաթեթ]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="277"/>
        <source>[Secret Message]</source>
        <translation>[Գաղտնի հաղորդագրություն]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="279"/>
        <source>[Secret Image]</source>
        <translation>[Գաղտնի պատկեր]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="281"/>
        <source>[Secret File]</source>
        <translation>[Գաղտնի Ֆայլ]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="283"/>
        <source>[Location]</source>
        <translation>[Որտեղից]</translation>
    </message>
    <message>
        <location filename="NewMsgNotifyShareLib.cpp" line="285"/>
        <source>[Gravitational Waves]</source>
        <translation>[Գրաֆիտացիոն ալիք]</translation>
    </message>
</context>
<context>
    <name>NewNotifyGui</name>
    <message>
        <location filename="NewNotifyGui.ui" line="32"/>
        <location filename="GeneratedFiles/ui_NewNotifyGui.h" line="91"/>
        <source>NewNotifyGui</source>
        <translation></translation>
    </message>
    <message>
        <location filename="NewNotifyGui.ui" line="124"/>
        <location filename="GeneratedFiles/ui_NewNotifyGui.h" line="92"/>
        <location filename="NewNotifyGui.cpp" line="90"/>
        <source>New Message</source>
        <translation>Նոր հաղորդագրություն</translation>
    </message>
    <message>
        <location filename="NewNotifyGui.ui" line="150"/>
        <location filename="GeneratedFiles/ui_NewNotifyGui.h" line="93"/>
        <source>Ignore</source>
        <translation>Անտեսեք</translation>
    </message>
</context>
</TS>
