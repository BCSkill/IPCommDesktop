﻿#include "threadrequestgroupbuddyinfo.h"
#include "httpnetworksharelib.h"
#include <QEventLoop>
#include <QJsonParseError>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDir>
#include <QDebug>
#include "QStringLiteralBak.h"
#include "globalmanager.h"
#ifndef Q_OS_WIN
#include "inline_mac.h"
#endif

ThreadRequestGroupBuddyInfo::ThreadRequestGroupBuddyInfo(QObject *parent)
	: QThread(parent)
{
	qRegisterMetaType<QList<BuddyInfo>>("QList<BuddyInfo>");
}

ThreadRequestGroupBuddyInfo::~ThreadRequestGroupBuddyInfo()
{
	
}

void ThreadRequestGroupBuddyInfo::ParseGroupBuddyInfo(QByteArray byteArray)
{
	QList<BuddyInfo> listBuddy;
	QJsonParseError jsonError;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(byteArray, &jsonError);
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QJsonObject obj = jsonDocument.object();
			if (obj["result"].toString() == "success")
			{
				QVariant strTemp;
				QJsonValue groupValue;
				foreach(QString key, obj.keys())
				{
					groupValue = obj.take(key);
					strTemp = groupValue.toVariant();
					if (groupValue.isArray())
					{
						QVariantList groupList = strTemp.toList();
						for (int i = 0; i < groupList.size(); i++)
						{
							QVariantMap listData = groupList.at(i).toMap();
							BuddyInfo buddyInfo;
                            buddyInfo.strHttpAvatar = listData["avatar"].toString();
							buddyInfo.strEmail = listData["email"].toString();
							buddyInfo.strMobilePhone = listData["mobilePhone"].toString();
							buddyInfo.strNickName = listData["nickName"].toString();
							buddyInfo.strNote = listData["note"].toString();
							buddyInfo.strPhone = listData["phone"].toString();
							buddyInfo.strSex = listData["sex"].toString();
							buddyInfo.strSign = listData["sign"].toString();
							buddyInfo.nUserId = listData["userId"].toInt();
							buddyInfo.strUserName = listData["userName"].toString();
							buddyInfo.nUserType = listData["mana"].toInt();
							buddyInfo.disableStrangers = listData["disableStrangers"].toInt();
							buddyInfo.strLocalAvatar = gSettingsManager->getUserPath() + "/resource/header/" + listData["userId"].toString() + ".jpg";
							//if (buddyInfo.strSex == "F")//默认头像
							//{
							//	buddyInfo.strDefaultAvatar = QString(":/IMChatClient/Resources/imchatclient/female.png");
							//}
							//else
							//{
							//	buddyInfo.strDefaultAvatar = QString(":/PerChat/Resources/person/temp.png");
							//}
							buddyInfo.strDefaultAvatar = QString(":/PerChat/Resources/person/temp.png");
							listBuddy.append(buddyInfo);
						}
					}
				}
			}
			else
				qDebug() << tr("解析部落成员失败");
		}
	}
	emit sigParseGroupBuddyInfo(this->objectName(), listBuddy);
}

QString ThreadRequestGroupBuddyInfo::GetExeDir()
{
	QDir dir;
#ifdef Q_OS_WIN
	return dir.currentPath();
#else
    return getResourcePath();
#endif
}

void ThreadRequestGroupBuddyInfo::run()
{
	QNetworkAccessManager m_qnam;
	const QUrl url = QUrl::fromUserInput(mUrl);
	QNetworkRequest qnr(url);
	QNetworkReply* reply = m_qnam.get(qnr); 
	QTimer timer;
	QEventLoop eventLoop;
	connect(reply, &QNetworkReply::finished, &eventLoop, &QEventLoop::quit);
	QObject::connect(&timer, SIGNAL(timeout()), &eventLoop, SLOT(quit()));
	timer.start(5000);
	eventLoop.exec();

	if (timer.isActive()){
		timer.stop();
	}

	if (reply->error() == QNetworkReply::NoError)
	{
		QByteArray replyData = reply->readAll();
		ParseGroupBuddyInfo(replyData);
	}
	else
	{
		qDebug() << tr("请求好友失败");
	}
	reply->deleteLater();
	reply = nullptr;
}
