﻿#include "threadrequestgroupinfo.h"
#include "httpnetworksharelib.h"
#include <QEventLoop>
#include <QJsonParseError>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDir>
#ifndef Q_OS_WIN
#include "inline_mac.h"
#endif
#include "QStringLiteralBak.h"
#include "globalmanager.h"

#include <QMutex>
#include <QWaitCondition>

QMutex g_sigParseGroupInfoMutex;
QWaitCondition g_sigParseGroupInfoWaitCondition;

ThreadRequestGroupInfo::ThreadRequestGroupInfo(QObject *parent)
	: QThread(parent)
{
	qRegisterMetaType<GroupInfo>("GroupInfo");
}

ThreadRequestGroupInfo::~ThreadRequestGroupInfo()
{

}

void ThreadRequestGroupInfo::run()
{
	if (mListGroupInfo.size() == 0)
	{
		QNetworkAccessManager m_qnam;
		const QUrl url = QUrl::fromUserInput(mUrl);
		QNetworkRequest qnr(url);
		QNetworkReply* reply = m_qnam.get(qnr); //m_qnam是QNetworkAccessManager对象
		QTimer timer;
		QEventLoop eventLoop;
		connect(reply, &QNetworkReply::finished, &eventLoop, &QEventLoop::quit);
		QObject::connect(&timer, SIGNAL(timeout()), &eventLoop, SLOT(quit()));
		timer.start(10000);
		eventLoop.exec();

		if (timer.isActive()){
			timer.stop();
		}

		if (reply->error() == QNetworkReply::NoError)
		{
			QByteArray replyData = reply->readAll();
			ParseGroupInfo(replyData);
		}
		else
		{
			qDebug() << tr("请求部落失败");
		}
		reply->deleteLater();
		reply = nullptr;
	}
	else  //通过数据库
	{
		for (int i = 0; i < mListGroupInfo.size();i++)
		{
			//msleep(100);
			g_sigParseGroupInfoMutex.lock();

			emit sigParseGroupInfo(mListGroupInfo[i]);

			QTime time;
			time.start();

			g_sigParseGroupInfoWaitCondition.wait(&g_sigParseGroupInfoMutex);

			msleep(time.elapsed());

			g_sigParseGroupInfoMutex.unlock();
		}
	}
}

void ThreadRequestGroupInfo::ParseGroupInfo(QByteArray byteArray)
{
	QJsonParseError jsonError;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(byteArray, &jsonError);
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QJsonObject obj = jsonDocument.object();

			if (obj["result"].toString() == "success")
			{
				QVariant strTemp;
				QJsonValue groupValue;
				foreach(QString key, obj.keys())
				{
					groupValue = obj[key];
					strTemp = groupValue.toVariant();
					if (groupValue.isArray())
					{
						QVariantList groupList = strTemp.toList();
						for (int i = 0; i < groupList.size(); i++)
						{
							QVariantMap listData = groupList.at(i).toMap();
							GroupInfo groupInfo;
							groupInfo.groupId = listData["groupId"].toString();
							groupInfo.groupName = listData["groupName"].toString();
							groupInfo.createTime = listData["createTime"].toString();
							groupInfo.createUserId = listData["createUserId"].toString();
							groupInfo.groupHttpHeadImage = listData["avatar"].toString();
							groupInfo.groupLoacalHeadImage = gSettingsManager->getUserPath() + "/resource/header/groupheader/" + listData["groupId"].toString() + ".jpg";;
							groupInfo.groupDefaultAvatar = tr(":/GroupChat/Resources/groupchat/group.png");
							groupInfo.noSpeak = listData["noSpeak"].toInt();
							groupInfo.groupType = listData["groupType"].toInt();
							groupInfo.groupDesc = listData["groupDesc"].toString();
							groupInfo.groupKey = listData["groupKey"].toString();
							groupInfo.msgPrompt = listData["msgPrompt"].toInt();

							msleep(50);
							emit sigParseGroupInfo(groupInfo);
						}

						break;
					}
				}
			}
		}
	}
	else
	{
		qDebug() << tr("请求部落信息失败!");
	}
}

QString ThreadRequestGroupInfo::GetExeDir()
{
	QDir dir;
#ifdef Q_OS_WIN
	return dir.currentPath();
#else
    return getResourcePath();
#endif
}
