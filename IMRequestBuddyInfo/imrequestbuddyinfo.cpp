﻿#include "imrequestbuddyinfo.h"
#include "httpnetworksharelib.h"
#include "threadrequestbuddyinfo.h"
#include "threadrequestgroupinfo.h"
#include "threadrequestgroupbuddyinfo.h"
#include <QJsonParseError>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDir>
#include <QDebug>
#include "define.h"
#include "globalmanager.h"
#ifndef Q_OS_WIN
#include "inline_mac.h"
#endif

IMRequestBuddyInfo::IMRequestBuddyInfo(QObject *parent)
	: QObject(parent)
{

}

IMRequestBuddyInfo::~IMRequestBuddyInfo()
{

}

QString IMRequestBuddyInfo::GetExeDir()
{
	QDir dir;
#ifdef Q_OS_WIN
    return dir.currentPath();
#else
    return getResourcePath();
#endif
}

//请求好友信息
void IMRequestBuddyInfo::RequestBuddyInfo(QString strUrl,QString strIMUserID,QString strPwd,QString appID)
{
	ThreadRequestBuddyInfo *pRequestBuddy = new ThreadRequestBuddyInfo();
	connect(pRequestBuddy, SIGNAL(sigParseBuddyInfo(BuddyInfo)), this, SIGNAL(sigParseBuddyInfo(BuddyInfo)));
	connect(pRequestBuddy, SIGNAL(finished()), pRequestBuddy, SLOT(deleteLater()));
	QString strRequestUrl = SwitchRequestBuddyInfo(strUrl + HTTP_GETBUDDYLIST, strIMUserID, strPwd, appID);
	pRequestBuddy->setUrl(strRequestUrl);
	pRequestBuddy->start();
}

void IMRequestBuddyInfo::RequestBuddyInfo(QList<BuddyInfo> listBuddy)
{
	ThreadRequestBuddyInfo *pRequestBuddy = new ThreadRequestBuddyInfo();
	connect(pRequestBuddy, SIGNAL(sigParseBuddyInfo(BuddyInfo)), this, SIGNAL(sigParseBuddyInfo(BuddyInfo)));
	connect(pRequestBuddy, SIGNAL(finished()), pRequestBuddy, SLOT(deleteLater()));
	pRequestBuddy->setBuddyList(listBuddy);
	pRequestBuddy->start();
}

//拼接请求好友信息
QString IMRequestBuddyInfo::SwitchRequestBuddyInfo(QString url, QString AccountName, QString PassWord,QString appID)
{
	QString strURL;
#ifdef Q_OS_WIN
	QString strTemp = "&appId=" + appID + "&deviceType=win";
#else
    QString strTemp = "&appId=" + appID + "&deviceType=mac";
#endif
	strURL = url;
	strURL += "?userId=" + AccountName + "&passWord=" + PassWord + strTemp;
	return strURL;
}

//请求部落信息
void IMRequestBuddyInfo::RequestGroupInfo(QString strUrl, QString strIMUserID, QString strPwd, QString appID)
{
	ThreadRequestGroupInfo *pRequestGroup = new ThreadRequestGroupInfo();
	connect(pRequestGroup, SIGNAL(sigParseGroupInfo(GroupInfo)), this, SIGNAL(sigParseGroupInfo(GroupInfo)));
	connect(pRequestGroup, SIGNAL(finished()), pRequestGroup, SLOT(deleteLater()));
	QString strRequestUrl = SwitchRequestBuddyInfo(strUrl + HTTP_GETGROUPID, strIMUserID, strPwd, appID);
	pRequestGroup->setUrl(strRequestUrl);
	pRequestGroup->start();
}

void IMRequestBuddyInfo::RequestGroupInfo(QList<GroupInfo> listGroupInfo)
{
	ThreadRequestGroupInfo *pRequestGroup = new ThreadRequestGroupInfo();
	connect(pRequestGroup, SIGNAL(sigParseGroupInfo(GroupInfo)), this, SIGNAL(sigParseGroupInfo(GroupInfo)));
	connect(pRequestGroup, SIGNAL(finished()), pRequestGroup, SLOT(deleteLater()));
	pRequestGroup->setGroupInfoList(listGroupInfo);
	pRequestGroup->start();
}

//请求部落成员信息
void IMRequestBuddyInfo::RequestGroupBuddyInfo(QString strUrl, QString strIMUserID, QString strPwd, QString groupID)
{
	ThreadRequestGroupBuddyInfo *pRequestGroupBuddy = new ThreadRequestGroupBuddyInfo();
	connect(pRequestGroupBuddy, SIGNAL(sigParseGroupBuddyInfo(QString, QList<BuddyInfo>)), this, SIGNAL(sigParseGroupBuddyInfo(QString, QList<BuddyInfo>)));
	connect(pRequestGroupBuddy, SIGNAL(finished()), pRequestGroupBuddy, SLOT(deleteLater()));
	QString strRequestUrl = StitchGroupBuddyParameter(strUrl, strIMUserID, strPwd, groupID);
	pRequestGroupBuddy->setUrl(strRequestUrl);
	pRequestGroupBuddy->setObjectName(groupID);
	pRequestGroupBuddy->start();
}

//拼接部落成员URL
QString IMRequestBuddyInfo::StitchGroupBuddyParameter(QString url, QString AccountName, QString PassWord, QString strGroupID)
{
	QString strHttp = url + HTTP_GETGROUPUSER+ "?userId=" + AccountName +
		"&passWord=" + PassWord + "&groupId=" + strGroupID;
	return strHttp;
}

//请求个人信息
void IMRequestBuddyInfo::RequestPersonInfo(QString strUrl,QString strUserID)
{
	HttpNetWork::HttpNetWorkShareLib *PersonInfo = new HttpNetWork::HttpNetWorkShareLib();
	connect(PersonInfo, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotRequestPersonInfoFinished(bool, QString)));
	QString strRequestUrl = strUrl + HTTP_GetUserInfo + strUserID;
	PersonInfo->getHttpRequest(strRequestUrl);
}

//请求个人信息结果
void IMRequestBuddyInfo::slotRequestPersonInfoFinished(bool bResult, QString strResult)
{
	UserInfo userInfo;
	if (bResult)
	{
		QJsonParseError jsonError;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(strResult.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QVariantMap result = jsonDocument.toVariant().toMap();
				if (result["result"].toString() == "success")
				{
					QVariantMap usrInfo = result["user"].toMap();
					userInfo.nUserID = usrInfo["userId"].toInt();
					userInfo.strUserNickName = usrInfo["nickName"].toString();
					userInfo.strUserName = usrInfo["userName"].toString();
					userInfo.strUserAvatarHttp = usrInfo["avatar"].toString();
					userInfo.strSex = usrInfo["sex"].toString();
					userInfo.strNote = usrInfo["note"].toString();
					userInfo.strEmil = usrInfo["email"].toString();
					userInfo.strSign = usrInfo["sign"].toString();
					userInfo.strPhone = usrInfo["phone"].toString();
					userInfo.strUserAvatarLocal = gSettingsManager->getUserPath() + "/resource/header/" + usrInfo["userId"].toString() + ".jpg";
					emit sigRequestPersonInfoSuccess(true,userInfo);
					return;
				}
			}
		}
	}
	emit sigRequestPersonInfoSuccess(false, userInfo);
}
