﻿#include "threadrequestbuddyinfo.h"
#include "httpnetworksharelib.h"
#include <QEventLoop>
#include <QJsonParseError>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDir>
#include <QDebug>
#ifndef Q_OS_WIN
#include "inline_mac.h"
#endif
#include "QStringLiteralBak.h"
#include "globalmanager.h"

#include <QMutex>
#include <QWaitCondition>

QMutex g_sigParseBuddyInfoMutex;
QWaitCondition g_sigParseBuddyInfoWaitCondition;

ThreadRequestBuddyInfo::ThreadRequestBuddyInfo(QObject *parent)
	: QThread(parent)
{
	qRegisterMetaType<BuddyInfo>("BuddyInfo");
}

ThreadRequestBuddyInfo::~ThreadRequestBuddyInfo()
{
	requestInterruption();
	quit();
	wait();
}

QString ThreadRequestBuddyInfo::GetExeDir()
{
	QDir dir;
#ifdef Q_OS_WIN
    return dir.currentPath();
#else
    return getResourcePath();
#endif
}

void ThreadRequestBuddyInfo::run()
{
	if (mListBuddy.size() == 0)
	{
		QNetworkAccessManager m_qnam;
		const QUrl url = QUrl::fromUserInput(mUrl);
		QNetworkRequest qnr(url);
		QNetworkReply* reply = m_qnam.get(qnr); //m_qnam是QNetworkAccessManager对象
		QTimer timer;
		QEventLoop eventLoop;
		connect(reply, &QNetworkReply::finished, &eventLoop, &QEventLoop::quit);
		QObject::connect(&timer, SIGNAL(timeout()), &eventLoop, SLOT(quit()));
		timer.start(5000);
		eventLoop.exec();

		if (timer.isActive()){
			timer.stop();
		}

		if (reply->error() == QNetworkReply::NoError)
		{
			QByteArray replyData = reply->readAll();
			ParseBuddyInfo(replyData);
		}
		else
		{
			qDebug() << tr("请求好友失败");
		}
		reply->deleteLater();
		reply = nullptr;
	}
	else
	{
		for (int i = 0; i < mListBuddy.size();i++)
		{
			if (!isInterruptionRequested())
			{
				//msleep(50);
				g_sigParseBuddyInfoMutex.lock();

				emit sigParseBuddyInfo(mListBuddy[i]);

				QTime time;
				time.start();

				g_sigParseBuddyInfoWaitCondition.wait(&g_sigParseBuddyInfoMutex);

				msleep(time.elapsed());

				g_sigParseBuddyInfoMutex.unlock();
			}
			else
				break;
		}
	}
}

void ThreadRequestBuddyInfo::ParseBuddyInfo(QByteArray byteArray)
{
	QJsonParseError jsonError;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(byteArray, &jsonError);
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QJsonObject obj = jsonDocument.object();
			if (obj["result"].toString() == "success")
			{
				QVariant strTemp;
				QJsonValue userInfoValue;
				foreach(QString key, obj.keys())
				{
					userInfoValue = obj[key];
					strTemp = userInfoValue.toVariant();
					if (userInfoValue.isArray())
					{
						QVariantList userInfoList = strTemp.toList();
						for (int i = 0; i < userInfoList.size(); i++)
						{
							QString strPinYin;
							QVariantMap listData = userInfoList.at(i).toMap();
							BuddyInfo buddyInfo;
                            buddyInfo.strHttpAvatar = listData["avatar"].toString();
							buddyInfo.strEmail = listData["email"].toString();
							buddyInfo.strMobilePhone = listData["mobilePhone"].toString();
							buddyInfo.strNickName = listData["nickName"].toString();
							buddyInfo.strNote = listData["note"].toString();
							buddyInfo.strPhone = listData["phone"].toString();
							buddyInfo.strSex = listData["sex"].toString();
							buddyInfo.strSign = listData["sign"].toString();
							buddyInfo.nUserId = listData["userId"].toInt();
							buddyInfo.strUserName = listData["userName"].toString();
							buddyInfo.nUserType = listData["userType"].toInt();
                            buddyInfo.strLocalAvatar = gSettingsManager->getUserPath() + "/resource/header/" + listData["userId"].toString() + ".jpg";
							buddyInfo.disableStrangers = listData["disableStrangers"].toInt();
							buddyInfo.msgPrompt = listData["msgPrompt"].toInt();
							//根据名称获取首字母
							if (!buddyInfo.strNote.isEmpty())
								strPinYin = mAlphabeticalSort.GetChineseSpell(buddyInfo.strNote);
							else
								strPinYin = mAlphabeticalSort.GetChineseSpell(buddyInfo.strNickName);
							buddyInfo.strPingYin = strPinYin;
							buddyInfo.BuddyType = 1;

							buddyInfo.strDefaultAvatar = QString(":/PerChat/Resources/person/temp.png");
							msleep(50);
							emit sigParseBuddyInfo(buddyInfo);
						}
					}
				}
			}
		}
	}
}
