# ----------------------------------------------------
# This file is generated by the Qt Visual Studio Tools.
# ------------------------------------------------------

# This is a reminder that you are using a generated .pro file.
# Remove it when you are finished editing this file.
message("You are running qmake on a generated .pro file. This may not work!")


HEADERS += ./BTCWidget/btcmanager.h \
    ./BTCWidget/btcwidget.h \
    ./pwrWidget/changeintoWidget/changeintowidget.h \
    ./pwrWidget/changeintoWidget/changenumwidget.h \
    ./pwrWidget/transferWidget/detailswidget.h \
    ./ETHWidget/ethmanager.h \
    ./ETHWidget/ethturnoutmanager.h \
    ./ETHWidget/ethturnoutwidget.h \
    ./ETHWidget/ethwidget.h \
    ./ewalletmanager.h \
    ./ewalletwidget.h \
    ./ewalletassetswidget.h \
    ./pwrWidget/transferWidget/passwordwidget.h \
    ./pwrWidget/safeWidget/safewidget.h \
    ./starflashwidget.h \
    ./pwrWidget/transferWidget/transfermanager.h \
    ./pwrWidget/transferWidget/transferwidget.h \
    ./childrenWidget/turninwidget.h \
    ./childrenWidget/walletinfowidget.h \
    ./childrenWidget/walletmanager.h \
    ./childrenWidget/walletprikeywidget.h \
    ./pwrWidget/integralCardWidget/carditemwidget.h \
    ./pwrWidget/integralCardWidget/integralcardwidget.h \
    ./pwrWidget/integralCardWidget/chargewidget.h \
    ./pwrWidget/myDeviceWidget/deviceitemwidget.h \
    ./pwrWidget/myDeviceWidget/mydevicewidget.h \
    ./pwrWidget/hostingAccountWidget/hostingaccoutwidget.h \
    ./pwrWidget/hostingAccountWidget/hostingitemwidget.h \
    ./pwrWidget/hostingAccountWidget/hostingrecorditemwidget.h \
    ./pwrWidget/hostingAccountWidget/hostingchargewidget.h \
    ./pwrWidget/hostingAccountWidget/hostingcoinwidget.h \
    $$PWD/EOSWidget/eosmanager.h \
    $$PWD/EOSWidget/eosrecoverywidget.h \
    $$PWD/EOSWidget/eoswidget.h \
    $$PWD/EOSWidget/eosBase/eoshelp.h \
    $$PWD/childrenWidget/recorddetailwidget.h \
    $$PWD/childrenWidget/recordswidget.h \
    $$PWD/childrenWidget/turnoutwidget.h \
    $$PWD/BTCWidget/btcturnoutmanager.h \
    $$PWD/ETHWidget/ethtokenlistwidget.h \
    $$PWD/EOSWidget/eosBase/chain/action.h \
    $$PWD/EOSWidget/eosBase/chain/chainbase.h \
    $$PWD/EOSWidget/eosBase/chain/chainmanager.h \
    $$PWD/EOSWidget/eosBase/chain/eosbytewriter.h \
    $$PWD/EOSWidget/eosBase/chain/eosnewaccount.h \
    $$PWD/EOSWidget/eosBase/chain/packedtransaction.h \
    $$PWD/EOSWidget/eosBase/chain/signedtransaction.h \
    $$PWD/EOSWidget/eosBase/chain/transaction.h \
    $$PWD/EOSWidget/eosBase/chain/transactionextension.h \
    $$PWD/EOSWidget/eosBase/chain/transactionheader.h \
    $$PWD/EOSWidget/eosBase/chain/typeaccountpermissionweight.h \
    $$PWD/EOSWidget/eosBase/chain/typeauthority.h \
    $$PWD/EOSWidget/eosBase/chain/typekeypermissionweight.h \
    $$PWD/EOSWidget/eosBase/chain/typename.h \
    $$PWD/EOSWidget/eosBase/chain/typepermissionlevel.h \
    $$PWD/EOSWidget/eosBase/chain/typewaitweight.h \
    $$PWD/EOSWidget/eosBase/Crypto/aes.h \
    $$PWD/EOSWidget/eosBase/Crypto/aes.hpp \
    $$PWD/EOSWidget/eosBase/Crypto/libbase58.h \
    $$PWD/EOSWidget/eosBase/Crypto/macros.h \
    $$PWD/EOSWidget/eosBase/Crypto/options.h \
    $$PWD/EOSWidget/eosBase/Crypto/rmd160.h \
    $$PWD/EOSWidget/eosBase/Crypto/sha2.h \
    $$PWD/EOSWidget/eosBase/Crypto/sha3.h \
    $$PWD/EOSWidget/eosBase/Crypto/uECC.h \
    $$PWD/EOSWidget/eosBase/ec/eos_key_encode.h \
    $$PWD/EOSWidget/eosBase/ec/sha256.h \
    $$PWD/EOSWidget/eosBase/ec/sha512.h \
    $$PWD/EOSWidget/eosBase/ec/typechainid.h \
    $$PWD/EOSWidget/eosBase/utility/httpclient.h \
    $$PWD/EOSWidget/eosBase/utility/utils.h \
    $$PWD/EOSWidget/eosturnoutmanager.h \
    $$PWD/childrenWidget/recorddetailperwidget.h
SOURCES += ./BTCWidget/btcmanager.cpp \
    ./BTCWidget/btcwidget.cpp \
    ./pwrWidget/changeintoWidget/changeintowidget.cpp \
    ./pwrWidget/changeintoWidget/changenumwidget.cpp \
    ./pwrWidget/transferWidget/detailswidget.cpp \
    ./ETHWidget/ethmanager.cpp \
    ./ETHWidget/ethturnoutmanager.cpp \
    ./ETHWidget/ethturnoutwidget.cpp \
    ./ETHWidget/ethwidget.cpp \
    ./ewalletmanager.cpp \
    ./ewalletwidget.cpp \
    ./ewalletassetswidget.cpp \
    ./pwrWidget/transferWidget/passwordwidget.cpp \
    ./pwrWidget/safeWidget/safewidget.cpp \
    ./starflashwidget.cpp \
    ./pwrWidget/transferWidget/transfermanager.cpp \
    ./pwrWidget/transferWidget/transferwidget.cpp \
    ./childrenWidget/turninwidget.cpp \
    ./childrenWidget/walletinfowidget.cpp \
    ./childrenWidget/walletmanager.cpp \
    ./childrenWidget/walletprikeywidget.cpp \
    ./pwrWidget/integralCardWidget/carditemwidget.cpp \
    ./pwrWidget/integralCardWidget/integralcardwidget.cpp \
    ./pwrWidget/integralCardWidget/chargewidget.cpp \
    ./pwrWidget/myDeviceWidget/deviceitemwidget.cpp \
    ./pwrWidget/myDeviceWidget/mydevicewidget.cpp \
    ./pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp \
    ./pwrWidget/hostingAccountWidget/hostingitemwidget.cpp \
    ./pwrWidget/hostingAccountWidget/hostingrecorditemwidget.cpp \
    ./pwrWidget/hostingAccountWidget/hostingchargewidget.cpp \
    ./pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp \
    $$PWD/EOSWidget/eosmanager.cpp \
    $$PWD/EOSWidget/eosrecoverywidget.cpp \
    $$PWD/EOSWidget/eoswidget.cpp \
    $$PWD/EOSWidget/eosBase/eoshelp.cpp \
    $$PWD/childrenWidget/recorddetailwidget.cpp \
    $$PWD/childrenWidget/recordswidget.cpp \
    $$PWD/childrenWidget/turnoutwidget.cpp \
    $$PWD/BTCWidget/btcturnoutmanager.cpp \
    $$PWD/ETHWidget/ethtokenlistwidget.cpp \
    $$PWD/EOSWidget/eosBase/chain/action.cpp \
    $$PWD/EOSWidget/eosBase/chain/chainmanager.cpp \
    $$PWD/EOSWidget/eosBase/chain/eosbytewriter.cpp \
    $$PWD/EOSWidget/eosBase/chain/eosnewaccount.cpp \
    $$PWD/EOSWidget/eosBase/chain/packedtransaction.cpp \
    $$PWD/EOSWidget/eosBase/chain/signedtransaction.cpp \
    $$PWD/EOSWidget/eosBase/chain/transaction.cpp \
    $$PWD/EOSWidget/eosBase/chain/transactionextension.cpp \
    $$PWD/EOSWidget/eosBase/chain/transactionheader.cpp \
    $$PWD/EOSWidget/eosBase/chain/typeaccountpermissionweight.cpp \
    $$PWD/EOSWidget/eosBase/chain/typeauthority.cpp \
    $$PWD/EOSWidget/eosBase/chain/typekeypermissionweight.cpp \
    $$PWD/EOSWidget/eosBase/chain/typename.cpp \
    $$PWD/EOSWidget/eosBase/chain/typepermissionlevel.cpp \
    $$PWD/EOSWidget/eosBase/chain/typewaitweight.cpp \
    $$PWD/EOSWidget/eosBase/Crypto/base58.cpp \
    $$PWD/EOSWidget/eosBase/Crypto/aes.c \
    $$PWD/EOSWidget/eosBase/Crypto/rmd160.c \
    $$PWD/EOSWidget/eosBase/Crypto/sha2.c \
    $$PWD/EOSWidget/eosBase/Crypto/sha3.c \
    $$PWD/EOSWidget/eosBase/Crypto/uECC.c \
    $$PWD/EOSWidget/eosBase/ec/eos_key_encode.cpp \
    $$PWD/EOSWidget/eosBase/ec/sha256.cpp \
    $$PWD/EOSWidget/eosBase/ec/sha512.cpp \
    $$PWD/EOSWidget/eosBase/ec/typechainid.cpp \
    $$PWD/EOSWidget/eosBase/utility/httpclient.cpp \
    $$PWD/EOSWidget/eosBase/utility/utils.cpp \
    $$PWD/EOSWidget/eosturnoutmanager.cpp \
    $$PWD/childrenWidget/recorddetailperwidget.cpp
FORMS += ./BTCWidget/btcwidget.ui \
    ./pwrWidget/changeintoWidget/changeintowidget.ui \
    ./pwrWidget/changeintoWidget/changenumwidget.ui \
    ./pwrWidget/transferWidget/detailswidget.ui \
    ./ETHWidget/ethturnoutwidget.ui \
    ./ETHWidget/ethwidget.ui \
    ./ewalletwidget.ui \
    ./ewalletassetswidget.ui \
    ./pwrWidget/transferWidget/passwordwidget.ui \
    ./pwrWidget/safeWidget/safewidget.ui \
    ./starflashwidget.ui \
    ./pwrWidget/transferWidget/transferwidget.ui \
    ./childrenWidget/turninwidget.ui \
    ./childrenWidget/walletinfowidget.ui \
    ./childrenWidget/walletmanager.ui \
    ./childrenWidget/walletprikeywidget.ui \
    ./pwrWidget/integralCardWidget/carditemwidget.ui \
    ./pwrWidget/integralCardWidget/integralcardwidget.ui \
    ./pwrWidget/integralCardWidget/chargewidget.ui \
    ./pwrWidget/myDeviceWidget/deviceitemwidget.ui \
    ./pwrWidget/myDeviceWidget/mydevicewidget.ui \
    ./pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui \
    ./pwrWidget/hostingAccountWidget/hostingitemwidget.ui \
    ./pwrWidget/hostingAccountWidget/hostingrecorditemwidget.ui \
    ./pwrWidget/hostingAccountWidget/hostingchargewidget.ui \
    ./pwrWidget/hostingAccountWidget/hostingcoinwidget.ui \
    $$PWD/EOSWidget/eosrecoverywidget.ui \
    $$PWD/EOSWidget/eoswidget.ui \
    $$PWD/childrenWidget/recorddetailwidget.ui \
    $$PWD/childrenWidget/recordswidget.ui \
    $$PWD/childrenWidget/turnoutwidget.ui \
    $$PWD/ETHWidget/ethtokenlistwidget.ui \
    $$PWD/childrenWidget/recorddetailperwidget.ui
