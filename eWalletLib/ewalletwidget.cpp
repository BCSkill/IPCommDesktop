﻿#include "ewalletwidget.h"
#include "ui_ewalletwidget.h"

extern QString gThemeStyle;

EWalletWidget::EWalletWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::EWalletWidget();
	ui->setupUi(this);

	initGif();

	ui->scrollArea->installEventFilter(this);
	ui->turnInWidget->installEventFilter(this);
	ui->turnOutWidget->installEventFilter(this);
	ui->safeWidget->installEventFilter(this);
	ui->profileWidget->installEventFilter(this);
	ui->cardWidget->installEventFilter(this);
	ui->myDeviceWidget->installEventFilter(this);
	ui->hostingWidget->installEventFilter(this);

	intoWidget = new ChangeIntoWidget;
	transferWidget = new TransferWidget;
	safeWidget = new SafeWidget;
	recordsWidget = new RecordsWidget;
	cardWidget = new IntegralCardWidget;
	deviceWidget = new MyDeviceWidget;
	hostingWidget = new HostingAccoutWidget;

	ui->stackedWidget->addWidget(intoWidget);
	ui->stackedWidget->addWidget(transferWidget);
	ui->stackedWidget->addWidget(safeWidget);
	ui->stackedWidget->addWidget(recordsWidget);
	ui->stackedWidget->addWidget(cardWidget);
	ui->stackedWidget->addWidget(deviceWidget);
	ui->stackedWidget->addWidget(hostingWidget);

	connect(ui->assetBtn, SIGNAL(clicked()), this, SLOT(slotSwitchAssetsPage()));
	connect(ui->pwrBtn, SIGNAL(clicked()), this, SLOT(slotSwitchPWRPage()));
	connect(ui->ethBtn, SIGNAL(clicked()), this, SLOT(slotSwitchETHPage()));
	connect(ui->btcBtn, SIGNAL(clicked()), this, SLOT(slotSwitchBTCPage()));
	connect(ui->eosBtn, SIGNAL(clicked()), this, SLOT(slotSwitchEOSPage()));
	connect(ui->optionBtn, SIGNAL(clicked()), this, SLOT(slotOpenMenu()));

	connect(recordsWidget, SIGNAL(sigTurnIn()), this, SLOT(slotTurnInWidget()));
	connect(recordsWidget, SIGNAL(sigTurnOut()), this, SLOT(slotTurnOutWidget()));

	ui->stackedWidget->setCurrentWidget(intoWidget);
	if (gThemeStyle == "Blue")
	{
		ui->label_9->setStyleSheet("color:white");
	}
	else
	{
		//白色主题暂不高亮
	}


	QFile file(":/QSS/Resources/QSS/eWalletLib/ewalletwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();
}

EWalletWidget::~EWalletWidget()
{
	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

void EWalletWidget::addWidget(QWidget *page)
{
	ui->tabWidget->addWidget(page);
	ui->tabWidget->setCurrentWidget(page);
}

void EWalletWidget::initGif()
{
	ui->gifLabel->installEventFilter(this);

	gifState = FRONT;

	QMovie *gif = new QMovie(this);;
	gif->setFileName(":/ewallet/Resources/ewallet/pwrWidget/pwc_z.gif");
	gif->setScaledSize(ui->gifLabel->size());
	ui->gifLabel->setMovie(gif);
	gif->start();
}

bool EWalletWidget::eventFilter(QObject *obj, QEvent *event)
{
	if (obj == ui->gifLabel && event->type() == QEvent::MouseButtonPress)
	{
		if (gifState == FRONT)
		{
			gifState = FRONTOBACK;

			QMovie *gif = new QMovie(this);;
			gif->setFileName(":/ewallet/Resources/ewallet/pwrWidget/frontoback.gif");
			gif->setScaledSize(ui->gifLabel->size());
			ui->gifLabel->setMovie(gif);
			gif->start();
			connect(gif, SIGNAL(finished()), this, SLOT(slotGifToBack()));
		}
		if (gifState == BACK)
		{
			gifState = BACKTOFRONT;

			QMovie *gif = new QMovie(this);;
			gif->setFileName(":/ewallet/Resources/ewallet/pwrWidget/backtofront.gif");
			gif->setScaledSize(ui->gifLabel->size());
			ui->gifLabel->setMovie(gif);
			gif->start();
			connect(gif, SIGNAL(finished()), this, SLOT(slotGifToFront()));
		}
	}

	if (obj == ui->scrollArea && event->type() == QEvent::Enter)
	{
		ui->scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	}
	if (obj == ui->scrollArea && event->type() == QEvent::Leave)
	{
		ui->scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	}
	if (obj == ui->myDeviceWidget && event->type() == QEvent::MouseButtonPress)
	{
		ui->stackedWidget->setCurrentWidget(deviceWidget);
		deviceWidget->getDeviceList();
		slotClearChecked();
		if (gThemeStyle == "Blue")
		{
			ui->label_31->setStyleSheet("color:#ffffff");
		}
		else
		{

		}
		emit sigClearChecked();
	}
	if (obj == ui->hostingWidget && event->type() == QEvent::MouseButtonPress)
	{
		ui->stackedWidget->setCurrentWidget(hostingWidget);
		hostingWidget->getHostingAccount();
		slotClearChecked();
		if (gThemeStyle == "Blue")
		{
			ui->label_28->setStyleSheet("color:#ffffff");
		}
		else
		{

		}
		emit sigClearChecked();
	}	
	if (obj == ui->cardWidget && event->type() == QEvent::MouseButtonPress)
	{
		ui->stackedWidget->setCurrentWidget(cardWidget);
		cardWidget->getCard();
		emit sigRefreshAccount(); 
		slotClearChecked();
		if (gThemeStyle == "Blue")
		{
			ui->label_18->setStyleSheet("color:#ffffff");
		}
		else
		{

		}
		emit sigClearChecked();
	}
	if (obj == ui->turnOutWidget && event->type() == QEvent::MouseButtonPress)
	{
		ui->stackedWidget->setCurrentWidget(transferWidget);
		slotClearChecked();
		if (gThemeStyle == "Blue")
		{
			ui->label_8->setStyleSheet("color:#ffffff");
		}
		else
		{

		}
		emit sigClearChecked();
	}
	if (obj == ui->turnInWidget && event->type() == QEvent::MouseButtonPress)
	{
		ui->stackedWidget->setCurrentWidget(intoWidget);
		slotClearChecked();
		if (gThemeStyle == "Blue")
		{
			ui->label_9->setStyleSheet("color:#ffffff");
		}
		else
		{

		}
		emit sigClearChecked();
	}
	if (obj == ui->safeWidget && event->type() == QEvent::MouseButtonPress)
	{
		ui->stackedWidget->setCurrentWidget(safeWidget);
		slotClearChecked();
		if (gThemeStyle == "Blue")
		{
			ui->label_19->setStyleSheet("color:#ffffff");
		}
		else
		{

		}
		emit sigClearChecked();
	}
	if (obj == ui->profileWidget && event->type() == QEvent::MouseButtonPress)
	{
		ui->stackedWidget->setCurrentWidget(recordsWidget);
		emit sigGetRecords();
		slotClearChecked();
		if (gThemeStyle == "Blue")
		{
			ui->label_13->setStyleSheet("color:#ffffff");
		}
		else
		{

		}
		emit sigClearChecked();
	}
	
	return QWidget::eventFilter(obj, event);
}

void EWalletWidget::slotGifToBack()
{
	gifState = BACK;
	QMovie *gif = new QMovie(this);;
	gif->setFileName(":/ewallet/Resources/ewallet/pwrWidget/pwc_f.gif");
	gif->setScaledSize(ui->gifLabel->size());
	ui->gifLabel->setMovie(gif);
	gif->start();
}

void EWalletWidget::slotGifToFront()
{
	gifState = FRONT;
	QMovie *gif = new QMovie(this);;
	gif->setFileName(":/ewallet/Resources/ewallet/pwrWidget/pwc_z.gif");
	gif->setScaledSize(ui->gifLabel->size());
	ui->gifLabel->setMovie(gif);
	gif->start();
}

StarFlashWidget * EWalletWidget::getStarWidget()
{
	return ui->starWidget;
}

ChangeIntoWidget * EWalletWidget::getIntoWidget()
{
	return intoWidget;
}

TransferWidget * EWalletWidget::getTransWidget()
{
	return transferWidget;
}

SafeWidget * EWalletWidget::getSafeWidget()
{
	return safeWidget;
}

RecordsWidget * EWalletWidget::getRecordsWidget()
{
	return recordsWidget;
}

IntegralCardWidget * EWalletWidget::getCardWidget()
{
	return cardWidget;
}

MyDeviceWidget * EWalletWidget::getDeviceWidget()
{
	return deviceWidget;
}

HostingAccoutWidget * EWalletWidget::getHostingWidget()
{
	return hostingWidget;
}

void EWalletWidget::setEnergySum(QString energy)
{
	ui->energyLabel->setText(energy);
}

void EWalletWidget::switchDeviceWidget()
{
	ui->pwrBtn->setChecked(true);
	ui->tabWidget->setCurrentIndex(0);
	ui->stackedWidget->setCurrentWidget(deviceWidget);
	deviceWidget->getDeviceList();
}

void EWalletWidget::slotSwitchPWRPage()
{
	ui->tabWidget->setCurrentIndex(0);
}

void EWalletWidget::slotSwitchETHPage()
{
	ui->tabWidget->setCurrentIndex(1);
}

void EWalletWidget::slotSwitchBTCPage()
{
	ui->tabWidget->setCurrentIndex(2);
}

void EWalletWidget::slotSwitchEOSPage()
{
	ui->tabWidget->setCurrentIndex(3);
}


void EWalletWidget::slotOpenMenu()
{
	if (ui->ethBtn->isChecked())
		emit sigETHMenu();
	if (ui->btcBtn->isChecked())
		emit sigBTCMenu();
	if (ui->eosBtn->isChecked())
		emit sigEOSMenu();
}

QStackedWidget * EWalletWidget::getStackedWidget()
{
	return ui->stackedWidget;
}

void EWalletWidget::slotTurnInWidget()
{
	ui->stackedWidget->setCurrentWidget(intoWidget);
}

void EWalletWidget::slotTurnOutWidget()
{
	ui->stackedWidget->setCurrentWidget(transferWidget);
}

void EWalletWidget::slotSwitchAssetsPage()
{
	ui->tabWidget->setCurrentIndex(4);
}

void EWalletWidget::slotClearChecked()
{
	if (gThemeStyle == "Blue")
	{
		ui->label_13->setStyleSheet("color: #4c6889;");
		ui->label_20->setStyleSheet("color: #4c6889;");
		ui->label_19->setStyleSheet("color: #4c6889;");
		ui->label_18->setStyleSheet("color: #4c6889;");
		ui->label_26->setStyleSheet("color: #4c6889;");
		ui->label_28->setStyleSheet("color: #4c6889;");
		ui->label_31->setStyleSheet("color: #4c6889;");
		ui->label_8->setStyleSheet("color: #e4ba7b;");
		ui->label_9->setStyleSheet("color: #e4ba7b;");
	}
	else
	{

	}

}






