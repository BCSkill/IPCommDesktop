﻿#include "eosmanager.h"
#ifndef Q_OS_WIN
#include "inline_mac.h"
#endif
#include "QStringLiteralBak.h"
#include "globalmanager.h"

extern OPDataManager  *gOPDataManager;

EOSManager::EOSManager(QObject *parent)
	: QObject(parent)
{
	eosWidget = new EOSWidget;

	turnInWidget = new TurnInWidget;
	turnOutWidget = new TurnOutWidget;
	recordsWidget = new RecordsWidget;

	m_pCurrentWallet = NULL;
	m_strBalance = "0";

	UserInfo user = gDataManager->getUserInfo();
	turnOutWidget->setPassWord(user.strLoginPWD);

	connect(eosWidget, SIGNAL(sigRecoveryWallet()), this, SLOT(slotShowInfoWidget()));
	connect(eosWidget, SIGNAL(sigWalletManager(QString)), this, SLOT(slotWalletManager(QString)));
	connect(eosWidget, SIGNAL(sigTurnInWidget()), this, SLOT(slotTurnInWidget()));
	connect(eosWidget, SIGNAL(sigProfileWidget()), this, SLOT(slotProfileWidget()));
	connect(eosWidget, SIGNAL(sigTurnOutWidget()), this, SLOT(slotTurnOutWidget()));
	connect(eosWidget, SIGNAL(sigClearChecked()), this, SIGNAL(sigClearChecked()));

	connect(recordsWidget, SIGNAL(sigTurnIn()), this, SLOT(slotTurnInWidget()));
	connect(recordsWidget, SIGNAL(sigTurnOut()), this, SLOT(slotTurnOutWidget()));

	connect(turnOutWidget, SIGNAL(sigTurnOut(QString, QString, QString)),
		this, SLOT(slotTurnOutManager(QString, QString, QString)));
}

EOSManager::~EOSManager()
{
}

EOSWidget * EOSManager::getEOSWidget()
{
	return eosWidget;
}

void EOSManager::setWallets(QList<ChildrenWallet> walletList)
{
	this->walletList = walletList;

	switchWallet();
}

void EOSManager::switchWallet(ChildrenWallet* wallet /*= NULL*/)
{
	if (wallet == NULL)
	{
		if (walletList.count() > 0)
		{
			wallet = &walletList.first();
		}
	}

	if (wallet == NULL)
		eosWidget->initShow(false);
	else
	{
		if (wallet->balance.isEmpty())
		{
			OPRequestShareLib *request = new OPRequestShareLib;
			connect(request, SIGNAL(sigEOSBalance(QString)), this, SLOT(slotGetBalance(QString)));
			connect(request, SIGNAL(sigEOSBalance(QString)), request, SLOT(deleteLater()));
			request->getEOSBalance(wallet->accountAddress);
		}
		m_pCurrentWallet = wallet;
		turnInWidget->setTurnInData(*wallet);
		turnOutWidget->setTurnOutData(*wallet);
		eosWidget->setIndexWallet(*wallet);

		eosWidget->initShow(true);
	}
}

void EOSManager::slotGetBalance(QString balance)
{
	if (balance == "failed")
	{
		balance = "0";
	}

	m_strBalance = balance;
	eosWidget->setBalance(balance);
	turnOutWidget->setBalance(balance);
}

void EOSManager::slotShowInfoWidget()
{
	WalletInfoWidget *infoWidget = new WalletInfoWidget(eosWidget);
	connect(infoWidget, SIGNAL(sigSendWalletInfo(QString, QString)),
		this, SLOT(slotShowRecWidget(QString, QString)));

	infoWidget->show();
}

void EOSManager::slotShowRecWidget(QString name, QString ID)
{
	this->walletName = name;
	this->iconID = ID;

	EOSRecoveryWidget *recoveryWidget = new EOSRecoveryWidget(eosWidget);
	connect(recoveryWidget, SIGNAL(sigEosPubKey(QString, QString, QString, QString)),
		this, SLOT(slotRecovryWallet(QString, QString, QString, QString)));
	recoveryWidget->show();
}

void EOSManager::slotRecovryWallet(QString ownPubKey, QString ownPriKey,
	QString actPubKey, QString actPriKey)
{
	this->ownerPubKey = ownPubKey;
	this->ownerPriKey = ownPriKey;
	this->activePubKey = actPubKey;
	this->activePriKey = actPriKey;

	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigRecoveryEos(QString)), this, SLOT(slotRecoveryResult(QString)));
	connect(request, SIGNAL(sigRecoveryEos(QString)), request, SLOT(deleteLater()));
	request->getRecoveryEosWallet(ownerPubKey, activePubKey);
}

void EOSManager::slotRecoveryResult(QString result)
{
	if (result == "failed")
	{
		IMessageBox::tip(eosWidget, tr("Notice"), tr("Account Verification Failed!"));
	}
	else
	{
		QString address = result.split("_").first();
		QString accountIcon = gDataManager->getAppConfigInfo().PanServerDownloadURL + iconID + "/download";

		QString strOwnerPriKey = gOPDataManager->encryptAES(ownerPriKey.toUtf8(), walletKey);
		QString strActivePriKey = gOPDataManager->encryptAES(activePriKey.toUtf8(), walletKey);

		OPRequestShareLib *request = new OPRequestShareLib;
		connect(request, SIGNAL(sigAddChildrenWallet(bool)), this, SLOT(slotRecoveryFinished(bool)));
		connect(request, SIGNAL(sigAddChildrenWallet(bool)), request, SLOT(deleteLater()));
		request->addEosWallet(address, ownerPubKey, strOwnerPriKey,
			mainAddress, walletName, accountIcon, "EOS", 1, 
			activePubKey, strActivePriKey, result);
	}
}

void EOSManager::setMainAddress(QString address)
{
	this->mainAddress = address;
}

void EOSManager::setPriKey(QString key)
{
	this->walletKey = key;
}

void EOSManager::slotRecoveryFinished(bool success)
{
	if (success)
	{
		IMessageBox::tip(eosWidget, tr("Notice"), tr("EOS Wallet Restored Successfully!"));
		emit sigRefresh();
	}
	else
		IMessageBox::tip(eosWidget, tr("Notice"), tr("Failed to Restore EOS Wallet!"));
}

void EOSManager::slotWalletManager(QString address)
{
	for (int i = 0; i < walletList.count(); i++)
	{
		ChildrenWallet wallet = walletList.at(i);
		if (wallet.eosAccountNames == address)
		{
			WalletManager *walletManager = new WalletManager(eosWidget);
			connect(walletManager, SIGNAL(sigSetWalletInfo(QString, QString, QString, QString, QString)),
				this, SLOT(slotSetWalletInfo(QString, QString, QString, QString, QString)));
			connect(walletManager, SIGNAL(sigDeleteWallet(QString, QString, QString)),
				this, SLOT(slotDeleteWallet(QString, QString, QString)));
			connect(walletManager, SIGNAL(sigAccountChanged(QString)), this, SLOT(slotAccountChanged(QString)));
			walletManager->setWallet(wallet);
			walletManager->show();

			break;
		}
	}
}

void EOSManager::slotSetWalletInfo(QString accountAddress, QString mainAccountAddress, QString accountName, QString accountIcon, QString blockChainName)
{
	for (int i = 0; i < walletList.count(); i++)
	{
		ChildrenWallet wallet = walletList.at(i);
		if (wallet.accountAddress == accountAddress)
		{
			if (!accountName.isEmpty())
				wallet.accountName = accountName;
			if (!accountIcon.isEmpty())
				wallet.accountIcon = accountIcon;

			walletList.removeAt(i);
			walletList.append(wallet);

			switchWallet(&wallet);
			break;
		}
	}

	OPRequestShareLib *request = new OPRequestShareLib;
	request->setWalletInfo(accountAddress, mainAccountAddress, accountName, accountIcon, blockChainName);
	delete request;
}

void EOSManager::slotDeleteWallet(QString accountAddress, QString mainAccountAddress, QString blockChainName)
{
	for (int j = 0; j < walletList.count(); j++)
	{
		ChildrenWallet wallet = walletList.at(j);
		if (wallet.accountAddress == accountAddress)
		{
			walletList.removeAt(j);
			break;
		}
	}

	switchWallet();

	//发送网络请求。
	OPRequestShareLib *request = new OPRequestShareLib;
	request->deleteChildrenWallet(accountAddress, mainAccountAddress, blockChainName);
	delete request;

	IMessageBox::tip(NULL, tr("Notice"), tr("Wallet Deleted Successfully!"));
}

void EOSManager::slotAccountChanged(QString account)
{
	QString accounts = eosWidget->getEOSAccounts();

	for (int j = 0; j < walletList.count(); j++)
	{
		ChildrenWallet wallet = walletList.at(j);
		if (wallet.eosAccountNames == accounts)
		{
			walletList.removeAt(j);
			wallet.accountAddress = account;
			walletList.append(wallet);

			switchWallet(&wallet);
		}
	}
}

void EOSManager::slotOpenMenu()
{
	//主菜单。
	QMenu *menu = new QMenu();

	QAction *refreshAciton = new QAction(tr("Refresh Wallet"), menu);
	refreshAciton->setIcon(QIcon(":/ewallet/Resources/ewallet/openwallet_changewallet.png"));
	connect(refreshAciton, SIGNAL(triggered()), this, SIGNAL(sigRefresh()));
	menu->addAction(refreshAciton);
	refreshAciton->deleteLater();

	QAction *inAction = new QAction(tr("Import Wallet"), menu);
	inAction->setIcon(QIcon(":/ewallet/Resources/ewallet/openwallet_importwallet.png"));
	connect(inAction, SIGNAL(triggered()), this, SLOT(slotShowInfoWidget()));
	menu->addAction(inAction);
	inAction->deleteLater();

	QFile style(":/QSS/Resources/QSS/eWalletLib/eosmanager.qss");
	style.open(QFile::ReadOnly);
	QString sheet = QLatin1String(style.readAll());
	//二级菜单。
	QMenu *childMenu = new QMenu();
	childMenu->setTitle(tr("Switch Wallet ") + QString("(%1)").arg(walletList.count()));
	childMenu->setIcon(QIcon(":/ewallet/Resources/ewallet/openwallet_changewallet.png"));

#ifdef Q_OS_WIN
    childMenu->setStyleSheet(sheet);
#else
    childMenu->setStyleSheet("QMenu{background-color: #72a4d6;color: #042439;}QMenu::item:selected{color: white;}QMenu::item:checked{color: white;}");
#endif
	for (int i = 0; i < walletList.count(); i++)
	{
		ChildrenWallet wallet = walletList.at(i);

		QAction *inAction = new QAction(wallet.accountName, childMenu);
		inAction->setObjectName(wallet.eosAccountNames);
		QPixmap pix;
#ifdef Q_OS_WIN
        QString imagePath = gSettingsManager->getUserPath()+"/resource/" + wallet.id.remove("-") + ".png";
#else
        QString imagePath = gSettingsManager->getUserPath() + "/resource/" + wallet.id.remove("-") + ".png";
#endif

		if (QFile(imagePath).exists())
		{
			QByteArray bytearray;
			QFile file(imagePath);
			if (file.open(QIODevice::ReadOnly))
			{
				bytearray = file.readAll();
				file.close();
			}
			pix.loadFromData(bytearray);
		}
		if (pix.isNull())
			pix.load(":/ewallet/Resources/ewallet/defaultIcon.png");

		inAction->setIcon(QIcon(pix));
		inAction->setCheckable(true);
		if (inAction->objectName() == eosWidget->getEOSAccounts())
		{
			inAction->setChecked(true);
		}
		else
		{
			inAction->setChecked(false);
		}

		connect(inAction, SIGNAL(triggered()), this, SLOT(slotSwitchWallet()));
		childMenu->addAction(inAction);
		inAction->deleteLater();
	}

	menu->addMenu(childMenu);
#ifdef Q_OS_WIN
    menu->setStyleSheet(sheet);
#else
    menu->setStyleSheet("QMenu{background-color: #72a4d6;color: #042439;}QMenu::item:selected{color: white;}");
#endif
	style.close();
	menu->move(QCursor::pos());
	menu->show();
	QPoint menuPos = QCursor::pos();
	menuPos.setY(menuPos.y() - menu->height());
	menu->move(menuPos);
	menu->exec();
}

void EOSManager::slotSwitchWallet()
{
	QAction *action = qobject_cast<QAction *>(sender());

	QString address = action->objectName();

	for (int i = 0; i < walletList.count(); i++)
	{
		ChildrenWallet wallet = walletList.at(i);
		if (wallet.eosAccountNames == address)
		{
			switchWallet(&wallet);

			break;
		}
	}
}

void EOSManager::setStackedWidget(QStackedWidget *widget)
{
	this->stackedWidget = widget;

	stackedWidget->addWidget(turnInWidget);
	stackedWidget->addWidget(turnOutWidget);
	stackedWidget->addWidget(recordsWidget);
}

void EOSManager::slotTurnInWidget()
{
	stackedWidget->setCurrentWidget(turnInWidget);
}

void EOSManager::slotTurnOutWidget()
{
	stackedWidget->setCurrentWidget(turnOutWidget);
}

void EOSManager::slotProfileWidget()
{
	recordsWidget->setTitle("EOS");
	stackedWidget->setCurrentWidget(recordsWidget);
	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigRecords(QList<RecordInfo>)), recordsWidget, SLOT(slotRecords(QList<RecordInfo>)));
	connect(request, SIGNAL(sigRecords(QList<RecordInfo>)), request, SLOT(deleteLater()));
	request->getEOSRecords(eosWidget->getAddress(), 1, 100, 1);
}

void EOSManager::slotTurnOutManager(QString userAddress, QString otherAddress, QString value)
{
	EOSTurnOutManager *turnOutManager = new EOSTurnOutManager(turnOutWidget);

	for (int j = 0; j < walletList.count(); j++)
	{
		ChildrenWallet wallet = walletList.at(j);
		if (wallet.eosAccountNames == eosWidget->getEOSAccounts())
		{
			turnOutManager->setPriKey(wallet.eosActivePrivateKey);
			break;
		}
	}

	turnOutManager->setTurnOutData(userAddress, otherAddress, value);
}

void EOSManager::OtherWindow(QList<ChildrenWallet> BTCList)
{
	if (m_pCurrentWallet == NULL)
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("No EOS Wallet!"));
		return;
	}
	if (BTCList.size() > 0)
	{
		TurnOutWidget * pWidget = new TurnOutWidget();

		UserInfo user = gDataManager->getUserInfo();
		pWidget->setBalance(m_strBalance);
		pWidget->setTurnOutData(*m_pCurrentWallet);
		pWidget->setPassWord(user.strLoginPWD);
		connect(pWidget, SIGNAL(sigTurnOut(QString, QString)), this, SLOT(slotTurnOutManager(QString, QString)));

		pWidget->InitAsWindow();
		pWidget->setBuddyAddress(BTCList);
		pWidget->show();
	}
	else
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("The opposite side have no account!"));
	}
}

void EOSManager::slotClearChecked()
{
	eosWidget->slotClearChecked();
}
