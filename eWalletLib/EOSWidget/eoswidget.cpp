﻿#include "eoswidget.h"
#include "ui_eoswidget.h"
#include "globalmanager.h"

#ifdef Q_OS_MAC
#include "mycommonstyle.h"
#include "inline_mac.h"
#endif
EOSWidget::EOSWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::EOSWidget();
	ui->setupUi(this);

	ui->iconLabel->setShape(roundRect);
#ifdef Q_OS_MAC
    ui->followList->setStyle(new MyProxyStyle);
    ui->allList->setStyle(new MyProxyStyle);
#endif

	QFile file(":/QSS/Resources/QSS/eWalletLib/eoswidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->recoveryBtn, SIGNAL(clicked()), this, SIGNAL(sigRecoveryWallet()));
	connect(ui->turnInBtn, SIGNAL(clicked()), this, SIGNAL(sigTurnInWidget()));
	connect(ui->turnOutBtn, SIGNAL(clicked()), this, SIGNAL(sigTurnOutWidget()));
	connect(ui->copyBtn, SIGNAL(clicked()), this, SLOT(slotCopyAddress()));
	connect(ui->profileBtn, SIGNAL(clicked()), this, SIGNAL(sigProfileWidget()));
	//按钮点击时发送消息 通知其他widget取消选中
	connect(ui->turnInBtn, SIGNAL(clicked()), this, SIGNAL(sigClearChecked()));
	connect(ui->turnOutBtn, SIGNAL(clicked()), this, SIGNAL(sigClearChecked()));
	connect(ui->profileBtn, SIGNAL(clicked()), this, SIGNAL(sigClearChecked()));

	ui->iconLabel->installEventFilter(this);

	ui->allList->hide();
	ui->tipLabel->hide();

	ui->profileBtn->setAutoExclusive(true);
	ui->turnInBtn->setAutoExclusive(true);
	ui->turnOutBtn->setAutoExclusive(true);

}

EOSWidget::~EOSWidget()
{
	delete ui;
}

void EOSWidget::initShow(bool hasWallet)
{
	if (hasWallet)
	{
		ui->firstWidget->hide();
		ui->mainWidget->show();
	}
	else
	{
		ui->mainWidget->hide();
		ui->firstWidget->show();
	}
}

void EOSWidget::setIndexWallet(ChildrenWallet wallet)
{
	this->wallet = wallet;

	ui->nameLabel->setText(wallet.accountName);
	ui->addressLabel->setText(wallet.accountAddress);
	ui->addressLabel->setObjectName(wallet.eosAccountNames);
	ui->numberLabel->setText(wallet.balance);

#ifdef Q_OS_WIN
        QString imagePath = gSettingsManager->getUserPath() + "/resource/" + wallet.id.remove("-") + ".png";
#else
        QString imagePath = gSettingsManager->getUserPath() + "/resource/" + wallet.id.remove("-") + ".png";
#endif

	if (QFile(imagePath).exists())
	{
		QByteArray bytearray;
		QFile file(imagePath);
		if (file.open(QIODevice::ReadOnly))
		{
			bytearray = file.readAll();
			file.close();
		}
		QPixmap pix;
		pix.loadFromData(bytearray);

		if (pix.isNull())
			pix.load(":/ewallet/Resources/ewallet/defaultIcon.png");

		ui->iconLabel->setPixmap(pix);
	}
	else
	{
		if (wallet.accountIcon.isEmpty())
			ui->iconLabel->setPixmap(QPixmap(":/ewallet/Resources/ewallet/defaultIcon.png"));
		else
		{
			HttpNetWork::HttpDownLoadFile *http = new HttpNetWork::HttpDownLoadFile;
			connect(http, SIGNAL(sigDownFinished(bool)), this, SLOT(slotDownloadIcon(bool)));
			http->setData(QVariant(imagePath));
			http->StartDownLoadFile(wallet.accountIcon, imagePath);
		}
	}

	ui->followList->clear();
	EWalletItemFollow *followWidget = new EWalletItemFollow;
	followWidget->setData(":/ewallet/Resources/ewallet/eos.png", "EOS", wallet.balance, "", "");
	QListWidgetItem *followItem = new QListWidgetItem;
	followItem->setSizeHint(QSize(240, 50));
	ui->followList->addItem(followItem);
	ui->followList->setItemWidget(followItem, followWidget);
}

void EOSWidget::slotDownloadIcon(bool success)
{
	if (success)
	{
		HttpNetWork::HttpDownLoadFile *download = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());
		if (download)
		{
			QString imagePath = download->getData().toString();

			QByteArray bytearray;
			QFile file(imagePath);
			if (file.open(QIODevice::ReadOnly))
			{
				bytearray = file.readAll();
				file.close();
			}

			QPixmap pix;
			pix.loadFromData(bytearray);

			if (pix.isNull())
				pix.load(":/ewallet/Resources/ewallet/defaultIcon.png");

			ui->iconLabel->setPixmap(pix);
		}
	}
	else
	{
		ui->iconLabel->setPixmap(QPixmap(":/ewallet/Resources/ewallet/defaultIcon.png"));
	}
}

void EOSWidget::setBalance(QString balance)
{
	ui->numberLabel->setText(balance);

	ui->followList->clear();
	EWalletItemFollow *followWidget = new EWalletItemFollow;
	followWidget->setData(":/ewallet/Resources/ewallet/eos.png", "EOS", balance, "", "");
	QListWidgetItem *followItem = new QListWidgetItem;
	followItem->setSizeHint(QSize(240, 50));
	ui->followList->addItem(followItem);
	ui->followList->setItemWidget(followItem, followWidget);
}

void EOSWidget::slotCopyAddress()
{
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(ui->addressLabel->text());

	if (ui->tipLabel->isHidden())
	{
		ui->tipLabel->show();
		QTimer::singleShot(2000, ui->tipLabel, SLOT(hide()));
	}
}

bool EOSWidget::eventFilter(QObject *obj, QEvent *event)
{
	if (obj == ui->iconLabel)
	{
		if (event->type() == QEvent::MouseButtonPress)
		{
			emit sigWalletManager(ui->addressLabel->objectName());
		}
	}

	return QWidget::eventFilter(obj, event);
}

QString EOSWidget::getAddress()
{
	return ui->addressLabel->text();
}

QString EOSWidget::getEOSAccounts()
{
	return ui->addressLabel->objectName();
}

void EOSWidget::slotClearChecked()
{
	ui->profileBtn->setAutoExclusive(false);
	ui->turnInBtn->setAutoExclusive(false);
	ui->turnOutBtn->setAutoExclusive(false);

	ui->profileBtn->setChecked(false);
	ui->turnInBtn->setChecked(false);
	ui->turnOutBtn->setChecked(false);

	ui->profileBtn->setAutoExclusive(true);
	ui->turnInBtn->setAutoExclusive(true);
	ui->turnOutBtn->setAutoExclusive(true);
}
