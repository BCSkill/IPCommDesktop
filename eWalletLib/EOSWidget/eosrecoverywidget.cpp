﻿#include "eosrecoverywidget.h"
#include "ui_eosrecoverywidget.h"
#include "QStringLiteralBak.h"

EOSRecoveryWidget::EOSRecoveryWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::EOSRecoveryWidget();
	ui->setupUi(this);

	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog);
	setWindowModality(Qt::WindowModal);
	setAttribute(Qt::WA_DeleteOnClose);
	setAttribute(Qt::WA_TranslucentBackground);
	setWindowIcon(QIcon(":/Login/Resources/login/system.ico"));

	QFile file(":/QSS/Resources/QSS/eWalletLib/eosrecoverywidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->closeBtn, SIGNAL(clicked()), this, SLOT(close()));
	connect(ui->enterBtn, SIGNAL(clicked()), this, SLOT(slotEnter()));
}

EOSRecoveryWidget::~EOSRecoveryWidget()
{
	delete ui;
}

//鼠标事件的处理。
void EOSRecoveryWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void EOSRecoveryWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void EOSRecoveryWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

void EOSRecoveryWidget::slotEnter()
{
	QString ownString = ui->OWNEdit->toPlainText();
	QString activeString = ui->ACTIVEdit->toPlainText();

	if (ownString.isEmpty() || activeString.isEmpty())
	{
		IMessageBox::tip(this, tr("Notice"), tr("Private Key not Entered Completely!"));
	}
	else
	{
		EOSHelp help;
		QString ownPub = help.EosGetPublicKey(ownString);
		QString activePub = help.EosGetPublicKey(activeString);

		if (ownPub.isEmpty() || activePub.isEmpty())
		{
			IMessageBox::tip(this, tr("Notice"), tr("Incorrect Private Key，Failed to Get Public Key!"));
		}
		else
		{
			emit sigEosPubKey(ownPub, ownString, activePub, activeString);
			this->close();
		}
	}
}
