﻿#include "eosturnoutmanager.h"
#include "QStringLiteralBak.h"
#include "globalmanager.h"
#ifndef Q_OS_WIN
#include "inline_mac.h"
#endif

extern OPDataManager  *gOPDataManager;

EOSTurnOutManager::EOSTurnOutManager(QObject *parent)
	: QObject(parent)
{

}

EOSTurnOutManager::~EOSTurnOutManager()
{
}

void EOSTurnOutManager::setTurnOutData(QString userAddress, QString otherAddress, QString value)
{
	if (strKey.isEmpty())
	{
		IMessageBox::tip((QWidget *)this->parent(), tr("Notice"), tr("Failed to get the private key of EOS wallet!"));
	}
	else
	{
		this->from = userAddress;

		OPRequestShareLib *request = new OPRequestShareLib;
		connect(request, SIGNAL(sigEOSDataParam(QString)), this, SLOT(slotDataParam(QString)));
		connect(request, SIGNAL(sigEOSDataParam(QString)), request, SLOT(deleteLater()));
		request->getDataParam(userAddress, otherAddress, value, ChainID_EOS);
	}
}

void EOSTurnOutManager::slotDataParam(QString data)
{
	this->dataParam = data;

	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigEOSInfo(QString)), this, SLOT(slotEOSInfo(QString)));
	connect(request, SIGNAL(sigEOSInfo(QString)), request, SLOT(deleteLater()));
	request->getEOSInfo();
}

void EOSTurnOutManager::slotEOSInfo(QString info)
{
	this->infoByte = info.toUtf8();
	auto infoObj = QJsonDocument::fromJson(infoByte).object();
	auto infoObj2 = infoObj.value("data").toObject();
	QString head_block_id = infoObj2.value("head_block_id").toString();

	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigBlockNum(int, int)), this, SLOT(slotBlockNum(int, int)));
	connect(request, SIGNAL(sigBlockNum(int, int)), request, SLOT(deleteLater()));
	request->getBlockNum(head_block_id);
}

void EOSTurnOutManager::slotBlockNum(int block_num, int ref_block_prefix)
{
	//获取私钥。
	UserInfo user = gDataManager->getUserInfo();
	QString priKey;
#ifdef Q_OS_WIN
        QString keyPath = gSettingsManager->getUserPath() + "/wallet//" + QString::number(user.nUserID) + "//pwr.key";
#else
        QString keyPath = gSettingsManager->getUserPath() + "/wallet//" + QString::number(user.nUserID) + "//pwr.key";
#endif

	QString mainKey = gOPDataManager->Decryption(user.strLoginPWD, keyPath);
	strKey = strKey.replace(" ", "+");
	priKey = gOPDataManager->decryptAES(strKey.toUtf8(), mainKey);

	EOSHelp help;
	QString json = help.EosSignTransation(this->from, priKey, dataParam, infoByte, block_num, ref_block_prefix);

	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigTransFinished(bool)), this, SLOT(slotTransFinished(bool)));
	connect(request, SIGNAL(sigTransFinished(bool)), request, SLOT(deleteLater()));
	request->eosPushTransactionBody(json);
}

void EOSTurnOutManager::slotTransFinished(bool success)
{
	if (success)
		IMessageBox::tip((QWidget *)this->parent(), tr("Notice"), tr("Deal is Done!"));
	else
	{
		IMessageBox::tip((QWidget *)this->parent(), tr("Notice"), tr("Deal Failed!"));
	}
}
