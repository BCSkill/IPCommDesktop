﻿#include "ewalletassetswidget.h"
#include "ui_ewalletassetswidget.h"
#ifdef Q_OS_MAC
#include "mycommonstyle.h"
#endif
#include "QStringLiteralBak.h"

extern OPDatebaseShareLib *gOPDataBaseOpera;

EWalletAssetsWidget::EWalletAssetsWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::EWalletAssetsWidget();
	ui->setupUi(this);
#ifdef Q_OS_MAC
    ui->assetsList->setStyle(new MyProxyStyle);
#endif

	QFile file(":/QSS/Resources/QSS/eWalletLib/ewalletassetswidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	ui->assetsList->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff); //水平滚动条
	ui->assetsList->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff); //垂直滚动条

	connect(ui->refreshBtn, SIGNAL(clicked()), this, SLOT(slotRefresh()));
}

EWalletAssetsWidget::~EWalletAssetsWidget()
{
	delete ui;
}

void EWalletAssetsWidget::getAssets(QString phoneNumber)
{
	if (this->account.isEmpty())
	    this->account = phoneNumber;

	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigAssetsInfo(QList<AssetInfo>)), this, SLOT(slotInitAssets(QList<AssetInfo>)));
	connect(request, SIGNAL(sigAssetsInfo(QList<AssetInfo>)), request, SLOT(deleteLater()));
	request->getAssets(phoneNumber);
}

void EWalletAssetsWidget::slotInitAssets(QList<AssetInfo> assetsList)
{
	if (assetsList.isEmpty())
	{
		assetsList = gOPDataBaseOpera->DBGetAssetInfo();
	}
	else
	{
		gOPDataBaseOpera->DBInsertAssetInfo(assetsList);
	}

	double amount = 0;
	ui->assetsList->clear();

	foreach(AssetInfo asset, assetsList)
	{
		QString icon;
		if (asset.coinName == "PWR")
			icon = ":/ewallet/Resources/ewallet/pwrWidget/balance.png";
		if (asset.coinName == "ETH")
			icon = ":/ewallet/Resources/ewallet/eth.png";
		if (asset.coinName == "BTC")
			icon = ":/ewallet/Resources/ewallet/btc.png";
		if (asset.coinName == "EOS")
			icon = ":/ewallet/Resources/ewallet/eos.png";

		if (icon.isEmpty())
		{

		}
		else
		{
			QString name = asset.coinName;
			QString nameCNY = tr("≈") + QString::number(asset.coinMarketPrice, 'f', 2) + "CNY";
			QString number;

			if (name == "PWR")
				number = QString::number(asset.availableCoin, 'f', 5);
			else
			{
				number = QString::number(asset.availableCoin, 'f', 2);
			}

			QString numberCNY = QString::number(asset.userCoinMarketNum, 'f', 2) + "CNY";

			EWalletItemAsset *assetWidget = new EWalletItemAsset;
			assetWidget->setData(icon, name, number, nameCNY, numberCNY);
			QListWidgetItem *assetItem = new QListWidgetItem;
			assetItem->setSizeHint(QSize(280, 60));
			ui->assetsList->addItem(assetItem);
			ui->assetsList->setItemWidget(assetItem, assetWidget);

			if (name != "PWR")
				amount += asset.userCoinMarketNum;
		}
	}

	ui->numberLabel->setText(QString::number(amount, 'f', 2));
}

void EWalletAssetsWidget::slotRefresh()
{
	this->getAssets(account);
}


