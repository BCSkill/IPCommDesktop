﻿#include "hostingitemwidget.h"
#include "ui_hostingitemwidget.h"
#include <qfile.h>

HostingItemWidget::HostingItemWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::HostingItemWidget();
	ui->setupUi(this);

	QFile file(":/QSS/Resources/QSS/eWalletLib/hostingitemwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->nextBtn, SIGNAL(clicked()), this, SLOT(slotClickedNextBtn()));
}

HostingItemWidget::~HostingItemWidget()
{
	delete ui;
}

void HostingItemWidget::hideNextBtn()
{
	ui->nextBtn->hide();
}

void HostingItemWidget::setIcon(QString iconPath)
{
	QByteArray bytearray;
	QFile file(iconPath);
	if (file.open(QIODevice::ReadOnly) && file.size() != 0)
	{
		bytearray = file.readAll();
	}
	file.close();
	QPixmap pix;
	pix.loadFromData(bytearray);

	if (pix.isNull())
		return;

	ui->iconLabel->setPixmap(pix);
	this->strIconPath = iconPath;
}

void HostingItemWidget::setData(QString name, QString availableCoin, QString freezingCoin, double trusteeshipId)
{
	ui->nameLabel->setText(name);
	ui->usefulNumLabel->setText(availableCoin);
	ui->freezeNumLabel->setText(freezingCoin);

	this->trusteeshipId = trusteeshipId;
}

void HostingItemWidget::setData(QString iconPath, QString name, QString availableCoin, QString freezingCoin, double trusteeshipId)
{
	setIcon(iconPath);
	setData(name, availableCoin, freezingCoin, trusteeshipId);
}

QString HostingItemWidget::getName()
{
	return ui->nameLabel->text();
}

void HostingItemWidget::slotClickedNextBtn()
{
	emit sigShowRecords(ui->nameLabel->text());
}
