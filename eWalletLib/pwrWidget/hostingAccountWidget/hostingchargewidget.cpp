﻿#include "hostingchargewidget.h"
#include "ui_hostingchargewidget.h"
#include "QStringLiteralBak.h"

HostingChargeWidget::HostingChargeWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::HostingChargeWidget();
	ui->setupUi(this);

	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog);
	setWindowModality(Qt::WindowModal);
	setAttribute(Qt::WA_DeleteOnClose);
	setAttribute(Qt::WA_TranslucentBackground);
	setWindowIcon(QIcon(":/Login/Resources/login/system.ico"));

	QFile file(":/QSS/Resources/QSS/eWalletLib/hostingchargewidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->mBtnClose, SIGNAL(clicked()), this, SLOT(close()));
	connect(ui->enterBtn, SIGNAL(clicked()), this, SLOT(slotEnter()));
	connect(ui->addressCombox, SIGNAL(currentIndexChanged(int)), this, SLOT(slotChangeAddress(int)));

	//用于下拉框item的样式美化
	QStyledItemDelegate* itemDelegate = new QStyledItemDelegate();
	ui->addressCombox->setItemDelegate(itemDelegate);
	ui->valueEdit->setValidator(new QDoubleValidator(0, 10000, 8, this));
}

HostingChargeWidget::~HostingChargeWidget()
{
	delete ui;
}

void HostingChargeWidget::show()
{
	QWidget::show();
	this->raise();
	this->activateWindow();
}

void HostingChargeWidget::setAsset(AssetInfo asset)
{
	this->asset = asset;

	ui->titleLabel->setText(asset.coinName + tr("Transfer"));
	ui->nameLabel->setText(asset.coinName);

	QString miner = "0.0004" + asset.coinName;
	ui->minerLabel->setText(miner);

	if (asset.coinName == "PWR")
	{
		QString name = tr("Wallet") + mainAddress.left(6) + "****" + mainAddress.right(4);
		ui->addressCombox->addItem(name, mainAddress);
		ui->addressLabel->setText(mainAddress);

		OPRequestShareLib *request = new OPRequestShareLib;
		connect(request, SIGNAL(sigBalance(QString)), this, SLOT(slotSetBalance(QString)));
		connect(request, SIGNAL(sigBalance(QString)), request, SLOT(deleteLater()));
		request->getBalance(mainAddress);
	}
	else
	{
		OPRequestShareLib *request = new OPRequestShareLib;
		connect(request, SIGNAL(sigChildrenWallet(QList<ChildrenWallet>)), this, SLOT(slotInsertWallet(QList<ChildrenWallet>)));
		connect(request, SIGNAL(sigChildrenWallet(QList<ChildrenWallet>)), request, SLOT(deleteLater()));
		request->getChildrenWallet(mainAddress);
	}
}

void HostingChargeWidget::slotSetBalance(QString balance)
{
	//QString balance = QString::number(asset.availableCoin, 'f', 8);
	ui->balanceLabel->setText(balance);
}

//鼠标事件的处理。
void HostingChargeWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void HostingChargeWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void HostingChargeWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

void HostingChargeWidget::slotInsertWallet(QList<ChildrenWallet> walletList)
{
	if (asset.coinName == "ETH")
	{
		QString name = tr("Wallet") + mainAddress.left(6) + "****" + mainAddress.right(4);
		ui->addressCombox->addItem(name, mainAddress);
		ui->addressLabel->setText(mainAddress);

		/*
		OPRequestShareLib *request = new OPRequestShareLib;
		connect(request, SIGNAL(sigBalance(QString)), this, SLOT(slotSetBalance(QString)));
		connect(request, SIGNAL(sigBalance(QString)), request, SLOT(deleteLater()));
		request->getBalance(mainAddress);
		*/
	}

	this->walletsList.clear();
	this->walletsList = walletList;

	foreach(ChildrenWallet wallet, walletList)
	{
		if (wallet.blockChainName == asset.coinName)
		{
			ui->addressCombox->addItem(wallet.accountName);

			if (ui->addressLabel->text().isEmpty())
			{
				ui->addressLabel->setText(wallet.accountAddress);
				slotSetBalance(wallet.balance);
			}
				
		}
	}
}

void HostingChargeWidget::slotChangeAddress(int index)
{
	foreach(ChildrenWallet wallet, walletsList)
	{
		if (wallet.accountName == ui->addressCombox->itemText(index))
		{
			ui->addressLabel->setText(wallet.accountAddress);
			slotSetBalance(wallet.balance);
			break;
		}
	}
}

void HostingChargeWidget::slotEnter()
{
	QString address = ui->addressLabel->text();
	if (address.isEmpty())
	{
		IMessageBox::tip(this, tr("Notice"), tr("Payment address cannot be empty"));
		return;
	}
	QString value = ui->valueEdit->text();
	if (value.isEmpty())
	{
		IMessageBox::tip(this, tr("Notice"), tr("Amount cannot be empty"));
		return;
	}
	if (value.toDouble() > ui->balanceLabel->text().toDouble())
	{
		IMessageBox::tip(this, tr("Notice"), tr("Amount is greater than") + asset.coinName + tr("Balance"));
		return;
	}

	QString pwd = gDataManager->getUserInfo().strLoginPWD;
	PasswordWidget *password = new PasswordWidget(pwd, this);
	connect(password, SIGNAL(sigPasswordTrue()), this, SLOT(slotChargeHosting()));
	password->show();
}

void HostingChargeWidget::slotChargeHosting()
{
	QString userID = gDataManager->getUserInfo().strAccountName;

	TransferManager *transManager = new TransferManager;
	transManager->setPrivateKey(privateKey);
	transManager->hostingCharge(userID, QString::number(asset.trusteeshipId),
		ui->valueEdit->text(), ui->addressLabel->text(), asset.sysAddress);
}