﻿#include "hostingrecorditemwidget.h"
#include "ui_hostingrecorditemwidget.h"
#include "QStringLiteralBak.h"
#include <qfile.h>

hostingRecordItemWidget::hostingRecordItemWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::hostingRecordItemWidget();
	ui->setupUi(this);

	QFile file(":/QSS/Resources/QSS/eWalletLib/hostingrecorditemwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();
}

hostingRecordItemWidget::~hostingRecordItemWidget()
{
	delete ui;
}

void hostingRecordItemWidget::setData(QString type, QString time, QString number, QString state)
{
	ui->typeLabel->setText(type);
	ui->timeLabel->setText(time);
	ui->numLabel->setText(number);
	ui->stateLabel->setText(state);

	if (type == tr("Get Red Packet"))
	{
		ui->iconLabel->setPixmap(QPixmap(":/ewallet/Resources/ewallet/asset_records_redpacket.png"));
	}
	if (type == tr("Top Up"))
	{
		ui->iconLabel->setPixmap(QPixmap(":/ewallet/Resources/ewallet/asset_records_recharge.png"));
	}
	if (type == tr("Withdraw"))
	{
		ui->iconLabel->setPixmap(QPixmap(":/ewallet/Resources/ewallet/asset_records_withdraw.png"));
	}
}
