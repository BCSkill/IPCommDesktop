﻿#include "hostingcoinwidget.h"
#include "ui_hostingcoinwidget.h"
#include "QStringLiteralBak.h"

HostingCoinWidget::HostingCoinWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::HostingCoinWidget();
	ui->setupUi(this);

	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog);
	setWindowModality(Qt::WindowModal);
	setAttribute(Qt::WA_DeleteOnClose);
	setAttribute(Qt::WA_TranslucentBackground);
	setWindowIcon(QIcon(":/Login/Resources/login/system.ico"));

	QFile file(":/QSS/Resources/QSS/eWalletLib/hostingcoinwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->mBtnClose, SIGNAL(clicked()), this, SLOT(close()));
	connect(ui->allBtn, SIGNAL(clicked()), this, SLOT(slotAllBtn()));
	connect(ui->enterBtn, SIGNAL(clicked()), this, SLOT(slotEnter()));
	connect(ui->addressComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(slotChangeAddress(int)));

	//用于下拉框item的样式美化
	QStyledItemDelegate* itemDelegate = new QStyledItemDelegate();
	ui->addressComboBox->setItemDelegate(itemDelegate);
	ui->valueEdit->setValidator(new QDoubleValidator(0, 10000, 8, this));
}

HostingCoinWidget::~HostingCoinWidget()
{
	delete ui;
}

void HostingCoinWidget::show()
{
	QWidget::show();
	this->raise();
	this->activateWindow();
}

void HostingCoinWidget::setAsset(AssetInfo asset)
{
	this->asset = asset;

	QString balance = QString::number(asset.availableCoin, 'f', 8) + asset.coinName;
	ui->balanceLabel->setText(balance);

	QString miner = "0.002" + asset.coinName;
	ui->minerLabel->setText(miner);

	if (asset.coinName == "PWR")
	{
		QString name = tr("Wallet") + mainAddress.left(6) + "****" + mainAddress.right(4);
		ui->addressComboBox->addItem(name, mainAddress);
		ui->addressLabel->setText(mainAddress);
	}
	else
	{
		OPRequestShareLib *request = new OPRequestShareLib;
		connect(request, SIGNAL(sigChildrenWallet(QList<ChildrenWallet>)), this, SLOT(slotInsertWallet(QList<ChildrenWallet>)));
		connect(request, SIGNAL(sigChildrenWallet(QList<ChildrenWallet>)), request, SLOT(deleteLater()));
		request->getChildrenWallet(mainAddress);
	}
}

//鼠标事件的处理。
void HostingCoinWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void HostingCoinWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void HostingCoinWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

void HostingCoinWidget::slotInsertWallet(QList<ChildrenWallet> walletList)
{
	if (asset.coinName == "ETH")
	{
		QString name = tr("Wallet") + mainAddress.left(6) + "****" + mainAddress.right(4);
		ui->addressComboBox->addItem(name, mainAddress);
		ui->addressLabel->setText(mainAddress);
	}

	foreach(ChildrenWallet wallet, walletList)
	{
		if (wallet.blockChainName == asset.coinName)
		{
			ui->addressComboBox->addItem(wallet.accountName, wallet.accountAddress);

			if (ui->addressLabel->text().isEmpty())
				ui->addressLabel->setText(wallet.accountAddress);
		}
	}
}

void HostingCoinWidget::slotAllBtn()
{
	QString value = QString::number(asset.availableCoin, 'f', 8);
	ui->valueEdit->setText(value);
}

void HostingCoinWidget::slotChangeAddress(int index)
{
	QString address = ui->addressComboBox->itemData(index).toString();
	ui->addressLabel->setText(address);
}

void HostingCoinWidget::slotEnter()
{
	QString address = ui->addressLabel->text();
	if (address.isEmpty())
	{
		IMessageBox::tip(this, tr("Notice"), tr("Receive address cannot be empty"));
		return;
	}
	QString value = ui->valueEdit->text();
	if (value.isEmpty())
	{
		IMessageBox::tip(this, tr("Notice"), tr("Amount cannot be empty"));
		return;
	}
	if (value.toDouble() > asset.availableCoin)
	{
		IMessageBox::tip(this, tr("Notice"), tr("Amount is greater than available assets") + asset.coinName);
		return;
	}
	
	QString pwd = gDataManager->getUserInfo().strLoginPWD;
	PasswordWidget *password = new PasswordWidget(pwd, this);
	connect(password, SIGNAL(sigPasswordTrue()), this, SLOT(slotCoinValue()));
	password->show();
}

void HostingCoinWidget::slotCoinValue()
{
	QString value = ui->valueEdit->text();
	QString userID = gDataManager->getUserInfo().strAccountName;
	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigTransFinished(true)), this, SLOT(slotCoinResult(bool)));
	connect(request, SIGNAL(sigTransFinished(true)), request, SLOT(deleteLater()));
	request->HostingAccountCoin(userID, QString::number(asset.trusteeshipId), value, mainAddress);
}

void HostingCoinWidget::slotCoinResult(bool success)
{
	if (success)
		IMessageBox::tip(this, tr("Notice"), tr("Withdraw completed, Please check your balance later"));
	else
	{
		IMessageBox::tip(this, tr("Notice"), tr("Withdraw failed"));
	}
}
