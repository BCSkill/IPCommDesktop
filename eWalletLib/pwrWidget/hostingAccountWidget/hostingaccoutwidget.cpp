﻿#include "hostingaccoutwidget.h"
#include "ui_hostingaccoutwidget.h"
#include "globalmanager.h"
#include "QStringLiteralBak.h"
#ifndef Q_OS_WIN
#include "mycommonstyle.h"
#endif

HostingAccoutWidget::HostingAccoutWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::HostingAccoutWidget();
	ui->setupUi(this);
#ifndef Q_OS_WIN
    ui->accountsList->setStyle(new MyProxyStyle);
    ui->recordsList->setStyle(new MyProxyStyle);
#endif*/

	QFile file(":/QSS/Resources/QSS/eWalletLib/hostingaccoutwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->accountsList, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(slotDBClickedAssetsList(QListWidgetItem *)));

	connect(ui->backBtn, SIGNAL(clicked()), this, SLOT(slotAccountsPage()));

	connect(ui->allRecordsBtn, SIGNAL(clicked()), this, SLOT(slotAllTab()));
	connect(ui->chargeRecordsBtn, SIGNAL(clicked()), this, SLOT(slotChargeTab()));
	connect(ui->coinRecordBtn, SIGNAL(clicked()), this, SLOT(slotCoinTab()));
	connect(ui->otherRecordBtn, SIGNAL(clicked()), this, SLOT(slotOtherTab()));

	connect(ui->chargeBtn, SIGNAL(clicked()), this, SLOT(slotCharge()));
	connect(ui->coinBtn, SIGNAL(clicked()), this, SLOT(slotCoin()));

	ui->contentWidget->hideNextBtn();
}

HostingAccoutWidget::~HostingAccoutWidget()
{
	delete ui;
}

void HostingAccoutWidget::getHostingAccount()
{
	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigAssetsInfo(QList<AssetInfo>)), this, SLOT(slotHostingAccount(QList<AssetInfo>)));
	connect(request, SIGNAL(sigAssetsInfo(QList<AssetInfo>)), request, SLOT(deleteLater()));
	QString userID = gDataManager->getUserInfo().strAccountName;
	request->getHostingAccount(userID);
}

void HostingAccoutWidget::slotHostingAccount(QList<AssetInfo> assetList)
{
	ui->accountsList->clear();
	this->assetsList.clear();
	this->assetsList = assetList;

	foreach(AssetInfo asset, assetList)
	{
		QListWidgetItem *item = new QListWidgetItem;
		item->setSizeHint(QSize(ui->accountsList->width(), 50));

		ui->accountsList->addItem(item);
		HostingItemWidget *itemWidget = new HostingItemWidget;

		//拼接该代币的icon。
		QString iconPath = gSettingsManager->getUserPath() + "/resource/ewallet/header/" + asset.coinName + ".png";
		QFile iconFile(iconPath);
		if (iconFile.exists() && iconFile.size() != 0)
		    itemWidget->setData(iconPath, asset.coinName, 
				QString::number(asset.availableCoin, 'f', 8), QString::number(asset.freezingCoin, 'f', 8),
				asset.trusteeshipId);
		else
		{
			itemWidget->setData(asset.coinName, 
				QString::number(asset.availableCoin, 'f', 8), QString::number(asset.freezingCoin, 'f', 8),
				asset.trusteeshipId);
			
			HttpNetWork::HttpDownLoadFile *http = new HttpNetWork::HttpDownLoadFile;
			connect(http, SIGNAL(sigDownFinished(bool)), this, SLOT(slotDownloadAccountIcon(bool)));
			http->setData(QVariant(asset.coinName));
			http->StartDownLoadFile(asset.coinIcon, iconPath);
		}

		ui->accountsList->setItemWidget(item, itemWidget);
		connect(itemWidget, SIGNAL(sigShowRecords(QString)), this, SLOT(slotRecordsPage(QString)));
	}
}

void HostingAccoutWidget::slotDownloadAccountIcon(bool success)
{
	if (success)
	{
		HttpNetWork::HttpDownLoadFile *http = qobject_cast<HttpNetWork::HttpDownLoadFile *>(sender());
		QString name = http->getData().toString();

		for (int i = 0; i < ui->accountsList->count(); i++)
		{
			QListWidgetItem *item = ui->accountsList->item(i);

			if (item)
			{
				HostingItemWidget *itemWidget = (HostingItemWidget *)ui->accountsList->itemWidget(item);
				if (itemWidget)
				{
					if (itemWidget->getName() == name)
					{
						QString iconPath = gSettingsManager->getUserPath() + "/resource/ewallet/header/" + name + ".png";
						itemWidget->setIcon(iconPath);
					}
				}
			}
		}
	}
}

void HostingAccoutWidget::slotDBClickedAssetsList(QListWidgetItem *item)
{
	HostingItemWidget *itemWidget = (HostingItemWidget *)ui->accountsList->itemWidget(item);
	if (itemWidget)
	{
		QString name = itemWidget->getName();

		foreach(AssetInfo asset, assetsList)
		{
			if (asset.coinName == name)
			{
				this->localAsset = asset;
				break;
			}
		}
		QString iconPath = gSettingsManager->getUserPath() + "/resource/ewallet/header/" + localAsset.coinName + ".png";

		ui->contentWidget->setData(iconPath, localAsset.coinName,
			QString::number(localAsset.availableCoin, 'f', 8),
			QString::number(localAsset.freezingCoin, 'f', 8),
			localAsset.trusteeshipId);

		ui->titleLabel->setText(localAsset.coinName);

		UserInfo user = gDataManager->getUserInfo();
		OPRequestShareLib *request = new OPRequestShareLib;
		connect(request, SIGNAL(sigRecords(QList<RecordInfo>)), this, SLOT(slotRecords(QList<RecordInfo>)));
		connect(request, SIGNAL(sigRecords(QList<RecordInfo>)), request, SLOT(deleteLater()));
		request->getHostingRecords(user.strAccountName, localAsset.trusteeshipId);

		ui->mainStackedWidget->setCurrentIndex(1);
	}
}

void HostingAccoutWidget::slotRecordsPage(QString name)
{
	foreach(AssetInfo asset, assetsList)
	{
		if (asset.coinName == name)
		{
			this->localAsset = asset;
			break;
		}
	}
	QString iconPath = gSettingsManager->getUserPath() + "/resource/ewallet/header/" + localAsset.coinName + ".png";

	ui->contentWidget->setData(iconPath, localAsset.coinName,
		QString::number(localAsset.availableCoin, 'f', 8),
		QString::number(localAsset.freezingCoin, 'f', 8),
		localAsset.trusteeshipId);

	ui->titleLabel->setText(localAsset.coinName);

	UserInfo user = gDataManager->getUserInfo();
	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigRecords(QList<RecordInfo>)), this, SLOT(slotRecords(QList<RecordInfo>)));
	connect(request, SIGNAL(sigRecords(QList<RecordInfo>)), request, SLOT(deleteLater()));
	request->getHostingRecords(user.strAccountName, localAsset.trusteeshipId);

	ui->mainStackedWidget->setCurrentIndex(1);
}

void HostingAccoutWidget::slotAccountsPage()
{
	ui->recordsList->clear();
	this->getHostingAccount();

	ui->mainStackedWidget->setCurrentIndex(0);
}

void HostingAccoutWidget::slotRecords(QList<RecordInfo> recordsList)
{
	this->recordsList = recordsList;

	slotAllTab();
	ui->allRecordsBtn->setChecked(true);
}

void HostingAccoutWidget::slotCharge()
{
	HostingChargeWidget *chargeWidget = new HostingChargeWidget;
	chargeWidget->setMainAddress(mainAddress);
	chargeWidget->setAsset(localAsset);
	chargeWidget->setPrivateKey(privateKey);

	chargeWidget->show();
}

void HostingAccoutWidget::slotCoin()
{
	HostingCoinWidget *coinWidget = new HostingCoinWidget;
	coinWidget->setMainAddress(this->mainAddress);
	coinWidget->setAsset(this->localAsset);
	coinWidget->show();
}

void HostingAccoutWidget::slotAllTab()
{
	ui->recordsList->clear();

	foreach(RecordInfo recordInfo, recordsList)
	{
		hostingRecordItemWidget *record = new hostingRecordItemWidget;
		QString type;
		if (recordInfo.type == 1)
			type = tr("Get Red Packet");
		if (recordInfo.type == 4)
			type = tr("Top Up");
		if (recordInfo.type == 5)
			type = tr("WithDraw");

		if (!type.isEmpty())
		{
			record->setData(type, recordInfo.age, recordInfo.value, tr("Confirmed"));
			QListWidgetItem *recordItem = new QListWidgetItem;
			recordItem->setSizeHint(QSize(ui->recordsList->width(), 50));
			ui->recordsList->addItem(recordItem);
			ui->recordsList->setItemWidget(recordItem, record);
		}
	}
}

void HostingAccoutWidget::slotChargeTab()
{
	ui->recordsList->clear();

	foreach(RecordInfo recordInfo, recordsList)
	{
		hostingRecordItemWidget *record = new hostingRecordItemWidget;
		QString type;
		if (recordInfo.type == 4)
			type = tr("Top Up");

		if (!type.isEmpty())
		{
			record->setData(type, recordInfo.age, recordInfo.value, tr("Confirmed"));
			QListWidgetItem *recordItem = new QListWidgetItem;
			recordItem->setSizeHint(QSize(ui->recordsList->width(), 50));
			ui->recordsList->addItem(recordItem);
			ui->recordsList->setItemWidget(recordItem, record);
		}
	}
}

void HostingAccoutWidget::slotCoinTab()
{
	ui->recordsList->clear();

	foreach(RecordInfo recordInfo, recordsList)
	{
		hostingRecordItemWidget *record = new hostingRecordItemWidget;
		QString type;
		if (recordInfo.type == 5)
			type = tr("WithDraw");

		if (!type.isEmpty())
		{
			record->setData(type, recordInfo.age, recordInfo.value, tr("Confirmed"));
			QListWidgetItem *recordItem = new QListWidgetItem;
			recordItem->setSizeHint(QSize(ui->recordsList->width(), 50));
			ui->recordsList->addItem(recordItem);
			ui->recordsList->setItemWidget(recordItem, record);
		}
	}
}

void HostingAccoutWidget::slotOtherTab()
{
	ui->recordsList->clear();

	foreach(RecordInfo recordInfo, recordsList)
	{
		hostingRecordItemWidget *record = new hostingRecordItemWidget;
		QString type;
		if (recordInfo.type == 1)
			type = tr("Get Red Packet");

		if (!type.isEmpty())
		{
			record->setData(type, recordInfo.age, recordInfo.value, tr("Confirmed"));
			QListWidgetItem *recordItem = new QListWidgetItem;
			recordItem->setSizeHint(QSize(ui->recordsList->width(), 50));
			ui->recordsList->addItem(recordItem);
			ui->recordsList->setItemWidget(recordItem, record);
		}
	}
}
