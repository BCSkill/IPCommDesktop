﻿#include "mydevicewidget.h"
#include "ui_mydevicewidget.h"
#include <qtextcodec.h>
#ifdef Q_OS_MAC
#include "mycommonstyle.h"
#endif

MyDeviceWidget::MyDeviceWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::MyDeviceWidget();
	ui->setupUi(this);

	QFile file(":/QSS/Resources/QSS/eWalletLib/mydevicewidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

#ifdef Q_OS_MAC
    ui->listWidget->setStyle(new MyProxyStyle);
#endif

	/*
	for (int i = 0; i < 10; i++)
	{
		
	}
	*/
}

MyDeviceWidget::~MyDeviceWidget()
{
	delete ui;
}

void MyDeviceWidget::getDeviceList()
{
	UserInfo user = gDataManager->getUserInfo();
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotResult(bool, QString)));
	QString strResult = gDataManager->getAppConfigInfo().MessageServerAddress +
		QString("/IMServer/user/getSessionContextList?userId=%1&passWord=%2").arg(user.nUserID).arg(user.strUserPWD);
	http->getHttpRequest(strResult);
}

void MyDeviceWidget::slotResult(bool success, QString result)
{
	ui->listWidget->clear();
	deviceList.clear();

	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		QVariantList list = map.value("sessionContextList").toList();

		foreach(QVariant var, list)
		{
			QVariantMap data = var.toMap();
			DeviceInfo device;
			device.clientId = data.value("clientId").toString();
			device.deviceClass = data.value("deviceClass").toString();
			device.deviceId = data.value("deviceId").toString();
			QString str = data.value("deviceModel").toString();
			device.deviceType = data.value("deviceType").toString();
			device.sessionTime = data.value("sessionTime").toLongLong();

			QByteArray array;
			for (int i = 0; i < str.length();) {
				if (0 == QString::compare(str.mid(i, 1), QString("%"))) {
					if ((i + 2) < str.length()) {
						array.append(str.mid(i + 1, 2).toShort(0, 16));
						i = i + 3;
					}
					else {
						array.append(str.mid(i, 1));
						i++;
					}
				}
				else {
					array.append(str.mid(i, 1));
					i++;
				}
			}
			QTextCodec *code = QTextCodec::codecForName("UTF-8");
			device.deviceModel = code->toUnicode(array);

			this->deviceList.append(device);
		}

		this->onInsertDeviceItem();
	}
}

void MyDeviceWidget::onInsertDeviceItem()
{
	for (int i = 0; i < deviceList.count(); i++)
	{
		DeviceInfo device = deviceList.at(i);
		bool isSelf;
		if (GetDeviceInfo().GetDeviceID() == device.deviceId)
			isSelf = true;
		else
			isSelf = false;

		if (QString::number(device.sessionTime).length() == 13)
			device.sessionTime /= 1000;
		QDateTime time = QDateTime::fromTime_t(device.sessionTime);
		QString strTime = time.toString("yyyy-MM-dd hh:mm:ss");
		QString iconPath;
		if (device.deviceType.toLower() == "android")
			iconPath = ":/ewallet/Resources/ewallet/device/android.png";
		if (device.deviceType.toLower() == "ios")
			iconPath = ":/ewallet/Resources/ewallet/device/apple.png";
		if (device.deviceType.toLower() == "win")
			iconPath = ":/ewallet/Resources/ewallet/device/windows.png";
		if (device.deviceType.toLower() == "mac")
			iconPath = ":/ewallet/Resources/ewallet/device/macOS.png";
		if (device.deviceType.toLower() == "ws")
			iconPath = ":/ewallet/Resources/ewallet/device/H5.png";
		if (device.deviceType.toLower() == "linux")
			iconPath = ":/ewallet/Resources/ewallet/device/linux.png";

		QListWidgetItem *item = new QListWidgetItem;
		item->setData(Qt::UserRole, QVariant(device.deviceId));
		item->setSizeHint(QSize(ui->listWidget->width(), 50));

		if (isSelf)
			ui->listWidget->insertItem(0, item);
		else
			ui->listWidget->addItem(item);
		
		DeviceItemWidget *deviceWidget = new DeviceItemWidget;
		connect(deviceWidget, SIGNAL(sigDownLine()), this, SLOT(slotEnterPassWord()));
		deviceWidget->setData(iconPath, device.deviceModel, strTime, isSelf);
		ui->listWidget->setItemWidget(item, deviceWidget);
	}
}

void MyDeviceWidget::slotEnterPassWord()
{
	DeviceItemWidget *sendWidget = qobject_cast<DeviceItemWidget *>(sender());
	if (sendWidget)
	{
		UserInfo user = gDataManager->getUserInfo();
		PasswordWidget *password = new PasswordWidget(user.strLoginPWD, this);
		password->setObjectName(sendWidget->getName());
		connect(password, SIGNAL(sigPasswordTrue()), this, SLOT(slotDownLine()));
		password->show();
	}
}

void MyDeviceWidget::slotDownLine()
{
	PasswordWidget *password = qobject_cast<PasswordWidget *>(sender());
	for (int i = 0; i < ui->listWidget->count(); i++)
	{
		QListWidgetItem *item = ui->listWidget->item(i);
		if (item)
		{
			DeviceItemWidget *widget = (DeviceItemWidget *)ui->listWidget->itemWidget(item);
			if (widget != NULL && widget->getName() == password->objectName())
			{
				QString deviceID = item->data(Qt::UserRole).toString();

				foreach(DeviceInfo device, deviceList)
				{
					if (device.deviceId == deviceID)
					{
						//下线操作。
						UserInfo user = gDataManager->getUserInfo();
						HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
						QString strResult = gDataManager->getAppConfigInfo().MessageServerAddress +
							QString("/IMServer/user/deviceLogout?userId=%1&passWord=%2&clientId=%3&deviceId=%4")
							.arg(user.nUserID).arg(user.strUserPWD).arg(device.clientId).arg(device.deviceId);
						http->getHttpRequest(strResult);

						ui->listWidget->takeItem(i);
						return;
					}
				}
			}
		}
	}
}
