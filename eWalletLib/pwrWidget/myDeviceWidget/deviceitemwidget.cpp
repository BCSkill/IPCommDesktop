﻿#include "deviceitemwidget.h"
#include "ui_deviceitemwidget.h"
#include "QStringLiteralBak.h"
#include <qfile.h>

DeviceItemWidget::DeviceItemWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::DeviceItemWidget();
	ui->setupUi(this);

	QFile file(":/QSS/Resources/QSS/eWalletLib/deviceitemwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->downLineBtn, SIGNAL(clicked()), this, SIGNAL(sigDownLine()));
	QString buttonStyle("\
		QPushButton#downLineBtn	\
	{background-color: rgba(0,0,0, 0); color: #6A82A7; text-align: center}");
	ui->downLineBtn->setStyleSheet(buttonStyle);
}

DeviceItemWidget::~DeviceItemWidget()
{
	delete ui;
}

void DeviceItemWidget::setData(QString iconPath, QString name, QString time, bool isSelf)
{
	ui->iconLabel->setPixmap(QPixmap(iconPath));
	ui->nameLabel->setText(name);
	ui->timeLabel->setText(time);

	if (!isSelf)
	{
		QString buttonStyle("\
		QPushButton#downLineBtn	\
	{background-color: rgba(0,0,0, 0); color: #6A82A7; text-align: center}	\
		QPushButton#downLineBtn:hover	\
	{ color: #C8C8C8; }	\
		QPushButton#downLineBtn:pressed	\
	{ color: #C8C8C8; }");
		ui->downLineBtn->setStyleSheet(buttonStyle);
		ui->downLineBtn->setText(tr("Offline"));
		ui->downLineBtn->setCursor(Qt::PointingHandCursor);
	}
	else
	{
		ui->downLineBtn->setEnabled(false);
	}
}

QString DeviceItemWidget::getName()
{
	return ui->nameLabel->text();
}
