﻿#include "transfermanager.h"
#include "QStringLiteralBak.h"

TransferManager::TransferManager(QObject *parent)
	: QObject(parent)
{
	cefView = NULL;

//#ifdef Q_OS_MAC
	m_pWebChannel = NULL;
	m_pWebObject = NULL;
//#endif // Q_OS_MAC
}

TransferManager::~TransferManager()
{
	if (cefView)
	    cefView->deleteLater();

//#ifdef Q_OS_MAC
	if (m_pWebChannel)
		delete m_pWebChannel;
	if (m_pWebObject)
		delete m_pWebObject;
//#endif // Q_OS_MAC

}

void TransferManager::setPrivateKey(QString priKey)
{
	this->priKey = priKey;
}

void TransferManager::turnOut(QString userID, QString otherID, QString fromAddress, QString toAddress, QString miner, QString value,bool bIsWindow)
{
	this->userID = userID;
	this->otherID = otherID;
	this->fromAddress = fromAddress;
	this->toAddress = toAddress;
	this->miner = miner;
	this->value = value;
	this->bIsWindow = bIsWindow;

	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigNonce(QString)), this, SLOT(slotGetNonce(QString)));
	connect(request, SIGNAL(sigNonce(QString)), request, SLOT(deleteLater()));
	request->getNonce(fromAddress);
}

void TransferManager::hostingCharge(QString userID, QString trusteeshipId, QString value, QString txFrom, QString txTo)
{
	this->userID = userID;
	this->trusteeshipId = trusteeshipId;
	this->value = value;
	this->fromAddress = txFrom;
	this->toAddress = txTo;

	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigNonce(QString)), this, SLOT(slotGetNonce(QString)));
	connect(request, SIGNAL(sigNonce(QString)), request, SLOT(deleteLater()));
	request->getNonce(fromAddress);
}

void TransferManager::slotGetNonce(QString nonce)
{
	if (nonce == "error")
	{
		IMessageBox::tip(NULL, tr("Deal Failed"), tr("Error occured when getting Nonce"));
		this->deleteLater();
	}
	else
	{
		//对矿工费用进行计算。
		long double num = miner.toDouble() * (long double)1000000000000000000 / (long double)4300000;

		
//#ifdef Q_OS_WIN
//		if (cefView)
//			delete cefView;
//		cefView = new QCefView;
//		cefView->hide();
//		connect(cefView, SIGNAL(sigTransactionData(QString)), this, SLOT(slotGetTransactionData(QString)));
//		cefView->InitCefUrl("file:///./html/ethWallet.html");
//		QEventLoop loop;
//		connect(cefView, SIGNAL(sigInitFinished()), &loop, SLOT(quit()));
//		loop.exec();
//#else
		if (cefView)
			delete cefView;
		if (m_pWebChannel)
			delete m_pWebChannel;
		if (m_pWebObject)
			delete m_pWebObject;

		m_pWebChannel = new QWebChannel(this);
		m_pWebObject = new WebObjectShareLib(this);
		connect(m_pWebObject, SIGNAL(sigTransactionData(QString)), this, SLOT(slotGetTransactionData(QString)));

		/*QString urlName = QDir::currentPath() + ("/html/ethWallet.html");
		QUrl url = QUrl::fromUserInput(urlName);*/
        QUrl url = QUrl::fromUserInput("qrc:/html/Resources/html/ethWallet.html");
        //QUrl url = "qrc:/html/Resources/html/ethWallet.html";
		m_pWebChannel->registerObject("web", m_pWebObject);
		cefView = new QWebEngineView;
		cefView->page()->load(url);
		cefView->page()->setWebChannel(m_pWebChannel);
		cefView->setObjectName(nonce);
		connect(cefView, SIGNAL(loadFinished(bool)), this, SLOT(slotWebEngineFinish(bool)));
//#endif

//#ifdef Q_OS_WIN
//		cefView->ExecuteJavaScript(QString("GeneralSignTransaction(%1,%2,%3,\"%4\",%5,\"%6\")")
//			.arg(nonce.toInt()).arg(18000000000).arg(21000)
//			.arg(toAddress).arg(value.toDouble())
//			.arg(priKey));
//#else
      /*  cefView->page()->runJavaScript(QString("generalSign(%1,%2,%3,\"%4\",%5,\"%6\")")
            .arg(nonce.toInt()).arg(18000000000).arg(21000)
            .arg(toAddress).arg(value.toDouble())
            .arg(priKey));*/

//#endif
	}
}

void TransferManager::slotWebEngineFinish(bool bResult)
{
	if (cefView && bResult)
	{
		QString nonce = cefView->objectName();
		cefView->page()->runJavaScript(QString("generalSign(%1,%2,%3,\"%4\",%5,\"%6\")")
			.arg(nonce.toInt()).arg(18000000000).arg(21000)
			.arg(toAddress).arg(value.toDouble())
			.arg(priKey));
	}
}

void TransferManager::slotGetTransactionData(QString data)
{
	QJsonDocument doc = QJsonDocument::fromJson(data.toUtf8());
	QVariantMap map = doc.toVariant().toMap();

	if (map.value("result").toString() == "true")
	{
		QString transaction = map.value("SignTransaction").toString();
		OPRequestShareLib *http = new OPRequestShareLib;
		
		//此处根据trusteeshipId判断是否是托管账户充值。
		if (trusteeshipId.isEmpty())
		{
			connect(http, SIGNAL(sigTransfer(bool, QVariant)), this, SLOT(slotTransfer(bool, QVariant)));
			connect(http, SIGNAL(sigTransfer(bool, QVariant)), http, SLOT(deleteLater()));
			http->sendRawTransaction(userID, otherID, fromAddress, toAddress, value, transaction);
		}
		else
		{
			connect(http, SIGNAL(sigTransFinished(bool)), this, SLOT(slotHostingChargeResult(bool)));
			connect(http, SIGNAL(sigTransFinished(bool)), http, SLOT(deleteLater()));
			http->HostingCharge(userID, trusteeshipId, transaction, value, fromAddress, toAddress);
		}
	}

	if (map.value("result").toString() == "false")
	{
		IMessageBox::tip(NULL, tr("Deal Failed"), tr("Invalid private key"));
	}

	delete cefView;
	cefView = NULL;
//#ifdef Q_OS_MAC
	delete m_pWebChannel;
	m_pWebChannel = NULL;
	delete m_pWebObject;
	m_pWebObject = NULL;
//#endif // Q_OS_MAC
}

void TransferManager::slotTransfer(bool success, QVariant var)
{
	if (success)
	{
		QString strCurTime = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
		QVariantMap map = var.toMap();
		map.insert("value", value);
		map.insert("time", strCurTime);
		map.insert("turnInAddress", toAddress);
		map.insert("turnOutAddress", fromAddress);
		
		if (bIsWindow)
		{
			QJsonDocument jsonMsg;
			QJsonObject jsonObj;
			jsonObj.insert("sendUserId", userID);
			jsonObj.insert("tradingTime", strCurTime);
			jsonObj.insert("receiveUserId", otherID);
			jsonObj.insert("money", value);
			jsonObj.insert("chainId", 4);
			jsonObj.insert("sendIMUserId", userID);
			jsonObj.insert("tradingID", map["transactionHash"].toString());
			jsonObj.insert("receiveIMUserId", otherID);
			jsonObj.insert("receiveWalletAddress", toAddress);
			jsonObj.insert("sendWalletAddress", fromAddress);
			jsonMsg.setObject(jsonObj);
			QByteArray byte_array = jsonMsg.toJson(QJsonDocument::Compact);
			QString strMsg(byte_array);
			emit sigTransferMsg(userID, otherID, strMsg);
		}


		DetailsWidget *detailWidget = new DetailsWidget(NULL);
		detailWidget->setData(map);
		detailWidget->show();

		emit sigFinished();
	}
	else
	{
		IMessageBox::tip(NULL, tr("Deal Failed"), var.toString());
	}
}

void TransferManager::slotHostingChargeResult(bool success)
{
	if (success)
		IMessageBox::tip(NULL, tr("Notice"), tr("Top up successed，Please check your balance in your hosting account later"));
	else
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("Top up failed"));
	}
}


