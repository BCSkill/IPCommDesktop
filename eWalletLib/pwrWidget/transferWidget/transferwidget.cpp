﻿#include "transferwidget.h"
#include "ui_transferwidget.h"
#include <QDebug>
#ifdef Q_OS_MAC
#include "mycommonstyle.h"
#endif
#include "QStringLiteralBak.h"

TransferWidget::TransferWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::TransferWidget();

	ui->setupUi(this);
	m_bMove = false;

	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog | Qt::Tool);
	setWindowModality(Qt::WindowModal);
	setAttribute(Qt::WA_TranslucentBackground);
	ui->mBtnClose->hide();

#ifdef Q_OS_MAC
    ui->addressEdit->setStyle(new MyProxyStyle);
    ui->IDEdit->setStyle(new MyProxyStyle);
    ui->valueEdit->setStyle(new MyProxyStyle);
#endif

#ifndef Q_OS_WIN
    QFile style(":/QSS/Resources/QSS/qslider_horizontal.qss");
    style.open(QFile::ReadOnly);
    QString sheet = QLatin1String(style.readAll());
    ui->minerSlider->setStyleSheet(sheet);
    style.close();
#else
    ui->minerSlider->setStyleSheet("background-color: rgba(0,0,0, 0);");
#endif

	QFile file(":/QSS/Resources/QSS/eWalletLib/transferwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->minerSlider, SIGNAL(valueChanged(int)), this, SLOT(slotChangeMiner(int)));
	connect(ui->transBtn, SIGNAL(clicked(bool)), this, SLOT(slotClickTransBtn()));

	ui->valueEdit->setValidator(new QDoubleValidator(0, 10000, 8, this));
}

TransferWidget::~TransferWidget()
{
	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

void TransferWidget::show()
{
	QWidget::show();
	this->raise();
	this->activateWindow();
}

void TransferWidget::setAddress(QString address)
{
	QString left = address.left(6);
	QString right = address.right(4);
	ui->turnOutLabel->setText(left + "******" + right);
}

void TransferWidget::setEnergySum(QString energy)
{
	ui->energyLabel->setText(energy);
}

void TransferWidget::setPassWord(QString password)
{
	this->password = password;
}
void TransferWidget::setBuddyId(QString strId)
{
	ui->IDEdit->setText(strId);
}
void TransferWidget::setBuddyAddress(QString strAddress)
{
	ui->addressEdit->setText(strAddress);
}


void TransferWidget::slotClickTransBtn()
{
	QString otherID = ui->IDEdit->text();
	QString toAddress = ui->addressEdit->text();
	QString value = ui->valueEdit->text();
	double dvalue = value.toDouble();

	if (otherID.isEmpty() || toAddress.isEmpty() || value.isEmpty())
		IMessageBox::tip(NULL, tr("Notice"), tr("Input cannot be empty!"));
	else if (dvalue<=0)
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("Please enter valid number!"));
	}
	else
	{
		PasswordWidget *password = new PasswordWidget(this->password, this);
		connect(password, SIGNAL(sigPasswordTrue()), this, SLOT(slotTransfer()));
		password->show();
	}
	
}

void TransferWidget::slotTransfer()
{
	QString otherID = ui->IDEdit->text();
	QString toAddress = ui->addressEdit->text();
	toAddress = toAddress.trimmed();
	QString value = ui->valueEdit->text();
	QString miner = ui->minerLabel->text().split(" ").first();

	emit sigTransfer(otherID, toAddress, miner, value,m_bMove);
}

void TransferWidget::slotClearText()
{
	ui->IDEdit->clear();
	ui->addressEdit->clear();
	ui->valueEdit->clear();
	if (m_bMove)
	{
		close();
	}
}

void TransferWidget::slotChangeMiner(int value)
{
	QString strMiner;

	double v = value / double(10000);
	if (QString::number(value).endsWith("0"))
		strMiner = QString::number(v, 'f', 3);
	else
		strMiner = QString::number(v, 'f', 4);

	ui->minerLabel->setText(strMiner + " PWR");
}

void TransferWidget::InitAsWindow()
{
	QFile file(":/QSS/Resources/QSS/eWalletLib/transferwidget_dialog.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	ui->midWidget->setStyleSheet(styleSheet);
	file.close();

	ui->mBtnClose->show();
	m_bMove = true;
	connect(ui->mBtnClose, SIGNAL(clicked()), this, SLOT(close()));
}

//鼠标事件的处理。
void TransferWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void TransferWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void TransferWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0 || m_bMove == false)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

