﻿#include "passwordwidget.h"
#include "ui_passwordwidget.h"

#ifdef Q_OS_MAC
#include "mycommonstyle.h"
#endif
#include "QStringLiteralBak.h"

PasswordWidget::PasswordWidget(QString password, QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::PasswordWidget();

	ui->setupUi(this);

#ifdef Q_OS_MAC
    ui->lineEdit->setStyle(new MyProxyStyle);
#endif

	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog | Qt::Tool);
	setWindowModality(Qt::WindowModal);
	setAttribute(Qt::WA_DeleteOnClose);
	setAttribute(Qt::WA_TranslucentBackground);

	QFile file(":/QSS/Resources/QSS/eWalletLib/passwordwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	this->password = password;

	connect(ui->enterBtn, SIGNAL(clicked(bool)), this, SLOT(slotClickedEnterBtn()));
	connect(ui->closeBtn, SIGNAL(clicked(bool)), this, SLOT(close()));
}

PasswordWidget::~PasswordWidget()
{
	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

void PasswordWidget::show()
{
	QWidget::show();
	this->raise();
	this->activateWindow();
}

//鼠标事件的处理。
void PasswordWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void PasswordWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void PasswordWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

void PasswordWidget::slotClickedEnterBtn()
{
	QString text = ui->lineEdit->text();
	if (text.isEmpty())
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("Password cannot be empty!"));
	}
	else
	{
		QByteArray array = QCryptographicHash::hash(text.toUtf8(), QCryptographicHash::Sha1);
		QString string = array.toHex();

		if (string.toLower() != password.toLower())
			IMessageBox::tip(NULL, tr("Notice"), tr("Incorrect Password!"));
		else
		{
			emit sigPasswordTrue();
			close();
		}
	}
}
