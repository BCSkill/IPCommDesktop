﻿#include "safewidget.h"
#include "ui_safewidget.h"
#ifdef Q_OS_MAC
#include "mycommonstyle.h"
#endif
#include "QStringLiteralBak.h"

SafeWidget::SafeWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::SafeWidget();
	ui->setupUi(this);

#ifdef Q_OS_MAC
    ui->passwordEdit->setStyle(new MyProxyStyle);
#endif
	ui->passwordPage->hide();
	ui->backupPage->hide();
	ui->backupItem->installEventFilter(this);

	QFile file(":/QSS/Resources/QSS/eWalletLib/safewidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->enterBtn, SIGNAL(clicked(bool)), this, SLOT(slotEnterPassWord()));
	connect(ui->copyBtn, SIGNAL(clicked(bool)), this, SLOT(slotCopyPrivateKey()));
}

SafeWidget::~SafeWidget()
{
	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

void SafeWidget::setPassWord(QString passWord)
{
	this->passWord = passWord;
}

void SafeWidget::setPrivateKey(QString key)
{
	this->privateKey = key;
}

bool SafeWidget::eventFilter(QObject *obj, QEvent *event)
{
	if (obj == ui->backupItem && event->type() == QEvent::MouseButtonPress)
	{
		ui->backupPage->hide();
		ui->passwordPage->show();
		ui->passwordEdit->setFocus();
	}

	return QWidget::eventFilter(obj, event);
}

void SafeWidget::slotEnterPassWord()
{
	if (!passWord.isEmpty())
	{
		QString text = ui->passwordEdit->text();
		if (text.isEmpty())
			ui->tipLabel->setText(tr("Empty Password"));
		else
		{
			QByteArray array = QCryptographicHash::hash(text.toUtf8(), QCryptographicHash::Sha1);
			QString string = array.toHex();

			if (string.toLower() != passWord.toLower())
				ui->tipLabel->setText(tr("Incorrect Password"));
			else
			{
				ui->passwordEdit->clear();
				ui->tipLabel->clear();
				ui->passwordPage->hide();
				ui->plainTextEdit->setPlainText(privateKey);
				ui->backupPage->show();
			}
		}
	}
}

void SafeWidget::slotCopyPrivateKey()
{
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(ui->plainTextEdit->toPlainText());

	ui->backupPage->hide();
}
