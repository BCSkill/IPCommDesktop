﻿#include "chargewidget.h"
#include "ui_chargewidget.h"
#include "settings/NoFocusFrameDelegate.h"

ChargeWidget::ChargeWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::ChargeWidget();
	ui->setupUi(this);

	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog);
	setWindowModality(Qt::WindowModal);
	setAttribute(Qt::WA_DeleteOnClose);
	setAttribute(Qt::WA_TranslucentBackground);
	setWindowIcon(QIcon(":/Login/Resources/login/system.ico"));

	QFile file(":/QSS/Resources/QSS/eWalletLib/chargewidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->closeBtn, SIGNAL(clicked()), this, SLOT(close()));

	//用于下拉框item的样式美化
	ui->comboBox->setItemDelegate(new QStyledItemDelegate());
	ui->comboBox->setItemDelegate(new NoFocusFrameDelegate(this));
	ui->lineEdit->setValidator(new QDoubleValidator(0, 10000, 2, this));
}

ChargeWidget::~ChargeWidget()
{
	delete ui;
}

//鼠标事件的处理。
void ChargeWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void ChargeWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void ChargeWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}
