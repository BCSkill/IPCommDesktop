﻿#include "integralcardwidget.h"
#include "ui_integralcardwidget.h"
#include "QStringLiteralBak.h"
#include "oprequestsharelib.h"
#include <qdebug.h>

IntegralCardWidget::IntegralCardWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::IntegralCardWidget();
	ui->setupUi(this);

	QFile file(":/QSS/Resources/QSS/eWalletLib/integralcardwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	ui->pwrCardWidget->setData(":/ewallet/Resources/ewallet/pwrWidget/pwrBackground.png", ":/ewallet/Resources/ewallet/pwrWidget/pwrIcon.png");
	ui->whiteCardWidget->setData(":/ewallet/Resources/ewallet/pwrWidget/whiteBackground.png", ":/ewallet/Resources/ewallet/pwrWidget/whiteIcon.png");
	ui->blackCardWidget->setData(":/ewallet/Resources/ewallet/pwrWidget/blackBackground.png", ":/ewallet/Resources/ewallet/pwrWidget/blackIcon.png");
}

IntegralCardWidget::~IntegralCardWidget()
{
	delete ui;
}

void IntegralCardWidget::setPhoneNumber(QString phone)
{
	this->phoneNumber = phone;
}

void IntegralCardWidget::setAddress(QString strAddress)
{
	this->address = strAddress;
	strAddress = strAddress.left(6) + "****" + strAddress.right(4);
	ui->pwrCardWidget->setAddress(strAddress);
	ui->blackCardWidget->setAddress(strAddress);
	ui->whiteCardWidget->setAddress(strAddress);
}

void IntegralCardWidget::getCard()
{
	//清除旧的卡片。
	ui->scrollAreaWidgetContents->setMinimumHeight(0);
	foreach(CardItemWidget *cardItem, cardItems)
	{
		cardItem->close();
	}
	cardItems.clear();

	//从服务器获取卡片列表。
	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigGetCard(QList<CardInfo>)), this, SLOT(slotGetCard(QList<CardInfo>)));
	connect(request, SIGNAL(sigGetCard(QList<CardInfo>)), request, SLOT(deleteLater()));
	request->getCardList(phoneNumber, address);
}

void IntegralCardWidget::setPwrBalance(QString balance)
{
	balance = tr("Balance:") + balance;
	ui->pwrCardWidget->setBalance(balance);
}

void IntegralCardWidget::setWhiteBalance(QString balance)
{
	balance = tr("Balance:") + balance;
	ui->whiteCardWidget->setBalance(balance);
}

void IntegralCardWidget::setBlackBalance(QString balance)
{
	balance = tr("Balance:") + balance;
	ui->blackCardWidget->setBalance(balance);
}

void IntegralCardWidget::slotGetCard(QList<CardInfo> cards)
{
	int space = 8;  //界面上卡片间的空隙为8.
	//先根据要插入的卡片数量扩展显示区域的面积。
	int expandRows = cards.count() / 2;
	if (expandRows > 0)
	{
		int expandHeight = (ui->pwrCardWidget->height() + space) * expandRows;
		ui->scrollAreaWidgetContents->setMinimumHeight(ui->scrollAreaWidgetContents->height() + expandHeight);
	}
	for (int i = 1; i <= cards.count(); i++)
	{
		//计算卡片位置。
		int x = space;   //偶数个是在左侧，默认横坐标为space.
		if (i % 2 == 1)  //奇数个是在右侧。
		{
			x += ui->pwrCardWidget->width() + space;
		}
		int y = 0;
		int yCount = i / 2 + 1;
		y = (yCount + 1)*space + yCount*ui->pwrCardWidget->height();

		CardInfo card = cards.at(i - 1);
		CardItemWidget *cardItem = new CardItemWidget(ui->scrollAreaWidgetContents);
		cardItem->move(x, y);
		cardItem->setData(card.bgImg, card.cardImg);
		cardItem->setAddress(card.contractAddress.left(6) + "****" + card.contractAddress.right(4));
		cardItem->setBalance(tr("Balance:") + card.coinNumber);
		cardItem->show();
		cardItems.append(cardItem);
	}
}
