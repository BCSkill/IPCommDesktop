﻿#include "carditemwidget.h"
#include "ui_carditemwidget.h"
#include "httpnetworksharelib.h"
#include <qdir.h>

CardItemWidget::CardItemWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::CardItemWidget();
	ui->setupUi(this);

	QFile file(":/QSS/Resources/QSS/eWalletLib/carditemwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	setAttribute(Qt::WA_DeleteOnClose);

	connect(ui->rechargeBtn, SIGNAL(clicked()), this, SLOT(slotClickChargeBtn()));
}

CardItemWidget::~CardItemWidget()
{
	delete ui;
}

void CardItemWidget::setData(QString backPath, QString iconPath)
{
	if (backPath.startsWith(":")) //资源文件，直接加载。
	{
		ui->backLabel->setPixmap(QPixmap(backPath));
	}
	else     //网络图片，看本地是否已经存在。
	{
		QString fileName = backPath.split("/").last();
		QString tempFile = QDir::tempPath() + "/" + fileName;
		if (QFile(tempFile).exists())
			ui->backLabel->setPixmap(QPixmap(tempFile));
		else
		{
			HttpNetWork::HttpDownLoadFile *http = new HttpNetWork::HttpDownLoadFile;
			http->setObjectName(tempFile);
			connect(http, SIGNAL(sigDownFinished(bool)), this, SLOT(slotLoadBackImg()));
			http->StartDownLoadFile(backPath, tempFile);
		}
	}
	
	if (iconPath.startsWith(":"))
	{
		ui->iconLabel->setPixmap(QPixmap(iconPath));
	}
	else     //网络图片，看本地是否已经存在。
	{
		QString fileName = iconPath.split("/").last();
		QString tempFile = QDir::tempPath() + "/" + fileName;
		if (QFile(tempFile).exists())
			ui->iconLabel->setPixmap(QPixmap(tempFile));
		else
		{
			HttpNetWork::HttpDownLoadFile *http = new HttpNetWork::HttpDownLoadFile;
			http->setObjectName(tempFile);
			connect(http, SIGNAL(sigDownFinished(bool)), this, SLOT(slotLoadIconImg()));
			http->StartDownLoadFile(iconPath, tempFile);
		}
	}
}

void CardItemWidget::setAddress(QString address)
{
    ui->addressLabel->setText(address);
}

void CardItemWidget::setBalance(QString strBalance)
{
	ui->balanceLabel->setText(strBalance);
}

void CardItemWidget::slotLoadBackImg()
{
	HttpNetWork::HttpDownLoadFile *http = qobject_cast<HttpNetWork::HttpDownLoadFile *>(sender());
	QString filePath = http->objectName();
	ui->backLabel->setPixmap(QPixmap(filePath));
}

void CardItemWidget::slotLoadIconImg()
{
	HttpNetWork::HttpDownLoadFile *http = qobject_cast<HttpNetWork::HttpDownLoadFile *>(sender());
	QString filePath = http->objectName();
	ui->iconLabel->setPixmap(QPixmap(filePath));
}

void CardItemWidget::slotClickChargeBtn()
{
	ChargeWidget *chargeWidget = new ChargeWidget(this);
	chargeWidget->show();
}