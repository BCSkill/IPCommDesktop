<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="th_TH">
<context>
    <name>BTCManager</name>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="159"/>
        <location filename="BTCWidget/btcmanager.cpp" line="188"/>
        <location filename="BTCWidget/btcmanager.cpp" line="213"/>
        <location filename="BTCWidget/btcmanager.cpp" line="219"/>
        <location filename="BTCWidget/btcmanager.cpp" line="399"/>
        <location filename="BTCWidget/btcmanager.cpp" line="473"/>
        <location filename="BTCWidget/btcmanager.cpp" line="492"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="159"/>
        <source>BTC cannot be restored by private key for now, please use the mnemonic word</source>
        <translation>BTC ไม่สนับสนุนการกู้คืนคีย์ส่วนตัวในเวลานี้โปรดใช้ช่วยในการจำ</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="188"/>
        <source>Failed to restore the wallet!</source>
        <translation>การกู้คืนกระเป๋าเงินล้มเหลว!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="213"/>
        <source>The wallet restored successfully!</source>
        <translation>คืนค่ากระเป๋าเงินเรียบร้อยแล้ว!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="219"/>
        <source>The server returned error!</source>
        <translation>เซิร์ฟเวอร์ส่งคืนข้อผิดพลาด!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="228"/>
        <source>Refresh Wallet</source>
        <translation>รีเฟรชกระเป๋าเงิน</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="234"/>
        <source>Import Wallet</source>
        <translation>นำเข้ากระเป๋าเงิน</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="246"/>
        <source>Switch Wallet </source>
        <translation>สลับกระเป๋าเงิน</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="399"/>
        <source>The wallet deleted successfully!</source>
        <translation>ลบกระเป๋าเงินเรียบร้อยแล้ว!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="473"/>
        <source>There is no BTC wallet!</source>
        <translation>ไม่มีกระเป๋าเงิน BTC!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="492"/>
        <source>The opposite side have no account!</source>
        <translation>บุคคลอื่นไม่มีบัญชี!</translation>
    </message>
</context>
<context>
    <name>BTCTurnOutManager</name>
    <message>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="38"/>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="57"/>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="140"/>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="162"/>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="166"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="38"/>
        <source>Cannot find the private key of the wallet!</source>
        <translation>ไม่พบรหัสส่วนตัวของกระเป๋าเงิน!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="57"/>
        <source>Failed to get Utxo!</source>
        <translation>ไม่สามารถรับ Utxo!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="140"/>
        <source>Signature generation failed!</source>
        <translation>การสร้างลายเซ็นล้มเหลว!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="162"/>
        <source>Deal is Done!</source>
        <translation>การทำธุรกรรมสำเร็จ!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="166"/>
        <source>Deal Failed!</source>
        <translation>ธุรกรรมล้มเหลว!</translation>
    </message>
</context>
<context>
    <name>BTCWidget</name>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="14"/>
        <source>BTCWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="131"/>
        <source>BTC</source>
        <translation></translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="173"/>
        <source>Restore Wallet</source>
        <translation>กระเป๋าเงินคืน</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="275"/>
        <source>manage wallet</source>
        <translation>จัดการกระเป๋าเงิน</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="350"/>
        <source>Copy</source>
        <translation>สำเนา</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="383"/>
        <source>Copy successed</source>
        <translation>คัดลอกสำเร็จแล้ว</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="467"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="532"/>
        <source>Transfer In</source>
        <translation>เข้าไป</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="578"/>
        <source>Details</source>
        <translation>รายละเอียด</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="624"/>
        <source>Transfer Out</source>
        <translation>โอนออก</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="654"/>
        <source>assets</source>
        <translation>สินทรัพย์</translation>
    </message>
</context>
<context>
    <name>CardItemWidget</name>
    <message>
        <location filename="pwrWidget/integralCardWidget/carditemwidget.ui" line="26"/>
        <source>CardItemWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/carditemwidget.ui" line="96"/>
        <source>0xd0ee****d73e</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/carditemwidget.ui" line="120"/>
        <source>Balance:0</source>
        <translation>สมดุล:0</translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/carditemwidget.ui" line="148"/>
        <source> Top Up &gt;</source>
        <translation>เติมเงิน&gt;</translation>
    </message>
</context>
<context>
    <name>ChangeIntoWidget</name>
    <message>
        <location filename="pwrWidget/changeintoWidget/changeintowidget.ui" line="14"/>
        <source>ChangeIntoWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/changeintoWidget/changeintowidget.ui" line="77"/>
        <source>Scan the QR code to exchange power</source>
        <translation>สแกนพลังงานแลกเปลี่ยนรหัส QR</translation>
    </message>
    <message>
        <location filename="pwrWidget/changeintoWidget/changeintowidget.ui" line="214"/>
        <location filename="pwrWidget/changeintoWidget/changeintowidget.ui" line="220"/>
        <source>Set Aomunt</source>
        <translation>กำหนดจำนวนเงิน</translation>
    </message>
    <message>
        <location filename="pwrWidget/changeintoWidget/changeintowidget.ui" line="251"/>
        <location filename="pwrWidget/changeintoWidget/changeintowidget.ui" line="306"/>
        <source>Exchange Records</source>
        <translation>แลกเปลี่ยนบันทึก</translation>
    </message>
</context>
<context>
    <name>ChangeNumWidget</name>
    <message>
        <location filename="pwrWidget/changeintoWidget/changenumwidget.ui" line="14"/>
        <source>ChangeNumWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/changeintoWidget/changenumwidget.ui" line="71"/>
        <source>Amount</source>
        <translation>ปริมาณ</translation>
    </message>
    <message>
        <location filename="pwrWidget/changeintoWidget/changenumwidget.ui" line="167"/>
        <source>OK</source>
        <translation>ตกลง</translation>
    </message>
</context>
<context>
    <name>ChargeWidget</name>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="14"/>
        <source>Charge</source>
        <translation>เติมเงิน</translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="61"/>
        <source>Top Up</source>
        <translation>เติมเงิน</translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="130"/>
        <source>Top-Up Way</source>
        <translation>วิธีการเติมเงิน</translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="158"/>
        <source>Alipay</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="167"/>
        <source>WeChat</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="194"/>
        <source>Daily max amount￥50000.00</source>
        <translation>จำนวนการทำธุรกรรมรายวัน¥ 50000.00</translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="229"/>
        <source>Amount</source>
        <translation>ปริมาณ</translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="278"/>
        <source>￥</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="349"/>
        <source>Next</source>
        <translation>ถัดไป</translation>
    </message>
</context>
<context>
    <name>DetailsWidget</name>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="14"/>
        <source>DetailsWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="100"/>
        <source>Exchange Detail</source>
        <translation>รายละเอียดการทำธุรกรรม</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="185"/>
        <source>Exchange successed!</source>
        <translation>การทำธุรกรรมสำเร็จ!</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="244"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="260"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="276"/>
        <source>Power</source>
        <translation>อำนาจ</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="348"/>
        <source>Exchange ID</source>
        <translation>รหัสธุรกรรม</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="436"/>
        <source>Exchange Time</source>
        <translation>เวลาซื้อขาย</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="521"/>
        <source>Account Send</source>
        <translation>โอนบัญชี</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="606"/>
        <source>Account Receive</source>
        <translation>รับบัญชี</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="680"/>
        <source>OK</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DeviceItemWidget</name>
    <message>
        <location filename="pwrWidget/myDeviceWidget/deviceitemwidget.ui" line="14"/>
        <source>DeviceItemWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/myDeviceWidget/deviceitemwidget.ui" line="89"/>
        <source>ONEPLUS A600</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/myDeviceWidget/deviceitemwidget.ui" line="111"/>
        <source>2019-09-30 11:31:19</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/myDeviceWidget/deviceitemwidget.ui" line="144"/>
        <source>This Device</source>
        <translation>เครื่อง</translation>
    </message>
    <message>
        <location filename="pwrWidget/myDeviceWidget/deviceitemwidget.cpp" line="46"/>
        <source>Offline</source>
        <translation>ออฟไลน์</translation>
    </message>
</context>
<context>
    <name>EOSManager</name>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="135"/>
        <location filename="EOSWidget/eosmanager.cpp" line="168"/>
        <location filename="EOSWidget/eosmanager.cpp" line="172"/>
        <location filename="EOSWidget/eosmanager.cpp" line="240"/>
        <location filename="EOSWidget/eosmanager.cpp" line="417"/>
        <location filename="EOSWidget/eosmanager.cpp" line="436"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="135"/>
        <source>Account Verification Failed!</source>
        <translation>การยืนยันบัญชีล้มเหลว!</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="168"/>
        <source>EOS Wallet Restored Successfully!</source>
        <translation>กู้คืน EOS Wallet สำเร็จแล้ว!</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="172"/>
        <source>Failed to Restore EOS Wallet!</source>
        <translation>ไม่สามารถกู้คืน EOS Wallet ได้!</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="240"/>
        <source>Wallet Deleted Successfully!</source>
        <translation>ลบกระเป๋าเงินเรียบร้อยแล้ว!</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="266"/>
        <source>Refresh Wallet</source>
        <translation>รีเฟรชกระเป๋าเงิน</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="272"/>
        <source>Import Wallet</source>
        <translation>นำเข้ากระเป๋าเงิน</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="283"/>
        <source>Switch Wallet </source>
        <translation>สลับกระเป๋าเงิน</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="417"/>
        <source>No EOS Wallet!</source>
        <translation>ไม่มีกระเป๋าเงิน EOS!</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="436"/>
        <source>The opposite side have no account!</source>
        <translation>ฝั่งตรงข้ามไม่มีบัญชี!</translation>
    </message>
</context>
<context>
    <name>EOSRecoveryWidget</name>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.ui" line="14"/>
        <source>EOS Recovery</source>
        <translation>การกู้คืนกระเป๋าเงินของ EOS</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.ui" line="79"/>
        <source>Restore Wallet Account</source>
        <translation>กระเป๋าเงินคืน</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.ui" line="131"/>
        <source>Restore Your Wallet Accout By Private Key</source>
        <translation>คืนค่าบัญชีเงินฝากของคุณด้วยรหัสส่วนตัว</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.ui" line="165"/>
        <source>Please Enter OWN Private Key</source>
        <translation>กรุณาใส่รหัสส่วนตัว OWN</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.ui" line="190"/>
        <source>Please Enter ACTIVE Private Key</source>
        <translation>กรุณาใส่รหัสส่วนตัว ACTIVE</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.ui" line="215"/>
        <source>Finished</source>
        <translation>เสร็จ</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.cpp" line="62"/>
        <location filename="EOSWidget/eosrecoverywidget.cpp" line="72"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.cpp" line="62"/>
        <source>Private Key not Entered Completely!</source>
        <translation>รหัสส่วนตัวไม่ถูกป้อนอย่างสมบูรณ์!</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.cpp" line="72"/>
        <source>Incorrect Private Key，Failed to Get Public Key!</source>
        <translation>รหัสส่วนตัวไม่ถูกต้องล้มเหลวในการรับกุญแจสาธารณะ!</translation>
    </message>
</context>
<context>
    <name>EOSTurnOutManager</name>
    <message>
        <location filename="EOSWidget/eosturnoutmanager.cpp" line="24"/>
        <location filename="EOSWidget/eosturnoutmanager.cpp" line="87"/>
        <location filename="EOSWidget/eosturnoutmanager.cpp" line="90"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosturnoutmanager.cpp" line="24"/>
        <source>Failed to get the private key of EOS wallet!</source>
        <translation>ไม่สามารถรับกุญแจส่วนตัวของกระเป๋าเงิน EOS ได้!</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosturnoutmanager.cpp" line="87"/>
        <source>Deal is Done!</source>
        <translation>ธุรกรรมที่ประสบความสำเร็จ</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosturnoutmanager.cpp" line="90"/>
        <source>Deal Failed!</source>
        <translation>ธุรกรรมล้มเหลว</translation>
    </message>
</context>
<context>
    <name>EOSWidget</name>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="14"/>
        <source>EOSWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="131"/>
        <source>EOS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="173"/>
        <source>Restore Wallet</source>
        <translation>กระเป๋าเงินคืน</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="275"/>
        <source>manage wallet</source>
        <translation>จัดการกระเป๋าเงิน</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="350"/>
        <source>Copy</source>
        <translation>สำเนา</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="383"/>
        <source>Copy successed</source>
        <translation>คัดลอกสำเร็จแล้ว</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="467"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="532"/>
        <source>Transfer In</source>
        <translation>เข้าไป</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="581"/>
        <source>Details</source>
        <translation>รายละเอียด</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="627"/>
        <source>Transfer Out</source>
        <translation>โอนออก</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="657"/>
        <source>assets</source>
        <translation>สินทรัพย์</translation>
    </message>
</context>
<context>
    <name>ETHManager</name>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="258"/>
        <location filename="ETHWidget/ethmanager.cpp" line="283"/>
        <location filename="ETHWidget/ethmanager.cpp" line="289"/>
        <location filename="ETHWidget/ethmanager.cpp" line="588"/>
        <location filename="ETHWidget/ethmanager.cpp" line="595"/>
        <location filename="ETHWidget/ethmanager.cpp" line="616"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="258"/>
        <source>Failed to restore wallet!</source>
        <translation>การกู้คืนกระเป๋าเงินล้มเหลว!</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="283"/>
        <source>Wallet restored successfully!</source>
        <translation>คืนค่ากระเป๋าเงินเรียบร้อยแล้ว!</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="289"/>
        <source>The server returned error!</source>
        <translation>เซิร์ฟเวอร์ส่งคืนข้อผิดพลาด!</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="298"/>
        <source>Refresh Wallet</source>
        <translation>รีเฟรชกระเป๋าเงิน</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="304"/>
        <source>Import Wallet</source>
        <translation>นำเข้ากระเป๋าเงิน</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="315"/>
        <source>Switch Wallet </source>
        <translation>สลับกระเป๋าเงิน</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="588"/>
        <source>Wallet Deleted Successfully!</source>
        <translation>ลบกระเป๋าเงินเรียบร้อยแล้ว!</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="595"/>
        <source>No ETH Wallet!</source>
        <translation>ไม่มีกระเป๋าเงิน ETH!</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="616"/>
        <source>The opposite side have no account!</source>
        <translation>บุคคลอื่นไม่มีบัญชี!</translation>
    </message>
</context>
<context>
    <name>ETHTokenListWidget</name>
    <message>
        <location filename="ETHWidget/ethtokenlistwidget.ui" line="14"/>
        <source>ETHTokenListWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ETHWidget/ethtokenlistwidget.ui" line="77"/>
        <source>Replace Assets</source>
        <translation>การทดแทนสินทรัพย์</translation>
    </message>
</context>
<context>
    <name>ETHTurnOutManager</name>
    <message>
        <location filename="ETHWidget/ethturnoutmanager.cpp" line="50"/>
        <location filename="ETHWidget/ethturnoutmanager.cpp" line="131"/>
        <location filename="ETHWidget/ethturnoutmanager.cpp" line="185"/>
        <source>Deal Failed</source>
        <translation>ธุรกรรมล้มเหลว</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutmanager.cpp" line="50"/>
        <source>Error occured when getting Nonce</source>
        <translation>เกิดข้อผิดพลาดในการรับ Nonce</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutmanager.cpp" line="131"/>
        <source>Invalid Private Key</source>
        <translation>รหัสส่วนตัวไม่ถูกต้อง</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutmanager.cpp" line="180"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutmanager.cpp" line="180"/>
        <source>Deal is Done!</source>
        <translation>การทำธุรกรรมสำเร็จ!</translation>
    </message>
</context>
<context>
    <name>ETHTurnOutWidget</name>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="14"/>
        <source>ETHTurnOutWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="115"/>
        <source>ETH Balance</source>
        <translation>ยอดคงเหลือ ETH</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="144"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="181"/>
        <source>Amount</source>
        <translation>ปริมาณ</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="203"/>
        <source>Notice(optional)</source>
        <translation>แจ้งให้ทราบล่วงหน้า (อุปกรณ์เสริม)</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="225"/>
        <source>Receive Address</source>
        <translation>ที่อยู่ในการเก็บรวบรวม</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="257"/>
        <source>Enter Amount</source>
        <translation>ใส่จำนวนเงิน</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="282"/>
        <source>Enter Notice</source>
        <translation>ใส่จำนวนเงิน</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="321"/>
        <source>Enter  the receiver&apos;s account</source>
        <translation>ป้อนบัญชีของผู้รับ</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="415"/>
        <source>Payment Address</source>
        <translation>ที่อยู่การชำระเงิน</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="507"/>
        <source>Cost of Miners</source>
        <translation>ค่าใช้จ่ายขุด</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="523"/>
        <source>0.0004 PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="549"/>
        <source>0.0004</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="587"/>
        <source>0.006</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="644"/>
        <source>OK</source>
        <translation>ตกลง</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.cpp" line="97"/>
        <location filename="ETHWidget/ethturnoutwidget.cpp" line="101"/>
        <location filename="ETHWidget/ethturnoutwidget.cpp" line="146"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.cpp" line="97"/>
        <source>The amount and the address cannot be empty!</source>
        <translation>จำนวนและที่อยู่ต้องไม่ว่างเปล่า!</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.cpp" line="101"/>
        <source>Please enter valid number!</source>
        <translation>กรุณาใส่หมายเลขที่ถูกต้อง!</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.cpp" line="107"/>
        <source>Please enter login password:</source>
        <translation>กรุณาใส่รหัสผ่านเข้าสู่ระบบ:</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.cpp" line="146"/>
        <source>Incorrect password!</source>
        <translation>รหัสผ่านไม่ถูกต้อง!</translation>
    </message>
</context>
<context>
    <name>ETHWidget</name>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="14"/>
        <source>ETHWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="131"/>
        <source>ETH</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="173"/>
        <source>Restore Wallet</source>
        <translation>กระเป๋าเงินคืน</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="275"/>
        <source>manage wallet</source>
        <translation>จัดการกระเป๋าเงิน</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="350"/>
        <source>Copy</source>
        <translation>สำเนา</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="383"/>
        <source>Copy Successed</source>
        <translation>คัดลอกสำเร็จแล้ว</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="467"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="532"/>
        <source>Transfer In</source>
        <translation>เข้าไป</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="565"/>
        <source>Details</source>
        <translation>รายละเอียด</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="598"/>
        <source>Transfer Out</source>
        <translation>โอนออก</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="628"/>
        <source>assets</source>
        <translation>สินทรัพย์</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="663"/>
        <source>add</source>
        <translation>เพิ่ม</translation>
    </message>
</context>
<context>
    <name>EWalletAssetsWidget</name>
    <message>
        <location filename="ewalletassetswidget.ui" line="14"/>
        <source>EWalletAssetsWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ewalletassetswidget.ui" line="104"/>
        <source>Refresh</source>
        <translation>รีเฟรช</translation>
    </message>
    <message>
        <location filename="ewalletassetswidget.ui" line="128"/>
        <source>Total Assets(CNY)</source>
        <translation>สินทรัพย์รวม (CNY)</translation>
    </message>
    <message>
        <location filename="ewalletassetswidget.ui" line="147"/>
        <source>0.00</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ewalletassetswidget.cpp" line="80"/>
        <source>≈</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>EWalletManager</name>
    <message>
        <location filename="ewalletmanager.cpp" line="209"/>
        <source>Wallet </source>
        <translation>กระเป๋าสตางค์</translation>
    </message>
</context>
<context>
    <name>EWalletWidget</name>
    <message>
        <location filename="ewalletwidget.ui" line="20"/>
        <source>EWalletWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="234"/>
        <source>Power</source>
        <translation>พลังงาน</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="258"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="649"/>
        <source>Transfer Out</source>
        <translation>โอนออก</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="765"/>
        <source>Transfer In</source>
        <translation>เข้าไป</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="928"/>
        <source>Transaction Details</source>
        <translation>รายละเอียดการทำธุรกรรม</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1027"/>
        <source>Send Tokens</source>
        <translation>ส่งโทเค็น</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1132"/>
        <source>Safety Reinforcement</source>
        <translation>ความปลอดภัยชุบแข็ง</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1237"/>
        <source>Score Card</source>
        <translation>บัตรคะแนน</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1336"/>
        <source>Send Red Packet</source>
        <translation>ซองจดหมายสีแดง</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1435"/>
        <source>Escrow Account</source>
        <translation>บัญชีที่โฮสต์</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1534"/>
        <source>My Device(s)</source>
        <translation>อุปกรณ์ของฉัน</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1650"/>
        <source>Assets</source>
        <translation>สินทรัพย์รวม</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1696"/>
        <source>PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1742"/>
        <source>ETH</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1785"/>
        <source>BTC</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1828"/>
        <source>EOS</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>HostingAccoutWidget</name>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="14"/>
        <source>HostingAccoutWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="72"/>
        <source>Hosting Account</source>
        <translation>บัญชีที่โฮสต์</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="157"/>
        <source>Return</source>
        <translation>กลับ</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="163"/>
        <source>←</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="179"/>
        <source>ETH</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="252"/>
        <source>All Records</source>
        <translation>บันทึกทั้งหมด</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="292"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="441"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="226"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="250"/>
        <source>Top Up</source>
        <translation>เติมเงิน</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="332"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="490"/>
        <source>Withdraw</source>
        <translation>โอนออก</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="369"/>
        <source>Others</source>
        <translation>อื่น ๆ</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="224"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="294"/>
        <source>Get Red Packet</source>
        <translation>รับซองจดหมายสีแดง</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="228"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="272"/>
        <source>WithDraw</source>
        <translation>โอนออก</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="232"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="254"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="276"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="298"/>
        <source>Confirmed</source>
        <translation>ได้รับการยืนยัน</translation>
    </message>
</context>
<context>
    <name>HostingChargeWidget</name>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="14"/>
        <source>Hosting Charge</source>
        <translation>เติมเงินในบัญชีที่มีการจัดการ</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="101"/>
        <source>PWR Tranfer</source>
        <translation>การถ่ายโอน PWR</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="163"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="168"/>
        <source>Balance</source>
        <translation>สมดุล</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="178"/>
        <source>0.00000000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="199"/>
        <source>PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="241"/>
        <source>Address</source>
        <translation>ที่อยู่</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="260"/>
        <source>Hosting Address</source>
        <translation>บัญชีที่โฮสต์</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="302"/>
        <source>Amount</source>
        <translation>ปริมาณ</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="327"/>
        <source>Enter Amount</source>
        <translation>ใส่จำนวนเงิน</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="389"/>
        <source>Payment Address</source>
        <translation>ที่อยู่การชำระเงิน</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="492"/>
        <source>Cost of Miners</source>
        <translation>ค่าใช้จ่ายขุด</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="514"/>
        <source>0.0004 PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="576"/>
        <source>Next</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="49"/>
        <source>Transfer</source>
        <translation>โอน</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="57"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="108"/>
        <source>Wallet</source>
        <translation>กระเป๋าสตางค์</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="157"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="163"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="168"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="157"/>
        <source>Payment address cannot be empty</source>
        <translation>ที่อยู่การชำระเงินต้องไม่ว่างเปล่า</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="163"/>
        <source>Amount cannot be empty</source>
        <translation>จำนวนต้องไม่ว่างเปล่า</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="168"/>
        <source>Amount is greater than</source>
        <translation>จำนวนเงินโอนเกิน</translation>
    </message>
</context>
<context>
    <name>HostingCoinWidget</name>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="14"/>
        <source>Hosting Coin</source>
        <translation>การถอนบัญชีที่มีการจัดการ</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="110"/>
        <source>Withdraw</source>
        <translation>ถอนตัว</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="220"/>
        <source>Hosting Account</source>
        <translation>ถอนตัวบัญชีที่โฮสต์</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="237"/>
        <source>---------&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="293"/>
        <source>Local Wallet</source>
        <translation>กระเป๋าเงินท้องถิ่น</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="350"/>
        <source>Available</source>
        <translation>ที่มีจำหน่าย</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="363"/>
        <source>0PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="384"/>
        <source>Widthdraw All</source>
        <translation>ถอนตัวเต็ม</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="444"/>
        <source>Receive Address</source>
        <translation>ที่อยู่ในการเก็บรวบรวม</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="520"/>
        <source>Amount</source>
        <translation>ปริมาณ</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="548"/>
        <source>Enter Amount</source>
        <translation>ใส่จำนวนเงิน</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="626"/>
        <source>Poundage</source>
        <translation>ค่าธรรมเนียมการจัดการ</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="648"/>
        <source>0.002 PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="710"/>
        <source>Wthdraw</source>
        <translation>โอนออก</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="58"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="98"/>
        <source>Wallet</source>
        <translation>กระเป๋าสตางค์</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="132"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="138"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="143"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="166"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="169"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="132"/>
        <source>Receive address cannot be empty</source>
        <translation>ที่อยู่ในการเก็บต้องไม่ว่างเปล่า</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="138"/>
        <source>Amount cannot be empty</source>
        <translation>จำนวนต้องไม่ว่างเปล่า</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="143"/>
        <source>Amount is greater than available assets</source>
        <translation>จำนวนมากกว่าสินทรัพย์ที่มีอยู่</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="166"/>
        <source>Withdraw completed, Please check your balance later</source>
        <translation>ถอนออกแล้วโปรดตรวจสอบยอดเงินของคุณในภายหลัง</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="169"/>
        <source>Withdraw failed</source>
        <translation>การถอนเงินล้มเหลว</translation>
    </message>
</context>
<context>
    <name>HostingItemWidget</name>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingitemwidget.ui" line="14"/>
        <source>HostingItemWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingitemwidget.ui" line="83"/>
        <source>pwr</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingitemwidget.ui" line="107"/>
        <source>11.00000000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingitemwidget.ui" line="126"/>
        <source>0.00000000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingitemwidget.ui" line="155"/>
        <source>Available</source>
        <translation>ที่มีจำหน่าย</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingitemwidget.ui" line="171"/>
        <source>Frozen</source>
        <translation>แช่แข็ง</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingitemwidget.ui" line="204"/>
        <source>details</source>
        <translation>รายละเอียด</translation>
    </message>
</context>
<context>
    <name>IntegralCardWidget</name>
    <message>
        <location filename="pwrWidget/integralCardWidget/integralcardwidget.ui" line="14"/>
        <source>IntegralCardWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/integralcardwidget.cpp" line="62"/>
        <location filename="pwrWidget/integralCardWidget/integralcardwidget.cpp" line="68"/>
        <location filename="pwrWidget/integralCardWidget/integralcardwidget.cpp" line="74"/>
        <location filename="pwrWidget/integralCardWidget/integralcardwidget.cpp" line="105"/>
        <source>Balance:</source>
        <translation>สมดุล:</translation>
    </message>
</context>
<context>
    <name>MyDeviceWidget</name>
    <message>
        <location filename="pwrWidget/myDeviceWidget/mydevicewidget.ui" line="14"/>
        <source>MyDeviceWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/myDeviceWidget/mydevicewidget.ui" line="65"/>
        <source>My Device(s)</source>
        <translation>อุปกรณ์ของฉัน</translation>
    </message>
</context>
<context>
    <name>PasswordWidget</name>
    <message>
        <location filename="pwrWidget/transferWidget/passwordwidget.ui" line="14"/>
        <source>PasswordWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/passwordwidget.ui" line="121"/>
        <source>Please Enter Your Base Password:</source>
        <translation>กรุณาใส่รหัสผ่านฐานของคุณ:</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/passwordwidget.ui" line="225"/>
        <source>OK</source>
        <translation>ตกลง</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/passwordwidget.cpp" line="81"/>
        <location filename="pwrWidget/transferWidget/passwordwidget.cpp" line="89"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/passwordwidget.cpp" line="81"/>
        <source>Password cannot be empty!</source>
        <translation>รหัสผ่านต้องไม่ว่างเปล่า!</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/passwordwidget.cpp" line="89"/>
        <source>Incorrect Password!</source>
        <translation>รหัสผ่านไม่ถูกต้อง!</translation>
    </message>
</context>
<context>
    <name>RecordDetailPerWidget</name>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="14"/>
        <source>RecordDetailPerWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="77"/>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="188"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="238"/>
        <source>Outflow tansfer successed</source>
        <translation>ธุรกรรมถูกโอนสำเร็จแล้ว</translation>
    </message>
    <message>
        <source>The opposite side&apos;s interPlanet   ID:</source>
        <translation type="vanished">ID ดวงดาวของอีกฝ่าย:</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="308"/>
        <source>The opposite side&apos;s ID:</source>
        <oldsource>The opposite side&apos;s Telecomm ID:</oldsource>
        <translation>ID ฝั่งตรงข้าม:</translation>
    </message>
    <message>
        <source>Base ID:</source>
        <translation type="vanished">ID ฐาน:</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="385"/>
        <source>Acct.No</source>
        <translation>บัญชี</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="474"/>
        <source>Time:</source>
        <translation>เวลา:</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="566"/>
        <source>Tranfer ID:</source>
        <translation>โอน ID:</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="677"/>
        <source>My ID:</source>
        <oldsource>My personal ID:</oldsource>
        <translation>ID ของฉัน:</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="751"/>
        <source>Acct.No:</source>
        <translation>บัญชี:</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="877"/>
        <source>OK</source>
        <translation>ตกลง</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.cpp" line="68"/>
        <source>Transfer in successed</source>
        <translation>โอนสำเร็จแล้ว</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.cpp" line="79"/>
        <source>Tansfer out successed</source>
        <translation>โอนออกสำเร็จแล้ว</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.cpp" line="88"/>
        <source>Records of PWR Transfer</source>
        <translation>บันทึกการโอน PWR</translation>
    </message>
</context>
<context>
    <name>RecordDetailWidget</name>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="14"/>
        <source>RecordDetailWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="97"/>
        <source>Details</source>
        <translation>รายละเอียด</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="338"/>
        <source>Amount</source>
        <translation>ปริมาณ</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="429"/>
        <source>Cost of Miners</source>
        <translation>ค่าใช้จ่ายขุด</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="511"/>
        <source>Receive Address</source>
        <translation>ที่อยู่ในการเก็บรวบรวม</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="611"/>
        <source>Payment Address</source>
        <translation>ที่อยู่การชำระเงิน</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="711"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="793"/>
        <source>Deal Number</source>
        <translation>หมายเลขดีล</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="872"/>
        <source>Block</source>
        <translation>บล็อก</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="937"/>
        <source>OK</source>
        <translation>ตกลง</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.cpp" line="63"/>
        <source> Transfer in successed</source>
        <translation>โอนสำเร็จแล้ว</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.cpp" line="66"/>
        <source> Transfer out successed</source>
        <translation>โอนออกสำเร็จแล้ว</translation>
    </message>
</context>
<context>
    <name>RecordsWidget</name>
    <message>
        <location filename="childrenWidget/recordswidget.ui" line="14"/>
        <source>RecordsWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/recordswidget.ui" line="146"/>
        <source>All</source>
        <translation>สมบูรณ์</translation>
    </message>
    <message>
        <location filename="childrenWidget/recordswidget.ui" line="180"/>
        <location filename="childrenWidget/recordswidget.ui" line="334"/>
        <source>Transfer Out</source>
        <translation>โอนออก</translation>
    </message>
    <message>
        <location filename="childrenWidget/recordswidget.ui" line="211"/>
        <location filename="childrenWidget/recordswidget.ui" line="307"/>
        <source>Transfer In</source>
        <translation>เข้าไป</translation>
    </message>
    <message>
        <location filename="childrenWidget/recordswidget.ui" line="242"/>
        <source>failure</source>
        <translation>ความล้มเหลว</translation>
    </message>
</context>
<context>
    <name>SafeWidget</name>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.ui" line="14"/>
        <source>SafeWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.ui" line="164"/>
        <source>Backup private key</source>
        <translation>สำรองข้อมูลรหัสส่วนตัว</translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.ui" line="295"/>
        <source>Please enter your base password</source>
        <translation>กรุณาใส่รหัสผ่านฐานของคุณ:</translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.ui" line="391"/>
        <source>OK</source>
        <translation>ตกลง</translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.ui" line="451"/>
        <source>Back up private key</source>
        <translation>สำรองข้อมูลรหัสส่วนตัว</translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.ui" line="484"/>
        <source>Security warning:The private key is not encrypted,the export is risky,It is recommended to use mnemonic for backup</source>
        <translation>คำเตือนด้านความปลอดภัย: รหัสส่วนตัวไม่ได้เข้ารหัสการส่งออกมีความเสี่ยงขอแนะนำให้ใช้ตัวช่วยจำสำหรับการสำรองข้อมูล</translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.ui" line="570"/>
        <source>Copy</source>
        <translation>สำเนา</translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.cpp" line="68"/>
        <source>Empty Password</source>
        <translation>รหัสผ่านว่างเปล่า</translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.cpp" line="75"/>
        <source>Incorrect Password</source>
        <translation>รหัสผ่านไม่ถูกต้อง</translation>
    </message>
</context>
<context>
    <name>StarFlashWidget</name>
    <message>
        <location filename="starflashwidget.ui" line="32"/>
        <source>StarFlashWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="79"/>
        <source>164953236</source>
        <translation></translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="101"/>
        <source>Refresh</source>
        <translation>รีเฟรช</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="158"/>
        <source>ID 190024</source>
        <oldsource>Personal ID 190024</oldsource>
        <translation>ID ส่วนบุคคล 190024</translation>
    </message>
    <message>
        <source>Base ID </source>
        <translation type="vanished">ID ฐาน</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="184"/>
        <source>Acct.No</source>
        <translation>บัญชี</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="215"/>
        <source>Copy</source>
        <translation>สำเนา</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="248"/>
        <source>Copy Successed</source>
        <translation>คัดลอกสำเร็จแล้ว</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="299"/>
        <location filename="starflashwidget.ui" line="340"/>
        <location filename="starflashwidget.ui" line="381"/>
        <location filename="starflashwidget.ui" line="422"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="320"/>
        <source>Friend(s)</source>
        <translation>เพื่อน</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="361"/>
        <source>Group(s)</source>
        <oldsource>Tribe(s)</oldsource>
        <translation>กลุ่ม</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="402"/>
        <source>Invitation(s)</source>
        <translation>เชิญ</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="443"/>
        <source>Application(s)</source>
        <translation>ใบสมัคร</translation>
    </message>
    <message>
        <source>Personal ID </source>
        <translation type="vanished">ID ส่วนบุคคล</translation>
    </message>
    <message>
        <location filename="starflashwidget.cpp" line="83"/>
        <location filename="starflashwidget.cpp" line="92"/>
        <source>ID </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TransferManager</name>
    <message>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="68"/>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="165"/>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="218"/>
        <source>Deal Failed</source>
        <translation>ธุรกรรมล้มเหลว</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="68"/>
        <source>Error occured when getting Nonce</source>
        <translation>เกิดข้อผิดพลาดในการรับ Nonce</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="165"/>
        <source>Invalid private key</source>
        <translation>รหัสส่วนตัวไม่ถูกต้อง</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="225"/>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="228"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="225"/>
        <source>Top up successed，Please check your balance in your hosting account later</source>
        <translation>เติมเงินสำเร็จกรุณาตรวจสอบยอดเงินคงเหลือในบัญชีโฮสติ้งของคุณในภายหลัง</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="228"/>
        <source>Top up failed</source>
        <translation>การเติมล้มเหลว</translation>
    </message>
</context>
<context>
    <name>TransferWidget</name>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="20"/>
        <source>TransferWidget</source>
        <translation></translation>
    </message>
    <message>
        <source>Receiver&apos;s interPlanet ID</source>
        <translation type="vanished">ID ดวงดาวของผู้รับ</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="138"/>
        <source>Receiver&apos;s ID</source>
        <oldsource>Receiver&apos;s Telecomm ID</oldsource>
        <translation>ID ของผู้รับ</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="160"/>
        <source>Receiver&apos;s Acct.No</source>
        <oldsource>Receiver&apos;s base ID</oldsource>
        <translation>บัญชีของผู้รับ</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="182"/>
        <source>Amount</source>
        <translation>ปริมาณ</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="220"/>
        <source>Enter account number</source>
        <translation>ป้อนหมายเลขบัญชี</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="251"/>
        <source>Enter wallet address</source>
        <translation>ป้อนที่อยู่กระเป๋าเงิน</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="276"/>
        <source>Enter the amount of power</source>
        <translation>ป้อนปริมาณพลังงาน</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="320"/>
        <source>Sender&apos;s Acct.No</source>
        <oldsource>Sender&apos;s base ID</oldsource>
        <translation>บัญชีของผู้ส่ง</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="396"/>
        <source>Cost of Miners</source>
        <translation>ค่าใช้จ่ายขุด</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="412"/>
        <source>0.0004 PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="438"/>
        <source>0.0004</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="476"/>
        <source>0.006</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="502"/>
        <source>Power Balance</source>
        <translation>สมดุลพลังงาน</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="531"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="588"/>
        <source>Power Transfer</source>
        <translation>การแลกเปลี่ยนพลังงาน</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.cpp" line="100"/>
        <location filename="pwrWidget/transferWidget/transferwidget.cpp" line="103"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.cpp" line="100"/>
        <source>Input cannot be empty!</source>
        <translation>ข้อมูลที่ป้อนต้องไม่ว่างเปล่า!</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.cpp" line="103"/>
        <source>Please enter valid number!</source>
        <translation>กรุณาใส่หมายเลขที่ถูกต้อง!</translation>
    </message>
</context>
<context>
    <name>TurnInWidget</name>
    <message>
        <location filename="childrenWidget/turninwidget.ui" line="14"/>
        <source>TurnInWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/turninwidget.ui" line="314"/>
        <source>Replace
assets</source>
        <translation>การทดแทนสินทรัพย์</translation>
    </message>
    <message>
        <location filename="childrenWidget/turninwidget.ui" line="350"/>
        <source>Specify
the amount</source>
        <translation>จำนวนเงินที่ระบุ</translation>
    </message>
    <message>
        <location filename="childrenWidget/turninwidget.cpp" line="56"/>
        <location filename="childrenWidget/turninwidget.cpp" line="153"/>
        <source>Please tranfer into </source>
        <translation>กรุณาโอน </translation>
    </message>
    <message>
        <location filename="childrenWidget/turninwidget.cpp" line="87"/>
        <source>Please enter the amount:</source>
        <translation>กรุณาระบุจำนวนเงิน:</translation>
    </message>
    <message>
        <location filename="childrenWidget/turninwidget.cpp" line="96"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="childrenWidget/turninwidget.cpp" line="96"/>
        <source>the amount cannot be 0</source>
        <translation>จำนวนต้องไม่เป็น 0</translation>
    </message>
</context>
<context>
    <name>TurnOutWidget</name>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="14"/>
        <source>TurnOutWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="115"/>
        <location filename="childrenWidget/turnoutwidget.cpp" line="68"/>
        <source>BTC Balance</source>
        <translation>ยอดคงเหลือ BTC</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="144"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="176"/>
        <source>Amount</source>
        <translation>ปริมาณ</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="201"/>
        <source>Enter account number</source>
        <translation>ป้อนหมายเลขบัญชี</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="230"/>
        <source>Notice(optional)</source>
        <translation>ข้อสังเกต (ไม่จำเป็น)</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="255"/>
        <source>Enter Notice</source>
        <translation>ใส่จำนวนเงิน</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="284"/>
        <source>Receive Address</source>
        <translation>ที่อยู่ในการเก็บรวบรวม</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="346"/>
        <source>Enter  the receiver&apos;s account</source>
        <translation>ป้อนบัญชีของผู้รับ</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="394"/>
        <source>Payment Address</source>
        <translation>ที่อยู่การชำระเงิน</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="486"/>
        <source>Cost of Miners</source>
        <translation>ค่าใช้จ่ายขุด</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="502"/>
        <source>0.0004 PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="528"/>
        <source>0.0004</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="566"/>
        <source>0.006</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="623"/>
        <source>OK</source>
        <translation>ตกลง</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.cpp" line="70"/>
        <source>EOS Balance</source>
        <translation>ยอดคงเหลือ EOS</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.cpp" line="104"/>
        <location filename="childrenWidget/turnoutwidget.cpp" line="108"/>
        <location filename="childrenWidget/turnoutwidget.cpp" line="146"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.cpp" line="104"/>
        <source>Incomplete input!</source>
        <translation>อินพุตไม่สมบูรณ์!</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.cpp" line="108"/>
        <source>Please enter valid number!</source>
        <translation>กรุณาใส่หมายเลขที่ถูกต้อง!</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.cpp" line="114"/>
        <source>Please enter login password:</source>
        <translation>กรุณาใส่รหัสผ่านเข้าสู่ระบบ:</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.cpp" line="146"/>
        <source>Incorrect password!</source>
        <translation>รหัสผ่านไม่ถูกต้อง!</translation>
    </message>
</context>
<context>
    <name>WalletInfoWidget</name>
    <message>
        <location filename="childrenWidget/walletinfowidget.ui" line="14"/>
        <source>WalletInfoWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.ui" line="61"/>
        <source>Set wallet&apos;s avatar and name</source>
        <translation>ตั้งสัญลักษณ์และชื่อของกระเป๋าเงิน</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.ui" line="131"/>
        <location filename="childrenWidget/walletinfowidget.ui" line="161"/>
        <source>set avatar</source>
        <translation>ตั้งประจำตัว</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.ui" line="167"/>
        <source>Click here to set avatar</source>
        <translation>คลิกที่นี่เพื่อตั้งสัญลักษณ์</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.ui" line="195"/>
        <source>Wallet name</source>
        <translation>ชื่อกระเป๋าเงิน</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.ui" line="222"/>
        <source>Next</source>
        <translation>ถัดไป</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.cpp" line="55"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.cpp" line="55"/>
        <source>The name cannot be empty</source>
        <translation>ชื่อต้องไม่ว่างเปล่า</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.cpp" line="81"/>
        <source>Open Image</source>
        <translation>เปิดภาพ</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.cpp" line="81"/>
        <source>Image File(*.bmp;*.jpeg;*.jpg;*.png)</source>
        <translation>ไฟล์รูปภาพ (*. bmp; *. jpeg; *. jpg; *. png)</translation>
    </message>
</context>
<context>
    <name>WalletManager</name>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="14"/>
        <source>WalletManager</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="49"/>
        <source>Manage</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="175"/>
        <source>Change avatar</source>
        <translation>เปลี่ยนภาพแทนตัว</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="212"/>
        <source>Change Avatar</source>
        <translation>เปลี่ยนภาพแทนตัว</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="284"/>
        <location filename="childrenWidget/walletmanager.ui" line="300"/>
        <source>Change Wallet&apos;s Name</source>
        <translation>เปลี่ยนชื่อกระเป๋าเงิน</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="339"/>
        <source>Delete Wallet</source>
        <translation>ลบกระเป๋าเงิน</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="355"/>
        <source>Back Up Private Key</source>
        <translation>สำรองข้อมูลรหัสส่วนตัว</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="401"/>
        <source>Backup Private Key</source>
        <translation>สำรองข้อมูลรหัสส่วนตัว</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="442"/>
        <location filename="childrenWidget/walletmanager.ui" line="488"/>
        <source>Switch Account</source>
        <translation>สลับบัญชี</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.cpp" line="54"/>
        <source>Open Image</source>
        <translation>เปิดภาพ</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.cpp" line="54"/>
        <source>Image File(*.bmp;*.jpeg;*.jpg;*.png)</source>
        <translation>ไฟล์รูปภาพ (*. bmp; *. jpeg; *. jpg; *. png)</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.cpp" line="95"/>
        <source>Please enter your login password:</source>
        <translation>กรุณาใส่รหัสผ่านเข้าสู่ระบบของคุณ:</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.cpp" line="265"/>
        <location filename="childrenWidget/walletmanager.cpp" line="293"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.cpp" line="265"/>
        <source>Incorrect password!</source>
        <translation>รหัสผ่านไม่ถูกต้อง!</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.cpp" line="293"/>
        <source>Main wallet cannot be deleted!</source>
        <translation>ไม่สามารถลบกระเป๋าเงินหลักได้!</translation>
    </message>
</context>
<context>
    <name>WalletPriKeyWidget</name>
    <message>
        <location filename="childrenWidget/walletprikeywidget.ui" line="14"/>
        <source>WalletPriKeyWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/walletprikeywidget.ui" line="61"/>
        <source>Backup Private Key</source>
        <translation>สำรองข้อมูลรหัสส่วนตัว</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletprikeywidget.ui" line="97"/>
        <source>Copy</source>
        <translation>สำเนา</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletprikeywidget.ui" line="119"/>
        <source>Safety warning: It&apos;s risky to export with private key which is not encrypted,Back up with the mnemonic word is recommended.</source>
        <translation>คำเตือนด้านความปลอดภัย: มีความเสี่ยงที่จะส่งออกด้วยรหัสส่วนตัวที่ไม่ได้เข้ารหัสขอแนะนำให้สำรองข้อมูลด้วยคำช่วยในการจำ</translation>
    </message>
</context>
<context>
    <name>hostingRecordItemWidget</name>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.ui" line="14"/>
        <source>hostingRecordItemWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.ui" line="89"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.cpp" line="31"/>
        <source>Get Red Packet</source>
        <translation>รับซองจดหมายสีแดง</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.ui" line="111"/>
        <source>2018-09-30 18:09:11</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.ui" line="137"/>
        <source>11.00000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.ui" line="156"/>
        <source>Confirmed</source>
        <translation>ได้รับการยืนยัน</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.cpp" line="35"/>
        <source>Top Up</source>
        <translation>เติมเงิน</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.cpp" line="39"/>
        <source>Withdraw</source>
        <translation>โอนออก</translation>
    </message>
</context>
</TS>
