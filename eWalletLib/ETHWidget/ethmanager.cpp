﻿#include "ethmanager.h"
#ifndef Q_OS_WIN
#include "inline_mac.h"
#endif
#include "QStringLiteralBak.h"
#include "globalmanager.h"

extern OPDataManager  *gOPDataManager;

ETHManager::ETHManager(QObject *parent)
	: QObject(parent)
{
	ethWidget = new ETHWidget;
	turnInWidget = new TurnInWidget;
	turnOutWidget = new ETHTurnOutWidget;
	turnOutManager = new ETHTurnOutManager;
	recordsWidget = new RecordsWidget;

	cefView = NULL;
	m_strBalance = "0";

//#ifdef Q_OS_MAC
	m_pWebChannel = NULL;
	m_pWebObject = NULL;
//#endif // Q_OS_MAC

	connect(ethWidget, SIGNAL(sigRecoveryWallet()), this, SLOT(slotShowInfoWidget()));
	connect(ethWidget, SIGNAL(sigTurnInWidget()), this, SLOT(slotTurnInWidget()));
	connect(ethWidget, SIGNAL(sigProfileWidget()), this, SLOT(slotProfileWidget()));
	connect(ethWidget, SIGNAL(sigProfileWidget(QString, QString, QString)), this, SLOT(slotProfileWidget(QString, QString, QString)));
	connect(ethWidget, SIGNAL(sigTurnOutWidget()), this, SLOT(slotTurnOutWidget()));
	connect(ethWidget, SIGNAL(sigLoadAllToken()), this, SLOT(slotSetAllChain()));
	connect(ethWidget, SIGNAL(sigLoadFollowToken()), this, SLOT(slotSetFollowChain()));
	connect(ethWidget, SIGNAL(sigFollowToken(QString, QString, QString, bool)),
		this, SLOT(slotFollowToken(QString, QString, QString, bool)));
	connect(ethWidget, SIGNAL(sigWalletManager(QString)), this, SLOT(slotWalletManager(QString)));
	connect(ethWidget, SIGNAL(sigClearChecked()), this, SIGNAL(sigClearChecked()));
	connect(turnOutWidget, SIGNAL(sigTurnOut(QString, QString, QString,QString,bool)),
		this, SLOT(slotDoTurnOut(QString, QString, QString,QString,bool)));

	//connect(turnOutManager, SIGNAL(sigFinished()), this, SLOT());
	connect(turnOutManager, SIGNAL(sigTransferMsg(QString, QString, QString)), this, SIGNAL(sigTransferMsg(QString, QString, QString)));

	connect(recordsWidget, SIGNAL(sigTurnIn()), this, SLOT(slotTurnInWidget()));
	connect(recordsWidget, SIGNAL(sigTurnOut()), this, SLOT(slotTurnOutWidget()));
}

ETHManager::~ETHManager()
{
	if (cefView)
		cefView->deleteLater();

//#ifdef Q_OS_MAC
	if (m_pWebChannel)
		delete m_pWebChannel;
	if (m_pWebObject)
		delete m_pWebObject;
//#endif // Q_OS_MAC
}

QString ETHManager::stringToThousandth(QString string, int keep /*= 0*/)
{
	//根据保留的位数，进行补0。
	if (keep > 0)  //需要补0操作。
	{
		int count = 0;
		if (string.contains("."))   //含小数点，只补缺少的0。
			count = keep - string.split(".").last().length();
		else                        //不含小数点，直接补固定数量的0。
		{
			string += ".";
			count = keep;
		}
		//补0操作。
		for (int i = 0; i < count; i++)
			string += "0";
	}
	//进行千分位操作，先获取整数部分。
	QString number = string.split(".").first();   //整数部分。
	QString result;  //保存结果的字符串。
	int num = 0;
	for (int i = number.count(); i > 0; i--)
	{
		result = number.at(i - 1) + result;
		num++;
		if (num == 3 && i != 1)
		{
			result = "," + result;
			num = 0;
		}
	}
	if (keep > 0)
		result += "." + string.split(".").last();
	return result;
}

void ETHManager::setStackedWidget(QStackedWidget *widget)
{
	this->stackedWidget = widget;

	stackedWidget->addWidget(turnInWidget);
	stackedWidget->addWidget(turnOutWidget);
	stackedWidget->addWidget(recordsWidget);
}

void ETHManager::setMainAddress(QString address)
{
	this->mainAddress = address;
}

void ETHManager::setPriKey(QString priKey)
{
	this->walletKey = priKey;
	turnOutManager->setPrivateKey(priKey);
}

ETHWidget * ETHManager::getETHWidget()
{
	return ethWidget;
}

void ETHManager::setWallets(QList<ChildrenWallet> walletList)
{
	this->walletList = walletList;
	if (walletList.count() > 0)
	{
		ChildrenWallet wallet = walletList.first();

		if (wallet.balance.isEmpty())
		{
			OPRequestShareLib *request = new OPRequestShareLib;
			connect(request, SIGNAL(sigChildrenBalance(QString)), this, SLOT(slotGetBalance(QString)));
			connect(request, SIGNAL(sigChildrenBalance(QString)), request, SLOT(deleteLater()));
			request->getChildrenBalacne(ChainID_ETH, wallet.accountAddress);
		}
		else
		{
			wallet.balance = this->stringToThousandth(wallet.balance, 8);
		}
		m_pCurrentWallet = walletList.first();
		turnInWidget->setTurnInData(wallet);
		turnOutWidget->setTurnOutData(wallet);
		UserInfo eInfo = gDataManager->getUserInfo();
		turnOutWidget->setPassWord(eInfo.strLoginPWD);
		ethWidget->setIndexWallet(wallet);
		slotSetFollowChain();

		ethWidget->initShow(true);
	}
	else
	{
		ethWidget->initShow(false);
	}
}

void ETHManager::slotShowInfoWidget()
{
	WalletInfoWidget *infoWidget = new WalletInfoWidget(ethWidget);
	connect(infoWidget, SIGNAL(sigSendWalletInfo(QString, QString)),
		this, SLOT(slotShowRecWidget(QString, QString)));

	infoWidget->show();
}

void ETHManager::slotShowRecWidget(QString name, QString ID)
{
	this->walletName = name;
	this->iconID = ID;

	RecoveryWidget *recoveryWidget = new RecoveryWidget;
	connect(recoveryWidget, SIGNAL(sigRecoveryWord(QString)), this, SLOT(slotRecoveryWallet(QString)));
	connect(recoveryWidget, SIGNAL(sigPrivateKey(QString)), this, SLOT(slotRecoveryWallet(QString)));

	recoveryWidget->show();
}

void ETHManager::slotRecoveryWallet(QString string)
{
	RecoveryWidget *widget = qobject_cast<RecoveryWidget*>(sender());
	if (widget)
		widget->close();

//#ifdef Q_OS_WIN
//	if (cefView)
//		delete cefView;
//	cefView = new QCefView;
//	cefView->hide();
//	connect(cefView, SIGNAL(sigRecoveryResult(QString)), this, SLOT(slotRecoveryResult(QString)));
//	cefView->InitCefUrl("file:///./html/ethWallet.html");
//	QEventLoop loop;
//	connect(cefView, SIGNAL(sigInitFinished()), &loop, SLOT(quit()));
//	loop.exec();
//#else
	if (cefView)
		delete cefView;
	if (m_pWebChannel)
		delete m_pWebChannel;
	if (m_pWebObject)
		delete m_pWebObject;

	m_pWebChannel = new QWebChannel(this);
	m_pWebObject = new WebObjectShareLib(this);
	connect(m_pWebObject, SIGNAL(sigRecoveryResult(QString)), this, SLOT(slotRecoveryResult(QString)));

	/*QString urlName = QDir::currentPath() + ("/html/ethWallet.html");
	QUrl url = QUrl::fromUserInput(urlName);*/
    //QUrl url = "qrc:/html/Resources/html/ethWallet.html";
    QUrl url = QUrl::fromUserInput("qrc:/html/Resources/html/ethWallet.html");
	m_pWebChannel->registerObject("web", m_pWebObject);
	cefView = new QWebEngineView;
	cefView->page()->load(url);
	cefView->page()->setWebChannel(m_pWebChannel);
//#endif

	if (cefView)
	{
//#ifdef Q_OS_WIN
//		if (string.contains(" "))    //用空格隔开的是助记词。
//			cefView->ExecuteJavaScript(QString("recoveryFromWord(\"%1\")").arg(string));
//		else
//		{
//			cefView->ExecuteJavaScript(QString("recoveryFromKey(\"%1\")").arg(string));
//		}
//#else
        if (string.contains(" "))    //用空格隔开的是助记词。
            cefView->page()->runJavaScript(QString("recoveryFromWord(\"%1\")").arg(string));
        else
        {
            cefView->page()->runJavaScript(QString("recoveryFromKey(\"%1\")").arg(string));
        }
//#endif
	}
}

void ETHManager::slotRecoveryResult(QString json)
{
	QJsonDocument doc = QJsonDocument::fromJson(json.toUtf8());
	QVariantMap map = doc.toVariant().toMap();
	QString result = map.value("result").toString();
	if (result == "true")
	{
		
		QString address = map.value("address").toString().toLower();
		QString priKey = map.value("privkey").toString();
		priKey = priKey.startsWith("0x") ? priKey : "0x" + priKey;
		QString pubkey = map.value("pubkey").toString();

		QString accountIcon = gDataManager->getAppConfigInfo().PanServerDownloadURL + iconID + "/download";
		QString strKey = gOPDataManager->encryptAES(priKey.toUtf8(), walletKey);
		
		OPRequestShareLib *request = new OPRequestShareLib;
		connect(request, SIGNAL(sigAddChildrenWallet(bool)), this, SLOT(slotRecoveryFinished(bool)));
		connect(request, SIGNAL(sigAddChildrenWallet(bool)), request, SLOT(deleteLater()));
		request->addChildrenWallet(address, pubkey, strKey, mainAddress, walletName, accountIcon, "ETH", 1);
	}
	else
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("Failed to restore wallet!"));
	}

	/*删除cef*/
	if (cefView)
	{
		cefView->deleteLater();
		cefView = NULL;
	}
	if (m_pWebObject)
	{
		m_pWebObject->deleteLater();
		m_pWebObject = NULL;
	}
	if (m_pWebChannel)
	{
		m_pWebChannel->deleteLater();
		m_pWebChannel = NULL;
	}
}

void ETHManager::slotRecoveryFinished(bool success)
{
	if (success)
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("Wallet restored successfully!"));

		emit sigRefresh();
	}
	else
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("The server returned error!"));
	}
}

void ETHManager::slotOpenMenu()
{
	//主菜单。
	QMenu *menu = new QMenu();

	QAction *refreshAciton = new QAction(tr("Refresh Wallet"), menu);
	refreshAciton->setIcon(QIcon(":/ewallet/Resources/ewallet/openwallet_changewallet.png"));
	connect(refreshAciton, SIGNAL(triggered()), this, SIGNAL(sigRefresh()));
	menu->addAction(refreshAciton);
	refreshAciton->deleteLater();

	QAction *inAction = new QAction(tr("Import Wallet"), menu);
	inAction->setIcon(QIcon(":/ewallet/Resources/ewallet/openwallet_importwallet.png"));
	connect(inAction, SIGNAL(triggered()), this, SLOT(slotShowInfoWidget()));
	menu->addAction(inAction);
	inAction->deleteLater();

	QFile style(":/QSS/Resources/QSS/eWalletLib/ethmanager.qss");
	style.open(QFile::ReadOnly);
	QString sheet = QLatin1String(style.readAll());
	//二级菜单。
	QMenu *childMenu = new QMenu();
	childMenu->setTitle(tr("Switch Wallet ") + QString("(%1)").arg(walletList.count()));
	childMenu->setIcon(QIcon(":/ewallet/Resources/ewallet/openwallet_changewallet.png"));
#ifdef Q_OS_WIN
    childMenu->setStyleSheet(sheet);
#else
    childMenu->setStyleSheet("QMenu{background-color: #72a4d6;color: #042439;}QMenu::item:selected{color: white;}QMenu::item:checked{color: white;}");
#endif
	for (int i = 0; i < walletList.count(); i++)
	{
		ChildrenWallet wallet = walletList.at(i);

		QAction *inAction = new QAction(wallet.accountName, childMenu);
		inAction->setObjectName(wallet.accountAddress);
		QPixmap pix;
#ifdef Q_OS_WIN
        QString imagePath = gSettingsManager->getUserPath()+"/resource/" + wallet.id.remove("-") + ".png";
#else
        QString imagePath = gSettingsManager->getUserPath() + "/resource/" + wallet.id.remove("-") + ".png";
#endif

		if (QFile(imagePath).exists())
		{
			QByteArray bytearray;
			QFile file(imagePath);
			if (file.open(QIODevice::ReadOnly))
			{
				bytearray = file.readAll();
				file.close();
			}
			pix.loadFromData(bytearray);
		}
		if (pix.isNull())
			pix.load(":/ewallet/Resources/ewallet/defaultIcon.png");

		inAction->setIcon(QIcon(pix));
		inAction->setCheckable(true);
		if (inAction->objectName() == ethWidget->getAddress())
		{
			inAction->setChecked(true);
		}
		else
		{
			inAction->setChecked(false);
		}

		connect(inAction, SIGNAL(triggered()), this, SLOT(slotSwitchWallet()));
		childMenu->addAction(inAction);
		inAction->deleteLater();
	}

	menu->addMenu(childMenu);
#ifdef Q_OS_WIN
    menu->setStyleSheet(sheet);
#else
    menu->setStyleSheet("QMenu{background-color: #72a4d6;color: #042439;}QMenu::item:selected{color: white;}");
#endif
	style.close();

	menu->move(QCursor::pos());
	menu->show();
	QPoint menuPos = QCursor::pos();
	menuPos.setY(menuPos.y() - menu->height());
	menu->move(menuPos);
	menu->exec();
}

void ETHManager::slotSwitchWallet()
{
	QAction *action = qobject_cast<QAction *>(sender());
	if (action)
	{
		QString address = action->objectName();
		for (int i = 0; i < walletList.count(); i++)
		{
			ChildrenWallet wallet = walletList.at(i);
			if (wallet.accountAddress == address)
			{
				if (wallet.balance.isEmpty())
				{
					OPRequestShareLib *request = new OPRequestShareLib;
					connect(request, SIGNAL(sigChildrenBalance(QString)), this, SLOT(slotGetBalance(QString)));
					connect(request, SIGNAL(sigChildrenBalance(QString)), request, SLOT(deleteLater()));
					request->getChildrenBalacne(ChainID_ETH, wallet.accountAddress);
				}
				else
				{
					wallet.balance = stringToThousandth(wallet.balance, 8);
				}

				turnInWidget->setTurnInData(wallet);
				turnOutWidget->setTurnOutData(wallet);
				ethWidget->setIndexWallet(wallet);
				slotSetFollowChain();

				break;
			}
		}
	}
}

void ETHManager::slotGetBalance(QString balance)
{
	if (balance != "failed")
	{
		m_strBalance = balance;
		balance = stringToThousandth(balance, 8);
		ethWidget->setBalance(balance);
		turnOutWidget->setBalance(balance);
	}
}

void ETHManager::slotTurnInWidget()
{
	stackedWidget->setCurrentWidget(turnInWidget);
}

void ETHManager::slotProfileWidget()
{
	recordsWidget->setTitle("ETH");
	stackedWidget->setCurrentWidget(recordsWidget);
	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigRecords(QList<RecordInfo>)), recordsWidget, SLOT(slotRecords(QList<RecordInfo>)));
	connect(request, SIGNAL(sigRecords(QList<RecordInfo>)), request, SLOT(deleteLater()));
	request->getRecords(ethWidget->getAddress(), ChainID_ETH, 1, 100, 1);
}

void ETHManager::slotProfileWidget(QString address, QString name, QString tokenID)
{
	recordsWidget->setTitle(name);
	stackedWidget->setCurrentWidget(recordsWidget);
	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigRecords(QList<RecordInfo>)), recordsWidget, SLOT(slotRecords(QList<RecordInfo>)));
	connect(request, SIGNAL(sigRecords(QList<RecordInfo>)), request, SLOT(deleteLater()));
	request->getRecords(ethWidget->getAddress(), ChainID_ETH, 1, 100, 1, tokenID);
}

void ETHManager::slotTurnOutWidget()
{
	stackedWidget->setCurrentWidget(turnOutWidget);
}

void ETHManager::slotDoTurnOut(QString from, QString to, QString value,QString buddyId,bool bIswindow)
{
	int userID = gDataManager->getUserInfo().nUserID;
	turnOutManager->slotTurnOut(QString::number(userID), buddyId,
		from, to, "", value,bIswindow);
}

void ETHManager::slotSetFollowChain()
{
	QString address = ethWidget->getAddress();
	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigFollowToken(QList<TokenInfo>)), ethWidget, SLOT(slotSetFollowChain(QList<TokenInfo>)));
	connect(request, SIGNAL(sigFollowToken(QList<TokenInfo>)), request, SLOT(deleteLater()));
	request->getFollowToken(ChainID_ETH, address);
}

void ETHManager::slotSetAllChain()
{
	QString address = ethWidget->getAddress();
	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigAllToken(QList<TokenInfo>)), ethWidget, SLOT(slotSetAllChain(QList<TokenInfo>)));
	connect(request, SIGNAL(sigAllToken(QList<TokenInfo>)), request, SLOT(deleteLater()));
	request->getAllToken(ChainID_ETH, address);
}

void ETHManager::slotFollowToken(QString chainID, QString tokenID, QString address, bool followed)
{
	OPRequestShareLib *request = new OPRequestShareLib;
	request->setFollowToken(chainID, tokenID, address, followed);
	delete request;
}

void ETHManager::slotWalletManager(QString address)
{
	for (int i = 0; i < walletList.count(); i++)
	{
		ChildrenWallet wallet = walletList.at(i);
		if (wallet.accountAddress == address)
		{
			WalletManager *walletManager = new WalletManager(ethWidget);
			connect(walletManager, SIGNAL(sigSetWalletInfo(QString, QString, QString, QString, QString)),
				this, SLOT(slotSetWalletInfo(QString, QString, QString, QString, QString)));
			connect(walletManager, SIGNAL(sigDeleteWallet(QString, QString, QString)),
				this, SLOT(slotDeleteWallet(QString, QString, QString)));
			walletManager->setWallet(wallet);
			walletManager->show();

			break;
		}
	}
}

void ETHManager::slotSetWalletInfo(QString accountAddress, QString mainAccountAddress, QString accountName, QString accountIcon, QString blockChainName)
{
	for (int i = 0; i < walletList.count(); i++)
	{
		ChildrenWallet wallet = walletList.at(i);
		if (wallet.accountAddress == accountAddress)
		{
			if (!accountName.isEmpty())
				wallet.accountName = accountName;
			if (!accountIcon.isEmpty())
				wallet.accountIcon = accountIcon;

			walletList.removeAt(i);
			walletList.append(wallet);

			if (wallet.balance.isEmpty())
			{
				OPRequestShareLib *request = new OPRequestShareLib;
				connect(request, SIGNAL(sigChildrenBalance(QString)), this, SLOT(slotGetBalance(QString)));
				connect(request, SIGNAL(sigChildrenBalance(QString)), request, SLOT(deleteLater()));
				request->getChildrenBalacne(ChainID_ETH, wallet.accountAddress);
			}
			else
			{
				wallet.balance = stringToThousandth(wallet.balance, 8);
			}
			ethWidget->setIndexWallet(wallet);

			break;
		}
	}
	
	OPRequestShareLib *request = new OPRequestShareLib;
	request->setWalletInfo(accountAddress, mainAccountAddress, accountName, accountIcon, blockChainName);
	delete request;
}

void ETHManager::slotDeleteWallet(QString accountAddress, QString mainAccountAddress, QString blockChainName)
{
	for (int i = 0; i < walletList.count(); i++)
	{
		ChildrenWallet wallet = walletList.at(i);
		if (wallet.accountAddress == mainAddress)
		{
			if (wallet.balance.isEmpty())
			{
				OPRequestShareLib *request = new OPRequestShareLib;
				connect(request, SIGNAL(sigChildrenBalance(QString)), this, SLOT(slotGetBalance(QString)));
				connect(request, SIGNAL(sigChildrenBalance(QString)), request, SLOT(deleteLater()));
				request->getChildrenBalacne(ChainID_ETH, wallet.accountAddress);
			}
			else
			{
				wallet.balance = stringToThousandth(wallet.balance, 8);
			}

			turnInWidget->setTurnInData(wallet);
			turnOutWidget->setTurnOutData(wallet);
			ethWidget->setIndexWallet(wallet);
			slotSetFollowChain();

			break;
		}
	}

	for (int j = 0; j < walletList.count(); j++)
	{
		ChildrenWallet wallet = walletList.at(j);
		if (wallet.accountAddress == accountAddress)
		{
			walletList.removeAt(j);
			break;
		}
	}

	//发送网络请求。
	OPRequestShareLib *request = new OPRequestShareLib;
	request->deleteChildrenWallet(accountAddress, mainAccountAddress, blockChainName);
	delete request;

	IMessageBox::tip(NULL, tr("Notice"), tr("Wallet Deleted Successfully!"));
}

void ETHManager::OtherWindow(QList<ChildrenWallet> BTCList,QString buddyId)
{
	if (walletList.size() == 0)
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("No ETH Wallet!"));
		return;
	}
	if (BTCList.size() > 0)
	{
		ETHTurnOutWidget * pWidget = new ETHTurnOutWidget();

		UserInfo user = gDataManager->getUserInfo();
		pWidget->setBalance(m_strBalance);
		pWidget->setTurnOutData(m_pCurrentWallet);
		pWidget->setPassWord(user.strLoginPWD);
		pWidget->setBuddyId(buddyId);
		connect(pWidget, SIGNAL(sigTurnOut(QString, QString, QString,QString,bool)), this, SLOT(slotDoTurnOut(QString, QString, QString,QString,bool)));
		connect(turnOutManager, SIGNAL(sigFinished()), pWidget, SLOT(slotCloseWindow()));

		pWidget->InitAsWindow();
		pWidget->setBuddyAddress(BTCList);
		pWidget->show();
	}
	else
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("The opposite side have no account!"));
	}
}

void ETHManager::slotClearChecked()
{
	ethWidget->slotClearChecked();
}
