﻿#include "ethtokenlistwidget.h"
#include "ui_ethtokenlistwidget.h"
#ifndef Q_OS_WIN
#include "mycommonstyle.h"
#include "inline_mac.h"
#endif

ETHTokenListWidget::ETHTokenListWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::ETHTokenListWidget();
	ui->setupUi(this);

	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog | Qt::Tool);
	setWindowModality(Qt::WindowModal);
	setAttribute(Qt::WA_DeleteOnClose);
	setAttribute(Qt::WA_TranslucentBackground);

	QFile file(":/QSS/Resources/QSS/eWalletLib/ethtokenlistwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->closeBtn, SIGNAL(clicked()), this, SLOT(close()));
	connect(ui->tokenList, SIGNAL(itemClicked(QListWidgetItem *)), this, SLOT(slotClickToken(QListWidgetItem *)));

#ifdef Q_OS_MAC
    ui->tokenList->setStyle(new MyProxyStyle);
#endif
}

ETHTokenListWidget::~ETHTokenListWidget()
{
	delete ui;
}

//鼠标事件的处理。
void ETHTokenListWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void ETHTokenListWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void ETHTokenListWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

void ETHTokenListWidget::slotSetToken(QList<TokenInfo> tokens)
{
	this->tokens = tokens;

	QTimer::singleShot(100, this, SLOT(slotInsertTokenItem()));
}

void ETHTokenListWidget::slotInsertTokenItem()
{
	ui->tokenList->clear();

	for (int i = 0; i < tokens.count(); i++)
	{
		TokenInfo token = tokens.at(i);

		EWalletItemChecked *checkedWidget = new EWalletItemChecked;
		checkedWidget->setFixedSize(260, 60);

		if (token.tokenIcon.startsWith(":"))
		{
			checkedWidget->setData(token.tokenIcon, token.tokenName, token.tokenFullName,
				token.tokenAddress, true);
			checkedWidget->hideChecked();
		}
		else
		{
			checkedWidget->setTokenID(token.tokenID);

			bool checked = token.ifOwner == 1 ? true : false;
#ifdef Q_OS_WIN
            QString imagePath = QDir::tempPath() + "/" + token.tokenName + ".png";
#else
            QString imagePath = getResourcePath()  + token.tokenName + ".png";
#endif
			QFile imageFile(imagePath);
			if (imageFile.exists())
			{
				checkedWidget->setData(imagePath, token.tokenName, token.tokenFullName,
					token.tokenAddress, checked);
				checkedWidget->hideChecked();
			}
			else
			{
				checkedWidget->setData(token.tokenName, token.tokenFullName,
					token.tokenAddress, checked);
				checkedWidget->hideChecked();

				HttpNetWork::HttpDownLoadFile *http = new HttpNetWork::HttpDownLoadFile;
				connect(http, SIGNAL(sigDownFinished(bool)), this, SLOT(slotDownloadTokenIcon(bool)));
				http->setData(QVariant(token.tokenName));
				http->StartDownLoadFile(token.tokenIcon, imagePath);
			}
		}

		QListWidgetItem *checkedItem = new QListWidgetItem;
		checkedItem->setSizeHint(QSize(260, 60));
		ui->tokenList->addItem(checkedItem);
		ui->tokenList->setItemWidget(checkedItem, checkedWidget);
	}
}

void ETHTokenListWidget::slotDownloadTokenIcon(bool success)
{
	if (success)
	{
		HttpNetWork::HttpDownLoadFile *http = qobject_cast<HttpNetWork::HttpDownLoadFile *>(sender());
		QString name = http->getData().toString();

		for (int i = 0; i < ui->tokenList->count(); i++)
		{
			QListWidgetItem *item = ui->tokenList->item(i);

			if (item)
			{
				EWalletItemChecked *ckeckWidget = (EWalletItemChecked *)ui->tokenList->itemWidget(item);
				if (ckeckWidget)
				{
					if (ckeckWidget->getName() == name)
					{

#ifdef Q_OS_WIN
            QString imagePath = QDir::tempPath() + "/" + name + ".png";
#else
            QString imagePath = getResourcePath() + name + ".png";
#endif
						ckeckWidget->setIcon(imagePath);
					}
				}
			}
		}
	}
}

void ETHTokenListWidget::slotClickToken(QListWidgetItem *item)
{
	if (item)
	{
		EWalletItemChecked *checkedWidget = (EWalletItemChecked *)ui->tokenList->itemWidget(item);
		if (checkedWidget)
		{
			QString name = checkedWidget->getName();
			QString address = checkedWidget->getAddress();
			QString tokenID = checkedWidget->getTokenID();

			if (name == "ETH")
				address.clear();

			emit sigSelectToken(name, address, tokenID);
			this->close();
		}
	}
}

