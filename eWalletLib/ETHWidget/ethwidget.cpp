﻿#include "ethwidget.h"
#include "ui_ethwidget.h"
#include "globalmanager.h"
#ifdef Q_OS_MAC
#include "mycommonstyle.h"
#include "inline_mac.h"
#endif
ETHWidget::ETHWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::ETHWidget();
	ui->setupUi(this);
#ifdef Q_OS_MAC
    ui->followList->setStyle(new MyProxyStyle);
    ui->allList->setStyle(new MyProxyStyle);
#endif

	QFile file(":/QSS/Resources/QSS/eWalletLib/ethwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	ui->iconLabel->setShape(roundRect);

	connect(ui->recoveryBtn, SIGNAL(clicked()), this, SIGNAL(sigRecoveryWallet()));
	connect(ui->turnInBtn, SIGNAL(clicked()), this, SIGNAL(sigTurnInWidget()));
	connect(ui->profileBtn, SIGNAL(clicked()), this, SIGNAL(sigProfileWidget()));
	connect(ui->turnOutBtn, SIGNAL(clicked()), this, SIGNAL(sigTurnOutWidget()));
	connect(ui->addBtn, SIGNAL(clicked()), this, SLOT(slotClickedAddBtn()));
	connect(ui->copyBtn, SIGNAL(clicked()), this, SLOT(slotCopyAddress()));

	connect(ui->followList, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(slotDBClickFollowItem(QListWidgetItem *)));

	//按钮点击时发送消息 通知其他widget取消选中
	connect(ui->turnInBtn, SIGNAL(clicked()), this, SIGNAL(sigClearChecked()));
	connect(ui->turnOutBtn, SIGNAL(clicked()), this, SIGNAL(sigClearChecked()));
	connect(ui->profileBtn, SIGNAL(clicked()), this, SIGNAL(sigClearChecked()));

	ui->iconLabel->installEventFilter(this);

	ui->allList->hide();
	ui->tipLabel->hide();

	ui->profileBtn->setAutoExclusive(true);
	ui->turnInBtn->setAutoExclusive(true);
	ui->turnOutBtn->setAutoExclusive(true);
}

ETHWidget::~ETHWidget()
{
	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

bool ETHWidget::eventFilter(QObject *obj, QEvent *event)
{
	if (obj == ui->iconLabel)
	{
		if (event->type() == QEvent::MouseButtonPress)
		{
			emit sigWalletManager(getAddress());
		}
	}

	return QWidget::eventFilter(obj, event);
}

void ETHWidget::initShow(bool hasWallet)
{
	if (hasWallet)
	{
		ui->firstWidget->hide();
		ui->mainWidget->show();
	}
	else
	{
		ui->mainWidget->hide();
		ui->firstWidget->show();
	}
}


void ETHWidget::setIndexWallet(ChildrenWallet wallet)
{
	ui->nameLabel->setText(wallet.accountName);
	QString address = wallet.accountAddress.left(6) + "******" + wallet.accountAddress.right(4);
	ui->addressLabel->setText(address);
	ui->addressLabel->setObjectName(wallet.accountAddress);
	ui->numberLabel->setText(wallet.balance);
	QString imagePath = gSettingsManager->getUserPath()+"/resource/ewallet/header/" + wallet.id.remove("-") + ".png";
	if (QFile(imagePath).exists())
	{
		QByteArray bytearray;
		QFile file(imagePath);
		if (file.open(QIODevice::ReadOnly))
		{
			bytearray = file.readAll();
			file.close();
		}
		QPixmap pix;
		pix.loadFromData(bytearray);

		if (pix.isNull())
			pix.load(":/ewallet/Resources/ewallet/defaultIcon.png");

		ui->iconLabel->setPixmap(pix);
	}
	else
	{
		if (wallet.accountIcon.isEmpty())
			ui->iconLabel->setPixmap(QPixmap(":/ewallet/Resources/ewallet/defaultIcon.png"));
		else
		{
			HttpNetWork::HttpDownLoadFile *http = new HttpNetWork::HttpDownLoadFile;
			connect(http, SIGNAL(sigDownFinished(bool)), this, SLOT(slotDownloadIcon(bool)));
			http->setData(QVariant(imagePath));
			http->StartDownLoadFile(wallet.accountIcon, imagePath);
		}
	}
}

void ETHWidget::slotDownloadIcon(bool success)
{
	if (success)
	{
		HttpNetWork::HttpDownLoadFile *download = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());
		if (download)
		{
			QString imagePath = download->getData().toString();
			QByteArray bytearray;
			QFile file(imagePath);
			if (file.open(QIODevice::ReadOnly))
			{
				bytearray = file.readAll();
				file.close();
			}

			QPixmap pix;
			pix.loadFromData(bytearray);

			if (pix.isNull())
				pix.load(":/ewallet/Resources/ewallet/defaultIcon.png");

			ui->iconLabel->setPixmap(pix);
		}
	}
	else
	{
		ui->iconLabel->setPixmap(QPixmap(":/ewallet/Resources/ewallet/defaultIcon.png"));
	}
}

void ETHWidget::slotCopyAddress()
{
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(ui->addressLabel->objectName());

	if (ui->tipLabel->isHidden())
	{
		ui->tipLabel->show();
		QTimer::singleShot(2000, ui->tipLabel, SLOT(hide()));
	}
}

void ETHWidget::setBalance(QString balance)
{
	ui->numberLabel->setText(balance);
}

QString ETHWidget::getAddress()
{
	return ui->addressLabel->objectName();
}

void ETHWidget::slotSetFollowChain(QList<TokenInfo> tokenList)
{
	ui->followList->clear();

	for (int i = 0; i < tokenList.count(); i++)
	{
		TokenInfo token = tokenList.at(i);

		EWalletItemFollow *followWidget = new EWalletItemFollow;
		if (token.tokenIcon.startsWith(":"))
		{
			token.tokenAddress = getAddress();
			followWidget->setData(token.tokenIcon, token.tokenName, token.balance, token.tokenAddress, token.tokenID);
		}
		else
		{
			QString strPath = gSettingsManager->getUserPath();
			QString imagePath = strPath + "/resource/ewallet/header/" + token.tokenName + ".png";
			QFile imageFile(imagePath);
			if (imageFile.exists())
			{
				followWidget->setData(imagePath, token.tokenName, token.balance, token.tokenAddress, token.tokenID);
			}
			else
			{
				followWidget->setData(token.tokenName, token.balance, token.tokenAddress, token.tokenID);

				HttpNetWork::HttpDownLoadFile *http = new HttpNetWork::HttpDownLoadFile;
				connect(http, SIGNAL(sigDownFinished(bool)), this, SLOT(slotDownloadTokenIcon(bool)));
				http->setData(QVariant(token.tokenName));
				http->StartDownLoadFile(token.tokenIcon, imagePath);
			}
		}

		QListWidgetItem *followItem = new QListWidgetItem;
		followItem->setSizeHint(QSize(240, 50));
		ui->followList->addItem(followItem);
		ui->followList->setItemWidget(followItem, followWidget);
	}

	ui->addBtn->setStyleSheet("QPushButton#addBtn{border-image: url(:/ewallet/Resources/ewallet/add.png);}");

	ui->allList->hide();
	ui->followList->show();
}

void ETHWidget::slotSetAllChain(QList<TokenInfo> tokenList)
{
	ui->allList->clear();

	for (int i = 0; i < tokenList.count(); i++)
	{
		TokenInfo token = tokenList.at(i);

		EWalletItemChecked *checkedWidget = new EWalletItemChecked;
		connect(checkedWidget, SIGNAL(sigChecked(QString, bool)), this, SLOT(slotClickFollowBtn(QString, bool)));

		if (token.tokenIcon.startsWith(":"))
		{
			checkedWidget->setData(token.tokenIcon, token.tokenName, token.tokenFullName,
				token.tokenAddress, true);
			checkedWidget->hideChecked();
		}
		else
		{
			checkedWidget->setTokenID(token.tokenID);

			bool checked = token.ifOwner == 1 ? true : false;
			QString strPath = gSettingsManager->getUserPath();
			QString imagePath = strPath + "/resource/ewallet/header/" + token.tokenName + ".png";
			QFile imageFile(imagePath);
			if (imageFile.exists())
			{
				checkedWidget->setData(imagePath, token.tokenName, token.tokenFullName,
					token.tokenAddress, checked);
			}
			else
			{
				checkedWidget->setData(token.tokenName, token.tokenFullName,
					token.tokenAddress, checked);

				HttpNetWork::HttpDownLoadFile *http = new HttpNetWork::HttpDownLoadFile;
				connect(http, SIGNAL(sigDownFinished(bool)), this, SLOT(slotDownloadTokenIcon(bool)));
				http->setData(QVariant(token.tokenName));
				http->StartDownLoadFile(token.tokenIcon, imagePath);
			}
		}

		QListWidgetItem *checkedItem = new QListWidgetItem;
		checkedItem->setSizeHint(QSize(240, 60));
		ui->allList->addItem(checkedItem);
		ui->allList->setItemWidget(checkedItem, checkedWidget);
	}

	ui->addBtn->setStyleSheet("QPushButton#addBtn{border-image: url(:/ewallet/Resources/ewallet/back.png);}");

	ui->followList->hide();
	ui->allList->show();
}

void ETHWidget::slotDownloadTokenIcon(bool success)
{
	if (success)
	{
		HttpNetWork::HttpDownLoadFile *http = qobject_cast<HttpNetWork::HttpDownLoadFile *>(sender());
		QString name = http->getData().toString();

		if (ui->followList->isVisible())
		{
			for (int i = 0; i < ui->followList->count(); i++)
			{
				QListWidgetItem *item = ui->followList->item(i);

				if (item)
				{
					EWalletItemFollow *followWidget = (EWalletItemFollow *)ui->followList->itemWidget(item);
					if (followWidget)
					{
						if (followWidget->getName() == name)
						{
							QString strPath = gSettingsManager->getUserPath();
							QString imagePath = strPath + "/resource/ewallet/header/" + name + ".png";
							followWidget->setIcon(imagePath);
						}
					}
				}
			}
		}
		if (ui->allList->isVisible())
		{
			for (int i = 0; i < ui->allList->count(); i++)
			{
				QListWidgetItem *item = ui->allList->item(i);

				if (item)
				{
					EWalletItemChecked *checkedWidget = (EWalletItemChecked *)ui->allList->itemWidget(item);
					if (checkedWidget)
					{
						if (checkedWidget->getName() == name)
						{
							QString strPath = gSettingsManager->getUserPath();
							QString imagePath = strPath + "/resource/ewallet/header/" + name + ".png";
							checkedWidget->setIcon(imagePath);
						}
					}
				}
			}
		}
	}
}

void ETHWidget::slotClickedAddBtn()
{
	if (ui->followList->isVisible())
	{
		//ui->followList->hide();
		//ui->allList->show();

		emit sigLoadAllToken();
	}
	else
	{
		//ui->allList->hide();
		//ui->followList->show();
		
		emit sigLoadFollowToken();
	}
}

void ETHWidget::slotClickFollowBtn(QString tokenID, bool checked)
{
	QString address = ui->addressLabel->objectName();

	emit sigFollowToken(ChainID_ETH, tokenID, address, checked);
}



void ETHWidget::slotDBClickFollowItem(QListWidgetItem *item)
{
	if (item)
	{
		EWalletItemFollow *followWidget = (EWalletItemFollow *)ui->followList->itemWidget(item);
		if (followWidget)
		{
			QString address = followWidget->getAddress();
			QString tokenID = followWidget->getTokenID();
			QString name = followWidget->getName();
			emit sigProfileWidget(address, name, tokenID);
		}
	}
}

void ETHWidget::slotClearChecked()
{
	ui->profileBtn->setAutoExclusive(false);
	ui->turnInBtn->setAutoExclusive(false);
	ui->turnOutBtn->setAutoExclusive(false);

	ui->profileBtn->setChecked(false);
	ui->turnInBtn->setChecked(false);
	ui->turnOutBtn->setChecked(false);

	ui->profileBtn->setAutoExclusive(true);
	ui->turnInBtn->setAutoExclusive(true);
	ui->turnOutBtn->setAutoExclusive(true);
}
