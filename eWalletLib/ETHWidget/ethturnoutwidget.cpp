﻿#include "ethturnoutwidget.h"
#include "ui_ethturnoutwidget.h"
#include "QStringLiteralBak.h"
#include "inputbox.h"
#include <QCryptographicHash>
#include "httpnetworksharelib.h"
#include "combobox/mycombobox.h"
#include "combobox/myBoxItem.h"
#include "globalmanager.h"

ETHTurnOutWidget::ETHTurnOutWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::ETHTurnOutWidget();
	ui->setupUi(this);

	ui->label_7->hide();
	ui->label_8->hide();
	ui->minerSlider->hide();

	m_bMove = false;
	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog | Qt::Tool);
	setWindowModality(Qt::WindowModal);
	setAttribute(Qt::WA_TranslucentBackground);
	ui->mBtnClose->hide();
	ui->minerWidget->hide();
	ui->recvBox->hide();

	QFile file(":/QSS/Resources/QSS/eWalletLib/ethturnoutwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->transBtn, SIGNAL(clicked()), this, SLOT(slotClickTransBtn()));
	connect(ui->recvBox, SIGNAL(activated(int)), this, SLOT(slotonIndexChanged(int)));
	connect(ui->recvBox, SIGNAL(currentIndexChanged(int)), this, SLOT(slotonIndexChanged(int)));
#ifndef Q_OS_WIN
    QFile style(":/QSS/Resources/QSS/qslider_horizontal.qss");
    style.open(QFile::ReadOnly);
    QString sheet = QLatin1String(style.readAll());
    ui->minerSlider->setStyleSheet(sheet);
    style.close();
#else
    ui->minerSlider->setStyleSheet("background-color: rgba(0,0,0, 0);");
#endif
}

ETHTurnOutWidget::~ETHTurnOutWidget()
{
	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

void ETHTurnOutWidget::show()
{
	QWidget::show();
	this->raise();
	this->activateWindow();
}

void ETHTurnOutWidget::setTurnOutData(ChildrenWallet wallet)
{
	this->wallet = wallet;

	ui->amountLabel->setText(wallet.balance);
	ui->walletLabel->setText(wallet.accountName);
	ui->addressLabel->setText(wallet.accountAddress);
}

void ETHTurnOutWidget::setBalance(QString balance)
{
	ui->amountLabel->setText(balance);
}


void ETHTurnOutWidget::slotClickTransBtn()
{
	QString value = ui->valueEdit->text();
	QString toAddress;
	if (m_bMove)
	{
		toAddress = ui->recvBox->currentText();
	}
	else
	{
		toAddress = ui->recvEdit->text();
	}
	toAddress = toAddress.trimmed();

	double dValue = value.toDouble();
	if (value.isEmpty() || toAddress.isEmpty())
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("The amount and the address cannot be empty!"));
	}
	else if(dValue <=0)
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("Please enter valid number!"));
	}
	else
	{
		InputBox *box = new InputBox(this);
		connect(box, SIGNAL(sigEnter(QString)), this, SLOT(slotEnterWord(QString)));
		box->init(tr("Please enter login password:"), false, QLineEdit::Password);
	}
// 	else
// 	{
// 		emit sigTurnOut(wallet.accountAddress, toAddress, value);
// 	}
}
void ETHTurnOutWidget::setPassWord(QString word)
{
	this->passWord = word;
}

void ETHTurnOutWidget::slotEnterWord(QString word)
{
	QByteArray array = QCryptographicHash::hash(word.toUtf8(), QCryptographicHash::Sha1);
	QString string = array.toHex();



	if (string.toLower() == passWord.toLower())
	{
		QString value = ui->valueEdit->text();
		QString toAddress;
		if (m_bMove)
		{
			toAddress = ui->recvBox->currentText();
		}
		else
		{
			toAddress = ui->recvEdit->text();
		};
		toAddress = toAddress.trimmed();
		emit sigTurnOut(wallet.accountAddress, toAddress, value,buddyId,m_bMove);

		ui->valueEdit->clear();
		ui->recvEdit->clear();
	}
	else
	{
		IMessageBox::tip(this, tr("Notice"), tr("Incorrect password!"));
	}
}

//鼠标事件的处理。
void ETHTurnOutWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void ETHTurnOutWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void ETHTurnOutWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0 || m_bMove == false)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

void ETHTurnOutWidget::InitAsWindow()
{
	QFile file(":/QSS/Resources/QSS/eWalletLib/ethturnoutwidget_dialog.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	ui->midWidget->setStyleSheet(styleSheet);
	file.close();

	ui->mBtnClose->show();
	ui->recvBox->show();
	ui->recvEdit->hide();
	m_bMove = true;
	connect(ui->mBtnClose, SIGNAL(clicked()), this, SLOT(close()));
}

void ETHTurnOutWidget::setBuddyAddress(QList<ChildrenWallet> BTCList)
{
	QString strStyle = "";
	for (int i = 0; i < BTCList.size(); i++)
	{
		QString imagePath = gSettingsManager->getUserPath() + "/resource/" + BTCList[i].id.remove("-") + ".png";
		QString strValue = BTCList[i].accountName;
		QString strTitle;
		QString strName;
		if (!strValue.isEmpty())
		{
			strValue += ": ";
		}
		strValue += BTCList[i].accountAddress;
		strTitle = BTCList[i].accountName;
		strName = BTCList[i].accountAddress;
		QPixmap pix;
		if (QFile(imagePath).exists())
		{
			QByteArray bytearray;
			QFile file(imagePath);
			if (file.open(QIODevice::ReadOnly))
			{
				bytearray = file.readAll();
				file.close();
			}

			pix.loadFromData(bytearray);

			if (pix.isNull())
				pix.load(":/ewallet/Resources/ewallet/defaultIcon.png");

			//ui->iconLabel->setPixmap(pix);
		}
		else
		{
			if (wallet.accountIcon.isEmpty())
				pix.load(":/ewallet/Resources/ewallet/defaultIcon.png");
			//ui->iconLabel->setPixmap(QPixmap(":/ewallet/Resources/ewallet/defaultIcon.png"));
			else
			{
				HttpNetWork::HttpDownLoadFile *http = new HttpNetWork::HttpDownLoadFile;
				//connect(http, SIGNAL(sigDownFinished(bool)), this, SLOT(slotDownloadIcon(bool)));
				http->setData(QVariant(imagePath));
				http->StartDownLoadFile(wallet.accountIcon, imagePath);
			}
		}
		// 		WalletBoxItem * pItem = new WalletBoxItem();
		// 		pItem->setItemStyle(strStyle);
		// 		pItem->setIconPath(pix);
		// 		pItem->setTitleText(strValue);
		//pix = pix.scaled(40, 40, Qt::KeepAspectRatio, Qt::SmoothTransformation);
		MyBoxItem * boxiem = new MyBoxItem(pix, strTitle, strName);
		ui->recvBox->AddAccount(boxiem);
		if (i == 0)
		{
			ui->recvBox->OnShowAccount(strName);
		}

	}
}

void ETHTurnOutWidget::slotonIndexChanged(int)
{
	QString strText = ui->recvBox->currentText();
	int i = strText.indexOf(":");
	if (i > -1)
	{
		strText = strText.right(strText.length() - i - 2);
		ui->recvBox->setEditText(strText);
	}
}

void ETHTurnOutWidget::setBuddyId(QString strId)
{
	buddyId = strId;
}

void ETHTurnOutWidget::slotCloseWindow()
{
	if (m_bMove)
	{
		close();
	}
}
