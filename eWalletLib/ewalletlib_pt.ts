<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>BTCManager</name>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="159"/>
        <location filename="BTCWidget/btcmanager.cpp" line="188"/>
        <location filename="BTCWidget/btcmanager.cpp" line="213"/>
        <location filename="BTCWidget/btcmanager.cpp" line="219"/>
        <location filename="BTCWidget/btcmanager.cpp" line="399"/>
        <location filename="BTCWidget/btcmanager.cpp" line="473"/>
        <location filename="BTCWidget/btcmanager.cpp" line="492"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="159"/>
        <source>BTC cannot be restored by private key for now, please use the mnemonic word</source>
        <translation>BTC não pode ser restaurado por chave privada por enquanto, por favor use a palavra mnemônica</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="188"/>
        <source>Failed to restore the wallet!</source>
        <translation>Falha ao restaurar a carteira!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="213"/>
        <source>The wallet restored successfully!</source>
        <translation>A carteira restaurada com sucesso!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="219"/>
        <source>The server returned error!</source>
        <translation>O servidor retornou um erro!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="228"/>
        <source>Refresh Wallet</source>
        <translation>Atualizar Carteira</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="234"/>
        <source>Import Wallet</source>
        <translation>Carteira de Importação</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="246"/>
        <source>Switch Wallet </source>
        <translation>Trocar Carteira </translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="399"/>
        <source>The wallet deleted successfully!</source>
        <translation>A carteira foi excluída com sucesso!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="473"/>
        <source>There is no BTC wallet!</source>
        <translation>Não há carteira BTC!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="492"/>
        <source>The opposite side have no account!</source>
        <translation>O lado oposto não tem conta!</translation>
    </message>
</context>
<context>
    <name>BTCTurnOutManager</name>
    <message>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="38"/>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="57"/>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="140"/>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="162"/>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="166"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="38"/>
        <source>Cannot find the private key of the wallet!</source>
        <translation>Não é possível encontrar a chave privada da carteira!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="57"/>
        <source>Failed to get Utxo!</source>
        <translation>Falha ao obter o Utxo!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="140"/>
        <source>Signature generation failed!</source>
        <translation>Geração de assinatura falhou!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="162"/>
        <source>Deal is Done!</source>
        <translation>Negócio é feito!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="166"/>
        <source>Deal Failed!</source>
        <translation>Falha no negócio!</translation>
    </message>
</context>
<context>
    <name>BTCWidget</name>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="14"/>
        <source>BTCWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="131"/>
        <source>BTC</source>
        <translation></translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="173"/>
        <source>Restore Wallet</source>
        <translation>Restaurar carteira</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="275"/>
        <source>manage wallet</source>
        <translation>gerenciar carteira</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="350"/>
        <source>Copy</source>
        <translation>cópia de</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="383"/>
        <source>Copy successed</source>
        <translation>Suceder</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="467"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="532"/>
        <source>Transfer In</source>
        <translation>Transferência em</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="578"/>
        <source>Details</source>
        <translation>Detalhes</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="624"/>
        <source>Transfer Out</source>
        <translation>Transferir</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="654"/>
        <source>assets</source>
        <translation>Ativos</translation>
    </message>
</context>
<context>
    <name>CardItemWidget</name>
    <message>
        <location filename="pwrWidget/integralCardWidget/carditemwidget.ui" line="26"/>
        <source>CardItemWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/carditemwidget.ui" line="96"/>
        <source>0xd0ee****d73e</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/carditemwidget.ui" line="120"/>
        <source>Balance:0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/carditemwidget.ui" line="148"/>
        <source> Top Up &gt;</source>
        <translation> Top Up&gt;</translation>
    </message>
</context>
<context>
    <name>ChangeIntoWidget</name>
    <message>
        <location filename="pwrWidget/changeintoWidget/changeintowidget.ui" line="14"/>
        <source>ChangeIntoWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/changeintoWidget/changeintowidget.ui" line="77"/>
        <source>Scan the QR code to exchange power</source>
        <translation>Digitalize o código 
QR para trocar poder</translation>
    </message>
    <message>
        <location filename="pwrWidget/changeintoWidget/changeintowidget.ui" line="214"/>
        <location filename="pwrWidget/changeintoWidget/changeintowidget.ui" line="220"/>
        <source>Set Aomunt</source>
        <translation>Montante</translation>
    </message>
    <message>
        <location filename="pwrWidget/changeintoWidget/changeintowidget.ui" line="251"/>
        <location filename="pwrWidget/changeintoWidget/changeintowidget.ui" line="306"/>
        <source>Exchange Records</source>
        <translation>Registros de Troca</translation>
    </message>
</context>
<context>
    <name>ChangeNumWidget</name>
    <message>
        <location filename="pwrWidget/changeintoWidget/changenumwidget.ui" line="14"/>
        <source>ChangeNumWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/changeintoWidget/changenumwidget.ui" line="71"/>
        <source>Amount</source>
        <translation>Montante</translation>
    </message>
    <message>
        <location filename="pwrWidget/changeintoWidget/changenumwidget.ui" line="167"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>ChargeWidget</name>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="14"/>
        <source>Charge</source>
        <oldsource>ChargeWidget</oldsource>
        <translation>Recarregar</translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="61"/>
        <source>Top Up</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="130"/>
        <source>Top-Up Way</source>
        <translation>Maneira Top-Up</translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="158"/>
        <source>Alipay</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="167"/>
        <source>WeChat</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="194"/>
        <source>Daily max amount￥50000.00</source>
        <translation>Valor máximo diário￥50000,00</translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="229"/>
        <source>Amount</source>
        <translation>Montante</translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="278"/>
        <source>￥</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="349"/>
        <source>Next</source>
        <translation>Próximo</translation>
    </message>
</context>
<context>
    <name>DetailsWidget</name>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="14"/>
        <source>DetailsWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="100"/>
        <source>Exchange Detail</source>
        <translation>Detalhe da troca</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="185"/>
        <source>Exchange successed!</source>
        <translation>Troca bem sucedida!</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="244"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="260"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="276"/>
        <source>Power</source>
        <translation>Poder</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="348"/>
        <source>Exchange ID</source>
        <translation>ID da troca</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="436"/>
        <source>Exchange Time</source>
        <translation>Hora da troca</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="521"/>
        <source>Account Send</source>
        <translation>Envio de conta</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="606"/>
        <source>Account Receive</source>
        <translation>Conta Recebida</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="680"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>DeviceItemWidget</name>
    <message>
        <location filename="pwrWidget/myDeviceWidget/deviceitemwidget.ui" line="14"/>
        <source>DeviceItemWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/myDeviceWidget/deviceitemwidget.ui" line="89"/>
        <source>ONEPLUS A600</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/myDeviceWidget/deviceitemwidget.ui" line="111"/>
        <source>2019-09-30 11:31:19</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/myDeviceWidget/deviceitemwidget.ui" line="144"/>
        <source>This Device</source>
        <translation>Este aparelho</translation>
    </message>
    <message>
        <location filename="pwrWidget/myDeviceWidget/deviceitemwidget.cpp" line="46"/>
        <source>Offline</source>
        <translation>Desligado</translation>
    </message>
</context>
<context>
    <name>EOSManager</name>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="135"/>
        <location filename="EOSWidget/eosmanager.cpp" line="168"/>
        <location filename="EOSWidget/eosmanager.cpp" line="172"/>
        <location filename="EOSWidget/eosmanager.cpp" line="240"/>
        <location filename="EOSWidget/eosmanager.cpp" line="417"/>
        <location filename="EOSWidget/eosmanager.cpp" line="436"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="135"/>
        <source>Account Verification Failed!</source>
        <translation>Falha na confirmação da conta!</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="168"/>
        <source>EOS Wallet Restored Successfully!</source>
        <translation>Carteira EOS restaurada com sucesso!</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="172"/>
        <source>Failed to Restore EOS Wallet!</source>
        <translation>Falha ao restaurar o EOS Wallet!</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="240"/>
        <source>Wallet Deleted Successfully!</source>
        <translation>A carteira foi excluída com sucesso!</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="266"/>
        <source>Refresh Wallet</source>
        <translation>Atualizar Carteira</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="272"/>
        <source>Import Wallet</source>
        <translation>Carteira de Importação</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="283"/>
        <source>Switch Wallet </source>
        <translation>Trocar Carteira </translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="417"/>
        <source>No EOS Wallet!</source>
        <translation>Não EOS Wallet!</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="436"/>
        <source>The opposite side have no account!</source>
        <translation>O lado oposto não tem conta!</translation>
    </message>
</context>
<context>
    <name>EOSRecoveryWidget</name>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.ui" line="14"/>
        <source>EOS Recovery</source>
        <oldsource>EOSRecoveryWidget</oldsource>
        <translation>Recuperação de carteira EOS</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.ui" line="79"/>
        <source>Restore Wallet Account</source>
        <translation>Restaurar conta da carteira</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.ui" line="131"/>
        <source>Restore Your Wallet Accout By Private Key</source>
        <translation>Restaurar sua conta da 
Google Wallet por chave privada</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.ui" line="165"/>
        <source>Please Enter OWN Private Key</source>
        <translation>Por favor, digite sua própria chave privada</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.ui" line="190"/>
        <source>Please Enter ACTIVE Private Key</source>
        <translation>Por favor, digite a chave privada ativa</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.ui" line="215"/>
        <source>Finished</source>
        <translation>Acabado</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.cpp" line="62"/>
        <location filename="EOSWidget/eosrecoverywidget.cpp" line="72"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.cpp" line="62"/>
        <source>Private Key not Entered Completely!</source>
        <translation>Chave privada não inserida completamente!</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.cpp" line="72"/>
        <source>Incorrect Private Key，Failed to Get Public Key!</source>
        <translation>Chave Privada Incorreta, Falha ao Obter Chave Pública!</translation>
    </message>
</context>
<context>
    <name>EOSTurnOutManager</name>
    <message>
        <location filename="EOSWidget/eosturnoutmanager.cpp" line="24"/>
        <location filename="EOSWidget/eosturnoutmanager.cpp" line="87"/>
        <location filename="EOSWidget/eosturnoutmanager.cpp" line="90"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosturnoutmanager.cpp" line="24"/>
        <source>Failed to get the private key of EOS wallet!</source>
        <translation>Falha ao obter a chave privada da carteira EOS!</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosturnoutmanager.cpp" line="87"/>
        <source>Deal is Done!</source>
        <translation>Negócio é feito!</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosturnoutmanager.cpp" line="90"/>
        <source>Deal Failed!</source>
        <translation>Falha no negócio!</translation>
    </message>
</context>
<context>
    <name>EOSWidget</name>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="14"/>
        <source>EOSWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="131"/>
        <source>EOS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="173"/>
        <source>Restore Wallet</source>
        <translation>Restaurar carteira</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="275"/>
        <source>manage wallet</source>
        <translation>gerenciar carteira</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="350"/>
        <source>Copy</source>
        <translation>cópia de</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="383"/>
        <source>Copy successed</source>
        <translation>Suceder</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="467"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="532"/>
        <source>Transfer In</source>
        <translation>Transferência em</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="581"/>
        <source>Details</source>
        <translation>Detalhes</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="627"/>
        <source>Transfer Out</source>
        <translation>Transferir</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="657"/>
        <source>assets</source>
        <translation>Transferir</translation>
    </message>
</context>
<context>
    <name>ETHManager</name>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="258"/>
        <location filename="ETHWidget/ethmanager.cpp" line="283"/>
        <location filename="ETHWidget/ethmanager.cpp" line="289"/>
        <location filename="ETHWidget/ethmanager.cpp" line="588"/>
        <location filename="ETHWidget/ethmanager.cpp" line="595"/>
        <location filename="ETHWidget/ethmanager.cpp" line="616"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="258"/>
        <source>Failed to restore wallet!</source>
        <translation>Falha ao restaurar a carteira!</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="283"/>
        <source>Wallet restored successfully!</source>
        <translation>A carteira restaurada com sucesso!</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="289"/>
        <source>The server returned error!</source>
        <translation>O servidor retornou um erro!</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="298"/>
        <source>Refresh Wallet</source>
        <translation>Atualizar Carteira</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="304"/>
        <source>Import Wallet</source>
        <translation>Carteira de Importação</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="315"/>
        <source>Switch Wallet </source>
        <translation>Trocar Carteira </translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="588"/>
        <source>Wallet Deleted Successfully!</source>
        <translation>A carteira foi excluída com sucesso!</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="595"/>
        <source>No ETH Wallet!</source>
        <translation>Não ETH Wallet!</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="616"/>
        <source>The opposite side have no account!</source>
        <translation>O lado oposto não tem conta!</translation>
    </message>
</context>
<context>
    <name>ETHTokenListWidget</name>
    <message>
        <location filename="ETHWidget/ethtokenlistwidget.ui" line="14"/>
        <source>ETHTokenListWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ETHWidget/ethtokenlistwidget.ui" line="77"/>
        <source>Replace Assets</source>
        <translation>Substituir ativos</translation>
    </message>
</context>
<context>
    <name>ETHTurnOutManager</name>
    <message>
        <location filename="ETHWidget/ethturnoutmanager.cpp" line="50"/>
        <location filename="ETHWidget/ethturnoutmanager.cpp" line="131"/>
        <location filename="ETHWidget/ethturnoutmanager.cpp" line="185"/>
        <source>Deal Failed</source>
        <translation>Falha no negócio</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutmanager.cpp" line="50"/>
        <source>Error occured when getting Nonce</source>
        <translation>Ocorreu um erro ao obter o Nonce</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutmanager.cpp" line="131"/>
        <source>Invalid Private Key</source>
        <translation>Chave privada inválida</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutmanager.cpp" line="180"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutmanager.cpp" line="180"/>
        <source>Deal is Done!</source>
        <translation>Negócio é feito!</translation>
    </message>
</context>
<context>
    <name>ETHTurnOutWidget</name>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="14"/>
        <source>ETHTurnOutWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="115"/>
        <source>ETH Balance</source>
        <translation>Equilíbrio ETH</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="144"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="181"/>
        <source>Amount</source>
        <translation>Montante</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="203"/>
        <source>Notice(optional)</source>
        <translation>Aviso (opcional)</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="225"/>
        <source>Receive Address</source>
        <translation>Receber endereço</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="257"/>
        <source>Enter Amount</source>
        <translation>Digite Montante</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="282"/>
        <source>Enter Notice</source>
        <translation>Insira o aviso</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="321"/>
        <source>Enter  the receiver&apos;s account</source>
        <translation>Digite a conta do destinatário</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="415"/>
        <source>Payment Address</source>
        <translation>Endereço de pagamento</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="507"/>
        <source>Cost of Miners</source>
        <translation>Custo dos Mineiros</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="523"/>
        <source>0.0004 PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="549"/>
        <source>0.0004</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="587"/>
        <source>0.006</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="644"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.cpp" line="97"/>
        <location filename="ETHWidget/ethturnoutwidget.cpp" line="101"/>
        <location filename="ETHWidget/ethturnoutwidget.cpp" line="146"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.cpp" line="97"/>
        <source>The amount and the address cannot be empty!</source>
        <translation>A quantidade e o endereço não podem estar vazios!</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.cpp" line="101"/>
        <source>Please enter valid number!</source>
        <translation>Por favor, insira um número válido!</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.cpp" line="107"/>
        <source>Please enter login password:</source>
        <translation>Por favor digite a senha de login:</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.cpp" line="146"/>
        <source>Incorrect password!</source>
        <translation>Senha incorreta!</translation>
    </message>
</context>
<context>
    <name>ETHWidget</name>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="14"/>
        <source>ETHWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="131"/>
        <source>ETH</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="173"/>
        <source>Restore Wallet</source>
        <translation>Restaurar carteira</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="275"/>
        <source>manage wallet</source>
        <translation>gerenciar carteira</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="350"/>
        <source>Copy</source>
        <translation>cópia de</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="383"/>
        <source>Copy Successed</source>
        <translation>Suceder</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="467"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="532"/>
        <source>Transfer In</source>
        <translation>Transferência em</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="565"/>
        <source>Details</source>
        <translation>Detalhes</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="598"/>
        <source>Transfer Out</source>
        <translation>Transferir</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="628"/>
        <source>assets</source>
        <translation>Transferir</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="663"/>
        <source>add</source>
        <translation>adicionar</translation>
    </message>
</context>
<context>
    <name>EWalletAssetsWidget</name>
    <message>
        <location filename="ewalletassetswidget.ui" line="14"/>
        <source>EWalletAssetsWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ewalletassetswidget.ui" line="104"/>
        <source>Refresh</source>
        <translation>Atualizar</translation>
    </message>
    <message>
        <location filename="ewalletassetswidget.ui" line="128"/>
        <source>Total Assets(CNY)</source>
        <translation>Ativos Totais (CNY)</translation>
    </message>
    <message>
        <location filename="ewalletassetswidget.ui" line="147"/>
        <source>0.00</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ewalletassetswidget.cpp" line="80"/>
        <source>≈</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>EWalletManager</name>
    <message>
        <location filename="ewalletmanager.cpp" line="209"/>
        <source>Wallet </source>
        <translation>Carteira </translation>
    </message>
</context>
<context>
    <name>EWalletWidget</name>
    <message>
        <location filename="ewalletwidget.ui" line="20"/>
        <source>EWalletWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="234"/>
        <source>Power</source>
        <translation>Poder</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="258"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="649"/>
        <source>Transfer Out</source>
        <translation>Transferir</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="765"/>
        <source>Transfer In</source>
        <translation>Transferência em</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="928"/>
        <source>Transaction Details</source>
        <translation>Detalhes da transação</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1027"/>
        <source>Send Tokens</source>
        <translation>Enviar fichas</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1132"/>
        <source>Safety Reinforcement</source>
        <translation>Reforço de Segurança</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1237"/>
        <source>Score Card</source>
        <translation>Cartão de Pontuação</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1336"/>
        <source>Send Red Packet</source>
        <translation>Envie um pacote vermelho</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1435"/>
        <source>Escrow Account</source>
        <translation>Conta de depósito</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1534"/>
        <source>My Device(s)</source>
        <translation>Meus dispositivos)</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1650"/>
        <source>Assets</source>
        <translation>Ativos</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1696"/>
        <source>PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1742"/>
        <source>ETH</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1785"/>
        <source>BTC</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1828"/>
        <source>EOS</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>HostingAccoutWidget</name>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="14"/>
        <source>HostingAccoutWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="72"/>
        <source>Hosting Account</source>
        <translation>Conta de Hospedagem</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="157"/>
        <source>Return</source>
        <translation>Retorna</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="163"/>
        <source>←</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="179"/>
        <source>ETH</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="252"/>
        <source>All Records</source>
        <translation>Todos os registros</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="292"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="441"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="226"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="250"/>
        <source>Top Up</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="332"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="490"/>
        <source>Withdraw</source>
        <translation>retirar o</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="369"/>
        <source>Others</source>
        <translation>Outros</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="224"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="294"/>
        <source>Get Red Packet</source>
        <translation>Obter pacote vermelho</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="228"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="272"/>
        <source>WithDraw</source>
        <translation>retirar o</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="232"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="254"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="276"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="298"/>
        <source>Confirmed</source>
        <translation>Confirmado</translation>
    </message>
</context>
<context>
    <name>HostingChargeWidget</name>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="14"/>
        <source>Hosting Charge</source>
        <oldsource>HostingChargeWidget</oldsource>
        <translation>Recarga de conta gerenciada</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="101"/>
        <source>PWR Tranfer</source>
        <translation>Transferência PWR</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="163"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="168"/>
        <source>Balance</source>
        <translation>Equilibrar</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="178"/>
        <source>0.00000000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="199"/>
        <source>PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="241"/>
        <source>Address</source>
        <translation>Endereço</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="260"/>
        <source>Hosting Address</source>
        <translation>Endereço de Hospedagem</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="302"/>
        <source>Amount</source>
        <translation>Montante</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="327"/>
        <source>Enter Amount</source>
        <translation>Digite Montante</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="389"/>
        <source>Payment Address</source>
        <translation>Endereço de pagamento</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="492"/>
        <source>Cost of Miners</source>
        <translation>Custo dos Mineiros</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="514"/>
        <source>0.0004 PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="576"/>
        <source>Next</source>
        <translation>Próximo</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="49"/>
        <source>Transfer</source>
        <translation>Transferir</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="57"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="108"/>
        <source>Wallet</source>
        <translation>Carteira</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="157"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="163"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="168"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="157"/>
        <source>Payment address cannot be empty</source>
        <translation>O endereço de pagamento não pode estar vazio</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="163"/>
        <source>Amount cannot be empty</source>
        <translation>O valor não pode estar vazio</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="168"/>
        <source>Amount is greater than</source>
        <translation>O valor é maior que</translation>
    </message>
</context>
<context>
    <name>HostingCoinWidget</name>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="14"/>
        <source>Hosting Coin</source>
        <oldsource>HostingCoinWidget</oldsource>
        <translation>Retirada da conta gerenciada</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="110"/>
        <source>Withdraw</source>
        <translation>retirar o</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="220"/>
        <source>Hosting Account</source>
        <translation>Conta de Hospedagem</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="237"/>
        <source>---------&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="293"/>
        <source>Local Wallet</source>
        <translation>Carteira local</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="350"/>
        <source>Available</source>
        <translation>acessível</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="363"/>
        <source>0PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="384"/>
        <source>Widthdraw All</source>
        <translation>Retirar tudo</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="444"/>
        <source>Receive Address</source>
        <translation>Receber endereço</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="520"/>
        <source>Amount</source>
        <translation>Montante</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="548"/>
        <source>Enter Amount</source>
        <translation>Digite Montante</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="626"/>
        <source>Poundage</source>
        <translation>Poundage</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="648"/>
        <source>0.002 PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="710"/>
        <source>Wthdraw</source>
        <translation>retirar o</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="58"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="98"/>
        <source>Wallet</source>
        <translation>Carteira</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="132"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="138"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="143"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="166"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="169"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="132"/>
        <source>Receive address cannot be empty</source>
        <translation>O endereço de pagamento não pode estar vazio</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="138"/>
        <source>Amount cannot be empty</source>
        <translation>O valor não pode estar vazio</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="143"/>
        <source>Amount is greater than available assets</source>
        <translation>O valor é maior que os ativos disponíveis</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="166"/>
        <source>Withdraw completed, Please check your balance later</source>
        <translation>Retirada concluída, por favor, verifique o seu saldo mais tarde</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="169"/>
        <source>Withdraw failed</source>
        <translation>Retirar falhou</translation>
    </message>
</context>
<context>
    <name>HostingItemWidget</name>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingitemwidget.ui" line="14"/>
        <source>HostingItemWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingitemwidget.ui" line="83"/>
        <source>pwr</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingitemwidget.ui" line="107"/>
        <source>11.00000000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingitemwidget.ui" line="126"/>
        <source>0.00000000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingitemwidget.ui" line="155"/>
        <source>Available</source>
        <translation>acessível</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingitemwidget.ui" line="171"/>
        <source>Frozen</source>
        <translation>gelado</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingitemwidget.ui" line="204"/>
        <source>details</source>
        <translation>Detalhes</translation>
    </message>
</context>
<context>
    <name>IntegralCardWidget</name>
    <message>
        <location filename="pwrWidget/integralCardWidget/integralcardwidget.ui" line="14"/>
        <source>IntegralCardWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/integralcardwidget.cpp" line="62"/>
        <location filename="pwrWidget/integralCardWidget/integralcardwidget.cpp" line="68"/>
        <location filename="pwrWidget/integralCardWidget/integralcardwidget.cpp" line="74"/>
        <location filename="pwrWidget/integralCardWidget/integralcardwidget.cpp" line="105"/>
        <source>Balance:</source>
        <translation>Equilibrar:</translation>
    </message>
</context>
<context>
    <name>MyDeviceWidget</name>
    <message>
        <location filename="pwrWidget/myDeviceWidget/mydevicewidget.ui" line="14"/>
        <source>MyDeviceWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/myDeviceWidget/mydevicewidget.ui" line="65"/>
        <source>My Device(s)</source>
        <translation>Meus dispositivos)</translation>
    </message>
</context>
<context>
    <name>PasswordWidget</name>
    <message>
        <location filename="pwrWidget/transferWidget/passwordwidget.ui" line="14"/>
        <source>PasswordWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/passwordwidget.ui" line="121"/>
        <source>Please Enter Your Base Password:</source>
        <translation>Por favor, digite sua senha de base:</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/passwordwidget.ui" line="225"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/passwordwidget.cpp" line="81"/>
        <location filename="pwrWidget/transferWidget/passwordwidget.cpp" line="89"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/passwordwidget.cpp" line="81"/>
        <source>Password cannot be empty!</source>
        <translation>A senha não pode estar vazia!</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/passwordwidget.cpp" line="89"/>
        <source>Incorrect Password!</source>
        <translation>Senha incorreta!</translation>
    </message>
</context>
<context>
    <name>RecordDetailPerWidget</name>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="14"/>
        <source>RecordDetailPerWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="77"/>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="188"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="238"/>
        <source>Outflow tansfer successed</source>
        <translation>Transferência de saída bem sucedida</translation>
    </message>
    <message>
        <source>The opposite side&apos;s interPlanet   ID:</source>
        <translation type="vanished">O ID do interPlanet do lado oposto:</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="308"/>
        <source>The opposite side&apos;s ID:</source>
        <oldsource>The opposite side&apos;s Telecomm ID:</oldsource>
        <translation>ID relativo:</translation>
    </message>
    <message>
        <source>Base ID:</source>
        <translation type="vanished">ID base:</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="385"/>
        <source>Acct.No</source>
        <translation>Conta</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="474"/>
        <source>Time:</source>
        <translation>Tempo:</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="566"/>
        <source>Tranfer ID:</source>
        <translation>ID da 
transferência:</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="677"/>
        <source>My ID:</source>
        <oldsource>My personal ID:</oldsource>
        <translation>Meu ID:</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="751"/>
        <source>Acct.No:</source>
        <translation>Conta:</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="877"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.cpp" line="68"/>
        <source>Transfer in successed</source>
        <translation>Transferência em sucesso</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.cpp" line="79"/>
        <source>Tansfer out successed</source>
        <translation>Transferência para fora suceder</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.cpp" line="88"/>
        <source>Records of PWR Transfer</source>
        <translation>Registros de Transferência PWR</translation>
    </message>
</context>
<context>
    <name>RecordDetailWidget</name>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="14"/>
        <source>RecordDetailWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="97"/>
        <source>Details</source>
        <translation>Detalhes</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="338"/>
        <source>Amount</source>
        <translation>Montante</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="429"/>
        <source>Cost of Miners</source>
        <translation>Custo dos 
Mineiros</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="511"/>
        <source>Receive Address</source>
        <translation>Receber endereço</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="611"/>
        <source>Payment Address</source>
        <translation>Endereço de 
pagamento</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="711"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="793"/>
        <source>Deal Number</source>
        <translation>Número da oferta</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="872"/>
        <source>Block</source>
        <translation>Quadra</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="937"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.cpp" line="63"/>
        <source> Transfer in successed</source>
        <translation> Transferência em sucesso</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.cpp" line="66"/>
        <source> Transfer out successed</source>
        <translation> Transferência para fora suceder</translation>
    </message>
</context>
<context>
    <name>RecordsWidget</name>
    <message>
        <location filename="childrenWidget/recordswidget.ui" line="14"/>
        <source>RecordsWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/recordswidget.ui" line="146"/>
        <source>All</source>
        <translation>Todos</translation>
    </message>
    <message>
        <location filename="childrenWidget/recordswidget.ui" line="180"/>
        <location filename="childrenWidget/recordswidget.ui" line="334"/>
        <source>Transfer Out</source>
        <translation>Transferir</translation>
    </message>
    <message>
        <location filename="childrenWidget/recordswidget.ui" line="211"/>
        <location filename="childrenWidget/recordswidget.ui" line="307"/>
        <source>Transfer In</source>
        <translation>Transferência em</translation>
    </message>
    <message>
        <location filename="childrenWidget/recordswidget.ui" line="242"/>
        <source>failure</source>
        <translation>falha</translation>
    </message>
</context>
<context>
    <name>SafeWidget</name>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.ui" line="14"/>
        <source>SafeWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.ui" line="164"/>
        <source>Backup private key</source>
        <translation>Chave de backup</translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.ui" line="295"/>
        <source>Please enter your base password</source>
        <translation>Digite sua senha base</translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.ui" line="391"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.ui" line="451"/>
        <source>Back up private key</source>
        <translation>Chave de backup</translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.ui" line="484"/>
        <source>Security warning:The private key is not encrypted,the export is risky,It is recommended to use mnemonic for backup</source>
        <translation>Atenção: A chave privada não é criptografada, a exportação é arriscada, é recomendável usar o mnemônico para backup</translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.ui" line="570"/>
        <source>Copy</source>
        <translation>cópia de</translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.cpp" line="68"/>
        <source>Empty Password</source>
        <translation>Senha Vazia</translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.cpp" line="75"/>
        <source>Incorrect Password</source>
        <translation>Senha incorreta</translation>
    </message>
</context>
<context>
    <name>StarFlashWidget</name>
    <message>
        <location filename="starflashwidget.ui" line="32"/>
        <source>StarFlashWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="79"/>
        <source>164953236</source>
        <translation></translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="101"/>
        <source>Refresh</source>
        <translation>Atualizar</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="158"/>
        <source>ID 190024</source>
        <oldsource>Personal ID 190024</oldsource>
        <translation></translation>
    </message>
    <message>
        <source>Base ID </source>
        <translation type="vanished">ID base </translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="184"/>
        <source>Acct.No</source>
        <translation>Conta</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="215"/>
        <source>Copy</source>
        <translation>cópia de</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="248"/>
        <source>Copy Successed</source>
        <translation>Suceder</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="299"/>
        <location filename="starflashwidget.ui" line="340"/>
        <location filename="starflashwidget.ui" line="381"/>
        <location filename="starflashwidget.ui" line="422"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="320"/>
        <source>Friend(s)</source>
        <translation>Amigos</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="361"/>
        <source>Group(s)</source>
        <oldsource>Tribe(s)</oldsource>
        <translation>Grupo</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="402"/>
        <source>Invitation(s)</source>
        <translation>Convite(s)</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="443"/>
        <source>Application(s)</source>
        <translation>Aplicativos)</translation>
    </message>
    <message>
        <source>Personal ID </source>
        <translation type="vanished">ID pessoal </translation>
    </message>
    <message>
        <location filename="starflashwidget.cpp" line="83"/>
        <location filename="starflashwidget.cpp" line="92"/>
        <source>ID </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TransferManager</name>
    <message>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="68"/>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="165"/>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="218"/>
        <source>Deal Failed</source>
        <translation>Falha no negócio</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="68"/>
        <source>Error occured when getting Nonce</source>
        <translation>Ocorreu um erro ao obter o Nonce</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="165"/>
        <source>Invalid private key</source>
        <translation>Chave privada inválida</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="225"/>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="228"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="225"/>
        <source>Top up successed，Please check your balance in your hosting account later</source>
        <translation>Top up bem-sucedido, por favor, verifique seu saldo em sua conta de hospedagem mais tarde</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="228"/>
        <source>Top up failed</source>
        <translation>Recarga falhou</translation>
    </message>
</context>
<context>
    <name>TransferWidget</name>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="20"/>
        <source>TransferWidget</source>
        <translation></translation>
    </message>
    <message>
        <source>Receiver&apos;s interPlanet ID</source>
        <translation type="vanished">ID do interplanetário do receptor</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="138"/>
        <source>Receiver&apos;s ID</source>
        <oldsource>Receiver&apos;s Telecomm ID</oldsource>
        <translation>ID do destinatário</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="160"/>
        <source>Receiver&apos;s Acct.No</source>
        <oldsource>Receiver&apos;s base ID</oldsource>
        <translation>Conta do destinatário</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="182"/>
        <source>Amount</source>
        <translation>Montante</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="220"/>
        <source>Enter account number</source>
        <translation>Digite o número da conta</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="251"/>
        <source>Enter wallet address</source>
        <translation>Digite o endereço da carteira</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="276"/>
        <source>Enter the amount of power</source>
        <translation>Digite a quantidade de energia</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="320"/>
        <source>Sender&apos;s Acct.No</source>
        <oldsource>Sender&apos;s base ID</oldsource>
        <translation>Conta do remetente</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="396"/>
        <source>Cost of Miners</source>
        <translation>Custo dos Mineiros</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="412"/>
        <source>0.0004 PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="438"/>
        <source>0.0004</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="476"/>
        <source>0.006</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="502"/>
        <source>Power Balance</source>
        <translation>Equilíbrio de poder</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="531"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="588"/>
        <source>Power Transfer</source>
        <translation>Transferência 
de Potência</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.cpp" line="100"/>
        <location filename="pwrWidget/transferWidget/transferwidget.cpp" line="103"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.cpp" line="100"/>
        <source>Input cannot be empty!</source>
        <translation>A entrada não pode estar vazia!</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.cpp" line="103"/>
        <source>Please enter valid number!</source>
        <translation>Por favor, insira um número válido!</translation>
    </message>
</context>
<context>
    <name>TurnInWidget</name>
    <message>
        <location filename="childrenWidget/turninwidget.ui" line="14"/>
        <source>TurnInWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/turninwidget.ui" line="314"/>
        <source>Replace
assets</source>
        <translation>Substituir 
ativos</translation>
    </message>
    <message>
        <location filename="childrenWidget/turninwidget.ui" line="350"/>
        <source>Specify
the amount</source>
        <translation>Especificamos
Quantidade</translation>
    </message>
    <message>
        <location filename="childrenWidget/turninwidget.cpp" line="56"/>
        <location filename="childrenWidget/turninwidget.cpp" line="153"/>
        <source>Please tranfer into </source>
        <translation>Por favor, transfira para </translation>
    </message>
    <message>
        <location filename="childrenWidget/turninwidget.cpp" line="87"/>
        <source>Please enter the amount:</source>
        <translation>Por favor insira o valor:</translation>
    </message>
    <message>
        <location filename="childrenWidget/turninwidget.cpp" line="96"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="childrenWidget/turninwidget.cpp" line="96"/>
        <source>the amount cannot be 0</source>
        <translation>o montante não pode ser 0</translation>
    </message>
</context>
<context>
    <name>TurnOutWidget</name>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="14"/>
        <source>TurnOutWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="115"/>
        <location filename="childrenWidget/turnoutwidget.cpp" line="68"/>
        <source>BTC Balance</source>
        <translation>Balanço BTC</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="144"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="176"/>
        <source>Amount</source>
        <translation>Montante</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="201"/>
        <source>Enter account number</source>
        <translation>Digite o número da conta</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="230"/>
        <source>Notice(optional)</source>
        <translation>Aviso (opcional)</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="255"/>
        <source>Enter Notice</source>
        <translation>Insira o aviso</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="284"/>
        <source>Receive Address</source>
        <translation>Receber endereço</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="346"/>
        <source>Enter  the receiver&apos;s account</source>
        <translation>Digite a conta do destinatário</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="394"/>
        <source>Payment Address</source>
        <translation>Endereço de pagamento</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="486"/>
        <source>Cost of Miners</source>
        <translation>Custo dos Mineiros</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="502"/>
        <source>0.0004 PWR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="528"/>
        <source>0.0004</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="566"/>
        <source>0.006</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="623"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.cpp" line="70"/>
        <source>EOS Balance</source>
        <translation>EOS Balance</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.cpp" line="104"/>
        <location filename="childrenWidget/turnoutwidget.cpp" line="108"/>
        <location filename="childrenWidget/turnoutwidget.cpp" line="146"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.cpp" line="104"/>
        <source>Incomplete input!</source>
        <translation>Entrada incompleta!</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.cpp" line="108"/>
        <source>Please enter valid number!</source>
        <translation>Por favor, insira um número válido!</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.cpp" line="114"/>
        <source>Please enter login password:</source>
        <translation>Por favor digite a senha de login:</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.cpp" line="146"/>
        <source>Incorrect password!</source>
        <translation>Senha incorreta!</translation>
    </message>
</context>
<context>
    <name>WalletInfoWidget</name>
    <message>
        <location filename="childrenWidget/walletinfowidget.ui" line="14"/>
        <source>WalletInfoWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.ui" line="61"/>
        <source>Set wallet&apos;s avatar and name</source>
        <translation>Definir avatar e nome</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.ui" line="131"/>
        <location filename="childrenWidget/walletinfowidget.ui" line="161"/>
        <source>set avatar</source>
        <translation>definir avatar</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.ui" line="167"/>
        <source>Click here to set avatar</source>
        <translation>Clique aqui para definir o avatar</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.ui" line="195"/>
        <source>Wallet name</source>
        <translation>Nome da carteira</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.ui" line="222"/>
        <source>Next</source>
        <translation>Próximo</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.cpp" line="55"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.cpp" line="55"/>
        <source>The name cannot be empty</source>
        <translation>O nome não pode estar vazio</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.cpp" line="81"/>
        <source>Open Image</source>
        <translation>Abrir imagem</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.cpp" line="81"/>
        <source>Image File(*.bmp;*.jpeg;*.jpg;*.png)</source>
        <translation>Arquivo de Imagem (*. Bmp; *. Jpeg; *. Jpg; *. Png)</translation>
    </message>
</context>
<context>
    <name>WalletManager</name>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="14"/>
        <source>WalletManager</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="49"/>
        <source>Manage</source>
        <translation>Gerir</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="175"/>
        <source>Change avatar</source>
        <translation>Mudar Avatar</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="212"/>
        <source>Change Avatar</source>
        <translation>Mudar Avatar</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="284"/>
        <location filename="childrenWidget/walletmanager.ui" line="300"/>
        <source>Change Wallet&apos;s Name</source>
        <translation>Alterar nome da carteira</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="339"/>
        <source>Delete Wallet</source>
        <translation>Excluir carteira</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="355"/>
        <source>Back Up Private Key</source>
        <translation>Chave privada de backup</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="401"/>
        <source>Backup Private Key</source>
        <translation>Chave privada de backup</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="442"/>
        <location filename="childrenWidget/walletmanager.ui" line="488"/>
        <source>Switch Account</source>
        <translation>Mudar de conta</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.cpp" line="54"/>
        <source>Open Image</source>
        <translation>Abrir imagem</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.cpp" line="54"/>
        <source>Image File(*.bmp;*.jpeg;*.jpg;*.png)</source>
        <translation>Arquivo de Imagem (*. Bmp; *. Jpeg; *. Jpg; *. Png)</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.cpp" line="95"/>
        <source>Please enter your login password:</source>
        <translation>Por favor, digite sua senha de login:</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.cpp" line="265"/>
        <location filename="childrenWidget/walletmanager.cpp" line="293"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.cpp" line="265"/>
        <source>Incorrect password!</source>
        <translation>Senha incorreta!</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.cpp" line="293"/>
        <source>Main wallet cannot be deleted!</source>
        <translation>A carteira principal não pode ser excluída!</translation>
    </message>
</context>
<context>
    <name>WalletPriKeyWidget</name>
    <message>
        <location filename="childrenWidget/walletprikeywidget.ui" line="14"/>
        <source>WalletPriKeyWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="childrenWidget/walletprikeywidget.ui" line="61"/>
        <source>Backup Private Key</source>
        <translation>Chave privada de backup</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletprikeywidget.ui" line="97"/>
        <source>Copy</source>
        <translation>cópia de</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletprikeywidget.ui" line="119"/>
        <source>Safety warning: It&apos;s risky to export with private key which is not encrypted,Back up with the mnemonic word is recommended.</source>
        <translation>Aviso de segurança: É arriscado exportar com chave privada que não é criptografada. Recomenda-se o backup com a palavra mnemônica.</translation>
    </message>
</context>
<context>
    <name>hostingRecordItemWidget</name>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.ui" line="14"/>
        <source>hostingRecordItemWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.ui" line="89"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.cpp" line="31"/>
        <source>Get Red Packet</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.ui" line="111"/>
        <source>2018-09-30 18:09:11</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.ui" line="137"/>
        <source>11.00000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.ui" line="156"/>
        <source>Confirmed</source>
        <translation>Confirmado</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.cpp" line="35"/>
        <source>Top Up</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.cpp" line="39"/>
        <source>Withdraw</source>
        <translation>retirar o</translation>
    </message>
</context>
</TS>
