<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>BTCManager</name>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="159"/>
        <location filename="BTCWidget/btcmanager.cpp" line="188"/>
        <location filename="BTCWidget/btcmanager.cpp" line="213"/>
        <location filename="BTCWidget/btcmanager.cpp" line="219"/>
        <location filename="BTCWidget/btcmanager.cpp" line="399"/>
        <location filename="BTCWidget/btcmanager.cpp" line="473"/>
        <location filename="BTCWidget/btcmanager.cpp" line="492"/>
        <source>Notice</source>
        <translation>уведомление</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="159"/>
        <source>BTC cannot be restored by private key for now, please use the mnemonic word</source>
        <translation>BTC пока не может быть восстановлен с помощью закрытого ключа, пожалуйста, используйте мнемоническое слово</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="188"/>
        <source>Failed to restore the wallet!</source>
        <translation>Не удалось восстановить кошелек!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="213"/>
        <source>The wallet restored successfully!</source>
        <translation>Кошелек успешно восстановлен!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="219"/>
        <source>The server returned error!</source>
        <translation>Сервер вернул ошибку!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="228"/>
        <source>Refresh Wallet</source>
        <translation>Обновить кошелек</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="234"/>
        <source>Import Wallet</source>
        <translation>Импортный кошелек</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="246"/>
        <source>Switch Wallet </source>
        <translation>Переключить кошелек </translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="399"/>
        <source>The wallet deleted successfully!</source>
        <translation>Кошелек успешно удален!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="473"/>
        <source>There is no BTC wallet!</source>
        <translation>Нет BTC кошелька!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcmanager.cpp" line="492"/>
        <source>The opposite side have no account!</source>
        <translation>У противоположной стороны нет аккаунта!</translation>
    </message>
</context>
<context>
    <name>BTCTurnOutManager</name>
    <message>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="38"/>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="57"/>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="140"/>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="162"/>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="166"/>
        <source>Notice</source>
        <translation>уведомление</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="38"/>
        <source>Cannot find the private key of the wallet!</source>
        <translation>Не удается найти закрытый ключ кошелька!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="57"/>
        <source>Failed to get Utxo!</source>
        <translation>Не удалось получить Utxo!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="140"/>
        <source>Signature generation failed!</source>
        <translation>Генерация подписи не удалась!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="162"/>
        <source>Deal is Done!</source>
        <translation>Сделка завершена!</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcturnoutmanager.cpp" line="166"/>
        <source>Deal Failed!</source>
        <translation>Сделка не удалась!</translation>
    </message>
</context>
<context>
    <name>BTCWidget</name>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="14"/>
        <source>BTCWidget</source>
        <translation>BTCWidget</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="131"/>
        <source>BTC</source>
        <translation>BTC</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="173"/>
        <source>Restore Wallet</source>
        <translation>Восстановить кошелек</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="275"/>
        <source>manage wallet</source>
        <translation>управлять кошельком</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="350"/>
        <source>Copy</source>
        <translation>копия</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="383"/>
        <source>Copy successed</source>
        <translation>преуспевать</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="467"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="532"/>
        <source>Transfer In</source>
        <translation>Трансфер в</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="578"/>
        <source>Details</source>
        <translation>подробности</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="624"/>
        <source>Transfer Out</source>
        <translation>Трансфер из</translation>
    </message>
    <message>
        <location filename="BTCWidget/btcwidget.ui" line="654"/>
        <source>assets</source>
        <translation>активы</translation>
    </message>
</context>
<context>
    <name>CardItemWidget</name>
    <message>
        <location filename="pwrWidget/integralCardWidget/carditemwidget.ui" line="26"/>
        <source>CardItemWidget</source>
        <translation>CardItemWidget</translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/carditemwidget.ui" line="96"/>
        <source>0xd0ee****d73e</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/carditemwidget.ui" line="120"/>
        <source>Balance:0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/carditemwidget.ui" line="148"/>
        <source> Top Up &gt;</source>
        <translation>Пополнить&gt;</translation>
    </message>
</context>
<context>
    <name>ChangeIntoWidget</name>
    <message>
        <location filename="pwrWidget/changeintoWidget/changeintowidget.ui" line="14"/>
        <source>ChangeIntoWidget</source>
        <translation>ChangeIntoWidget</translation>
    </message>
    <message>
        <location filename="pwrWidget/changeintoWidget/changeintowidget.ui" line="77"/>
        <source>Scan the QR code to exchange power</source>
        <translation>Отсканируйте QR-код</translation>
    </message>
    <message>
        <location filename="pwrWidget/changeintoWidget/changeintowidget.ui" line="214"/>
        <location filename="pwrWidget/changeintoWidget/changeintowidget.ui" line="220"/>
        <source>Set Aomunt</source>
        <translation>Количество</translation>
    </message>
    <message>
        <location filename="pwrWidget/changeintoWidget/changeintowidget.ui" line="251"/>
        <location filename="pwrWidget/changeintoWidget/changeintowidget.ui" line="306"/>
        <source>Exchange Records</source>
        <translation>Обмен записями</translation>
    </message>
</context>
<context>
    <name>ChangeNumWidget</name>
    <message>
        <location filename="pwrWidget/changeintoWidget/changenumwidget.ui" line="14"/>
        <source>ChangeNumWidget</source>
        <translation>ChangeNumWidget</translation>
    </message>
    <message>
        <location filename="pwrWidget/changeintoWidget/changenumwidget.ui" line="71"/>
        <source>Amount</source>
        <translation>Количество</translation>
    </message>
    <message>
        <location filename="pwrWidget/changeintoWidget/changenumwidget.ui" line="167"/>
        <source>OK</source>
        <translation>Хорошо</translation>
    </message>
</context>
<context>
    <name>ChargeWidget</name>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="14"/>
        <source>Charge</source>
        <oldsource>ChargeWidget</oldsource>
        <translation>Обвинять</translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="61"/>
        <source>Top Up</source>
        <translation>Пополнить</translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="130"/>
        <source>Top-Up Way</source>
        <translation>Пополнение пути</translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="158"/>
        <source>Alipay</source>
        <translation>Alipay</translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="167"/>
        <source>WeChat</source>
        <translation>WeChat</translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="194"/>
        <source>Daily max amount￥50000.00</source>
        <translation>Максимальная суточная сумма ￥ 50000,00</translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="229"/>
        <source>Amount</source>
        <translation>Количество</translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="278"/>
        <source>￥</source>
        <translation>¥</translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/chargewidget.ui" line="349"/>
        <source>Next</source>
        <translation>следующий</translation>
    </message>
</context>
<context>
    <name>DetailsWidget</name>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="14"/>
        <source>DetailsWidget</source>
        <translation>DetailsWidget</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="100"/>
        <source>Exchange Detail</source>
        <translation>Обмен детали</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="185"/>
        <source>Exchange successed!</source>
        <translation>Обмен выполнен успешно!</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="244"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="260"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="276"/>
        <source>Power</source>
        <translation>Мощность</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="348"/>
        <source>Exchange ID</source>
        <translation>Обмен ID</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="436"/>
        <source>Exchange Time</source>
        <translation>Время обмена</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="521"/>
        <source>Account Send</source>
        <translation>Аккаунт Отправить</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="606"/>
        <source>Account Receive</source>
        <translation>Получение аккаунта</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/detailswidget.ui" line="680"/>
        <source>OK</source>
        <translation>Хорошо</translation>
    </message>
</context>
<context>
    <name>DeviceItemWidget</name>
    <message>
        <location filename="pwrWidget/myDeviceWidget/deviceitemwidget.ui" line="14"/>
        <source>DeviceItemWidget</source>
        <translation>DeviceItemWidget</translation>
    </message>
    <message>
        <location filename="pwrWidget/myDeviceWidget/deviceitemwidget.ui" line="89"/>
        <source>ONEPLUS A600</source>
        <translation>ONEPLUS A600</translation>
    </message>
    <message>
        <location filename="pwrWidget/myDeviceWidget/deviceitemwidget.ui" line="111"/>
        <source>2019-09-30 11:31:19</source>
        <translation>2019-09-30 11:31:19</translation>
    </message>
    <message>
        <location filename="pwrWidget/myDeviceWidget/deviceitemwidget.ui" line="144"/>
        <source>This Device</source>
        <translation>ток устройство</translation>
    </message>
    <message>
        <location filename="pwrWidget/myDeviceWidget/deviceitemwidget.cpp" line="46"/>
        <source>Offline</source>
        <translation>Не в сети</translation>
    </message>
</context>
<context>
    <name>EOSManager</name>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="135"/>
        <location filename="EOSWidget/eosmanager.cpp" line="168"/>
        <location filename="EOSWidget/eosmanager.cpp" line="172"/>
        <location filename="EOSWidget/eosmanager.cpp" line="240"/>
        <location filename="EOSWidget/eosmanager.cpp" line="417"/>
        <location filename="EOSWidget/eosmanager.cpp" line="436"/>
        <source>Notice</source>
        <translation>уведомление</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="135"/>
        <source>Account Verification Failed!</source>
        <translation>Ошибка проверки аккаунта!</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="168"/>
        <source>EOS Wallet Restored Successfully!</source>
        <translation>EOS Кошелек успешно восстановлен!</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="172"/>
        <source>Failed to Restore EOS Wallet!</source>
        <translation>Не удалось восстановить кошелек EOS!</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="240"/>
        <source>Wallet Deleted Successfully!</source>
        <translation>Кошелек успешно удален!</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="266"/>
        <source>Refresh Wallet</source>
        <translation>Обновить кошелек</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="272"/>
        <source>Import Wallet</source>
        <translation>Импортный кошелек</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="283"/>
        <source>Switch Wallet </source>
        <translation>Переключить кошелек </translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="417"/>
        <source>No EOS Wallet!</source>
        <translation>Нет EOS Кошелек!</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosmanager.cpp" line="436"/>
        <source>The opposite side have no account!</source>
        <translation>У противоположной стороны нет аккаунта!</translation>
    </message>
</context>
<context>
    <name>EOSRecoveryWidget</name>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.ui" line="14"/>
        <source>EOS Recovery</source>
        <oldsource>EOSRecoveryWidget</oldsource>
        <translation>EOS кошелек Восстановление</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.ui" line="79"/>
        <source>Restore Wallet Account</source>
        <translation>Восстановить аккаунт</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.ui" line="131"/>
        <source>Restore Your Wallet Accout By Private Key</source>
        <translation>По секретному ключу</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.ui" line="165"/>
        <source>Please Enter OWN Private Key</source>
        <translation>Пожалуйста, введите свой личный ключ</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.ui" line="190"/>
        <source>Please Enter ACTIVE Private Key</source>
        <translation>Пожалуйста, введите АКТИВНЫЙ закрытый ключ</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.ui" line="215"/>
        <source>Finished</source>
        <translation>Законченный</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.cpp" line="62"/>
        <location filename="EOSWidget/eosrecoverywidget.cpp" line="72"/>
        <source>Notice</source>
        <translation>уведомление</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.cpp" line="62"/>
        <source>Private Key not Entered Completely!</source>
        <translation>Закрытый ключ не введен полностью!</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosrecoverywidget.cpp" line="72"/>
        <source>Incorrect Private Key，Failed to Get Public Key!</source>
        <translation>Неверный закрытый ключ ， Не удалось получить открытый ключ!</translation>
    </message>
</context>
<context>
    <name>EOSTurnOutManager</name>
    <message>
        <location filename="EOSWidget/eosturnoutmanager.cpp" line="24"/>
        <location filename="EOSWidget/eosturnoutmanager.cpp" line="87"/>
        <location filename="EOSWidget/eosturnoutmanager.cpp" line="90"/>
        <source>Notice</source>
        <translation>уведомление</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosturnoutmanager.cpp" line="24"/>
        <source>Failed to get the private key of EOS wallet!</source>
        <translation>Не удалось получить закрытый ключ кошелька EOS!</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosturnoutmanager.cpp" line="87"/>
        <source>Deal is Done!</source>
        <translation>Сделка завершена!</translation>
    </message>
    <message>
        <location filename="EOSWidget/eosturnoutmanager.cpp" line="90"/>
        <source>Deal Failed!</source>
        <translation>Сделка не удалась!</translation>
    </message>
</context>
<context>
    <name>EOSWidget</name>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="14"/>
        <source>EOSWidget</source>
        <translation>EOSWidget</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="131"/>
        <source>EOS</source>
        <translation>EOS</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="173"/>
        <source>Restore Wallet</source>
        <translation>Восстановить кошелек</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="275"/>
        <source>manage wallet</source>
        <translation>управлять кошельком</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="350"/>
        <source>Copy</source>
        <translation>копия</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="383"/>
        <source>Copy successed</source>
        <translation>преуспевать</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="467"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="532"/>
        <source>Transfer In</source>
        <translation>Трансфер в</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="581"/>
        <source>Details</source>
        <translation>подробности</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="627"/>
        <source>Transfer Out</source>
        <translation>Трансфер из</translation>
    </message>
    <message>
        <location filename="EOSWidget/eoswidget.ui" line="657"/>
        <source>assets</source>
        <translation>активы</translation>
    </message>
</context>
<context>
    <name>ETHManager</name>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="258"/>
        <location filename="ETHWidget/ethmanager.cpp" line="283"/>
        <location filename="ETHWidget/ethmanager.cpp" line="289"/>
        <location filename="ETHWidget/ethmanager.cpp" line="588"/>
        <location filename="ETHWidget/ethmanager.cpp" line="595"/>
        <location filename="ETHWidget/ethmanager.cpp" line="616"/>
        <source>Notice</source>
        <translation>уведомление</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="258"/>
        <source>Failed to restore wallet!</source>
        <translation>Не удалось восстановить кошелек!</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="283"/>
        <source>Wallet restored successfully!</source>
        <translation>Кошелек успешно восстановлен!</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="289"/>
        <source>The server returned error!</source>
        <translation>Сервер вернул ошибку!</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="298"/>
        <source>Refresh Wallet</source>
        <translation>Обновить кошелек</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="304"/>
        <source>Import Wallet</source>
        <translation>Импортный кошелек</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="315"/>
        <source>Switch Wallet </source>
        <translation>Переключить кошелек </translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="588"/>
        <source>Wallet Deleted Successfully!</source>
        <translation>Кошелек успешно удален!</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="595"/>
        <source>No ETH Wallet!</source>
        <translation>Нет ETH Кошелек!</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethmanager.cpp" line="616"/>
        <source>The opposite side have no account!</source>
        <translation>У противоположной стороны нет аккаунта!</translation>
    </message>
</context>
<context>
    <name>ETHTokenListWidget</name>
    <message>
        <location filename="ETHWidget/ethtokenlistwidget.ui" line="14"/>
        <source>ETHTokenListWidget</source>
        <translation>ETHTokenListWidget</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethtokenlistwidget.ui" line="77"/>
        <source>Replace Assets</source>
        <translation>Заменить активы</translation>
    </message>
</context>
<context>
    <name>ETHTurnOutManager</name>
    <message>
        <location filename="ETHWidget/ethturnoutmanager.cpp" line="50"/>
        <location filename="ETHWidget/ethturnoutmanager.cpp" line="131"/>
        <location filename="ETHWidget/ethturnoutmanager.cpp" line="185"/>
        <source>Deal Failed</source>
        <translation>Сделка не удалась</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutmanager.cpp" line="50"/>
        <source>Error occured when getting Nonce</source>
        <translation>Произошла ошибка при получении Nonce</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutmanager.cpp" line="131"/>
        <source>Invalid Private Key</source>
        <translation>Неверный закрытый ключ</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutmanager.cpp" line="180"/>
        <source>Notice</source>
        <translation>уведомление</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutmanager.cpp" line="180"/>
        <source>Deal is Done!</source>
        <translation>Сделка завершена!</translation>
    </message>
</context>
<context>
    <name>ETHTurnOutWidget</name>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="14"/>
        <source>ETHTurnOutWidget</source>
        <translation>ETHTurnOutWidget</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="115"/>
        <source>ETH Balance</source>
        <translation>ETH Баланс</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="144"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="181"/>
        <source>Amount</source>
        <translation>Количество</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="257"/>
        <source>Enter Amount</source>
        <translation>Вносить количество</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="203"/>
        <source>Notice(optional)</source>
        <translation>Примечание </translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="282"/>
        <source>Enter Notice</source>
        <translation>Введите уведомление</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="225"/>
        <source>Receive Address</source>
        <translation>Получить адрес</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="321"/>
        <source>Enter  the receiver&apos;s account</source>
        <translation>Введите счет получателя</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="415"/>
        <source>Payment Address</source>
        <translation>Платежный адрес</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="507"/>
        <source>Cost of Miners</source>
        <translation>Стоимость майнеров</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="523"/>
        <source>0.0004 PWR</source>
        <translation>0.0004 PWR</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="549"/>
        <source>0.0004</source>
        <translation>0.0004</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="587"/>
        <source>0.006</source>
        <translation>0.006</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.ui" line="644"/>
        <source>OK</source>
        <translation>Хорошо</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.cpp" line="97"/>
        <location filename="ETHWidget/ethturnoutwidget.cpp" line="101"/>
        <location filename="ETHWidget/ethturnoutwidget.cpp" line="146"/>
        <source>Notice</source>
        <translation>уведомление</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.cpp" line="97"/>
        <source>The amount and the address cannot be empty!</source>
        <translation>Сумма и адрес не могут быть пустыми!</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.cpp" line="101"/>
        <source>Please enter valid number!</source>
        <translation>Пожалуйста, введите правильный номер!</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.cpp" line="107"/>
        <source>Please enter login password:</source>
        <translation>Пожалуйста, введите пароль для входа:</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethturnoutwidget.cpp" line="146"/>
        <source>Incorrect password!</source>
        <translation>Неверный пароль!</translation>
    </message>
</context>
<context>
    <name>ETHWidget</name>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="14"/>
        <source>ETHWidget</source>
        <translation>ETHWidget</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="131"/>
        <source>ETH</source>
        <translation>ETH</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="173"/>
        <source>Restore Wallet</source>
        <translation>Восстановить кошелек</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="275"/>
        <source>manage wallet</source>
        <translation>управлять кошельком</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="350"/>
        <source>Copy</source>
        <translation>копия</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="383"/>
        <source>Copy Successed</source>
        <translation>преуспевать</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="467"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="532"/>
        <source>Transfer In</source>
        <translation>Трансфер в</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="565"/>
        <source>Details</source>
        <translation>подробности</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="598"/>
        <source>Transfer Out</source>
        <translation>Трансфер из</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="628"/>
        <source>assets</source>
        <translation>активы</translation>
    </message>
    <message>
        <location filename="ETHWidget/ethwidget.ui" line="663"/>
        <source>add</source>
        <translation>добавлять</translation>
    </message>
</context>
<context>
    <name>EWalletAssetsWidget</name>
    <message>
        <location filename="ewalletassetswidget.ui" line="14"/>
        <source>EWalletAssetsWidget</source>
        <translation>EWalletAssetsWidget</translation>
    </message>
    <message>
        <location filename="ewalletassetswidget.ui" line="104"/>
        <source>Refresh</source>
        <translation>обновление</translation>
    </message>
    <message>
        <location filename="ewalletassetswidget.ui" line="128"/>
        <source>Total Assets(CNY)</source>
        <translation>Всего активов (CNY)</translation>
    </message>
    <message>
        <location filename="ewalletassetswidget.ui" line="147"/>
        <source>0.00</source>
        <translation>0.00</translation>
    </message>
    <message>
        <location filename="ewalletassetswidget.cpp" line="80"/>
        <source>≈</source>
        <translation>≈</translation>
    </message>
</context>
<context>
    <name>EWalletManager</name>
    <message>
        <location filename="ewalletmanager.cpp" line="209"/>
        <source>Wallet </source>
        <translation>Бумажник </translation>
    </message>
</context>
<context>
    <name>EWalletWidget</name>
    <message>
        <location filename="ewalletwidget.ui" line="20"/>
        <source>EWalletWidget</source>
        <translation>EWalletWidget</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="234"/>
        <source>Power</source>
        <translation>Мощность</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="258"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="649"/>
        <source>Transfer Out</source>
        <translation>Трансфер из</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="765"/>
        <source>Transfer In</source>
        <translation>Трансфер в</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="928"/>
        <source>Transaction Details</source>
        <translation>Детали транзакции</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1027"/>
        <source>Send Tokens</source>
        <translation>Отправить токены</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1132"/>
        <source>Safety Reinforcement</source>
        <translation>Усиление безопасности</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1237"/>
        <source>Score Card</source>
        <translation>Счетная карточка</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1336"/>
        <source>Send Red Packet</source>
        <translation>Отправить красный пакет</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1435"/>
        <source>Escrow Account</source>
        <translation>Депозитный счет</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1534"/>
        <source>My Device(s)</source>
        <translation>Мое устройство (а)</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1650"/>
        <source>Assets</source>
        <translation>активы</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1696"/>
        <source>PWR</source>
        <translation>PWR</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1742"/>
        <source>ETH</source>
        <translation>ETH</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1785"/>
        <source>BTC</source>
        <translation>BTC</translation>
    </message>
    <message>
        <location filename="ewalletwidget.ui" line="1828"/>
        <source>EOS</source>
        <translation>EOS</translation>
    </message>
</context>
<context>
    <name>HostingAccoutWidget</name>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="14"/>
        <source>HostingAccoutWidget</source>
        <translation>HostingAccoutWidget</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="72"/>
        <source>Hosting Account</source>
        <translation>Хостинг Аккаунт</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="157"/>
        <source>Return</source>
        <translation>Вернуть</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="163"/>
        <source>←</source>
        <translation>←</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="179"/>
        <source>ETH</source>
        <translation>ETH</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="252"/>
        <source>All Records</source>
        <translation>Все записи</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="292"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="441"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="226"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="250"/>
        <source>Top Up</source>
        <translation>Пополнить</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="332"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="490"/>
        <source>Withdraw</source>
        <translation>Изымать</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.ui" line="369"/>
        <source>Others</source>
        <translation>другие</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="224"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="294"/>
        <source>Get Red Packet</source>
        <translation>Получить красный пакет</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="228"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="272"/>
        <source>WithDraw</source>
        <translation>Изымать</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="232"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="254"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="276"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingaccoutwidget.cpp" line="298"/>
        <source>Confirmed</source>
        <translation>подтвердил</translation>
    </message>
</context>
<context>
    <name>HostingChargeWidget</name>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="14"/>
        <source>Hosting Charge</source>
        <oldsource>HostingChargeWidget</oldsource>
        <translation>Пополнение управляемого счета</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="101"/>
        <source>PWR Tranfer</source>
        <translation>Передача PWR</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="163"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="168"/>
        <source>Balance</source>
        <translation>Остаток средств</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="178"/>
        <source>0.00000000</source>
        <translation>0.00000000</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="199"/>
        <source>PWR</source>
        <translation>PWR</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="241"/>
        <source>Address</source>
        <translation>Адрес</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="260"/>
        <source>Hosting Address</source>
        <translation>Адрес хостинга</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="302"/>
        <source>Amount</source>
        <translation>Количество</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="327"/>
        <source>Enter Amount</source>
        <translation>Вносить количество</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="389"/>
        <source>Payment Address</source>
        <translation>Платежный адрес</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="492"/>
        <source>Cost of Miners</source>
        <translation>Стоимость майнеров</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="514"/>
        <source>0.0004 PWR</source>
        <translation>0.0004 PWR</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.ui" line="576"/>
        <source>Next</source>
        <translation>следующий</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="49"/>
        <source>Transfer</source>
        <translation>Перечислить</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="57"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="108"/>
        <source>Wallet</source>
        <translation>Бумажник</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="157"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="163"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="168"/>
        <source>Notice</source>
        <translation>уведомление</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="157"/>
        <source>Payment address cannot be empty</source>
        <translation>Платежный адрес не может быть пустым</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="163"/>
        <source>Amount cannot be empty</source>
        <translation>Сумма не может быть пустой</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingchargewidget.cpp" line="168"/>
        <source>Amount is greater than</source>
        <translation>Сумма больше чем</translation>
    </message>
</context>
<context>
    <name>HostingCoinWidget</name>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="14"/>
        <source>Hosting Coin</source>
        <oldsource>HostingCoinWidget</oldsource>
        <translation>Управляемый вывод средств</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="110"/>
        <source>Withdraw</source>
        <translation>Изымать</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="220"/>
        <source>Hosting Account</source>
        <translation>Хостинг Аккаунт</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="237"/>
        <source>---------&gt;</source>
        <translation>---------&gt;</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="293"/>
        <source>Local Wallet</source>
        <translation>Местный кошелек</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="350"/>
        <source>Available</source>
        <translation>Имеется в наличии</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="363"/>
        <source>0PWR</source>
        <translation>0PWR</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="384"/>
        <source>Widthdraw All</source>
        <translation>Снять все</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="444"/>
        <source>Receive Address</source>
        <translation>Получить адрес</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="520"/>
        <source>Amount</source>
        <translation>Количество</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="548"/>
        <source>Enter Amount</source>
        <translation>Вносить количество</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="626"/>
        <source>Poundage</source>
        <translation>пошлина с веса</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="648"/>
        <source>0.002 PWR</source>
        <translation>0.002 PWR</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.ui" line="710"/>
        <source>Wthdraw</source>
        <translation>Изымать</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="58"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="98"/>
        <source>Wallet</source>
        <translation>Бумажник</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="132"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="138"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="143"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="166"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="169"/>
        <source>Notice</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="132"/>
        <source>Receive address cannot be empty</source>
        <translation>Адрес получения не может быть пустым</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="138"/>
        <source>Amount cannot be empty</source>
        <translation>Сумма не может быть пустой</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="143"/>
        <source>Amount is greater than available assets</source>
        <translation>Сумма больше доступных активов</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="166"/>
        <source>Withdraw completed, Please check your balance later</source>
        <translation>Снятие завершено, пожалуйста, проверьте свой баланс позже</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingcoinwidget.cpp" line="169"/>
        <source>Withdraw failed</source>
        <translation>Снятие не удалось</translation>
    </message>
</context>
<context>
    <name>HostingItemWidget</name>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingitemwidget.ui" line="14"/>
        <source>HostingItemWidget</source>
        <translation>HostingItemWidget</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingitemwidget.ui" line="83"/>
        <source>pwr</source>
        <translation>pwr</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingitemwidget.ui" line="107"/>
        <source>11.00000000</source>
        <translation>11.00000000</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingitemwidget.ui" line="126"/>
        <source>0.00000000</source>
        <translation>0.00000000</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingitemwidget.ui" line="155"/>
        <source>Available</source>
        <translation>налицо</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingitemwidget.ui" line="171"/>
        <source>Frozen</source>
        <translation>мерзлый</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingitemwidget.ui" line="204"/>
        <source>details</source>
        <translation>подробности</translation>
    </message>
</context>
<context>
    <name>IntegralCardWidget</name>
    <message>
        <location filename="pwrWidget/integralCardWidget/integralcardwidget.ui" line="14"/>
        <source>IntegralCardWidget</source>
        <translation>IntegralCardWidget</translation>
    </message>
    <message>
        <location filename="pwrWidget/integralCardWidget/integralcardwidget.cpp" line="62"/>
        <location filename="pwrWidget/integralCardWidget/integralcardwidget.cpp" line="68"/>
        <location filename="pwrWidget/integralCardWidget/integralcardwidget.cpp" line="74"/>
        <location filename="pwrWidget/integralCardWidget/integralcardwidget.cpp" line="105"/>
        <source>Balance:</source>
        <translation>Остаток средств:</translation>
    </message>
</context>
<context>
    <name>MyDeviceWidget</name>
    <message>
        <location filename="pwrWidget/myDeviceWidget/mydevicewidget.ui" line="14"/>
        <source>MyDeviceWidget</source>
        <translation>MyDeviceWidget</translation>
    </message>
    <message>
        <location filename="pwrWidget/myDeviceWidget/mydevicewidget.ui" line="65"/>
        <source>My Device(s)</source>
        <translation>Мое устройство (а)</translation>
    </message>
</context>
<context>
    <name>PasswordWidget</name>
    <message>
        <location filename="pwrWidget/transferWidget/passwordwidget.ui" line="14"/>
        <source>PasswordWidget</source>
        <translation>PasswordWidget</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/passwordwidget.ui" line="121"/>
        <source>Please Enter Your Base Password:</source>
        <translation>Введите свой базовый пароль:</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/passwordwidget.ui" line="225"/>
        <source>OK</source>
        <translation>Хорошо</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/passwordwidget.cpp" line="81"/>
        <location filename="pwrWidget/transferWidget/passwordwidget.cpp" line="89"/>
        <source>Notice</source>
        <translation>уведомление</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/passwordwidget.cpp" line="81"/>
        <source>Password cannot be empty!</source>
        <translation>Пароль не может быть пустым!</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/passwordwidget.cpp" line="89"/>
        <source>Incorrect Password!</source>
        <translation>Неверный пароль!</translation>
    </message>
</context>
<context>
    <name>RecordDetailPerWidget</name>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="14"/>
        <source>RecordDetailPerWidget</source>
        <translation>RecordDetailPerWidget</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="77"/>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="188"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="238"/>
        <source>Outflow tansfer successed</source>
        <translation>Успешная передача</translation>
    </message>
    <message>
        <source>The opposite side&apos;s interPlanet   ID:</source>
        <translation type="vanished">InterPlanet ID противоположной стороны:</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="308"/>
        <source>The opposite side&apos;s ID:</source>
        <oldsource>The opposite side&apos;s Telecomm ID:</oldsource>
        <translation>Относительный ID:</translation>
    </message>
    <message>
        <source>Base ID:</source>
        <translation type="vanished">База ID:</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="385"/>
        <source>Acct.No</source>
        <translation>счета</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="474"/>
        <source>Time:</source>
        <translation>Время:</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="566"/>
        <source>Tranfer ID:</source>
        <translation>перевода ID:</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="677"/>
        <source>My ID:</source>
        <oldsource>My personal ID:</oldsource>
        <translation>Мой ID:</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="751"/>
        <source>Acct.No:</source>
        <translation>счета:</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.ui" line="877"/>
        <source>OK</source>
        <translation>Хорошо</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.cpp" line="68"/>
        <source>Transfer in successed</source>
        <translation>Передача в случае успеха</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.cpp" line="79"/>
        <source>Tansfer out successed</source>
        <translation>Трансфер успешно</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailperwidget.cpp" line="88"/>
        <source>Records of PWR Transfer</source>
        <translation>Записи о передаче PWR</translation>
    </message>
</context>
<context>
    <name>RecordDetailWidget</name>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="14"/>
        <source>RecordDetailWidget</source>
        <translation>RecordDetailWidget</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="97"/>
        <source>Details</source>
        <translation>подробности</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="338"/>
        <source>Amount</source>
        <translation>Количество</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="429"/>
        <source>Cost of Miners</source>
        <translation>плата</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="511"/>
        <source>Receive Address</source>
        <translation>Получить адрес</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="611"/>
        <source>Payment Address</source>
        <translation>Платежный адрес</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="711"/>
        <source>Notice</source>
        <translation>уведомление</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="793"/>
        <source>Deal Number</source>
        <translation>Номер сделки</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="872"/>
        <source>Block</source>
        <translation>блок</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.ui" line="937"/>
        <source>OK</source>
        <translation>Хорошо</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.cpp" line="63"/>
        <source> Transfer in successed</source>
        <translation> Передача в случае успеха</translation>
    </message>
    <message>
        <location filename="childrenWidget/recorddetailwidget.cpp" line="66"/>
        <source> Transfer out successed</source>
        <translation> Трансфер успешно</translation>
    </message>
</context>
<context>
    <name>RecordsWidget</name>
    <message>
        <location filename="childrenWidget/recordswidget.ui" line="14"/>
        <source>RecordsWidget</source>
        <translation>RecordsWidget</translation>
    </message>
    <message>
        <location filename="childrenWidget/recordswidget.ui" line="146"/>
        <source>All</source>
        <translation>Все</translation>
    </message>
    <message>
        <location filename="childrenWidget/recordswidget.ui" line="180"/>
        <location filename="childrenWidget/recordswidget.ui" line="334"/>
        <source>Transfer Out</source>
        <translation>Трансфер из</translation>
    </message>
    <message>
        <location filename="childrenWidget/recordswidget.ui" line="211"/>
        <location filename="childrenWidget/recordswidget.ui" line="307"/>
        <source>Transfer In</source>
        <translation>Трансфер в</translation>
    </message>
    <message>
        <location filename="childrenWidget/recordswidget.ui" line="242"/>
        <source>failure</source>
        <translation>отказ</translation>
    </message>
</context>
<context>
    <name>SafeWidget</name>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.ui" line="14"/>
        <source>SafeWidget</source>
        <translation>SafeWidget</translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.ui" line="164"/>
        <source>Backup private key</source>
        <translation>Резервный ключ</translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.ui" line="295"/>
        <source>Please enter your base password</source>
        <translation>Введите свой базовый пароль</translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.ui" line="391"/>
        <source>OK</source>
        <translation>Хорошо</translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.ui" line="451"/>
        <source>Back up private key</source>
        <translation>Резервный ключ</translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.ui" line="484"/>
        <source>Security warning:The private key is not encrypted,the export is risky,It is recommended to use mnemonic for backup</source>
        <translation>Предупреждение о безопасности: закрытый ключ не зашифрован, экспорт рискован</translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.ui" line="570"/>
        <source>Copy</source>
        <translation>копия</translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.cpp" line="68"/>
        <source>Empty Password</source>
        <translation>Пустой пароль</translation>
    </message>
    <message>
        <location filename="pwrWidget/safeWidget/safewidget.cpp" line="75"/>
        <source>Incorrect Password</source>
        <translation>Неверный пароль</translation>
    </message>
</context>
<context>
    <name>StarFlashWidget</name>
    <message>
        <location filename="starflashwidget.ui" line="32"/>
        <source>StarFlashWidget</source>
        <translation>StarFlashWidget</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="79"/>
        <source>164953236</source>
        <translation>164953236</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="101"/>
        <source>Refresh</source>
        <translation>обновление</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="158"/>
        <source>ID 190024</source>
        <oldsource>Personal ID 190024</oldsource>
        <translation>Персональный ID 190024</translation>
    </message>
    <message>
        <source>Base ID </source>
        <translation type="vanished">База ID </translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="184"/>
        <source>Acct.No</source>
        <translation>счета</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="215"/>
        <source>Copy</source>
        <translation>копия</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="248"/>
        <source>Copy Successed</source>
        <translation>преуспевать</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="299"/>
        <location filename="starflashwidget.ui" line="340"/>
        <location filename="starflashwidget.ui" line="381"/>
        <location filename="starflashwidget.ui" line="422"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="320"/>
        <source>Friend(s)</source>
        <translation>друг</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="361"/>
        <source>Group(s)</source>
        <oldsource>Tribe(s)</oldsource>
        <translation>группа</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="402"/>
        <source>Invitation(s)</source>
        <translation>Приглашение (s)</translation>
    </message>
    <message>
        <location filename="starflashwidget.ui" line="443"/>
        <source>Application(s)</source>
        <translation>Приложения)</translation>
    </message>
    <message>
        <source>Personal ID </source>
        <translation type="vanished">личный ID </translation>
    </message>
    <message>
        <location filename="starflashwidget.cpp" line="83"/>
        <location filename="starflashwidget.cpp" line="92"/>
        <source>ID </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TransferManager</name>
    <message>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="68"/>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="165"/>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="218"/>
        <source>Deal Failed</source>
        <translation>Сделка не удалась</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="68"/>
        <source>Error occured when getting Nonce</source>
        <translation>Произошла ошибка при получении Nonce</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="165"/>
        <source>Invalid private key</source>
        <translation>Неверный закрытый ключ</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="225"/>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="228"/>
        <source>Notice</source>
        <translation>уведомление</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="225"/>
        <source>Top up successed，Please check your balance in your hosting account later</source>
        <translation>Пополнение успешно, пожалуйста, проверьте свой баланс в вашей учетной записи хостинга позже</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transfermanager.cpp" line="228"/>
        <source>Top up failed</source>
        <translation>Пополнить не удалось</translation>
    </message>
</context>
<context>
    <name>TransferWidget</name>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="20"/>
        <source>TransferWidget</source>
        <translation>TransferWidget</translation>
    </message>
    <message>
        <source>Receiver&apos;s interPlanet ID</source>
        <translation type="vanished">InterPlanet ID получателя</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="138"/>
        <source>Receiver&apos;s ID</source>
        <oldsource>Receiver&apos;s Telecomm ID</oldsource>
        <translation>ID получателя</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="160"/>
        <source>Receiver&apos;s Acct.No</source>
        <oldsource>Receiver&apos;s base ID</oldsource>
        <translation>Счет получателя</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="182"/>
        <source>Amount</source>
        <translation>Количество</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="220"/>
        <source>Enter account number</source>
        <translation>Введите номер счета</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="251"/>
        <source>Enter wallet address</source>
        <translation>Введите адрес кошелька</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="276"/>
        <source>Enter the amount of power</source>
        <translation>Введите количество энергии</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="320"/>
        <source>Sender&apos;s Acct.No</source>
        <oldsource>Sender&apos;s base ID</oldsource>
        <translation>Счет отправителя</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="396"/>
        <source>Cost of Miners</source>
        <translation>Стоимость майнеров</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="412"/>
        <source>0.0004 PWR</source>
        <translation>0.0004 PWR</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="438"/>
        <source>0.0004</source>
        <translation>0.0004</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="476"/>
        <source>0.006</source>
        <translation>0.006</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="502"/>
        <source>Power Balance</source>
        <translation>Баланс сил</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="531"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.ui" line="588"/>
        <source>Power Transfer</source>
        <translation>Передача энергии</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.cpp" line="100"/>
        <location filename="pwrWidget/transferWidget/transferwidget.cpp" line="103"/>
        <source>Notice</source>
        <translation>уведомление</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.cpp" line="100"/>
        <source>Input cannot be empty!</source>
        <translation>Ввод не может быть пустым!</translation>
    </message>
    <message>
        <location filename="pwrWidget/transferWidget/transferwidget.cpp" line="103"/>
        <source>Please enter valid number!</source>
        <translation>Пожалуйста, введите правильный номер!</translation>
    </message>
</context>
<context>
    <name>TurnInWidget</name>
    <message>
        <location filename="childrenWidget/turninwidget.ui" line="14"/>
        <source>TurnInWidget</source>
        <translation>TurnInWidget</translation>
    </message>
    <message>
        <location filename="childrenWidget/turninwidget.ui" line="314"/>
        <source>Replace
assets</source>
        <translation>Заменить
активы</translation>
    </message>
    <message>
        <location filename="childrenWidget/turninwidget.ui" line="350"/>
        <source>Specify
the amount</source>
        <translation>Уточнить
количество</translation>
    </message>
    <message>
        <location filename="childrenWidget/turninwidget.cpp" line="56"/>
        <location filename="childrenWidget/turninwidget.cpp" line="153"/>
        <source>Please tranfer into </source>
        <translation>Пожалуйста, перенесите в </translation>
    </message>
    <message>
        <location filename="childrenWidget/turninwidget.cpp" line="87"/>
        <source>Please enter the amount:</source>
        <translation>Пожалуйста, введите сумму:</translation>
    </message>
    <message>
        <location filename="childrenWidget/turninwidget.cpp" line="96"/>
        <source>Notice</source>
        <translation>уведомление</translation>
    </message>
    <message>
        <location filename="childrenWidget/turninwidget.cpp" line="96"/>
        <source>the amount cannot be 0</source>
        <translation>сумма не может быть 0</translation>
    </message>
</context>
<context>
    <name>TurnOutWidget</name>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="14"/>
        <source>TurnOutWidget</source>
        <translation>TurnOutWidget</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="115"/>
        <location filename="childrenWidget/turnoutwidget.cpp" line="68"/>
        <source>BTC Balance</source>
        <translation>Баланс BTC</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="144"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="176"/>
        <source>Amount</source>
        <translation>Количество</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="201"/>
        <source>Enter account number</source>
        <translation>Введите номер счета</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="230"/>
        <source>Notice(optional)</source>
        <translation>Примечание </translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="255"/>
        <source>Enter Notice</source>
        <translation>Введите уведомление</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="284"/>
        <source>Receive Address</source>
        <translation>Получить адрес</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="346"/>
        <source>Enter  the receiver&apos;s account</source>
        <translation>Введите счет получателя</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="394"/>
        <source>Payment Address</source>
        <translation>Платежный адрес</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="486"/>
        <source>Cost of Miners</source>
        <translation>Стоимость майнеров</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="502"/>
        <source>0.0004 PWR</source>
        <translation>0.0004 PWR</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="528"/>
        <source>0.0004</source>
        <translation>0.0004</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="566"/>
        <source>0.006</source>
        <translation>0.006</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.ui" line="623"/>
        <source>OK</source>
        <translation>Хорошо</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.cpp" line="70"/>
        <source>EOS Balance</source>
        <translation>EOS Баланс</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.cpp" line="104"/>
        <location filename="childrenWidget/turnoutwidget.cpp" line="108"/>
        <location filename="childrenWidget/turnoutwidget.cpp" line="146"/>
        <source>Notice</source>
        <translation>уведомление</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.cpp" line="104"/>
        <source>Incomplete input!</source>
        <translation>Неполный ввод!</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.cpp" line="108"/>
        <source>Please enter valid number!</source>
        <translation>Пожалуйста, введите правильный номер!</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.cpp" line="114"/>
        <source>Please enter login password:</source>
        <translation>Пожалуйста, введите пароль для входа:</translation>
    </message>
    <message>
        <location filename="childrenWidget/turnoutwidget.cpp" line="146"/>
        <source>Incorrect password!</source>
        <translation>Неверный пароль!</translation>
    </message>
</context>
<context>
    <name>WalletInfoWidget</name>
    <message>
        <location filename="childrenWidget/walletinfowidget.ui" line="14"/>
        <source>WalletInfoWidget</source>
        <translation>WalletInfoWidget</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.ui" line="61"/>
        <source>Set wallet&apos;s avatar and name</source>
        <translation>аватар и имя кошелька</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.ui" line="131"/>
        <location filename="childrenWidget/walletinfowidget.ui" line="161"/>
        <source>set avatar</source>
        <translation>установить аватар</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.ui" line="167"/>
        <source>Click here to set avatar</source>
        <translation>установить аватар</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.ui" line="195"/>
        <source>Wallet name</source>
        <translation>Имя кошелька</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.ui" line="222"/>
        <source>Next</source>
        <translation>следующий</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.cpp" line="55"/>
        <source>Notice</source>
        <translation>уведомление</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.cpp" line="55"/>
        <source>The name cannot be empty</source>
        <translation>Имя не может быть пустым</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.cpp" line="81"/>
        <source>Open Image</source>
        <translation>Открыть изображение</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletinfowidget.cpp" line="81"/>
        <source>Image File(*.bmp;*.jpeg;*.jpg;*.png)</source>
        <translation>Файл изображения (*. Bmp; *. Jpeg; *. Jpg; *. Png)</translation>
    </message>
</context>
<context>
    <name>WalletManager</name>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="14"/>
        <source>WalletManager</source>
        <translation>WalletManager</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="49"/>
        <source>Manage</source>
        <translation>управлять</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="175"/>
        <source>Change avatar</source>
        <translation>Сменить аватар</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="212"/>
        <source>Change Avatar</source>
        <translation>Сменить аватар</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="284"/>
        <location filename="childrenWidget/walletmanager.ui" line="300"/>
        <source>Change Wallet&apos;s Name</source>
        <translation>Изменить имя кошелька</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="339"/>
        <source>Delete Wallet</source>
        <translation>Удалить кошелек</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="355"/>
        <source>Back Up Private Key</source>
        <translation>Резервный закрытый ключ</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="401"/>
        <source>Backup Private Key</source>
        <translation>Резервный закрытый ключ</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.ui" line="442"/>
        <location filename="childrenWidget/walletmanager.ui" line="488"/>
        <source>Switch Account</source>
        <translation>Сменить аккаунт</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.cpp" line="54"/>
        <source>Open Image</source>
        <translation>Открыть изображение</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.cpp" line="54"/>
        <source>Image File(*.bmp;*.jpeg;*.jpg;*.png)</source>
        <translation>Файл изображения (*. Bmp; *. Jpeg; *. Jpg; *. Png)</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.cpp" line="95"/>
        <source>Please enter your login password:</source>
        <translation>Пожалуйста, введите пароль для входа:</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.cpp" line="265"/>
        <location filename="childrenWidget/walletmanager.cpp" line="293"/>
        <source>Notice</source>
        <translation>уведомление</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.cpp" line="265"/>
        <source>Incorrect password!</source>
        <translation>Неверный пароль!</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletmanager.cpp" line="293"/>
        <source>Main wallet cannot be deleted!</source>
        <translation>Основной кошелек не может быть удален!</translation>
    </message>
</context>
<context>
    <name>WalletPriKeyWidget</name>
    <message>
        <location filename="childrenWidget/walletprikeywidget.ui" line="14"/>
        <source>WalletPriKeyWidget</source>
        <translation>WalletPriKeyWidget</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletprikeywidget.ui" line="61"/>
        <source>Backup Private Key</source>
        <translation>Резервный закрытый ключ</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletprikeywidget.ui" line="97"/>
        <source>Copy</source>
        <translation>копия</translation>
    </message>
    <message>
        <location filename="childrenWidget/walletprikeywidget.ui" line="119"/>
        <source>Safety warning: It&apos;s risky to export with private key which is not encrypted,Back up with the mnemonic word is recommended.</source>
        <translation>Экспортировать закрытый ключ рискованно, поэтому рекомендуется использовать мнемоническую резервную копию.</translation>
    </message>
</context>
<context>
    <name>hostingRecordItemWidget</name>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.ui" line="14"/>
        <source>hostingRecordItemWidget</source>
        <translation>hostingRecordItemWidget</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.ui" line="89"/>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.cpp" line="31"/>
        <source>Get Red Packet</source>
        <translation>Get Red Packet</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.ui" line="111"/>
        <source>2018-09-30 18:09:11</source>
        <translation>2018-09-30 18:09:11</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.ui" line="137"/>
        <source>11.00000</source>
        <translation>11.00000</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.ui" line="156"/>
        <source>Confirmed</source>
        <translation>Confirmed</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.cpp" line="35"/>
        <source>Top Up</source>
        <translation>Пополнить</translation>
    </message>
    <message>
        <location filename="pwrWidget/hostingAccountWidget/hostingrecorditemwidget.cpp" line="39"/>
        <source>Withdraw</source>
        <translation>Изымать</translation>
    </message>
</context>
</TS>
