﻿#include "recorddetailperwidget.h"
#include "ui_recorddetailperwidget.h"
#include <QFile>
#include <QEvent>
#include "QStringLiteralBak.h"
#include <QMouseEvent>


RecordDetailPerWidget::RecordDetailPerWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::RecordDetailPerWidget();
	ui->setupUi(this);

	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog | Qt::Tool);
	//setWindowModality(Qt::WindowModal);
	setAttribute(Qt::WA_DeleteOnClose);
	setAttribute(Qt::WA_TranslucentBackground);

// 	QFile file(":/qssWidget/Resources/qssWidget/recorddetailper.qss");
// 	file.open(QFile::ReadOnly);
// 	QString styleSheet = QLatin1String(file.readAll());
// 	setStyleSheet(styleSheet);
// 	file.close();

	QFile file(":/QSS/Resources/QSS/eWalletLib/recorddetailperwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->closeBtn, SIGNAL(clicked(bool)), this, SLOT(close()));
	connect(ui->enterBtn, SIGNAL(clicked(bool)), this, SLOT(close()));
}

RecordDetailPerWidget::~RecordDetailPerWidget()
{
	delete ui;
}

//鼠标事件的处理。
void RecordDetailPerWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void RecordDetailPerWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void RecordDetailPerWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

void RecordDetailPerWidget::setRecord(RecordInfo info)
{
	if (info.status == "receiver")
	{
		ui->stateLabel->setText(tr("Transfer in successed"));
		ui->otherIdlabel_value->setText(info.sendId);
		ui->myIdLabel_value->setText(info.receiveId);
		ui->myAddressLabel_value->setPlainText(info.to);
		ui->otherAddressLabel_value->setPlainText(info.from);
		ui->PicLabel->setStyleSheet("border-image: url(:/ewallet/Resources/ewallet/pwrIn.png);");
		ui->valueLabel->setText("+"+info.value);
		ui->valueLabel->setStyleSheet("color:rgb(255,202,127)");
	}
	else
	{
		ui->stateLabel->setText(tr("Tansfer out successed"));
		ui->otherIdlabel_value->setText(info.receiveId);
		ui->myIdLabel_value->setText(info.sendId);
		ui->myAddressLabel_value->setPlainText(info.from);
		ui->otherAddressLabel_value->setPlainText(info.to);
		ui->PicLabel->setStyleSheet("border-image: url(:/ewallet/Resources/ewallet/pwrOut.png);");
		ui->valueLabel->setText("-" + info.value);
		ui->valueLabel->setStyleSheet("color:#188ce1");
	}
	ui->titleLabel->setText(tr("Records of PWR Transfer"));
	ui->timeLabel_value->setText(info.age);
	ui->transIDLabel_value->setPlainText(info.txHash);
}
