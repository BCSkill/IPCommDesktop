﻿#include "turninwidget.h"
#include "ui_turninwidget.h"
#include "QStringLiteralBak.h"
#include "globalmanager.h"
#ifndef Q_OS_WIN
#include "inline_mac.h"
#endif

TurnInWidget::TurnInWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::TurnInWidget();
	ui->setupUi(this);
	ui->iconLabel->setShape(roundRect);

	QFile file(":/QSS/Resources/QSS/eWalletLib/turninwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->amountBtn, SIGNAL(clicked()), this, SLOT(slotChangeAmount()));
	connect(ui->changeBtn, SIGNAL(clicked()), this, SLOT(slotOpenTokenWidget()));

	amount = 0;
	ui->changeBtn->hide();

}

TurnInWidget::~TurnInWidget()
{
	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

void TurnInWidget::setTurnInData(ChildrenWallet wallet)
{
	walletInfo = wallet;

	//测试代码。
	if (wallet.blockChainName == "EOS")
		wallet.blockChainName = "EOS";

	if (wallet.blockChainName == "ETH")
	{
		ui->changeBtn->show();
		OPRequestShareLib *request = new OPRequestShareLib;
		connect(request, SIGNAL(sigAllToken(QList<TokenInfo>)), this, SLOT(slotTokenList(QList<TokenInfo>)));
		connect(request, SIGNAL(sigAllToken(QList<TokenInfo>)), request, SLOT(deleteLater()));
		request->getAllToken(ChainID_ETH, wallet.accountAddress);
	}

	ui->tipLabel->setText(tr("Please tranfer into ") + wallet.blockChainName);
	ui->walletLabel->setText(wallet.accountName);
	ui->addressLabel->setText(wallet.accountAddress);

#ifdef Q_OS_WIN
        QString imagePath = gSettingsManager->getUserPath() + "/resource/" + wallet.id.remove("-") + ".png";
#else
        QString imagePath = gSettingsManager->getUserPath() + "/resource/" + wallet.id.remove("-") + ".png";
#endif
	QByteArray bytearray;
	QFile file(imagePath);
	if (file.open(QIODevice::ReadOnly))
	{
		bytearray = file.readAll();
		file.close();
	}
	QPixmap pix;
	pix.loadFromData(bytearray);

	if (pix.isNull())
		pix.load(":/ewallet/Resources/ewallet/defaultIcon.png");

	ui->iconLabel->setPixmap(pix);

	setImage();
}

void TurnInWidget::slotChangeAmount()
{
	InputBox *box = new InputBox();
	connect(box, SIGNAL(sigEnter(QString)), this, SLOT(slotSetAmount(QString)));
	box->init(tr("Please enter the amount:"), true);
	box->move(box->x(), this->height() / 3);
}

void TurnInWidget::slotSetAmount(QString string)
{
	amount = string.toDouble();

	if (amount == 0)
		IMessageBox::tip(this, tr("Notice"), tr("the amount cannot be 0"));
	else
		setImage();
}

void TurnInWidget::setImage()
{
	QVariantMap map;
	map.insert("tokenUnit", walletInfo.blockChainName);

	if (walletInfo.blockChainName == "BTC")
		map.insert("chainId", ChainID_BTC);
	if (walletInfo.blockChainName == "ETH")
		map.insert("chainId", ChainID_ETH);

	//测试代码。
	if (walletInfo.blockChainName == "EOS")
		map.insert("chainId", ChainID_EOS);

	map.insert("amount", QString::number(amount));
	map.insert("tokenId", tokenID);
	map.insert("isTokenAccount", false);
	map.insert("contractAddress", contractAddress);
	map.insert("type", "tokenTrade");
	map.insert("walletAddress", ui->addressLabel->text());

	QByteArray array = QJsonDocument::fromVariant(map).toJson();
	QString string = QString::fromUtf8(array);

	QRenCodeShareLib qr;
	QImage image = qr.GenerateQRcode(string);
	QPixmap pixmap = QPixmap::fromImage(image);

	ui->codeLabel->setPixmap(pixmap);
}

void TurnInWidget::slotTokenList(QList<TokenInfo> list)
{
	this->tokenList = list;
}

void TurnInWidget::slotOpenTokenWidget()
{
	ETHTokenListWidget *tokenWidget = new ETHTokenListWidget(this);
	connect(tokenWidget, SIGNAL(sigSelectToken(QString, QString, QString)),
		this, SLOT(slotSelectToken(QString, QString, QString)));
	tokenWidget->show();
	tokenWidget->slotSetToken(tokenList);
}

void TurnInWidget::slotSelectToken(QString name, QString address, QString tokenID)
{
	walletInfo.blockChainName = name;
	walletInfo.accountAddress = address;
	this->contractAddress = address;
	this->tokenID = tokenID;

	ui->tipLabel->setText(tr("Please tranfer into ") + walletInfo.blockChainName);

	setImage();
}

