﻿#include "walletprikeywidget.h"
#include "ui_walletprikeywidget.h"

WalletPriKeyWidget::WalletPriKeyWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::WalletPriKeyWidget();
	ui->setupUi(this);

	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog | Qt::Tool);
	setWindowModality(Qt::WindowModal);
	setAttribute(Qt::WA_DeleteOnClose);
	setAttribute(Qt::WA_TranslucentBackground);

	QFile file(":/QSS/Resources/QSS/eWalletLib/walletprikeywidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->closeBtn, SIGNAL(clicked()), this, SLOT(close()));
	connect(ui->enterBtn, SIGNAL(clicked()), this, SLOT(slotCopy()));
}

WalletPriKeyWidget::~WalletPriKeyWidget()
{
	delete ui;
}

//鼠标事件的处理。
void WalletPriKeyWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void WalletPriKeyWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void WalletPriKeyWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

void WalletPriKeyWidget::setPriKey(QString key)
{
	ui->textEdit->setPlainText(key);
}

void WalletPriKeyWidget::slotCopy()
{
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(ui->textEdit->toPlainText());
}
