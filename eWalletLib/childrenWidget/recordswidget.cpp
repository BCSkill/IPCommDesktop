﻿#include "recordswidget.h"
#include "ui_recordswidget.h"
#include <qfile.h>

#ifdef Q_OS_MAC
#include "mycommonstyle.h"
#endif

RecordsWidget::RecordsWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::RecordsWidget();
	ui->setupUi(this);

	QFile file(":/QSS/Resources/QSS/eWalletLib/recordswidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

#ifdef Q_OS_MAC
    ui->recordsList->setStyle(new MyProxyStyle);
#endif
	connect(ui->turnInBtn, SIGNAL(clicked()), this, SIGNAL(sigTurnIn()));
	connect(ui->turnOutBtn, SIGNAL(clicked()), this, SIGNAL(sigTurnOut()));

	connect(ui->allTab, SIGNAL(clicked()), this, SLOT(slotAllTab()));
	connect(ui->turnInTab, SIGNAL(clicked()), this, SLOT(slotTurnInTab()));
	connect(ui->turnOutTab, SIGNAL(clicked()), this, SLOT(slotTurnOutTab()));
	connect(ui->failTab, SIGNAL(clicked()), this, SLOT(slotFailTab()));
	connect(ui->recordsList, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(slotDBClickRecord(QListWidgetItem*)));
	ui->failTab->hide();
}

RecordsWidget::~RecordsWidget()
{
	delete ui;
}

void RecordsWidget::setTitle(QString name)
{
	ui->tokenLabel->setText(name);
	ui->recordsList->clear();
}

void RecordsWidget::slotRecords(QList<RecordInfo> recordsList)
{
	this->recordsList = recordsList;
	
	slotAllTab();
	ui->allTab->setChecked(true);
}

void RecordsWidget::slotAllTab()
{
	ui->recordsList->clear();

    foreach (RecordInfo recordInfo , recordsList)
	{
		EWalletItemRecord *record = new EWalletItemRecord;
		if (recordInfo.status == "receiver")
		{
			record->setData(recordInfo.from, recordInfo.age, recordInfo.value, true);
		}
		else
		{
			record->setData(recordInfo.to, recordInfo.age, recordInfo.value, false);
		}
		record->setObjectName(recordInfo.txHash);

		QListWidgetItem *recordItem = new QListWidgetItem;
		recordItem->setSizeHint(QSize(310, 50));
		ui->recordsList->addItem(recordItem);
		ui->recordsList->setItemWidget(recordItem, record);
	}
}

void RecordsWidget::slotTurnOutTab()
{
	ui->recordsList->clear();

    foreach (RecordInfo recordInfo , recordsList)
	{
		if (recordInfo.status == "sender")
		{
			EWalletItemRecord *record = new EWalletItemRecord;
			record->setData(recordInfo.to, recordInfo.age, recordInfo.value, false);
			record->setObjectName(recordInfo.txHash);

			QListWidgetItem *recordItem = new QListWidgetItem;
			recordItem->setSizeHint(QSize(310, 50));
			ui->recordsList->addItem(recordItem);
			ui->recordsList->setItemWidget(recordItem, record);
		}
	}
}

void RecordsWidget::slotTurnInTab()
{
	ui->recordsList->clear();

    foreach (RecordInfo recordInfo , recordsList)
	{
		if (recordInfo.status == "receiver")
		{
			EWalletItemRecord *record = new EWalletItemRecord;
			record->setData(recordInfo.from, recordInfo.age, recordInfo.value, true);
			record->setObjectName(recordInfo.txHash);

			QListWidgetItem *recordItem = new QListWidgetItem;
			recordItem->setSizeHint(QSize(310, 50));
			ui->recordsList->addItem(recordItem);
			ui->recordsList->setItemWidget(recordItem, record);
		}
	}
}

void RecordsWidget::slotFailTab()
{
	ui->recordsList->clear();
}

void RecordsWidget::slotDBClickRecord(QListWidgetItem *item)
{
	if (item)
	{
		EWalletItemRecord *recordWidget = (EWalletItemRecord *)ui->recordsList->itemWidget(item);
		if (recordWidget)
		{
			QString hex = recordWidget->objectName();

            foreach (RecordInfo info , recordsList)
			{
				if (info.txHash == hex)
				{
					RecordDetailWidget *detailWidget = new RecordDetailWidget(this);
					detailWidget->setRecord(info);
					detailWidget->show();
				}
			}
		}
	}
}

