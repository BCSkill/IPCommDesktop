﻿#include "recorddetailwidget.h"
#include "QStringLiteralBak.h"
#include "ui_recorddetailwidget.h"
#ifdef Q_OS_MAC
#include "mycommonstyle.h"
#endif
RecordDetailWidget::RecordDetailWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::RecordDetailWidget();
	ui->setupUi(this);

#ifdef Q_OS_MAC
    ui->turnInEdit->setStyle(new MyProxyStyle);
#endif

	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog | Qt::Tool);
	//setWindowModality(Qt::WindowModal);
	setAttribute(Qt::WA_DeleteOnClose);
	setAttribute(Qt::WA_TranslucentBackground);

	QFile file(":/QSS/Resources/QSS/eWalletLib/recorddetailwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->closeBtn, SIGNAL(clicked(bool)), this, SLOT(close()));
	connect(ui->enterBtn, SIGNAL(clicked(bool)), this, SLOT(close()));
}

RecordDetailWidget::~RecordDetailWidget()
{
	delete ui;
}

//鼠标事件的处理。
void RecordDetailWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void RecordDetailWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void RecordDetailWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

void RecordDetailWidget::setRecord(RecordInfo info)
{
	if (info.status == "receiver")
		ui->titleBtn->setText(tr(" Transfer in successed"));
	else
	{
		ui->titleBtn->setText(tr(" Transfer out successed"));
	}

	ui->timeLabel->setText(info.age);
	ui->valueLabel->setText(info.value);
	ui->mineLabel->setText(info.txFee);
	ui->turnInEdit->setPlainText(info.to);
	ui->turnOutEdit->setPlainText(info.from);
	QString strID = info.txHash.left(6) + "****" + info.txHash.right(4);
	ui->IDLabel->setText(strID);

	if (info.block != 0)
	    ui->blockLabel->setText(QString::number(info.block));
}
