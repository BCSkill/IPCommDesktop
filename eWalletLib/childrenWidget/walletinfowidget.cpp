﻿#include "walletinfowidget.h"
#include "ui_walletinfowidget.h"
#ifdef Q_OS_MAC
#include "mycommonstyle.h"
#endif
#include "QStringLiteralBak.h"

#ifdef Q_OS_LINUX
    #include <qdesktopwidget.h>
#endif

WalletInfoWidget::WalletInfoWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::WalletInfoWidget();
	ui->setupUi(this);

	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog | Qt::Tool);
	setWindowModality(Qt::WindowModal);
	setAttribute(Qt::WA_DeleteOnClose);
	setAttribute(Qt::WA_TranslucentBackground);

	QFile file(":/QSS/Resources/QSS/eWalletLib/walletinfowidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	ui->iconLabel->installEventFilter(this);
	ui->tipLabel->installEventFilter(this);

	connect(ui->closeBtn, SIGNAL(clicked()), this, SLOT(close()));
	connect(ui->enterBtn, SIGNAL(clicked()), this, SLOT(slotEnter()));

	send = false;

#ifdef Q_OS_MAC
    ui->nameEdit->setStyle(new MyProxyStyle);
#endif

#ifdef Q_OS_LINUX
	setLinuxCenter();
#endif
}

WalletInfoWidget::~WalletInfoWidget()
{
	delete ui;
}

void WalletInfoWidget::slotEnter()
{
	if (ui->nameEdit->text().isEmpty())
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("The name cannot be empty"));
	}
	else
	{
		this->walletName = ui->nameEdit->text();

		if (iconPath.isEmpty())
		{
			emit sigSendWalletInfo(walletName, iconId);
			this->hide();
		}
		else
		{
			if (iconId.isEmpty())
				send = true;
			else
			{
				emit sigSendWalletInfo(walletName, iconId);
				this->hide();
			}
		}
	}
}

void WalletInfoWidget::slotSelectIcon()
{
	QString strPath = QFileDialog::getOpenFileName(this, tr("Open Image"), QDir::homePath(), tr("Image File(*.bmp;*.jpeg;*.jpg;*.png)"));

	if (!strPath.isEmpty())
	{
		iconPath = strPath;
		ui->iconLabel->setPixmap(QPixmap(strPath));
		//上传密件。
		HttpNetWork::HttpUpLoadFile *upload = new HttpNetWork::HttpUpLoadFile;
		connect(upload, SIGNAL(sigUpLoadFinished(bool, QByteArray)), this, SLOT(slotUploadImage(bool, QByteArray)));
		QVariantMap pargram;
		pargram.insert("parentId", "66662");
		pargram.insert("createUser", "6662");
		upload->StartUpLoadFile(gDataManager->getAppConfigInfo().PanServerUploadURL, strPath, pargram);
	}
}

void WalletInfoWidget::slotUploadImage(bool success, QByteArray result)
{
	if (success)
	{
		QVariantMap map = QJsonDocument::fromJson(result).toVariant().toMap();
		iconId = map["fileid"].toString();

		if (send)
		{
			emit sigSendWalletInfo(walletName, iconId);
			this->hide();
		}
	}
}

bool WalletInfoWidget::eventFilter(QObject *obj, QEvent *e)
{
	if (obj == ui->tipLabel || obj == ui->iconLabel)
	{
		if (e->type() == QEvent::MouseButtonPress)
		{
			slotSelectIcon();
		}
	}

	return QWidget::eventFilter(obj, e);
}

//鼠标事件的处理。
void WalletInfoWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void WalletInfoWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void WalletInfoWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

#ifdef Q_OS_LINUX
void WalletInfoWidget::setLinuxCenter()
{
    window()->setGeometry
    (
            QStyle::alignedRect
            (
                    Qt::LeftToRight,
                    Qt::AlignCenter,
                    window()->size(),
                    qApp->desktop()->availableGeometry()
            )
    );
}
#endif
