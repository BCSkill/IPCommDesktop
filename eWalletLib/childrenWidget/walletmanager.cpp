﻿#include "walletmanager.h"
#include "ui_walletmanager.h"
#ifdef Q_OS_MAC
#include "mycommonstyle.h"
#include "inline_mac.h"
#endif
#include "QStringLiteralBak.h"
#include "globalmanager.h"

extern OPDataManager  *gOPDataManager;

WalletManager::WalletManager(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::WalletManager();
	ui->setupUi(this);
#ifdef Q_OS_MAC
    ui->nameEdit->setStyle(new MyProxyStyle);
#endif

	ui->iconLabel->setShape(roundRect);
	ui->headerLabel->setShape(roundRect);

	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog | Qt::Tool);
	setWindowModality(Qt::WindowModal);
	setAttribute(Qt::WA_DeleteOnClose);
	setAttribute(Qt::WA_TranslucentBackground);

	QFile file(":/QSS/Resources/QSS/eWalletLib/walletmanager.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->closeBtn, SIGNAL(clicked()), this, SLOT(close()));
	connect(ui->nameEdit, SIGNAL(editingFinished()), this, SLOT(slotChangeName()));
	connect(ui->deleteBtn, SIGNAL(clicked()), this, SLOT(slotDeleteWallet()));

	ui->changeHeaderWidget->installEventFilter(this);
	ui->backupWidget->installEventFilter(this);
}

WalletManager::~WalletManager()
{
	delete ui;
}

bool WalletManager::eventFilter(QObject *obj, QEvent *e)
{
	if (obj == ui->changeHeaderWidget)
	{
		if (e->type() == QEvent::MouseButtonPress)
		{
			QString strPath = QFileDialog::getOpenFileName(this, tr("Open Image"), QDir::homePath(), tr("Image File(*.bmp;*.jpeg;*.jpg;*.png)"));
			
			if (!strPath.isEmpty())
			{
				QFile file(strPath);
				QByteArray bytearray;
				if (file.open(QIODevice::ReadOnly))
				{
					bytearray = file.readAll();
					file.close();
				}
				QPixmap pix;
				pix.loadFromData(bytearray);
#ifdef Q_OS_WIN
        QString imagePath = gSettingsManager->getUserPath()+"/resource/" + walletInfo.id.remove("-") + ".png";
#else
        QString imagePath = gSettingsManager->getUserPath() + "/resource/" + walletInfo.id.remove("-") + ".png";
#endif

				QFile sourceFile(imagePath);
				sourceFile.remove();

				ui->iconLabel->setPixmap(pix);
				ui->headerLabel->setPixmap(pix);
				//上传密件。
				HttpNetWork::HttpUpLoadFile *upload = new HttpNetWork::HttpUpLoadFile;
				connect(upload, SIGNAL(sigUpLoadFinished(bool, QByteArray)), this, SLOT(slotUploadImage(bool, QByteArray)));
				QVariantMap pargram;
				pargram.insert("parentId", "66662");
				pargram.insert("createUser", "6662");
				upload->StartUpLoadFile(gDataManager->getAppConfigInfo().PanServerUploadURL, strPath, pargram);
			}
		}
	}

	if (obj == ui->backupWidget)
	{
		if (e->type() == QEvent::MouseButtonPress)
		{
			InputBox *box = new InputBox(this);
			connect(box, SIGNAL(sigEnter(QString)), this, SLOT(slotBackupPriKey(QString)));
			box->init(tr("Please enter your login password:"), false, QLineEdit::Password);
		}
	}

	return QWidget::eventFilter(obj, e);
}

//鼠标事件的处理。
void WalletManager::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void WalletManager::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void WalletManager::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

void WalletManager::setWallet(ChildrenWallet wallet)
{
	walletInfo = wallet;

	ui->nameLabel->setText(wallet.accountName);

	if (wallet.blockChainName == "EOS")
	{
		ui->addressLabel->setText(wallet.accountAddress);
		ui->addressLabel->setObjectName(wallet.eosAccountNames);

		QStringList accountList = wallet.eosAccountNames.split("_");
		disconnect(ui->accountBox, SIGNAL(currentTextChanged(QString)), this, SIGNAL(sigAccountChanged(QString)));
		foreach(QString account, accountList)
		{
			ui->accountBox->addItem(account);
		}
		ui->accountBox->setCurrentText(wallet.accountAddress);
		connect(ui->accountBox, SIGNAL(currentTextChanged(QString)), this, SIGNAL(sigAccountChanged(QString)));
	}
	else
	{
		ui->accountWidget->hide();

		QString address = wallet.accountAddress.left(6) + "******" + wallet.accountAddress.right(4);
		ui->addressLabel->setText(address);
		ui->addressLabel->setObjectName(wallet.accountAddress);
	}

	ui->nameEdit->setText(wallet.accountName);

#ifdef Q_OS_WIN
        QString imagePath = gSettingsManager->getUserPath()+"/resource/" + wallet.id.remove("-") + ".png";
#else
        QString imagePath = gSettingsManager->getUserPath() + "/resource/" + wallet.id.remove("-") + ".png";
#endif
	if (QFile(imagePath).exists())
	{
		QByteArray bytearray;
		QFile file(imagePath);
		if (file.open(QIODevice::ReadOnly))
		{
			bytearray = file.readAll();
			file.close();
		}
		QPixmap pix;
		pix.loadFromData(bytearray);

		if (pix.isNull())
			pix.load(":/ewallet/Resources/ewallet/defaultIcon.png");

		ui->iconLabel->setPixmap(pix);
		ui->headerLabel->setPixmap(pix);
	}
	else
	{
		if (wallet.accountIcon.isEmpty())
		{
			ui->iconLabel->setPixmap(QPixmap(":/ewallet/Resources/ewallet/defaultIcon.png"));
			ui->headerLabel->setPixmap(QPixmap(":/ewallet/Resources/ewallet/defaultIcon.png"));
		}
		else
		{
			HttpNetWork::HttpDownLoadFile *http = new HttpNetWork::HttpDownLoadFile;
			connect(http, SIGNAL(sigDownFinished(bool)), this, SLOT(slotDownloadIcon(bool)));
			http->setData(QVariant(imagePath));
			http->StartDownLoadFile(wallet.accountIcon, imagePath);
		}
	}
}

void WalletManager::slotDownloadIcon(bool success)
{
	if (success)
	{
		HttpNetWork::HttpDownLoadFile *download = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());
		QString imagePath = download->getData().toString();

		QByteArray bytearray;
		QFile file(imagePath);
		if (file.open(QIODevice::ReadOnly))
		{
			bytearray = file.readAll();
			file.close();
		}

		QPixmap pix;
		pix.loadFromData(bytearray);

		if (pix.isNull())
			pix.load(":/ewallet/Resources/ewallet/defaultIcon.png");

		ui->iconLabel->setPixmap(pix);
		ui->headerLabel->setPixmap(pix);
	}
	else
	{
		ui->iconLabel->setPixmap(QPixmap(":/ewallet/Resources/ewallet/defaultIcon.png"));
		ui->headerLabel->setPixmap(QPixmap(":/ewallet/Resources/ewallet/defaultIcon.png"));
	}
}

void WalletManager::slotUploadImage(bool success, QByteArray byte)
{
	if (success)
	{
		QVariantMap map = QJsonDocument::fromJson(byte).toVariant().toMap();
		QString iconID = map["fileid"].toString();

		QString accountIcon = gDataManager->getAppConfigInfo().PanServerDownloadURL + iconID + "/download";
		walletInfo.accountIcon = accountIcon;

		emit sigSetWalletInfo(walletInfo.accountAddress, walletInfo.mainAccountAddress, "", walletInfo.accountIcon, walletInfo.blockChainName);
	}
}

void WalletManager::slotChangeName()
{
	QString accountName = ui->nameEdit->text();
	if (!accountName.isEmpty())
	{
		walletInfo.accountName = accountName;
		ui->nameLabel->setText(accountName);

		emit sigSetWalletInfo(walletInfo.accountAddress, walletInfo.mainAccountAddress, walletInfo.accountName, "", walletInfo.blockChainName);
	}
}

void WalletManager::slotBackupPriKey(QString text)
{
	QString priKey;

	QByteArray array = QCryptographicHash::hash(text.toUtf8(), QCryptographicHash::Sha1);
	QString string = array.toHex();

	QString userID = QString::number(gDataManager->getUserInfo().nUserID);
	QString passWord = gDataManager->getUserInfo().strLoginPWD;

	if (string.toLower() != passWord.toLower())
	{
		IMessageBox::tip(this, tr("Notice"), tr("Incorrect password!"));
		return;
	}
	else
	{
#ifdef Q_OS_WIN
        QString keyPath = gSettingsManager->getUserPath() + "/wallet//" + userID + "//pwr.key";
#else
        QString keyPath = gSettingsManager->getUserPath() + "/wallet//" + userID + "//pwr.key";
#endif
		QString mainKey = gOPDataManager->Decryption(passWord, keyPath);
		if (walletInfo.accountAddress == walletInfo.mainAccountAddress)
			priKey = mainKey;
		else
		{
			walletInfo.accountPrivateKey = walletInfo.accountPrivateKey.replace(" ", "+");
			priKey = gOPDataManager->decryptAES(walletInfo.accountPrivateKey.toUtf8(), mainKey);
		}
		
		WalletPriKeyWidget *keyWidget = new WalletPriKeyWidget;
		keyWidget->setPriKey(priKey);
		keyWidget->show();
	}
}

void WalletManager::slotDeleteWallet()
{
	if (walletInfo.accountAddress == walletInfo.mainAccountAddress)
		IMessageBox::tip(this, tr("Notice"), tr("Main wallet cannot be deleted!"));
	else
	{
		emit sigDeleteWallet(walletInfo.accountAddress, walletInfo.mainAccountAddress, walletInfo.blockChainName);
		this->close();
	}
}
