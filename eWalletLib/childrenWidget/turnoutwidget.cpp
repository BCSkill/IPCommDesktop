﻿#include "turnoutwidget.h"
#include "ui_turnoutwidget.h"
#ifdef Q_OS_MAC
#include "mycommonstyle.h"
#endif
#include "QStringLiteralBak.h"
#include "globalmanager.h"
#include "httpnetworksharelib.h"
#include "combobox/mycombobox.h"
#include "combobox/myBoxItem.h"

TurnOutWidget::TurnOutWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::TurnOutWidget();
	ui->setupUi(this);

	m_bMove = false;
	setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog | Qt::Tool);
	setWindowModality(Qt::WindowModal);
	setAttribute(Qt::WA_TranslucentBackground);
	ui->mBtnClose->hide();
	ui->minerWidget->hide();
	ui->recvBox->hide();

	QFile file(":/QSS/Resources/QSS/eWalletLib/turnoutwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->transBtn, SIGNAL(clicked()), this, SLOT(slotClickTransBtn()));
	connect(ui->recvBox, SIGNAL(activated(int)), this, SLOT(slotonIndexChanged(int)));
	connect(ui->recvBox, SIGNAL(currentIndexChanged(int)), this, SLOT(slotonIndexChanged(int)));

#ifdef Q_OS_MAC
    ui->recvEdit->setStyle(new MyProxyStyle);
#endif

#ifndef Q_OS_WIN
    QFile style(":/QSS/Resources/QSS/qslider_horizontal.qss");
    style.open(QFile::ReadOnly);
    QString sheet = QLatin1String(style.readAll());
    ui->minerSlider->setStyleSheet(sheet);
    style.close();
#else
    ui->minerSlider->setStyleSheet("background-color: rgba(0,0,0, 0);");
#endif
}

TurnOutWidget::~TurnOutWidget()
{
	delete ui;
}

void TurnOutWidget::show()
{
	QWidget::show();
	this->raise();
	this->activateWindow();
}

void TurnOutWidget::setTurnOutData(ChildrenWallet wallet)
{
	this->wallet = wallet;

	if (wallet.blockChainName == "BTC")
		ui->chainNameLabel->setText(tr("BTC Balance"));
	if (wallet.blockChainName == "EOS")
		ui->chainNameLabel->setText(tr("EOS Balance"));

	ui->amountLabel->setText(wallet.balance);
	ui->walletLabel->setText(wallet.accountName);
	ui->addressLabel->setText(wallet.accountAddress);
}

void TurnOutWidget::setBalance(QString balance)
{
	ui->amountLabel->setText(balance);
}

void TurnOutWidget::setPassWord(QString word)
{
	this->passWord = word;
}

void TurnOutWidget::slotClickTransBtn()
{
	QString value = ui->valueEdit->text();
	QString address;
	if (m_bMove)
	{
		address = ui->recvBox->currentText();
	}
	else
	{
		address = ui->recvEdit->text();
	}
	address = address.trimmed();
	
	double dValue = value.toDouble();
	if (value.isEmpty() || address.isEmpty())
	{
		IMessageBox::tip(this, tr("Notice"), tr("Incomplete input!"));
	}
	else if (dValue <= 0)
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("Please enter valid number!"));
	}
	else
	{
		InputBox *box = new InputBox(this);
		connect(box, SIGNAL(sigEnter(QString)), this, SLOT(slotEnterWord(QString)));
		box->init(tr("Please enter login password:"), false, QLineEdit::Password);
	}
}

void TurnOutWidget::slotEnterWord(QString word)
{
	QByteArray array = QCryptographicHash::hash(word.toUtf8(), QCryptographicHash::Sha1);
	QString string = array.toHex();

	if (string.toLower() == passWord.toLower())
	{
		QString value = ui->valueEdit->text();
		QString address;
		if (m_bMove)
		{
			address = ui->recvBox->currentText();
		}
		else
		{
			address = ui->recvEdit->text();
		}
		address = address.trimmed();
		if (wallet.blockChainName == "BTC")
		    emit sigTurnOut(address, value);
		if (wallet.blockChainName == "EOS")
			emit sigTurnOut(ui->addressLabel->text(), address, QString("%1 EOS").arg(value));

		ui->valueEdit->clear();
		ui->recvEdit->clear();
	}
	else
	{
		IMessageBox::tip(this, tr("Notice"), tr("Incorrect password!"));
	}
}

//鼠标事件的处理。
void TurnOutWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void TurnOutWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void TurnOutWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0 || m_bMove == false)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

void TurnOutWidget::InitAsWindow()
{
	QFile file(":/QSS/Resources/QSS/eWalletLib/turnoutwidget_dialog.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	ui->midWidget->setStyleSheet(styleSheet);
	file.close();

	ui->mBtnClose->show();
	ui->recvBox->show();
	ui->recvEdit->hide();
	m_bMove = true;
	connect(ui->mBtnClose, SIGNAL(clicked()), this, SLOT(close()));
}

void TurnOutWidget::setBuddyAddress(QList<ChildrenWallet> BTCList)
{
	QString strStyle = "";
	for (int i = 0; i < BTCList.size(); i++)
	{
		QString imagePath = gSettingsManager->getUserPath() + "/resource/" + BTCList[i].id.remove("-") + ".png";
		QString strValue = BTCList[i].accountName;
		QString strTitle;
		QString strName;
		if (!strValue.isEmpty())
		{
			strValue += ": ";
		}
		strValue += BTCList[i].accountAddress;
		strTitle = BTCList[i].accountName;
		strName = BTCList[i].accountAddress;
		QPixmap pix;
		if (QFile(imagePath).exists())
		{
			QByteArray bytearray;
			QFile file(imagePath);
			if (file.open(QIODevice::ReadOnly))
			{
				bytearray = file.readAll();
				file.close();
			}
			
			pix.loadFromData(bytearray);

			if (pix.isNull())
				pix.load(":/ewallet/Resources/ewallet/defaultIcon.png");

			//ui->iconLabel->setPixmap(pix);
		}
		else
		{
			if (wallet.accountIcon.isEmpty())
				pix.load(":/ewallet/Resources/ewallet/defaultIcon.png");
				//ui->iconLabel->setPixmap(QPixmap(":/ewallet/Resources/ewallet/defaultIcon.png"));
			else
			{
				HttpNetWork::HttpDownLoadFile *http = new HttpNetWork::HttpDownLoadFile;
				//connect(http, SIGNAL(sigDownFinished(bool)), this, SLOT(slotDownloadIcon(bool)));
				http->setData(QVariant(imagePath));
				http->StartDownLoadFile(wallet.accountIcon, imagePath);
			}
		}
// 		WalletBoxItem * pItem = new WalletBoxItem();
// 		pItem->setItemStyle(strStyle);
// 		pItem->setIconPath(pix);
// 		pItem->setTitleText(strValue);
		//pix = pix.scaled(40, 40, Qt::KeepAspectRatio, Qt::SmoothTransformation);
		MyBoxItem * boxiem = new MyBoxItem(pix, strTitle, strName);
		ui->recvBox->AddAccount(boxiem);
		if (i == 0)
		{
			ui->recvBox->OnShowAccount(strName);
		}

	}
}          

void TurnOutWidget::slotonIndexChanged(int)
{
	QString strText = ui->recvBox->currentText();
	int i = strText.indexOf(":");
	if (i > -1)
	{
		strText = strText.right(strText.length() - i - 2);
		ui->recvBox->setEditText(strText);
	}

// 	QLineEdit *lineEdit = new QLineEdit;
// 	lineEdit->setStyleSheet("background - color: #15447c;color: white;padding - left: 4px;");
// 	lineEdit->setText("123");
// 	lineEdit->setReadOnly(true);
// 	ui->recvBox->setLineEdit(lineEdit);
}

