﻿#include "starflashwidget.h"
#include "ui_starflashwidget.h"
#include "QStringLiteralBak.h"

StarFlashWidget::StarFlashWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::StarFlashWidget();
	ui->setupUi(this);

	connect(ui->copyBtn, SIGNAL(clicked()), this, SLOT(slotClickCopy()));
	ui->refreshLabel->setPixmap(QPixmap(":/GroupChat/Resources/groupchat/refresh.png"));

	refreshGif = new QMovie(this);;
	refreshGif->setFileName(":/GroupChat/Resources/groupchat/refresh.gif");
	refreshGif->setScaledSize(ui->refreshLabel->size());

	QFile file(":/QSS/Resources/QSS/eWalletLib/starflashwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	ui->refreshLabel->installEventFilter(this);
	ui->tipLabel->hide();
}

StarFlashWidget::~StarFlashWidget()
{
	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

bool StarFlashWidget::eventFilter(QObject *obj, QEvent *event)
{
	if (obj == ui->refreshLabel && event->type() == QEvent::MouseButtonPress)
	{
		ui->refreshLabel->setMovie(refreshGif);
		refreshGif->start();
		emit sigRefreshWallet();
	}

	return QWidget::eventFilter(obj, event);
}

void StarFlashWidget::paintEvent(QPaintEvent * event)
{
	if (!starImage.isNull())
	{
		QPainter *painter = new QPainter;
		painter->begin(this);
		painter->drawImage(0, 0, starImage);
		painter->end();
		delete painter;
	}

	QWidget::paintEvent(event);
}


void StarFlashWidget::slotClickCopy()
{
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(this->objectName());

	if (ui->tipLabel->isHidden())
	{
		ui->tipLabel->show();
		QTimer::singleShot(2000, ui->tipLabel, SLOT(hide()));
	}
}

void StarFlashWidget::setAccount(QString account)
{
	ui->accountLabel->setText(account);
}

void StarFlashWidget::setId(QString id)
{
	ui->idLabel->setText(tr("ID ") + id);
}

void StarFlashWidget::setAddress(QString address)
{
	QString left = address.left(6);
	QString right = address.right(4);

	this->setObjectName(address);
	ui->addressLabel->setText(tr("ID ") + left + "******" + right);
}

void StarFlashWidget::setStar(QString star)
{
	this->starImagePath = ":/star/Resources/walletStar/" + star + ".png";
	starImage.load(starImagePath);
	starImage = starImage.scaledToWidth(this->width() * 2, Qt::SmoothTransformation);
	starImage = starImage.copy(this->width() / 2, 0, this->width(), this->height());

	QPixmap background1(":/ewallet/Resources/ewallet/background1.png");
	background1 = background1.scaledToWidth(this->width() * 2, Qt::SmoothTransformation);
	background1 = background1.copy(this->width() / 2, 0, this->width(), this->height());

	QPixmap background2(":/ewallet/Resources/ewallet/background2.png");
	background2 = background2.scaledToWidth(this->width() * 2, Qt::SmoothTransformation);
	background2 = background2.copy(this->width() / 2, 0, this->width(), this->height());

	QPainter *painter = new QPainter;
	painter->begin(&starImage);
	painter->drawPixmap(0, 0, background1);
	painter->drawPixmap(0, 0, background2);
	painter->end();
	delete painter;
}

void StarFlashWidget::setBuddysNum(int num)
{
	ui->buddysLabel->setText(QString::number(num));
}

void StarFlashWidget::setGroupsNum(int num)
{
	ui->groupsLabel->setText(QString::number(num));
}

QString StarFlashWidget::getId()
{
	QStringList list = ui->idLabel->text().split(" ");

	if (list.count() == 2)
		return list.at(1);
	else
	{
		return "";
	}
}

void StarFlashWidget::stopRefreshGif()
{
	refreshGif->stop();

	ui->refreshLabel->setPixmap(QPixmap(":/GroupChat/Resources/groupchat/refresh.png"));
}

void StarFlashWidget::setInviteCount(QString count)
{
	ui->inviteCountLabel->setText(count);
}
