﻿#include "btcturnoutmanager.h"
#ifndef Q_OS_WIN
#include "inline_mac.h"
#endif
#include "QStringLiteralBak.h"
#include "globalmanager.h"

extern OPDataManager  *gOPDataManager;

BTCTurnOutManager::BTCTurnOutManager(QObject *parent)
	: QObject(parent)
{
	cefView = NULL;

#ifdef Q_OS_MAC
	m_pWebChannel = NULL;
	m_pWebObject = NULL;
#endif // Q_OS_MAC
}

BTCTurnOutManager::~BTCTurnOutManager()
{
	if (cefView)
		cefView->deleteLater();

//#ifdef Q_OS_MAC
	if (m_pWebChannel)
		delete m_pWebChannel;
	if (m_pWebObject)
		delete m_pWebObject;
//#endif // Q_OS_MAC
}

void BTCTurnOutManager::turnOut(QString fromAddress, QString toAddress, QString value)
{
	if (strKey.isEmpty())
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("Cannot find the private key of the wallet!"));
		return;
	}

	this->fromAddress = fromAddress;
	this->toAddress = toAddress;
	this->value = value;

	//获取utxo。
	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigBtcUtxo(QString)), this, SLOT(slotBtcUtxo(QString)));
	connect(request, SIGNAL(sigBtcUtxo(QString)), request, SLOT(deleteLater()));
	request->getBtcUtxo(fromAddress);
}

void BTCTurnOutManager::slotBtcUtxo(QString utxo)
{
	if (utxo == "failed")
	{
		IMessageBox::tip((QWidget *)this->parent(), tr("Notice"), tr("Failed to get Utxo!"));
	}
	else
	{
		UserInfo user = gDataManager->getUserInfo();
		//获取私钥。
		QString priKey;
#ifdef Q_OS_WIN
		QString keyPath = gSettingsManager->getUserPath() + "/wallet//" + QString::number(user.nUserID) + "//pwr.key";
#else
        QString keyPath = gSettingsManager->getUserPath() + "/wallet//" + QString::number(user.nUserID) + "//pwr.key";
#endif
		QString mainKey = gOPDataManager->Decryption(user.strLoginPWD, keyPath);
		strKey = strKey.replace(" ", "+");
		priKey = gOPDataManager->decryptAES(strKey.toUtf8(), mainKey);

		qint64 amount = value.toInt() * 100000000;

		utxo.remove(QRegExp("\r|\n|\\s"));
		utxo.replace("\"", "\\\"");

//#ifdef Q_OS_WIN
//		if (cefView)
//			delete cefView;
//		cefView = new QCefView;
//		cefView->hide();
//		connect(cefView, SIGNAL(sigTransactionData(QString)), this, SLOT(slotTransactionData(QString)));
//		cefView->InitCefUrl("file:///./html/btcWallet.html");
//		QEventLoop loop;
//		connect(cefView, SIGNAL(sigInitFinished()), &loop, SLOT(quit()));
//		loop.exec();
//#else
		if (cefView)
			delete cefView;
		if (m_pWebChannel)
			delete m_pWebChannel;
		if (m_pWebObject)
			delete m_pWebObject;

		m_pWebChannel = new QWebChannel(this);
		m_pWebObject = new WebObjectShareLib(this);
		connect(m_pWebObject, SIGNAL(sigTransactionData(QString)), this, SLOT(slotTransactionData(QString)));

		/*QString urlName = QDir::currentPath() + ("/html/btcWallet.html");
		QUrl url = QUrl::fromUserInput(urlName);*/
        QUrl url = QUrl::fromUserInput("qrc:/html/Resources/html/btcWallet.html");
        //QUrl url = "qrc:/html/Resources/html/btcWallet.html";
		m_pWebChannel->registerObject("web", m_pWebObject);
		cefView = new QWebEngineView;
		cefView->page()->load(url);
		cefView->page()->setWebChannel(m_pWebChannel);
//#endif

//#ifdef Q_OS_WIN
//		cefView->ExecuteJavaScript(QString("GeneralSignTransaction(\"%1\",\"%2\",%3,\"%4\")")
//			.arg(priKey).arg(toAddress).arg(amount).arg(utxo));
//#else
        cefView->page()->runJavaScript(QString("GeneralSignTransaction(\"%1\",\"%2\",%3,\"%4\")")
            .arg(priKey).arg(toAddress).arg(amount).arg(utxo));
//#endif
	}
}

void BTCTurnOutManager::slotTransactionData(QString data)
{
	QJsonDocument doc = QJsonDocument::fromJson(data.toUtf8());
	QVariantMap map = doc.toVariant().toMap();

	QString rawhex;
	QString txhash;
	if (map.value("result").toString() == "true")
	{
		rawhex = map.value("rawhex").toString();
		txhash = map.value("txhash").toString();

		UserInfo user = gDataManager->getUserInfo();
		OPRequestShareLib *request = new OPRequestShareLib;
		connect(request, SIGNAL(sigBtcTransResult(bool)), this, SLOT(slotTransResult(bool)));
		connect(request, SIGNAL(sigBtcTransResult(bool)), request, SLOT(deleteLater()));
		request->publicBtcTrans(rawhex, txhash, QString::number(user.nUserID));
	}
	else
	{
		IMessageBox::tip((QWidget *)this->parent(), tr("Notice"), tr("Signature generation failed!"));
	}

	delete cefView;
	cefView = NULL;
//#ifdef Q_OS_MAC
	delete m_pWebChannel;
	m_pWebChannel = NULL;
	delete m_pWebObject;
	m_pWebObject = NULL;
//#endif // Q_OS_MAC
}

void BTCTurnOutManager::setPriKey(QString strKey)
{
	this->strKey = strKey;
}

void BTCTurnOutManager::slotTransResult(bool success)
{
	if (success)
	{
		IMessageBox::tip((QWidget *)this->parent(), tr("Notice"), tr("Deal is Done!"));
	}
	else
	{
		IMessageBox::tip((QWidget *)this->parent(), tr("Notice"), tr("Deal Failed!"));
	}
}
