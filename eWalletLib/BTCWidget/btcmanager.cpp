﻿#include "btcmanager.h"
#include "QStringLiteralBak.h"
#include "globalmanager.h"

extern OPDataManager  *gOPDataManager;

BTCManager::BTCManager(QObject *parent)
	: QObject(parent)
{
	btcWidget = new BTCWidget;
	turnInWidget = new TurnInWidget;
	turnOutWidget = new TurnOutWidget;
	recordsWidget = new RecordsWidget;

	UserInfo user = gDataManager->getUserInfo();
	turnOutWidget->setPassWord(user.strLoginPWD);

	cefView = NULL;
	m_pCurrentWallet = NULL;
	m_strBalance = "0";

//#ifdef Q_OS_MAC
	m_pWebChannel = NULL;
	m_pWebObject = NULL;
//#endif // Q_OS_MAC

	connect(btcWidget, SIGNAL(sigRecoveryWallet()), this, SLOT(slotShowInfoWidget()));
	connect(btcWidget, SIGNAL(sigWalletManager(QString)), this, SLOT(slotWalletManager(QString)));
	connect(btcWidget, SIGNAL(sigTurnInWidget()), this, SLOT(slotTurnInWidget()));
	connect(btcWidget, SIGNAL(sigTurnOutWidget()), this, SLOT(slotTurnOutWidget()));
	connect(btcWidget, SIGNAL(sigProfileWidget()), this, SLOT(slotProfileWidget()));
	connect(btcWidget, SIGNAL(sigClearChecked()), this, SIGNAL(sigClearChecked()));

	connect(turnOutWidget, SIGNAL(sigTurnOut(QString, QString)), this, SLOT(slotTurnOutManager(QString, QString)));

	connect(recordsWidget, SIGNAL(sigTurnIn()), this, SLOT(slotTurnInWidget()));
	connect(recordsWidget, SIGNAL(sigTurnOut()), this, SLOT(slotTurnOutWidget()));
}

BTCManager::~BTCManager()
{
	if (cefView)
		cefView->deleteLater();

//#ifdef Q_OS_MAC
	if (m_pWebChannel)
		delete m_pWebChannel;
	if (m_pWebObject)
		delete m_pWebObject;
//#endif // Q_OS_MAC
}

void BTCManager::setStackedWidget(QStackedWidget *widget)
{
	this->stackedWidget = widget;

	stackedWidget->addWidget(turnInWidget);
	stackedWidget->addWidget(turnOutWidget);
	stackedWidget->addWidget(recordsWidget);
}

void BTCManager::setMainAddress(QString address)
{
	this->mainAddress = address;
}

void BTCManager::setPriKey(QString priKey)
{
	this->walletKey = priKey;
}

BTCWidget * BTCManager::getBTCWidget()
{
	return btcWidget;
}

void BTCManager::setWallets(QList<ChildrenWallet> walletList)
{
	this->walletList = walletList;

	switchWallet();
}

void BTCManager::slotShowInfoWidget()
{
	WalletInfoWidget *infoWidget = new WalletInfoWidget(btcWidget);
	connect(infoWidget, SIGNAL(sigSendWalletInfo(QString, QString)),
		this, SLOT(slotShowRecWidget(QString, QString)));

	infoWidget->show();
}

void BTCManager::slotShowRecWidget(QString name, QString ID)
{
	this->walletName = name;
	this->iconID = ID;

	RecoveryWidget *recoveryWidget = new RecoveryWidget;
	connect(recoveryWidget, SIGNAL(sigRecoveryWord(QString)), this, SLOT(slotRecoveryWallet(QString)));
	connect(recoveryWidget, SIGNAL(sigPrivateKey(QString)), this, SLOT(slotRecoveryWallet(QString)));

	recoveryWidget->setBTCMode();
	recoveryWidget->show();
}

void BTCManager::slotRecoveryWallet(QString string)
{
	RecoveryWidget *widget = qobject_cast<RecoveryWidget*>(sender());
	widget->close();

//#ifdef Q_OS_WIN
//	if (cefView)
//		delete cefView;
//	cefView = new QCefView;
//	cefView->hide();
//	connect(cefView, SIGNAL(sigRecoveryResult(QString)), this, SLOT(slotRecoveryResult(QString)));
//	cefView->InitCefUrl("file:///./html/btcWallet.html");
//	QEventLoop loop;
//	connect(cefView, SIGNAL(sigInitFinished()), &loop, SLOT(quit()));
//	loop.exec();
//#else
	if (cefView)
		delete cefView;
	if (m_pWebChannel)
		delete m_pWebChannel;
	if (m_pWebObject)
		delete m_pWebObject;

	m_pWebChannel = new QWebChannel(this);
	m_pWebObject = new WebObjectShareLib(this);
	connect(m_pWebObject, SIGNAL(sigRecoveryResult(QString)), this, SLOT(slotRecoveryResult(QString)));

	/*QString urlName = QDir::currentPath() + ("/html/btcWallet.html");
	QUrl url = QUrl::fromUserInput(urlName);*/
//	QUrl url = "qrc:/html/Resources/html/btcWallet.html";
    QUrl url = QUrl::fromUserInput("qrc:/html/Resources/html/btcWallet.html");
	m_pWebChannel->registerObject("web", m_pWebObject);
	cefView = new QWebEngineView;
	cefView->page()->load(url);
	cefView->page()->setWebChannel(m_pWebChannel);
//#endif

	if (cefView)
	{
//#ifdef Q_OS_WIN
//		if (string.contains(" "))    //用空格隔开的是助记词。
//			cefView->ExecuteJavaScript(QString("recoveryFromWord(\"%1\")").arg(string));
//		else
//		{
//			cefView->ExecuteJavaScript(QString("recoveryFromKey(\"%1\")").arg(string));
//			IMessageBox::tip(NULL, tr("Notice"), tr("比特币暂不支持私钥恢复，请使用助记词"));
//		}
//#else
        if (string.contains(" "))    //用空格隔开的是助记词。
            cefView->page()->runJavaScript(QString("recoveryFromWord(\"%1\")").arg(string));
        else
        {
            cefView->page()->runJavaScript(QString("recoveryFromKey(\"%1\")").arg(string));
            IMessageBox::tip(NULL, tr("Notice"), tr("BTC cannot be restored by private key for now, please use the mnemonic word"));
        }
//#endif
	}
}

void BTCManager::slotRecoveryResult(QString json)
{
	QJsonDocument doc = QJsonDocument::fromJson(json.toUtf8());
	QVariantMap map = doc.toVariant().toMap();
	QString result = map.value("result").toString();
	if (result == "true")
	{

		QString address = map.value("address").toString().toLower();
		QString priKey = map.value("privkey").toString();
		priKey = priKey.startsWith("0x") ? priKey : "0x" + priKey;
		QString pubkey = map.value("pubkey").toString();

		QString accountIcon = gDataManager->getAppConfigInfo().PanServerDownloadURL + iconID + "/download";
		QString strKey = gOPDataManager->encryptAES(priKey.toUtf8(), walletKey);

		OPRequestShareLib *request = new OPRequestShareLib;
		connect(request, SIGNAL(sigAddChildrenWallet(bool)), this, SLOT(slotRecoveryFinished(bool)));
		connect(request, SIGNAL(sigAddChildrenWallet(bool)), request, SLOT(deleteLater()));
		request->addChildrenWallet(address, pubkey, strKey, mainAddress, walletName, accountIcon, "BTC", 1);
	}
	else
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("Failed to restore the wallet!"));
	}

	/*删除cef*/
	if (cefView)
	{
		cefView->deleteLater();
		cefView = NULL;
	}
	if (m_pWebObject)
	{
		m_pWebObject->deleteLater();
		m_pWebObject = NULL;
	}
	if (m_pWebChannel)
	{
		m_pWebChannel->deleteLater();
		m_pWebChannel = NULL;
	}
}

void BTCManager::slotRecoveryFinished(bool success)
{
	if (success)
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("The wallet restored successfully!"));

		emit sigRefresh();
	}
	else
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("The server returned error!"));
	}
}

void BTCManager::slotOpenMenu()
{
	//主菜单。
	QMenu *menu = new QMenu();

	QAction *refreshAciton = new QAction(tr("Refresh Wallet"), menu);
	refreshAciton->setIcon(QIcon(":/ewallet/Resources/ewallet/openwallet_changewallet.png"));
	connect(refreshAciton, SIGNAL(triggered()), this, SIGNAL(sigRefresh()));
	menu->addAction(refreshAciton);
	refreshAciton->deleteLater();

	QAction *inAction = new QAction(tr("Import Wallet"), menu);
	inAction->setIcon(QIcon(":/ewallet/Resources/ewallet/openwallet_importwallet.png"));
	connect(inAction, SIGNAL(triggered()), this, SLOT(slotShowInfoWidget()));
	menu->addAction(inAction);
	inAction->deleteLater();

	QFile style(":/QSS/Resources/QSS/eWalletLib/btcmanager.qss");
	style.open(QFile::ReadOnly);
	QString sheet = QLatin1String(style.readAll());

	//二级菜单。
	QMenu *childMenu = new QMenu();
	childMenu->setTitle(tr("Switch Wallet ") + QString("(%1)").arg(walletList.count()));
	childMenu->setIcon(QIcon(":/ewallet/Resources/ewallet/openwallet_changewallet.png"));
#ifdef Q_OS_WIN
    childMenu->setStyleSheet(sheet);
#else
    childMenu->setStyleSheet("QMenu{background-color: #72a4d6;color: #042439;}QMenu::item:selected{color: white;}QMenu::item:checked{color: white;}");
#endif
	for (int i = 0; i < walletList.count(); i++)
	{
		ChildrenWallet wallet = walletList.at(i);

		QAction *inAction = new QAction(wallet.accountName, childMenu);
		inAction->setObjectName(wallet.accountAddress);
		QPixmap pix;
		QString imagePath = gSettingsManager->getUserPath()+ "/resource/" + wallet.id.remove("-") + ".png";
		if (QFile(imagePath).exists())
		{
			QByteArray bytearray;
			QFile file(imagePath);
			if (file.open(QIODevice::ReadOnly))
			{
				bytearray = file.readAll();
				file.close();
			}
			pix.loadFromData(bytearray);
		}
		if (pix.isNull())
			pix.load(":/ewallet/Resources/ewallet/defaultIcon.png");

		inAction->setIcon(QIcon(pix));
		inAction->setCheckable(true);
		if (inAction->objectName() == btcWidget->getAddress())
		{
			inAction->setChecked(true);
		}
		else
		{
			inAction->setChecked(false);
		}

		connect(inAction, SIGNAL(triggered()), this, SLOT(slotSwitchWallet()));
		childMenu->addAction(inAction);
		inAction->deleteLater();
	}

	menu->addMenu(childMenu);
#ifdef Q_OS_WIN
    menu->setStyleSheet(sheet);
#else
    menu->setStyleSheet("QMenu{background-color: #72a4d6;color: #042439;}QMenu::item:selected{color: white;}");
#endif
	menu->move(QCursor::pos());
	menu->show();
	QPoint menuPos = QCursor::pos();
	menuPos.setY(menuPos.y() - menu->height());
	menu->move(menuPos);
	menu->exec();
}

void BTCManager::slotSwitchWallet()
{
	QAction *action = qobject_cast<QAction *>(sender());

	QString address = action->objectName();

	for (int i = 0; i < walletList.count(); i++)
	{
		ChildrenWallet wallet = walletList.at(i);
		if (wallet.accountAddress == address)
		{
			switchWallet(&wallet);

			break;
		}
	}
}

void BTCManager::slotGetBalance(QString balance)
{
	if (balance == "failed")
	{
		balance = "0";
	}
	m_strBalance = balance;
	btcWidget->setBalance(balance);
	turnOutWidget->setBalance(balance);
}

void BTCManager::slotWalletManager(QString address)
{
	for (int i = 0; i < walletList.count(); i++)
	{
		ChildrenWallet wallet = walletList.at(i);
		if (wallet.accountAddress == address)
		{
			WalletManager *walletManager = new WalletManager(btcWidget);
			connect(walletManager, SIGNAL(sigSetWalletInfo(QString, QString, QString, QString, QString)),
				this, SLOT(slotSetWalletInfo(QString, QString, QString, QString, QString)));
			connect(walletManager, SIGNAL(sigDeleteWallet(QString, QString, QString)),
				this, SLOT(slotDeleteWallet(QString, QString, QString)));
			walletManager->setWallet(wallet);
			walletManager->show();

			break;
		}
	}
}

void BTCManager::slotSetWalletInfo(QString accountAddress, QString mainAccountAddress, QString accountName, QString accountIcon, QString blockChainName)
{
	for (int i = 0; i < walletList.count(); i++)
	{
		ChildrenWallet wallet = walletList.at(i);
		if (wallet.accountAddress == accountAddress)
		{
			if (!accountName.isEmpty())
				wallet.accountName = accountName;
			if (!accountIcon.isEmpty())
				wallet.accountIcon = accountIcon;

			walletList.removeAt(i);
			walletList.append(wallet);

			switchWallet(&wallet);

			break;
		}
	}

	OPRequestShareLib *request = new OPRequestShareLib;
	request->setWalletInfo(accountAddress, mainAccountAddress, accountName, accountIcon, blockChainName);
	delete request;
}

void BTCManager::slotDeleteWallet(QString accountAddress, QString mainAccountAddress, QString blockChainName)
{
	for (int j = 0; j < walletList.count(); j++)
	{
		ChildrenWallet wallet = walletList.at(j);
		if (wallet.accountAddress == accountAddress)
		{
			walletList.removeAt(j);
			break;
		}
	}

	switchWallet();

	//发送网络请求。
	OPRequestShareLib *request = new OPRequestShareLib;
	request->deleteChildrenWallet(accountAddress, mainAccountAddress, blockChainName);
	delete request;

	IMessageBox::tip(NULL, tr("Notice"), tr("The wallet deleted successfully!"));
}

void BTCManager::slotTurnInWidget()
{
	stackedWidget->setCurrentWidget(turnInWidget);
}

void BTCManager::slotTurnOutWidget()
{
	stackedWidget->setCurrentWidget(turnOutWidget);
}

void BTCManager::slotProfileWidget()
{
	recordsWidget->setTitle("BTC");
	stackedWidget->setCurrentWidget(recordsWidget);
	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigRecords(QList<RecordInfo>)), recordsWidget, SLOT(slotRecords(QList<RecordInfo>)));
	connect(request, SIGNAL(sigRecords(QList<RecordInfo>)), request, SLOT(deleteLater()));
	request->getBtcTxs(btcWidget->getAddress());
}

void BTCManager::slotTurnOutManager(QString address, QString value)
{
	BTCTurnOutManager *turnOutManager = new BTCTurnOutManager(turnOutWidget);

	for (int j = 0; j < walletList.count(); j++)
	{
		ChildrenWallet wallet = walletList.at(j);
		if (wallet.accountAddress == btcWidget->getAddress())
		{
			turnOutManager->setPriKey(wallet.accountPrivateKey);
			break;
		}
	}

	turnOutManager->turnOut(btcWidget->getAddress(), address, value);
}

void BTCManager::switchWallet(ChildrenWallet* wallet /*= NULL*/)
{
	if (wallet == NULL)
	{
		if (walletList.count() > 0)
		{
			wallet = &walletList.first();
		}
	}

	if (wallet == NULL)
		btcWidget->initShow(false);
	else
	{
		if (wallet->balance.isEmpty())
		{
			OPRequestShareLib *request = new OPRequestShareLib;
			connect(request, SIGNAL(sigBtcBalance(QString)), this, SLOT(slotGetBalance(QString)));
			connect(request, SIGNAL(sigBtcBalance(QString)), request, SLOT(deleteLater()));
			request->getBtcBalance(wallet->accountAddress);
		}
		m_pCurrentWallet = wallet;
		turnInWidget->setTurnInData(*wallet);
		turnOutWidget->setTurnOutData(*wallet);
		btcWidget->setIndexWallet(*wallet);

		btcWidget->initShow(true);
	}
}

void BTCManager::OtherWindow(QList<ChildrenWallet> BTCList)
{
	if (m_pCurrentWallet == NULL)
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("There is no BTC wallet!"));
		return;
	}
	if (BTCList.size() > 0)
	{
		TurnOutWidget * pWidget = new TurnOutWidget();

		UserInfo user = gDataManager->getUserInfo();
		pWidget->setBalance(m_strBalance);
		pWidget->setTurnOutData(*m_pCurrentWallet);
		pWidget->setPassWord(user.strLoginPWD);
		connect(pWidget, SIGNAL(sigTurnOut(QString, QString)), this, SLOT(slotTurnOutManager(QString, QString)));

		pWidget->InitAsWindow();
		pWidget->setBuddyAddress(BTCList);
		pWidget->show();
	}
	else
	{
		IMessageBox::tip(NULL, tr("Notice"), tr("The opposite side have no account!"));
	}

}

void BTCManager::slotClearChecked()
{
	btcWidget->slotClearChecked();
}
