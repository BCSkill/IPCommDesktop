﻿#include "btcwidget.h"
#include "ui_btcwidget.h"
#include "globalmanager.h"
#ifdef Q_OS_MAC
#include "mycommonstyle.h"
#include "inline_mac.h"
#endif

BTCWidget::BTCWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::BTCWidget();
	ui->setupUi(this);

#ifdef Q_OS_MAC
    ui->followList->setStyle(new MyProxyStyle);
    ui->allList->setStyle(new MyProxyStyle);
#endif

	ui->iconLabel->setShape(roundRect);

	QFile file(":/QSS/Resources/QSS/eWalletLib/btcwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	connect(ui->recoveryBtn, SIGNAL(clicked()), this, SIGNAL(sigRecoveryWallet()));
	connect(ui->turnInBtn, SIGNAL(clicked()), this, SIGNAL(sigTurnInWidget()));
	connect(ui->turnOutBtn, SIGNAL(clicked()), this, SIGNAL(sigTurnOutWidget()));
	connect(ui->profileBtn, SIGNAL(clicked()), this, SIGNAL(sigProfileWidget()));
	connect(ui->copyBtn, SIGNAL(clicked()), this, SLOT(slotCopyAddress()));
	//按钮点击时发送消息 通知其他widget取消选中
	connect(ui->turnInBtn, SIGNAL(clicked()), this, SIGNAL(sigClearChecked()));
	connect(ui->turnOutBtn, SIGNAL(clicked()), this, SIGNAL(sigClearChecked()));
	connect(ui->profileBtn, SIGNAL(clicked()), this, SIGNAL(sigClearChecked()));

	connect(ui->followList, SIGNAL(itemClicked(QListWidgetItem *)), this, SIGNAL(sigProfileWidget()));

	ui->iconLabel->installEventFilter(this);

	ui->allList->hide();
	ui->tipLabel->hide();

	ui->profileBtn->setAutoExclusive(true);
	ui->turnInBtn->setAutoExclusive(true);
	ui->turnOutBtn->setAutoExclusive(true);
}

BTCWidget::~BTCWidget()
{
	delete ui;
}

bool BTCWidget::eventFilter(QObject *obj, QEvent *event)
{
	if (obj == ui->iconLabel)
	{
		if (event->type() == QEvent::MouseButtonPress)
		{
			emit sigWalletManager(getAddress());
		}
	}

	return QWidget::eventFilter(obj, event);
}

void BTCWidget::initShow(bool hasWallet)
{
	if (hasWallet)
	{
		ui->firstWidget->hide();
		ui->mainWidget->show();
	}
	else
	{
		ui->mainWidget->hide();
		ui->firstWidget->show();
	}
}

void BTCWidget::setIndexWallet(ChildrenWallet wallet)
{
	ui->nameLabel->setText(wallet.accountName);
	QString address = wallet.accountAddress.left(6) + "******" + wallet.accountAddress.right(4);
	ui->addressLabel->setText(address);
	ui->addressLabel->setObjectName(wallet.accountAddress);
	ui->numberLabel->setText(wallet.balance);
#ifdef Q_OS_WIN
        QString imagePath = gSettingsManager->getUserPath() + "/resource/" + wallet.id.remove("-") + ".png";
#else
        QString imagePath = gSettingsManager->getUserPath() + "/resource/" + wallet.id.remove("-") + ".png";
#endif

	if (QFile(imagePath).exists())
	{
		QByteArray bytearray;
		QFile file(imagePath);
		if (file.open(QIODevice::ReadOnly))
		{
			bytearray = file.readAll();
			file.close();
		}
		QPixmap pix;
		pix.loadFromData(bytearray);

		if (pix.isNull())
			pix.load(":/ewallet/Resources/ewallet/defaultIcon.png");

		ui->iconLabel->setPixmap(pix);
	}
	else
	{
		if (wallet.accountIcon.isEmpty())
			ui->iconLabel->setPixmap(QPixmap(":/ewallet/Resources/ewallet/defaultIcon.png"));
		else
		{
			HttpNetWork::HttpDownLoadFile *http = new HttpNetWork::HttpDownLoadFile;
			connect(http, SIGNAL(sigDownFinished(bool)), this, SLOT(slotDownloadIcon(bool)));
			http->setData(QVariant(imagePath));
			http->StartDownLoadFile(wallet.accountIcon, imagePath);
		}
	}


	ui->followList->clear();
	EWalletItemFollow *followWidget = new EWalletItemFollow;
	followWidget->setData(":/ewallet/Resources/ewallet/btc.png", "BTC", wallet.balance, "", "");
	QListWidgetItem *followItem = new QListWidgetItem;
	followItem->setSizeHint(QSize(240, 50));
	ui->followList->addItem(followItem);
	ui->followList->setItemWidget(followItem, followWidget);
}

void BTCWidget::slotDownloadIcon(bool success)
{
	if (success)
	{
		HttpNetWork::HttpDownLoadFile *download = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());
		QString imagePath = download->getData().toString();

		QByteArray bytearray;
		QFile file(imagePath);
		if (file.open(QIODevice::ReadOnly))
		{
			bytearray = file.readAll();
			file.close();
		}

		QPixmap pix;
		pix.loadFromData(bytearray);

		if (pix.isNull())
			pix.load(":/ewallet/Resources/ewallet/defaultIcon.png");

		ui->iconLabel->setPixmap(pix);
	}
	else
	{
		ui->iconLabel->setPixmap(QPixmap(":/ewallet/Resources/ewallet/defaultIcon.png"));
	}
}

void BTCWidget::slotCopyAddress()
{
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(ui->addressLabel->objectName());

	if (ui->tipLabel->isHidden())
	{
		ui->tipLabel->show();
		QTimer::singleShot(2000, ui->tipLabel, SLOT(hide()));
	}
}

void BTCWidget::setBalance(QString balance)
{
	ui->numberLabel->setText(balance);

	ui->followList->clear();
	EWalletItemFollow *followWidget = new EWalletItemFollow;
	followWidget->setData(":/ewallet/Resources/ewallet/btc.png", "BTC", balance, "", "");
	QListWidgetItem *followItem = new QListWidgetItem;
	followItem->setSizeHint(QSize(240, 50));
	ui->followList->addItem(followItem);
	ui->followList->setItemWidget(followItem, followWidget);
}

QString BTCWidget::getAddress()
{
	return ui->addressLabel->objectName();
}

void BTCWidget::slotClearChecked()
{	
	ui->profileBtn->setAutoExclusive(false);
	ui->turnInBtn->setAutoExclusive(false);
	ui->turnOutBtn->setAutoExclusive(false);

	ui->profileBtn->setChecked(false);
	ui->turnInBtn->setChecked(false);
	ui->turnOutBtn->setChecked(false);

	ui->profileBtn->setAutoExclusive(true);
	ui->turnInBtn->setAutoExclusive(true);
	ui->turnOutBtn->setAutoExclusive(true);
}
