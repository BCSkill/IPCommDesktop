﻿#include "ewalletmanager.h"
#include "QStringLiteralBak.h"
#include "globalmanager.h"
#ifndef Q_OS_WIN
#include "inline_mac.h"
#endif
#include "QStringLiteralBak.h"

extern OPDataManager  *gOPDataManager;

EWalletManager::EWalletManager(QObject *parent)
	: QObject(parent)
{
	walletWidget = new EWalletWidget;
	connect(walletWidget, SIGNAL(sigGetRecords()), this, SLOT(slotGetRecords()));

	cardWidget = walletWidget->getCardWidget();
	deviceWidget = walletWidget->getDeviceWidget();
	hostingWidget = walletWidget->getHostingWidget();
	
	intoWidget = walletWidget->getIntoWidget();
	safeWidget = walletWidget->getSafeWidget();
	recordsWidget = walletWidget->getRecordsWidget();

	starWidget = walletWidget->getStarWidget();
	connect(starWidget, SIGNAL(sigRefreshWallet()), this, SLOT(slotRefresh()));

	transWidget = walletWidget->getTransWidget();
	connect(transWidget, SIGNAL(sigTransfer(QString, QString, QString,QString, bool)),
		this, SLOT(slotTransfer(QString, QString, QString, QString, bool)));

	trans = new TransferManager;
	connect(trans, SIGNAL(sigFinished()), this, SLOT(slotRefresh()));
	connect(trans, SIGNAL(sigFinished()), transWidget, SLOT(slotClearText()));
	connect(trans, SIGNAL(sigTransferMsg(QString, QString, QString)), this, SIGNAL(sigTransferMsg(QString, QString, QString)));

	ethManager = new ETHManager;
	connect(ethManager, SIGNAL(sigRefresh()), this, SLOT(slotLoadChildren()));
	connect(ethManager, SIGNAL(sigTransferMsg(QString, QString, QString)), this, SIGNAL(sigTransferMsg(QString, QString, QString)));
	connect(walletWidget, SIGNAL(sigETHMenu()), ethManager, SLOT(slotOpenMenu()));
	walletWidget->addWidget(ethManager->getETHWidget());
	ethManager->setStackedWidget(walletWidget->getStackedWidget());

	btcManager = new BTCManager;
	connect(btcManager, SIGNAL(sigRefresh()), this, SLOT(slotLoadChildren()));
	connect(walletWidget, SIGNAL(sigBTCMenu()), btcManager, SLOT(slotOpenMenu()));
	walletWidget->addWidget(btcManager->getBTCWidget());
	btcManager->setStackedWidget(walletWidget->getStackedWidget());

	eosManager = new EOSManager;
	connect(eosManager, SIGNAL(sigRefresh()), this, SLOT(slotLoadChildren()));
	connect(walletWidget, SIGNAL(sigEOSMenu()), eosManager, SLOT(slotOpenMenu()));
	walletWidget->addWidget(eosManager->getEOSWidget());
	eosManager->setStackedWidget(walletWidget->getStackedWidget());

	assetsWidget = new EWalletAssetsWidget;
	walletWidget->addWidget(assetsWidget);
	connect(walletWidget,SIGNAL(sigRefreshAccount()), this,SLOT(slotRefresh()));


	//点击响应 各个页面互相取消高亮
	connect(eosManager, SIGNAL(sigClearChecked()), btcManager, SLOT(slotClearChecked()));
	connect(eosManager, SIGNAL(sigClearChecked()), ethManager, SLOT(slotClearChecked()));
	connect(eosManager, SIGNAL(sigClearChecked()), walletWidget, SLOT(slotClearChecked()));

	connect(btcManager, SIGNAL(sigClearChecked()), eosManager, SLOT(slotClearChecked()));
	connect(btcManager, SIGNAL(sigClearChecked()), ethManager, SLOT(slotClearChecked()));
	connect(btcManager, SIGNAL(sigClearChecked()), walletWidget, SLOT(slotClearChecked()));

	connect(ethManager, SIGNAL(sigClearChecked()), btcManager, SLOT(slotClearChecked()));
	connect(ethManager, SIGNAL(sigClearChecked()), eosManager, SLOT(slotClearChecked()));
	connect(ethManager, SIGNAL(sigClearChecked()), walletWidget, SLOT(slotClearChecked()));

	connect(walletWidget, SIGNAL(sigClearChecked()), btcManager, SLOT(slotClearChecked()));
	connect(walletWidget, SIGNAL(sigClearChecked()), eosManager, SLOT(slotClearChecked()));
	connect(walletWidget, SIGNAL(sigClearChecked()), ethManager, SLOT(slotClearChecked()));
}

EWalletManager::~EWalletManager()
{

}

EWalletWidget * EWalletManager::getWalletWidget()
{
	return walletWidget;
}

void EWalletManager::countBuddysNum()
{
	QList<BuddyInfo> buddysList = gDataBaseOpera->DBGetBuddyInfo();
	int count = 0;
	foreach(BuddyInfo buddy, buddysList)
	{
		if (buddy.BuddyType == 1)
			count++;
	}
	starWidget->setBuddysNum(count);
}

void EWalletManager::countGroupsNum()
{
	QList<GroupInfo> groupsList = gDataBaseOpera->DBGetAllGroupInfo();
	starWidget->setGroupsNum(groupsList.count());
}

void EWalletManager::refreshDeviceState()
{
	deviceWidget->getDeviceList();
}

void EWalletManager::showDeviceWidget()
{
	walletWidget->switchDeviceWidget();
}

void EWalletManager::slotHostingCharge(int trusteeshipId, QString fromAddress, QString toAddress, QString value)
{
	TransferManager *transManager = new TransferManager;

#ifdef Q_OS_WIN
	QString keyPath = gSettingsManager->getUserPath() + "/wallet//" + QString::number(userInfo.nUserID) + "//pwr.key";
#else
	QString keyPath = gSettingsManager->getUserPath() + "/wallet//" + QString::number(userInfo.nUserID) + "//pwr.key";
#endif

	transManager->setPrivateKey(gOPDataManager->Decryption(userInfo.strLoginPWD, keyPath));
	transManager->hostingCharge(userInfo.strAccountName, QString::number(trusteeshipId),
		value, fromAddress, toAddress);
}

void EWalletManager::setUserInfo(UserInfo info)
{
	this->userInfo = info;

	starWidget->setAccount(userInfo.strUserNickName);
	starWidget->setId(QString::number(userInfo.nUserID));
	safeWidget->setPassWord(userInfo.strLoginPWD);
	transWidget->setPassWord(userInfo.strLoginPWD);

	assetsWidget->getAssets(userInfo.strAccountName);
	cardWidget->setPhoneNumber(userInfo.strAccountName);

#ifdef Q_OS_WIN
        QString keyPath = gSettingsManager->getUserPath() + "/wallet//" + QString::number(info.nUserID) + "//pwr.key";
#else
        QString keyPath = gSettingsManager->getUserPath() + "/wallet//" + QString::number(info.nUserID) + "//pwr.key";
#endif

	safeWidget->setPrivateKey(gOPDataManager->Decryption(userInfo.strLoginPWD, keyPath));
	trans->setPrivateKey(gOPDataManager->Decryption(userInfo.strLoginPWD, keyPath));
	hostingWidget->setPrivateKey(gOPDataManager->Decryption(userInfo.strLoginPWD, keyPath));
	ethManager->setPriKey(gOPDataManager->Decryption(userInfo.strLoginPWD, keyPath));
	btcManager->setPriKey(gOPDataManager->Decryption(userInfo.strLoginPWD, keyPath));
	eosManager->setPriKey(gOPDataManager->Decryption(userInfo.strLoginPWD, keyPath));
}

void EWalletManager::setWalletInfo(WalletInfo info)
{
	this->walletInfo = info;

	starWidget->setAddress(walletInfo.address);
	starWidget->setStar(walletInfo.planet);
	starWidget->setInviteCount(walletInfo.inviteCodeUseCount);
	transWidget->setAddress(walletInfo.address);
	intoWidget->setTurnInData(QString::number(userInfo.nUserID), walletInfo.address);
	cardWidget->setAddress(info.address);
	hostingWidget->setMainAddress(info.address);

	slotRefresh();

	ethManager->setMainAddress(walletInfo.address);
	btcManager->setMainAddress(walletInfo.address);
	eosManager->setMainAddress(walletInfo.address);
	slotLoadChildren();
}

void EWalletManager::SlotChildrenWallet(QList<ChildrenWallet> walletList)
{
	bool hasMain = false;
	QList<ChildrenWallet> ethlist;
	QList<ChildrenWallet> btclist;
	QList<ChildrenWallet> eoslist;
	for (int i = 0; i < walletList.count(); i++)
	{
		if (walletList[i].blockChainName == "ETH")
		{
			if (walletList[i].accountAddress == walletInfo.address)
				hasMain = true;
			ethlist.append(walletList[i]);
		}
		if (walletList[i].blockChainName == "BTC")
			btclist.append(walletList[i]);
		if (walletList[i].blockChainName == "EOS")
			eoslist.append(walletList[i]);
	}

	if (!hasMain)
	{
		//加入主钱包。
		ChildrenWallet wallet;
		wallet.accountAddress = walletInfo.address;
		wallet.mainAccountAddress = walletInfo.address;
		wallet.accountPrivateKey = walletInfo.privkey;
		wallet.accountPublicKey = walletInfo.pubkey;
		wallet.id = "main";
		QString left = walletInfo.address.left(6);
		QString right = walletInfo.address.right(4);
		wallet.accountName = tr("Wallet ") + left + "******" + right;
		wallet.blockChainName = "ETH";

		ethlist.append(wallet);
	}

	ethManager->setWallets(ethlist);
	btcManager->setWallets(btclist);
	eosManager->setWallets(eoslist);
}

void EWalletManager::slotRefresh()
{
	//获取银钻。
	OPRequestShareLib *requestSilver = new OPRequestShareLib;
	connect(requestSilver, SIGNAL(sigIntegral(QString)), this, SLOT(slotSetIntegral(QString)));
	connect(requestSilver, SIGNAL(sigIntegral(QString)), requestSilver, SLOT(deleteLater()));
	requestSilver->getIntegral(gOPDataManager->getAccessToken());

	//获取能量。
	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigBalance(QString)), this, SLOT(slotSetBalance(QString)));
	connect(request, SIGNAL(sigBalance(QString)), request, SLOT(deleteLater()));
	request->getBalance(walletInfo.address);
}


void EWalletManager::slotSetIntegral(QString integral)
{
	integral = stringToThousandth(integral);
	cardWidget->setWhiteBalance(integral);

	starWidget->stopRefreshGif();
}

void EWalletManager::slotSetBalance(QString balance)
{
	m_balance = balance;
	balance = stringToThousandth(balance, 8);
	walletWidget->setEnergySum(balance);
	transWidget->setEnergySum(balance);
	cardWidget->setPwrBalance(balance);

	starWidget->stopRefreshGif();
}

QString EWalletManager::stringToThousandth(QString string, int keep /*= 0*/)
{
	//根据保留的位数，进行补0。
	if (keep > 0)  //需要补0操作。
	{
		int count = 0;
		if (string.contains("."))   //含小数点，只补缺少的0。
			count = keep - string.split(".").last().length();
		else                        //不含小数点，直接补固定数量的0。
		{
			string += ".";
			count = keep;
		}
		//补0操作。
		for (int i = 0; i < count; i++)
			string += "0";
	}

	//进行千分位操作，先获取整数部分。
	QString number = string.split(".").first();   //整数部分。
	QString result;  //保存结果的字符串。
	int num = 0;
	for (int i = number.count(); i > 0; i--)
	{
		result = number.at(i - 1) + result;
		num++;
		if (num == 3 && i != 1)
		{
			result = "," + result;
			num = 0;
		}
	}

	if (keep > 0)
	    result += "." + string.split(".").last();

	return result;
}

void EWalletManager::slotTransfer(QString otherID, QString toAddress, QString miner, QString value,bool bIsWindow)
{
	trans->turnOut(QString::number(userInfo.nUserID), otherID,
		walletInfo.address, toAddress, miner, value, bIsWindow);
}

void EWalletManager::slotLoadChildren()
{
	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigChildrenWallet(QList<ChildrenWallet>)), this, SLOT(SlotChildrenWallet(QList<ChildrenWallet>)));
	connect(request, SIGNAL(sigChildrenWallet(QList<ChildrenWallet>)), request, SLOT(deleteLater()));
	request->getChildrenWallet(walletInfo.address);
}

void EWalletManager::slotGetRecords()
{
	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigRecords(QList<RecordInfo>)), recordsWidget, SLOT(slotRecords(QList<RecordInfo>)));
	connect(request, SIGNAL(sigRecords(QList<RecordInfo>)), request, SLOT(deleteLater()));
	request->getRecords(walletInfo.address, ChainID_PWR, 1, 100, 1);
}

void EWalletManager::slotTransferWindow(QString buddyId, QString address)
{
	TransferWidget * pWidget = new TransferWidget();
	pWidget->InitAsWindow();
	UserInfo eInfo = gDataManager->getUserInfo();
	pWidget->setPassWord(eInfo.strLoginPWD);
	pWidget->setAddress(walletInfo.address);
	pWidget->setEnergySum(m_balance);
	pWidget->setBuddyId(buddyId);
	pWidget->setBuddyAddress(address);
	connect(pWidget, SIGNAL(sigTransfer(QString, QString, QString, QString,bool)),
		this, SLOT(slotTransfer(QString, QString, QString, QString,bool)));
	pWidget->show();
}

void EWalletManager::slotETHWindow(QString buddyId, QString strAddress)
{
	OPRequestShareLib *request = new OPRequestShareLib;
	request->setProperty("buddyId", QVariant::fromValue(buddyId));
	connect(request, SIGNAL(sigChildrenWallet(QList<ChildrenWallet>)), this, SLOT(slotBuddyETHWallets(QList<ChildrenWallet>)));
	connect(request, SIGNAL(sigChildrenWallet(QList<ChildrenWallet>)), request, SLOT(deleteLater()));
	request->getChildrenWallet(strAddress);
}
void EWalletManager::slotBTCWindow(QString buddyId, QString strAddress)
{
	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigChildrenWallet(QList<ChildrenWallet>)), this, SLOT(slotBuddyBTCWallets(QList<ChildrenWallet>)));
	connect(request, SIGNAL(sigChildrenWallet(QList<ChildrenWallet>)), request, SLOT(deleteLater()));
	request->getChildrenWallet(strAddress);
}
void EWalletManager::slotEOSWindow(QString buddyId, QString strAddress)
{
	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigChildrenWallet(QList<ChildrenWallet>)), this, SLOT(slotBuddyEOSWallets(QList<ChildrenWallet>)));
	connect(request, SIGNAL(sigChildrenWallet(QList<ChildrenWallet>)), request, SLOT(deleteLater()));
	request->getChildrenWallet(strAddress);
}

void EWalletManager::slotBuddyBTCWallets(QList<ChildrenWallet> walletList)
{
	QList<ChildrenWallet> btclist;
	for (int i = 0; i < walletList.count(); i++)
	{
		if (walletList[i].blockChainName == "BTC")
			btclist.append(walletList[i]);
	}
	btcManager->OtherWindow(btclist);
}
void EWalletManager::slotBuddyETHWallets(QList<ChildrenWallet> walletList)
{
	OPRequestShareLib *requestHttp = qobject_cast<OPRequestShareLib*>(sender());
	QVariant var = requestHttp->property("buddyId");
	QString buddyId = var.value<QString>();
	QList<ChildrenWallet> btclist;
	for (int i = 0; i < walletList.count(); i++)
	{
		if (walletList[i].blockChainName == "ETH")
			btclist.append(walletList[i]);
	}
	ethManager->OtherWindow(btclist,buddyId);
}
void EWalletManager::slotBuddyEOSWallets(QList<ChildrenWallet> walletList)
{
	QList<ChildrenWallet> btclist;
	for (int i = 0; i < walletList.count(); i++)
	{
		if (walletList[i].blockChainName == "EOS")
			btclist.append(walletList[i]);
	}
	eosManager->OtherWindow(btclist);
}


