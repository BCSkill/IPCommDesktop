<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>GroupAvatarWidget</name>
    <message>
        <location filename="groupavatarwidget.ui" line="26"/>
        <location filename="groupavatarwidget.ui" line="123"/>
        <location filename="GeneratedFiles/ui_groupavatarwidget.h" line="226"/>
        <location filename="GeneratedFiles/ui_groupavatarwidget.h" line="228"/>
        <source>Change avatar</source>
        <translation>Cambiar avatar</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.ui" line="340"/>
        <location filename="GeneratedFiles/ui_groupavatarwidget.h" line="231"/>
        <source>upload photos</source>
        <translation>subir fotos</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.ui" line="370"/>
        <location filename="GeneratedFiles/ui_groupavatarwidget.h" line="232"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.ui" line="400"/>
        <location filename="GeneratedFiles/ui_groupavatarwidget.h" line="233"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.cpp" line="231"/>
        <source>Choose a photo</source>
        <translation>Elige una foto</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.cpp" line="231"/>
        <source>ImageFile(*.jpg *.png *.bmp)</source>
        <translation>Archivo de imagen (*. Jpg * .png * .bmp)</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.cpp" line="249"/>
        <source>Wait a moment</source>
        <translation>Espera un momento</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.cpp" line="249"/>
        <source>The avatar is being uploaded!</source>
        <translation>El avatar se está cargando!</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.cpp" line="277"/>
        <source>Sorry</source>
        <translation>Lo siento</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.cpp" line="277"/>
        <source>Uploading an avatar failed!</source>
        <translation>¡No se pudo cargar un avatar!</translation>
    </message>
</context>
<context>
    <name>GroupProfileWidget</name>
    <message>
        <location filename="groupprofilewidget.ui" line="26"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="492"/>
        <source>Group details</source>
        <oldsource>Tribal details</oldsource>
        <translation>Detalles del grupo</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="56"/>
        <source>Share the group</source>
        <oldsource>Share the tribe</oldsource>
        <translation>Comparte el grupo</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="223"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="496"/>
        <source>Group QR code</source>
        <oldsource>Tribal QR code</oldsource>
        <translation>Código QR del grupo</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="325"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="499"/>
        <source>Group name</source>
        <oldsource>Tribal name</oldsource>
        <translation>Nombre del grupo</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="379"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="502"/>
        <source>English name</source>
        <translation>nombre inglés</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="437"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="503"/>
        <source>creator</source>
        <translation>creador</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="487"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="505"/>
        <source>Creation time</source>
        <translation>Tiempo de 
creación</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="540"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="507"/>
        <source>Group
introduction</source>
        <oldsource>Tribal
introduction</oldsource>
        <translation>Introducción
 de grupo</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="622"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="510"/>
        <source>Group owner</source>
        <translation>propietario</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="669"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="513"/>
        <source>Add member</source>
        <translation>Añadir miembro</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="726"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="516"/>
        <source>My nickname</source>
        <translation>Mi apodo</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="779"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="517"/>
        <source>Free to join</source>
        <translation>Libre de unirse</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="869"/>
        <location filename="groupprofilewidget.ui" line="872"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="520"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="522"/>
        <source>Join the group</source>
        <oldsource>Join the tribe</oldsource>
        <translation>Únete al grupo</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="885"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="523"/>
        <location filename="groupprofilewidget.cpp" line="709"/>
        <source>Apply for group success</source>
        <oldsource>Apply for tribal success</oldsource>
        <translation>Solicite el éxito del grupo</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="48"/>
        <source>Send message</source>
        <translation>Enviar mensaje</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="90"/>
        <source>close</source>
        <translation>cerrar</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="167"/>
        <location filename="groupprofilewidget.cpp" line="257"/>
        <source>Change avatar</source>
        <translation>Cambiar avatar</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="172"/>
        <location filename="groupprofilewidget.cpp" line="262"/>
        <source>Group avatar</source>
        <oldsource>Tribal avatar</oldsource>
        <translation>Avatar de grupo</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="178"/>
        <location filename="groupprofilewidget.cpp" line="268"/>
        <source>Group ID: </source>
        <oldsource>Tribe ID: </oldsource>
        <translation>ID de grupo:</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="186"/>
        <location filename="groupprofilewidget.cpp" line="276"/>
        <source>Not set yet</source>
        <translation>Aún no establecido</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="200"/>
        <location filename="groupprofilewidget.cpp" line="290"/>
        <source>.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="205"/>
        <location filename="groupprofilewidget.cpp" line="295"/>
        <source>No group introduction</source>
        <oldsource>No tribe introduction</oldsource>
        <translation>Sin introducción grupal</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="715"/>
        <source>Applying for a group failure</source>
        <oldsource>Applying for a tribal failure</oldsource>
        <translation>Solicitud de un grupo fallido</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="729"/>
        <source>、</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>perProfileWidget</name>
    <message>
        <location filename="perprofilewidget.ui" line="32"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="434"/>
        <source>Member details</source>
        <oldsource>Personal details</oldsource>
        <translation>Detalles del miembro</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="122"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="436"/>
        <source>View avatar</source>
        <translation>Ver avatar</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="351"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="444"/>
        <source>Modify note</source>
        <translation>Modificar nota</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="389"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="448"/>
        <source>Recommend to a friend</source>
        <translation>Recomendar a un amigo</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="568"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="452"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="645"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="454"/>
        <source>Acct.No</source>
        <translation>Cuenta</translation>
    </message>
    <message>
        <source>Interstellar ID</source>
        <translation type="vanished">ID interestelar</translation>
    </message>
    <message>
        <source>Base ID</source>
        <translation type="vanished">ID base</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="691"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="457"/>
        <source>copy</source>
        <translation>dupdo</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="724"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="460"/>
        <source>Success</source>
        <translation>Éxito</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="839"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="462"/>
        <location filename="perprofilewidget.cpp" line="105"/>
        <location filename="perprofilewidget.cpp" line="114"/>
        <source>Send message</source>
        <translation>Enviar mensaje</translation>
    </message>
    <message>
        <location filename="perprofilewidget.cpp" line="46"/>
        <source>close</source>
        <translation>cerrar</translation>
    </message>
    <message>
        <location filename="perprofilewidget.cpp" line="109"/>
        <source>Add to friends</source>
        <translation>Añadir a amigos</translation>
    </message>
    <message>
        <location filename="perprofilewidget.cpp" line="337"/>
        <source>Can&apos;t add yourself as a friend</source>
        <translation>No puedo agregarte como amigo</translation>
    </message>
    <message>
        <location filename="perprofilewidget.cpp" line="350"/>
        <source>Request sent successfully</source>
        <translation>Solicitud enviada exitosamente</translation>
    </message>
    <message>
        <location filename="perprofilewidget.cpp" line="355"/>
        <source>Request failed to send</source>
        <translation>Solicitud no enviada</translation>
    </message>
</context>
</TS>
