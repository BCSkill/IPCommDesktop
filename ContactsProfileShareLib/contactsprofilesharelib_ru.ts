<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>GroupAvatarWidget</name>
    <message>
        <location filename="groupavatarwidget.ui" line="26"/>
        <location filename="groupavatarwidget.ui" line="123"/>
        <location filename="GeneratedFiles/ui_groupavatarwidget.h" line="226"/>
        <location filename="GeneratedFiles/ui_groupavatarwidget.h" line="228"/>
        <source>Change avatar</source>
        <translation>Сменить аватар</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.ui" line="340"/>
        <location filename="GeneratedFiles/ui_groupavatarwidget.h" line="231"/>
        <source>upload photos</source>
        <translation>Загрузить</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.ui" line="370"/>
        <location filename="GeneratedFiles/ui_groupavatarwidget.h" line="232"/>
        <source>OK</source>
        <translation>Хорошо</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.ui" line="400"/>
        <location filename="GeneratedFiles/ui_groupavatarwidget.h" line="233"/>
        <source>Cancel</source>
        <translation>отменить</translation>
    </message>
    <message>
        <source>选择照片</source>
        <translation type="vanished">Выберите фото</translation>
    </message>
    <message>
        <source>图片文件(*.jpg *.png *.bmp)</source>
        <translation type="vanished">изображение(*.jpg *.png *.bmp)</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.cpp" line="231"/>
        <source>Choose a photo</source>
        <translation>Выберите фото</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.cpp" line="231"/>
        <source>ImageFile(*.jpg *.png *.bmp)</source>
        <translation>изображение(*.jpg *.png *.bmp)</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.cpp" line="249"/>
        <source>Wait a moment</source>
        <translation>Подождите минутку</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.cpp" line="249"/>
        <source>The avatar is being uploaded!</source>
        <translation>Аватар загружается!</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.cpp" line="277"/>
        <source>Sorry</source>
        <translation>сожалею</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.cpp" line="277"/>
        <source>Uploading an avatar failed!</source>
        <translation>Загрузка аватара не удалась!</translation>
    </message>
</context>
<context>
    <name>GroupProfileWidget</name>
    <message>
        <location filename="groupprofilewidget.ui" line="26"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="492"/>
        <source>Group details</source>
        <oldsource>Tribal details</oldsource>
        <translation>Детали группы</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="56"/>
        <source>Share the group</source>
        <oldsource>Share the tribe</oldsource>
        <translation>Поделиться группой</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="223"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="496"/>
        <source>Group QR code</source>
        <oldsource>Tribal QR code</oldsource>
        <translation>Групповой QR код</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="325"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="499"/>
        <source>Group name</source>
        <oldsource>Tribal name</oldsource>
        <translation>Имя группы</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="379"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="502"/>
        <source>English name</source>
        <translation>английское имя</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="437"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="503"/>
        <source>creator</source>
        <translation>создатель</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="487"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="505"/>
        <source>Creation time</source>
        <translation>Время создания</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="540"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="507"/>
        <source>Group
introduction</source>
        <oldsource>Tribal
introduction</oldsource>
        <translation>группа
вступление</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="622"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="510"/>
        <source>Group owner</source>
        <translation>владелец</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="669"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="513"/>
        <source>Add member</source>
        <translation>Добавить члена</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="726"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="516"/>
        <source>My nickname</source>
        <translation>Мой ник</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="779"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="517"/>
        <source>Free to join</source>
        <translation>Бесплатно присоединиться</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="869"/>
        <location filename="groupprofilewidget.ui" line="872"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="520"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="522"/>
        <source>Join the group</source>
        <oldsource>Join the tribe</oldsource>
        <translation>Вступить в группу</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="885"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="523"/>
        <location filename="groupprofilewidget.cpp" line="709"/>
        <source>Apply for group success</source>
        <oldsource>Apply for tribal success</oldsource>
        <translation>Подать заявку на успех группы</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="48"/>
        <source>Send message</source>
        <translation>Отправить сообщение</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="90"/>
        <source>close</source>
        <translation>близко</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="167"/>
        <location filename="groupprofilewidget.cpp" line="257"/>
        <source>Change avatar</source>
        <translation>Сменить аватар</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="172"/>
        <location filename="groupprofilewidget.cpp" line="262"/>
        <source>Group avatar</source>
        <oldsource>Tribal avatar</oldsource>
        <translation>Групповой аватар</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="178"/>
        <location filename="groupprofilewidget.cpp" line="268"/>
        <source>Group ID: </source>
        <oldsource>Tribe ID: </oldsource>
        <translation>ID группы:</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="186"/>
        <location filename="groupprofilewidget.cpp" line="276"/>
        <source>Not set yet</source>
        <translation>Еще не установлено</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="200"/>
        <location filename="groupprofilewidget.cpp" line="290"/>
        <source>.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="205"/>
        <location filename="groupprofilewidget.cpp" line="295"/>
        <source>No group introduction</source>
        <oldsource>No tribe introduction</oldsource>
        <translation>Нет группового введения</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="715"/>
        <source>Applying for a group failure</source>
        <oldsource>Applying for a tribal failure</oldsource>
        <translation>Подать заявку на группу не удалось</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="729"/>
        <source>、</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>perProfileWidget</name>
    <message>
        <location filename="perprofilewidget.ui" line="32"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="434"/>
        <source>Member details</source>
        <oldsource>Personal details</oldsource>
        <translation>Детали участника</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="122"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="436"/>
        <source>View avatar</source>
        <translation>Посмотреть аватар</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="351"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="444"/>
        <source>Modify note</source>
        <translation>Изменить заметку</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="389"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="448"/>
        <source>Recommend to a friend</source>
        <translation>Рекомендовать другу</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="568"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="452"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="645"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="454"/>
        <source>Acct.No</source>
        <translation>счета</translation>
    </message>
    <message>
        <source>Interstellar ID</source>
        <translation type="vanished">межзвездный ID</translation>
    </message>
    <message>
        <source>Base ID</source>
        <translation type="vanished">База ID</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="691"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="457"/>
        <source>copy</source>
        <translation>копия</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="724"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="460"/>
        <source>Success</source>
        <translation>успех</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="839"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="462"/>
        <location filename="perprofilewidget.cpp" line="105"/>
        <location filename="perprofilewidget.cpp" line="114"/>
        <source>Send message</source>
        <translation>Отправить сообщение</translation>
    </message>
    <message>
        <location filename="perprofilewidget.cpp" line="46"/>
        <source>close</source>
        <translation>близко</translation>
    </message>
    <message>
        <location filename="perprofilewidget.cpp" line="109"/>
        <source>Add to friends</source>
        <translation>Добавить в друзья</translation>
    </message>
    <message>
        <location filename="perprofilewidget.cpp" line="337"/>
        <source>Can&apos;t add yourself as a friend</source>
        <translation>Не могу добавить себя в друзья</translation>
    </message>
    <message>
        <location filename="perprofilewidget.cpp" line="350"/>
        <source>Request sent successfully</source>
        <translation>Запрос успешно отправлен</translation>
    </message>
    <message>
        <location filename="perprofilewidget.cpp" line="355"/>
        <source>Request failed to send</source>
        <translation>Запрос не отправлен</translation>
    </message>
</context>
</TS>
