﻿#include "perprofilewidget.h"
#include "ui_perprofilewidget.h"
#include "leavemessagewidget.h"
//#include "oprequestsharelib.h"
#ifndef Q_OS_WIN
#include "mycommonstyle.h"
#endif

#include "QStringLiteralBak.h"

perProfileWidget::perProfileWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::perProfileWidget();

    ui->setupUi(this);

	m_pPicWidget = NULL;

    this->setWindowFlags(Qt::FramelessWindowHint);
	this->setAttribute(Qt::WA_TranslucentBackground);
	this->setAttribute(Qt::WA_DeleteOnClose);
	setWindowIcon(QIcon(":/Login/Resources/login/system.ico"));

	QFile file(":/QSS/Resources/QSS/ContactsProfileShareLib/perprofilewidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	QPixmap mask(":/profile/Resources/profile/profileMask.png");
	this->setMask(mask.mask());

	connect(ui->copyAddressBtn, SIGNAL(clicked(bool)), this, SLOT(slotCopyAddress()));
	connect(ui->perChatBtn, SIGNAL(clicked(bool)), this, SLOT(slotPerChat()));
	connect(ui->perAddBtn, SIGNAL(clicked(bool)), this, SLOT(slotStrangerChat()));

	ui->headerLabel->installEventFilter(this);

	//没有父窗口的时候，该窗口为单独的悬浮窗口，此时显示关闭按钮。
	if (this->parent() == NULL)
	{
		closeBtn = new QPushButton(this);
		closeBtn->setStyleSheet("QPushButton{border-image: url(:/profile/Resources/profile/close.png);}");
		closeBtn->setCursor(Qt::PointingHandCursor);
		closeBtn->setToolTip(tr("close"));
		closeBtn->resize(24, 24);
		closeBtn->move(310, 10);

		connect(closeBtn, SIGNAL(clicked(bool)), this, SIGNAL(sigCloseProfile()));

		ui->starLabel->installEventFilter(this);
		ui->noteBtn->hide();
	}
	    
	connect(ui->noteBtn, SIGNAL(clicked()), this, SLOT(slotEditNote()));
	connect(ui->nameEdit, SIGNAL(returnPressed()), this, SLOT(slotEditNote()));
	connect(ui->shareBtn, SIGNAL(clicked()), this, SLOT(slotShare()));

	ui->copyLabel->hide();
	ui->perAddBtn->hide();

#ifndef Q_OS_WIN
    ui->nameEdit->setStyle(new MyProxyStyle);
#endif
}

perProfileWidget::~perProfileWidget()
{
	if (m_pPicWidget)
	{
		delete m_pPicWidget;
		m_pPicWidget = NULL;
	}

	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

void perProfileWidget::setBuddy(BuddyInfo buddy, AddressInfo wallet)
{
	m_buddyInfo = buddy;
	buddyID = QString::number(buddy.nUserId);
	headerPath = buddy.strLocalAvatar;

	//先清空。
	ui->nameEdit->setReadOnly(true);
	ui->nameEdit->clear();
	ui->sexLabel->clear();
	ui->signLabel->clear();
	ui->lianxinLabel->clear();
	ui->addressLabel->clear();

	if (gDataManager->getUserInfo().nUserID == buddy.nUserId)
	{
		ui->perChatBtn->hide();
	}
	else
	{
		if (gDataBaseOpera->DBJudgeFriendIsHaveByID(buddyID))
		{
			ui->perChatBtn->setText(tr("Send message"));
		}
		else
		{
			ui->perChatBtn->setText(tr("Add to friends"));
			//不是好友的情况下，隐藏分享的按钮。
			ui->shareBtn->hide();
			if (buddy.disableStrangers == 0)
			{
				ui->perAddBtn->setText(tr("Send message"));
				ui->perAddBtn->show();
			}
		}
	}

	QPixmap pix(buddy.strLocalAvatar);
	if (pix.isNull())
	{
		QByteArray bytearray = "";
		QFile file(buddy.strLocalAvatar);
		if (file.open(QIODevice::ReadOnly) && file.size() != 0)
		{
			bytearray = file.readAll();
		}
		file.close();
		if (!pix.loadFromData(bytearray) || bytearray == "")
		{
			pix.load(":/PerChat/Resources/person/temp.png");
			headerPath = ":/PerChat/Resources/person/temp.png";
		}
	}

	pix = pix.scaled(ui->headerLabel->size(), Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation);

	ui->headerLabel->setPixmap(pix);

	if (buddy.strSex == "F")
		ui->sexLabel->setPixmap(QPixmap(":/profile/Resources/profile/female.png"));
	if (buddy.strSex == "M")
		ui->sexLabel->setPixmap(QPixmap(":/profile/Resources/profile/male.png"));

	ui->signLabel->setText(buddy.strSign);
	if (buddy.strNote.isEmpty())
		ui->nameEdit->setText(buddy.strNickName);
	else
	    ui->nameEdit->setText(buddy.strNote);


	ui->lianxinLabel->setText(QString::number(buddy.nUserId));

	setAddress(wallet);
}

void perProfileWidget::slotPerChat()
{
	if (gDataBaseOpera->DBJudgeFriendIsHaveByID(buddyID))
	{
		slotOpenPerChat();
	}
	else
	{
		slotAddGroupUser();
	}	
}
void perProfileWidget::slotOpenPerChat()
{
	emit sigPerChat(OpenPer, QVariant::fromValue(buddyID));

	if (this->parent() == NULL)
		this->hide();
}

void perProfileWidget::slotStrangerChat()
{
	BuddyInfo checkInfo = gDataBaseOpera->DBGetBuddyInfoByID(buddyID);
	if (checkInfo.nUserId == -1)
	{
		checkInfo = gDataBaseOpera->DBGetGroupUserFromID(buddyID);
		if (checkInfo.nUserId == -1)
		{
			//数据库里没有 需暂存
			m_buddyInfo.BuddyType = 0;
			gDataBaseOpera->DBInsertBuddyInfo(m_buddyInfo);
		}
	}
		
	slotOpenPerChat();
}

void perProfileWidget::slotCopyAddress()
{
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(ui->addressLabel->objectName());

	if (ui->copyLabel->isHidden())
	{
		ui->copyLabel->show();
		QTimer::singleShot(2000, ui->copyLabel, SLOT(hide()));
	}
}

void perProfileWidget::setStar(QString star)
{
	//绘制星球球体。
	QPixmap background(":/ewallet/Resources/ewallet/pwrWidget/background.png");
	background = background.scaled(ui->starLabel->size(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

	QString starPath = ":/star/Resources/walletStar/" + star + ".png";
	QPixmap starImage(starPath);
	if (!starImage.isNull())
	{
		starImage = starImage.scaledToWidth(ui->starLabel->width() * 2, Qt::SmoothTransformation);
		starImage = starImage.copy(ui->starLabel->width() / 2, ui->starLabel->height() / 4, ui->starLabel->width(), ui->starLabel->height());
	}

	QPixmap background1(":/ewallet/Resources/ewallet/background1.png");
	background1 = background1.scaledToWidth(ui->starLabel->width() * 2, Qt::SmoothTransformation);
	background1 = background1.copy(ui->starLabel->width() / 2, 0, ui->starLabel->width(), ui->starLabel->height());

	QPixmap background2(":/ewallet/Resources/ewallet/background2.png");
	background2 = background2.scaledToWidth(ui->starLabel->width() * 2, Qt::SmoothTransformation);
	background2 = background2.copy(ui->starLabel->width() / 2, ui->starLabel->height() / 4, ui->starLabel->width(), ui->starLabel->height());

	QPainter *painter = new QPainter;
	painter->begin(&background);
	if (!starImage.isNull())
	  painter->drawPixmap(0, 0, starImage);
	painter->drawPixmap(0, 0, background1);
	painter->drawPixmap(0, 0, background2);
	painter->end();
	delete painter;
	
	ui->starLabel->setPixmap(background);
}

bool perProfileWidget::eventFilter(QObject *obj, QEvent *e)
{
	if (obj == ui->starLabel)
	{
		if (e->type() == QEvent::MouseButtonPress)
		{
			QMouseEvent *event = (QMouseEvent *)e;
			mouse = event->pos();   //设置移动的原始位置。
		}
		if (e->type() == QEvent::MouseMove)
		{
			QMouseEvent *event = (QMouseEvent *)e;
			if (mouse.x() >= 0)
			{
				//首先通过做差值，获得鼠标位移的距离。
				int x = event->pos().x() - mouse.x();
				int y = event->pos().y() - mouse.y();
				//移动本窗体。
				this->move(this->x() + x, this->y() + y);
			}
		}
		if (e->type() == QEvent::MouseButtonRelease)
		{
			QMouseEvent *event = (QMouseEvent *)e;
			mouse.setX(-1);
		}
	}

	if (obj == ui->headerLabel)
	{
		if (e->type() == QEvent::MouseButtonPress)
		{
			//20180828wmc 屏蔽原有图片浏览窗口使用新窗口
			// 			ZoomImg *zoom = new ZoomImg;
			// 			zoom->OpenImg(headerPath);
// 			m_pPicWidget = new PicWidget();
// 			connect(m_pPicWidget, SIGNAL(sigPicClose()), this, SLOT(slotPicClose()));
// 			m_pPicWidget->setPath(headerPath, this,1);
// 			m_pPicWidget->show();
			sigOpenPic(headerPath,NULL,this);
		}
	}

	return QWidget::eventFilter(obj, e);
}

void perProfileWidget::slotEditNote()
{
	if (ui->nameEdit->isReadOnly())
	{
		ui->nameEdit->setReadOnly(false);
		ui->nameEdit->setFocus();
	}
	else
	{
		//编辑结束。
		ui->nameEdit->setReadOnly(true);
		ui->label->setFocus();

		//将用户编辑的备注放入buddy。
		BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(buddyID);
		buddy.strNote = ui->nameEdit->text();

		//重新设置被编辑好友的拼音首字母，如果用户编辑的内容为空，将姓名栏还原为好友的昵称
		AlphabeticalSortSharedLib sortLib;
		if (buddy.strNote.isEmpty())
		{
			buddy.strPingYin = sortLib.GetChineseSpell(buddy.strNickName);
			ui->nameEdit->setText(buddy.strNickName);
		}
		else
		{
			buddy.strPingYin = sortLib.GetChineseSpell(buddy.strNote);
		}

		//保存至数据库。
		gDataBaseOpera->DBInsertBuddyInfo(buddy);

		//发送请求通知服务器
		HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
		UserInfo userInfo = gDataManager->getUserInfo();
		QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/user/updateFriend"
			+ QString("?userId=%1&passWord=%2&friendUserId=%3&note=%4").arg(userInfo.nUserID).arg(userInfo.strUserPWD).arg(buddyID).arg(buddy.strNote);

		http->getHttpRequest(url);

		//向联系人列表发送更新该好友的消息。
		emit sigUpdateBuddyInfo(buddy);
	}
}

void perProfileWidget::slotAddGroupUser()
{
	LeaveMessageWidget* MegWidget = new LeaveMessageWidget(this);
	UserInfo eInfo = gDataManager->getUserInfo();
	if (eInfo.nUserID == buddyID.toInt())
	{
		ui->tipLabel->setText(tr("Can't add yourself as a friend"));
	}
	else
	{
		MegWidget->OnInitBuddyInfo(buddyID);
		connect(MegWidget, SIGNAL(sigApplySuccessed()), this, SLOT(slotApplySuccessed()));
		connect(MegWidget, SIGNAL(sigApplyFailed()), this, SLOT(slotApplyFailed()));
		MegWidget->show();
	}
}

void perProfileWidget::slotApplySuccessed()
{
	ui->tipLabel->setText(tr("Request sent successfully"));
}

void perProfileWidget::slotApplyFailed()
{
	ui->tipLabel->setText(tr("Request failed to send"));
}

void perProfileWidget::slotShare()
{
	emit sigShareID(OpenPer, buddyID);
	if (this->parent() == NULL)
		this->hide();
}

void perProfileWidget::slotPicClose()
{
	m_pPicWidget = NULL;
}

QString perProfileWidget::getBuddyId()
{
	return buddyID;
}

void perProfileWidget::setAddress(AddressInfo info)
{
	if (!info.ethAddress.isEmpty())
	{
		ui->addressLabel->setObjectName(info.ethAddress);
		ui->addressLabel->setText(info.ethAddress.left(6) + "******" + info.ethAddress.right(4));

		setStar(info.planet);
		ui->copyAddressBtn->show();
	}
	else
	{
		ui->copyAddressBtn->hide();
	}
}

void perProfileWidget::closeEvent(QCloseEvent * event)
{
	sigCloseProfile();
	//QWidget::closeEvent(event);
}