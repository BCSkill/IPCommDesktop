<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hy_AM">
<context>
    <name>GroupAvatarWidget</name>
    <message>
        <location filename="groupavatarwidget.ui" line="26"/>
        <location filename="groupavatarwidget.ui" line="123"/>
        <location filename="GeneratedFiles/ui_groupavatarwidget.h" line="226"/>
        <location filename="GeneratedFiles/ui_groupavatarwidget.h" line="228"/>
        <source>Change avatar</source>
        <translation>Փոխել avatarը</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.ui" line="340"/>
        <location filename="GeneratedFiles/ui_groupavatarwidget.h" line="231"/>
        <source>upload photos</source>
        <translation>վերբեռնեք</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.ui" line="370"/>
        <location filename="GeneratedFiles/ui_groupavatarwidget.h" line="232"/>
        <source>OK</source>
        <translation>լավ</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.ui" line="400"/>
        <location filename="GeneratedFiles/ui_groupavatarwidget.h" line="233"/>
        <source>Cancel</source>
        <translation>Չեղարկել</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.cpp" line="231"/>
        <source>Choose a photo</source>
        <translation>Ընտրեք լուսանկար</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.cpp" line="231"/>
        <source>ImageFile(*.jpg *.png *.bmp)</source>
        <translation>պատկերի ֆայլը(*.jpg *.png *.bmp)</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.cpp" line="249"/>
        <source>Wait a moment</source>
        <translation>稍等</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.cpp" line="249"/>
        <source>The avatar is being uploaded!</source>
        <translation>Ավանդը վերբեռնվում է！</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.cpp" line="277"/>
        <source>Sorry</source>
        <translation>Ներողություն</translation>
    </message>
    <message>
        <location filename="groupavatarwidget.cpp" line="277"/>
        <source>Uploading an avatar failed!</source>
        <translation>Ավատարի վերբեռնումը ձախողվեց!</translation>
    </message>
</context>
<context>
    <name>GroupProfileWidget</name>
    <message>
        <location filename="groupprofilewidget.ui" line="26"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="492"/>
        <source>Group details</source>
        <oldsource>Tribal details</oldsource>
        <translation>Խմբի մանրամասները</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="56"/>
        <source>Share the group</source>
        <oldsource>Share the tribe</oldsource>
        <translation>Կիսվեք խմբին</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="223"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="496"/>
        <source>Group QR code</source>
        <oldsource>Tribal QR code</oldsource>
        <translation>Խմբի QR կոդը</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="325"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="499"/>
        <source>Group name</source>
        <oldsource>Tribal name</oldsource>
        <translation>Խմբի անունը</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="379"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="502"/>
        <source>English name</source>
        <translation>Անգլերեն անունը</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="437"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="503"/>
        <source>creator</source>
        <translation>ստեղծող</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="487"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="505"/>
        <source>Creation time</source>
        <translation>Ստեղծվել է</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="540"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="507"/>
        <source>Group
introduction</source>
        <oldsource>Tribal
introduction</oldsource>
        <translation>Խումբը
Ներածություն</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="622"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="510"/>
        <source>Group owner</source>
        <translation>Խմբի սեփականատերը</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="669"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="513"/>
        <source>Add member</source>
        <translation>Ավելացնել անդամ</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="726"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="516"/>
        <source>My nickname</source>
        <translation>Իմ մականունը</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="779"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="517"/>
        <source>Free to join</source>
        <translation>Անվճար մուտք</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="869"/>
        <location filename="groupprofilewidget.ui" line="872"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="520"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="522"/>
        <source>Join the group</source>
        <oldsource>Join the tribe</oldsource>
        <translation>Միանալ խմբին</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.ui" line="885"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="523"/>
        <location filename="groupprofilewidget.cpp" line="709"/>
        <source>Apply for group success</source>
        <oldsource>Apply for tribal success</oldsource>
        <translation>Դիմեք խմբի համար հաջողության հասնելու համար</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="48"/>
        <source>Send message</source>
        <translation>Ուղարկել հաղորդագրություն</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="90"/>
        <source>close</source>
        <translation>փակել</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="167"/>
        <location filename="groupprofilewidget.cpp" line="257"/>
        <source>Change avatar</source>
        <translation>Փոխել avatarը</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="172"/>
        <location filename="groupprofilewidget.cpp" line="262"/>
        <source>Group avatar</source>
        <oldsource>Tribal avatar</oldsource>
        <translation>Խմբի պատկերը</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="178"/>
        <location filename="groupprofilewidget.cpp" line="268"/>
        <source>Group ID: </source>
        <oldsource>Tribe ID: </oldsource>
        <translation>Խմբի ID:</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="186"/>
        <location filename="groupprofilewidget.cpp" line="276"/>
        <source>Not set yet</source>
        <translation>Դեռեւս չի սահմանվել</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="200"/>
        <location filename="groupprofilewidget.cpp" line="290"/>
        <source>.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="205"/>
        <location filename="groupprofilewidget.cpp" line="295"/>
        <source>No group introduction</source>
        <oldsource>No tribe introduction</oldsource>
        <translation>Խմբի ներածություն չկա</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="715"/>
        <source>Applying for a group failure</source>
        <oldsource>Applying for a tribal failure</oldsource>
        <translation>Խմբի համար դիմել ձախողվեց</translation>
    </message>
    <message>
        <location filename="groupprofilewidget.cpp" line="729"/>
        <source>、</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>perProfileWidget</name>
    <message>
        <location filename="perprofilewidget.ui" line="32"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="434"/>
        <source>Member details</source>
        <oldsource>Personal details</oldsource>
        <translation>Անդամների մանրամասները</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="122"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="436"/>
        <source>View avatar</source>
        <translation>Դիտել պրոֆիլը</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="351"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="444"/>
        <source>Modify note</source>
        <translation>Փոփոխել գրառումը</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="389"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="448"/>
        <source>Recommend to a friend</source>
        <translation>Խորհուրդ տալ ընկերոջը</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="568"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="452"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="645"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="454"/>
        <source>Acct.No</source>
        <translation>Հաշիվ</translation>
    </message>
    <message>
        <source>Interstellar ID</source>
        <translation type="vanished">Միջաստղային ID</translation>
    </message>
    <message>
        <source>Base ID</source>
        <translation type="vanished">բազան ID</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="691"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="457"/>
        <source>copy</source>
        <translation>պատճենը</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="724"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="460"/>
        <source>Success</source>
        <translation>Հաջողություն</translation>
    </message>
    <message>
        <location filename="perprofilewidget.ui" line="839"/>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="462"/>
        <location filename="perprofilewidget.cpp" line="105"/>
        <location filename="perprofilewidget.cpp" line="114"/>
        <source>Send message</source>
        <translation>Ուղարկել հաղորդագրություն</translation>
    </message>
    <message>
        <location filename="perprofilewidget.cpp" line="46"/>
        <source>close</source>
        <translation>փակել</translation>
    </message>
    <message>
        <location filename="perprofilewidget.cpp" line="109"/>
        <source>Add to friends</source>
        <translation>Ավելացնել ընկերներին</translation>
    </message>
    <message>
        <location filename="perprofilewidget.cpp" line="337"/>
        <source>Can&apos;t add yourself as a friend</source>
        <translation>Չի կարող ավելացնել ձեր ընկերը</translation>
    </message>
    <message>
        <location filename="perprofilewidget.cpp" line="350"/>
        <source>Request sent successfully</source>
        <translation>Հարցումը հաջողությամբ ուղարկվեց</translation>
    </message>
    <message>
        <location filename="perprofilewidget.cpp" line="355"/>
        <source>Request failed to send</source>
        <translation>Հայտը չի ուղարկվել</translation>
    </message>
</context>
</TS>
