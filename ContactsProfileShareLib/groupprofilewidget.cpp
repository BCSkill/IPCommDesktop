﻿#include "groupprofilewidget.h"
#include "ui_groupprofilewidget.h"
#include <QDesktopWidget>
#include "childWidget/groupaddbuddywidget.h"
#include "qgroupqr.h"
#include "leavemessagewidget.h"

#ifndef Q_OS_WIN
#include "mycommonstyle.h"
#endif

#include "QStringLiteralBak.h"

GroupProfileWidget::GroupProfileWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::GroupProfileWidget();

	ui->setupUi(this);

	this->setWindowFlags(Qt::FramelessWindowHint);
	this->setAttribute(Qt::WA_TranslucentBackground);
	//this->setAttribute(Qt::WA_DeleteOnClose);
	setWindowIcon(QIcon(":/Login/Resources/login/system.ico"));

	QPixmap mask(":/profile/Resources/profile/profileMask.png");
	this->setMask(mask.mask());

	headerLabel = new QLabelHeader(this);
	headerLabel->setScaledContents(true);
	headerLabel->setFixedSize(70, 70);
	headerLabel->move(15, 15);

	this->installEventFilter(this);
	headerLabel->installEventFilter(this);
	//
	ui->nameEdit->installEventFilter(this);
	ui->enEdit->installEventFilter(this);
 	ui->descEdit->installEventFilter(this);
	ui->noteEdit->installEventFilter(this);
	//
	groupChatBtn = new QPushButton(this);
	groupChatBtn->setStyleSheet("QPushButton{border-image:url(:/profile/Resources/profile/chatBtn.png)}"
		"QPushButton:hover{border-image:url(:/profile/Resources/profile/chatBtn_hover.png)}");
	groupChatBtn->resize(48, 48);
	groupChatBtn->move(290, 76);
	//groupChatBtn->setCursor(Qt::PointingHandCursor);
	groupChatBtn->setToolTip(tr("Send message"));

	shareBtn = new QPushButton(this);
	shareBtn->setStyleSheet("QPushButton{border-image:url(:/profile/Resources/profile/shareBtn.png)}"
		"QPushButton:hover{border-image:url(:/profile/Resources/profile/shareBtn_hover.png)}");
	shareBtn->resize(48, 48);
	shareBtn->move(235, 76);
	//groupChatBtn->setCursor(Qt::PointingHandCursor);
	shareBtn->setToolTip(tr("Share the group"));

	connect(groupChatBtn, SIGNAL(clicked(bool)), this, SLOT(slotGroupChat()));
	connect(ui->freeBtn, SIGNAL(clicked()), this, SLOT(slotToggledFreeBtn()));
	connect(ui->QRcodeBtn, SIGNAL(clicked()), this, SLOT(slotClickQRcodeBtn()));

// 	connect(ui->nameEdit, SIGNAL(editingFinished()), this, SLOT(slotEditName()));
// 	connect(ui->enEdit, SIGNAL(editingFinished()), this, SLOT(slotEditKey()));
// 	connect(ui->descEdit, SIGNAL(modificationChanged()), this, SLOT(slotEditDesc()));
// 	connect(ui->noteEdit, SIGNAL(editingFinished()), this, SLOT(slotEditNote()));

	connect(ui->addBtn, SIGNAL(clicked()), this, SLOT(slotGroupAddBuddy()));
	connect(shareBtn, SIGNAL(clicked()), this, SLOT(slotShare()));
	connect(ui->joinBtn, SIGNAL(clicked()), this, SLOT(slotApplyJoinGroup()));

	avatarWidget = new GroupAvatarWidget();
	connect(avatarWidget, SIGNAL(sigUploadImagePath(QString)), this, SLOT(slotUploadHeaderImage(QString)));

	QFile file(":/QSS/Resources/QSS/ContactsProfileShareLib/groupprofilewidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();
	
	ui_nameEditStyleSheet = ui->nameEdit->styleSheet();
	ui_enEditStyleSheet = ui->enEdit->styleSheet();
	ui_descEditStyleSheet = ui->descEdit->styleSheet();

	//没有父窗口的时候，该窗口为单独的悬浮窗口，此时显示关闭按钮。
	if (this->parent() == NULL)
	{
		closeBtn = new QPushButton(this);
		closeBtn->setStyleSheet("QPushButton{border-image: url(:/profile/Resources/profile/close.png);}");
		closeBtn->setCursor(Qt::PointingHandCursor);
		closeBtn->setToolTip(tr("close"));
		closeBtn->resize(24, 24);
		closeBtn->move(310, 10);

		connect(closeBtn, SIGNAL(clicked(bool)), this, SIGNAL(sigCloseProfile()));

		ui->topWidget->installEventFilter(this);
	}
	ui->enEdit->setToolTip(ui->enEdit->text());
	ui->nameEdit->setToolTip(ui->nameEdit->text());
#ifndef Q_OS_WIN
    ui->enEdit->setStyle(new MyProxyStyle);
    ui->nameEdit->setStyle(new MyProxyStyle);
    ui->noteEdit->setStyle(new MyProxyStyle);
#endif
}

GroupProfileWidget::~GroupProfileWidget()
{
	//if (closeBtn)
	//{
	//	delete closeBtn;
	//	closeBtn = NULL;
	//}
	//if (headerLabel)
	//{
	//	delete headerLabel;
	//	headerLabel = NULL;
	//}
	//if (groupChatBtn)
	//{
	//	delete groupChatBtn;
	//	groupChatBtn = NULL;
	//}
	//if (shareBtn)
	//{
	//	delete shareBtn;
	//	shareBtn = NULL;
	//}
	if (avatarWidget)
	{
		delete avatarWidget;
		avatarWidget = NULL;
	}
	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

void GroupProfileWidget::setGroup(GroupInfo group)
{
	//保存群ID，从数据库读取群信息。
	this->groupID = group.groupId;

	//设置群头像。
	QPixmap pix(group.groupLoacalHeadImage);
	if (pix.isNull())
	{
		QByteArray bytearray = "";
		QFile file(group.groupLoacalHeadImage);
		if (file.open(QIODevice::ReadOnly) && file.size() != 0)
		{
			bytearray = file.readAll();
		}
		file.close();
		if (!pix.loadFromData(bytearray) || bytearray == "")
		{
			pix.load(":/GroupChat/Resources/groupchat/group.png");
		}
	}
	headerLabel->setPixmap(pix);
	//群主可以修改头像。
	UserInfo userInfo = gDataManager->getUserInfo();
	if (group.createUserId == QString::number(userInfo.nUserID))   //自己是群主。
	{
		headerLabel->setToolTip(tr("Change avatar"));
		headerLabel->setCursor(Qt::PointingHandCursor);
	}
	else
	{
		headerLabel->setToolTip(tr("Group avatar"));
		headerLabel->setCursor(Qt::ArrowCursor);
	}

	//设置title位置的标题和群ID。
	setAdjustText(ui->groupLabel, group.groupName);
	ui->numberLabel->setText(tr("Group ID: ") + group.groupId);

	//设置群名称和群英文名。
	ui->nameEdit->setText(group.groupName);
	ui->nameEdit->setToolTip(group.groupName);
	ui->nameEdit->setCursorPosition(0);
	if (group.groupKey.isEmpty())
	{
		ui->enEdit->setText(tr("Not set yet"));
		ui->enEdit->setToolTip("");
	}
	else
	{
		ui->enEdit->setText(group.groupKey);
		ui->enEdit->setToolTip(group.groupKey);
		ui->enEdit->setCursorPosition(0);
	}
		

	//设置创建者ID和创建时间。
	ui->createrLabel->setText(group.createUserId);
	if (!group.createTime.isEmpty())
		group.createTime = group.createTime.left(4) + tr(".") + group.createTime.mid(4, 2) + tr(".") + group.createTime.mid(6, 2) + tr(".");
	ui->timeLabel->setText(group.createTime);

	//设置部落介绍。
	if (group.groupDesc.isEmpty())
		ui->descEdit->setPlainText(tr("No group introduction"));
	else
		ui->descEdit->setPlainText(group.groupDesc);

	ui->nameEdit->setReadOnly(true);
	ui->nameEdit->setStyleSheet("QLineEdit{background:transparent; border - width:0; border - style:outset }");

	ui->enEdit->setReadOnly(true);
	ui->enEdit->setStyleSheet("QLineEdit{background:transparent;border-width:0;border-style:outset}");

	ui->descEdit->setReadOnly(true);
	ui->descEdit->setStyleSheet("background:transparent;border-width:0;border-style:outset");

	ui->freeBtn->setEnabled(false);

	//隐藏进入聊天按钮，
	groupChatBtn->hide();
	//隐藏本地群显示的信息。
	ui->localWidget->hide();
	//隐藏分享按钮
	shareBtn->hide();
	//隐藏加群申请提示标签。
	ui->tipLabel->hide();
}

void GroupProfileWidget::setGroup(QString groupID)
{
	//保存群ID，从数据库读取群信息。
	this->groupID = groupID;
	GroupInfo group = gDataBaseOpera->DBGetGroupFromID(groupID);

	//设置群头像。
	QPixmap pix(group.groupLoacalHeadImage);
	if (pix.isNull())
	{
		QByteArray bytearray = "";
		QFile file(group.groupLoacalHeadImage);
		if (file.open(QIODevice::ReadOnly) && file.size() != 0)
		{
			bytearray = file.readAll();
		}
		file.close();
		if (!pix.loadFromData(bytearray) || bytearray == "")
		{
			pix.load(":/GroupChat/Resources/groupchat/group.png");
		}
	}
	headerLabel->setPixmap(pix);
	//群主可以修改头像。
	UserInfo userInfo = gDataManager->getUserInfo();
	if (group.createUserId == QString::number(userInfo.nUserID))   //自己是群主。
	{
		headerLabel->setToolTip(tr("Change avatar"));
		headerLabel->setCursor(Qt::PointingHandCursor);
	}
	else
	{
		headerLabel->setToolTip(tr("Group avatar"));
		headerLabel->setCursor(Qt::ArrowCursor);
	}

	//设置title位置的标题和群ID。
	setAdjustText(ui->groupLabel, group.groupName);
	ui->numberLabel->setText(tr("Group ID: ") + group.groupId);

	//设置群名称和群英文名。
	ui->nameEdit->setText(group.groupName);
	ui->nameEdit->setToolTip(group.groupName);
	ui->nameEdit->setCursorPosition(0);
	if (group.groupKey.isEmpty())
	{
		ui->enEdit->setText(tr("Not set yet"));
		ui->enEdit->setToolTip("");
	}
	else
	{
		ui->enEdit->setText(group.groupKey);
		ui->enEdit->setToolTip(group.groupKey);
		ui->enEdit->setCursorPosition(0);
	}


	//设置创建者ID和创建时间。
	ui->createrLabel->setText(group.createUserId);
	if (!group.createTime.isEmpty())
		group.createTime = group.createTime.left(4) + tr(".") + group.createTime.mid(4, 2) + tr(".") + group.createTime.mid(6, 2) + tr(".");
	ui->timeLabel->setText(group.createTime);
	
	//设置部落介绍。
	if (group.groupDesc.isEmpty())
		ui->descEdit->setPlainText(tr("No group introduction"));
	else
	    ui->descEdit->setPlainText(group.groupDesc);

	//设置本群昵称。
	BuddyInfo buddy = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(groupID, QString::number(userInfo.nUserID));
	if (buddy.strNote.isEmpty())
		ui->noteEdit->setText(buddy.strNickName);
	else
		ui->noteEdit->setText(buddy.strNote);

	//设置自由加群功能。
	if (group.groupType == 0)
		ui->freeBtn->setChecked(false);
	if (group.groupType == 1)
		ui->freeBtn->setChecked(true);

	//根据该用户权限设置
	BuddyInfo user = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(groupID, QString::number(userInfo.nUserID));
	if (user.nUserType == 0) //0是普通群员，1是管理员，9是群主。
	{
		ui->nameEdit->setReadOnly(true);
		ui->nameEdit->setStyleSheet("QLineEidt{background:transparent;border-width:0;border-style:outset}");

		ui->enEdit->setReadOnly(true);
		ui->enEdit->setStyleSheet("QLineEdit{background:transparent;border-width:0;border-style:outset}");

		ui->descEdit->setReadOnly(true);
		ui->descEdit->setStyleSheet("background:transparent;border-width:0;border-style:outset");

		ui->freeBtn->setEnabled(false);

		ui->addBtn->hide();
	}
	else
	{
		ui->nameEdit->setReadOnly(false);
		ui->nameEdit->setStyleSheet(ui_enEditStyleSheet);

		ui->enEdit->setReadOnly(false);
		ui->enEdit->setStyleSheet(ui_enEditStyleSheet);

		ui->descEdit->setReadOnly(false);
		ui->descEdit->setStyleSheet(ui_descEditStyleSheet);

		ui->freeBtn->setEnabled(true);

		ui->addBtn->show();
	}

	setManagerNote();

	//隐藏加群按钮。
	ui->joinBtn->hide();
	//隐藏加群申请提示标签。
	ui->tipLabel->hide();
}

void GroupProfileWidget::slotGroupChat()
{
	emit sigGroupChat(OpenGroup, QVariant::fromValue(groupID));

	if (this->parent() == NULL)
		this->hide();
}

bool GroupProfileWidget::eventFilter(QObject *obj, QEvent *e)
{
	if (obj == ui->topWidget)
	{
		if (e->type() == QEvent::MouseButtonPress)
		{
			QMouseEvent *event = (QMouseEvent *)e;
			mouse = event->pos();   //设置移动的原始位置。
		}
		if (e->type() == QEvent::MouseMove)
		{
			QMouseEvent *event = (QMouseEvent *)e;
			if (mouse.x() >= 0)
			{
				//首先通过做差值，获得鼠标位移的距离。
				int x = event->pos().x() - mouse.x();
				int y = event->pos().y() - mouse.y();
				//移动本窗体。
				this->move(this->x() + x, this->y() + y);
			}
		}
		if (e->type() == QEvent::MouseButtonRelease)
		{
			QMouseEvent *event = (QMouseEvent *)e;
			mouse.setX(-1);
		}
	}

	if (obj == ui->descEdit&&e->type() == QEvent::FocusOut)
	{
		slotEditDesc();
	}
	if (obj == ui->nameEdit&&e->type() == QEvent::FocusOut)
	{
		ui->nameEdit->setCursorPosition(0);
		ui->nameEdit->setToolTip(ui->nameEdit->text());
		slotEditName();
	}
	if (obj == ui->enEdit&&e->type() == QEvent::FocusOut)
	{
		ui->enEdit->setCursorPosition(0);
		ui->enEdit->setToolTip(ui->enEdit->text());
		slotEditKey();
	}

	if (obj == ui->noteEdit&&e->type() == QEvent::FocusOut)
	{
		slotEditNote();
	}


	if (e->type() == QEvent::MouseButtonPress)
	{
		if (obj == headerLabel && headerLabel->cursor().shape() == Qt::PointingHandCursor)
		{
			GroupInfo groupInfo = gDataBaseOpera->DBGetGroupFromID(groupID);
			avatarWidget->setGroupInfo(groupInfo);
			avatarWidget->show();
		}

		if (obj == this)
		{
			if (ui->descEdit->hasFocus())
				this->setFocus();

			if (ui->enEdit->hasFocus())
				this->setFocus();

			if (ui->noteEdit->hasFocus())
				this->setFocus();

			if (ui->nameEdit->hasFocus())
				this->setFocus();
		}
			
	}

	return QWidget::eventFilter(obj, e);
}

void GroupProfileWidget::slotUploadHeaderImage(QString filePath)
{
	headerImage.load(filePath);

	HttpNetWork::HttpUpLoadFile *upload = new HttpNetWork::HttpUpLoadFile;
	connect(upload, SIGNAL(sigUpLoadFinished(bool, QByteArray)), this, SLOT(slotHttpChangeAvatar(bool, QByteArray)));
	QVariantMap pargram;
	pargram.insert("parentId", "66662");
	pargram.insert("createUser", "6662");
	upload->StartUpLoadFile(gDataManager->getAppConfigInfo().PanServerUploadURL, filePath, pargram);
}

void GroupProfileWidget::slotHttpChangeAvatar(bool success, QByteArray byte)
{
	if (success)
	{
		QVariantMap map = QJsonDocument::fromJson(byte).toVariant().toMap();
		QString fileid = map["fileid"].toString();

		GroupInfo groupInfo = gDataBaseOpera->DBGetGroupFromID(groupID);

		//上传成功了，通知服务器登录用户的头像已经改变。
		QString avatarURL = gDataManager->getAppConfigInfo().PanServerDownloadURL + fileid + "/download";
		UserInfo userInfo = gDataManager->getUserInfo();
		QString url = gDataManager->getAppConfigInfo().MessageServerAddress +
			QString("/IMServer/group/updateGroup?userId=%1&passWord=%2&groupId=%3&groupName=%4&avatar=%5")
			.arg(userInfo.nUserID).arg(userInfo.strUserPWD).arg(groupInfo.groupId).arg(groupInfo.groupName).arg(avatarURL);

		HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
		connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotAvatarChanged(bool, QString)));
		http->getHttpRequest(url);
	}
}

void GroupProfileWidget::slotAvatarChanged(bool success, QString byte)
{
	avatarWidget->upPicFileFinished(success);
	if (success)
	{
		headerLabel->setPixmap(headerImage);
	}
}

void GroupProfileWidget::setAdjustText(QLabel *label, QString text)
{
	QFontMetrics fm(label->font());
	QRect rec = fm.boundingRect(text);

	if (rec.width() > label->width())
	{
		while (text.count() > 0)
		{
			//移除最后一个字符。
			text = text.left(text.count() - 1);

			QString tempString = text + QString("...");
			rec = fm.boundingRect(tempString);
			if (rec.width() <= label->width())
			{
				label->setText(tempString);
				return;
			}
		}
	}
	else
	{
		label->setText(text);
	}
}

QString GroupProfileWidget::getGroupID()
{
	return groupID;
}

void GroupProfileWidget::slotToggledFreeBtn()
{
	int groupType;
	if (ui->freeBtn->isChecked())
		groupType = 1;
	else
	{
		groupType = 0;
	}

	UserInfo userInfo = gDataManager->getUserInfo();
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
	QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/updateGroupType"
		+ QString("?userId=%1&passWord=%2&groupId=%3&groupType=%4").arg(userInfo.nUserID).arg(userInfo.strUserPWD).arg(groupID).arg(groupType);

	http->getHttpRequest(url);
}

void GroupProfileWidget::slotClickQRcodeBtn()
{
	QVariantMap data;
	data.insert("type", "addGroup");
	data.insert("userID", groupID);

	QByteArray bytes = QJsonDocument::fromVariant(data).toJson();
	QString string = bytes;

	QRenCodeShareLib qr;
	QImage image = qr.GenerateQRcode(string);
	QPixmap pixmap = QPixmap::fromImage(image);

	QPixmap base(200, 200);
	base.fill(Qt::white);
	pixmap = pixmap.scaled(160, 160);
	QPainter *painter = new QPainter;
	painter->begin(&base);
	painter->drawPixmap(20, 20, pixmap);
	painter->end();
	delete painter;

	QPoint pos = QWidget::mapToGlobal(this->pos());
	QRect deskRt = QApplication::desktop()->availableGeometry(pos);
//	ZoomImg *zoom = new ZoomImg();
//	zoom->OpenImg(base);
// 	zoom->move(deskRt.x() + (deskRt.width() - zoom->width()) / 2, deskRt.y() + (deskRt.height() - zoom->height()) / 2);
	GroupInfo group = gDataBaseOpera->DBGetGroupFromID(groupID);
	QPixmap pix(group.groupLoacalHeadImage);
	if (pix.isNull())
	{
		QByteArray bytearray = "";
		QFile file(group.groupLoacalHeadImage);
		if (file.open(QIODevice::ReadOnly) && file.size() != 0)
		{
			bytearray = file.readAll();
		}
		file.close();
		if (!pix.loadFromData(bytearray) || bytearray == "")
		{
			pix.load(":/GroupChat/Resources/groupchat/group.png");
		}
	}
 	QGroupQR * pQRWidget = new QGroupQR();
 	pQRWidget->OpenImg(base);
	pQRWidget->SetNickName(group.groupName);
	pQRWidget->OnSetPicPath(pix);
	pQRWidget->show();
}

void GroupProfileWidget::slotEditName()
{
	QString groupName = ui->nameEdit->text();

	UserInfo userInfo = gDataManager->getUserInfo();
	GroupInfo group = gDataBaseOpera->DBGetGroupFromID(groupID);

	if (groupName.isEmpty())
	{
		ui->nameEdit->setText(group.groupName);
		ui->nameEdit->setToolTip(group.groupName);
		ui->nameEdit->setCursorPosition(0);
	}
	else
	{
		if (groupName != group.groupName)
		{
			//将用户输入的部落名称上传服务器。
			QString url = gDataManager->getAppConfigInfo().MessageServerAddress +
				QString("/IMServer/group/updateGroup?userId=%1&passWord=%2&groupId=%3&groupName=%4&avatar=%5")
				.arg(userInfo.nUserID).arg(userInfo.strUserPWD).arg(group.groupId).arg(groupName).arg(group.groupHttpHeadImage);

			HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
			//connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotAvatarChanged(bool, QString)));
			http->getHttpRequest(url);

			group.groupName = groupName;
			gDataBaseOpera->DBUpdateGroupInfo(group);
		}
		ui->nameEdit->setToolTip(group.groupName);
		ui->nameEdit->setCursorPosition(0);
	}

	ui->numberLabel->setFocus();
}

void GroupProfileWidget::slotEditNote()
{
	QString note = ui->noteEdit->text();

	UserInfo userInfo = gDataManager->getUserInfo();
	BuddyInfo buddy = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(groupID, QString::number(userInfo.nUserID));

	if (note.isEmpty())
	{
		//用户输入为空，设置本群默认昵称。
		if (buddy.strNote.isEmpty())
			ui->noteEdit->setText(buddy.strNickName);
		else
			ui->noteEdit->setText(buddy.strNote);
	}
	else
	{
		//将用户输入的昵称上传服务器。
		HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
		QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/updateOwnerNoteInGroup"
			+ QString("?userId=%1&passWord=%2&groupId=%3&note=%4").arg(userInfo.nUserID).arg(userInfo.strUserPWD).arg(groupID).arg(note);

		http->getHttpRequest(url);

		buddy.strNote = note;
		gDataBaseOpera->DBInsertGroupBuddyInfo(groupID, buddy);
	}

	setManagerNote();
	ui->numberLabel->setFocus();
}

void GroupProfileWidget::slotEditKey()
{
	QString key = ui->enEdit->text();
	ui->enEdit->setToolTip(key);
	ui->enEdit->setCursorPosition(0);
	UserInfo userInfo = gDataManager->getUserInfo();
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
	QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/updateGroupKey"
		+ QString("?userId=%1&passWord=%2&groupId=%3&groupKey=%4").arg(userInfo.nUserID).arg(userInfo.strUserPWD).arg(groupID).arg(key);

	http->getHttpRequest(url);

	ui->numberLabel->setFocus();
}

void GroupProfileWidget::slotEditDesc()
{
	QString desc = ui->descEdit->toPlainText();

	UserInfo userInfo = gDataManager->getUserInfo();
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
	QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/updateGroupDesc"
		+ QString("?userId=%1&passWord=%2&groupId=%3&groupDesc=%4").arg(userInfo.nUserID).arg(userInfo.strUserPWD).arg(groupID).arg(desc);

	http->getHttpRequest(url);

	ui->numberLabel->setFocus();
}

void GroupProfileWidget::slotGroupAddBuddy()
{
	GroupAddBuddyWidget *addWidget = new GroupAddBuddyWidget;
	connect(addWidget, SIGNAL(sigTipMessage(int, QString, QString)), this, SIGNAL(sigTipMessage(int, QString, QString)));
	addWidget->setGroupID(groupID);
	addWidget->show();
}

void GroupProfileWidget::slotShare()
{
	emit sigShareID(OpenGroup, groupID);
	if (this->parent() == NULL)
		this->hide();
}

void GroupProfileWidget::slotApplyJoinGroup()
{
	UserInfo userInfo = gDataManager->getUserInfo();

	LeaveMessageWidget* MegWidget = new LeaveMessageWidget(this);
	MegWidget->OnInitGroupInfo(groupID);
	connect(MegWidget, SIGNAL(sigApplySuccessed()), this, SLOT(slotJoinSuccess()));
	connect(MegWidget, SIGNAL(sigApplyFailed()), this, SLOT(slotJoinFailed()));
	MegWidget->show();
}

void GroupProfileWidget::slotJoinSuccess()
{
	ui->tipLabel->setText(tr("Apply for group success"));
	ui->tipLabel->show();
}

void GroupProfileWidget::slotJoinFailed()
{
	ui->tipLabel->setText(tr("Applying for a group failure"));
	ui->tipLabel->show();
}

void GroupProfileWidget::setManagerNote()
{
	//设置群主/管理员
	QString strManagers;
	QList<BuddyInfo> buddyList = gDataBaseOpera->DBGetGroupBuddyInfoFromID(groupID);
	foreach(BuddyInfo buddy, buddyList)
	{
		if (buddy.nUserType > 0)
		{
			if (!strManagers.isEmpty())
				strManagers.append(tr("、"));

			if (!buddy.strNote.isEmpty())
			{
				strManagers.append(buddy.strNote);
			}
			else
			{
				strManagers.append(buddy.strNickName);
			}
		}
	}

	setAdjustText(ui->managerLabel, strManagers);
}

void GroupProfileWidget::closeEvent(QCloseEvent * event)
{
	sigCloseProfile();
	//QWidget::closeEvent(event);
}