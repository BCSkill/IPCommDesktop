﻿#include "imdownloadheaderimg.h"
#include <QFile>
#include "immainwidget.h"

IMDownLoadHeaderImg::IMDownLoadHeaderImg(QObject *parent)
	: QObject(parent)
{
	
}

IMDownLoadHeaderImg::~IMDownLoadHeaderImg()
{

}

//Qualifier: 传入路径，返回QPixmap
QPixmap IMDownLoadHeaderImg::GetImagePix(QString strPath)
{
	QFile file(strPath);
    QPixmap pix;
	if (file.open(QIODevice::ReadOnly))
	{
		QByteArray byteArray = file.readAll();
		file.close();
		pix.loadFromData(byteArray);
	}
	return pix;
}

//Qualifier: 判断是否有图像文件
bool IMDownLoadHeaderImg::IsImageFile(QString localPath)
{
	QFile file(localPath);
	if (file.exists() && file.size() != 0)
	{
		return true;
	}
	return false;
}

//开始下载用户头像
void IMDownLoadHeaderImg::StartDownLoadUserInfoHeaderImage(UserInfo userInfo)
{
	HttpNetWork::HttpDownLoadFile *NetWork = new HttpNetWork::HttpDownLoadFile;
	connect(NetWork, SIGNAL(sigDownFinished(bool)), this, SLOT(slotDownUserPicFinished(bool)));

	//如果该头像已经存在并且有效，那么将新头像重命名为temp结尾的，如果新头像有效再替换旧头像。
	QFile file(userInfo.strUserAvatarLocal);
	if (file.exists() && file.size() > 0)
	{
		userInfo.strUserAvatarLocal.chop(4);  //删除.jpg
		userInfo.strUserAvatarLocal.append("temp.jpg");
	}

	NetWork->setData(QVariant::fromValue(userInfo));
	NetWork->StartDownLoadFile(userInfo.strUserAvatarHttp, userInfo.strUserAvatarLocal);
}

bool IMDownLoadHeaderImg::IsValidImage(QString imgPath)
{
	QFile file(imgPath);
	if (file.open(QIODevice::ReadOnly))
	{
		QByteArray byteArray = file.readAll();
		file.close();
		QImage image = QImage::fromData(byteArray);

		if (image.isNull())
		{
			return false;
		}
	}
	else
	{
		return false;
	}

	return true;
}

//下载用户头像结束
void IMDownLoadHeaderImg::slotDownUserPicFinished(bool bResult)
{
	HttpNetWork::HttpDownLoadFile *requestHttp = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());
	if (requestHttp)
	{
		QVariant var = requestHttp->getData();
		UserInfo userInfo = var.value<UserInfo>();

		if (userInfo.strUserAvatarLocal.endsWith("temp.jpg"))
		{
			QFile tempFile(userInfo.strUserAvatarLocal);
			if (tempFile.exists() && tempFile.size() > 0)
			{
				//保存temp文件的路径。
				QString oldName = userInfo.strUserAvatarLocal;
				//将userInfo中的路径设置为正常路径。
				userInfo.strUserAvatarLocal.chop(8);  //删除temp.jpg
				userInfo.strUserAvatarLocal.append(".jpg");

				//删除正常路径的图片，并用temp文件重命名进行替换。
				/*QFile::remove(userInfo.strUserAvatarLocal);
				QFile::rename(oldName, userInfo.strUserAvatarLocal);*/

				QFile::remove(userInfo.strUserAvatarLocal);
				tempFile.copy(userInfo.strUserAvatarLocal);
				QFile::remove(oldName);
			}
		}
		/*else
		{
			//将userInfo中的路径设置为正常路径。
			userInfo.strUserAvatarLocal.chop(8);  //删除temp.jpg
			userInfo.strUserAvatarLocal.append(".jpg");
		}*/

        QPixmap pix;
		QString imagePath;

		if (bResult)
		{
			imagePath = userInfo.strUserAvatarLocal;
			pix = GetImagePix(userInfo.strUserAvatarLocal);
			if (pix.isNull())
			{
				imagePath = userInfo.strUserDefaultAvatar;
				pix = GetImagePix(userInfo.strUserDefaultAvatar);
			}
		}
		else
		{
			imagePath = userInfo.strUserDefaultAvatar;
			pix = GetImagePix(userInfo.strUserDefaultAvatar);
		}

		if (!IsValidImage(imagePath))
		{
			imagePath = QStringLiteral(":/PerChat/Resources/person/temp.png");
		}

		emit sigUpdateBuddyHeaderImage(userInfo.nUserID, pix);
		emit sigUpdateBuddyHeaderImagePath(userInfo.nUserID, imagePath);
		if (IMMainWidget::self())
		{
			emit IMMainWidget::self()->sigUpdateBuddyHeaderImagePath(userInfo.nUserID, imagePath);
		}
		requestHttp->deleteLater();
	}

	this->deleteLater();
}

//开始下载图像
void IMDownLoadHeaderImg::StartDownLoadBuddyeHeaderImage(BuddyInfo buddyInfo)
{
	HttpNetWork::HttpDownLoadFile *NetWork = new HttpNetWork::HttpDownLoadFile;
	connect(NetWork, SIGNAL(sigDownFinished(bool)), this, SLOT(slotDownBuddyPicFinished(bool)));

	//如果该头像已经存在并且有效，那么将新头像重命名为temp结尾的，如果新头像有效再替换旧头像。
	QFile file(buddyInfo.strLocalAvatar);
	if (file.exists() && file.size() > 0)
	{
		buddyInfo.strLocalAvatar.chop(4);  //删除.jpg
		buddyInfo.strLocalAvatar.append("temp.jpg");
	}

	NetWork->setData(QVariant::fromValue(buddyInfo));
	NetWork->StartDownLoadFile(buddyInfo.strHttpAvatar, buddyInfo.strLocalAvatar);
}

//下载图像结束
void IMDownLoadHeaderImg::slotDownBuddyPicFinished(bool bResult)
{
	HttpNetWork::HttpDownLoadFile *requestHttp = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());
	if (requestHttp)
	{
		QVariant var = requestHttp->getData();
		BuddyInfo buddyInfo = var.value<BuddyInfo>();

		if (buddyInfo.strLocalAvatar.endsWith("temp.jpg"))
		{
			QFile tempFile(buddyInfo.strLocalAvatar);
			if (tempFile.exists() && tempFile.size() > 0)
			{
				//保存temp文件的路径。
				QString oldName = buddyInfo.strLocalAvatar;
				//将buddyInfo中的路径设置为正常路径。
				buddyInfo.strLocalAvatar.chop(8);  //删除temp.jpg
				buddyInfo.strLocalAvatar.append(".jpg");
				
				//删除正常路径的图片，并用temp文件重命名进行替换。
				QFile::remove(buddyInfo.strLocalAvatar);
				tempFile.copy(buddyInfo.strLocalAvatar);
				QFile::remove(oldName);
				//QFile::rename(oldName, buddyInfo.strLocalAvatar);
			}
		}
		/*else
		{
			//将buddyInfo中的路径设置为正常路径。
			buddyInfo.strLocalAvatar.chop(8);  //删除temp.jpg
			buddyInfo.strLocalAvatar.append(".jpg");
		}*/

		QPixmap pix;
		QString imagePath;


		imagePath = buddyInfo.strLocalAvatar;
		pix = GetImagePix(buddyInfo.strLocalAvatar);
		if (pix.isNull())
		{
			imagePath = buddyInfo.strDefaultAvatar;
			pix = GetImagePix(buddyInfo.strDefaultAvatar);
		}

		if (!IsValidImage(imagePath))
		{
			imagePath = QStringLiteral(":/PerChat/Resources/person/temp.png");
		}

		emit sigUpdateBuddyHeaderImage(buddyInfo.nUserId,pix);
		emit sigUpdateBuddyHeaderImagePath(buddyInfo.nUserId, imagePath);
		if (IMMainWidget::self())
		{
			emit IMMainWidget::self()->sigUpdateBuddyHeaderImagePath(buddyInfo.nUserId, imagePath);
		}
		requestHttp->deleteLater();
		requestHttp = NULL;
	}
	this->deleteLater();

}

//下载部落头像
void IMDownLoadHeaderImg::StartDownLoadGroupHeaderImage(GroupInfo groupInfo)
{
	HttpNetWork::HttpDownLoadFile *NetWork = new HttpNetWork::HttpDownLoadFile;
	connect(NetWork, SIGNAL(sigDownFinished(bool)), this, SLOT(slotDownGroupPicFinished(bool)));

	//如果该头像已经存在并且有效，那么将新头像重命名为temp结尾的，如果新头像有效再替换旧头像。
	QFile file(groupInfo.groupLoacalHeadImage);
	if (file.exists() && file.size() > 0)
	{
		groupInfo.groupLoacalHeadImage.chop(4);  //删除.jpg
		groupInfo.groupLoacalHeadImage.append("temp.jpg");
	}

	NetWork->setData(QVariant::fromValue(groupInfo));
	NetWork->StartDownLoadFile(groupInfo.groupHttpHeadImage, groupInfo.groupLoacalHeadImage);
}

//下载部落图片结束
void IMDownLoadHeaderImg::slotDownGroupPicFinished(bool bResult)
{
	HttpNetWork::HttpDownLoadFile *requestHttp = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());
	if (requestHttp)
	{
		QVariant var = requestHttp->getData();
		GroupInfo groupInfo = var.value<GroupInfo>();

		if (groupInfo.groupLoacalHeadImage.endsWith("temp.jpg"))
		{
			QFile tempFile(groupInfo.groupLoacalHeadImage);
			if (tempFile.exists() && tempFile.size() > 0)
			{
				//保存temp文件的路径。
				QString oldName = groupInfo.groupLoacalHeadImage;
				//将groupInfo中的路径设置为正常路径。
				groupInfo.groupLoacalHeadImage.chop(8);  //删除temp.jpg
				groupInfo.groupLoacalHeadImage.append(".jpg");

				//删除正常路径的图片，并用temp文件重命名进行替换。
				/*QFile::remove(groupInfo.groupLoacalHeadImage);
				QFile::rename(oldName, groupInfo.groupLoacalHeadImage);*/

				QFile::remove(groupInfo.groupLoacalHeadImage);
				tempFile.copy(groupInfo.groupLoacalHeadImage);
				QFile::remove(oldName);
			}
			else
			{
				//将groupInfo中的路径设置为正常路径。
				groupInfo.groupLoacalHeadImage.chop(8);  //删除temp.jpg
				groupInfo.groupLoacalHeadImage.append(".jpg");
			}
		}

		QPixmap pix;
		QString imagePath;

		imagePath = groupInfo.groupLoacalHeadImage;
		pix = GetImagePix(groupInfo.groupLoacalHeadImage);
		if (pix.isNull())
		{
			imagePath = groupInfo.groupDefaultAvatar;
			pix = GetImagePix(groupInfo.groupDefaultAvatar);
		}

		if (!IsValidImage(imagePath))
		{
			imagePath = QStringLiteral(":/GroupChat/Resources/groupchat/group.png");
		}

		emit sigUpdateBuddyHeaderImage(groupInfo.groupId.toInt(), pix);
		emit sigUpdateBuddyHeaderImagePath(groupInfo.groupId.toInt(), imagePath);
		if (IMMainWidget::self())
		{
			emit IMMainWidget::self()->sigUpdateBuddyHeaderImagePath(groupInfo.groupId.toInt(), imagePath);
		}
		requestHttp->deleteLater();
		requestHttp = NULL;
	}

	this->deleteLater();
}
