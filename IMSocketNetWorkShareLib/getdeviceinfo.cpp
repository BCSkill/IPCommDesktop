﻿#include "getdeviceinfo.h"
#include <QNetworkInterface>
#include <QHostInfo>
#ifdef Q_OS_WIN
#include <process.h>
#else
#include <unistd.h>
#include <QProcess>
#endif

GetDeviceInfo::GetDeviceInfo(QObject *parent)
	: QObject(parent)
{

}

GetDeviceInfo::~GetDeviceInfo()
{

}

//获取设备类型
QString GetDeviceInfo::GetDeviceType()
{
	//1、ios 2、android 3、win 4、mac 5、ws  设备类型
#ifdef Q_OS_WIN
	return "win";
#endif
#ifdef Q_OS_MAC
	return "mac";
#endif
#ifdef Q_OS_LINUX
    return "linux";
#endif
}

//设备分类
QString GetDeviceInfo::GetDeviceClass()
{
	return "pc";
}

//获取设备版本
QString GetDeviceInfo::GetDeviceVersion()
{
	return "v1.0";
}

//获取设备厂商
QString GetDeviceInfo::GetDeviceManufacturer()
{
#ifdef Q_OS_WIN
	return "lenovo";
#else
    return "apple";
#endif
}

//获取设备型号
QString GetDeviceInfo::GetDeviceModel()
{
#ifdef Q_OS_WIN
    return QHostInfo::localHostName();
#endif
#ifdef Q_OS_MAC
    QProcess p;
    p.start("scutil --get ComputerName");
    p.waitForFinished();
    return QString::fromLocal8Bit(p.readAllStandardOutput()).trimmed();
#endif
#ifdef Q_OS_LINUX
    QProcess p;
    p.start("hostname");
    p.waitForFinished();
    return QString::fromLocal8Bit(p.readAllStandardOutput()).trimmed();
#endif
    //return machineName;
    //return "E431";
}

//获取应用版本号
QString GetDeviceInfo::GetAppVersion()
{
	return "V1.0";
}

//获取设备ID
QString GetDeviceInfo::GetDeviceID()
{
	QString strMacName = GetCurrentMacAddress();
	
	int iPid = (int)getpid();

	strMacName = strMacName + "-" + QString::number(iPid);
	return strMacName;
}

//获取当前mac地址
QString GetDeviceInfo::GetCurrentMacAddress()
{
	QList<QNetworkInterface> nets = QNetworkInterface::allInterfaces();// 获取所有网络接口列表
	int nCnt = nets.count();
	QString strMacAddr = "";
	for (int i = 0; i < nCnt; i++)
	{
		// 如果此网络接口被激活并且正在运行并且不是回环地址，则就是我们需要找的Mac地址
		if (nets[i].flags().testFlag(QNetworkInterface::IsUp) && nets[i].flags().testFlag(QNetworkInterface::IsRunning) && !nets[i].flags().testFlag(QNetworkInterface::IsLoopBack))
		{
			strMacAddr = nets[i].hardwareAddress();
			break;
		}
	}
	return strMacAddr;
}
