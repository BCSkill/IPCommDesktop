<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="th_TH">
<context>
    <name>IMSocketMessageInfo</name>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="86"/>
        <source>Login request failed!</source>
        <translation>คำขอเข้าสู่ระบบล้มเหลว!</translation>
    </message>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="91"/>
        <location filename="imsocketmessageinfo.cpp" line="96"/>
        <source>Requesting socket information failed!</source>
        <translation>การขอข้อมูลซ็อกเก็ตล้มเหลว!</translation>
    </message>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="820"/>
        <source>This message type is not supported at this time. Please check it on the mobile side</source>
        <translation>ประเภทข้อความนี้ไม่รองรับในขณะนี้กรุณาตรวจสอบในด้านมือถือ</translation>
    </message>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="893"/>
        <source>[Lightspeed SMS] Please check on the mobile side</source>
        <translation>[ข้อความความเร็วแสง]โปรดตรวจสอบทางมือถือ</translation>
    </message>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="1067"/>
        <source>Disconnected from the server and is reconnecting!</source>
        <translation>ตัดการเชื่อมต่อจากเซิร์ฟเวอร์และกำลังเชื่อมต่อใหม่!</translation>
    </message>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="1097"/>
        <source>Start the timer and reconnect!</source>
        <translation>เริ่มจับเวลาและเชื่อมต่อใหม่!</translation>
    </message>
</context>
<context>
    <name>IMSocketNetWorkShareLib</name>
    <message>
        <location filename="imsocketnetworksharelib.cpp" line="203"/>
        <source>未收到服务器心跳:当前时间：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imsocketnetworksharelib.cpp" line="203"/>
        <source>心跳包时间: </source>
        <translation></translation>
    </message>
</context>
</TS>
