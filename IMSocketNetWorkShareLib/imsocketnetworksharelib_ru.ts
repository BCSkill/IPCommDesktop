<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>IMSocketMessageInfo</name>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="86"/>
        <source>Login request failed!</source>
        <translation>Запрос на вход не выполнен!</translation>
    </message>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="91"/>
        <location filename="imsocketmessageinfo.cpp" line="96"/>
        <source>Requesting socket information failed!</source>
        <translation>Не удалось запросить информацию о сокете!</translation>
    </message>
    <message>
        <source>fileid</source>
        <translation type="vanished">FILEID</translation>
    </message>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="820"/>
        <source>This message type is not supported at this time. Please check it on the mobile side</source>
        <translation>Этот тип сообщения не поддерживается в настоящее время. Пожалуйста, проверьте это на мобильной стороне</translation>
    </message>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="893"/>
        <source>[Lightspeed SMS] Please check on the mobile side</source>
        <translation>[Lightspeed SMS] Пожалуйста, проверьте на мобильной стороне</translation>
    </message>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="1067"/>
        <source>Disconnected from the server and is reconnecting!</source>
        <translation>Отключен от сервера и снова подключается!</translation>
    </message>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="1097"/>
        <source>Start the timer and reconnect!</source>
        <translation>Запустите таймер и переподключите!</translation>
    </message>
</context>
<context>
    <name>IMSocketNetWorkShareLib</name>
    <message>
        <location filename="imsocketnetworksharelib.cpp" line="203"/>
        <source>未收到服务器心跳:当前时间：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="imsocketnetworksharelib.cpp" line="203"/>
        <source>心跳包时间: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
