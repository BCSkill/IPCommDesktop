<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>IMSocketMessageInfo</name>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="86"/>
        <source>Login request failed!</source>
        <translation>Solicitud de inicio de sesión fallida!</translation>
    </message>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="91"/>
        <location filename="imsocketmessageinfo.cpp" line="96"/>
        <source>Requesting socket information failed!</source>
        <translation>La solicitud de información de socket falló!</translation>
    </message>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="820"/>
        <source>This message type is not supported at this time. Please check it on the mobile side</source>
        <translation>Este tipo de mensaje no es compatible en este momento. Por favor, compruebe en el lado móvil</translation>
    </message>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="893"/>
        <source>[Lightspeed SMS] Please check on the mobile side</source>
        <translation>[Lightspeed SMS] Por favor, compruebe en el lado móvil</translation>
    </message>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="1067"/>
        <source>Disconnected from the server and is reconnecting!</source>
        <translation>Desconectado del servidor y se está reconectando!</translation>
    </message>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="1097"/>
        <source>Start the timer and reconnect!</source>
        <translation>¡Arranca el temporizador y vuelve a conectarte!</translation>
    </message>
</context>
<context>
    <name>IMSocketNetWorkShareLib</name>
    <message>
        <location filename="imsocketnetworksharelib.cpp" line="203"/>
        <source>未收到服务器心跳:当前时间：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imsocketnetworksharelib.cpp" line="203"/>
        <source>心跳包时间: </source>
        <translation></translation>
    </message>
</context>
</TS>
