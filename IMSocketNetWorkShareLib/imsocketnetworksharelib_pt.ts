<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>IMSocketMessageInfo</name>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="86"/>
        <source>Login request failed!</source>
        <translation>Solicitação de login falhou!</translation>
    </message>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="91"/>
        <location filename="imsocketmessageinfo.cpp" line="96"/>
        <source>Requesting socket information failed!</source>
        <translation>Solicitando informações de soquete falhou!</translation>
    </message>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="820"/>
        <source>This message type is not supported at this time. Please check it on the mobile side</source>
        <translation>Este tipo de mensagem não é suportado neste momento. Por favor, verifique no lado móvel</translation>
    </message>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="893"/>
        <source>[Lightspeed SMS] Please check on the mobile side</source>
        <translation>[Lightspeed SMS] Por favor, verifique no lado móvel</translation>
    </message>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="1067"/>
        <source>Disconnected from the server and is reconnecting!</source>
        <translation>Desconectado do servidor e está se reconectando!</translation>
    </message>
    <message>
        <location filename="imsocketmessageinfo.cpp" line="1097"/>
        <source>Start the timer and reconnect!</source>
        <translation>Inicie o temporizador e volte a ligar!</translation>
    </message>
</context>
<context>
    <name>IMSocketNetWorkShareLib</name>
    <message>
        <location filename="imsocketnetworksharelib.cpp" line="203"/>
        <source>未收到服务器心跳:当前时间：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="imsocketnetworksharelib.cpp" line="203"/>
        <source>心跳包时间: </source>
        <translation></translation>
    </message>
</context>
</TS>
