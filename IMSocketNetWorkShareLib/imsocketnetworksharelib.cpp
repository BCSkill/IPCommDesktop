﻿#include "imsocketnetworksharelib.h"
#include <QDateTime>
#include "QStringLiteralBak.h"

//#define SOCKET_MESSAGE_DEBUG_ENABLE //是否记录消息内容

IMSocketNetWorkShareLib::IMSocketNetWorkShareLib(QObject *parent) : QObject(parent), mSocketClient(NULL),mHeartTime(NULL),mMonitorServerHeart(NULL), timer_send(NULL)
{
	mSocketMessage = "";
	mbReConnect = true;         //是否重连
}

IMSocketNetWorkShareLib::~IMSocketNetWorkShareLib()
{
	if (mSocketClient)
	{
		mSocketClient->DisConnectSocket();
		delete mSocketClient;
		mSocketClient = NULL;
	}
	void StopHeartBeat();
	void StopMonitorServerHeartTimer();
}

bool IMSocketNetWorkShareLib::OnConnectSocket(QString strServerIP, QString strServerPort, QString strToken, QString strLastMessage)
{
	if (mSocketClient == NULL)
	{
		mSocketClient = new SocketNetWorkShareLib();
		connect(mSocketClient, SIGNAL(sigSendReadData(QByteArray)), this, SLOT(slotRevServerMsg(QByteArray)));
		connect(mSocketClient, SIGNAL(sigDisConnectServer()), this, SLOT(slotDisConnectServer()));
	}

	if (mSocketClient->ConnectServer(strServerIP, strServerPort.toInt()))
	{
		sendTocken(strLastMessage, strToken);//发送tocket值
		StartHeartBeat();	//启动心跳包
		StartMonitorServerHeartTimer();//开始检测心跳
		OnStart();
		return true;
	}
	return false;
}

//发送消息
MessageInfo IMSocketNetWorkShareLib::SendMessage(int nFromUserID, int nToUserID, int nDeliverType, short MessageChildType, int nMessageState, QString strMsg)
{
	MessageInfo messageInfo;
	messageInfo.version = 1;                           //版本号
	messageInfo.nFromUserID = nFromUserID;              //userID
	messageInfo.nToUserID = nToUserID;                  //toUserID
	qlonglong clientTime = QDateTime::currentDateTime().toTime_t();  //客户端时间
	messageInfo.ClientTime = clientTime * 1000;  //客户端时间
	// qDebug()<<"客户端时间:"<<messageInfo.ClientTime;
	messageInfo.ServerTime = 0;                         //服务器时间
	QTime time = QTime::currentTime();
	qsrand(time.msec() + time.second() * 1000);
	messageInfo.nMsgOrder = qrand() % 1000;         //msgOrder
	messageInfo.MsgDeliverType = (BYTE)nDeliverType;   //投递类型
	messageInfo.MessageChildType = MessageChildType;                  //消息子类型 0文本，1图片2音频 3视频4用户自定义
	messageInfo.strMsg = strMsg;                       //消息内容
	messageInfo.isHavePMsgId = (BYTE)0;                 //是否有MsgID
	messageInfo.nUserID = 0;//pUserID

	messageInfo.PerByte[0] = (BYTE)0;              //离线消息
	messageInfo.PerByte[1] = (BYTE)0;              //积分大消息类型
	messageInfo.PerByte[2] = (BYTE)0;              //积分小消息类型
	messageInfo.PerByte[3] = (BYTE)0;               
	messageInfo.PerByte[4] = (BYTE)0;
	messageInfo.PerByte[5] = (BYTE)0;
	messageInfo.PerByte[6] = (BYTE)0;
	messageInfo.PerByte[7] = (BYTE)0;
	messageInfo.PerByte[8] = (BYTE)0;
	messageInfo.PerByte[9] = (BYTE)0;

	messageInfo.MessageState = nMessageState;

	//生成MSGID
	messageInfo.msgID = CreateMsgID(messageInfo.nFromUserID, messageInfo.nToUserID, messageInfo.ClientTime, messageInfo.nMsgOrder, messageInfo.MsgDeliverType);

	//qDebug() << "发送的 " << messageInfo.msgID;
	messageInfo.SendTime = 0; //设置发送时间为0；
	messageInfo.integral = 0;
	messageInfo.bIsSend = false;
	if (MessageChildType != 102)
	{
		OnInsertMessage(messageInfo, nToUserID);
	}
	else
	{
		SendQByteArrayMsg(messageInfo);
	}
	return messageInfo;
}

void IMSocketNetWorkShareLib::ChangeMessageInfo(QByteArray msgID, QString strMsg)
{
	int nTemp = 0;
	QMap<QString, QList<MessageInfo> >::iterator itor = mMapMessageInfo.begin();
	for (; itor != mMapMessageInfo.end(); ++itor)
	{
		for (int i = 0; i < itor.value().size(); i++)
		{
			if (itor.value()[i].msgID == msgID)
			{
				nTemp = 1;
				itor.value()[i].strMsg = strMsg;
				break;
			}
		}
		if (nTemp == 1)
		{
			break;
		}
	}
}

//发送token
void IMSocketNetWorkShareLib::sendTocken(QString strMessageLastTime,QString strToken)
{
	mServerToken = strToken + "&lastMsgTime=" + strMessageLastTime;
	QTimer::singleShot(500, this, SLOT(slotSendToken()));
}

//定时器发送token
void IMSocketNetWorkShareLib::slotSendToken()
{
	if (mSocketClient)
	{
		QByteArray dstByteArray("");
		QDataStream writeToByteArray(&dstByteArray, QIODevice::ReadWrite);
		writeToByteArray << (BYTE)1;
		writeToByteArray << mServerToken.toUtf8();
		mSocketClient->SendMsg(dstByteArray);
	}
}

//心跳包
void IMSocketNetWorkShareLib::StartHeartBeat()
{
	if (mHeartTime == NULL)
	{
		mHeartTime = new QTimer(this);
		connect(mHeartTime, SIGNAL(timeout()), this, SLOT(slotSendHeartData()));
		mHeartTime->start(TIMER_HEARTBEAT);
	}
}

//停止心跳
void IMSocketNetWorkShareLib::StopHeartBeat()
{
	if (mHeartTime != NULL)
	{
		if (mHeartTime->isActive())
		{
			mHeartTime->stop();
		}
		delete mHeartTime;
		mHeartTime = NULL;
	}
}

//启动检测心跳包定时器
void IMSocketNetWorkShareLib::StartMonitorServerHeartTimer()
{
	if (mMonitorServerHeart == NULL)
	{
		mMonitorServerHeart = new QTimer(this);
		mServerHeartTime = QDateTime::currentDateTime().toTime_t();
		connect(mMonitorServerHeart, SIGNAL(timeout()), this, SLOT(slotMonitorServerHeart()));
	}
	if (!mMonitorServerHeart->isActive())
	{
		mMonitorServerHeart->start(10000);
	}
}

//停止检测心跳包
void IMSocketNetWorkShareLib::StopMonitorServerHeartTimer()
{
	if (mMonitorServerHeart != NULL)
	{
		if (mMonitorServerHeart->isActive())
		{
			mMonitorServerHeart->stop();
		}
		delete mMonitorServerHeart;
		mMonitorServerHeart = NULL;
	}
}

//检测心跳包时间
void IMSocketNetWorkShareLib::slotMonitorServerHeart()
{
	qlonglong currentTime = QDateTime::currentDateTime().toTime_t();
	int nTime = currentTime - mServerHeartTime;
    if (nTime >= 20)
	{
		StopHeartBeat();
		StopMonitorServerHeartTimer();
		if (mSocketClient)
		{
			qDebug() << tr("未收到服务器心跳:当前时间：") << currentTime << tr("心跳包时间: ") << mServerHeartTime;
			mSocketClient->DisConnectSocket();
		}
	}
}

//发送心跳
void IMSocketNetWorkShareLib::slotSendHeartData()
{
	if (mSocketClient)
	{
		QByteArray dstByteArray("");
		QDataStream writeToByteArray(&dstByteArray, QIODevice::ReadWrite);
		writeToByteArray << (BYTE)0;
		mSocketClient->SendMsg(dstByteArray);
	}
}

// Qualifier: 处理接收到的数据
void IMSocketNetWorkShareLib::slotRevServerMsg(QByteArray buff)
{
	mSocketMessage.append(buff);
	while (mSocketMessage.size() != 0)
	{
		/*判断消息类型*/
		QByteArray msgType = mSocketMessage.mid(0, 1);
		int nType = msgType.toHex().toInt();
		if (nType == 0)
		{
			OnDealServerHeart();                       //处理心跳消息
			mSocketMessage = mSocketMessage.mid(1, mSocketMessage.length());
		}
		else
		{
			/*先截取消息长度*/
			QByteArray msgByteArray = mSocketMessage.mid(1, mSocketMessage.length());
			QDataStream messageRecive(&msgByteArray, QIODevice::ReadWrite);
			int nMsgLength = 0;                  //总消息长度
			messageRecive >> nMsgLength;
			if (mSocketMessage.size() >= nMsgLength)
			{
				QByteArray msgArray = mSocketMessage.mid(1, nMsgLength + 4);
				if (nType == 2)
				{
					onDealMessageInfo(msgArray);               //消息
				}
				else if (nType == 3)
				{
					onDealReceiptMessage(msgArray);             //回执
				}
				mSocketMessage = mSocketMessage.mid(msgArray.size() + 1, mSocketMessage.size());
			}
			else
			{
				break;
			}
		}
	}
}

/*处理来自服务器的心跳*/
void IMSocketNetWorkShareLib::OnDealServerHeart()
{
        //qDebug() << "接收心跳,时间: " << mServerHeartTime;
	mServerHeartTime = QDateTime::currentDateTime().toTime_t();
}

//处理消息信息
void IMSocketNetWorkShareLib::onDealMessageInfo(QByteArray byteArray)
{
	// qDebug()<<byteArray;
	QByteArray msgByteArray = byteArray.mid(0, byteArray.length());
	QDataStream messageRecive(&msgByteArray, QIODevice::ReadWrite);
	int nMsgLength;                  //总消息长度
	messageRecive >> nMsgLength;
	/************版本号***********/
	MessageInfo messageInfo;
	messageRecive >> messageInfo.version;
	// qDebug()<<"版本号 "<<messageInfo.version;
	/************发送者***********/
	messageRecive >> messageInfo.nFromUserID;
	//  qDebug()<<"发送者 "<<messageInfo.nFromUserID;
	/************接收者***********/
	messageRecive >> messageInfo.nToUserID;
	//  qDebug()<<"接收者 "<<messageInfo.nToUserID;
	/************客户端时间***********/
	messageRecive >> messageInfo.ClientTime;
	// qDebug()<<"客户端时间 "<<messageInfo.ClientTime;
	/************服务器时间***********/
	messageRecive >> messageInfo.ServerTime;
	//  qDebug()<<"服务器时间 "<<messageInfo.ServerTime;
	/************MsgOrder***********/
	messageRecive >> messageInfo.nMsgOrder;
	//  qDebug()<<"MsgOrder "<<messageInfo.nMsgOrder;
	/************投递类型***********/
	messageRecive >> messageInfo.MsgDeliverType;
	// qDebug()<<"投递类型 "<<messageInfo.MsgDeliverType;
	/************消息子类型***********/
	messageRecive >> messageInfo.MessageChildType;
	//   qDebug()<<"消息子类型 "<<messageInfo.MessageChildType;
	/************消息内容及长度***********/
	int msgContentLength;
	messageRecive >> msgContentLength;
	QByteArray msgContent = byteArray.mid(41, msgContentLength);
	messageInfo.strMsg = QString::fromStdString(msgContent.toStdString());
	
	//首先发送消息回执
	QByteArray byteArrayMsgRe = CreateMsgID(messageInfo.nFromUserID,
		messageInfo.nToUserID, messageInfo.ClientTime,
		messageInfo.nMsgOrder, messageInfo.MsgDeliverType);
	SendReceiptMessage(byteArrayMsgRe);
	messageInfo.msgID = byteArrayMsgRe;

#ifdef SOCKET_MESSAGE_DEBUG_ENABLE
	qDebug().noquote() << "<<================msgID=" << messageInfo.msgID << ",MessageChildType=" << messageInfo.MessageChildType << ",strMsg=" << messageInfo.strMsg;
#endif

	QByteArray arry = byteArray.mid(41 + msgContentLength);
	QDataStream msgRev(&arry, QIODevice::ReadWrite);

	/*是否有PmsgID*/
	msgRev >> messageInfo.isHavePMsgId;
	if (messageInfo.isHavePMsgId == 0) //没有
	{
		msgRev >> messageInfo.nUserID;
		msgRev >> messageInfo.offLineFlag;    //离线消息标志
		msgRev >> messageInfo.interalTypeMain;//积分主类型
		msgRev >> messageInfo.interalTypeVice;//积分副类型
		msgRev >> messageInfo.integral;      //积分
	}

	/*判断消息是否重复*/
	if (!OnJudgeMessageRepet(byteArrayMsgRe))
	{
		switch (messageInfo.MessageChildType)
		{
		case Message_SYSTEM:  //系统消息
			onDealRevSystemMessage(messageInfo);
			break;
		case Message_RECEIPT:               //系统已送达
			SetMessageInfo(msgContent,messageInfo);
			SetMessageState(msgContent, MESSAGE_STATE_RECEIVE);
			break;
		case Message_VIEW:               //消息已查看
			SetMessageInfo(msgContent,messageInfo);
			SetMessageState(msgContent, MESSAGE_STATE_READ);
			break;
		default:    // 接收到消息(文字、图片....)
			emit sigRevServerMessage(messageInfo);
			break;
		}
	}
}

/*获取msgID*/
QByteArray IMSocketNetWorkShareLib::CreateMsgID(int nFromID, int nToId, unsigned long long msgTime, int nMsgOrder, BYTE msgPost)
{
	QByteArray byteMsgID = "";
	QDataStream writeToByteArray(&byteMsgID, QIODevice::ReadWrite);
	writeToByteArray << nFromID;
	writeToByteArray << nToId;
	writeToByteArray << msgTime;
	writeToByteArray << nMsgOrder;
	writeToByteArray << msgPost;
	return byteMsgID.toHex().toUpper();
}

/*发送回执消息*/
void IMSocketNetWorkShareLib::SendReceiptMessage(QByteArray ReceiptMessage)
{
	// qDebug()<<"MSGID "<<ReceiptMessage;
	QByteArray msgArray = "";
	QDataStream msgHeaderStream(&msgArray, QIODevice::ReadWrite);
	msgHeaderStream << (BYTE)3;

	QByteArray msgContent = "";
	QDataStream msgContentStream(&msgContent, QIODevice::ReadWrite);
	msgContentStream << (BYTE)0;
	unsigned long long clientTime = QDateTime::currentDateTime().toTime_t();
	msgContentStream << clientTime;

	QByteArray ByteArray = ReceiptMessage + msgContent;
	msgHeaderStream << ByteArray;

	if (mSocketClient)
		mSocketClient->SendMsg(msgArray);
}

//判断重复消息
bool IMSocketNetWorkShareLib::OnJudgeMessageRepet(QByteArray strMsgID)
{
	QMap<QString, QList<MessageInfo> >::iterator itor = mMapMessageInfo.begin();
	for (; itor != mMapMessageInfo.end(); ++itor)
	{
		for (int i = 0; i < itor.value().size(); i++)
		{
			if (itor.value()[i].msgID == strMsgID)
			{
				return true;
			}
		}
	}
	return false;
}

//更改msgInfo
void IMSocketNetWorkShareLib::SetMessageInfo(QByteArray msgID,MessageInfo messageInfo)
{
	int nflags = 0;
	QMap<QString, QList<MessageInfo> >::iterator itor = mMapMessageInfo.begin();
	for (; itor != mMapMessageInfo.end(); ++itor)
	{
		for (int i = 0; i < itor.value().size(); i++)
		{
			if (itor.value()[i].msgID == msgID)
			{
				nflags = 1;
				itor.value()[i].isHavePMsgId = messageInfo.isHavePMsgId;
				itor.value()[i].nUserID = messageInfo.nUserID;
				itor.value()[i].offLineFlag = messageInfo.offLineFlag;    //离线消息标志
				itor.value()[i].interalTypeMain = messageInfo.interalTypeMain;//积分主类型
				itor.value()[i].interalTypeVice = messageInfo.interalTypeVice;//积分副类型
				itor.value()[i].integral += messageInfo.integral;      //积分
				break;
			}
		}
	}

	//如果map中没有了 则查找数据库
	if (nflags == 0)
	{
		emit sigDBSetMessageInfo(messageInfo);
	}
}

//处理系统消息
void IMSocketNetWorkShareLib::onDealRevSystemMessage(MessageInfo msgInfo)
{
	emit sigRecSystemMessage(msgInfo);
}


void IMSocketNetWorkShareLib::OnInsertMessage(MessageInfo messageInfo, int nUserID)
{
	if (messageInfo.MsgDeliverType == 1)
		nUserID = messageInfo.nToUserID;
	int nRePeatToUserID = 0;
	QMap<QString, QList<MessageInfo> >::iterator itor = mMapMessageInfo.begin();
	for (; itor != mMapMessageInfo.end(); ++itor)
	{
		//先判断是否存在对应的聊天
		if (itor.key().toInt() == nUserID)
		{
			nRePeatToUserID = 1;
			//如果存在 在判断消息是否有重复
			int nRePeatMessage = 0;
			for (int i = 0; i < itor.value().size(); i++)
			{
				if (itor.value()[i].msgID == messageInfo.msgID)
				{
					nRePeatMessage = 1;
					break;
				}
			}
			if (nRePeatMessage == 0)
			{
				itor.value().append(messageInfo);
				return;
			}
		}
	}
	if (nRePeatToUserID == 0)
	{
		QList<MessageInfo> listTemp;
		listTemp.append(messageInfo);
		mMapMessageInfo.insert(QString("%1").arg(nUserID), listTemp);
	}
}

//处理回执消息
void IMSocketNetWorkShareLib::onDealReceiptMessage(QByteArray buff)
{
	QByteArray recetMSG = buff.mid(4, 42);
#ifdef SOCKET_MESSAGE_DEBUG_ENABLE
	qDebug().noquote() << "<<===============recetMSG=" << recetMSG;
#endif
	SetMessageState(recetMSG, MESSAGE_STATE_SEND);
}

//设置消息状态
void IMSocketNetWorkShareLib::SetMessageState(QByteArray msgID, int nState)
{
	int nflags = 0;
	QMap<QString, QList<MessageInfo> >::iterator itor = mMapMessageInfo.begin();
	for (; itor != mMapMessageInfo.end(); ++itor)
	{
		for (int i = 0; i < itor.value().size(); i++)
		{
			if (itor.value()[i].msgID == msgID)
			{
				nflags = 1;
				if (itor.value()[i].MessageChildType == 102)  //回执消息
				{
					itor.value()[i].MessageState = MESSAGE_STATE_READ;
					emit sigSendUpdateMessage(itor.value()[i], MESSAGE_STATE_READ);
					break;
				}
				else if (itor.value()[i].MsgDeliverType == 1 && itor.value()[i].MessageState == MESSAGE_STATE_SEND)  //部落消息直接设置已读
				{
					itor.value()[i].MessageState = MESSAGE_STATE_READ;
					emit sigSendUpdateMessage(itor.value()[i], MESSAGE_STATE_READ);
					break;
				}
				else
				{
					itor.value()[i].MessageState = nState;
					emit sigSendUpdateMessage(itor.value()[i], nState);
					break;
				}
			}
		}
	}
	//如果map中没有了 则查找数据库
	if (nflags == 0)
	{
		emit sigDBSetMessageState(msgID, nState);
	}
}

QList<QByteArray> IMSocketNetWorkShareLib::SetPerMessageRead(int nUserID)
{
	QList<QByteArray> msgIDList;
	QMap<QString, QList<MessageInfo> >::iterator itor = mMapMessageInfo.begin();
	for (; itor != mMapMessageInfo.end(); ++itor)
	{
		if (itor.key().toInt() == nUserID)
		{
			for (int i = 0; i < itor.value().size(); i++)
			{
				if (itor.value()[i].nFromUserID == nUserID
					&&itor.value()[i].MsgDeliverType == 0
					&& (itor.value()[i].MessageState == MESSAGE_STATE_UNREAD ))
				{
					itor.value()[i].MessageState = MESSAGE_STATE_READ;
					msgIDList.append(itor.value()[i].msgID);
				}
			}
		}
	}
	return msgIDList;
}

QList<QByteArray> IMSocketNetWorkShareLib::SetGroupMessageRead(int nGroupID)
{
	QList<QByteArray> msgIDList;
	QMap<QString, QList<MessageInfo> >::iterator itor = mMapMessageInfo.begin();
	for (; itor != mMapMessageInfo.end(); ++itor)
	{
		for (int i = 0; i < itor.value().size(); i++)
		{
			if (itor.value()[i].nToUserID == nGroupID
				&&itor.value()[i].MsgDeliverType == 1
				&& itor.value()[i].MessageState == MESSAGE_STATE_UNREAD)
			{
				itor.value()[i].MessageState = MESSAGE_STATE_READ;
				msgIDList.append(itor.value()[i].msgID);
			}
		}
	}
	return msgIDList;
}

// Qualifier: 与服务器断开连接
void IMSocketNetWorkShareLib::slotDisConnectServer()
{
	//OnStop();
	StopMonitorServerHeartTimer();

	if (mbReConnect)  //如果需要重连
	{
		emit sigDisConnectServerServer();
	}
}

void IMSocketNetWorkShareLib::OnStart()
{
	if (timer_send == NULL)
	{
		timer_send = new QTimer(this);
		connect(timer_send, SIGNAL(timeout()), this, SLOT(slotJudgetSendMsgState()));
	}
	if (!timer_send->isActive())
	{
		timer_send->start(1000);
	}
}

void IMSocketNetWorkShareLib::OnStop()
{
	if (timer_send != NULL)
	{
		if (timer_send->isActive())
		{
			timer_send->stop();
		}
	}
}

void IMSocketNetWorkShareLib::slotJudgetSendMsgState()
{
	if (mMapMessageInfo.size() == 0) return;
	QMap<QString, QList<MessageInfo> >::iterator itor = mMapMessageInfo.begin();
	for (; itor != mMapMessageInfo.end();)
	{
		for (int i = 0; i < itor.value().size(); i++)
		{
			if (itor.value()[i].nFromUserID == nIMUserID &&
				itor.value()[i].MessageState == MESSAGE_STATE_PRESEND)
			{
				//通知界面 消息为待发送
				SetMessageState(itor.value()[i].msgID, MESSAGE_STATE_SENDING);
			}
			if (itor.value()[i].nFromUserID == nIMUserID &&itor.value()[i].MessageState == MESSAGE_STATE_SENDING)
			{
				//通知界面 消息改为发送中
				//qDebug() << "消息发送中";
				/*判断插入时间与当前时间，如果大于1分钟，默认发送失败*/
				qlonglong clientTime = QDateTime::currentDateTime().toTime_t();
				qlonglong nCrurrent = clientTime * 1000;
				if ((nCrurrent - itor.value()[i].ClientTime) > 60000 && itor.value()[i].MessageChildType != Message_FILE && itor.value()[i].MessageChildType != Message_INPUTTING)
				{
					itor.value()[i].MessageState = MESSAGE_STATE_FAILURE;
					emit sigSendUpdateMessage(itor.value()[i], MESSAGE_STATE_FAILURE);
				}
				else
				{
					if (!itor.value()[i].bIsSend)
					{
						SendQByteArrayMsg(itor.value()[i]);
						emit sigSendMessage(itor.value()[i]);
						itor.value()[i].bIsSend = true;
					}
				}
			}
			else if (itor.value()[i].MessageState == MESSAGE_STATE_READ || itor.value()[i].MessageState == MESSAGE_STATE_FAILURE)
			{
				if (itor.value()[i].MessageChildType == MessageType::Message_PIC
					&& itor.value()[i].strMsg == "load")
				{
					continue;
				}
				if (itor.value()[i].MessageChildType == MessageType::Message_VEDIO
					&& itor.value()[i].strMsg == "load")
				{
					continue;
				}
				itor.value().removeAt(i);
			}
		}
		if (itor.value().size() == 0)
		{
			itor = mMapMessageInfo.erase(itor);
		}
		else
		{
			++itor;
		}
	}
}

void IMSocketNetWorkShareLib::SendQByteArrayMsg(MessageInfo messageInfo)
{
	QByteArray dstMessage("");
	QDataStream writeToByteArray(&dstMessage, QIODevice::ReadWrite);
	writeToByteArray << messageInfo.version;
	writeToByteArray << messageInfo.nFromUserID;
	writeToByteArray << messageInfo.nToUserID;
	writeToByteArray << messageInfo.ClientTime;
	writeToByteArray << messageInfo.ServerTime;
	writeToByteArray << messageInfo.nMsgOrder;
	writeToByteArray << messageInfo.MsgDeliverType;
	writeToByteArray << messageInfo.MessageChildType;
	writeToByteArray << messageInfo.strMsg.toUtf8();
	writeToByteArray << messageInfo.isHavePMsgId;
	writeToByteArray << messageInfo.nUserID;
	for (int i = 0; i < 10; i++)
	{
		writeToByteArray << messageInfo.PerByte[i];
	}
	QByteArray messageHeader("");
	QDataStream messageStream(&messageHeader, QIODevice::ReadWrite);
	messageStream << (BYTE)2;
	messageStream << dstMessage;
	if (messageInfo.MessageChildType == 102)
	{
		SetMessageState(messageInfo.msgID, MESSAGE_STATE_READ);
	}
	else
	{
		SetMessageState(messageInfo.msgID, MESSAGE_STATE_SENDING);
	}
	if (mSocketClient)
		mSocketClient->SendMsg(messageHeader);
#ifdef SOCKET_MESSAGE_DEBUG_ENABLE
	qDebug().noquote() << "===============>>" << "MessageChildType=" << messageInfo.MessageChildType << ",msgID=" << messageInfo.msgID << ",strMsg=" << messageInfo.strMsg;
#endif
}

void IMSocketNetWorkShareLib::setReConnectState(bool bState)
{
	mbReConnect = bState;
	slotDisConnectServer();
}
