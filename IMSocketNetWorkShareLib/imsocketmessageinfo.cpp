﻿#include "imsocketmessageinfo.h"
#include "httpnetworksharelib.h"
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QVariant>
#include "getdeviceinfo.h"
#include <QDir>
#include "QStringLiteralBak.h"
#include "define.h"

#ifndef Q_OS_WIN
#include "inline_mac.h"
#endif

IMSocketMessageInfo::IMSocketMessageInfo(QObject *parent)
	: QObject(parent)
{
	mSocketOpera = NULL;
	mSocketDataBase = NULL;
	mReConnectTimer = NULL;
	mbReConnect = true;
	m_strUserPath = "";
}

IMSocketMessageInfo::~IMSocketMessageInfo()
{
	if (mSocketOpera)
	{
		mSocketOpera->deleteLater();
		mSocketOpera = NULL;
	}
	if (mSocketDataBase)
	{
		mSocketDataBase->DisConnectDB();
		delete mSocketDataBase;
		mSocketDataBase = NULL;
	}
}

//请求socket地址
void IMSocketMessageInfo::RequestSocketUrl(QString strUrl, QString strUserID, QString strUserPwd)
{
	SetIMUserID(strUserID.toInt());
	mRequestSocketInfo.strUrl = strUrl;
	mRequestSocketInfo.strUserID = strUserID;
	mRequestSocketInfo.strPwd = strUserPwd;
	HttpNetWork::HttpNetWorkShareLib *requestUrl = new HttpNetWork::HttpNetWorkShareLib;
	connect(requestUrl, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotReplySocketInfoFinished(bool, QString)));
	QString strRequestUrl = StitchSocketParameter(strUrl + HTTP_GETSOCKETADDRESS,strUserID,strUserPwd);
	qDebug() << strRequestUrl;
	requestUrl->getHttpRequest(strRequestUrl);
}

QString IMSocketMessageInfo::StitchSocketParameter(QString url, QString AccountName, QString PassWord)
{
	GetDeviceInfo deviceInfo;
	QString deviceType = deviceInfo.GetDeviceType();
	QString deviceVersion = deviceInfo.GetDeviceVersion();
	QString deviceCompany = deviceInfo.GetDeviceManufacturer();
	QString deviceModel = deviceInfo.GetDeviceModel();
	QString appVersion = deviceInfo.GetAppVersion();
	QString deviceId = deviceInfo.GetDeviceID();
	QString deviceClass = deviceInfo.GetDeviceClass();
	QString strHttp = url + "?userId=" + AccountName + "&"
		"passWord=" + PassWord + "&deviceType=" + deviceType + "&deviceVersion=" + deviceVersion + "&"
		"deviceCompany=" + deviceCompany + "&deviceModel=" + deviceModel + "&appVersion=" + appVersion + "&"
		"deviceId=" + deviceId + "&deviceClass=" + deviceClass;
	return strHttp;
}

void IMSocketMessageInfo::slotReplySocketInfoFinished(bool bResult, QString strResult)
{
	if (bResult)
	{
		if (ParseSocketInfo(strResult))
		{
			ConnectDataBase();
			/*开始连接socket*/
			if (OnConnectSocket(mSocketInfo.ServerIP,mSocketInfo.ServerPort,mSocketInfo.ServerToken))
			{
				//emit sigGetSocketAddressErro(tr("正在初始化..."));
				emit sigConnectSocketSuccess();
			}
			else
			{
				emit sigGetSocketAddressErro(tr("Login request failed!"));
			}
		}
		else
		{
			emit sigGetSocketAddressErro(tr("Requesting socket information failed!"));
		}
	}
	else 
	{
		emit sigGetSocketAddressErro(tr("Requesting socket information failed!"));
	}
}

bool IMSocketMessageInfo::ParseSocketInfo(QString socketInfo)
{
	QJsonParseError jsonError;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(socketInfo.toUtf8(), &jsonError);
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QJsonObject obj = jsonDocument.object();
			if (obj["login"].toString() == "success")
			{
				if (obj.contains("token"))
				{
					mSocketInfo.ServerToken = obj.take("token").toString();
				}
				if (obj.contains("server"))
				{
					QJsonValue value = obj.take("server");
					if (value.isObject())
					{
						QJsonObject dataObj = value.toObject();
						if (dataObj.contains("TCP"))
						{
							QJsonValue value_list = dataObj.take("TCP");
							if (value_list.isArray())
							{
								QJsonArray arr = value_list.toArray();
								QJsonValue value = arr.at(0);
								QString serverInfo = value.toString();
								int nIndex = serverInfo.indexOf(":");
								mSocketInfo.ServerIP = serverInfo.mid(0, nIndex);
								mSocketInfo.ServerPort = serverInfo.mid(nIndex + 1, serverInfo.length());
								return true;
							}
						}
					}
				}
			}
		}
	}
	return false;
}

//设置IMUserID
void IMSocketMessageInfo::SetIMUserID(int nIMID)
{
	nIMUserID = nIMID;
	if (mSocketOpera)
	{
		mSocketOpera->SetIMUserID(nIMID);
	}
}

//连接socket
bool IMSocketMessageInfo::OnConnectSocket(QString strServerIP, QString strServerPort, QString strToken)
{
	if (mSocketOpera == NULL)
	{
		mSocketOpera = new IMSocketNetWorkShareLib;
		connect(mSocketOpera, SIGNAL(sigSendMessage(MessageInfo)), this, SLOT(slotSendMessage(MessageInfo)));
		connect(mSocketOpera, SIGNAL(sigSendUpdateMessage(MessageInfo, int)), this, SLOT(slotRevUpdateMessage(MessageInfo, int)));
		connect(mSocketOpera, SIGNAL(sigRecSystemMessage(MessageInfo)), this, SLOT(slotRecSystemMessage(MessageInfo)));
		connect(mSocketOpera, SIGNAL(sigRevServerMessage(MessageInfo)), this, SLOT(slotRevServerMessage(MessageInfo)));
		connect(mSocketOpera, SIGNAL(sigDisConnectServerServer()), this, SLOT(slotDisConnectServerServer()));
		connect(mSocketOpera, SIGNAL(sigDBSetMessageInfo(MessageInfo)), this, SLOT(slotDBSetMessageInfo(MessageInfo)));
		connect(mSocketOpera, SIGNAL(sigDBSetMessageState(QByteArray, int)), this, SLOT(slotDBSetMessageState(QByteArray, int)));
	}
	QString strLastMessage = DBGetMessageLastTime(nIMUserID); 
	return mSocketOpera->OnConnectSocket(strServerIP, strServerPort, strToken, strLastMessage);
}

//连接消息数据库
void IMSocketMessageInfo::ConnectDataBase()
{
	if (mSocketDataBase == NULL)
	{
		mSocketDataBase = new IMSocketDataBaseShareLib();
		mSocketDataBase->setUserPath(m_strUserPath);
	}
#ifdef Q_OS_WIN
	QString strDBPath = m_strUserPath + "/database/" + QString::number(nIMUserID) + "//message.db";/*数据库路径*/
#else
    QDir dir;
    QString strDBPath = m_strUserPath + "/database/" + QString::number(nIMUserID) + "//message.db";/*数据库路径*/
#endif
	mSocketDataBase->ConnectDB(strDBPath, "messageDB");
}

//发送消息
MessageInfo IMSocketMessageInfo::SendMessage(int nFromUserID, int nToUserID, int nDeliverType, short MessageChildType, int nMessageState,QString strMsg)
{
	MessageInfo msgInfo;
	if (mSocketOpera)
	{
		msgInfo = mSocketOpera->SendMessage(nFromUserID, nToUserID, nDeliverType, MessageChildType, nMessageState, strMsg);
	}
	if (mSocketDataBase && MessageChildType!=102 && MessageChildType != 103)
	{
		mSocketDataBase->InserMessageInfo(nIMUserID, nToUserID, msgInfo);
	}
	return msgInfo;
}

//发送文本消息
MessageInfo IMSocketMessageInfo::SendTextMessage(int nFromUserID, int nToUserID, int nDeliverType, short MessageChildType, QString strMsg)
{
	return SendMessage(nFromUserID, nToUserID, nDeliverType, MessageChildType, MESSAGE_STATE_PRESEND,strMsg);
}
//发送视频消息(手机端转发)
MessageInfo IMSocketMessageInfo::SendVideoMessage(int nFromUserID, int nToUserID, int nDeliverType, short MessageChildType, QString strMsg)
{
	return SendMessage(nFromUserID, nToUserID, nDeliverType, MessageChildType, MESSAGE_STATE_PRESEND, strMsg);
}

//发送消息已读
void IMSocketMessageInfo::SendMessageReadMessage(int nFromUserID, int nToUserID, int nDeliverType, QString strMsg)
{
	SendMessage(nFromUserID, nToUserID, nDeliverType, 102, MESSAGE_STATE_PRESEND, strMsg);
}

//发送图片消息
MessageInfo IMSocketMessageInfo::SendPicMessage(int nFromUserID, int nToUserID, int nDeliverType, QString strMsg)
{
    Q_UNUSED(strMsg)
	return SendMessage(nFromUserID, nToUserID, nDeliverType, (short)1, MESSAGE_STATE_WAITSEND, "");
}

//发送通知
MessageInfo IMSocketMessageInfo::SendNoteiceMessage(int nFromUserID, int nToUserID, int nDeliverType, QString strMsg)
{
    Q_UNUSED(strMsg)
	return SendMessage(nFromUserID, nToUserID, nDeliverType, Message_NOTICE, MESSAGE_STATE_WAITSEND, "");
}

//设置消息状态
void IMSocketMessageInfo::SetMessageState(QByteArray msgID, int nState)
{
	if (mSocketOpera)
	{
		mSocketOpera->SetMessageState(msgID, nState);
	}
}

//上传图片结束
void IMSocketMessageInfo::setUpPicReplyFinished(QByteArray result, PicMessage *picMessage, MessageInfo msgInfo)
{
	QString fileID;
	QJsonParseError jsonError;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(result, &jsonError);
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QVariantMap result = jsonDocument.toVariant().toMap();
			if (result["result"].toString() == "success")
			{
				fileID = result["fileid"].toString();

				if (picMessage)
				{
					QString strUrl = picMessage->strDownLoadUrl + fileID + "/download";
					if (picMessage->nPicWidth == 0)
						picMessage->nPicWidth = 320;
					if (picMessage->nPicHeight == 0)
						picMessage->nPicHeight = 480;
					QString strScale = QString::number(picMessage->nPicWidth) + ":" + QString::number(picMessage->nPicHeight);
					QJsonObject json;
					json.insert("url", strUrl);
					json.insert("path", picMessage->strPicPath);
					json.insert("scale", strScale);
					QString strJSON = QString(QJsonDocument(json).toJson());
					strJSON.remove("\n");
					if (mSocketOpera)
					{
						mSocketOpera->ChangeMessageInfo(msgInfo.msgID, strJSON.trimmed());
						SetMessageState(msgInfo.msgID, MESSAGE_STATE_PRESEND);
						msgInfo.strMsg = strJSON.trimmed();
						mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nToUserID, msgInfo);
						return;
					}
				}
			}
		}
	}

	slotRevUpdateMessage(msgInfo, MESSAGE_STATE_FAILURE);
}
//上传通告图片结束
void IMSocketMessageInfo::setUpNoticePicReplyFinished(QByteArray result, QMap<QString,QString> mapData, MessageInfo msgInfo)
{
	QString fileID;
	QJsonParseError jsonError;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(result, &jsonError);
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QVariantMap result = jsonDocument.toVariant().toMap();
			if (result["result"].toString() == "success")
			{
				fileID = result["fileid"].toString();


					QString strUrl = mapData.value("imageUrl") + fileID + "/download";
					//QString strScale = QString::number(picMessage->nPicWidth) + ":" + QString::number(picMessage->nPicHeight);
					QJsonObject json;
					json.insert("imageUrl", strUrl);
					json.insert("imageTitle", mapData.value("imageTitle"));
					json.insert("webUrl", mapData.value("webUrl"));
					json.insert("webTitle", mapData.value("webTitle"));
					QString strJSON = QString(QJsonDocument(json).toJson());
					strJSON.remove("\n");
					if (mSocketOpera)
					{
						mSocketOpera->ChangeMessageInfo(msgInfo.msgID, strJSON.trimmed());
						SetMessageState(msgInfo.msgID, MESSAGE_STATE_PRESEND);
						msgInfo.strMsg = strJSON.trimmed();
						mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nToUserID, msgInfo);
						return;
					}
			}
		}
	}

	slotRevUpdateMessage(msgInfo, MESSAGE_STATE_FAILURE);
}

//发送文件消息
MessageInfo IMSocketMessageInfo::SendFileMessage(int nFromUserID, int nToUserID, int nDeliverType, QString strMsg)
{
    Q_UNUSED(strMsg)
	return SendMessage(nFromUserID, nToUserID, nDeliverType, (short)5, MESSAGE_STATE_WAITSEND, "");
}

//上传文件结束
void IMSocketMessageInfo::setUpFileReplyFinished(QByteArray result, FileMessage* fileMessage, MessageInfo msgInfo)
{
	QString fileID;
	QJsonParseError jsonError;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(result, &jsonError);
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QVariantMap result = jsonDocument.toVariant().toMap();
			if (result["result"].toString() == "success")
			{
				fileID = result["fileid"].toString();

				if (fileMessage)
				{
					QJsonObject json;
					json.insert("FileLocalPath", fileMessage->strFilePath);
					json.insert("FileName", fileMessage->FileName);
					json.insert("FileType", fileMessage->FileType);
					json.insert("FileSize", fileMessage->FileSize);
					json.insert("FileId", fileID);
					QString strUrl = fileMessage->strDownLoadUrl + fileID + QString("/download");
					json.insert("url", strUrl);
					QString strJSON = QString(QJsonDocument(json).toJson());
					strJSON.remove("\n");
					if (mSocketOpera)
					{
						mSocketOpera->ChangeMessageInfo(msgInfo.msgID, strJSON.trimmed());
						SetMessageState(msgInfo.msgID, MESSAGE_STATE_PRESEND);
						msgInfo.strMsg = strJSON.trimmed();
						mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nToUserID, msgInfo);
						return;
					}
				}
			}
		}
	}

	qDebug() << "imsocketmessageInfo:311";
	slotRevUpdateMessage(msgInfo, MESSAGE_STATE_FAILURE);
}

//接收到系统消息
void IMSocketMessageInfo::slotRecSystemMessage(MessageInfo msgInfo)
{
	DBInsertMessageInfo(nIMUserID, msgInfo.nToUserID, msgInfo);
	emit sigRevSystemMessage(msgInfo);
}

//接收到服务器消息
void IMSocketMessageInfo::slotRevServerMessage(MessageInfo msgInfo)
{
	//判断数据库中是否重复
	MessageInfo messageInfo = mSocketDataBase->GetDBMessageInfoWithMsgID(nIMUserID,msgInfo.msgID);
	if (messageInfo.msgID.isEmpty())
	{
		switch (msgInfo.MessageChildType)
		{
		case Message_TEXT:
			OnDealMessageText(msgInfo);
			break;
		case Message_PIC:
			OnDealMessagePic(msgInfo);
			break;
		case Message_AUDIO:
			OnDealMessageAudio(msgInfo);
			break;
		case Message_VEDIO:
			OnDealMessageVideo(msgInfo);
			break;
		case Message_FILE:
			OnDealMessageFile(msgInfo);
			break;
		case Message_AD:
			onDealMessageAD(msgInfo);
			break;
		case Message_REDBAG:
			onDealMessageRedBag(msgInfo);
			break;
		case Message_TRANSFER:
			OnDealMessageText(msgInfo);
			break;
		case Message_VOICE:
			onDealMessageVoice(msgInfo);
			break;
		case Message_SECRETLETTER:
			OnDealMessageText(msgInfo);
			break;
		case Message_SECRETIMAGE:
			OnDealMessageText(msgInfo);
			break;
		case Message_SECRETFILE:
			OnDealMessageText(msgInfo);
			break;
		case Message_NOTICE:
			OnDealMessageText(msgInfo);
			break;
		case Message_URL:
			OnDealMessageText(msgInfo);
			break;
		case Message_LOCATION:
			OnDealMessageText(msgInfo);
			break;
		case Message_COMMON:
			OnDealMessageCommon(msgInfo);
			break;
		case Message_GW:
			OnDealMessageText(msgInfo);
			break;
		case Message_NOTIFY:
			OnDealMessageText(msgInfo);
			break;
		case Message_ROBOT:
			OnDealMessageRobot(msgInfo);
			break;
		case Message_AT:
			OnDealMessageAt(msgInfo);
			break;
		case Message_Ticket:
			OnDealMessageText(msgInfo);
			break;
		case Message_INPUTTING:
			OnDealMessageInputting(msgInfo);
			break;
		default:
			break;
		}
	}
}

//更改数据库消息积分情况
void IMSocketMessageInfo::slotDBSetMessageInfo(MessageInfo msgInfo)
{
	MessageInfo messageInfo = DBGetMessageInfoWithMsgID(msgInfo.msgID);
	messageInfo.isHavePMsgId = msgInfo.isHavePMsgId;
	messageInfo.nUserID = msgInfo.nUserID;
	messageInfo.offLineFlag = msgInfo.offLineFlag;    //离线消息标志
	messageInfo.interalTypeMain = msgInfo.interalTypeMain;//积分主类型
	messageInfo.interalTypeVice = msgInfo.interalTypeVice;//积分副类型
	messageInfo.integral += msgInfo.integral;      //积分
	DBUpdateMessageStateInfo(msgInfo.msgID, msgInfo.MessageState, messageInfo.integral);
}

//设置数据库消息状态
void IMSocketMessageInfo::slotDBSetMessageState(QByteArray msgID,int nState)
{
	MessageInfo messageInfo = DBGetMessageInfoWithMsgID(msgID);
	if (messageInfo.MessageChildType == 102)  //回执消息
	{
		messageInfo.MessageState = MESSAGE_STATE_READ;
	}
	else if (messageInfo.MsgDeliverType == 1 && messageInfo.MessageState == MESSAGE_STATE_SEND)  //部落消息直接设置已读
	{
		messageInfo.MessageState = MESSAGE_STATE_READ;
		slotRevUpdateMessage(messageInfo, MESSAGE_STATE_READ);
	}
	else if (messageInfo.MessageState == MESSAGE_STATE_UNLOADED)
	{
		return;
	}
	else
	{
		slotRevUpdateMessage(messageInfo, nState);
	}
}

// Qualifier: 处理收到的文本消息
void IMSocketMessageInfo::OnDealMessageText(MessageInfo msgInfo)
{
	msgInfo.MessageState = MESSAGE_STATE_RECEIVE;
	if (msgInfo.MsgDeliverType == 1)
	{
		msgInfo.MessageState = MESSAGE_STATE_SEND;
	}
	if (msgInfo.nFromUserID == nIMUserID)
	{
		if (mSocketOpera)
		{
			mSocketOpera->OnInsertMessage(msgInfo, msgInfo.nToUserID);
		}
		if (mSocketDataBase)
			mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nToUserID, msgInfo);
		emit sigRevOtherDeviceMsg(msgInfo);      //处理其他设备发送的消息
	}
	else
	{
		msgInfo.MessageState = MESSAGE_STATE_UNREAD;  //消息类型设置未读
		if (mSocketOpera)
		{
			mSocketOpera->OnInsertMessage(msgInfo, msgInfo.nFromUserID);
		}
		if (mSocketDataBase)
			mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nFromUserID, msgInfo);
		emit sigRevMsgInfo(msgInfo, msgInfo.nFromUserID);
	}
}

QString IMSocketMessageInfo::GetExeDir()
{
	QDir dir;
#ifdef Q_OS_WIN
    return dir.currentPath();
#else
    return getResourcePath();
#endif
}

// Qualifier: 处理收到的图片消息
void IMSocketMessageInfo::OnDealMessagePic(MessageInfo msgInfo)
{
	QJsonParseError jsonError;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8(), &jsonError);
	QJsonObject  saveJson;
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QVariantMap result = jsonDocument.toVariant().toMap();

			QString strPath = result["path"].toString();
			QString strFileType = strPath.right(strPath.length() - strPath.lastIndexOf('.'));
			if (strFileType != ".gif")
			{
				//20181030wmc 固定png格式导致发送接收到到图片时长宽比错误
				//strFileType = ".png";
			}
			//21081107原本为strMsg改为"load"后使用本地路径  更改为保留url直到成功下载 用新的状态unload作为标志
			QString strFileName = m_strUserPath + "/resource/photo/" + QString::number(nIMUserID) + "/" + msgInfo.msgID + strFileType;
			msgInfo.strMsg = "load";
			if (msgInfo.nFromUserID != nIMUserID)
			{
				emit sigRevMsgInfo(msgInfo, msgInfo.nFromUserID);
				saveJson.insert("type", strFileType);
				saveJson.insert("url", result["url"].toString());
				QJsonDocument documentTmp;
				documentTmp.setObject(saveJson);
				QByteArray byte_array = documentTmp.toJson(QJsonDocument::Compact);
				QString json_str(byte_array);
				msgInfo.strMsg = json_str;
				msgInfo.MessageState = MESSAGE_STATE_UNLOADED;
				if (mSocketOpera)
				{
					mSocketOpera->OnInsertMessage(msgInfo, msgInfo.nFromUserID);
				}
				if (mSocketDataBase)
				{
					mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nFromUserID, msgInfo);
				}

				HttpNetWork::HttpDownLoadFile *requestHttp = new HttpNetWork::HttpDownLoadFile;

				requestHttp->setData(QVariant::fromValue(msgInfo));
				requestHttp->setProperty("localpath", strFileName);
				connect(requestHttp, SIGNAL(sigDownFinished(bool)), this, SLOT(slotDownFileFinish(bool)));
				requestHttp->StartDownLoadFile(result["url"].toString(), strFileName);
			}
			else
			{
				msgInfo.MessageState = MESSAGE_STATE_RECEIVE;
				if (msgInfo.MsgDeliverType == 1)
				{
					msgInfo.MessageState = MESSAGE_STATE_SEND;
				}
				if (mSocketOpera)
				{
					mSocketOpera->OnInsertMessage(msgInfo, msgInfo.nToUserID);
				}
				if (mSocketDataBase)
				{
					mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nToUserID, msgInfo);
				}
				HttpNetWork::HttpDownLoadFile *requestHttp = new HttpNetWork::HttpDownLoadFile;
				msgInfo.strMsg = strFileName;
				requestHttp->setData(QVariant::fromValue(msgInfo));
				requestHttp->setProperty("localpath", strFileName);
				connect(requestHttp, SIGNAL(sigDownFinished(bool)), this, SLOT(slotDownFileFinish(bool)));
				requestHttp->StartDownLoadFile(result["url"].toString(), strFileName);
			}
		}
	}
}

// Qualifier: 处理收到的音频消息
void IMSocketMessageInfo::OnDealMessageAudio(MessageInfo msgInfo)
{
	QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();

	QString type = map.value("path").toString().right(4);
	QString url = map.value("url").toString();
	QString strFileName = m_strUserPath + "/resource/audio/" + QString::number(nIMUserID) + "/" + msgInfo.msgID + type;

	msgInfo.strMsg = strFileName;

	HttpNetWork::HttpDownLoadFile *requestHttp = new HttpNetWork::HttpDownLoadFile;
	requestHttp->setData(QVariant::fromValue(msgInfo));
	requestHttp->setProperty("url", url);
	requestHttp->setProperty("type", type);
	connect(requestHttp, SIGNAL(sigDownFinished(bool)), this, SLOT(slotDownAudioFinish(bool)));
	requestHttp->StartDownLoadFile(url, strFileName);
}

void IMSocketMessageInfo::slotDownAudioFinish(bool bResult)
{
	HttpNetWork::HttpDownLoadFile *http = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());
	QVariant var = http->getData();
	MessageInfo msgInfo = var.value<MessageInfo>();
	QVariant vurl = http->property("url");
	QString strUrl = vurl.value<QString>();
	QVariant vType = http->property("type");
	QString strType = vType.value<QString>();
	if (bResult)
	{
		msgInfo.MessageState = MESSAGE_STATE_RECEIVE;
		if (msgInfo.MsgDeliverType == 1)
		{
			msgInfo.MessageState = MESSAGE_STATE_SEND;
		}
		if (msgInfo.nFromUserID == nIMUserID)
		{
			if (msgInfo.MessageChildType != 5)
			{
				if (mSocketOpera)
				{
					mSocketOpera->OnInsertMessage(msgInfo, msgInfo.nToUserID);
				}
				if (mSocketDataBase)
					mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nToUserID, msgInfo);
				emit sigRevOtherDeviceMsg(msgInfo);      //处理其他设备发送的消息
			}
		}
		else
		{
			msgInfo.MessageState = MESSAGE_STATE_UNREAD;  //消息类型设置未读
			if (mSocketOpera)
			{
				mSocketOpera->OnInsertMessage(msgInfo, msgInfo.nFromUserID);
			}
			if (mSocketDataBase)
				mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nFromUserID, msgInfo);
			emit sigRevMsgInfo(msgInfo, msgInfo.nFromUserID);
		}
	}
	else
	{
		QJsonObject  saveJson;
		saveJson.insert("type", strType);
		saveJson.insert("url", strUrl);
		QJsonDocument documentTmp;
		documentTmp.setObject(saveJson);
		QByteArray byte_array = documentTmp.toJson(QJsonDocument::Compact);
		QString json_str(byte_array);
		msgInfo.strMsg = json_str;

		msgInfo.strMsg = strUrl;
		msgInfo.MessageState = MESSAGE_STATE_UNLOADED;
		if (mSocketOpera)
		{
			mSocketOpera->OnInsertMessage(msgInfo, msgInfo.nFromUserID);
		}
		if (mSocketDataBase)
			mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nFromUserID, msgInfo);
		emit sigRevMsgInfo(msgInfo, msgInfo.nFromUserID);
	}
}

// Qualifier: 处理收到的视频消息
void IMSocketMessageInfo::OnDealMessageVideo(MessageInfo msgInfo)
{
	//qDebug() << "bIsSend" << msgInfo.bIsSend;
	//qDebug() << "ClientTime" << msgInfo.ClientTime;
	//qDebug() << "integral" << msgInfo.integral;
	//qDebug() << "interalTypeMain" << msgInfo.interalTypeMain;
	//qDebug() << "interalTypeVice" << msgInfo.interalTypeVice;
	//qDebug() << "isHavePMsgId" << msgInfo.isHavePMsgId;
	//qDebug() << "MessageChildType" << msgInfo.MessageChildType;
	//qDebug() << "MessageState" << msgInfo.MessageState;
	//qDebug() << "MsgDeliverType" << msgInfo.MsgDeliverType;
	//qDebug() << "msgID" << msgInfo.msgID;
	//qDebug() << "nFromUserID" << msgInfo.nFromUserID;
	//qDebug() << "nMsgOrder" <<msgInfo.nMsgOrder;
	//qDebug() << "nToUserID" << msgInfo.nToUserID;
	//qDebug() << "nUserID" <<msgInfo.nUserID;
	//qDebug() << "offLineFlag" <<msgInfo.offLineFlag;
	//qDebug() << "PerByte"<<msgInfo.PerByte;
	//qDebug() << "PMSGID"<<msgInfo.PMSGID;
	//qDebug() << "SendTime"<<msgInfo.SendTime;
	//qDebug() << "ServerTime"<<msgInfo.ServerTime;
	//qDebug() << "strMessageListDesc"<<msgInfo.strMessageListDesc;
	//qDebug() << "strMessageListJson"<<msgInfo.strMessageListJson;
	//qDebug() << "strMsg"<<msgInfo.strMsg;
	//qDebug() << "strRowId"<<msgInfo.strRowId;
	//qDebug() << "version"<<msgInfo.version;

	QJsonParseError jsonError;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8(), &jsonError);
	QJsonObject  saveJson;
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QVariantMap result = jsonDocument.toVariant().toMap();
			QString strFileType = result["path"].toString().right(4);
			QString strFileName = m_strUserPath + "/resource/vedio/" + QString::number(nIMUserID) + "/" + msgInfo.msgID + strFileType;
			msgInfo.strMsg = "load";
			if (msgInfo.nFromUserID != nIMUserID)
			{
				emit sigRevMsgInfo(msgInfo, msgInfo.nFromUserID);
				saveJson.insert("type", strFileType);
				saveJson.insert("url", result["url"].toString());
				saveJson.insert("time", result["time"].toString());
				saveJson.insert("path", result["path"].toString());
				QJsonDocument documentTmp;
				documentTmp.setObject(saveJson);
				QByteArray byte_array = documentTmp.toJson(QJsonDocument::Compact);
				QString json_str(byte_array);
				msgInfo.strMsg = json_str;
				msgInfo.MessageState = MESSAGE_STATE_UNLOADED;
				if (mSocketOpera)
				{
					mSocketOpera->OnInsertMessage(msgInfo, msgInfo.nFromUserID);
				}
				if (mSocketDataBase)
				{
					mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nFromUserID, msgInfo);
				}
				HttpNetWork::HttpDownLoadFile *requestHttp = new HttpNetWork::HttpDownLoadFile;
				requestHttp->setProperty("localpath", strFileName);
				requestHttp->setData(QVariant::fromValue(msgInfo));
				connect(requestHttp, SIGNAL(sigDownFinished(bool)), this, SLOT(slotDownVideoFinish(bool)));
				requestHttp->StartDownLoadFile(result["url"].toString(), strFileName);
			}
			else
			{
				saveJson.insert("type", strFileType);
				saveJson.insert("url", result["url"].toString());
				saveJson.insert("time", result["time"].toString());
				saveJson.insert("path", result["path"].toString());

				QJsonDocument documentTmp;
				documentTmp.setObject(saveJson);
				QByteArray byte_array = documentTmp.toJson(QJsonDocument::Compact);
				QString json_str(byte_array);
				msgInfo.strMsg = json_str;

				msgInfo.MessageState = MESSAGE_STATE_RECEIVE;
				if (msgInfo.MsgDeliverType == 1)
				{
					msgInfo.MessageState = MESSAGE_STATE_SEND;
				}
				if (mSocketOpera)
				{
					mSocketOpera->OnInsertMessage(msgInfo, msgInfo.nToUserID);
				}
				if (mSocketDataBase)
				{
					mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nToUserID, msgInfo);
				}
				HttpNetWork::HttpDownLoadFile *requestHttp = new HttpNetWork::HttpDownLoadFile;
				requestHttp->setData(QVariant::fromValue(msgInfo));
				requestHttp->setProperty("localpath", strFileName);
				connect(requestHttp, SIGNAL(sigDownFinished(bool)), this, SLOT(slotDownVideoFinish(bool)));
				requestHttp->StartDownLoadFile(result["url"].toString(), strFileName);
			}
		}
	}
}

// Qualifier: 处理收到的文件消息
void IMSocketMessageInfo::OnDealMessageFile(MessageInfo messageInfo)
{
	QString strMsg = messageInfo.strMsg;
	QJsonDocument docJson = QJsonDocument::fromJson(strMsg.toUtf8());
	QVariantMap map = docJson.toVariant().toMap();
	map.insert("recvFlag", 1);
	docJson = QJsonDocument::fromVariant(map);
	strMsg = docJson.toJson();
	messageInfo.strMsg = strMsg;
	OnDealMessageText(messageInfo);
}

void IMSocketMessageInfo::onDealMessageAD(MessageInfo msgInfo)
{
	msgInfo.MessageChildType = Message_TEXT;
	msgInfo.strMsg = tr("This message type is not supported at this time. Please check it on the mobile side");

	msgInfo.MessageState = MESSAGE_STATE_RECEIVE;
	if (msgInfo.MsgDeliverType == 1)
	{
		msgInfo.MessageState = MESSAGE_STATE_SEND;
	}
	if (msgInfo.nFromUserID == nIMUserID)
	{
		if (msgInfo.MessageChildType != 5)
		{
			if (mSocketOpera)
			{
				mSocketOpera->OnInsertMessage(msgInfo, msgInfo.nToUserID);
			}
			if (mSocketDataBase)
				mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nToUserID, msgInfo);
			emit sigRevOtherDeviceMsg(msgInfo);      //处理其他设备发送的消息
		}
	}
	else
	{
		msgInfo.MessageState = MESSAGE_STATE_UNREAD;  //消息类型设置未读
		if (mSocketOpera)
		{
			mSocketOpera->OnInsertMessage(msgInfo, msgInfo.nFromUserID);
		}
		if (mSocketDataBase)
			mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nFromUserID, msgInfo);
		emit sigRevMsgInfo(msgInfo, msgInfo.nFromUserID);
	}
}

void IMSocketMessageInfo::onDealMessageRedBag(MessageInfo msgInfo)
{
	msgInfo.MessageState = MESSAGE_STATE_RECEIVE;
	if (msgInfo.MsgDeliverType == 1)
	{
		msgInfo.MessageState = MESSAGE_STATE_SEND;
	}
	if (msgInfo.nFromUserID == nIMUserID)
	{
		if (mSocketOpera)
		{
			mSocketOpera->OnInsertMessage(msgInfo, msgInfo.nToUserID);
		}
		if (mSocketDataBase)
		{
			MessageInfo localMsg = mSocketDataBase->GetDBMessageInfoWithMsgID(nIMUserID, msgInfo.msgID);
			if (!localMsg.strMsg.isEmpty())
				msgInfo.strMsg = localMsg.strMsg;

			mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nToUserID, msgInfo);
		}
			
		emit sigRevOtherDeviceMsg(msgInfo);      //处理其他设备发送的消息
	}
	else
	{
		msgInfo.MessageState = MESSAGE_STATE_UNREAD;  //消息类型设置未读
		if (mSocketOpera)
		{
			mSocketOpera->OnInsertMessage(msgInfo, msgInfo.nFromUserID);
		}
		if (mSocketDataBase)
			mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nFromUserID, msgInfo);
		emit sigRevMsgInfo(msgInfo, msgInfo.nFromUserID);
	}
}

void IMSocketMessageInfo::onDealMessageVoice(MessageInfo msgInfo)
{
	msgInfo.MessageChildType = Message_TEXT;
	msgInfo.strMsg = tr("[Lightspeed SMS] Please check on the mobile side");

	msgInfo.MessageState = MESSAGE_STATE_RECEIVE;
	if (msgInfo.MsgDeliverType == 1)
	{
		msgInfo.MessageState = MESSAGE_STATE_SEND;
	}
	if (msgInfo.nFromUserID == nIMUserID)
	{
		if (msgInfo.MessageChildType != 5)
		{
			if (mSocketOpera)
			{
				mSocketOpera->OnInsertMessage(msgInfo, msgInfo.nToUserID);
			}
			if (mSocketDataBase)
				mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nToUserID, msgInfo);
			emit sigRevOtherDeviceMsg(msgInfo);      //处理其他设备发送的消息
		}
	}
	else
	{
		msgInfo.MessageState = MESSAGE_STATE_UNREAD;  //消息类型设置未读
		if (mSocketOpera)
		{
			mSocketOpera->OnInsertMessage(msgInfo, msgInfo.nFromUserID);
		}
		if (mSocketDataBase)
			mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nFromUserID, msgInfo);
		emit sigRevMsgInfo(msgInfo, msgInfo.nFromUserID);
	}
}

// Qualifier: 处理收到的通用消息
void IMSocketMessageInfo::OnDealMessageCommon(MessageInfo msgInfo)
{
	OnDealMessageText(msgInfo);
}

// Qualifier: 处理收到的机器人消息
void IMSocketMessageInfo::OnDealMessageRobot(MessageInfo messageInfo)
{
	OnDealMessageText(messageInfo);
}

void IMSocketMessageInfo::OnDealMessageAt(MessageInfo msgInfo)
{
	OnDealMessageText(msgInfo);
}

//Qualifier: 視頻下载完毕
void IMSocketMessageInfo::slotDownVideoFinish(bool bResult)
{
	HttpNetWork::HttpDownLoadFile *act = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());
	if (act)
	{
		QVariant variant = act->getData();
		MessageInfo msgInfo = variant.value<MessageInfo>();
		QVariant var = act->property("localpath");
		QString strPath = var.value<QString>();

		//更新FILEINFO表
		emit sigInsertLocalFileTable(msgInfo.msgID, strPath, msgInfo.strMsg);
		if (!bResult)
		{
			//msgInfo.strMsg = "fail";
		}
		else
		{
			if (msgInfo.MessageState == MESSAGE_STATE_UNLOADED)
			{
				msgInfo.MessageState = MESSAGE_STATE_UNREAD;
			}
			if (mSocketOpera)
			{
				mSocketOpera->SetMessageState(msgInfo.msgID, msgInfo.MessageState);
			}
		}
		msgInfo.strMsg = strPath;
		if ((msgInfo.MessageState == MESSAGE_STATE_READ) || (msgInfo.MessageState == MESSAGE_STATE_UNREAD) || (msgInfo.MessageState == MESSAGE_STATE_UNLOADED))
		{
			if (mSocketDataBase)
			{
				mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nFromUserID, msgInfo);
			}
			emit sigRevMsgInfo(msgInfo, msgInfo.nFromUserID);
		}
		else if (msgInfo.MessageState == MESSAGE_STATE_RECEIVE || msgInfo.MessageState == MESSAGE_STATE_SEND)
		{
			if (mSocketDataBase)
			{
				mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nToUserID, msgInfo);
			}
			emit sigRevOtherDeviceMsg(msgInfo);      //处理其他设备发送的消息
		}
	}
}


//Qualifier: 文件下载完毕
void IMSocketMessageInfo::slotDownFileFinish(bool bResult)
{
	HttpNetWork::HttpDownLoadFile *act = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());
	if (act)
	{
		QVariant variant = act->getData();
		MessageInfo msgInfo = variant.value<MessageInfo>();
		QVariant var = act->property("localpath");
		QString strPath = var.value<QString>();


		if (!bResult)
		{
			//msgInfo.strMsg = "fail";
		}
		else
		{
			if (msgInfo.MessageState == MESSAGE_STATE_UNLOADED)
			{
				msgInfo.MessageState = MESSAGE_STATE_UNREAD;
			}
			if (mSocketOpera)
			{
				mSocketOpera->SetMessageState(msgInfo.msgID, msgInfo.MessageState);
			}
			msgInfo.strMsg = strPath;
		}
		if ((msgInfo.MessageState == MESSAGE_STATE_READ) || (msgInfo.MessageState == MESSAGE_STATE_UNREAD) || (msgInfo.MessageState == MESSAGE_STATE_UNLOADED))
		{
			if (mSocketDataBase)
			{
				mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nFromUserID, msgInfo);
			}
			emit sigRevMsgInfo(msgInfo, msgInfo.nFromUserID);
		}
		else if (msgInfo.MessageState == MESSAGE_STATE_RECEIVE || msgInfo.MessageState == MESSAGE_STATE_SEND)
		{
			if (mSocketDataBase)
			{
				mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nToUserID, msgInfo);
			}
			emit sigRevOtherDeviceMsg(msgInfo);      //处理其他设备发送的消息
		}
	}
}

QList<QByteArray> IMSocketMessageInfo::SetPerMessageRead(int nUserID)
{
	return mSocketOpera->SetPerMessageRead(nUserID);
}

QList<QByteArray> IMSocketMessageInfo::SetGroupMessageRead(int nGroupID)
{
	return mSocketOpera->SetGroupMessageRead(nGroupID);
}

//更新消息状态信号
void IMSocketMessageInfo::slotRevUpdateMessage(MessageInfo msgInfo, int nState)
{
	DBUpdateMessageStateInfo(msgInfo.msgID, nState,msgInfo.integral);
	emit sigRevUpdateMessageState(msgInfo, nState);
}

//发送消息信号
void IMSocketMessageInfo::slotSendMessage(MessageInfo msgInfo)
{
	emit sigSendMessage(msgInfo);
}

//与服务器断开连接信号
void IMSocketMessageInfo::slotDisConnectServerServer()
{
	if (mbReConnect)
	{
		emit sigDisConnectServerServer(tr("Disconnected from the server and is reconnecting!"));
		HttpNetWork::HttpNetWorkShareLib *requestUrl = new HttpNetWork::HttpNetWorkShareLib;
		connect(requestUrl, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotReConnectSocketInfoFinished(bool, QString)));
		QString strRequestUrl = StitchSocketParameter(mRequestSocketInfo.strUrl + HTTP_GETSOCKETADDRESS, mRequestSocketInfo.strUserID, mRequestSocketInfo.strPwd);
		requestUrl->getHttpRequest(strRequestUrl);
	}
	else
	{
		if (mReConnectTimer != NULL)
		{
			if (mReConnectTimer->isActive())
			{
				mReConnectTimer->stop();
				delete mReConnectTimer;
				mReConnectTimer = NULL;
			}
		}
	}
}

//重连请求
void IMSocketMessageInfo::slotReConnectSocketInfoFinished(bool bResult, QString result)
{
	if (bResult)
	{
		if (ParseSocketInfo(result))
		{
			if (!OnConnectSocket(mSocketInfo.ServerIP, mSocketInfo.ServerPort, mSocketInfo.ServerToken))
			{
				//启动定时器 重新连接
				qDebug() << tr("Start the timer and reconnect!");
				if (mReConnectTimer == NULL)
				{
					mReConnectTimer = new QTimer(this);
					connect(mReConnectTimer, SIGNAL(timeout()), this, SLOT(slotDisConnectServerServer()));
                    mReConnectTimer->start(5000);
				}
			}
			else
			{
				if (mReConnectTimer != NULL)
				{
					if (mReConnectTimer->isActive())
					{
						mReConnectTimer->stop();
						delete mReConnectTimer;
						mReConnectTimer = NULL;
					}
				}
				emit sigReConnectSuccess();
			}
		}
		else
		{
			if (mReConnectTimer == NULL)
			{
				mReConnectTimer = new QTimer(this);
				connect(mReConnectTimer, SIGNAL(timeout()), this, SLOT(slotDisConnectServerServer()));
			}
			if (!mReConnectTimer->isActive())
			{
                mReConnectTimer->start(5000);
			}
		}
	}
	else
	{
		if (mReConnectTimer == NULL)
		{
			mReConnectTimer = new QTimer(this);
			connect(mReConnectTimer, SIGNAL(timeout()), this, SLOT(slotDisConnectServerServer()));
		}
		if (!mReConnectTimer->isActive())
		{
            mReConnectTimer->start(5000);
		}
	}
}

//获取最后一条消息的时间
QString IMSocketMessageInfo::DBGetMessageLastTime(int nIMUserID)
{
	QString strLastMessage = "";
	if (mSocketDataBase)
	{
		strLastMessage = mSocketDataBase->DBGetMessageLastTime(nIMUserID);
	}
	return strLastMessage;
}

//更新数据库中消息状态
void IMSocketMessageInfo::DBUpdateMessageStateInfo(QByteArray msgID, int nState, int integral)
{
	if (mSocketDataBase)
	{
		mSocketDataBase->UpdataMessageStateInfo(msgID, nState, integral);
	}
}

//更新数据库中视频信息的路径
void IMSocketMessageInfo::DBRestoreLocalPathInMessageInfo(QByteArray msgID, QString localPath)
{
	if (mSocketDataBase)
	{
		mSocketDataBase->RestoreLocalPathInMessageInfo(msgID, localPath);
	}
}

QString IMSocketMessageInfo::DBGetLastSysMsgInMessageInfo(int msgID, QString &fromUser, QString &toUser, int msgType)
{
	if (mSocketDataBase)
	{
		QString strMsg = mSocketDataBase->GetLastSysMsgInMessageList(msgID,  fromUser, toUser, msgType);
		return strMsg;
	}

	return "";
}

int IMSocketMessageInfo::DBGetFromUserOfLastMsgInMessageList(int chatID, int msgType)
{
	if (mSocketDataBase)
	{
		int fromUser = mSocketDataBase->GetFromUserOfLastMsgInMessageList(chatID, msgType);
		return fromUser;
	}

	return 0;
}

void IMSocketMessageInfo::DBUpdateMessageContent(QByteArray msgID, QString content)
{
	if (mSocketDataBase)
	{
		mSocketDataBase->UpdataMessageContent(msgID, content);
	}
}

//数据库插入消息
void IMSocketMessageInfo::DBInsertMessageInfo(int nSelfID, int nUserID, MessageInfo messageInfo)
{
	if (mSocketDataBase)
	{
		mSocketDataBase->InserMessageInfo(nSelfID, nUserID, messageInfo);
	}
}

// Qualifier: 获取全部的消息内容
QMap<QString, QList<MessageInfo> > IMSocketMessageInfo::DBGetAllMessage()
{
	QMap<QString, QList<MessageInfo> > mapMessageInfo;
	if (mSocketDataBase)
	{
		mapMessageInfo = mSocketDataBase->GetAllMessage(nIMUserID);
	}
	return mapMessageInfo;
}

// Qualifier: 获取当天的消息
QMap<QString, QList<MessageInfo> > IMSocketMessageInfo::DBGetRecnetMessage(int userID, int chatID, QString strEndRowId/* = ""*/)
{
	QMap<QString, QList<MessageInfo> > mapMessageInfo;
	if (mSocketDataBase)
	{
		mapMessageInfo = mSocketDataBase->GetDBRecentMessage(userID, chatID, strEndRowId);
	}
	return mapMessageInfo;
}

// Qualifier: 获取当天的消息
QMap<QString, QList<MessageInfo> > IMSocketMessageInfo::DBGetCurrentDayMessage()
{
	QMap<QString, QList<MessageInfo> > mapMessageInfo;
	if (mSocketDataBase)
	{
		mapMessageInfo = mSocketDataBase->GetDBCurrentDayMessage(nIMUserID);
	}
	return mapMessageInfo;
}

// Qualifier: 获取当天对话ID 的消息内容
QMap<QString, QList<MessageInfo> > IMSocketMessageInfo::DBGetCurrentDayMessageWithIMUserID(int nChatID)
{
	QMap<QString, QList<MessageInfo> > mapMessageInfo;
	if (mSocketDataBase)
	{
		mapMessageInfo = mSocketDataBase->GetDBCurrentDayMessageWithIMUserID(nIMUserID, nChatID);
	}
	return mapMessageInfo;
}

// Qualifier: 获取聊天ID下的消息页数
int IMSocketMessageInfo::DBGetMessageRecordPageNum(int nChatID, int nPageNum)
{
	int nNum = 0;
	if (mSocketDataBase)
	{
		nNum = mSocketDataBase->GetDBMessageRecordPageNum(nChatID, nPageNum);
	}
	return nNum;
}

// Qualifier: 获取消息记录数据
QList<MessageInfo> IMSocketMessageInfo::DBGetMessageRecordByPage(int nChatID, int nPage , int messageType)
{
	QList<MessageInfo> listMessageInfo;
	if (mSocketDataBase)
	{
		listMessageInfo = mSocketDataBase->GetDBMessageRecordByPage(nChatID, nPage, messageType);
	}
	return listMessageInfo;
}

// Qualifier: 获取搜索的分页页数
int IMSocketMessageInfo::DBGetSearchMessagePageNum(int nChatID, QString strLikeMsg, int nPageNum)
{
	int nNum = 0;
	if (mSocketDataBase)
	{
		nNum = mSocketDataBase->GetSearchMessagePageNum(nChatID, strLikeMsg, nPageNum);
	}
	return nNum;
}

// Qualifier: 根据页数获取搜索的结果
QList<MessageInfo> IMSocketMessageInfo::DBGetSearchMessageRecordByPage(int nChatID, int nPage)
{
	QList<MessageInfo> listMessageInfo;
	if (mSocketDataBase)
	{
		listMessageInfo = mSocketDataBase->GetDBSearchMessageRecordByPage(nChatID, nPage);
	}
	return listMessageInfo;
}

// Qualifier: 根据MessageID获取数据库消息
MessageInfo IMSocketMessageInfo::DBGetMessageInfoWithMsgID(QString strMsgID)
{
	MessageInfo messageInfo;
	if (mSocketDataBase)
	{
		messageInfo = mSocketDataBase->GetDBMessageInfoWithMsgID(nIMUserID, strMsgID);
	}
	return messageInfo;
}

//字节转换
QString IMSocketMessageInfo::ByteConversion(qint64 nByte)
{
	QString strByte = "";
	if (nByte > 1024)
	{
		double kByte = nByte / 1024;
		if (kByte > 1024)
		{
			double mByte = kByte / 1024;
			if (mByte > 1024)
			{
				double gByte = mByte / 1024;
				strByte = QString::number(gByte, 'f', 2) + "GB";
			}
			else
			{
				strByte = QString::number(mByte, 'f', 2) + "MB";
			}
		}
		else
		{
			strByte = QString::number(kByte, 'f', 2) + "KB";
		}
	}
	else
	{
		strByte = QString::number(nByte) + "B";
	}
	return strByte;
}

//停止重连
void IMSocketMessageInfo::setReConnectState(bool bState)
{
	mbReConnect = bState;
	if (mSocketOpera)
	{
		mSocketOpera->setReConnectState(mbReConnect);
	}
}


void IMSocketMessageInfo::ChangeOldPathCmd()
{
	mSocketDataBase->ChangeOldPath();
}

void IMSocketMessageInfo::ClearUserData()
{
	mSocketDataBase->ClearUserData();
}

void IMSocketMessageInfo::DeleteByMsgId(QString strMsgId)
{
	mSocketDataBase->DeleteByMsgId(strMsgId);
}

void IMSocketMessageInfo::setUserPath(QString strPath)
{
	m_strUserPath = strPath;
}




bool IMSocketMessageInfo::IsLastMsgIdFailure(QString myUserId, QString chatBuddyOrGroupId)
{
	return mSocketDataBase->IsLastMsgIdFailure(myUserId, chatBuddyOrGroupId);
}

void IMSocketMessageInfo::DBMsgReLoad(MessageInfo msgInfo)
{
	if (msgInfo.MessageChildType == Message_PIC)
	{
		DBPicReLoad(msgInfo);
	}
	else if(msgInfo.MessageChildType == Message_VEDIO)
	{
		DBVideoReLoad(msgInfo);
	}
	else if(msgInfo.MessageChildType == Message_AUDIO)
	{
		DBAudioReLoad(msgInfo);
	}
}

void IMSocketMessageInfo::DBPicReLoad(MessageInfo msgInfo)
{
	QJsonParseError jsonError;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8(), &jsonError);
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QVariantMap result = jsonDocument.toVariant().toMap();
			QString strFileType = result["type"].toString();
			//21081107原本为strMsg改为"load"后使用本地路径  更改为保留url直到成功下载 用新的状态unload作为标志
			QString strFileName = m_strUserPath + "/resource/photo/" + QString::number(nIMUserID) + "/" + msgInfo.msgID + strFileType;
			QString strJson = msgInfo.strMsg;
			msgInfo.strMsg = "load";
			if (msgInfo.nFromUserID != nIMUserID)
			{
				emit sigRevMsgInfo(msgInfo, msgInfo.nFromUserID);
				msgInfo.strMsg = strJson;
				msgInfo.MessageState = MESSAGE_STATE_UNLOADED;
				if (mSocketOpera)
				{
					mSocketOpera->OnInsertMessage(msgInfo, msgInfo.nFromUserID);
				}
				if (mSocketDataBase)
				{
					mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nFromUserID, msgInfo);
				}

				HttpNetWork::HttpDownLoadFile *requestHttp = new HttpNetWork::HttpDownLoadFile;

				requestHttp->setData(QVariant::fromValue(msgInfo));
				requestHttp->setProperty("localpath", strFileName);
				connect(requestHttp, SIGNAL(sigDownFinished(bool)), this, SLOT(slotDownFileFinish(bool)));
				requestHttp->StartDownLoadFile(result["url"].toString(), strFileName);
			}
		}
	}
}

void IMSocketMessageInfo::DBVideoReLoad(MessageInfo msgInfo)
{
	QJsonParseError jsonError;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8(), &jsonError);
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QVariantMap result = jsonDocument.toVariant().toMap();
			QString strFileType = result["type"].toString();

			QString strFileName = m_strUserPath + "/resource/vedio/" + QString::number(nIMUserID) + "/" + msgInfo.msgID + strFileType;
			msgInfo.strMsg = "load";
			if (msgInfo.nFromUserID != nIMUserID)
			{
				emit sigRevMsgInfo(msgInfo, msgInfo.nFromUserID);
				msgInfo.strMsg = result["url"].toString();
				msgInfo.MessageState = MESSAGE_STATE_UNLOADED;
				if (mSocketOpera)
				{
					mSocketOpera->OnInsertMessage(msgInfo, msgInfo.nFromUserID);
				}
				if (mSocketDataBase)
				{
					mSocketDataBase->InserMessageInfo(nIMUserID, msgInfo.nFromUserID, msgInfo);
				}
				HttpNetWork::HttpDownLoadFile *requestHttp = new HttpNetWork::HttpDownLoadFile;
				requestHttp->setProperty("localpath", strFileName);
				requestHttp->setData(QVariant::fromValue(msgInfo));
				connect(requestHttp, SIGNAL(sigDownFinished(bool)), this, SLOT(slotDownFileFinish(bool)));
				requestHttp->StartDownLoadFile(result["url"].toString(), strFileName);
			}
		}
	}
}

void IMSocketMessageInfo::DBAudioReLoad(MessageInfo msgInfo)
{
	QJsonDocument doc = QJsonDocument::fromJson(msgInfo.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();

	QString type = map.value("type").toString();
	QString url = map.value("url").toString();
	QString strFileName = m_strUserPath + "/resource/audio/" + QString::number(nIMUserID) + "/" + msgInfo.msgID + type;

	msgInfo.strMsg = strFileName;

	HttpNetWork::HttpDownLoadFile *requestHttp = new HttpNetWork::HttpDownLoadFile;
	requestHttp->setData(QVariant::fromValue(msgInfo));
	requestHttp->setProperty("url", url);
	connect(requestHttp, SIGNAL(sigDownFinished(bool)), this, SLOT(slotDownAudioFinish(bool)));
	requestHttp->StartDownLoadFile(url, strFileName);
}

void IMSocketMessageInfo::OnDealMessageInputting(MessageInfo msgInfo)
{
	emit sigInputting(msgInfo);
}
