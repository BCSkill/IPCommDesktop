<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>OPWindowsManagerShareLib</name>
    <message>
        <location filename="opwindowsmanagersharelib.cpp" line="369"/>
        <location filename="opwindowsmanagersharelib.cpp" line="374"/>
        <source>Recovery failed</source>
        <translation>Recuperación fallida</translation>
    </message>
    <message>
        <location filename="opwindowsmanagersharelib.cpp" line="369"/>
        <source>Base address does not match wallet！</source>
        <translation>La dirección de la base no coincide con la billetera！</translation>
    </message>
    <message>
        <location filename="opwindowsmanagersharelib.cpp" line="374"/>
        <source>Incorrect input information！</source>
        <translation>Información de entrada incorrecta！</translation>
    </message>
</context>
</TS>
