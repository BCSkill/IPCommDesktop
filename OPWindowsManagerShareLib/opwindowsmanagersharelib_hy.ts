<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hy_AM">
<context>
    <name>OPWindowsManagerShareLib</name>
    <message>
        <location filename="opwindowsmanagersharelib.cpp" line="369"/>
        <location filename="opwindowsmanagersharelib.cpp" line="374"/>
        <source>Recovery failed</source>
        <translation>Վերականգնումը չի հաջողվել</translation>
    </message>
    <message>
        <location filename="opwindowsmanagersharelib.cpp" line="369"/>
        <source>Base address does not match wallet！</source>
        <translation>Բազային հասցեն չի համապատասխանում դրամապանակին!</translation>
    </message>
    <message>
        <location filename="opwindowsmanagersharelib.cpp" line="374"/>
        <source>Incorrect input information！</source>
        <translation>Ներածման սխալ տեղեկություն!</translation>
    </message>
</context>
</TS>
