<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="th_TH">
<context>
    <name>OPWindowsManagerShareLib</name>
    <message>
        <location filename="opwindowsmanagersharelib.cpp" line="369"/>
        <location filename="opwindowsmanagersharelib.cpp" line="374"/>
        <source>Recovery failed</source>
        <translation>การกู้คืนล้มเหลว</translation>
    </message>
    <message>
        <location filename="opwindowsmanagersharelib.cpp" line="369"/>
        <source>Base address does not match wallet！</source>
        <translation>ที่อยู่ฐานไม่ตรงกับกระเป๋าเงิน!</translation>
    </message>
    <message>
        <location filename="opwindowsmanagersharelib.cpp" line="374"/>
        <source>Incorrect input information！</source>
        <translation>ข้อมูลอินพุตไม่ถูกต้อง!</translation>
    </message>
</context>
</TS>
