# ----------------------------------------------------
# This file is generated by the Qt Visual Studio Tools.
# ------------------------------------------------------
QT +=core network gui widgets sql webenginewidgets xml quickwidgets multimediawidgets multimedia
INCLUDEPATH += ../SharedLib_mac/common \
    ../SharedLib_mac/openssl/include \
    ../SharedLib_mac/libqrencode/include \
    ../SharedLib_mac/BaseUI/include \
    ../SharedLib_mac/SqlLiteShareLib/include \
    ../SharedLib_mac/IMDataManagerShareLib/include \
    ../SharedLib_mac/IMSocketNetWorkShareLib/include \
    ../SharedLib_mac/SocketNetWorkShareLib/include \
    ../SharedLib_mac/IMSocketDataBaseShareLib/include \
    ../SharedLib_mac/IMDataBaseOperaInfo/include \
    ../SharedLib_mac/HttpNetWorkShareLib/include \
    ../SharedLib_mac/OPDataManager/include \
    ../SharedLib_mac/OPDateBaseShareLib/include \
    ../SharedLib_mac/OPRequestShareLib/include \
    ../SharedLib_mac/IMDownLoadHeaderImg/include \
    ../SharedLib_mac/IMRequestBuddyInfo/include \
    ../SharedLib_mac/AlphabeticalSortSharedLib/include \
    ../SharedLib_mac/CefWidgetShareLib \
    ../SharedLib_mac/VideoPlayShareLib/include \
    ../SharedLib_mac/OPMainMangerShareLib/include \
    ../SharedLib_mac/OPMainWidget/include \
    ../SharedLib_mac/ContactsWidget/include \
    ../SharedLib_mac/ContactsProfileShareLib/include \
    ../SharedLib_mac/ChatWidget/include \
    ../SharedLib_mac/eWalletShareLib/include \
    ../SharedLib_mac/QRenCodeShareLib/include \
    ../SharedLib_mac/CreateAddWidgetShareLib/include \
    ../SharedLib_mac/ScanQRLoginShareLib/include \
    ../SharedLib_mac/OPRecoveryWalletShareLib/include \
    ../SharedLib_mac/LoginDatabaseOperaShareLib/include \
    ../SharedLib_mac/botan/include \
    ../SharedLib_mac/WebObjectShareLib/include \
    ../SharedLib_mac/QxtGlobalShortCut/include \
    ./../SharedLib_mac/SettingsManagerShareLib/include
include(OPWindowsManagerShareLib.pri)
include(shared.pri)
