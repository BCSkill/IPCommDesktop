﻿#include "opwindowsmanagersharelib.h"
#include "QStringLiteralBak.h"
#include "globalmanager.h"
#ifndef Q_OS_WIN
#include "inline_mac.h"
#endif

#include <QMutex>
#include <QWaitCondition>

extern QMutex g_sigParseBuddyInfoMutex;
extern QWaitCondition g_sigParseBuddyInfoWaitCondition;

extern QMutex g_sigParseGroupInfoMutex;
extern QWaitCondition g_sigParseGroupInfoWaitCondition;


OPDatebaseShareLib *gOPDataBaseOpera = NULL;

extern OPDataManager *gOPDataManager;

OPWindowsManagerShareLib::OPWindowsManagerShareLib(QObject *parent) : QObject(parent)
{
	mMainManager = NULL;
	recoveryWidget = NULL;
	cefView = NULL;
	//mLoginWidget = NULL;
	m_pWebObject = NULL;
	m_pWebChannel = NULL;
}

OPWindowsManagerShareLib::~OPWindowsManagerShareLib()
{
	if (gOPDataBaseOpera)
	{
		gOPDataBaseOpera->CloseDB();
	}
	
	if (mMainManager)
	{
		mMainManager->deleteLater();
		mMainManager = NULL;
	}
 	if (recoveryWidget)
 	{
 		recoveryWidget->deleteLater();
 		recoveryWidget = NULL;
 	}
	/*删除cef*/
	if (cefView)
	{
		cefView->deleteLater();
		cefView = NULL;
	}
	if (m_pWebObject)
	{
		m_pWebObject->deleteLater();
		m_pWebObject = NULL;
	}
	if (m_pWebChannel)
	{
		m_pWebChannel->deleteLater();
		m_pWebChannel = NULL;
	}
}

// qualifier: 启动登陆界面
void OPWindowsManagerShareLib::StartLoginWidget(QString strYWYHRequestURL, QString appID)
{
	m_pScanQR = new ScanQRLoginShareLib(this);
	connect(m_pScanQR, SIGNAL(sigCloseLoginWidget()), this, SLOT(slotCloseApp()));
	connect(m_pScanQR, SIGNAL(sigQRLoginSuccess(QVariantMap)), this, SIGNAL(sigLoginSuccess(QVariantMap)));
}

void OPWindowsManagerShareLib::slotCloseApp()
{
	if (m_pScanQR)
	{
		m_pScanQR->deleteLater();
		m_pScanQR = NULL;
	}
	emit sigExit();
}

void OPWindowsManagerShareLib::ChangeLoginWidget(QString msg)
{
	if (m_pScanQR)
	{
		m_pScanQR->showErro(msg);
	}
}

// Qualifier: 销毁登陆窗口
void OPWindowsManagerShareLib::DestroyLoginWidget()
{
	if (m_pScanQR)
	{
		m_pScanQR->HideScanWidget();
		m_pScanQR->deleteLater();
		m_pScanQR = NULL;
	}
}

//启动主界面
void OPWindowsManagerShareLib::StartIMMainWidget(UserInfo userInfo)
{
	if (mMainManager == NULL)
	{
		mMainManager = new OPMainManagerShareLib(this);
		connect(this, SIGNAL(sigRefreshDeviceState()), mMainManager, SLOT(slotRefreshDeviceState()));
		connect(this, SIGNAL(sigUpdateMessageState(MessageInfo, int)), mMainManager, SIGNAL(sigUpdateMessageState(MessageInfo, int)));
		connect(this, SIGNAL(sigSetMainWidgetStatusLabel(QString)), mMainManager, SIGNAL(sigSetMainWidgetStatusLabel(QString)));
		connect(this, SIGNAL(sigNetWarning(bool)), mMainManager, SIGNAL(sigNetWarning(bool)));
		connect(this, SIGNAL(sigUpdateUserInfo(UserInfo)), mMainManager, SLOT(slotInitMainWidget(UserInfo)));
		connect(this, SIGNAL(sigOpenChat(int, QVariant)), mMainManager, SLOT(slotOpenChat(int, QVariant)));
		connect(this, SIGNAL(sigUpdateInfo(int, QVariant)), mMainManager, SLOT(slotUpdateInfo(int, QVariant)));
		connect(this, SIGNAL(sigUserQuitGroup(QString, QString)), mMainManager, SLOT(slotUserQuitGroup(QString, QString)));
		connect(this, SIGNAL(sigAddFriendSuccess(BuddyInfo)), mMainManager, SLOT(slotAddFriendSuccess(BuddyInfo)));
		connect(this, SIGNAL(sigApplyFriend(int, MessageInfo *)), mMainManager, SLOT(slotApplyFriend(int, MessageInfo *)));
		connect(this, SIGNAL(sigAddSuccessGroup(GroupInfo)), mMainManager, SLOT(slotAddSuccessGroup(GroupInfo)));
		connect(this, SIGNAL(sigAddSuccessGroupUserInfo(QString, BuddyInfo)), mMainManager, SLOT(slotAddSuccessGroupUserInfo(QString, BuddyInfo)));
		connect(this, SIGNAL(sigDeleteGroup(QString)), mMainManager, SLOT(slotDeleteGroup(QString)));
		connect(this, SIGNAL(sigGroupNoSpeak(int, int)), mMainManager, SLOT(slotGroupNoSpeak(int, int)));
		connect(this, SIGNAL(sigDeleteFriend(int, QVariant)), mMainManager, SLOT(slotDeleteBuddy(int, QVariant)));
		connect(this, SIGNAL(sigRevOtherDeviceMsg(MessageInfo)), mMainManager, SIGNAL(sigRevOtherDeviceMsg(MessageInfo)));
		connect(mMainManager, SIGNAL(sigExit()), this, SIGNAL(sigExit()));
        connect(this, SIGNAL(sigClickedDock()), mMainManager, SIGNAL(sigClickedDock()));
        connect(this, SIGNAL(sigGlobalMouseMouse()), mMainManager, SIGNAL(sigGlobalMouseMouse()));
		connect(mMainManager, SIGNAL(sigCancel()), this, SIGNAL(sigCancel()));
		connect(mMainManager, SIGNAL(sigCancelandDelete(QString)), this, SIGNAL(sigCancelandDelete(QString)));
		connect(mMainManager, SIGNAL(sigGlobalScreenShot()), this, SIGNAL(sigGlobalScreenShot()));
		connect(mMainManager, SIGNAL(sigCheckUpdate()), this, SIGNAL(sigCheckUpdate()));

#ifndef Q_OS_WIN
        connect(this, SIGNAL(sigSendScreenShotPic()), mMainManager, SLOT(slotSendScreenShotPic()));
#endif
	}
	mMainManager->startMainWidget(userInfo);
}

//销毁主界面
void OPWindowsManagerShareLib::DestroyIMMainWidget()
{
	if (mMainManager)
	{
		mMainManager->deleteLater();
		mMainManager = NULL;
	}
}

//联系人界面插入好友信息
void OPWindowsManagerShareLib::slotInsertBuddyInfo(BuddyInfo buddyInfo)
{
	g_sigParseBuddyInfoMutex.lock();

	if (gDataBaseOpera)
	{
		AlphabeticalSortSharedLib mAlphabeticalSort;
		if (!buddyInfo.strNote.isEmpty())
			buddyInfo.strPingYin = mAlphabeticalSort.GetChineseSpell(buddyInfo.strNote);
		else
			buddyInfo.strPingYin = mAlphabeticalSort.GetChineseSpell(buddyInfo.strNickName);
		gDataBaseOpera->DBInsertBuddyInfo(buddyInfo);
	}
	if (mMainManager)
	{
		mMainManager->slotBuddysManager(BuddyInsert, QVariant::fromValue(buddyInfo));
	}

	g_sigParseBuddyInfoWaitCondition.wakeAll();

	g_sigParseBuddyInfoMutex.unlock();
}

//插入部落信息
void OPWindowsManagerShareLib::slotInsertGroupInfo(GroupInfo groupInfo)
{
	g_sigParseGroupInfoMutex.lock();

	if (gDataBaseOpera)
	{
		gDataBaseOpera->DBInsertGroupInfo(groupInfo);
	}
	if (mMainManager)
	{
		mMainManager->slotGroupsManager(GroupInsert, QVariant::fromValue(groupInfo));
	}

	g_sigParseGroupInfoWaitCondition.wakeAll();

	g_sigParseGroupInfoMutex.unlock();
}

//接收到服务器消息
void OPWindowsManagerShareLib::RevServerMessageInfo(MessageInfo msgInfo)
{
	if (mMainManager)
	{
		mMainManager->RevServerMessage(msgInfo);
	}
}

void OPWindowsManagerShareLib::ReadWalletAddress()
{
	if (gOPDataBaseOpera == NULL)
		gOPDataBaseOpera = new OPDatebaseShareLib;

	UserInfo user = gDataManager->getUserInfo();

#ifdef Q_OS_WIN
	QString strDBPath = gSettingsManager->getUserPath()+"/database/" + QString::number(user.nUserID) + "//ewallet.db";
#else
    QString strDBPath = gSettingsManager->getUserPath() + "/database/" + QString::number(user.nUserID) + "//ewallet.db";
#endif
	if (gOPDataBaseOpera->ConnectDB(strDBPath, "ewallet"))
	{
		QList<WalletInfo> list = gOPDataBaseOpera->DBGetWalletInfo();

		if (!list.isEmpty())  //本地存储的基地地址不为空，判断基地与钱包是否符合。
		{
			WalletInfo localWallet = list.first();   //数据库中只有一条记录。
			WalletInfo netWallet = gOPDataManager->getWalletInfo();
			netWallet.address = netWallet.address.toLower();
			localWallet.address = localWallet.address.toLower();
			if (!netWallet.address.startsWith("0x"))
				netWallet.address = "0x" + netWallet.address;
#ifdef Q_OS_WIN
			QString keyPath = gSettingsManager->getUserPath() + "/wallet//" + QString::number(user.nUserID) + "//pwr.key";
#else
            QString keyPath = gSettingsManager->getUserPath() + "/wallet//" + QString::number(user.nUserID) + "//pwr.key";
#endif
			if (localWallet.address == netWallet.address && QFile(keyPath).exists())
			{
				//合法，启动主界面.
				StartIMMainWidget(gDataManager->getUserInfo());
				mMainManager->setWalletInfo(netWallet);
				emit sigInitBuddyGroup();
				return;
			}
			else
			{
				//IMessageBox::tip(recoveryWidget, tr("登录失败"), tr("基地地址与钱包不匹配！"));
			}
		}

		//如果不符或者本地记录为空，显示恢复钱包的界面。
		if (cefView == NULL)
        {
//#ifdef Q_OS_WIN
//            cefView = new QCefView;
//            cefView->hide();
//            connect(cefView, SIGNAL(sigRecoveryResult(QString)), this, SLOT(slotRecoveryResult(QString)));
//            cefView->InitCefUrl("file:///./html/ethWallet.html");
//			QEventLoop loop;
//			connect(cefView, SIGNAL(sigInitFinished()), &loop, SLOT(quit()));
//			loop.exec();
//#else
            m_pWebChannel = NULL;
            m_pWebObject = NULL;

            m_pWebChannel = new QWebChannel(this);
            m_pWebObject = new WebObjectShareLib(this);
            cefView = new QWebEngineView;
            connect(m_pWebObject, SIGNAL(sigRecoveryResult(QString)), this, SLOT(slotRecoveryResult(QString)));

			/*QString urlName = QDir::currentPath() + ("/html/ethWallet.html");
			QUrl url = QUrl::fromUserInput(urlName);*/
            //QUrl url = "qrc:/html/Resources/html/ethWallet.html";
            QUrl url = QUrl::fromUserInput("qrc:/html/Resources/html/ethWallet.html");
            m_pWebChannel->registerObject("web",m_pWebObject);
            cefView->page()->load(url);
            cefView->page()->setWebChannel(m_pWebChannel);
//#endif

		}

		if (recoveryWidget == NULL)
		{
			recoveryWidget = new RecoveryWidget;
			connect(recoveryWidget, SIGNAL(sigClose()), this, SIGNAL(sigExit()));
			connect(recoveryWidget, SIGNAL(sigRecoveryWord(QString)), this, SLOT(slotRecoveryWallet(QString)));
			connect(recoveryWidget, SIGNAL(sigPrivateKey(QString)), this, SLOT(slotRecoveryWallet(QString)));
		}
		recoveryWidget->show();	
	}
}

void OPWindowsManagerShareLib::slotRecoveryWallet(QString string)
{
	if (cefView)
    {
//#ifdef Q_OS_WIN
//		if (string.contains(" "))    //用空格隔开的是助记词。
//		    cefView->ExecuteJavaScript(QString("recoveryFromWord(\"%1\")").arg(string));
//		else
//			cefView->ExecuteJavaScript(QString("recoveryFromKey(\"%1\")").arg(string));
//#else
        if (string.contains(" "))    //用空格隔开的是助记词。
            cefView->page()->runJavaScript(QString("recoveryFromWord(\"%1\")").arg(string));
        else
            cefView->page()->runJavaScript(QString("recoveryFromKey(\"%1\")").arg(string));
//#endif
	}
}

void OPWindowsManagerShareLib::slotRecoveryResult(QString json)
{	
	QJsonDocument doc = QJsonDocument::fromJson(json.toUtf8());
	QVariantMap map = doc.toVariant().toMap();
	QString result = map.value("result").toString();
	if (result == "true")
	{
		QString address = map.value("address").toString().toLower();
		QString priKey = map.value("privkey").toString();
		priKey = priKey.startsWith("0x") ? priKey : "0x" + priKey;

		WalletInfo wallet;
		wallet.address = address;

		WalletInfo netWallet = gOPDataManager->getWalletInfo();
		netWallet.address = netWallet.address.toLower();
		if (!netWallet.address.startsWith("0x"))
			netWallet.address = "0x" + netWallet.address;

		if (wallet.address == netWallet.address)
		{
			gOPDataBaseOpera->DBInsertWalletInfo(wallet);  //写入数据库。
			//将密钥加密后保存到本地。
			QString key = gDataManager->getUserInfo().strLoginPWD;
#ifdef Q_OS_WIN
			QString keyDir = gSettingsManager->getUserPath() + "/wallet//" + QString::number(gDataManager->getUserInfo().nUserID);
#else
            QString keyDir = gSettingsManager->getUserPath() + "/wallet//" + QString::number(gDataManager->getUserInfo().nUserID);
#endif
			if (!QDir(keyDir).exists())
				QDir::current().mkpath(keyDir);
			QString keyPath = keyDir + "//pwr.key";
			gOPDataManager->encryption(priKey, key, keyPath);
			recoveryWidget->close();
			//启动主界面.
			StartIMMainWidget(gDataManager->getUserInfo());
			//设置钱包信息。
			mMainManager->setWalletInfo(netWallet);
			//发送初始化联系人和部落的信号。
			emit sigInitBuddyGroup();
			//初次登录接收一条欢迎使用星际通讯的通告消息。
			OPRequestShareLib *request = new OPRequestShareLib;
			request->getOnlineMessage();
			delete request;

			/*删除cef*/
			if (cefView)
			{
				cefView->deleteLater();
				cefView = NULL;
			}
			if (m_pWebObject)
			{
				m_pWebObject->deleteLater();
				m_pWebObject = NULL;
			}
			if (m_pWebChannel)
			{
				m_pWebChannel->deleteLater();
				m_pWebChannel = NULL;
			}
		}
		else
		{
			IMessageBox::tip(recoveryWidget, tr("Recovery failed"), tr("Base address does not match wallet！"));
		}
	}
	else
	{
		IMessageBox::tip(recoveryWidget, tr("Recovery failed"), tr("Incorrect input information！"));
	}
}

bool OPWindowsManagerShareLib::JudgeMainWindow()
{
	if (mMainManager != NULL)
	{
		return true;
	}
	return false;
}

void OPWindowsManagerShareLib::RevInputting(MessageInfo msgInfo)
{
	if (mMainManager)
	{
		mMainManager->RevInputting(msgInfo);
	}
}