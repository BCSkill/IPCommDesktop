<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>OPWindowsManagerShareLib</name>
    <message>
        <location filename="opwindowsmanagersharelib.cpp" line="369"/>
        <location filename="opwindowsmanagersharelib.cpp" line="374"/>
        <source>Recovery failed</source>
        <translation>恢复失败</translation>
    </message>
    <message>
        <location filename="opwindowsmanagersharelib.cpp" line="369"/>
        <source>Base address does not match wallet！</source>
        <translation>基地地址与钱包不匹配！</translation>
    </message>
    <message>
        <location filename="opwindowsmanagersharelib.cpp" line="374"/>
        <source>Incorrect input information！</source>
        <translation>输入信息有误！</translation>
    </message>
</context>
</TS>
