<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>OPWindowsManagerShareLib</name>
    <message>
        <location filename="opwindowsmanagersharelib.cpp" line="369"/>
        <location filename="opwindowsmanagersharelib.cpp" line="374"/>
        <source>Recovery failed</source>
        <translation>Recuperação falhou</translation>
    </message>
    <message>
        <location filename="opwindowsmanagersharelib.cpp" line="369"/>
        <source>Base address does not match wallet！</source>
        <translation>Endereço base não corresponde à carteira！</translation>
    </message>
    <message>
        <location filename="opwindowsmanagersharelib.cpp" line="374"/>
        <source>Incorrect input information！</source>
        <translation>Informação incorreta de entrada！</translation>
    </message>
</context>
</TS>
