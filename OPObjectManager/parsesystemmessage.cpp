﻿#include "immainwidget.h"
#include "parsesystemmessage.h"
#include "imtranstype.h"
#include "httpnetworksharelib.h"
#include <QJsonParseError>
#include <QJsonDocument>
#include <QDir>
#include <QApplication>
#include <QJsonObject>
#include <QDebug>
#include "define.h"
#include "QStringLiteralBak.h"
#include "globalmanager.h"
#include "oprequestsharelib.h"
#include "opdatamanager.h"
#include "opdatebasesharelib.h"


#ifndef Q_OS_WIN
#include "inline_mac.h"
#endif


extern OPDatebaseShareLib *gOPDataBaseOpera;

ParseSystemMessage::ParseSystemMessage(QObject *parent)
: QObject(parent)
{

}

ParseSystemMessage::~ParseSystemMessage()
{

}

QString ParseSystemMessage::GetExeDir()
{
	QString path;
	QDir dir;
#ifdef Q_OS_WIN
	return dir.currentPath();
#else
	return getResourcePath();
#endif
}

void ParseSystemMessage::ParseSysMessage(MessageInfo messageInfo)
{
	QString strMessage = messageInfo.strMsg;
	//qDebug() << strMessage;
	QJsonParseError jsonError;
	QJsonDocument jsonDocument = QJsonDocument::fromJson(strMessage.toUtf8(), &jsonError);
	if (jsonError.error == QJsonParseError::NoError)
	{
		if (jsonDocument.isObject())
		{
			QVariantMap result = jsonDocument.toVariant().toMap();
			if (result["CMD"].toString() == "deviceLogout")
			{
				emit sigDeviceLogout();
			}
			else if(result["CMD"].toString() == "sessionOnline" || result["CMD"].toString() == "sessionLogout")
			{
				emit sigRefreshDeviceState();
			}
			else if (result["CMD"].toString() == "applyAddFriend")                //申请添加好友消息
			{
				int userID = result["friendUserId"].toInt();
				QString strMegId = messageInfo.msgID;
				OnDealApplyFriend(result, strMegId);
				//if (!gDataBaseOpera->DBJudgeFriendIsHaveByID(QString::number(userID)))
				//emit sigApplyFriend(QString::number(userID));
			}
			else if (result["CMD"].toString() == "applyAddGroup")                //申请加入部落
			{
				//GroupInfo groupInfo = gDataBaseOpera->DBGetGroupFromID(QString::number(result["groupId"].toInt()));
// 				QString applyID = result["userId"].toString();
// 				QString GroupID = result["groupId"].toString();
				QString strMegId = messageInfo.msgID;
				OnDealApplyGroup(result, strMegId);
				//
				//if (!gDataBaseOpera->DBJudgeGroupIsHaveBuddy(groupInfo.groupId, applyID))
				//emit sigApplyAddGroup(applyID, groupInfo.groupId);
			}
			else if (result["CMD"].toString() == "updateUser")             //更新用户信息
			{
				OnDealUpDateUser(messageInfo, result["user"].toMap());
			}
			else if (result["CMD"].toString() == "updateFriend")             //更新用户信息
			{
				OnDealUpDateFriend(messageInfo, result["friendUserId"].toString());
			}
			else if (result["CMD"].toString() == "updateOwnerNoteInGroup")
			{
				onDealUpdateOwnerNoteInGroup(result);
			}
			else if (result["CMD"].toString() == "updateGroup")          //更新部落信息
			{
				OnDealUpdateGroupInfo(result);
			}
			else if (result["CMD"].toString() == "addFriend")
			{
				OnDealAddFriend(result);
			}
			else if (result["CMD"].toString() == "addUserToGroup")   //申请加入部落同意
			{
				OnDealAddUserToGroup(result);
			}
			else if (result["CMD"].toString() == "userQuitGroup")
			{
				OnDealUserQuitGroup(result);
			}
			else if (result["CMD"].toString() == "deleteGroupUser")
			{
				OnDealUserQuitGroup(result);
			}
			else if (result["CMD"].toString() == "dissolveGroup")                //解散部落
			{
				OnDealDisSolveGroup(result);
			}
			else if (result["CMD"].toString() == "deleteFriend")
			{
				//gDataBaseOpera->DBDeleteBuddyInfoByID(result["friendUserId"].toString());
				BuddyInfo eInfo = gDataBaseOpera->DBGetBuddyInfoByID(result["friendUserId"].toString());
				eInfo.BuddyType = 0;
				gDataBaseOpera->DBInsertBuddyInfo(eInfo);
				emit sigDeleteFriend(BuddyDelete, result["friendUserId"]);
			}
			else if (result["CMD"].toString() == "groupAllUserNoSpeakSet")
			{
				OnDealGroupNoSpeak(result);
			}
			else if (result["CMD"].toString() == "updateGroupType")
			{
				OnDealUpdateGroupType(result);
			}
			else if (result["CMD"].toString() == "updateGroupDesc")
			{
				OnDealUpdateGroupDesc(result);
			}
			else if (result["CMD"].toString() == "updateGroupKey")
			{
				OnDealUpdateGroupKey(result);
			}
			else if (result["CMD"].toString() == "rejectAddUserToGroup")
			{
				QString strMegId = messageInfo.msgID;
				OnDealRejectGroup(result, strMegId);
			}
			else if (result["CMD"].toString() == "rejectAddFriend")
			{
				QString strMegId = messageInfo.msgID;
				OnDealRejectFriend(result, strMegId);
			}
		}
	}
}

//更新好友信息
void ParseSystemMessage::OnDealUpDateUser(MessageInfo msgInfo, QVariantMap mapResult)
{
	BuddyInfo buddyInfo;
	buddyInfo.strHttpAvatar = mapResult["avatar"].toString();
	buddyInfo.strEmail = mapResult["email"].toString();
	buddyInfo.strMobilePhone = mapResult["mobilePhone"].toString();
	buddyInfo.strNickName = mapResult["nickName"].toString();
	buddyInfo.strPingYin = mAlphabeticalSort.GetChineseSpell(buddyInfo.strNickName);
	buddyInfo.strNote = mapResult["note"].toString();
	buddyInfo.strPhone = mapResult["phone"].toString();
	buddyInfo.strSex = mapResult["sex"].toString();
	buddyInfo.strSign = mapResult["sign"].toString();
	buddyInfo.nUserId = mapResult["userId"].toInt();
	buddyInfo.strUserName = mapResult["userName"].toString();
	buddyInfo.nUserType = mapResult["userType"].toInt();
	buddyInfo.disableStrangers = mapResult["disableStrangers"].toInt();
	buddyInfo.strLocalAvatar = gSettingsManager->getUserPath() + "/resource/header/" + mapResult["userId"].toString() + ".jpg";
	//if (buddyInfo.strSex == "F")//默认头像
	//{
	//	buddyInfo.strDefaultAvatar = QString(":/IMChatClient/Resources/imchatclient/female.png");
	//}
	//else
	//{
	//	buddyInfo.strDefaultAvatar = QString(":/PerChat/Resources/person/temp.png");
	//}
	buddyInfo.strDefaultAvatar = QString(":/PerChat/Resources/person/temp.png");
	buddyInfo.BuddyType = 1;

	if (gDataManager)
	{
		UserInfo userInfo = gDataManager->getUserInfo();
		if (gDataBaseOpera)
		{
			if (gDataBaseOpera->DBJudgeBuddyIsHaveByID((QString::number(buddyInfo.nUserId))))
			{
				gDataBaseOpera->DBInsertBuddyInfo(buddyInfo);
			}
			if (gDataBaseOpera->DBJudgeGroupIsHaveBuddy(QString::number(msgInfo.nToUserID), QString::number(buddyInfo.nUserId)))
			{
				gDataBaseOpera->DBInsertGroupBuddyInfo(QString::number(msgInfo.nToUserID), buddyInfo);
			}
		}
		if (buddyInfo.nUserId == userInfo.nUserID)
		{
			userInfo.strUserNickName = mapResult["nickName"].toString();
			userInfo.strUserName = mapResult["nickName"].toString();
			userInfo.strUserAvatarHttp = mapResult["avatar"].toString();
			userInfo.strSex = mapResult["sex"].toString();
			userInfo.strNote = mapResult["note"].toString();
			userInfo.strEmil = mapResult["email"].toString();
			userInfo.strSign = mapResult["sign"].toString();
			userInfo.strPhone = mapResult["phone"].toString();
			userInfo.strUserAvatarLocal = gSettingsManager->getUserPath() + "/resource/header/" + mapResult["userId"].toString() + ".jpg";
			userInfo.disableStrangers = mapResult["disableStrangers"].toInt();
			gDataManager->setUserInfo(userInfo);  //
			emit sigUpdateUserInfo(userInfo);        //更新用户信息
			gDataBaseOpera->DBUpdateAllGroupUserInfo(buddyInfo);

			/*开始下载自己的头像*/
			IMDownLoadHeaderImg *headerImage = new IMDownLoadHeaderImg(this);
			connect(headerImage, SIGNAL(sigUpdateBuddyHeaderImage(int, QPixmap)), this, SLOT(slotUpdateBuddyHeaderImage(int, QPixmap)));
			headerImage->StartDownLoadBuddyeHeaderImage(buddyInfo);
		}
		else
		{
			/*开始下载好友头像*/
			IMDownLoadHeaderImg *headerImage = new IMDownLoadHeaderImg(this);
			connect(headerImage, SIGNAL(sigUpdateBuddyHeaderImage(int, QPixmap)), this, SLOT(slotUpdateBuddyHeaderImage(int, QPixmap)));
			headerImage->StartDownLoadBuddyeHeaderImage(buddyInfo);
		}
	}

	if (IMMainWidget::self())
	{
		gDataBaseOpera->DBUpdateAllGroupNickName(buddyInfo.nUserId,buddyInfo.strNickName);
		emit IMMainWidget::self()->sigUpdateBuddyNickName(buddyInfo.nUserId, buddyInfo.strNickName,-1);
	}
	
}

//更新好友备注
void ParseSystemMessage::OnDealUpDateFriend(MessageInfo msgInfo, QString userID)
{
	//QString str1 = msgInfo["friendID"].toString();
// 	HttpNetWork::HttpNetWorkShareLib *PersonInfo = new HttpNetWork::HttpNetWorkShareLib();
// 	connect(PersonInfo, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotTempFriend(bool, QString)));
// 	AppConfig configInfo = gDataManager->getAppConfigInfo();
// 	QString strRequest = configInfo.MessageServerAddress + HTTP_GETBUDDYINFOBYID + userID;
// 	PersonInfo->getHttpRequest(strRequest);
}

void ParseSystemMessage::onDealUpdateOwnerNoteInGroup(QVariantMap mapResult)
{
	int groupid = mapResult["groupId"].toInt();
	int userid = mapResult["userId"].toInt();
	QString strnote = mapResult["note"].toString();
	gDataBaseOpera->DBUpdateGroupUserNote(groupid, userid, strnote);
	emit IMMainWidget::self()->sigUpdateBuddyNickName(userid, strnote, groupid);
}

//更新好友头像
void ParseSystemMessage::slotUpdateBuddyHeaderImage(int buddyID, QPixmap pix)
{
	IMDownLoadHeaderImg *headerImage = qobject_cast<IMDownLoadHeaderImg*>(sender());
	BuddyInfo buddyInfo = gDataBaseOpera->DBGetBuddyInfoByID(QString::number(buddyID));
	emit sigUpdateInfo(BuddyUpdate, QVariant::fromValue(buddyInfo));
	if (headerImage)
	{
		headerImage->deleteLater();
		headerImage = NULL;
	}
}

//更新部落信息
void ParseSystemMessage::OnDealUpdateGroupInfo(QVariantMap mapResult)
{
	GroupInfo group = gDataBaseOpera->DBGetGroupFromID(mapResult["groupId"].toString());
	group.groupName = mapResult["groupName"].toString();
	group.groupHttpHeadImage = mapResult["avatar"].toString();
// 	groupInfo.groupId = mapResult["groupId"].toString();
// 	groupInfo.groupName = mapResult["groupName"].toString();
	group.groupLoacalHeadImage = gSettingsManager->getUserPath() + tr("/resource/header/groupheader/") + group.groupId + ".jpg";
	group.groupDefaultAvatar = ":/GroupChat/Resources/groupchat/group.png";
// 	groupInfo.noSpeak = mapResult["noSpeak"].toInt();
// 	groupInfo.groupType = mapResult["groupType"].toInt();
// 	groupInfo.groupDesc = mapResult["groupDesc"].toString();
// 	groupInfo.groupKey = mapResult["groupKey"].toString();

	if (gDataBaseOpera)
	{
		GroupInfo oldGroupInfo = gDataBaseOpera->DBGetGroupFromID(group.groupId);
		group.createUserId = oldGroupInfo.createUserId;
		gDataBaseOpera->DBInsertGroupInfo(group);
	}

	IMDownLoadHeaderImg *headerImage = new IMDownLoadHeaderImg;
	connect(headerImage, SIGNAL(sigUpdateBuddyHeaderImage(int, QPixmap)), this, SLOT(slotUpdateGroupHeaderImage(int, QPixmap)));
	headerImage->StartDownLoadGroupHeaderImage(group);
}

//更新部落头像
void ParseSystemMessage::slotUpdateGroupHeaderImage(int buddyID, QPixmap pix)
{
	IMDownLoadHeaderImg *headerImage = qobject_cast<IMDownLoadHeaderImg*>(sender());
	if (gDataBaseOpera)
	{
		GroupInfo groupInfo = gDataBaseOpera->DBGetGroupFromID(QString::number(buddyID));
		emit sigUpdateInfo(GroupUpdate, QVariant::fromValue(groupInfo));
	}
	if (headerImage)
	{
		headerImage->deleteLater();
		headerImage = NULL;
	}
}

//处理用户退出部落
void ParseSystemMessage::OnDealUserQuitGroup(QVariantMap mapResult)
{
	QString groupID = QString::number(mapResult["groupId"].toInt());
	QString userID = QString::number(mapResult["groupUserId"].toInt());
	if (gDataBaseOpera)
	{
		gDataBaseOpera->DBDeleteGroupUserByID(groupID, userID);
	}
	emit sigUserQuitGroup(groupID, userID);
}

//申请添加好友成功
void ParseSystemMessage::OnDealAddFriend(QVariantMap result)
{
	QString strBuddyID = result["friendUserId"].toString();

	//更新好友申请表
	AddApplyMessage stInfo;
	bool bHas = gDataBaseOpera->DBFindAddApplyMessage(0, strBuddyID.toInt(),"", stInfo);
	if (bHas)
	{
		stInfo.iState = 1;//状态改为已接受
		gDataBaseOpera->DBUpdateAddApplyMessage(stInfo);
	}

	if (!gDataBaseOpera->DBJudgeFriendIsHaveByID(strBuddyID))
	{
		HttpNetWork::HttpNetWorkShareLib *PersonInfo = new HttpNetWork::HttpNetWorkShareLib();
		connect(PersonInfo, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotRequestPersonInfoFinished(bool, QString)));
		AppConfig configInfo = gDataManager->getAppConfigInfo();
		QString strRequest = configInfo.MessageServerAddress + HTTP_GETBUDDYINFOBYID + strBuddyID;
		PersonInfo->getHttpRequest(strRequest);
	}
	else
	{
		emit sigAddFriendSuccess(gDataBaseOpera->DBGetBuddyInfoByID(strBuddyID));
	}
	//获取基地
	OPRequestShareLib *request = new OPRequestShareLib;
	request->setProperty("UserId", strBuddyID);
	connect(request, SIGNAL(sigBuddyAddressInfo(AddressInfo)), this, SLOT(slotInsertAddressInfo(AddressInfo)));
	connect(request, SIGNAL(sigBuddyAddressInfo(AddressInfo)), request, SLOT(deleteLater()));
	request->getBuddyAddressInfo(strBuddyID);
}

void ParseSystemMessage::slotRequestPersonInfoFinished(bool bResult, QString result)
{
	if (bResult)
	{
		QJsonParseError jsonError;
		BuddyInfo buddyInfo;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(result.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QVariantMap result = jsonDocument.toVariant().toMap();
				if (result["result"].toString() == "success")
				{
					QVariantMap usrInfo = result["user"].toMap();
					if (!usrInfo.isEmpty())
					{
						QString strPinYin;
						buddyInfo.strHttpAvatar = usrInfo["avatar"].toString();
						buddyInfo.strEmail = usrInfo["email"].toString();
						buddyInfo.strMobilePhone = usrInfo["mobilePhone"].toString();
						buddyInfo.strNickName = usrInfo["nickName"].toString();
						buddyInfo.strNote = usrInfo["note"].toString();
						buddyInfo.strPhone = usrInfo["phone"].toString();
						buddyInfo.strSex = usrInfo["sex"].toString();
						buddyInfo.strSign = usrInfo["sign"].toString();
						buddyInfo.nUserId = usrInfo["userId"].toInt();
						buddyInfo.strUserName = usrInfo["userName"].toString();
						buddyInfo.nUserType = usrInfo["userType"].toInt();
						buddyInfo.disableStrangers = usrInfo["disableStrangers"].toInt();
						buddyInfo.BuddyType = 1;
						buddyInfo.strLocalAvatar = gSettingsManager->getUserPath() + "/resource/header/" + usrInfo["userId"].toString() + ".jpg";
						//if (buddyInfo.strSex == "F")//默认头像
						//{
						//	buddyInfo.strDefaultAvatar = QString(":/IMChatClient/Resources/imchatclient/female.png");
						//}
						//else
						//{
						//	buddyInfo.strDefaultAvatar = QString(":/PerChat/Resources/person/temp.png");
						//}
						buddyInfo.strDefaultAvatar = QString(":/PerChat/Resources/person/temp.png");
						//根据名称获取首字母
						if (!buddyInfo.strNote.isEmpty())
							strPinYin = mAlphabeticalSort.GetChineseSpell(buddyInfo.strNote);
						else
							strPinYin = mAlphabeticalSort.GetChineseSpell(buddyInfo.strNickName);
						buddyInfo.strPingYin = strPinYin;
						if (gDataBaseOpera)
						{
							gDataBaseOpera->DBJudgeBuddyIsHaveByID(QString::number(buddyInfo.nUserId));
							BuddyInfo StTmpInfo = gDataBaseOpera->DBGetBuddyInfoByID(QString::number(buddyInfo.nUserId));
							buddyInfo.strNote = StTmpInfo.strNote;
							gDataBaseOpera->DBInsertBuddyInfo(buddyInfo);
						}
					}
				}
			}
		}
		emit sigAddFriendSuccess(buddyInfo);
	}
}

void ParseSystemMessage::slotRequestGroupInfoFinished(bool bResult, QString result)
{
	if (bResult)
	{
		QJsonParseError jsonError;
		GroupInfo groupInfo;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(result.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QVariantMap result = jsonDocument.toVariant().toMap();
				if (result["result"].toString() == "success")
				{
					QVariantMap resultInfo = result["group"].toMap();
					groupInfo.groupHttpHeadImage = resultInfo["avatar"].toString();
					groupInfo.createTime = resultInfo["createTime"].toString();
					groupInfo.createUserId = resultInfo["createUserId"].toString();
					groupInfo.groupId = resultInfo["groupId"].toString();
#ifdef Q_OS_WIN
					QString strPath = gSettingsManager->getUserPath();
					groupInfo.groupLoacalHeadImage = strPath + "/resource/header/groupheader/" + groupInfo.groupId + ".jpg";
#else
					groupInfo.groupLoacalHeadImage = gSettingsManager->getUserPath() + "/resource/header/groupheader/" + groupInfo.groupId + ".jpg";
#endif
					groupInfo.groupDefaultAvatar = tr(":/GroupChat/Resources/groupchat/group.png");
					groupInfo.groupName = resultInfo["groupName"].toString();
					groupInfo.noSpeak = resultInfo["noSpeak"].toInt();
					groupInfo.groupType = resultInfo["groupType"].toInt();
					groupInfo.groupDesc = resultInfo["groupDesc"].toString();
					groupInfo.groupKey = resultInfo["groupKey"].toString();
					gDataBaseOpera->DBInsertGroupInfo(groupInfo);
				}
			}
		}
		emit sigAddSuccessGroup(groupInfo);
	}
}

//申请加入部落同意
void ParseSystemMessage::OnDealAddUserToGroup(QVariantMap result)
{
	QString strGroupID = result["groupId"].toString();
	QString groupUserID = result["groupUserId"].toString();
	QString manaId = result["manaId"].toString();

	//更新好友申请表
	AddApplyMessage stInfo;
	bool bHas = gDataBaseOpera->DBFindAddApplyMessage(1, groupUserID.toInt(), strGroupID, stInfo);
	if (bHas)
	{
		stInfo.iState = 1;//状态改为已接受
		gDataBaseOpera->DBUpdateAddApplyMessage(stInfo);
	}

	UserInfo userInfo = gDataManager->getUserInfo();
	//判断添加上的userId是否是登录用户
	if (groupUserID == QString::number(userInfo.nUserID))
	{
		if (!gDataBaseOpera->DBJudgeGroupIsHaveByID(strGroupID))
		{
			HttpNetWork::HttpNetWorkShareLib *PersonInfo = new HttpNetWork::HttpNetWorkShareLib();
			connect(PersonInfo, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotRequestGroupInfoFinished(bool, QString)));
			UserInfo userInfo = gDataManager->getUserInfo();
			AppConfig appConf = gDataManager->getAppConfigInfo();
			QString strResult = appConf.MessageServerAddress + HTTP_GETGROUPINFOBYGROUPID + QString("?userId=") + QString::number(userInfo.nUserID) + "&passWord=" + userInfo.strUserPWD + "&groupId=" + strGroupID;
			PersonInfo->getHttpRequest(strResult);
		}
	}
	else
	{
		//加群的是其他人，先获取群信息和部落成员，如果群信息为空或者群里没有人，就不再进行处理。以此作为首次加载的依据。
		GroupInfo group = gDataBaseOpera->DBGetGroupFromID(strGroupID);
		QList<BuddyInfo> buddyList = gDataBaseOpera->DBGetGroupBuddyInfoFromID(strGroupID);
		if (buddyList.isEmpty() || group.groupName.isEmpty())
		{
			emit sigApplyFriend(1);//0时增加消息提醒1时仅更新列表
		}
		else
		{
			if (gDataBaseOpera->DBJudgeGroupIsHaveBuddy(strGroupID, groupUserID))
			{
				return;
			}
			/*部落添加成员成功 请求部落信息*/
			HttpNetWork::HttpNetWorkShareLib *PersonInfo = new HttpNetWork::HttpNetWorkShareLib();
			connect(PersonInfo, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotRecvAddGroupUser(bool, QString)));
			AppConfig configInfo = gDataManager->getAppConfigInfo();
			QString strRequest = configInfo.MessageServerAddress + HTTP_GETBUDDYINFOBYID + groupUserID;

			QVariantMap map;
			map.insert("groupID", strGroupID);
			map.insert("manaID", manaId);
			QJsonDocument doc = QJsonDocument::fromVariant(map);
			PersonInfo->setObjectName(doc.toJson());

			PersonInfo->getHttpRequest(strRequest);
		}
	}
}

void ParseSystemMessage::slotRecvAddGroupUser(bool success, QString data)
{
	if (success)
	{
		HttpNetWork::HttpNetWorkShareLib *act = qobject_cast<HttpNetWork::HttpNetWorkShareLib*>(sender());
		if (act)
		{
			QString strMap = act->objectName();
			QVariantMap map = QJsonDocument::fromJson(strMap.toUtf8()).toVariant().toMap();

			QJsonDocument document = QJsonDocument::fromJson(data.toUtf8());
			QJsonObject object = document.object().take("user").toObject();

			BuddyInfo info;
			info.strHttpAvatar = object.take("avatar").toString();
			info.strNickName = object.take("nickName").toString();
			info.strMobilePhone = object.take("nickName").toString();
			info.strPhone = object.take("phone").toString();
			info.strUserName = object.take("userName").toString();
			info.strSign = object.take("sign").toString();
			info.strSex = object.take("sex").toString();
			info.nUserId = object.take("userId").toInt();
			info.nUserType = object.take("userType").toInt();
			info.BuddyType = 0;
			info.strDefaultAvatar = QString(":/PerChat/Resources/person/temp.png");
#ifdef Q_OS_WIN
			QString strPath = gSettingsManager->getUserPath();
			info.strLocalAvatar = strPath + "/resource/header/" + QString::number(info.nUserId) + ".jpg";
#else
			info.strLocalAvatar = gSettingsManager->getUserPath() + "/resource/header/" + QString::number(info.nUserId) + ".jpg";
#endif
			info.disableStrangers = object.take("disableStrangers").toInt();
			gDataBaseOpera->DBInsertGroupBuddyInfo(map.value("groupID").toString(), info);


			emit sigAddSuccessGroupUserInfo(strMap, info);
		}
	}
}

//解散部落
void ParseSystemMessage::OnDealDisSolveGroup(QVariantMap result)
{
	QString groupID = result["groupId"].toString();
	if (gDataBaseOpera)
	{
		gDataBaseOpera->DBDeleteGroupInfoByID(groupID);
	}
	emit sigDeleteGroup(groupID);
}

void ParseSystemMessage::OnDealGroupNoSpeak(QVariantMap result)
{
	int groupId = result.value("groupId").toInt();
	int noSpeak = result.value("noSpeak").toInt();

	//修改数据库。
	GroupInfo group = gDataBaseOpera->DBGetGroupFromID(QString::number(groupId));
	group.noSpeak = noSpeak;
	gDataBaseOpera->DBUpdateGroupInfo(group);

	//通知聊天界面。
	emit sigGroupNoSpeak(groupId, noSpeak);
}

void ParseSystemMessage::OnDealUpdateGroupType(QVariantMap result)
{
	int groupId = result.value("groupId").toInt();
	int groupType = result.value("groupType").toInt();

	//修改数据库。
	GroupInfo group = gDataBaseOpera->DBGetGroupFromID(QString::number(groupId));
	group.groupType = groupType;
	gDataBaseOpera->DBUpdateGroupInfo(group);

	emit sigUpdateInfo(GroupUpdate, QVariant::fromValue(group));
}

void ParseSystemMessage::OnDealUpdateGroupDesc(QVariantMap result)
{
	int groupId = result.value("groupId").toInt();
	QString groupDesc = result.value("groupDesc").toString();

	//修改数据库。
	GroupInfo group = gDataBaseOpera->DBGetGroupFromID(QString::number(groupId));
	group.groupDesc = groupDesc;
	gDataBaseOpera->DBUpdateGroupInfo(group);

	emit sigUpdateInfo(GroupUpdate, QVariant::fromValue(group));
}

void ParseSystemMessage::OnDealUpdateGroupKey(QVariantMap result)
{
	int groupId = result.value("groupId").toInt();
	QString groupKey = result.value("groupKey").toString();

	//修改数据库。
	GroupInfo group = gDataBaseOpera->DBGetGroupFromID(QString::number(groupId));
	group.groupKey = groupKey;
	gDataBaseOpera->DBUpdateGroupInfo(group);

	emit sigUpdateInfo(GroupUpdate, QVariant::fromValue(group));
}

//接收好友请求
void ParseSystemMessage::OnDealApplyFriend(QVariantMap result, QString strMegId)
{
	// 	QString strBuddyID = result["friendUserId"].toString();
	// 	if (!gDataBaseOpera->DBJudgeFriendIsHaveByID(strBuddyID))
	// 	{
	// 		HttpNetWork::HttpNetWorkShareLib *PersonInfo = new HttpNetWork::HttpNetWorkShareLib();
	// 		connect(PersonInfo, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotRequestPersonInfoFinished(bool, QString)));
	// 		AppConfig configInfo = gDataManager->getAppConfigInfo();
	// 		QString strRequest = configInfo.MessageServerAddress + HTTP_GETBUDDYINFOBYID + strBuddyID;
	// 		PersonInfo->getHttpRequest(strRequest);
	// 	}
	QDateTime local(QDateTime::currentDateTime());
	QString localTime = local.toString("yyyyMMddhhmmss");

	AddApplyMessage stInfo;
	stInfo.strMegId = strMegId;
	stInfo.iType = 0;
	stInfo.iId = result["friendUserId"].toInt();
	stInfo.strMessage = result["leaveMessage"].toString();
	stInfo.iState = 0;
	stInfo.strDate = localTime;
	stInfo.strGroupId = "";
	stInfo.iRead = 0;

	//查找数据库是否已有此申请
	AddApplyMessage stFind;
	bool  bHad = gDataBaseOpera->DBFindAddApplyMessage(stInfo.iType, stInfo.iId,"", stFind);
	if (bHad)
	{
		gDataBaseOpera->DBUpdateAddApplyMessage(stInfo);
	}
	else
	{
		gDataBaseOpera->DBInsertAddApplyMessage(stInfo);
	}
	//BuddyType = 0 提前保存用户信息
	QString strBuddyID = result["friendUserId"].toString();
	if (!gDataBaseOpera->DBJudgeFriendIsHaveByID(strBuddyID))
	{
		HttpNetWork::HttpNetWorkShareLib *PersonInfo = new HttpNetWork::HttpNetWorkShareLib();
		connect(PersonInfo, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotTempFriend(bool, QString)));
		AppConfig configInfo = gDataManager->getAppConfigInfo();
		QString strRequest = configInfo.MessageServerAddress + HTTP_GETBUDDYINFOBYID + strBuddyID;
		PersonInfo->getHttpRequest(strRequest);
	}
	else
	{
		MessageInfo messageInfo;
		messageInfo.nFromUserID = stInfo.iId;
		messageInfo.MsgDeliverType = 0; //好友申请
		emit sigApplyFriend(0, &messageInfo);//0时增加消息提醒1时仅更新列表
	}
	return;
}

void ParseSystemMessage::slotTempFriend(bool bResult, QString result)
{
	if (bResult)
	{
		QJsonParseError jsonError;
		BuddyInfo buddyInfo;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(result.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QVariantMap result = jsonDocument.toVariant().toMap();
				if (result["result"].toString() == "success")
				{
					//暂定 以后要从中取消息字段
					QVariantMap usrInfo = result["user"].toMap();
					if (!usrInfo.isEmpty())
					{
						QString strPinYin;
						buddyInfo.strHttpAvatar = usrInfo["avatar"].toString();
						buddyInfo.strEmail = usrInfo["email"].toString();
						buddyInfo.strMobilePhone = usrInfo["mobilePhone"].toString();
						buddyInfo.strNickName = usrInfo["nickName"].toString();
						buddyInfo.strNote = usrInfo["note"].toString();
						buddyInfo.strPhone = usrInfo["phone"].toString();
						buddyInfo.strSex = usrInfo["sex"].toString();
						buddyInfo.strSign = usrInfo["sign"].toString();
						buddyInfo.nUserId = usrInfo["userId"].toInt();
						buddyInfo.strUserName = usrInfo["userName"].toString();
						buddyInfo.nUserType = usrInfo["userType"].toInt();
						buddyInfo.disableStrangers = usrInfo["disableStrangers"].toInt();
						buddyInfo.BuddyType = 0;
						buddyInfo.strLocalAvatar = gSettingsManager->getUserPath() + "/resource/header/" + usrInfo["userId"].toString() + ".jpg";
						//if (buddyInfo.strSex == "F")//默认头像
						//{
						//	buddyInfo.strDefaultAvatar = QString(":/IMChatClient/Resources/imchatclient/female.png");
						//}
						//else
						//{
						//	buddyInfo.strDefaultAvatar = QString(":/PerChat/Resources/person/temp.png");
						//}
						buddyInfo.strDefaultAvatar = QString(":/PerChat/Resources/person/temp.png");
						//根据名称获取首字母
						if (!buddyInfo.strNote.isEmpty())
							strPinYin = mAlphabeticalSort.GetChineseSpell(buddyInfo.strNote);
						else
							strPinYin = mAlphabeticalSort.GetChineseSpell(buddyInfo.strNickName);
						buddyInfo.strPingYin = strPinYin;
						if (gDataBaseOpera)
						{
							gDataBaseOpera->DBInsertBuddyInfo(buddyInfo);
						}
					}
				}
			}
		}
		MessageInfo messageInfo;
		messageInfo.nFromUserID = buddyInfo.nUserId;
		messageInfo.MsgDeliverType = 0; //假裝好友申请
		emit sigApplyFriend(0, &messageInfo);//0时增加消息提醒1时仅更新列表
	}
}

//接收好友请求
void ParseSystemMessage::OnDealApplyGroup(QVariantMap result, QString strMegId)
{
	QDateTime local(QDateTime::currentDateTime());
	QString localTime = local.toString("yyyyMMddhhmmss");
// 	QString applyID = result["userId"].toString();
// 	QString GroupID = result["groupId"].toString();
	AddApplyMessage stInfo;
	stInfo.strMegId = strMegId;
	stInfo.iType = 1;//0是好友申请 1是群申请
	stInfo.iId = result["userId"].toInt();
	stInfo.strMessage = result["leaveMessage"].toString();
	stInfo.strGroupId = result["groupId"].toString();
	stInfo.iState = 0;
	stInfo.strDate = localTime;
	stInfo.iRead = 0;

	//查找数据库是否已有此申请
	AddApplyMessage stFind;
	bool  bHad = gDataBaseOpera->DBFindAddApplyMessage(stInfo.iType, stInfo.iId, stInfo.strGroupId, stFind);
	if (bHad)
	{
		gDataBaseOpera->DBUpdateAddApplyMessage(stInfo);
	}
	else
	{
		gDataBaseOpera->DBInsertAddApplyMessage(stInfo);
	}
	//BuddyType = 0 提前保存用户信息
	QString strBuddyID = result["userId"].toString();
	if (!gDataBaseOpera->DBJudgeFriendIsHaveByID(strBuddyID))
	{
		HttpNetWork::HttpNetWorkShareLib *PersonInfo = new HttpNetWork::HttpNetWorkShareLib();
		connect(PersonInfo, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotTempFriend(bool, QString)));
		AppConfig configInfo = gDataManager->getAppConfigInfo();
		QString strRequest = configInfo.MessageServerAddress + HTTP_GETBUDDYINFOBYID + strBuddyID;
		PersonInfo->getHttpRequest(strRequest);
	}
	else
	{
		MessageInfo messageInfo;
		messageInfo.nFromUserID = stInfo.iId;
		messageInfo.MsgDeliverType = 1; //群组申请
		emit sigApplyFriend(0, &messageInfo);//0时增加消息提醒1时仅更新列表
	}
	return;
}
void ParseSystemMessage::OnDealRejectFriend(QVariantMap result, QString strMegId)
{
	//result["applyUserId"].toInt();
	//result["groupId"].toInt();
	QDateTime local(QDateTime::currentDateTime());
	QString localTime = local.toString("yyyyMMddhhmmss");
	AddApplyMessage stInfo;
	stInfo.strMegId = strMegId;
	stInfo.iType = 0;//0是好友申请 1是群申请
	stInfo.iId = result["applyUserId"].toInt();
	stInfo.strMessage = result["leaveMessage"].toString();
	stInfo.strGroupId = "";
	stInfo.iState = 2;
	stInfo.strDate = localTime;

	//查找数据库是否已有此申请
	AddApplyMessage stFind;
	bool  bHad = gDataBaseOpera->DBFindAddApplyMessage(stInfo.iType, stInfo.iId, stInfo.strGroupId, stFind);
	if (bHad)
	{
		stInfo.strMessage = stFind.strMessage;
		gDataBaseOpera->DBUpdateAddApplyMessage(stInfo);
	}
	emit sigApplyFriend(1);//0时增加消息提醒1时仅更新列表
}
void ParseSystemMessage::OnDealRejectGroup(QVariantMap result, QString strMegId)
{
// 	result["applyUserId"].toInt();
// 	result["groupId"].toInt();
	QDateTime local(QDateTime::currentDateTime());
	QString localTime = local.toString("yyyyMMddhhmmss");
	AddApplyMessage stInfo;
	stInfo.strMegId = strMegId;
	stInfo.iType = 1;//0是好友申请 1是群申请
	stInfo.iId = result["applyUserId"].toInt();
	stInfo.strMessage = result["leaveMessage"].toString();
	stInfo.strGroupId = result["groupId"].toString();
	stInfo.iState = 2;
	stInfo.strDate = localTime;

	//查找数据库是否已有此申请
	AddApplyMessage stFind;
	bool  bHad = gDataBaseOpera->DBFindAddApplyMessage(stInfo.iType, stInfo.iId, stInfo.strGroupId, stFind);
	if (bHad)
	{
		stInfo.strMessage = stFind.strMessage;
		gDataBaseOpera->DBUpdateAddApplyMessage(stInfo);
	}
	emit sigApplyFriend(1);//0时增加消息提醒1时仅更新列表
}

void ParseSystemMessage::slotInsertAddressInfo(AddressInfo addInfo)
{
	OPRequestShareLib *act = qobject_cast<OPRequestShareLib*>(sender());
	QVariant vType = act->property("UserId");
	QString strBuddyId = vType.value<QString>();
	addInfo.userID = strBuddyId;
	gOPDataBaseOpera->DBInsertAddressInfo(addInfo);
	//pPer->show();
	//pPer->activateWindow();
}