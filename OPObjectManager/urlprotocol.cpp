﻿#include "urlprotocol.h"

UrlProtocol::UrlProtocol(QObject *parent)
	: QObject(parent)
{
	manager = new QNetworkAccessManager(this);
	connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(slotFinished(QNetworkReply*)));
}

UrlProtocol::~UrlProtocol()
{

}

void UrlProtocol::setURL(QString protocol)
{
	//protocol示例：ospp://imchatclient&xxx=111
	if (protocol.startsWith("openplanet://openplanet.io/"))
	{
		QString commond = protocol.split("/").last();
		QString group = commond.split("?").last();
		if (group.startsWith("groupKey"))
		{
			QString groupKey = group.split("=").last();

			if (!groupKey.isEmpty())
			{
				//发送申请部落的消息。
				QUrl request("http://127.0.0.1:12306/ApplyGroup?GroupKey=" + groupKey);
				manager->get(QNetworkRequest(request));
				return;
			}
		}
	}

	//url不合法，退出程序。
	exit(0);
}

void UrlProtocol::slotFinished(QNetworkReply* reply)
{
	if (reply->error() == QNetworkReply::NoError)
	{
		//qDebug() << reply->readAll();
	}
	else
	{
		//qDebug() << reply->errorString();
	}

	exit(0);
}
