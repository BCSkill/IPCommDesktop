<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="th_TH">
<context>
    <name>OPObjectManager</name>
    <message>
        <location filename="opobjectmanager.cpp" line="205"/>
        <location filename="opobjectmanager.cpp" line="881"/>
        <source>Warning</source>
        <translation>การเตือน</translation>
    </message>
    <message>
        <source>Openplanet is logged in, do you want to restart the program?</source>
        <translation type="vanished">การสื่อสารระหว่างดวงดาวเข้าสู่ระบบคุณต้องการเริ่มโปรแกรมใหม่หรือไม่</translation>
    </message>
    <message>
        <source>Telecomm is logged in, do you want to restart the program?</source>
        <translation type="vanished">การสื่อสารโทรคมนาคมถูกล็อกอินคุณต้องการเริ่มโปรแกรมใหม่หรือไม่</translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="205"/>
        <source>OpenPlanet is logged in, do you want to restart the program?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="426"/>
        <location filename="opobjectmanager.cpp" line="868"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="426"/>
        <source>Already the latest version</source>
        <translation>เป็นเวอร์ชั่นล่าสุดแล้ว</translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="550"/>
        <source>Requesting personal information failed!</source>
        <translation>การขอข้อมูลส่วนบุคคลล้มเหลว!</translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="868"/>
        <source>Request to add group success!</source>
        <translation>ขอเพิ่มความสำเร็จของกลุ่ม!</translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="881"/>
        <source>Failed to get group information, please try again~</source>
        <translation>ไม่สามารถรับข้อมูลกลุ่มได้โปรดลองอีกครั้ง</translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="678"/>
        <source>OpenPlanet  </source>
        <translation>การสื่อสารระหว่างดวงดาว</translation>
    </message>
    <message>
        <source>Request to add tribal success!</source>
        <translation type="vanished">ขอเพิ่มความสำเร็จของเผ่า!</translation>
    </message>
    <message>
        <source>Failed to get tribal information, please try again~</source>
        <translation type="vanished">ไม่สามารถรับข้อมูลเกี่ยวกับเผ่าได้โปรดลองอีกครั้ง</translation>
    </message>
</context>
<context>
    <name>ParseSystemMessage</name>
    <message>
        <location filename="parsesystemmessage.cpp" line="282"/>
        <source>/resource/header/groupheader/</source>
        <translation></translation>
    </message>
    <message>
        <location filename="parsesystemmessage.cpp" line="450"/>
        <source>:/GroupChat/Resources/groupchat/group.png</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="opobjectmanager.cpp" line="245"/>
        <location filename="opobjectmanager.cpp" line="277"/>
        <source>简体中文</source>
        <translation></translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="248"/>
        <location filename="opobjectmanager.cpp" line="286"/>
        <source>русский</source>
        <translation></translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="251"/>
        <location filename="opobjectmanager.cpp" line="294"/>
        <source>Հայերեն</source>
        <translation></translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="254"/>
        <location filename="opobjectmanager.cpp" line="302"/>
        <source>Español</source>
        <translation></translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="257"/>
        <location filename="opobjectmanager.cpp" line="310"/>
        <source>Português</source>
        <translation></translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="260"/>
        <location filename="opobjectmanager.cpp" line="318"/>
        <source>ไทย</source>
        <translation></translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="264"/>
        <source>English</source>
        <translation></translation>
    </message>
</context>
</TS>
