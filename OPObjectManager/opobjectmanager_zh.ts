<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>OPObjectManager</name>
    <message>
        <source>Telecomm is logged in, do you want to restart the program?</source>
        <translation type="vanished">Telecomm已登录，是否要重启程序？</translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="205"/>
        <source>OpenPlanet is logged in, do you want to restart the program?</source>
        <translation>星际通讯已登录，是否要重启程序？</translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="426"/>
        <location filename="opobjectmanager.cpp" line="868"/>
        <source>Notice</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="426"/>
        <source>Already the latest version</source>
        <translation>已经是最新版本</translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="550"/>
        <source>Requesting personal information failed!</source>
        <translation>请求个人信息失败!</translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="868"/>
        <source>Request to add group success!</source>
        <translation>请求添加群组成功！</translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="881"/>
        <source>Failed to get group information, please try again~</source>
        <translation>获取群组信息失败，请重试</translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="678"/>
        <source>OpenPlanet  </source>
        <translation>星际通讯  </translation>
    </message>
    <message>
        <source>Request to add tribal success!</source>
        <translation type="vanished">请求添加部落成功！</translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="205"/>
        <location filename="opobjectmanager.cpp" line="881"/>
        <source>Warning</source>
        <translation>注意</translation>
    </message>
    <message>
        <source>Openplanet is logged in, do you want to restart the program?</source>
        <translation type="vanished">星际通讯已登录，是否要重启程序？</translation>
    </message>
    <message>
        <source>Failed to get tribal information, please try again~</source>
        <translation type="vanished">获取部落信息失败，请重试~</translation>
    </message>
</context>
<context>
    <name>ParseSystemMessage</name>
    <message>
        <location filename="parsesystemmessage.cpp" line="282"/>
        <source>/resource/header/groupheader/</source>
        <translation></translation>
    </message>
    <message>
        <location filename="parsesystemmessage.cpp" line="450"/>
        <source>:/GroupChat/Resources/groupchat/group.png</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="opobjectmanager.cpp" line="245"/>
        <location filename="opobjectmanager.cpp" line="277"/>
        <source>简体中文</source>
        <translation></translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="248"/>
        <location filename="opobjectmanager.cpp" line="286"/>
        <source>русский</source>
        <translation></translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="251"/>
        <location filename="opobjectmanager.cpp" line="294"/>
        <source>Հայերեն</source>
        <translation></translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="254"/>
        <location filename="opobjectmanager.cpp" line="302"/>
        <source>Español</source>
        <translation></translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="257"/>
        <location filename="opobjectmanager.cpp" line="310"/>
        <source>Português</source>
        <translation></translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="260"/>
        <location filename="opobjectmanager.cpp" line="318"/>
        <source>ไทย</source>
        <translation></translation>
    </message>
    <message>
        <location filename="opobjectmanager.cpp" line="264"/>
        <source>English</source>
        <translation></translation>
    </message>
</context>
</TS>
