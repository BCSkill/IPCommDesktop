﻿#include "opobjectmanager.h"
#include "readappconfig.h"
#include "opdatamanager.h"
#include "QStringLiteralBak.h"
#include "settingsmanager.h"
#include "urlprotocol.h"
#include <QResource>

#ifndef Q_OS_WIN
#include "inline_mac.h"
#include "oescreenshot.h"
#endif
#ifdef Q_OS_WIN
#undef LONG long long
#include <TlHelp32.h>
#include <tchar.h>
#endif
#include <QProcess>


IMDataBaseOperaInfo *gDataBaseOpera = NULL;
IMDataManagerShareLib *gDataManager = NULL;
IMSocketMessageInfo *gSocketMessage = NULL;
SettingsManager* gSettingsManager = NULL;
OPDataManager  *gOPDataManager = NULL;

extern bool gbIsInitCef;

QString gI18NLocale = "en";//html多国语言切换用
QString gThemeStyle = "Default"; //切换主题

/*记录日志*/
void LogInfo(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
	QString text;
	switch (type)
	{
	case QtDebugMsg:
		text = QString("Debug:");
		break;
	case QtWarningMsg:
		text = QString("Warning:");
		break;
	case QtCriticalMsg:
		text = QString("Critical:");
		break;
	case QtFatalMsg:
		text = QString("Fatal:");
		break;
	}

	QString context_info = QString("File:(%1) Line:(%2)").arg(QString(context.file)).arg(context.line);
	QString current_date_time = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss ddd");
	QString current_date = QString("(%1)").arg(current_date_time);
	QString message = QString("%1 %2 %3 %4").arg(text).arg(context_info).arg(msg).arg(current_date);
	QString fileDate = QDateTime::currentDateTime().toString("yyyy_MM_dd");
	QString strFileName = QString("log/%1.txt").arg(fileDate);


	if (!QFile::exists(strFileName))//文件不存在，则创建
	{
		QDir fileDir = QFileInfo(strFileName).absoluteDir();
		QString strFileDir = QFileInfo(strFileName).absolutePath();
		if (!fileDir.exists()) //路径不存在，创建路径
		{
			fileDir.mkpath(strFileDir);
		}
	}
	QFile logFile(strFileName);
	if (!logFile.open(QIODevice::WriteOnly | QIODevice::Append))//未成功打开
	{
		return;
	}
	QTextStream text_stream(&logFile);
	text_stream << message << "\r\n";
	logFile.flush();
	logFile.close();
}

OPObjectManager::OPObjectManager(QObject *parent)
: QObject(parent)
{
	if (gDataBaseOpera == NULL)
		gDataBaseOpera = new IMDataBaseOperaInfo;
	mWindowManager = NULL;
	sysMessage = NULL;
	m_bChecking = false;
	gSettingsManager = new SettingsManager();
	gSettingsManager->ChangeNewPath();	//兼容就版本 迁移exe路径下用户文件
#ifdef Q_OS_WIN
	mHttpServer = NULL;
#endif
}

OPObjectManager::~OPObjectManager()
{
	if (gDataManager)
	{
		delete gDataManager;
		gDataManager = NULL;
	}
	if (gDataBaseOpera)
	{
	//	gDataBaseOpera->DisConnectDB();
		delete gDataBaseOpera;
		gDataBaseOpera = NULL;
	}
	if (gSocketMessage)
	{
		gSocketMessage->deleteLater();
		gSocketMessage = NULL;
	}
	if (gOPDataManager)
	{
		delete gOPDataManager;
		gOPDataManager = NULL;
	}
	if (mWindowManager)
	{
		mWindowManager->deleteLater();
		mWindowManager = NULL;
	}
	if (sysMessage)
	{
		sysMessage->deleteLater();
		sysMessage = NULL;
	}
	if (gSettingsManager)
	{
		delete gSettingsManager;
		gSettingsManager = NULL;
	}
}

//初始化
void OPObjectManager::InitApp()
{
	connect(gSettingsManager, SIGNAL(sigThemeSwitch(QString)), this, SLOT(slotThemeSwitch(QString)));
	
	loadRccResource(); //加载RCC资源文件
    InitLog();  //初始化日志信息
	RegisterMetaType(); //注册信号类型
	gDataManager = new IMDataManagerShareLib;

	if (mWindowManager == NULL)
	{
		mWindowManager = new OPWindowsManagerShareLib;
		connect(mWindowManager, SIGNAL(sigGlobalScreenShot()), this, SLOT(slotGlobalScreenShot()));
		connect(mWindowManager, SIGNAL(sigCheckUpdate()), this, SLOT(slotCheckUpdate()));
	}
	if (sysMessage == NULL)
	{
		sysMessage = new ParseSystemMessage;
	}
	connect(sysMessage, SIGNAL(sigDeviceLogout()), this, SLOT(slotCancel()));
	connect(sysMessage, SIGNAL(sigRefreshDeviceState()), mWindowManager, SIGNAL(sigRefreshDeviceState()));
	connect(sysMessage, SIGNAL(sigUpdateUserInfo(UserInfo)), mWindowManager, SIGNAL(sigUpdateUserInfo(UserInfo)));
	connect(sysMessage, SIGNAL(sigUpdateInfo(int, QVariant)), mWindowManager, SIGNAL(sigUpdateInfo(int, QVariant)));
	connect(sysMessage, SIGNAL(sigUserQuitGroup(QString, QString)), mWindowManager, SIGNAL(sigUserQuitGroup(QString, QString)));
	connect(sysMessage, SIGNAL(sigAddFriendSuccess(BuddyInfo)), mWindowManager, SIGNAL(sigAddFriendSuccess(BuddyInfo)));
	connect(sysMessage, SIGNAL(sigApplyFriend(int, MessageInfo *)), mWindowManager, SIGNAL(sigApplyFriend(int, MessageInfo* )));
	connect(sysMessage, SIGNAL(sigAddSuccessGroup(GroupInfo)), mWindowManager, SIGNAL(sigAddSuccessGroup(GroupInfo)));
	connect(sysMessage, SIGNAL(sigAddSuccessGroupUserInfo(QString, BuddyInfo)), mWindowManager, SIGNAL(sigAddSuccessGroupUserInfo(QString, BuddyInfo)));
	connect(sysMessage, SIGNAL(sigDeleteGroup(QString)), mWindowManager, SIGNAL(sigDeleteGroup(QString)));
	connect(sysMessage, SIGNAL(sigDeleteFriend(int, QVariant)), mWindowManager, SIGNAL(sigDeleteFriend(int, QVariant)));
	connect(sysMessage, SIGNAL(sigGroupNoSpeak(int, int)), mWindowManager, SIGNAL(sigGroupNoSpeak(int, int)));
    connect(this, SIGNAL(sigClickedDock()), mWindowManager, SIGNAL(sigClickedDock()));
    connect(this, SIGNAL(sigGlobalMouseMouse()), mWindowManager, SIGNAL(sigGlobalMouseMouse()));
	connect(mWindowManager, SIGNAL(sigCancel()), this, SLOT(slotCancel()));
	connect(mWindowManager, SIGNAL(sigCancelandDelete(QString)), this, SLOT(slotCancelandDelete(QString)));
#ifdef Q_OS_MAC
    connect(this, SIGNAL(sigSendScreenShotPic()), mWindowManager, SIGNAL(sigSendScreenShotPic()));
#endif
}

int OPObjectManager::InitMain()
{
	QStringList argv = qApp->arguments();
	int argc = argv.count();

	AppConfig appConfig = this->readAppConf(); //读取配置文件
													   
	this->initLanguage();//设置语言。
#ifdef Q_OS_WIN
	shareMem.setKey(appConfig.shareMemory);
	if (shareMem.attach() || argc == 2)
	{
		if (argc == 2)
		{
			if (argv[1] == QString("RestartMainExe") || argv[1] == QString("ignoreUpdate"))
				this->initUpdate(false);
			else
			{
				//通过url protocol进行传参处理。
				QString url(argv[1]);

				UrlProtocol urlProtocol;
				urlProtocol.setURL(url);
			}
			return qApp->exec();
		}
		else
		{
			IMessageBox* pBox = new IMessageBox(NULL);
			pBox->initAsk(tr("Warning"), tr("OpenPlanet is logged in, do you want to restart the program?"));
			connect(pBox, SIGNAL(sigOK()), SLOT(slotRestartMain()));
			connect(pBox, SIGNAL(sigClose()), qApp, SLOT(quit()));
			return qApp->exec();
		}
	}
	else
	{
		this->initUpdate();
		return qApp->exec();
	}
#else
    this->initUpdate();
    return qApp->exec();
#endif
}

//初始化日志信息
void OPObjectManager::InitLog()
{
#ifdef QT_NO_DEBUG 
 //   QDir::setCurrent(QApplication::applicationDirPath());
   // qInstallMessageHandler(LogInfo);  //日志模块
#else 
#endif
}

void OPObjectManager::initLanguage()
{
	//获取用户设置的语言。
	QString proLanguage = gSettingsManager->getLanguage();

	//初次启动，获取到的语言会为空，此时检测系统语言为其设置默认值。
	if (proLanguage.isEmpty())
	{
		QLocale locale;
		switch (locale.language())
		{
			//系统语言为中文。
		case QLocale::Chinese:
			proLanguage = QObject::tr("简体中文");
			break;
		case QLocale::Russian:
			proLanguage = QObject::tr("русский");
			break;
		case QLocale::Armenian:
			proLanguage = QObject::tr("Հայերեն");
			break;
		case QLocale::Spanish:
			proLanguage = QObject::tr("Español");
			break;
		case QLocale::Portuguese:
			proLanguage = QObject::tr("Português");
			break;
		case QLocale::Thai:
			proLanguage = QObject::tr("ไทย");
			break;
			//其他所有情况设置为英文.
		default:
			proLanguage = QObject::tr("English");
			break;
		}

		//写入配置文件。
		gSettingsManager->setLanguage(proLanguage);
	}

	//根据语言设置读取对应的翻译文件。
	//默认为英语不需要设置翻译文件。

	QString prefix;
	QString suffix;
	if (proLanguage == QObject::tr("简体中文"))
	{
		qDebug() << "locale language: chinese";
		QCoreApplication::setApplicationName("星际通讯");
		prefix = ":/Simplified Chinese/Resources/language/Simplified Chinese/";
		suffix = "_zh.qm";

		gI18NLocale = "zh_CN";
	}
	if (proLanguage == QObject::tr("русский"))
	{
		qDebug() << "locale language: Russian";
		prefix = ":/Russian/Resources/language/Russian/";
		suffix = "_ru.qm";

		gI18NLocale = "ru";
	}
	if (proLanguage == QObject::tr("Հայերեն"))
	{
		qDebug() << "locale language: Armenian";
		prefix = ":/Hayeren/Resources/language/Hayeren/";
		suffix = "_hy.qm";

		gI18NLocale = "hy";
	}
	if (proLanguage == QObject::tr("Español"))
	{
		qDebug() << "locale language: Spanish";
		prefix = ":/Spanish/Resources/language/Spanish/";
		suffix = "_es.qm";

		gI18NLocale = "es";
	}
	if (proLanguage == QObject::tr("Português"))
	{
		qDebug() << "locale language: Portuguese";
		prefix = ":/Portuguese/Resources/language/Portuguese/";
		suffix = "_pt.qm";

		gI18NLocale = "pt";
	}
	if (proLanguage == QObject::tr("ไทย"))
	{
		qDebug() << "locale language: Thai";
		prefix = ":/Thai/Resources/language/Thai/";
		suffix = "_th.qm";

		gI18NLocale = "th";
	}

	if (!prefix.isEmpty())
	{
		this->installTranslator(prefix + "baseuisharelib" + suffix);
		this->installTranslator(prefix + "chatwidgetsharelib" + suffix);
		this->installTranslator(prefix + "contactsprofilesharelib" + suffix);
		this->installTranslator(prefix + "contactswidgetsharelib" + suffix);
		this->installTranslator(prefix + "createaddwidgetsharelib" + suffix);
		this->installTranslator(prefix + "imsocketnetworksharelib" + suffix);
		this->installTranslator(prefix + "opmainmanagersharelib" + suffix);
		this->installTranslator(prefix + "opmainwidgetsharelib" + suffix);
		this->installTranslator(prefix + "opobjectmanager" + suffix);
		this->installTranslator(prefix + "oprecoverywalletsharelib" + suffix);
		this->installTranslator(prefix + "oprequestsharelib" + suffix);
		this->installTranslator(prefix + "opwindowsmanagersharelib" + suffix);
		this->installTranslator(prefix + "scanqrloginsharelib" + suffix);
		this->installTranslator(prefix + "updatesharelib" + suffix);
		this->installTranslator(prefix + "ewalletlib" + suffix);
#ifdef Q_OS_WIN
		this->installTranslator(prefix + "newmsgnotifysharelib" + suffix);
#endif // Q_OS_WIN


#ifdef Q_OS_MAC
        this->installTranslator(prefix + "macupdate" + suffix);
        this->installTranslator(prefix + "screencutsharelib" + suffix);
#endif
	}
}

void OPObjectManager::initUpdate(bool doUpdate)
{
#ifdef Q_OS_WIN
    shareMem.create(1);
#endif

	//载入代理设置。
	LoginDatabaseOperaShareLib *database = new LoginDatabaseOperaShareLib;
	QString strUserPath = gSettingsManager->getUserPath();
	database->ConnectLoginDB(strUserPath + "/database/common.db", "Login");
	NetWorkProxyInfo info = database->GetNetWorkProxyInfoDB();
	if (info.proxyModel != 0)
	{
		if (!info.strAddress.isEmpty() && info.strPort != "0")
		{
			QNetworkProxy proxy;
			proxy.setType(QNetworkProxy::HttpProxy);
			proxy.setHostName(info.strAddress);
			proxy.setPort(info.strPort.toInt());
			proxy.setUser(info.strUserName);
			proxy.setPassword(info.strUserPwd);
			QNetworkProxy::setApplicationProxy(proxy);
		}
	}
	delete database;

	if (doUpdate)
		this->update();     //判断升级
	else
		this->StartLoginWidget();
}

//注册信号类型
void OPObjectManager::RegisterMetaType()
{
	qRegisterMetaType<UserInfo>("UserInfo");
	qRegisterMetaType<BuddyInfo>("BuddyInfo");
}

void OPObjectManager::installTranslator(QString qmPath)
{
	QTranslator *lib = new QTranslator;
	lib->load(qmPath);
	qApp->installTranslator(lib);
}

//读取配置文件
AppConfig OPObjectManager::readAppConf()
{
	ReadAppConfig conf;
	AppConfig appConf = conf.ReadConfig("config/config.xml");
	if (gDataManager)
	{
		gDataManager->setAppConfigInfo(appConf);
	}
	return appConf;
}

//启动登陆窗口
void OPObjectManager::StartLoginWidget()
{
	connect(mWindowManager, SIGNAL(sigLoginSuccess(QVariantMap)), this, SLOT(slotLoginSuccess(QVariantMap)));
	connect(mWindowManager, SIGNAL(sigExit()), this, SLOT(slotExit()));
	connect(mWindowManager, SIGNAL(sigInitBuddyGroup()), this, SLOT(slotInitBuddyGroup()));
	connect(this, SIGNAL(sigReConnectServerSuccess(QString)), mWindowManager, SIGNAL(sigSetMainWidgetStatusLabel(QString)));

	if (gDataManager)
	{
		if (mWindowManager->JudgeMainWindow())
		{
			IMessageBox::tip(NULL, tr("Notice"), tr("Already the latest version"));
			m_bChecking = false;
			return;
		}
		AppConfig conf = gDataManager->getAppConfigInfo();
		mWindowManager->StartLoginWidget(conf.strYWYHRequestURL, conf.appVersion.appID);
	}
}

//登陆成功
void OPObjectManager::slotLoginSuccess(QVariantMap varMap)
{
	UserInfo userInfo = ParseUserInfoFromServer(varMap);

	if (gDataManager)
	{
		gDataManager->setUserInfo(userInfo);
	};

	slotParsePersonInfo(true, userInfo);

#ifdef Q_OS_WIN
	//登录成功后再启动
	if (NULL == mHttpServer)
	{
		mHttpServer = new QHttpServer(12306);
		connect(mHttpServer, SIGNAL(sigApplyGroup(QString)), this, SLOT(slotGetGroupByKey(QString)));
		mHttpServer->slotThreadListen();
	}
#endif
	connect(this, SIGNAL(sigOpenChat(int, QVariant)), mWindowManager, SIGNAL(sigOpenChat(int, QVariant)));
}

UserInfo OPObjectManager::ParseUserInfoFromServer(QVariantMap varMap)
{
	UserInfo userInfo;
	QVariantMap mapVar = varMap["userInfo"].toMap();
	userInfo.strUserPWD = mapVar["imPassword"].toString();
	userInfo.nUserID = mapVar["imUserId"].toInt();
	userInfo.strUserNickName = mapVar["nickName"].toString();
	userInfo.strLoginPWD = mapVar["password"].toString();
	userInfo.strSex = mapVar["sex"].toString();
	userInfo.strAccountName = mapVar["userId"].toString();
	userInfo.strUserName = mapVar["userName"].toString();
	userInfo.strUserDefaultAvatar = ":/PerChat/Resources/person/temp.png";
	QString path;
	QDir dir;
#ifdef Q_OS_WIN
	userInfo.strUserAvatarLocal = gSettingsManager->getUserPath() + "/resource/header/" + mapVar["imUserId"].toString() + ".jpg";
#else
    userInfo.strUserAvatarLocal = gSettingsManager->getUserPath() + "/resource/header/" + mapVar["imUserId"].toString() + ".jpg";
#endif
	userInfo.strUserAvatarHttp = mapVar["avatar"].toString();

	userInfo.disableStrangers = mapVar["disableStrangers"].toInt();

	/*解析钱包信息*/
	if (gOPDataManager == NULL)
	{
		gOPDataManager = new OPDataManager;
	}

	gOPDataManager->setAccessToken(varMap["access_token"].toString());
	gOPDataManager->setRefreshToken(varMap["refresh_token"].toString());
	WalletInfo wallet;
	wallet.address = mapVar["ethAddress"].toString();
	wallet.pubkey = mapVar["ethPublicKey"].toString();
	wallet.fromInviteCode = mapVar["fromInviteCode"].toString();
	wallet.inviteCode = mapVar["inviteCode"].toString();
	wallet.inviteCodeUseCount = mapVar["inviteCodeUseCount"].toString();
	wallet.userIdMd5 = mapVar["userIdMd5"].toString();
	wallet.planet = mapVar["planet"].toString();
	gOPDataManager->setWalletInfo(wallet);
	return userInfo;
}

void OPObjectManager::requestOffline()
{
	//发送网络请求通知服务器下线。
	UserInfo user = gDataManager->getUserInfo();
	if (user.nUserID != -1)
	{
		HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
		QString strResult = gDataManager->getAppConfigInfo().MessageServerAddress +
			QString("/IMServer/user/logout?userId=%1&passWord=%2&deviceId=%3")
			.arg(user.nUserID).arg(user.strUserPWD).arg(GetDeviceInfo().GetDeviceID());
		QEventLoop loop;
		connect(http, SIGNAL(sigReplyFinished(bool, QString)), &loop, SLOT(quit()));
		http->getHttpRequest(strResult,5000);
		//开启子事件循环
		loop.exec();
		http->deleteLater();
	}
}

//请求个人信息
void OPObjectManager::RequestPersonInfo()
{
	IMRequestBuddyInfo *requestPerson = new IMRequestBuddyInfo;
	connect(requestPerson, SIGNAL(sigRequestPersonInfoSuccess(bool, UserInfo)), this, SLOT(slotParsePersonInfo(bool, UserInfo)));
	if (gDataManager)
	{
		UserInfo userInfo = gDataManager->getUserInfo();
		AppConfig appConf = gDataManager->getAppConfigInfo();
		requestPerson->RequestPersonInfo(appConf.MessageServerAddress, QString::number(userInfo.nUserID));
	}
}

//请求个人信息结束
void OPObjectManager::slotParsePersonInfo(bool bResult, UserInfo userInfo)
{
	if (bResult)
	{
		if (gDataManager)
		{
			gDataManager->updateUserInfo(userInfo);
		}
		/*请求socket服务*/
		ConnectSocketServer();
	}
	else
	{
		if (mWindowManager)
		{
			mWindowManager->ChangeLoginWidget(tr("Requesting personal information failed!"));
		}
	}
}

//请求好友信息
void OPObjectManager::RequestBuddyInfo()
{
	IMRequestBuddyInfo *requestBuddy = new IMRequestBuddyInfo;
	connect(requestBuddy, SIGNAL(sigParseBuddyInfo(BuddyInfo)), mWindowManager, SLOT(slotInsertBuddyInfo(BuddyInfo)));
	if (gDataManager)
	{
		UserInfo userInfo = gDataManager->getUserInfo();
		AppConfig appConf = gDataManager->getAppConfigInfo();
		requestBuddy->RequestBuddyInfo(appConf.MessageServerAddress, QString::number(userInfo.nUserID), userInfo.strUserPWD, appConf.appVersion.appID);
	}
}

//请求部落信息
void OPObjectManager::RequestGroupInfo()
{
	IMRequestBuddyInfo *requestBuddy = new IMRequestBuddyInfo;
	connect(requestBuddy, SIGNAL(sigParseGroupInfo(GroupInfo)), mWindowManager, SLOT(slotInsertGroupInfo(GroupInfo)));
	if (gDataManager)
	{
		UserInfo userInfo = gDataManager->getUserInfo();
		AppConfig appConf = gDataManager->getAppConfigInfo();
		requestBuddy->RequestGroupInfo(appConf.MessageServerAddress, QString::number(userInfo.nUserID), userInfo.strUserPWD, appConf.appVersion.appID);
	}
}

//连接socket服务
void OPObjectManager::ConnectSocketServer()
{
	if (gSocketMessage == NULL)
	{
		gSocketMessage = new IMSocketMessageInfo(this);
		gSocketMessage->setUserPath(gSettingsManager->getUserPath());
		connect(gSocketMessage, SIGNAL(sigRevSystemMessage(MessageInfo)), this, SLOT(slotRevSystemMessage(MessageInfo)));  //接收到系统消息
		connect(gSocketMessage, SIGNAL(sigRevOtherDeviceMsg(MessageInfo)), mWindowManager, SIGNAL(sigRevOtherDeviceMsg(MessageInfo)));  //收到通过其他设备发送的消息
		connect(gSocketMessage, SIGNAL(sigRevMsgInfo(MessageInfo, int)), this, SLOT(slotRevMsgInfo(MessageInfo, int)));  //接收到消息
		connect(gSocketMessage, SIGNAL(sigRevUpdateMessageState(MessageInfo, int)), mWindowManager, SIGNAL(sigUpdateMessageState(MessageInfo, int)));  //接收到消息
		connect(gSocketMessage, SIGNAL(sigDisConnectServerServer(QString)), this, SLOT(slotDisConnectServerServer(QString)));  //与服务器断开连接信号
		connect(gSocketMessage, SIGNAL(sigGetSocketAddressErro(QString)), this, SLOT(slotGetSocketAddressErro(QString)));  //获取socket信息失败
		connect(gSocketMessage, SIGNAL(sigConnectSocketSuccess()), this, SLOT(slotConnectSocketSuccess()));  //连接socket成功
		connect(gSocketMessage, SIGNAL(sigReConnectSuccess()), this, SLOT(slotReConnectSuccess()));  //重连socket成功
		connect(gSocketMessage, SIGNAL(sigInputting(MessageInfo)), this, SLOT(slotInputting(MessageInfo)));  //正在输入
		connect(gSocketMessage, SIGNAL(sigInsertLocalFileTable(QByteArray, QString, QString)), this, SLOT(slotInsertLocalFileTable(QByteArray, QString, QString)));  //正在输入
		//connect(gSocketMessage, SIGNAL(sigDisConnectServerServer(QString)), IMMainWidget::self(), SLOT(slotHideWarning()));
		//connect(gSocketMessage, SIGNAL(sigReConnectSuccess()), IMMainWidget::self(), SLOT(slotShowWarning()));
	}
	if (gDataManager)
	{
		AppConfig conf = gDataManager->getAppConfigInfo();
		UserInfo userInfo = gDataManager->getUserInfo();
		gSocketMessage->RequestSocketUrl(conf.MessageServerAddress, QString::number(userInfo.nUserID), userInfo.strUserPWD);
	}
}

//Qualifier: 接收到系统消息
void OPObjectManager::slotRevSystemMessage(MessageInfo msgInfo)
{
	sysMessage->ParseSysMessage(msgInfo);
}

//Qualifier: 接收到消息
void OPObjectManager::slotRevMsgInfo(MessageInfo msgInfo, int)
{
	if (mWindowManager)
	{
		mWindowManager->RevServerMessageInfo(msgInfo);
	}
}

//Qualifier: 获取socket信息失败
void OPObjectManager::slotGetSocketAddressErro(QString msg)
{
	if (mWindowManager)
	{
		mWindowManager->ChangeLoginWidget(msg);
	}
}

//Qualifier: 连接socket成功
void OPObjectManager::slotConnectSocketSuccess()
{
	//连接数据库
	if (gDataBaseOpera == NULL)
		gDataBaseOpera = new IMDataBaseOperaInfo;
	if (gDataManager)
	{
		UserInfo userInfo = gDataManager->getUserInfo();
#ifdef Q_OS_WIN
		QString strDBPath = gSettingsManager->getUserPath()+"/database/" + QString::number(userInfo.nUserID) + "//database.db";
#else
        QString strDBPath = gSettingsManager->getUserPath() + "/database/" + QString::number(userInfo.nUserID) + "//database.db";
#endif
		gDataBaseOpera->ConnectDB(strDBPath, "IMChatClient");
		gSocketMessage->SetIMUserID(userInfo.nUserID);
		if (mWindowManager)
		{
			/*
#ifdef Q_OS_WIN
			//初始化cef。
			OnInitCef();
#endif
			*/
			//如果数据库中有基地地址，进入主界面，否则要求用户恢复钱包。
			mWindowManager->ReadWalletAddress();
			//销毁登录窗口。
			mWindowManager->DestroyLoginWidget();
		}
	}
}

void OPObjectManager::slotDisConnectServerServer(QString strValue)
{
	emit mWindowManager->sigSetMainWidgetStatusLabel(strValue);
	emit mWindowManager->sigNetWarning(true);
}

//Qualifier:重连socket成功
void OPObjectManager::slotReConnectSuccess()
{
	//获取版本号
	if (gDataManager)
	{
		AppConfig conf = gDataManager->getAppConfigInfo();
		QString msg = tr("OpenPlanet  ") + conf.appVersion.versionID;
		emit sigReConnectServerSuccess(msg);
		emit mWindowManager->sigNetWarning(false);
	}
}

//判断是否请求好友信息
void OPObjectManager::InitBuddyInfo()
{
	if (gDataBaseOpera)
	{
		if (gDataBaseOpera->DBJudgeBuddyIsEmpty())
		{
			RequestBuddyInfo();/*请求好友信息*/
		}
		else      //加载数据库信息
		{
			LoadBuddyInfo();
		}
	}
}

//加载好友信息
void OPObjectManager::LoadBuddyInfo()
{
	if (mWindowManager)
	{
		QList<BuddyInfo> listBuddyInfo = gDataBaseOpera->DBGetBuddyInfo();
		IMRequestBuddyInfo *requestBuddy = new IMRequestBuddyInfo;
		connect(requestBuddy, SIGNAL(sigParseBuddyInfo(BuddyInfo)), mWindowManager, SLOT(slotInsertBuddyInfo(BuddyInfo)));
		requestBuddy->RequestBuddyInfo(listBuddyInfo);
	}
}

//判断是否请求部落信息
void OPObjectManager::InitGroupInfo()
{
	if (gDataBaseOpera)
	{
		if (gDataBaseOpera->DBJudgetGroupIsEmpty())
		{
			RequestGroupInfo();/*请求部落信息*/
		}
		else      //加载数据库信息
		{
			LoadGroupInfo();
		}
	}

}

void OPObjectManager::LoadGroupInfo()
{
	if (mWindowManager)
	{
		QList<GroupInfo> listGroupInfo = gDataBaseOpera->DBGetAllGroupInfo();
		IMRequestBuddyInfo *requestGroup = new IMRequestBuddyInfo;
		connect(requestGroup, SIGNAL(sigParseGroupInfo(GroupInfo)), mWindowManager, SLOT(slotInsertGroupInfo(GroupInfo)));
		requestGroup->RequestGroupInfo(listGroupInfo);
	}
}

void OPObjectManager::slotExit()
{
//#ifdef Q_OS_WIN
//	if (gbIsInitCef)
//	{
//		CefShutdown();
//	}
//#endif

	//程序关闭时清除截屏和剪贴板
	gSettingsManager->DelTempUserFile();

	requestOffline();

	if (mWindowManager)
	{
		mWindowManager->deleteLater();
		mWindowManager = NULL;
	}

	if (gDataBaseOpera)
	{
	//	gDataBaseOpera->DisConnectDB();
		delete gDataBaseOpera;
		gDataBaseOpera = NULL;
	}

	if (gDataManager)
	{
		delete gDataManager;
		gDataManager = NULL;
	}

	if (gSocketMessage)
	{
		gSocketMessage->deleteLater();
		gSocketMessage = NULL;
	}

#ifdef Q_OS_WIN
	::TerminateProcess(::GetCurrentProcess(), 0);
#else
	exit(0);
#endif
	
}

void OPObjectManager::update()
{
	UpdateShareLib *updateShareLib = new UpdateShareLib;
	connect(updateShareLib, SIGNAL(sigAlreadyLatest()), this, SLOT(slotStart()));
	updateShareLib->onUpdate();
}

void OPObjectManager::slotRestartMain()
{
#ifdef Q_OS_WIN
	QString exeName = "OpenPlanet.exe";

	HANDLE proSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);  //TH32CS_SNAPPROCESS表示在快照中包含系统中所有的进程。
	PROCESSENTRY32 proEntry;
	proEntry.dwSize = sizeof(PROCESSENTRY32);    //初始化结构体大小。没有这一句Process32First()会失败。
	bool exist = Process32First(proSnap, &proEntry);    //获得HANDLE中的第一个进程信息，如果成功，返回true，否则false。
	while (exist)    //读取进程信息。
	{
		QString proName = QString::fromWCharArray(proEntry.szExeFile);
		if (exeName == proName)
		{
			if (GetCurrentProcessId() != proEntry.th32ProcessID)
			{
				QProcess process;
				process.execute(QString("TASKKILL /PID %1 /T /F").arg(proEntry.th32ProcessID));
				process.close();
			}
		}
		exist = Process32Next(proSnap, &proEntry);    //获取下一个进程信息，成功返回true，否则返回false。
	}
	CloseHandle(proSnap);    //关闭HANDLE。
	this->initUpdate();
#endif
}

void OPObjectManager::slotStart()
{
	m_bChecking = false;
	StartLoginWidget();
}

void OPObjectManager::slotInitBuddyGroup()
{
	InitBuddyInfo();  //判断是否加载好友信息
	InitGroupInfo();/*请求部落信息*/
}

void OPObjectManager::slotGetGroupByKey(QString groupKey)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;

	QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/getGroupByGroupIdOrKey?groupKey="
		+ groupKey;
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotApplyGroup(bool, QString)));
	http->getHttpRequest(url);
}

void OPObjectManager::slotApplyGroup(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();

		if (map.value("result").toString() == "success")
		{
			QVariantMap group = map.value("group").toMap();
			int id = group.value("groupId").toInt();

			QString groupID = QString::number(id);
			GroupInfo groupInfo = gDataBaseOpera->DBGetGroupFromID(groupID);
			if (groupInfo.groupName.isEmpty())
			{
				UserInfo userInfo = gDataManager->getUserInfo();

				HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
				QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/applyAddGroup"
					+ QString("?userId=%1&passWord=%2&groupId=%3").arg(userInfo.nUserID).arg(userInfo.strUserPWD).arg(groupID);

				http->getHttpRequest(url);

				IMessageBox::tip(NULL, tr("Notice"), tr("Request to add group success!"));

				emit sigOpenChat(OpenGroup, QVariant());
			}
			else
			{
				emit sigOpenChat(OpenGroup, QVariant(groupID));
			}

			return;
		}
	}

	IMessageBox::tip(NULL, tr("Warning"), tr("Failed to get group information, please try again~"));
}

void OPObjectManager::slotCancel()
{
	requestOffline();

	if (mWindowManager)
	{
		mWindowManager->deleteLater();
		mWindowManager = NULL;
	}
	if (sysMessage)
	{
		delete sysMessage;
		sysMessage = NULL;
	}
	if (gSocketMessage)
	{
		delete gSocketMessage;
		gSocketMessage = NULL;
	}
	if (gDataManager)
	{
		delete gDataManager;
		gDataManager = NULL;
	}
	if (gDataBaseOpera)
	{
		delete gDataBaseOpera;
		gDataBaseOpera = NULL;
	}
	InitApp();
	readAppConf();
	StartLoginWidget();
}

void OPObjectManager::slotCancelandDelete(QString strValue)
{
	gSettingsManager->copyUserDir(gSettingsManager->getUserPath(), strValue, true);
	QString strOldPath = gSettingsManager->getUserPath();
	gSettingsManager->DelDir(strOldPath);
	gSettingsManager->setUserPath(strValue);

	if (mWindowManager)
	{
		delete mWindowManager;
		mWindowManager = NULL;
	}
	if (sysMessage)
	{
		delete sysMessage;
		sysMessage = NULL;
	}
	if (gSocketMessage)
	{
		delete gSocketMessage;
		gSocketMessage = NULL;
	}
	if (gDataManager)
	{
		delete gDataManager;
		gDataManager = NULL;
	}
	if (gDataBaseOpera)
	{
		gDataBaseOpera->DisConnectDB();
		delete gDataBaseOpera;
		gDataBaseOpera = NULL;
	}

	InitApp();
	readAppConf();
	StartLoginWidget();
}

void OPObjectManager::slotGlobalScreenShot()
{
#ifdef Q_OS_WIN
	TCHAR szAppPath[MAX_PATH] = { 0 };
	_stprintf(szAppPath, L"0x%x,%d %s", IMMainWidget::self()->winId(), gDataManager->getCurrentChatWidget(), gSettingsManager->getLanguage().toStdWString().c_str());
	ShellExecute(NULL, L"open", L"ScreenShotTool.exe", szAppPath, NULL, SW_SHOW);
#else
	OEScreenshot *screenCut = OEScreenshot::Instance();
    connect(screenCut, SIGNAL(sigScreenComplete()), this, SLOT(slotSendScreenShotPic()));
    connect(screenCut, SIGNAL(sigScreenCanclePixMap()), this, SLOT(slotScreenCanclePixMap()));
#endif
}

#ifndef Q_OS_WIN
void OPObjectManager::slotScreenCanclePixMap()
{
    OEScreenshot *act=qobject_cast<OEScreenshot*>(sender());
    if(act)
    {
        act->showNormal();
        act->close();
    }
}

void OPObjectManager::slotSendScreenShotPic()
{
    OEScreenshot *act=qobject_cast<OEScreenshot*>(sender());
    if(act)
    {
        act->showNormal();
        act->close();
    }
    emit sigSendScreenShotPic();
}
#endif

void OPObjectManager::slotCheckUpdate()
{
	if (m_bChecking == false)
	{
		m_bChecking = true;
		readAppConf();
		update();
		
	}
}

//对方正在输入
void OPObjectManager::slotInputting(MessageInfo megInfo)
{
	if (mWindowManager)
	{
		mWindowManager->RevInputting(megInfo);
	}
}
//插入文件信息
void OPObjectManager::slotInsertLocalFileTable(QByteArray msgID, QString strPath, QString strMsg)
{
	gDataBaseOpera->DBOnInsertFileInfo(QString(msgID), strPath, strMsg);
}

void OPObjectManager::slotThemeSwitch(QString currTheme)
{
	QSettings settings("InterPlanet", "Qt");
	QString strTheme = settings.value("Theme").toString();
	settings.setValue("Theme", currTheme);
	gThemeStyle = currTheme;

	connect(this, SIGNAL(sigRestart()), gSettingsManager, SIGNAL(sigRestart()));

	if (strTheme == currTheme)
		return;
	else
	{
		emit sigRestart();
		//QString qssDir = "../../OpenPlanet/Resources/QSS";
		//QString lastDir = "../../OpenPlanet/Resources/QSS_" + strTheme;
		//QString currDir = "../../OpenPlanet/Resources/QSS_" + currTheme;
		//if (!QFile::rename(qssDir, lastDir))
		//	qDebug() << "文件重命名失败...";
		//if (!QFile::rename(currDir, qssDir))
		//	qDebug() << "文件重命名失败...";	

		/*************************** 无法反注册处于占用状态的资源文件RCC，需要重启 ********************************/
		//QString resourceUnreg = "resources_" + strTheme + ".rcc";
		//qDebug() << "resourceUnreg :" << resourceUnreg;
		//if (!QResource::unregisterResource(resourceUnreg))
		//	qDebug() << "unregisterResource failed!!!";


		//QString resourceReg = "resources_" + currTheme + ".rcc";
		//qDebug() << "resourceReg :" << resourceReg;
		//if(!QResource::registerResource(resourceReg))
		//	qDebug() << "registerResource failed!!!";
	}
}

void OPObjectManager::loadRccResource()
{
	QSettings settings("InterPlanet", "Qt");
	QString currTheme = settings.value("Theme").toString();

	if (currTheme == "Blue")
	{
		QResource::registerResource("resources_Blue.rcc");
		gThemeStyle = "Blue";
	}
	else if (currTheme == "White")
	{
		QResource::registerResource("resources_White.rcc");
		gThemeStyle = "White";
	}
	else
	{
		//设为默认主题 浅色
		QResource::registerResource("resources_White.rcc");
		gThemeStyle = "White";
	}
}