﻿#include "profilemanager.h"
#include "immainwidget.h"
#include "perprofilewidget.h"
#include "groupprofilewidget.h"
#include "opdatebasesharelib.h"
#include "oprequestsharelib.h"
#include "define.h"
#include "QStringLiteralBak.h"
#include "globalmanager.h"
//#include "stdafx.h"

extern OPDatebaseShareLib *gOPDataBaseOpera;

profilemanager::profilemanager(QObject *parent)
	: QObject(parent)
{
	connect(this, SIGNAL(sigCreatePerFile(QString)), this,SLOT(slotCreatePerFile(QString)));
	connect(this, SIGNAL(sigCreateGrpFile(QString)), this, SLOT(slotCreateGrpFile(QString)));
	connect(this, SIGNAL(sigCreatePerFileByInfo(BuddyInfo)), this, SLOT(slotCreatePerFileByInfo(BuddyInfo)));
	connect(this, SIGNAL(sigCreateGrpFileByInfo(GroupInfo)), this, SLOT(slotCreateGrpFileByInfo(GroupInfo)));
}

profilemanager::profilemanager(const profilemanager&) {

}


profilemanager& profilemanager::operator=(const profilemanager&) {
	return *instance;
}

profilemanager* profilemanager::instance = NULL;// = new profilemanager(NULL);
profilemanager* profilemanager::getInstance() {
	//return instance;
	if (!instance)
	{
		instance = new profilemanager(NULL);
	}

	return instance;
}


profilemanager::~profilemanager()
{
}

void profilemanager::slotCreatePerFileByInfo(BuddyInfo buddyInfo)
{
	perProfileWidget* pPer = NULL;
	if (mapPerFile.contains(QString::number(buddyInfo.nUserId)))
	{
		pPer = mapPerFile[QString::number(buddyInfo.nUserId)];
		if (pPer == NULL)
		{
			return;
		}
		AddressInfo wallet = gOPDataBaseOpera->DBGetAddressInfo(QString::number(buddyInfo.nUserId));

		connect(pPer, SIGNAL(sigCloseProfile()), this, SLOT(slotClosePerFile()));
		connect(pPer, SIGNAL(sigShareID(int, QString)), this, SIGNAL(sigShareID(int, QString)));
		connect(pPer, SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget*)), this, SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget*)));//图片浏览
		connect(pPer, SIGNAL(sigPerChat(int, QVariant)), this, SIGNAL(sigOpenChat(int, QVariant)));
		pPer->setBuddy(buddyInfo, wallet);

		if (wallet.comPublicKey.isEmpty())
		{
			OPRequestShareLib *request = new OPRequestShareLib;
			request->setProperty("UserId", buddyInfo.nUserId);
			connect(request, SIGNAL(sigBuddyAddressInfo(AddressInfo)), this, SLOT(slotAddressInfo(AddressInfo)));
			connect(request, SIGNAL(sigBuddyAddressInfo(AddressInfo)), request, SLOT(deleteLater()));
			request->getBuddyAddressInfo(QString::number(buddyInfo.nUserId));
		}
		pPer->setProperty("IsLoading", false);
		pPer->show();
		pPer->activateWindow();
	}
}
void profilemanager::slotCreatePerFile(QString buddyId)
{
	perProfileWidget* pPer = NULL;
	if (mapPerFile.contains(buddyId) && mapPerFile[buddyId])
	{
		pPer = mapPerFile[buddyId];
		if (pPer == NULL)
		{
			return;
		}
		bool bLoading = pPer->property("IsLoading").toBool();
		if (bLoading)
		{
			return;
		}
		pPer->setProperty("IsLoading", false);
		pPer->show();
		pPer->activateWindow();
	}
	else
	{
		pPer = new perProfileWidget();
		if (pPer == NULL)
		{
			return;
		}
		pPer->setProperty("IsLoading", true);
		mapPerFile.insert(buddyId, pPer);
		BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(buddyId);
		if (buddy.nUserId == -1)  //不存在
		{
			buddy = gDataBaseOpera->DBGetGroupUserFromID(buddyId);
		}
		if (buddy.nUserId < 0)
		{
			HttpNetWork::HttpNetWorkShareLib *PersonInfo = new HttpNetWork::HttpNetWorkShareLib();
			PersonInfo->setProperty("BuddyId", buddyId);
			connect(PersonInfo, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotShareBuddyInfoResult(bool, QString)));
			AppConfig configInfo = gDataManager->getAppConfigInfo();
			QString strRequest = configInfo.MessageServerAddress + HTTP_GETBUDDYINFOBYID + buddyId;
			PersonInfo->getHttpRequest(strRequest);
			return;
		}
		AddressInfo wallet = gOPDataBaseOpera->DBGetAddressInfo(buddyId);
		pPer->setBuddy(buddy, wallet);

		connect(pPer, SIGNAL(sigCloseProfile()), this, SLOT(slotClosePerFile()));
		connect(pPer, SIGNAL(sigShareID(int, QString)), this, SIGNAL(sigShareID(int, QString)));
		connect(pPer, SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget*)), this, SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget*)));//图片浏览
		connect(pPer, SIGNAL(sigPerChat(int, QVariant)), this, SIGNAL(sigOpenChat(int, QVariant)));
		if (wallet.comPublicKey.isEmpty())
		{
			OPRequestShareLib *request = new OPRequestShareLib;
			request->setProperty("UserId", buddy.nUserId);
			connect(request, SIGNAL(sigBuddyAddressInfo(AddressInfo)), this, SLOT(slotAddressInfo(AddressInfo)));
			connect(request, SIGNAL(sigBuddyAddressInfo(AddressInfo)), request, SLOT(deleteLater()));
			request->getBuddyAddressInfo(buddyId);
		}
		pPer->setProperty("IsLoading", false);
		pPer->show();
		pPer->activateWindow();
	}

}

void profilemanager::slotCreateGrpFileByInfo(GroupInfo groupInfo)
{
	GroupProfileWidget* pGrp = NULL;
	if (mapGrpFile.contains(groupInfo.groupId))
	{
		pGrp = mapGrpFile[groupInfo.groupId];
		if (pGrp == NULL)
		{
			return;
		}
		pGrp->setGroup(groupInfo);
		connect(pGrp, SIGNAL(sigCloseProfile()), this, SLOT(slotCloseGrpFile()));
		connect(pGrp, SIGNAL(sigShareID(int, QString)), this, SIGNAL(sigShareID(int, QString)));
		connect(pGrp, SIGNAL(sigGroupChat(int, QVariant)), this, SIGNAL(sigOpenChat(int, QVariant)));
		pGrp->setProperty("IsLoading", false);
		pGrp->show();
		pGrp->activateWindow();
	}
}

void profilemanager::slotCreateGrpFile(QString groupId)
{
	GroupProfileWidget* pGrp = NULL;
	if (mapGrpFile.contains(groupId))
	{
		pGrp = mapGrpFile[groupId];
		if (pGrp == NULL)
		{
			return;
		}
		bool bLoading = pGrp->property("IsLoading").toBool();
		if (bLoading)
		{
			return;
		}
	}
	else
	{
		pGrp = new GroupProfileWidget();
		if (pGrp == NULL)
		{
			return;
		}
		pGrp->setProperty("IsLoading",true);
 		mapGrpFile.insert(groupId, pGrp);
		GroupInfo group = gDataBaseOpera->DBGetGroupFromID(groupId);
		if (group.groupId.isEmpty())
		{
			UserInfo user = gDataManager->getUserInfo();
			HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
			http->setProperty("GroupId", groupId);
			connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotShareGroupInfoResult(bool, QString)));
			AppConfig configInfo = gDataManager->getAppConfigInfo();
			QString strResult = configInfo.MessageServerAddress + HTTP_GETGROUPINFOBYGROUPID + QString("?userId=") + QString::number(user.nUserID) + "&passWord=" + user.strUserPWD + "&groupId=" + groupId;
			http->getHttpRequest(strResult);
			return;
		}
		pGrp->setGroup(groupId);
		connect(pGrp, SIGNAL(sigCloseProfile()), this, SLOT(slotCloseGrpFile()));
		connect(pGrp, SIGNAL(sigShareID(int, QString)), this, SIGNAL(sigShareID(int, QString)));
		connect(pGrp, SIGNAL(sigGroupChat(int, QVariant)), this, SIGNAL(sigOpenChat(int, QVariant)));
	}
	pGrp->setProperty("IsLoading", false);
	pGrp->show();
	pGrp->activateWindow();
}

void profilemanager::slotClosePerFile()
{
	perProfileWidget* pPer = (perProfileWidget*)(sender());
	mapPerFile.remove(pPer->getBuddyId());
	if(pPer)
		delete pPer;
}

void profilemanager::slotCloseGrpFile()
{
	GroupProfileWidget* pGrp = (GroupProfileWidget*)(sender());
	mapGrpFile.remove(pGrp->getGroupID());
	if (pGrp)
		pGrp->deleteLater();
}


void profilemanager::slotShareBuddyInfoResult(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		if (map.value("result").toString() == "success")
		{
			QVariantMap user = map.value("user").toMap();

			BuddyInfo buddy;
			buddy.BuddyType = 0;
			buddy.nUserType = user.value("userType").toInt();
			buddy.disableStrangers = user.value("disableStrangers").toInt();
			buddy.nUserId = user.value("userId").toInt();
			buddy.strEmail = user.value("email").toString();
			buddy.strHttpAvatar = user.value("avatar").toString();
			if (buddy.strHttpAvatar.isEmpty())
				buddy.strHttpAvatar = "https://tc.ipcom.io/panserver/files/c64b5061-77dc-45af-b91b-e5e06d401347/download";
			
			buddy.strLocalAvatar = gSettingsManager->getUserPath() + "/resource/header/tmp/"+ QString::number(gDataManager->getUserInfo().nUserID) + "/" +QString::number(buddy.nUserId) + ".png";
			//buddy.strLocalAvatar = QDir::tempPath() + "/" + QString::number(buddy.nUserId) + ".png";
			buddy.strMobilePhone = user.value("phone").toString();
			buddy.strNickName = user.value("nickName").toString();
			buddy.strPhone = user.value("phone").toString();
			buddy.strSex = user.value("sex").toString();
			buddy.strSign = user.value("sign").toString();
			buddy.strUserName = user.value("userName").toString();

			HttpNetWork::HttpDownLoadFile *down = new HttpNetWork::HttpDownLoadFile;
			down->setData(QVariant::fromValue(buddy));
			connect(down, SIGNAL(sigDownFinished(bool)), this, SLOT(slotShareBuddyHeader(bool)));
			down->StartDownLoadFile(buddy.strHttpAvatar, buddy.strLocalAvatar);
		}
	}
	else
	{
		HttpNetWork::HttpNetWorkShareLib *down = qobject_cast<HttpNetWork::HttpNetWorkShareLib *>(sender());
		QVariant vurl = down->property("BuddyId");
		QString strBuddyId = vurl.value<QString>();
		delete mapPerFile[strBuddyId];
		mapPerFile.remove(strBuddyId);
		IMessageBox::tip(NULL, tr("Warning"), tr("Failed to get business card information!"));
	}
}

void profilemanager::slotShareBuddyHeader(bool success)
{
 	HttpNetWork::HttpDownLoadFile *down = qobject_cast<HttpNetWork::HttpDownLoadFile *>(sender());
 	BuddyInfo buddy = down->getData().value<BuddyInfo>();
	emit sigCreatePerFileByInfo(buddy);
}


void profilemanager::slotShareGroupInfoResult(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		if (map.value("result").toString() == "success")
		{
			QVariantMap groupMap = map.value("group").toMap();

			GroupInfo group;
			group.createTime = groupMap.value("createTime").toString();
			group.createUserId = QString::number(groupMap.value("createUserId").toInt());
			group.groupId = QString::number(groupMap.value("groupId").toInt());
			group.groupKey = groupMap.value("groupKey").toString();
			group.groupHttpHeadImage = groupMap.value("avatar").toString();
			if (group.groupHttpHeadImage.isEmpty())
				group.groupHttpHeadImage = "https://tc.ipcom.io/panserver/files/0bb98df6-cddd-444e-a36c-c50beac4dcd1/download";
			group.groupLoacalHeadImage = QDir::tempPath() + "/" + group.groupId + ".png";
			group.groupName = groupMap.value("groupName").toString();
			group.groupType = groupMap.value("groupType").toInt();
			group.noSpeak = groupMap.value("noSpeak").toInt();
			group.groupDesc = groupMap.value("groupDesc").toString();

			HttpNetWork::HttpDownLoadFile *down = new HttpNetWork::HttpDownLoadFile;
			down->setData(QVariant::fromValue(group));
			connect(down, SIGNAL(sigDownFinished(bool)), this, SLOT(slotShareGroupHeader(bool)));
			down->StartDownLoadFile(group.groupHttpHeadImage, group.groupLoacalHeadImage);
		}
		else
		{
			HttpNetWork::HttpNetWorkShareLib *down = qobject_cast<HttpNetWork::HttpNetWorkShareLib *>(sender());
			QVariant vurl = down->property("GroupId");
			QString strGroupId = vurl.value<QString>();
			delete mapGrpFile[strGroupId];
			mapPerFile.remove(strGroupId);
			IMessageBox::tip(NULL, tr("Warning"), tr("Failed to get business card information!"));
		}
	}
	else
	{
		HttpNetWork::HttpNetWorkShareLib *down = qobject_cast<HttpNetWork::HttpNetWorkShareLib *>(sender());
		QVariant vurl = down->property("GroupId");
		QString strGroupId = vurl.value<QString>();
		delete mapGrpFile[strGroupId];
		mapPerFile.remove(strGroupId);
		IMessageBox::tip(NULL, tr("Warning"), tr("Failed to get business card information!"));
	}
}

void profilemanager::slotShareGroupHeader(bool success)
{
	HttpNetWork::HttpDownLoadFile *down = qobject_cast<HttpNetWork::HttpDownLoadFile *>(sender());
	GroupInfo group = down->getData().value<GroupInfo>();
	emit profilemanager::getInstance()->sigCreateGrpFileByInfo(group);
}

void profilemanager::slotAddressInfo(AddressInfo addInfo)
{
	OPRequestShareLib *act = qobject_cast<OPRequestShareLib*>(sender());
	QVariant vType = act->property("UserId");
	int iUserId = vType.value<int>();
	perProfileWidget* pPer = mapPerFile[QString::number(iUserId)];
	if (pPer)
	{
		pPer->setAddress(addInfo);
	}
	
	//pPer->show();
	//pPer->activateWindow();
}