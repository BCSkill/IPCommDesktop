<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="th_TH">
<context>
    <name>OPMainManagerShareLib</name>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="196"/>
        <source>Screen capture hotkey conflict!</source>
        <translation>ความขัดแย้งฮอตฮอตจับภาพหน้าจอ!</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="200"/>
        <source>Quickly open hotkey conflicts!</source>
        <translation>เปิดข้อขัดแย้งของฮอตคีย์อย่างรวดเร็ว!</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="212"/>
        <source>Notice</source>
        <translation>แจ้งให้ทราบ</translation>
    </message>
    <message>
        <source>OpenPlanet  </source>
        <translation type="vanished">การสื่อสารระหว่างดวงดาว</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="264"/>
        <source>Telecomm  </source>
        <translation></translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="839"/>
        <source>:/GroupChat/Resources/groupchat/group.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1015"/>
        <location filename="opmainmanagersharelib.cpp" line="1065"/>
        <source>Clear chat history: </source>
        <translation>ล้างประวัติการแชท:</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1206"/>
        <source>[image]</source>
        <translation>[ภาพ]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1208"/>
        <source>[audio]</source>
        <translation>[เสียง]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1210"/>
        <source>[video]</source>
        <translation>[วีดีโอ]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1212"/>
        <source>[file]</source>
        <translation>[File]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1214"/>
        <source>This message type is not supported at this time</source>
        <translation>ประเภทข้อความนี้ไม่รองรับในขณะนี้</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1216"/>
        <source>[transfer]</source>
        <translation>[การโอนเงิน]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1218"/>
        <source>[secret letter]</source>
        <translation>[จดหมายลับ]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1220"/>
        <source>[secret image]</source>
        <translation>[ภาพลับ]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1222"/>
        <source>[secret file]</source>
        <translation>[ไฟล์ลับ]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1228"/>
        <source>[notice]</source>
        <translation>[ประกาศ]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1231"/>
        <source>[location]</source>
        <translation>[สถานที่]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1233"/>
        <source>[gw]</source>
        <translation>[คลื่นความโน้มถ่วง]</translation>
    </message>
</context>
<context>
    <name>profilemanager</name>
    <message>
        <location filename="profilemanager.cpp" line="270"/>
        <location filename="profilemanager.cpp" line="318"/>
        <location filename="profilemanager.cpp" line="328"/>
        <source>Warning</source>
        <translation>การเตือน</translation>
    </message>
    <message>
        <location filename="profilemanager.cpp" line="270"/>
        <location filename="profilemanager.cpp" line="318"/>
        <location filename="profilemanager.cpp" line="328"/>
        <source>Failed to get business card information!</source>
        <translation>ไม่สามารถรับข้อมูลนามบัตรได้!</translation>
    </message>
</context>
</TS>
