﻿#include "opmainmanagersharelib.h"
#include "imdownloadheaderimg.h"
#include "QStringLiteralBak.h"
#include "qxtglobalshortcut.h"
#include <QSettings>
#include "globalmanager.h"
#include "profilemanager.h"
#include <QMimeData>
#ifndef Q_OS_WIN
#include "inline_mac.h"
#include "oescreenshot.h"
#endif

OPMainManagerShareLib::OPMainManagerShareLib(QObject *parent)
: QObject(parent)
{
	mainWidget = NULL;
	contactsManager = NULL;
	chatManager = NULL;
	userProfileManager = NULL;
	walletManager = NULL;
	m_pProFileManager = NULL;

	createGroupWidget = NULL;
	addPersonWidget = NULL;

	m_pSettingsWidget = NULL;

	m_shortcutPic = NULL;
	m_shortcutOpen = NULL;
}

OPMainManagerShareLib::~OPMainManagerShareLib()
{
	if (mainWidget)
	{
		mainWidget->deleteLater();
		mainWidget = NULL;
	}
	if (contactsManager)
	{
		contactsManager->deleteLater();
		contactsManager = NULL;
	}
	if (chatManager)
	{
		chatManager->deleteLater();
		chatManager = NULL;
	}
	if (userProfileManager)
	{
		userProfileManager->deleteLater();
		userProfileManager = NULL;
	}
	if (walletManager)
	{
		walletManager->deleteLater();
		walletManager = NULL;
	}
	if (m_pProFileManager)
	{
		m_pProFileManager->deleteLater();
		m_pProFileManager = NULL;
	}


	if (createGroupWidget)
	{
		createGroupWidget->close();
	}
	if (addPersonWidget)
	{
		addPersonWidget->close();
	}
	if (m_pSettingsWidget)
	{
		delete m_pSettingsWidget;
		m_pSettingsWidget = NULL;
	}
	if (m_shortcutPic)
	{
		delete m_shortcutPic;
		m_shortcutPic = NULL;
	}
	if (m_shortcutOpen)
	{
		delete m_shortcutOpen;
		m_shortcutOpen = NULL;
	}
}

void OPMainManagerShareLib::slotUpdateHtmlBuddyHeaderImagePath(int buddyId, QString path)
{
	if (mainWidget)
	{
		UserInfo userInfo = gDataManager->getUserInfo();
		if (userInfo.nUserID == buddyId)
		{
			IMDownLoadHeaderImg *headerImage = new IMDownLoadHeaderImg;
			if (headerImage->IsImageFile(path))
			{
				mainWidget->setHeaderImage(headerImage->GetImagePix(path));  //设置本地图像
				headerImage->deleteLater();
			}
		}
	}
}

void OPMainManagerShareLib::startMainWidget(UserInfo userInfo)
{
	//兼容旧版绝对路径头像
//	gDataBaseOpera->ChangeOldHeaderPath();
//	gSocketMessage->ChangeOldPathCmd();
	mainWidget = new IMMainWidget();
	gDataManager->setMainWidget(mainWidget);

	connect(gSettingsManager, SIGNAL(sigRestart()), this, SLOT(slotRestart()));//切换主题重启

	connect(this, SIGNAL(sigSetMainWidgetStatusLabel(QString)), mainWidget, SLOT(slotSetMainWidgetStatusLabel(QString)));     //设置消息状态
	connect(this, SIGNAL(sigNetWarning(bool)), mainWidget, SLOT(slotNetWarning(bool)));
	connect(this, SIGNAL(sigClickedDock()), mainWidget, SLOT(slotClickedDock()));
    connect(this, SIGNAL(sigGlobalMouseMouse()), mainWidget, SLOT(slotGlobalMouseMouse()));

	connect(mainWidget, SIGNAL(sigDormancyState(bool)), this, SLOT(slotDormancyState(bool)));                                //机器休眠状态

    connect(mainWidget, SIGNAL(sigOpenChat(int, QVariant)), this, SLOT(slotOpenChat(int, QVariant)));
	connect(profilemanager::getInstance(), SIGNAL(sigOpenChat(int, QVariant)), this, SLOT(slotOpenChat(int, QVariant)));
	connect(mainWidget, SIGNAL(sigCheckUpdate()), this, SIGNAL(sigCheckUpdate()));
#ifndef Q_OS_WIN
    connect(mainWidget, SIGNAL(sigNotifyReplied(QString, QString)), this, SLOT(slotNotifyReplied(QString, QString)));
#endif

	connect(mainWidget, SIGNAL(sigExit()), this, SIGNAL(sigExit()));     //设置消息状态
	userProfileManager = new UserProfileDataManager(mainWidget);
	connect(mainWidget, SIGNAL(sigUserProfile()), userProfileManager, SLOT(slotProfileWidget()));
	connect(mainWidget, SIGNAL(sigSettings()), this, SLOT(slotSettings()));//设置界面
	//connect(gSettingsManager, SIGNAL(sigQuickOpen()), mainWidget, SLOT(slotQuickOpen()));//设置界面
	connect(this, SIGNAL(sigQuickOpen()), mainWidget, SLOT(slotQuickOpen()));//设置界面
	
	chatManager = new ChatDataManager();
	connect(chatManager, SIGNAL(sigWidgetMinSize()), this, SLOT(slotWidgetMinSize()));
	connect(chatManager, SIGNAL(sigShowNormalWindow()), this, SLOT(slotShowNormalWindow()));
	connect(chatManager, SIGNAL(sigMakeGroupHeader(QString)), this, SLOT(slotMakeGroupHeader(QString)));
	connect(chatManager, SIGNAL(sigDealTrayIocnFlash(QString)), mainWidget, SLOT(slotDealTrayIconFlash(QString)));
	connect(chatManager, SIGNAL(sigCountMessageNum(int)), mainWidget, SLOT(slotCountMessage(int)));
	connect(chatManager, SIGNAL(sigAddChatGroup(QString)), this, SLOT(slotAddChatGroup(QString)));
	connect(chatManager, SIGNAL(sigTransferRecord(RecordInfo)), this, SLOT(slotTransferRecord(RecordInfo)));
	connect(chatManager, SIGNAL(sigUpdateSelfImage(int, QPixmap)), this, SLOT(slotUpdateUserHeaderImg(int, QPixmap)));
	connect(chatManager, SIGNAL(sigShowDeviceWidget()), this, SLOT(slotShowDeviceWidget()));
	connect(mainWidget, SIGNAL(sigTrayOpenChat(int, QVariant)), chatManager, SLOT(recvChat(int, QVariant)));
	mainWidget->addWidget(chatManager->getChatWidget());

	contactsManager = new ContactsDataManager();
	mainWidget->addWidget(contactsManager->getContactsWidget());

	connect(contactsManager, SIGNAL(sigOpenChat(int, QVariant)), this, SLOT(slotOpenChat(int, QVariant)));
	connect(contactsManager, SIGNAL(sigAddCreateGroup()), this, SLOT(slotAddCreateGroup()));
	connect(contactsManager, SIGNAL(sigAddPerson()), this, SLOT(slotAddPerson()));
	connect(contactsManager, SIGNAL(sigUpdateImage(int, QVariant)), chatManager, SLOT(recvChat(int, QVariant)));
	connect(this, SIGNAL(sigUpdateMessageState(MessageInfo, int)), chatManager, SLOT(slotUpdateMessageState(MessageInfo, int)));
	connect(this, SIGNAL(sigRevOtherDeviceMsg(MessageInfo)), chatManager, SLOT(slotRevOtherDeviceMsg(MessageInfo)));
	connect(contactsManager, SIGNAL(sigMessageNum(int)), mainWidget, SLOT(slotContactsMessageNum(int)));
	connect(mainWidget, SIGNAL(sigNewApply()), contactsManager, SLOT(slotNewApply()));
	//connect(this, SIGNAL(sigRevOtherDeviceMsg(MessageInfo)), this, SLOT(slotAddStranger(MessageInfo)));
	connect(contactsManager, SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget*)), chatManager, SLOT(slotOpenPic(QString, QList<QString>*, QWidget*)));//图片浏览
	connect(contactsManager, SIGNAL(sigTipMessage(int, QString, QString)), chatManager, SLOT(slotTipMessage(int, QString, QString)));
	connect(IMMainWidget::self(), SIGNAL(sigTipMessage(int, QString, QString)), chatManager, SLOT(slotTipMessage(int, QString, QString)));
	connect(contactsManager, SIGNAL(sigShareID(int, QString)), chatManager, SLOT(slotShareWidget(int, QString)));
	connect(contactsManager, SIGNAL(sigShareID(int, QString)), this, SLOT(slotShowChatWidget()));
	connect(contactsManager, SIGNAL(sigUpdateInfo(int, QVariant)), this, SLOT(slotUpdateInfo(int, QVariant)));
	connect(contactsManager, SIGNAL(sigUpdateSelfMessage(bool, QVariant, QString, bool, QMap<QString, QVariant>)), this, SLOT(slotUpdateSelfMessage(bool, QVariant, QString, bool, QMap<QString, QVariant>)));
	slotInitMainWidget(userInfo);
	mainWidget->setAppVersion(gDataManager->getAppConfigInfo().appVersion.versionID);
	mainWidget->setAppName(gDataManager->getAppConfigInfo().appVersion.appString);
	contactsManager->UpdateNewFriendWidget();

	walletManager = new EWalletManager(NULL);
	walletManager->setUserInfo(userInfo);
	mainWidget->addWidget(walletManager->getWalletWidget());
	connect(chatManager, SIGNAL(sigTransferWindow(QString,QString)), walletManager, SLOT(slotTransferWindow(QString,QString)));
	connect(chatManager, SIGNAL(sigETHWindow(QString, QString)), walletManager, SLOT(slotETHWindow(QString, QString)));
	connect(chatManager, SIGNAL(sigBTCWindow(QString, QString)), walletManager, SLOT(slotBTCWindow(QString, QString)));
	connect(chatManager, SIGNAL(sigEOSWindow(QString, QString)), walletManager, SLOT(slotEOSWindow(QString, QString)));
	connect(chatManager, SIGNAL(sigHostingCharge(int, QString, QString, QString)), walletManager, SLOT(slotHostingCharge(int, QString, QString, QString)));
	connect(walletManager, SIGNAL(sigTransferMsg(QString, QString, QString)), chatManager, SLOT(slotTransferMsg(QString, QString, QString)));

	mainWidget->setCurrentWidget(OpenPer);
	//资料窗口管理
	//m_pProFileManager = new profilemanager(NULL);
	//注册截图
	bool bSc = InitScreenShot();
#ifdef Q_OS_WIN
	//注册显示隐藏
	bool bQo = InitQuickOpen();
	QString strWarn = "";
	if (!bSc)strWarn += tr("Screen capture hotkey conflict!");
	if (!bQo)
	{
		if (strWarn != "")strWarn += " ";
		strWarn += tr("Quickly open hotkey conflicts!");
	}
#endif
	//第一次显示时，将窗口居中。
	QDesktopWidget *pDesk = QApplication::desktop();
	int mainIndex = pDesk->primaryScreen();
	QRect screen = pDesk->screenGeometry(mainIndex);
	mainWidget->move((screen.width() - mainWidget->width()) / 2, (screen.height() - mainWidget->height()) / 2);
	mainWidget->show();
#ifdef Q_OS_WIN
	if (strWarn != "" && gSettingsManager->getCheckKey() == 2)
	{	//checkKey为2时显示提示0时不显示
		IMessageBox::tip(mainWidget, tr("Notice"), strWarn);
	}
#endif

	if (IMMainWidget::self())
	{
		//更新HTML里的头像
		connect(IMMainWidget::self(), SIGNAL(sigUpdateBuddyHeaderImagePath(int, QString)), this, SLOT(slotUpdateHtmlBuddyHeaderImagePath(int, QString)));
	}
}

//最小化
void OPMainManagerShareLib::slotWidgetMinSize()
{
    qDebug() << "OPMainManagerShareLib::slotWidgetMinSize.........................";
	if (mainWidget)
	{
		mainWidget->showMinimized();
	}
}

//还原窗口
void OPMainManagerShareLib::slotShowNormalWindow()
{
	if (mainWidget)
	{
		mainWidget->showNormal();
	}
}

void OPMainManagerShareLib::slotInitMainWidget(UserInfo userInfo)
{
	if (mainWidget)
	{
		mainWidget->setNikeName(userInfo.strUserName);
		IMDownLoadHeaderImg *headerImage = new IMDownLoadHeaderImg;
		if (headerImage->IsImageFile(userInfo.strUserAvatarLocal))
		{
			mainWidget->setHeaderImage(headerImage->GetImagePix(userInfo.strUserAvatarLocal));  //设置本地图像
			headerImage->deleteLater();
		}
		else
		{
			mainWidget->setHeaderImage(headerImage->GetImagePix(userInfo.strUserDefaultAvatar));//设置默认图像
		}
		connect(headerImage, SIGNAL(sigUpdateBuddyHeaderImage(int, QPixmap)), this, SLOT(slotUpdateUserHeaderImg(int, QPixmap)));
		connect(headerImage, SIGNAL(sigUpdateBuddyHeaderImagePath(int, QString)), this, SLOT(slotUpdateBuddyHeaderImgPath(int, QString)));
		headerImage->StartDownLoadUserInfoHeaderImage(userInfo);

		if (gDataManager)
		{
			AppConfig conf = gDataManager->getAppConfigInfo();
			QString msg = tr("OpenPlanet  ") + conf.appVersion.versionID;
			mainWidget->slotSetMainWidgetStatusLabel(msg);
		}
	}
}


//更新好友头像
void OPMainManagerShareLib::slotUpdateBuddyHeaderImgPath(int nUserID, QString path)
{
	if (chatManager)
	{
		chatManager->updateBuddyHeaderImgPath(nUserID, path);
	}
}


//更新好友头像
void OPMainManagerShareLib::slotUpdateBuddyHeaderImg(int nUserID, QString imgPath)
{
	IMDownLoadHeaderImg *headerImage = qobject_cast<IMDownLoadHeaderImg*>(sender());
	if (contactsManager)
	{
		contactsManager->updateImage(BuddyUpdate, nUserID, imgPath);
	}
	if (headerImage)
	{
		headerImage->deleteLater();
	}
}

//更新部落头像
void OPMainManagerShareLib::slotUpdateGroupHeaderImg(int nUserID, QString imgPath)
{
	IMDownLoadHeaderImg *headerImage = qobject_cast<IMDownLoadHeaderImg*>(sender());
	if (contactsManager)
	{
		contactsManager->updateImage(GroupUpdate, nUserID, imgPath);
	}
	if (headerImage)
	{
		headerImage->deleteLater();
		headerImage = NULL;
	}
}

//更新用户头像
void OPMainManagerShareLib::slotUpdateUserHeaderImg(int nUserID, QPixmap pix)
{
	IMDownLoadHeaderImg *headerImage = qobject_cast<IMDownLoadHeaderImg*>(sender());
	if (mainWidget)
	{
		mainWidget->setHeaderImage(pix);
	}
	if (headerImage)
	{
		headerImage->deleteLater();
		headerImage = NULL;
	}
}

void OPMainManagerShareLib::slotBuddysManager(int type, QVariant data)
{
	if (contactsManager)
	{
		IMDownLoadHeaderImg *headerImage = new IMDownLoadHeaderImg;
		contactsManager->recvBuddyData(type, data);
		BuddyInfo buddy = data.value<BuddyInfo>();
		//判断是否下载头像
		if (headerImage->IsImageFile(buddy.strLocalAvatar))
		{
			contactsManager->updateImage(BuddyUpdate, buddy.nUserId, buddy.strLocalAvatar);
			headerImage->deleteLater();
		}
		else
		{
			contactsManager->updateImage(BuddyUpdate, buddy.nUserId, buddy.strDefaultAvatar);
			connect(headerImage, SIGNAL(sigUpdateBuddyHeaderImagePath(int, QString)), this, SLOT(slotUpdateBuddyHeaderImg(int, QString)));
			connect(headerImage, SIGNAL(sigUpdateBuddyHeaderImagePath(int, QString)), this, SLOT(slotUpdateBuddyHeaderImgPath(int, QString)));

			headerImage->StartDownLoadBuddyeHeaderImage(buddy);
		}
	}
	if (walletManager)   //walletWidget有部落数量显示的功能。
	{
		if (type == BuddyInsert)
		    walletManager->countBuddysNum();
	}
}

//更新好友、部落信息
void OPMainManagerShareLib::slotUpdateInfo(int type, QVariant var)
{
	if (contactsManager && chatManager)
	{
		if (type == BuddyUpdate)
			contactsManager->recvBuddyData(type, var);
		if (type == GroupUpdate)
			contactsManager->recvGroupData(type, var);
		chatManager->recvChat(type, var);
	}
}

//用户退出部落
void OPMainManagerShareLib::slotUserQuitGroup(QString groupID, QString buddyID)
{
	UserInfo userInfo = gDataManager->getUserInfo();

	if (userInfo.nUserID == buddyID.toInt())
	{
		slotDeleteGroup(groupID);
		if (gDataBaseOpera)
		{
			gDataBaseOpera->DBDeleteGroupInfoByID(groupID);
		}
	}
	chatManager->recvChat(QuitGroup, QVariant(groupID), QVariant(buddyID));
}

//添加好友成功
void OPMainManagerShareLib::slotAddFriendSuccess(BuddyInfo buddyInfo)
{
	if (contactsManager)
	{
		QVariant var = QVariant::fromValue(buddyInfo);
		contactsManager->setUpdateIndexFlag(false);
		contactsManager->recvBuddyData(BuddyInsert, var);
		contactsManager->setUpdateIndexFlag(true);
		contactsManager->UpdateNewFriendWidget();
	}

	walletManager->countBuddysNum();
}

//收到好友申请
void OPMainManagerShareLib::slotApplyFriend(int i, MessageInfo *messageInfo)
{
	if (contactsManager && mainWidget)
	{
		contactsManager->UpdateNewFriendWidget();
		if (i == 0)
		{
			if (mainWidget->isHidden())
			{
				messageInfo->MessageChildType = Message_SYSTEM;
				mainWidget->StartMessageFlash(NewApply, ":/GroupChat/Resources/groupchat/newfriend.png", messageInfo);
			}
			//contactsManager->SetMessageNumUp();
		}
	}
}

//加入部落成功
void OPMainManagerShareLib::slotAddSuccessGroup(GroupInfo groupInfo)
{
	if (contactsManager)
	{
		QVariant var = QVariant::fromValue(groupInfo);
		contactsManager->recvGroupData(GroupInsert, var);
	}

	walletManager->countGroupsNum();
}

//部落添加成员成功
void OPMainManagerShareLib::slotAddSuccessGroupUserInfo(QString strMap, BuddyInfo buddy)
{
	if (chatManager && contactsManager)
	{
		chatManager->recvChat(GroupAddUser, QVariant::fromValue(strMap), QVariant::fromValue(buddy));
		contactsManager->UpdateNewFriendWidget();
	}
}

void OPMainManagerShareLib::slotGroupsManager(int type, QVariant data)
{
	if (contactsManager)
	{
		IMDownLoadHeaderImg *headerImage = new IMDownLoadHeaderImg;
		if (headerImage)
		{
			contactsManager->recvGroupData(type, data);
			GroupInfo groupInfo = data.value<GroupInfo>();
			//判断是否下载头像
			if (headerImage->IsImageFile(groupInfo.groupLoacalHeadImage))
			{
				contactsManager->updateImage(GroupUpdate, groupInfo.groupId.toInt(), groupInfo.groupLoacalHeadImage);
				headerImage->deleteLater();
			}
			else
			{
				contactsManager->updateImage(GroupUpdate, groupInfo.groupId.toInt(), groupInfo.groupDefaultAvatar);
				//connect(headerImage, SIGNAL(sigUpdateBuddyHeaderImage(int, QPixmap)), this, SLOT(slotUpdateGroupHeaderImg(int, QPixmap)));
				connect(headerImage, SIGNAL(sigUpdateBuddyHeaderImagePath(int, QString)), this, SLOT(slotUpdateGroupHeaderImg(int, QString)));
				headerImage->StartDownLoadGroupHeaderImage(groupInfo);
			}
		}
	}
	if (walletManager)   //walletWidget有部落数量显示的功能。
	{
		if (type == GroupInsert)
			walletManager->countGroupsNum();
	}
}

void OPMainManagerShareLib::RevServerMessage(MessageInfo msgInfo)
{
	//qDebug() << "------RevServerMessage------";
	//qDebug() << "bIsSend" << msgInfo.bIsSend;
	//qDebug() << "ClientTime" << msgInfo.ClientTime;
	//qDebug() << "integral" << msgInfo.integral;
	//qDebug() << "interalTypeMain" << msgInfo.interalTypeMain;
	//qDebug() << "interalTypeVice" << msgInfo.interalTypeVice;
	//qDebug() << "isHavePMsgId" << msgInfo.isHavePMsgId;
	//qDebug() << "MessageChildType" << msgInfo.MessageChildType;
	//qDebug() << "MessageState" << msgInfo.MessageState;
	//qDebug() << "MsgDeliverType" << msgInfo.MsgDeliverType;
	//qDebug() << "msgID" << msgInfo.msgID;
	//qDebug() << "nFromUserID" << msgInfo.nFromUserID;
	//qDebug() << "nMsgOrder" <<msgInfo.nMsgOrder;
	//qDebug() << "nToUserID" << msgInfo.nToUserID;
	//qDebug() << "nUserID" <<msgInfo.nUserID;
	//qDebug() << "offLineFlag" <<msgInfo.offLineFlag;
	//qDebug() << "PerByte"<<msgInfo.PerByte;
	//qDebug() << "PMSGID"<<msgInfo.PMSGID;
	//qDebug() << "SendTime"<<msgInfo.SendTime;
	//qDebug() << "ServerTime"<<msgInfo.ServerTime;
	//qDebug() << "strMessageListDesc"<<msgInfo.strMessageListDesc;
	//qDebug() << "strMessageListJson"<<msgInfo.strMessageListJson;
	//qDebug() << "strMsg"<<msgInfo.strMsg;
	//qDebug() << "strRowId"<<msgInfo.strRowId;
	//qDebug() << "version"<<msgInfo.version;
	//qDebug() << "---------------------------------";

	
	if (chatManager)
	{
		if (msgInfo.MsgDeliverType == 0)
		{
			QString userID = QString::number(msgInfo.nFromUserID);
			BuddyInfo info = gDataBaseOpera->DBGetBuddyInfoByID(userID);
			
			if (info.nUserId == -1)
			{
				info = gDataBaseOpera->DBGetGroupUserFromID(userID);
				if (info.strNickName.isEmpty())
				{
					//收到的消息如果本地没有该联系人的数据，就进行下载。
					this->slotAddStranger(msgInfo);
					return;
				}
			}
			
				QVariant message, buddy;
				message.setValue(msgInfo);
				buddy.setValue(info);
				chatManager->recvChat(PerMessage, message, buddy);
			
			if (mainWidget)
			{
				if (mainWidget->isHidden() && info.msgPrompt != 1)
				{
					mainWidget->StartMessageFlash(OpenPer, info.strLocalAvatar, &msgInfo);
				}
				else
				{
					if (!mainWidget->hasFocus())
						QApplication::alert(mainWidget);
				}
#ifdef Q_OS_MAC
                MessageNoticePer(msgInfo,info);
#endif
			}
		}
		if (msgInfo.MsgDeliverType == 1)
		{
			QString groupID = QString::number(msgInfo.nToUserID);
			GroupInfo info = gDataBaseOpera->DBGetGroupFromID(groupID);
			if (info.createUserId.isEmpty())
			{
				//收到的消息如果本地没有该部落的数据，就进行下载。
				this->slotAddStranger(msgInfo);
				return;
			}

			QVariant message, group;
			message.setValue(msgInfo);
			group.setValue(info);
			chatManager->recvChat(GroupMessage, message, group);

			if (mainWidget)
			{
				if (mainWidget->isHidden()&& info.msgPrompt!=1)
				{
					mainWidget->StartMessageFlash(OpenGroup, info.groupLoacalHeadImage, &msgInfo);
				}
				else
				{
					if (!mainWidget->hasFocus())
						QApplication::alert(mainWidget);
				}
#ifdef Q_OS_MAC
            MessageNoticeGroup(msgInfo,info);
#endif
			}
		}
	}
}

void OPMainManagerShareLib::slotOpenChat(int type, QVariant userID)
{
	/*
	if (!mainWidget->isVisible() || mainWidget->isMinimized())
	{
		mainWidget->close();
		
	}
	*/

	if (!mainWidget->hasFocus())
	{
#ifdef Q_OS_WIN
		//设置窗口置顶
		::SetWindowPos(HWND(mainWidget->winId()), HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
		::SetWindowPos(HWND(mainWidget->winId()), HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#endif
		mainWidget->show();
		mainWidget->activateWindow();
		mainWidget->setFocus();
	}

	if (userID.toString().isEmpty())
	{

	}
	else
	{
		chatManager->recvChat(type, userID);
		//设置当前页面
		mainWidget->setCurrentWidget(type);
	}
}

void OPMainManagerShareLib::slotAddCreateGroup()
{
	if (createGroupWidget)
		createGroupWidget->close();
	createGroupWidget = new CreateGroupWidget;
	connect(createGroupWidget, SIGNAL(sigCreateGroupSuccess(QString)), this, SLOT(slotOpenCreateGroup(QString)));
	connect(createGroupWidget, SIGNAL(sigClose()), this, SLOT(slotCloseCreateGroup()));
	createGroupWidget->show();
}

void OPMainManagerShareLib::slotAddChatGroup(QString buddyID)
{
	if (createGroupWidget)
		createGroupWidget->close();
	createGroupWidget = new CreateGroupWidget;
	connect(createGroupWidget, SIGNAL(sigCreateGroupSuccess(QString)), this, SLOT(slotOpenCreateGroup(QString)));
	connect(createGroupWidget, SIGNAL(sigClose()), this, SLOT(slotCloseCreateGroup()));
	createGroupWidget->selectContact(buddyID);
	createGroupWidget->show();
}

void OPMainManagerShareLib::slotOpenCreateGroup(QString groupID)
{
	this->slotOpenChat(OpenGroup, QVariant(groupID));
	this->slotCloseCreateGroup();
}

void OPMainManagerShareLib::slotCloseCreateGroup()
{
	createGroupWidget->close();
	createGroupWidget = NULL;
}

void OPMainManagerShareLib::slotAddPerson()
{
	if (addPersonWidget)
		addPersonWidget->close();
	addPersonWidget = new IMAddPerson();
	connect(addPersonWidget, SIGNAL(sigClose()), this, SLOT(slotCloseAddPerson()));
	addPersonWidget->show();
}

void OPMainManagerShareLib::slotCloseAddPerson()
{
	addPersonWidget->close();
	addPersonWidget = NULL;
}

void OPMainManagerShareLib::setWalletInfo(WalletInfo info)
{
	walletManager->setWalletInfo(info);
}

//设置休眠状态,如果休眠，不重连。如果唤醒，重新开始重连
void OPMainManagerShareLib::slotDormancyState(bool bState)
{
	if (gSocketMessage)
	{
		gSocketMessage->setReConnectState(!bState);
	}
}

//删除部落
void OPMainManagerShareLib::slotDeleteGroup(QString groupID)
{
	if (contactsManager)
	{
		QVariant var = QVariant::fromValue(groupID);
		contactsManager->recvGroupData(GroupDelete, var);
	}
	chatManager->recvChat(QuitGroup, QVariant(groupID), QVariant(""));
	walletManager->countGroupsNum();
}

void OPMainManagerShareLib::slotGroupNoSpeak(int groupId, int noSpeak)
{
	if (chatManager)
	{
		QVariant id = QVariant::fromValue(groupId);
		QVariant varSpeak = QVariant::fromValue(noSpeak);
		chatManager->recvChat(GroupNoSpeak, id, varSpeak);
	}
}

void OPMainManagerShareLib::slotDeleteBuddy(int type, QVariant userID)
{
	if (contactsManager)
	{
		contactsManager->recvBuddyData(type, userID);
	}
	chatManager->recvChat(BuddyDelete, QVariant(userID), QVariant(""));
	walletManager->countBuddysNum();
}

void OPMainManagerShareLib::slotAddStranger(MessageInfo message)
{
	if (message.MsgDeliverType == 0)
	{
		QString userID = QString::number(message.nFromUserID);
		HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
		connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotStrangerResult(bool, QString)));

		QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/user/getOtherUserByUserId?otherUserId=" + userID;
		http->setData(QVariant::fromValue(message));
		http->getHttpRequest(url);
	}
	if (message.MsgDeliverType == 1)
	{
		//在部落消息中，发送者为部落成员，接收者为部落。
		QString groupID = QString::number(message.nToUserID);
		HttpNetWork::HttpNetWorkShareLib *request = new HttpNetWork::HttpNetWorkShareLib();
		connect(request, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotRequestGroupResult(bool, QString)));
		UserInfo userInfo = gDataManager->getUserInfo();
		AppConfig appConf = gDataManager->getAppConfigInfo();
		QString strResult = appConf.MessageServerAddress + HTTP_GETGROUPINFOBYGROUPID + QString("?userId=") + QString::number(userInfo.nUserID) + "&passWord=" + userInfo.strUserPWD + "&groupId=" + groupID;
		request->setData(QVariant::fromValue(message));
		request->getHttpRequest(strResult);
	}
}

void OPMainManagerShareLib::slotStrangerResult(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		if (map.value("result").toString() == "success")
		{
			QVariantMap user = map.value("user").toMap();

			BuddyInfo buddy;
			if (user.value("userType").toInt() == 1)
			{
				
				buddy.strHttpAvatar = user.value("avatar").toString();
				buddy.nUserId = user.value("userId").toInt();
				buddy.strUserName = user.value("userName").toString();
				buddy.strNickName = buddy.strUserName;
#ifdef Q_OS_WIN
				buddy.strLocalAvatar = gSettingsManager->getUserPath() + "/resource/header/" + QString::number(buddy.nUserId) + ".jpg";
#else
                QDir dir;
                buddy.strLocalAvatar = gSettingsManager->getUserPath() + "resource/header/" + QString::number(buddy.nUserId) + ".jpg";
#endif
				buddy.nUserType = 1;
				buddy.BuddyType = 0;
			}
			if (user.value("userType").toInt() == 0)
			{
				buddy.nUserId = user.value("userId").toInt();
				buddy.strHttpAvatar = user.value("avatar").toString();        
#ifdef Q_OS_WIN
				buddy.strLocalAvatar = gSettingsManager->getUserPath() + "/resource/header/" + QString::number(buddy.nUserId) + ".jpg";
#else
                QDir dir;
                buddy.strLocalAvatar = gSettingsManager->getUserPath() + "resource/header/" + QString::number(buddy.nUserId) + ".jpg";
#endif
				buddy.strNickName = user.value("nickName").toString();
				buddy.strPhone = user.value("phone").toString();
				buddy.strSex = user.value("sex").toString();
				buddy.strSign = user.value("sign").toString();
				buddy.strUserName = user.value("userName").toString();
				buddy.strPingYin = AlphabeticalSortSharedLib().GetChineseSpell(buddy.strNickName);
				buddy.disableStrangers = user.value("disableStrangers").toInt();

				buddy.nUserType = 0;
				buddy.BuddyType = 1;
			}

			gDataBaseOpera->DBInsertBuddyInfo(buddy);

			HttpNetWork::HttpNetWorkShareLib* netWork = qobject_cast<HttpNetWork::HttpNetWorkShareLib*>(sender());
			if (netWork)
			{
				QVariant var = netWork->getData();
				MessageInfo msgInfo = var.value<MessageInfo>();

				RevServerMessage(msgInfo);
#ifdef Q_OS_MAC
                MessageNoticePer(msgInfo,buddy);
#endif
				HttpNetWork::HttpDownLoadFile *http = new HttpNetWork::HttpDownLoadFile;
				http->setData(QVariant::fromValue(buddy));
				connect(http, SIGNAL(sigDownFinished(bool)),
					this, SLOT(slotDownloadStrangerHeader(bool)));
				http->setData(QVariant::fromValue(buddy));
				http->StartDownLoadFile(buddy.strHttpAvatar, buddy.strLocalAvatar);
			}
		}
	}
}

void OPMainManagerShareLib::slotDownloadStrangerHeader(bool)
{
	HttpNetWork::HttpDownLoadFile *http = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());
	if (http)
	{
		QVariant var = http->getData();
		BuddyInfo buddy = var.value<BuddyInfo>();

		chatManager->recvChat(BuddyUpdate, QVariant::fromValue(buddy));

		if (buddy.BuddyType == 1)  //buddyType是1.代表可以显示。
			contactsManager->recvBuddyData(BuddyInsert, QVariant::fromValue(buddy));
	}
}

void OPMainManagerShareLib::slotRequestGroupResult(bool bResult, QString result)
{
	if (bResult)
	{
		QJsonParseError jsonError;
		GroupInfo groupInfo;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(result.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QVariantMap result = jsonDocument.toVariant().toMap();
				if (result["result"].toString() == "success")
				{
					QVariantMap resultInfo = result["group"].toMap();
					groupInfo.groupHttpHeadImage = resultInfo["avatar"].toString();
					groupInfo.createTime = resultInfo["createTime"].toString();
					groupInfo.createUserId = resultInfo["createUserId"].toString();
					groupInfo.groupId = resultInfo["groupId"].toString();
#ifdef Q_OS_WIN
					QString strPath = gSettingsManager->getUserPath();
					groupInfo.groupLoacalHeadImage = strPath + "/resource/header/groupheader/" + groupInfo.groupId + ".jpg";
#else
                    groupInfo.groupLoacalHeadImage = gSettingsManager->getUserPath() + "resource/header/groupheader/" + groupInfo.groupId + ".jpg";
#endif
					groupInfo.groupDefaultAvatar = tr(":/GroupChat/Resources/groupchat/group.png");
					groupInfo.groupName = resultInfo["groupName"].toString();
					groupInfo.noSpeak = resultInfo["noSpeak"].toInt();
					groupInfo.groupType = resultInfo["groupType"].toInt();

					//插入数据库。
					gDataBaseOpera->DBInsertGroupInfo(groupInfo);

					HttpNetWork::HttpNetWorkShareLib* netWork = qobject_cast<HttpNetWork::HttpNetWorkShareLib*>(sender());
					QVariant var = netWork->getData();
					MessageInfo msgInfo = var.value<MessageInfo>();

					RevServerMessage(msgInfo);
#ifdef Q_OS_MAC
                    MessageNoticeGroup(msgInfo,groupInfo);
#endif

					HttpNetWork::HttpDownLoadFile *http = new HttpNetWork::HttpDownLoadFile;
					http->setData(QVariant::fromValue(groupInfo));
					connect(http, SIGNAL(sigDownFinished(bool)),
						this, SLOT(slotDownloadGroupHeader(bool)));

					http->StartDownLoadFile(groupInfo.groupHttpHeadImage, groupInfo.groupLoacalHeadImage);
				}
			}
		}
	}
}

void OPMainManagerShareLib::slotDownloadGroupHeader(bool)
{
	HttpNetWork::HttpDownLoadFile *http = qobject_cast<HttpNetWork::HttpDownLoadFile*>(sender());
	if (http)
	{
		QVariant var = http->getData();
		GroupInfo group = var.value<GroupInfo>();

		chatManager->recvChat(GroupUpdate, QVariant::fromValue(group));
		contactsManager->recvGroupData(GroupInsert, QVariant::fromValue(group));
	}
}

void OPMainManagerShareLib::slotMakeGroupHeader(QString groupID)
{
	GroupInfo group = gDataBaseOpera->DBGetGroupFromID(groupID);
	QFile header(group.groupLoacalHeadImage);
	QByteArray bytearray;
	if (header.open(QIODevice::ReadOnly))
	{
		bytearray = header.readAll();
		header.close();
	}
	if (!header.exists() || bytearray.isEmpty() || bytearray.endsWith("temp"))
	{
		QList<BuddyInfo> buddyList = gDataBaseOpera->DBGetGroupBuddyInfoFromID(groupID);
		if (!buddyList.isEmpty())
		{
			QPixmap back(300, 300);
			back.fill(Qt::white);

			QPainter *painter = new QPainter;
			painter->begin(&back);

			for (int i = 0; i < buddyList.count(); i++)
			{
				QByteArray bytearray;
				QFile imageFile(buddyList[i].strLocalAvatar);
				if (imageFile.size() != 0 && imageFile.open(QIODevice::ReadOnly))
				{
					bytearray = imageFile.readAll();
				}
				imageFile.close();

				QPixmap image;
				image.loadFromData(bytearray);

				if (image.isNull())
					image.load(":/PerChat/Resources/person/temp.png");

				if (buddyList.count() > 6)   //九个的格子。
				{
					image = image.scaled(QSize(80, 80));

					if (i >= 0 && i < 3)  //第一排。
						painter->drawPixmap(20 + i * 90, 20, image);
					if (i >= 3 && i < 6)  //第二排。
						painter->drawPixmap(20 + (i - 3) * 90, 110, image);
					if (i >= 6 && i < 9)  //第三排。
						painter->drawPixmap(20 + (i - 6) * 90, 200, image);
				}
				if (buddyList.count() == 5 || buddyList.count() == 6)
				{
					image = image.scaled(QSize(80, 80));

					if (i >= 0 && i < 3)
						painter->drawPixmap(20 + i * 90, 65, image);
					if (i >= 3 && i < 6)
						painter->drawPixmap(20 + (i - 3) * 90, 155, image);
				}

				if (buddyList.count() == 3 || buddyList.count() == 4)
				{
					image = image.scaled(QSize(120, 120));

					if (i >= 0 && i < 2)
						painter->drawPixmap(20 + i * 140, 20, image);
					if (i >= 2 && i < 4)
						painter->drawPixmap(20 + (i - 2) * 140, 160, image);
				}

				if (buddyList.count() <= 2)
				{
					image = image.scaled(QSize(120, 120));
					if (i == 0)
						painter->drawPixmap(20, 90, image);
					if (i == 1)
						painter->drawPixmap(160, 90, image);
				}

				if (i >= 9)
					break;
			}

			painter->end();
			delete painter;
			back.save(group.groupLoacalHeadImage);

			//加入标志。
			QFile markFile(group.groupLoacalHeadImage);
			if (markFile.open(QIODevice::Append))
			{
				markFile.write("temp");
				markFile.close();
			}

			contactsManager->recvGroupData(GroupUpdate, QVariant::fromValue(group));
			chatManager->recvChat(GroupUpdate, QVariant::fromValue(group));
		}
	}
}

void OPMainManagerShareLib::slotTransferRecord(RecordInfo record)
{
	if (record.sendId.isEmpty() || record.receiveId.isEmpty())
	{
		RecordDetailWidget *detailWidget = new RecordDetailWidget();
		detailWidget->setRecord(record);
		detailWidget->show();
	}
	else
	{
		RecordDetailPerWidget *detailWidget = new RecordDetailPerWidget();
		detailWidget->setRecord(record);
		detailWidget->show();
	}

}

void OPMainManagerShareLib::slotSettings()
{
	if (m_pSettingsWidget)
	{
		delete m_pSettingsWidget;
		m_pSettingsWidget = NULL;
	}
	m_pSettingsWidget = new SettingsWidget();
	UserInfo userinfo = gDataManager->getUserInfo();
	m_pSettingsWidget->setUserInfo(userinfo);
	m_pSettingsWidget->setScreenCut(gSettingsManager->getScreenShot());
	m_pSettingsWidget->setQuickOpen(gSettingsManager->getQuickOpen());
	m_pSettingsWidget->setSendMsg(gSettingsManager->getSendMeg());
	m_pSettingsWidget->setCheckKey(gSettingsManager->getCheckKey());
	m_pSettingsWidget->setUserPath(gSettingsManager->getUserPath());
	m_pSettingsWidget->setLanguage(gSettingsManager->getLanguage());
	QString strId = QString::number(gDataManager->getUserInfo().nUserID);
	QString strSize = gSettingsManager->getUserFileSize(strId);
	m_pSettingsWidget->setClear(tr("Clear chat history: ") + strSize);
	connect(m_pSettingsWidget, SIGNAL(sigSettingsClose()), this, SLOT(slotSettingsClose()));
	connect(m_pSettingsWidget, SIGNAL(sigCancel()), this, SIGNAL(sigCancel()));
	connect(m_pSettingsWidget, SIGNAL(sigScreenCut(QString)), this, SLOT(slotScreenCut(QString)));
	connect(m_pSettingsWidget, SIGNAL(sigQuickOpen(QString)), this, SLOT(slotSetQuickOpen(QString)));
	connect(m_pSettingsWidget, SIGNAL(sigSendMsg(int)), this, SLOT(slotSendMsg(int)));
	connect(m_pSettingsWidget, SIGNAL(sigCheckKey(int)), this, SLOT(slotCheckKey(int)));
	connect(m_pSettingsWidget, SIGNAL(sigClearPath()), this, SLOT(slotClearPath()));
	connect(m_pSettingsWidget, SIGNAL(sigChangePath(QString)), this, SLOT(slotChangePath(QString)));
	connect(m_pSettingsWidget, SIGNAL(sigLanguage(QString)), this, SLOT(slotLanguage(QString)));
	m_pSettingsWidget->show();
}
void OPMainManagerShareLib::slotSettingsClose()
{
	m_pSettingsWidget = NULL;
}

void OPMainManagerShareLib::slotScreenCut(QString strValue)
{
	gSettingsManager->setScreenShot(strValue);
}

void OPMainManagerShareLib::slotSetQuickOpen(QString strValue)
{
	gSettingsManager->setQuickOpen(strValue);
}

void OPMainManagerShareLib::slotSendMsg(int iValue)
{
	gSettingsManager->setSendMeg(iValue);
}

void OPMainManagerShareLib::slotCheckKey(int iValue)
{
	gSettingsManager->setCheckKey(iValue);
}

void OPMainManagerShareLib::slotRefreshDeviceState()
{
	walletManager->refreshDeviceState();
	chatManager->slotRefreshDeviceInfo();
}

void OPMainManagerShareLib::slotClearPath()
{
	QString strId = QString::number(gDataManager->getUserInfo().nUserID);
	gSettingsManager->DelUserFile(strId);
	gDataBaseOpera->ClearUserData();
	gSocketMessage->ClearUserData();
	QString strSize = gSettingsManager->getUserFileSize(strId);
	m_pSettingsWidget->setClear(tr("Clear chat history: ") + strSize);
}

void OPMainManagerShareLib::slotChangePath(QString strValue)
{
	sigCancelandDelete(strValue);
}

void OPMainManagerShareLib::slotShowDeviceWidget()
{
	mainWidget->setCurrentWidget(OpenWallet);
	walletManager->showDeviceWidget();
}

void OPMainManagerShareLib::slotShowChatWidget()
{
	/*功能调整，转发消息不再跳转聊天界面，因此注释掉。

	//此处openper和openGroup效果相同，都是跳转聊天界面。
	//mainWidget->setCurrentWidget(OpenPer);

	*/
}

bool OPMainManagerShareLib::InitScreenShot()
{
#ifdef Q_OS_WIN
	m_shortcutPic = new QxtGlobalShortcut(this);
	connect(m_shortcutPic, SIGNAL(activated()), this, SIGNAL(sigGlobalScreenShot()));
	bool bEn = m_shortcutPic->setShortcut(QKeySequence(gSettingsManager->getScreenShot()));
	connect(gSettingsManager, SIGNAL(sigSetScreenShot(QString)),this,SLOT(setScreenShot(QString)));
#else
	m_shortcutPic = new QxtGlobalShortcut(this);
    bool bEn = m_shortcutPic->setShortcut(QKeySequence(gSettingsManager->getScreenShot()));
#endif
	return bEn;
}

void OPMainManagerShareLib::setScreenShot(QString strValue)
{
	bool bEn = m_shortcutPic->setShortcut(QKeySequence(strValue));
}

bool OPMainManagerShareLib::InitQuickOpen()
{
	m_shortcutOpen = new QxtGlobalShortcut(this);
	connect(m_shortcutOpen, SIGNAL(activated()), this, SIGNAL(sigQuickOpen()));
	bool bEn = m_shortcutOpen->setShortcut(QKeySequence(gSettingsManager->getQuickOpen()));
	connect(gSettingsManager, SIGNAL(sigSetQuickOpen(QString)), this, SLOT(setQuickOpen(QString)));
	return bEn;
}

void OPMainManagerShareLib::setQuickOpen(QString strValue)
{
	bool bEn = m_shortcutOpen->setShortcut(QKeySequence(strValue));
}

void OPMainManagerShareLib::slotUpdateSelfMessage(bool bV, QVariant qV, QString msgV, bool bV2, QMap<QString, QVariant> params)
{
	chatManager->sigUpdateSelfMessage(bV, qV, msgV, bV2,params);
}

void OPMainManagerShareLib::RevInputting(MessageInfo msgInfo)
{
	chatManager->RevInputting(msgInfo);
}

void OPMainManagerShareLib::slotLanguage(QString strValue)
{
	if (gSettingsManager->getLanguage() != strValue)
	{
		gSettingsManager->setLanguage(strValue);

		QTimer::singleShot(1000, this, SLOT(slotRestart()));
	}
}

void OPMainManagerShareLib::slotRestart()
{
#ifdef Q_OS_WIN
	QStringList arguments;
	arguments.append("RestartMainExe");

	LPCWSTR path = reinterpret_cast<const wchar_t *>(QApplication::applicationFilePath().utf16());
	QString arg;
	for (int i = 0; i < arguments.count(); i++)
	{
		QString argument;
		if (arguments[i].isEmpty())
			argument = "\"\"";
		else
			argument = arguments[i];

		if (i == 0)
			arg += argument;
		else
			arg += " " + argument;
	}

	LPCWSTR str = reinterpret_cast<const wchar_t *>(arg.utf16());
	ShellExecute(0, L"open", path, str, NULL, SW_SHOW);

	qApp->quit();
#endif
#ifdef Q_OS_MAC
    QString strPath = QApplication::applicationFilePath();
    strPath = strPath.left(strPath.indexOf("/Contents/MacOS"));
    //qDebug() << strPath;
    strPath = "open -n "+strPath;
    system(strPath.toStdString().c_str());
    qApp->quit();
#endif
}

#ifndef Q_OS_WIN

void OPMainManagerShareLib::slotSendScreenShotPic()
{
    if(chatManager)
    {
        QString imageID = QString::number(QDateTime::currentDateTime().toTime_t());
        QString strPath = gSettingsManager->getUserPath() + "/Clipboard/" + imageID + ".png";
        QDir fileDir = QFileInfo(strPath).absoluteDir();
        QString strFileDir = QFileInfo(strPath).absolutePath();
        if (!fileDir.exists())
            fileDir.mkpath(strFileDir);

        QClipboard *board = QApplication::clipboard();
        const QMimeData *mimeData = board->mimeData();
        if (mimeData->hasImage())
        {
            QImage image = qvariant_cast<QImage>(mimeData->imageData());
            image.save(strPath, "PNG");
            chatManager->InsertTextEditPic(strPath);
        }
    }
}

QString OPMainManagerShareLib::GetNoticeMsgType(MessageInfo messageinfo)
{
    if (messageinfo.MessageChildType == MessageType::Message_PIC) //1图片3视频5文件。
        messageinfo.strMsg = tr("[image]");
    if (messageinfo.MessageChildType == MessageType::Message_AUDIO)
        messageinfo.strMsg = tr("[audio]");
    if (messageinfo.MessageChildType == MessageType::Message_VEDIO) //1图片3视频5文件。
        messageinfo.strMsg = tr("[video]");
    if (messageinfo.MessageChildType == MessageType::Message_FILE) //1图片3视频5文件。
        messageinfo.strMsg = tr("[file]");
    if (messageinfo.MessageChildType == MessageType::Message_AD)
        messageinfo.strMsg = tr("This message type is not supported at this time");
    if (messageinfo.MessageChildType == MessageType::Message_TRANSFER)
        messageinfo.strMsg = tr("[transfer]");
    if (messageinfo.MessageChildType == MessageType::Message_SECRETLETTER)
        messageinfo.strMsg = tr("[secret letter]");
    if (messageinfo.MessageChildType == MessageType::Message_SECRETIMAGE)
        messageinfo.strMsg = tr("[secret image]");
    if (messageinfo.MessageChildType == MessageType::Message_SECRETFILE)
        messageinfo.strMsg = tr("[secret file]");
    if (messageinfo.MessageChildType == MessageType::Message_NOTICE)  //通告消息
    {
        QJsonDocument doc = QJsonDocument::fromJson(messageinfo.strMsg.toUtf8());
        QVariantMap map = doc.toVariant().toMap();
        QString webTitle = map.value("webTitle").toString();
        messageinfo.strMsg = tr("[notice]") + webTitle;
    }
    if (messageinfo.MessageChildType == MessageType::Message_LOCATION)  //位置消息。
        messageinfo.strMsg = tr("[location]");
    if (messageinfo.MessageChildType == MessageType::Message_GW)
        messageinfo.strMsg = tr("[gw]");
    if (messageinfo.MessageChildType == MessageType::Message_NOTIFY)
        messageinfo.strMsg = tr("");

    return messageinfo.strMsg;
}

#ifdef Q_OS_MAC

//个人消息通知
void OPMainManagerShareLib::MessageNoticePer(MessageInfo messageinfo,BuddyInfo buddy)
{
    //消息通知
    if(mainWidget)
    {
        QString strNick = buddy.strNote;
        if (buddy.strNote.isEmpty())
            strNick = buddy.strNickName;
        QString strMsg = GetNoticeMsgType(messageinfo);
        if(!strMsg.isEmpty())
            mainWidget->NotifyNotice(strNick,QString::number(buddy.nUserId),strMsg);
    }
}

//部落消息通知
void OPMainManagerShareLib::MessageNoticeGroup(MessageInfo messageinfo,GroupInfo groupinfo)
{
    if(mainWidget)
    {
        QString strNick = groupinfo.groupName;
        BuddyInfo buddyinfo = gDataBaseOpera->DBGetGroupBuddyInfoWithGroupBuddyID(groupinfo.groupId,QString::number(messageinfo.nFromUserID));
        QString strName = buddyinfo.strNote;
        if (buddyinfo.strNote.isEmpty())
            strName = buddyinfo.strNickName;
        QString strMsg = strName + ":" + GetNoticeMsgType(messageinfo);
        mainWidget->NotifyNotice(strNick,"@"+groupinfo.groupId,strMsg);
    }
}

void OPMainManagerShareLib::slotNotifyReplied(QString userID, QString reply)
{
    if(userID.contains("@"))
    {
        if(chatManager)
        {
            chatManager->slotQuickReply(OpenGroup,userID.remove("@"),reply);
        }
    }
    else
    {
        if(chatManager)
        {
            chatManager->slotQuickReply(OpenPer,userID.remove("@"),reply);
        }
    }
}
#endif
#endif




