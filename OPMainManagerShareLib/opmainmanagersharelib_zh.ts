<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>OPMainManagerShareLib</name>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="196"/>
        <source>Screen capture hotkey conflict!</source>
        <translation>截屏热键冲突！</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="200"/>
        <source>Quickly open hotkey conflicts!</source>
        <translation>快捷打开热键冲突！</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="212"/>
        <source>Notice</source>
        <translation>提示</translation>
    </message>
    <message>
        <source>OpenPlanet  </source>
        <translation type="vanished">星际通讯</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="264"/>
        <source>Telecomm  </source>
        <translation></translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="839"/>
        <source>:/GroupChat/Resources/groupchat/group.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1015"/>
        <location filename="opmainmanagersharelib.cpp" line="1065"/>
        <source>Clear chat history: </source>
        <translation>清空聊天记录: </translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1206"/>
        <source>[image]</source>
        <translation>[图片]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1208"/>
        <source>[audio]</source>
        <translation>[音频]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1210"/>
        <source>[video]</source>
        <translation>[视频]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1212"/>
        <source>[file]</source>
        <translation>[文件]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1214"/>
        <source>This message type is not supported at this time</source>
        <translation>该消息类型暂不支持</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1216"/>
        <source>[transfer]</source>
        <translation>[转账]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1218"/>
        <source>[secret letter]</source>
        <translation>[密信]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1220"/>
        <source>[secret image]</source>
        <translation>[密图]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1222"/>
        <source>[secret file]</source>
        <translation>[密件]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1228"/>
        <source>[notice]</source>
        <translation>[通告]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1231"/>
        <source>[location]</source>
        <translation>[位置]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1233"/>
        <source>[gw]</source>
        <translation>[引力波]</translation>
    </message>
</context>
<context>
    <name>profilemanager</name>
    <message>
        <location filename="profilemanager.cpp" line="270"/>
        <location filename="profilemanager.cpp" line="318"/>
        <location filename="profilemanager.cpp" line="328"/>
        <source>Warning</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="profilemanager.cpp" line="270"/>
        <location filename="profilemanager.cpp" line="318"/>
        <location filename="profilemanager.cpp" line="328"/>
        <source>Failed to get business card information!</source>
        <translation>获取名片信息失败！</translation>
    </message>
</context>
</TS>
