<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hy_AM">
<context>
    <name>OPMainManagerShareLib</name>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="196"/>
        <source>Screen capture hotkey conflict!</source>
        <translation>Էկրանի գրավումը Դյուրանցման բանալին հակամարտությունը!</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="200"/>
        <source>Quickly open hotkey conflicts!</source>
        <translation>Արագ բացեք Դյուրանցման բանալին հակամարտությունը!</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="212"/>
        <source>Notice</source>
        <translation>Ծանուցում</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="264"/>
        <source>Telecomm  </source>
        <translation></translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="839"/>
        <source>:/GroupChat/Resources/groupchat/group.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1015"/>
        <location filename="opmainmanagersharelib.cpp" line="1065"/>
        <source>Clear chat history: </source>
        <translation>Զրուցարանի պատմությունը մաքրելը: </translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1206"/>
        <source>[image]</source>
        <translation>[նկարը]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1208"/>
        <source>[audio]</source>
        <translation>[աուդիո]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1210"/>
        <source>[video]</source>
        <translation>[տեսանյութ]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1212"/>
        <source>[file]</source>
        <translation>[ֆայլը]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1214"/>
        <source>This message type is not supported at this time</source>
        <translation>Այս հաղորդագրության տեսակը չի աջակցվում այս պահին</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1216"/>
        <source>[transfer]</source>
        <translation>[փոխանցում]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1218"/>
        <source>[secret letter]</source>
        <translation>[Գաղտնի նամակ]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1220"/>
        <source>[secret image]</source>
        <translation>[Գաղտնի նկար]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1222"/>
        <source>[secret file]</source>
        <translation>[Գաղտնի ֆայլը]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1228"/>
        <source>[notice]</source>
        <translation>[ծանուցում]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1231"/>
        <source>[location]</source>
        <translation>[Դիրքը]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1233"/>
        <source>[gw]</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>profilemanager</name>
    <message>
        <location filename="profilemanager.cpp" line="270"/>
        <location filename="profilemanager.cpp" line="318"/>
        <location filename="profilemanager.cpp" line="328"/>
        <source>Warning</source>
        <translation>Ուշադրություն</translation>
    </message>
    <message>
        <location filename="profilemanager.cpp" line="270"/>
        <location filename="profilemanager.cpp" line="318"/>
        <location filename="profilemanager.cpp" line="328"/>
        <source>Failed to get business card information!</source>
        <translation>Չհաջողվեց ստանալ այցեքարտի տեղեկություններ!</translation>
    </message>
</context>
</TS>
