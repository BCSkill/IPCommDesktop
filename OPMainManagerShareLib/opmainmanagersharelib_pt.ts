<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>OPMainManagerShareLib</name>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="196"/>
        <source>Screen capture hotkey conflict!</source>
        <translation>Conflito de atalhos de captura de tela!</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="200"/>
        <source>Quickly open hotkey conflicts!</source>
        <translation>Rapidamente abrir conflitos de teclas de atalho!</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="212"/>
        <source>Notice</source>
        <translation>Aviso prévio</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="264"/>
        <source>Telecomm  </source>
        <translation></translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="839"/>
        <source>:/GroupChat/Resources/groupchat/group.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1015"/>
        <location filename="opmainmanagersharelib.cpp" line="1065"/>
        <source>Clear chat history: </source>
        <translation>Limpar histórico do chat: </translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1206"/>
        <source>[image]</source>
        <translation>[imagem]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1208"/>
        <source>[audio]</source>
        <translation>[áudio]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1210"/>
        <source>[video]</source>
        <translation>[vídeo]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1212"/>
        <source>[file]</source>
        <translation>[Arquivo]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1214"/>
        <source>This message type is not supported at this time</source>
        <translation>Este tipo de mensagem não é suportado no momento</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1216"/>
        <source>[transfer]</source>
        <translation>[transferir]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1218"/>
        <source>[secret letter]</source>
        <translation>[carta secreta]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1220"/>
        <source>[secret image]</source>
        <translation>[imagem secreta]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1222"/>
        <source>[secret file]</source>
        <translation>[arquivo secreto]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1228"/>
        <source>[notice]</source>
        <translation>[aviso prévio]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1231"/>
        <source>[location]</source>
        <translation>[localização]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1233"/>
        <source>[gw]</source>
        <translation>[gw]</translation>
    </message>
</context>
<context>
    <name>profilemanager</name>
    <message>
        <location filename="profilemanager.cpp" line="270"/>
        <location filename="profilemanager.cpp" line="318"/>
        <location filename="profilemanager.cpp" line="328"/>
        <source>Warning</source>
        <translation>Aviso</translation>
    </message>
    <message>
        <location filename="profilemanager.cpp" line="270"/>
        <location filename="profilemanager.cpp" line="318"/>
        <location filename="profilemanager.cpp" line="328"/>
        <source>Failed to get business card information!</source>
        <translation>Falha ao obter informações de cartão de visita!</translation>
    </message>
</context>
</TS>
