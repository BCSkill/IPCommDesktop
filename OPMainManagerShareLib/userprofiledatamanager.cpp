﻿#include "userprofiledatamanager.h"

UserProfileDataManager::UserProfileDataManager(QWidget *parent)
	: QObject(parent)
{
	this->parentWidget = parent;

	avatarWidget = new ChangeAvatarWidget;
	avatarWidget->setUserInfo(gDataManager->getUserInfo());
	infoWidget = new UserInfoWidget;
	infoWidget->setUserInfo(gDataManager->getUserInfo());
}

UserProfileDataManager::~UserProfileDataManager()
{

}

void UserProfileDataManager::slotProfileWidget()
{
	userWidget = new UserProfileWidget();

	connect(userWidget, SIGNAL(sigOpenAvatarWidget()), avatarWidget, SLOT(show()));
	connect(userWidget, SIGNAL(sigOpenUserInfoWidget()), infoWidget, SLOT(show()));
	connect(userWidget, SIGNAL(sigDisableStrangers(bool)), this, SLOT(slotDisableStrangers(bool)));
	connect(avatarWidget, SIGNAL(sigUploadHeaderImage(QString)), this, SLOT(slotUploadHeaderImage(QString)));
	connect(infoWidget, SIGNAL(sigUserInfo(UserInfo)), this, SLOT(slotUserInfo(UserInfo)));

	QPoint pos = parentWidget->mapToGlobal(parentWidget->pos());
	QRect deskRt = QApplication::desktop()->availableGeometry(pos);
	userWidget->move(deskRt.x() + (deskRt.width() - userWidget->width()) / 2, deskRt.y() + (deskRt.height() - userWidget->height()) / 2);
	userWidget->show();
	
	userWidget->setUserInfo(gDataManager->getUserInfo());
}

void UserProfileDataManager::slotUploadHeaderImage(QString filePath)
{
	headerImage.load(filePath);

	HttpNetWork::HttpUpLoadFile *upload = new HttpNetWork::HttpUpLoadFile;
	connect(upload, SIGNAL(sigUpLoadFinished(bool, QByteArray)), this, SLOT(slotHttpChangeAvatar(bool, QByteArray)));
	QVariantMap pargram;
	pargram.insert("parentId", "66662");
	pargram.insert("createUser", "6662");
	upload->StartUpLoadFile(gDataManager->getAppConfigInfo().PanServerUploadURL, filePath, pargram);
}

void UserProfileDataManager::slotHttpChangeAvatar(bool success, QByteArray byte)
{
	if (success)
	{
		QVariantMap map = QJsonDocument::fromJson(byte).toVariant().toMap();
		QString fileid = map["fileid"].toString();

		//上传成功了，通知服务器登录用户的头像已经改变。
		QString avatarURL = gDataManager->getAppConfigInfo().PanServerDownloadURL + fileid + "/download";
		UserInfo userInfo = gDataManager->getUserInfo();
		QString url = gDataManager->getAppConfigInfo().MessageServerAddress +
			QString("/IMServer/user/updateUserByUserId?userId=%1&passWord=%2&avatar=%3").arg(userInfo.nUserID).arg(userInfo.strUserPWD).arg(avatarURL);

		HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
		connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotAvatarChanged(bool, QString)));
		http->getHttpRequest(url);
	}
}

void UserProfileDataManager::slotAvatarChanged(bool success, QString byte)
{
	avatarWidget->upPicFileFinished(success);
	if (success)
	{
		userWidget->setHeaderImage(headerImage);
	}
}

void UserProfileDataManager::slotUserInfo(UserInfo userInfo)
{
	UserInfo user = gDataManager->getUserInfo();
	user.strUserNickName = userInfo.strUserNickName;
	user.strSign = userInfo.strSign;
	user.strSex = userInfo.strSex;
	user.strPhone = userInfo.strPhone;
	user.strEmil = userInfo.strEmil;
	user.disableStrangers = userInfo.disableStrangers;

	userWidget->setUserInfo(user);

	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
	QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/user/updateUserByUserId"
		+ QString("?userId=%1&passWord=%2&nickName=%3&sex=%4&mobilePhone=%5&email=%6&sign=%7&disableStrangers=%8")
		.arg(user.nUserID).arg(user.strUserPWD).arg(user.strUserNickName).arg(user.strSex)
		.arg(user.strPhone).arg(user.strEmil).arg(user.strSign).arg(user.disableStrangers);
	
	http->getHttpRequest(url);
}

void UserProfileDataManager::slotDisableStrangers(bool strangers)
{
	int disableStrangers;

	if (strangers)
		disableStrangers = 0;
	else
	{
		disableStrangers = 1;
	}

	UserInfo user = gDataManager->getUserInfo();

	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
	QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/user/updateUserDisableStrangers"
		+ QString("?userId=%1&passWord=%2&disableStrangers=%3")
		.arg(user.nUserID).arg(user.strUserPWD).arg(disableStrangers);

	http->getHttpRequest(url);
}
