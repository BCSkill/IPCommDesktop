<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>OPMainManagerShareLib</name>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="196"/>
        <source>Screen capture hotkey conflict!</source>
        <translation>Захват экрана конфликт горячих клавиш!</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="200"/>
        <source>Quickly open hotkey conflicts!</source>
        <translation>Быстро открывать конфликты горячих клавиш!</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="212"/>
        <source>Notice</source>
        <translation>уведомление</translation>
    </message>
    <message>
        <source>OpenPlanet  </source>
        <translation type="vanished">Открытая планета</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="264"/>
        <source>Telecomm  </source>
        <translation></translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="839"/>
        <source>:/GroupChat/Resources/groupchat/group.png</source>
        <translation></translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1015"/>
        <location filename="opmainmanagersharelib.cpp" line="1065"/>
        <source>Clear chat history: </source>
        <translation>Удалить историю сообщений:</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1206"/>
        <source>[image]</source>
        <translation>[образ]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1208"/>
        <source>[audio]</source>
        <translation>[Аудио]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1210"/>
        <source>[video]</source>
        <translation>[видео]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1212"/>
        <source>[file]</source>
        <translation>[файл]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1214"/>
        <source>This message type is not supported at this time</source>
        <translation>Этот тип сообщения не поддерживается в настоящее время</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1216"/>
        <source>[transfer]</source>
        <translation>[перечислить]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1218"/>
        <source>[secret letter]</source>
        <translation>[секретное письмо]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1220"/>
        <source>[secret image]</source>
        <translation>[секретное изображение]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1222"/>
        <source>[secret file]</source>
        <translation>[секретный файл]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1228"/>
        <source>[notice]</source>
        <translation>[Информация]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1231"/>
        <source>[location]</source>
        <translation>[место нахождения]</translation>
    </message>
    <message>
        <location filename="opmainmanagersharelib.cpp" line="1233"/>
        <source>[gw]</source>
        <translation>[Gw]</translation>
    </message>
</context>
<context>
    <name>profilemanager</name>
    <message>
        <location filename="profilemanager.cpp" line="270"/>
        <location filename="profilemanager.cpp" line="318"/>
        <location filename="profilemanager.cpp" line="328"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="profilemanager.cpp" line="270"/>
        <location filename="profilemanager.cpp" line="318"/>
        <location filename="profilemanager.cpp" line="328"/>
        <source>Failed to get business card information!</source>
        <translation>Не удалось получить информацию о визитной карточке!</translation>
    </message>
</context>
</TS>
