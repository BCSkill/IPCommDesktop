﻿#include "imdatamanagersharelib.h"

IMDataManagerShareLib::IMDataManagerShareLib()
{
#ifdef Q_OS_WIN
	this->currentChatWidget = NULL;
#endif
	mUserInfo.nUserID = -1;
}

IMDataManagerShareLib::~IMDataManagerShareLib()
{

}

void IMDataManagerShareLib::setUserInfo(UserInfo userInfo)
{ 
	mUserInfo = userInfo; 
}

void IMDataManagerShareLib::updateUserInfo(UserInfo userInfo)
{
	mUserInfo.strUserAvatarHttp = userInfo.strUserAvatarHttp;
	mUserInfo.strNote = userInfo.strNote;        //用户
	mUserInfo.strSex = userInfo.strSex;        //用户性别
	mUserInfo.strEmil = userInfo.strEmil;       //邮箱
	mUserInfo.strPhone = userInfo.strPhone;     //电话
	mUserInfo.strSign = userInfo.strSign;      //签名
	mUserInfo.strUserNickName = userInfo.strUserNickName;
	mUserInfo.strUserName = userInfo.strUserNickName;
}

#ifdef Q_OS_WIN
void IMDataManagerShareLib::setCurrentChatWidget(WId id)
{
	this->currentChatWidget = id;
}
HWND IMDataManagerShareLib::getCurrentChatWidget()
{
	return (HWND)this->currentChatWidget;
}
#endif
