# ----------------------------------------------------
# This file is generated by the Qt Visual Studio Tools.
# ------------------------------------------------------

# This is a reminder that you are using a generated .pro file.
# Remove it when you are finished editing this file.
message("You are running qmake on a generated .pro file. This may not work!")


TEMPLATE = subdirs
SUBDIRS += BaseUIShareLib/BaseUIShareLib.pro \
    LoginDatabaseOperaShareLib/LoginDatabaseOperaShareLib.pro \
    HttpNetWorkShareLib/HttpNetWorkShareLib.pro \
    ContactsWidgetShareLib/ContactsWidgetShareLib.pro \
    ChatWidgetShareLib/ChatWidgetShareLib.pro \
    IMDataManagerShareLib/IMDataManagerShareLib.pro \
    UpdateShareLib/UpdateShareLib.pro \
    eWalletLib/eWalletLib.pro \
    ScanQRLoginShareLib/ScanQRLoginShareLib.pro \
    CreateAddWidgetShareLib/CreateAddWidgetShareLib.pro \
    OpenPlanet/OpenPlanet.pro \
    OPObjectManager/OPObjectManager.pro \
    OPWindowsManagerShareLib/OPWindowsManagerShareLib.pro \
    OPMainManagerShareLib/OPMainManagerShareLib.pro \
    OPMainWidgetShareLib/OPMainWidgetShareLib.pro \
    OPRecoveryWalletShareLib/OPRecoveryWalletShareLib.pro \
    OPDatebaseShareLib/OPDatebaseShareLib.pro \
    OPDataManager/OPDataManager.pro \
    OPRequestShareLib/OPRequestShareLib.pro \
    ContactsProfileShareLib/ContactsProfileShareLib.pro \
    ReadAppConfig/ReadAppConfig.pro \
    IMSocketNetWorkShareLib/IMSocketNetWorkShareLib.pro \
    IMSocketDataBaseShareLib/IMSocketDataBaseShareLib.pro \
    VideoPlayShareLib_mac/VideoPlayShareLib.pro \
    AlphabeticalSortSharedLib/AlphabeticalSortSharedLib.pro \
    IMDataBaseOperaInfo/IMDataBaseOperaInfo.pro \
    IMDownLoadHeaderImg/IMDownLoadHeaderImg.pro \
    IMRequestBuddyInfo/IMRequestBuddyInfo.pro \
    SqlLiteShareLib/SqlLiteShareLib.pro \
    SocketNetWorkShareLib/SocketNetWorkShareLib.pro \
    QRenCodeShareLib/QRenCodeShareLib.pro \
    ScreenCutShareLib_mac/ScreenCutShareLib.pro \
    WebObjectShareLib \
    MacUpdate \
    QxtGlobalShortCut_mac/QxtGlobalShortCut.pro \
    MacNotification \
    SettingsManagerShareLib/settingsmanager.pro

