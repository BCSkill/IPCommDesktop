<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>updateWidget</name>
    <message>
        <location filename="updatewidget.ui" line="19"/>
        <source>Update</source>
        <translation>Actualizar</translation>
    </message>
    <message>
        <location filename="updatewidget.ui" line="52"/>
        <source>Update note</source>
        <translation>Nota de actualización</translation>
    </message>
    <message>
        <location filename="updatewidget.ui" line="143"/>
        <source>Download</source>
        <translation>Descargar</translation>
    </message>
    <message>
        <location filename="updatewidget.ui" line="176"/>
        <source>cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="updatewidget.cpp" line="44"/>
        <source>Update content:</source>
        <translation>Actualizar contenido:</translation>
    </message>
</context>
</TS>
