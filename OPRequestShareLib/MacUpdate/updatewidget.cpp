#include "updatewidget.h"
#include "ui_updatewidget.h"

updateWidget::updateWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::updateWidget)
{
    ui->setupUi(this);

    OnInitUi();

    connect(ui->updateButton,SIGNAL(clicked()),this,SLOT(slotUpdate()));
    connect(ui->cancelButton,SIGNAL(clicked()),this,SLOT(slotExit()));
    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(slotExit()));
}

updateWidget::~updateWidget()
{
    delete ui;
}

void updateWidget::OnInitUi()
{

    setWindowFlags(Qt::FramelessWindowHint);
    ui->mLabelBK->setStyleSheet("background-color:#3692ef;");

    ui->pushButton->setStyleSheet("QPushButton#pushButton { \
            border-image:url(\":/update/Resources/update/sysbtn_close_white_normal.png\") 0 0 0 0 stretch stretch; \
            }   \
                    QPushButton#pushButton:hover  \
                    {\
                        border-image:url(\":/update/Resources/update/sysbtn_close_white_hover.png\") 0 0 0 0 stretch stretch;\
                    }\
                    QPushButton#pushButton:pressed \
                    { \
                        border-image:url(\":/update/Resources/update/sysbtn_close_white_down.png\") 0 0 0 0 stretch stretch; \
                    }");

     ui->labelOnline->setStyleSheet("color:white;");

     ui->labelLogo->setStyleSheet("border-image:url(:/update/Resources/update/OpenPlanet.png) 0 0 0 0 ;");

     ui->labelUpdateContent->setText(tr("Update content:"));
     ui->labelUpdateContent->setStyleSheet("color:#ffa405;font: 87 14pt 微软雅黑;");

     ui->labelContent->setStyleSheet("color:white;font: 75 12pt 微软雅黑;");
}

void updateWidget::SetUpdateDescribeInfo(QString strDescribeInfo)
{
    ui->labelContent->setText(strDescribeInfo);
    ui->labelContent->setWordWrap(true);
    ui->labelContent->setAlignment(Qt::AlignTop);
}

void updateWidget::slotUpdate()
{
    emit sigUpdate();
}

void updateWidget::slotExit()
{
    emit sigExit();
    this->close();
}

void updateWidget::mousePressEvent(QMouseEvent *event)
{
    mMoveing = true;
    mMovePosition = event->globalPos() - pos();
    return QWidget::mousePressEvent(event);
}

void updateWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (mMoveing && (event->buttons() && Qt::LeftButton)
        && (event->globalPos() - mMovePosition).manhattanLength() > QApplication::startDragDistance())
    {
        move(event->globalPos() - mMovePosition);
        mMovePosition = event->globalPos() - pos();
    }
    return QWidget::mouseMoveEvent(event);
}

void updateWidget::mouseReleaseEvent(QMouseEvent *event)
{
    mMoveing = false;
    return QWidget::mouseReleaseEvent(event);
}

