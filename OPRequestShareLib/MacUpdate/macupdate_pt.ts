<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>updateWidget</name>
    <message>
        <location filename="updatewidget.ui" line="19"/>
        <source>Update</source>
        <translation>Atualizar</translation>
    </message>
    <message>
        <location filename="updatewidget.ui" line="52"/>
        <source>Update note</source>
        <translation>Atualizar nota</translation>
    </message>
    <message>
        <location filename="updatewidget.ui" line="143"/>
        <source>Download</source>
        <translation>Baixar</translation>
    </message>
    <message>
        <location filename="updatewidget.ui" line="176"/>
        <source>cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="updatewidget.cpp" line="44"/>
        <source>Update content:</source>
        <translation>Atualizar conteúdo:</translation>
    </message>
</context>
</TS>
