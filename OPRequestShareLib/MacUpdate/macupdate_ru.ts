<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>updateWidget</name>
    <message>
        <location filename="updatewidget.ui" line="19"/>
        <source>Update</source>
        <translation>Թարմացնել</translation>
    </message>
    <message>
        <location filename="updatewidget.ui" line="52"/>
        <source>Update note</source>
        <translation>Թարմացրեք</translation>
    </message>
    <message>
        <location filename="updatewidget.ui" line="143"/>
        <source>Download</source>
        <translation>Բեռնել</translation>
    </message>
    <message>
        <location filename="updatewidget.ui" line="176"/>
        <source>cancel</source>
        <translation>չեղարկել</translation>
    </message>
    <message>
        <location filename="updatewidget.cpp" line="44"/>
        <source>Update content:</source>
        <translation>Թարմացնել բովանդակությունը：</translation>
    </message>
</context>
</TS>
