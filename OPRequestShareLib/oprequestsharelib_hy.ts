<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hy_AM">
<context>
    <name>OPRequestShareLib</name>
    <message>
        <location filename="oprequestsharelib.cpp" line="19"/>
        <source>config</source>
        <translation></translation>
    </message>
    <message>
        <location filename="oprequestsharelib.cpp" line="231"/>
        <source>Network connection failed!</source>
        <translation>Ցանցային կապը ձախողվեց！</translation>
    </message>
    <message>
        <location filename="oprequestsharelib.cpp" line="244"/>
        <source>Server returned error！</source>
        <translation>Սերվերը վերադարձրեց սխալը!</translation>
    </message>
    <message>
        <location filename="oprequestsharelib.cpp" line="1284"/>
        <source>random red packet</source>
        <translation>պատահական նվեր գումար</translation>
    </message>
    <message>
        <location filename="oprequestsharelib.cpp" line="1284"/>
        <source>identical red packet</source>
        <translation>նույնական նվեր գումար</translation>
    </message>
</context>
</TS>
