<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="th_TH">
<context>
    <name>OPRequestShareLib</name>
    <message>
        <location filename="oprequestsharelib.cpp" line="19"/>
        <source>config</source>
        <translation>การเชื่อมต่อเครือข่ายล้มเหลว!</translation>
    </message>
    <message>
        <location filename="oprequestsharelib.cpp" line="231"/>
        <source>Network connection failed!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="oprequestsharelib.cpp" line="244"/>
        <source>Server returned error！</source>
        <translation>เซิร์ฟเวอร์ส่งคืนข้อผิดพลาด!</translation>
    </message>
    <message>
        <location filename="oprequestsharelib.cpp" line="1284"/>
        <source>random red packet</source>
        <translation>แพ็คเก็ตสีแดงแบบสุ่ม</translation>
    </message>
    <message>
        <location filename="oprequestsharelib.cpp" line="1284"/>
        <source>identical red packet</source>
        <translation>แพ็คเก็ตสีแดงเหมือนกัน</translation>
    </message>
</context>
</TS>
