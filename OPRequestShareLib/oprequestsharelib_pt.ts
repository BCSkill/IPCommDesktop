<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>OPRequestShareLib</name>
    <message>
        <location filename="oprequestsharelib.cpp" line="19"/>
        <source>config</source>
        <translation></translation>
    </message>
    <message>
        <location filename="oprequestsharelib.cpp" line="231"/>
        <source>Network connection failed!</source>
        <translation>Conexão de rede falhou!</translation>
    </message>
    <message>
        <location filename="oprequestsharelib.cpp" line="244"/>
        <source>Server returned error！</source>
        <translation>Servidor retornou erro！</translation>
    </message>
    <message>
        <location filename="oprequestsharelib.cpp" line="1284"/>
        <source>random red packet</source>
        <translation>pacote vermelho aleatório</translation>
    </message>
    <message>
        <location filename="oprequestsharelib.cpp" line="1284"/>
        <source>identical red packet</source>
        <translation>pacote vermelho idêntico</translation>
    </message>
</context>
</TS>
