<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>ScreenControlWidget</name>
    <message>
        <location filename="screencontrolwidget.ui" line="35"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="80"/>
        <source>Rectangle</source>
        <translation>Rectángulo</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="147"/>
        <source>Circle</source>
        <translation>Circulo</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="213"/>
        <source>Arrow</source>
        <translation>Flecha</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="279"/>
        <source>Brush</source>
        <translation>Cepillo</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="336"/>
        <source>Mosaic</source>
        <translation>Mosaico</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="391"/>
        <source>Text</source>
        <translation>Texto</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="496"/>
        <source>Revert</source>
        <translation>Revertir</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="546"/>
        <source>Save</source>
        <translation>Salvar</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="636"/>
        <source>Exit</source>
        <translation>Salida</translation>
    </message>
    <message>
        <location filename="screencontrolwidget.ui" line="696"/>
        <source>Complete</source>
        <translation>Completar</translation>
    </message>
</context>
<context>
    <name>ScreenPenSettingWidget</name>
    <message>
        <location filename="screenpensettingwidget.ui" line="17"/>
        <source>ScreenPenSettingWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="screenpensettingwidget.ui" line="976"/>
        <source>Degree</source>
        <translation>Grado</translation>
    </message>
</context>
</TS>
