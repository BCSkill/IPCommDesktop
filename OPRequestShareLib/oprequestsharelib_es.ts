<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>OPRequestShareLib</name>
    <message>
        <location filename="oprequestsharelib.cpp" line="19"/>
        <source>config</source>
        <translation></translation>
    </message>
    <message>
        <location filename="oprequestsharelib.cpp" line="231"/>
        <source>Network connection failed!</source>
        <translation>Conexión de red fallida!</translation>
    </message>
    <message>
        <location filename="oprequestsharelib.cpp" line="244"/>
        <source>Server returned error！</source>
        <translation>El servidor devolvió el error！</translation>
    </message>
    <message>
        <location filename="oprequestsharelib.cpp" line="1284"/>
        <source>random red packet</source>
        <translation>paquete rojo al azar</translation>
    </message>
    <message>
        <location filename="oprequestsharelib.cpp" line="1284"/>
        <source>identical red packet</source>
        <translation>paquete rojo idéntico</translation>
    </message>
</context>
</TS>
