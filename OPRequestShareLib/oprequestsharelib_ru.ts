<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>OPRequestShareLib</name>
    <message>
        <location filename="oprequestsharelib.cpp" line="19"/>
        <source>config</source>
        <translation></translation>
    </message>
    <message>
        <location filename="oprequestsharelib.cpp" line="231"/>
        <source>Network connection failed!</source>
        <translation>Не удалось подключиться к сети!</translation>
    </message>
    <message>
        <location filename="oprequestsharelib.cpp" line="244"/>
        <source>Server returned error！</source>
        <translation>Сервер вернул ошибку！</translation>
    </message>
    <message>
        <location filename="oprequestsharelib.cpp" line="1284"/>
        <source>random red packet</source>
        <translation>случайный красный пакет</translation>
    </message>
    <message>
        <location filename="oprequestsharelib.cpp" line="1284"/>
        <source>identical red packet</source>
        <translation>идентичный красный пакет</translation>
    </message>
</context>
</TS>
