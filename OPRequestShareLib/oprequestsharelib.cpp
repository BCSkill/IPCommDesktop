﻿#include "oprequestsharelib.h"
#include "QStringLiteralBak.h"
//#include "messagebox.h"
extern OPDataManager  *gOPDataManager;

OPRequestShareLib::OPRequestShareLib()
{
	initConfig();
	openServer = "http://chaintest.openserver.cn";  //暂时使用这个。正式服务器部署后需替换。
}

OPRequestShareLib::~OPRequestShareLib()
{

}

void OPRequestShareLib::initConfig()
{
	QDomDocument doc(tr("config"));
	QFile file("./config/walletConfig.xml");
	if (file.open(QIODevice::ReadOnly))
	{
		if (doc.setContent(&file))
		{
			QDomElement docElem = doc.documentElement();
			QDomNode n = docElem.firstChild();
			while (!n.isNull())
			{
				QDomElement e = n.toElement();
				if (!e.isNull())
				{
					if (e.tagName() == "tcserver")
					{
						tcserver = e.text();
					}
					if (e.tagName() == "inviteUrl")
					{
						inviteUrl = e.text();
					}
				}

				n = n.nextSibling();
			}
		}
		file.close();
	}
}

void OPRequestShareLib::getIntegral(QString token)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotIntegralResult(bool, QString)));
	QString strRequest = tcserver + "/integral/integralTotal?access_token=" + token;
	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotHostingRecords(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		QVariantList trusteeshipTransactionLists = map.value("trusteeshipTransactionLists").toList();

		QList<RecordInfo> recordsList;
		foreach (QVariant var, trusteeshipTransactionLists)
		{
			QVariantMap data = var.toMap();
			RecordInfo record;
			record.value = QString::number(data.value("coin").toDouble(), 'f', 5);
			record.age = data.value("timestamp").toString();
			record.type = data.value("type").toInt();

			recordsList.append(record);
		}

		emit sigRecords(recordsList);
	}
}

void OPRequestShareLib::slotHostingCoinResult(bool success, QString result)
{
	if (success)
	{
		if (result.contains("success"))
		{
			emit sigTransFinished(true);
			return;
		}
	}

	emit sigTransFinished(false);
}

void OPRequestShareLib::slotIntegralResult(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();

		if (map.value("msg").toString() == "invalid_token")
		{
			QString strRequest = tcserver + "/oauth/accessToken";
			QByteArray append = "grant_type=refresh_token";
			append += "&client_id=a6f23fbb-0a1d-4e10-be7e-89181cdf089c";
			append += "&client_secret=2a6a9640-9a46-4622-b226-bc94b852848c";
			append += "&refresh_token=" + gOPDataManager->getRefreshToken();

			HttpNetWork::HttpNetWorkShareLib *request = new HttpNetWork::HttpNetWorkShareLib;
			connect(request, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotRefreshAccessToken(bool, QString)));
			request->postHttpRequest(strRequest, append);
		}
		else
		{
			int integralTotal = map.value("integralTotal").toInt();
			emit sigIntegral(QString::number(integralTotal));
		}
	}
}

void OPRequestShareLib::slotRefreshAccessToken(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();

		QString accessToken = map.value("access_token").toString();
		QString refreshToken = map.value("refresh_token").toString();

		if (!accessToken.isEmpty())
			gOPDataManager->setAccessToken(accessToken);
		if (!refreshToken.isEmpty())
			gOPDataManager->setRefreshToken(refreshToken);
	}
}

void OPRequestShareLib::getBalance(QString address)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotBalanceResult(bool, QString)));
	QString strRequest = tcserver + "/eth/ethGetBalance?ethAddress=" + address;
	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotBalanceResult(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		double num = map.value("balance").toDouble();
		QString balance;
		if (num == 0)
			balance = "0";
		else
		{
		    long double b = (long double)num / (long double)1000000000000000000;
			balance = QString::number((double)b, 'f', 8);
		}

		emit sigBalance(balance);
	}
}

void OPRequestShareLib::getBaseBuddyInfo(QString token)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotBaseBuddyInfo(bool, QString)));
	QString strRequest = tcserver + "/user/friendAddressList?access_token=" + token;
	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotBaseBuddyInfo(bool success, QString result)
{
	if (success)
	{
		emit sigBaseBuddyInfo(result);
	}
}

void OPRequestShareLib::getNonce(QString address)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotGetNonce(bool, QString)));
	QString strRequest = tcserver + "/eth/ethGetTransactionCount?ethAddress=" + address;
	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotGetNonce(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		if (map.value("result").toString() == "success")
		{
			int num = map.value("transactionCount").toInt();

			emit sigNonce(QString::number(num));
			return;
		}
	}

	emit sigNonce("error");
}

void OPRequestShareLib::sendRawTransaction(QString fromID, QString toID, QString fromAddress, QString toAddress, QString value, QString transactionData)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotTransfer(bool, QString)));
	QString strRequest = tcserver + "/eth/ethSendRawTransaction?" +
		QString("fromUserId=%1&toUserId=%2&fromEthAddress=%3&toEthAddress=%4&value=%5&signedTransactionData=%6")
		.arg(fromID).arg(toID).arg(fromAddress).arg(toAddress).arg(value).arg(transactionData);
	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::sendChainTransaction(QString fromID, QString toID, QString fromAddress, QString toAddress, QString value, QString transactionData)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotTransfer(bool, QString)));
	QString strRequest = tcserver + "/chain/ethSendRawTransaction?" +
		QString("fromUserId=%1&toUserId=%2&fromEthAddress=%3&toEthAddress=%4&value=%5&signedTransactionData=%6&type=1&chainID=2")
		.arg(fromID).arg(toID).arg(fromAddress).arg(toAddress).arg(value).arg(transactionData);
	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotTransfer(bool success, QString result)
{
	QString error = tr("Network connection failed!");

	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();

		if (map.value("result").toString() == "success")
		{
			emit sigTransfer(true, QVariant(map));
			return;
		}
		else
			error = tr("Server returned error！");

	}

	emit sigTransfer(false, QVariant(error));
}

QString OPRequestShareLib::getUpdateUrl()
{
#ifdef Q_OS_WIN
	return tcserver + "/app/appUpgrade?appId=winOpenPlanet&deviceType=win";
#else
    return tcserver + "/app/appUpgrade?appId=macOpenPlanet&deviceType=mac";
#endif
}

QString OPRequestShareLib::getInviteUrl()
{
	return inviteUrl;
}

void OPRequestShareLib::getBuddyAddressInfo(QString buddyID)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotBuddyAddressResult(bool, QString)));
	QString strRequest = tcserver + "/user/getUserByIMUserId?otherIMUserId=" + buddyID + "&access_token=" + gOPDataManager->getAccessToken();;
	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotBuddyAddressResult(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();

		if (map.value("result").toString() == "success")
		{
			QVariantMap user = map.value("user").toMap();
			AddressInfo info;
			info.comPublicKey = user.value("comPublicKey").toString();
			info.ethAddress = user.value("ethAddress").toString();
			info.ethPublicKey = user.value("ethPublicKey").toString();
			info.planet = user.value("planet").toString();
			info.userID = QString::number(user.value("192205").toInt());
			emit sigBuddyAddressInfo(info);
		}
	}
}

void OPRequestShareLib::getCommKey(QString userID, QString token)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotCommKey(bool, QString)));
	QString strRequest = tcserver + "/account/getUserSecretKeyByUserId?userId=" + userID + "&?access_token=" + token;
	qDebug() << "request: " << strRequest;
	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotCommKey(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();

		if (map.value("result").toString() == "success")
		{
			QVariantMap SecretKey = map.value("SecretKey").toMap();

			QString publicKey = SecretKey.value("publicKey").toString();
			QString privateKey = SecretKey.value("privateKey").toString();

			emit sigCommKey(publicKey, privateKey);
		}
	}
}

void OPRequestShareLib::getChildrenWallet(QString address)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotChildrenWallet(bool, QString)));
	QString strRequest = tcserver + "/account/getAllWallet?mainAccountAddress=" + address;
	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotChildrenWallet(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		QVariantList walletList = map.value("ChildrenWallet").toList();
		QList<ChildrenWallet> list;
		for (int i = 0; i < walletList.count(); i++)
		{
			ChildrenWallet walletInfo;
			QVariantMap walletMap = walletList.at(i).toMap();

			walletInfo.accountAddress = walletMap.value("accountAddress").toString();
			walletInfo.accountIcon = walletMap.value("accountIcon").toString();
			walletInfo.accountName = walletMap.value("accountName").toString();
			walletInfo.accountPrivateKey = walletMap.value("accountPrivateKey").toString();
			walletInfo.accountPublicKey = walletMap.value("accountPublicKey").toString();
			walletInfo.accountType = walletMap.value("accountType").toInt();
			walletInfo.balance = walletMap.value("balance").toString();
			walletInfo.blockChainName = walletMap.value("blockChainName").toString();
			walletInfo.id = walletMap.value("id").toString();
			walletInfo.mainAccountAddress = walletMap.value("mainAccountAddress").toString();

			walletInfo.eosIsActive = walletMap.value("eosIsActive").toInt();
			walletInfo.eosAccountNames = walletMap.value("eosAccountNames").toString();
			walletInfo.eosActivePrivateKey = walletMap.value("eosActivePrivateKey").toString();
			walletInfo.eosActivePublicKey = walletMap.value("eosActivePublicKey").toString();

			list.append(walletInfo);
		}
		emit sigChildrenWallet(list);
	}
}

void OPRequestShareLib::addChildrenWallet(QString address, QString pubKey, QString priKey, 
	QString mainAddress, QString name, QString icon, QString blockChain, int type, QString seedPhrase)
{
	//发送请求。
	HttpNetWork::HttpNetWorkShareLib *request = new HttpNetWork::HttpNetWorkShareLib;
	connect(request, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotAddChildrenWallet(bool, QString)));
	QString url = QString(tcserver + "/account/addWallet");
	url += QString("?accountAddress=") + address;
	url += QString("&accountPublicKey=") + pubKey;
	url += QString("&accountPrivateKey=") + priKey;
	url += QString("&mainAccountAddress=") + mainAddress;
	url += QString("&accountIcon=") + icon;
	url += QString("&accountName=") + name;
	url += QString("&blockChainName=" + blockChain);
	url += QString("&accountType=") + QString::number(type);
	request->getHttpRequest(url);
}

void OPRequestShareLib::slotAddChildrenWallet(bool success, QString result)
{
	if (success)
	{
		if (result.contains("success"))
		{
			emit sigAddChildrenWallet(true);
			return;
		}
	}

	emit sigAddChildrenWallet(false);
}

void OPRequestShareLib::getChildrenBalacne(QString chainID, QString address)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotChildrenBalance(bool, QString)));
	QString strRequest = tcserver + "/chain/getAllTokenBalanceByChainAndUserAddress?chainID=" + chainID;
	if (!address.startsWith("0x"))
		address = "0x" + address;
	strRequest += "&userAddress=" + address;
	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotChildrenBalance(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();
		if (map.value("result").toString() == "success")
		{
			emit sigChildrenBalance(map.value("AddressBalance").toString());
			return;
		}
	}

	emit sigChildrenBalance("failed");
}

void OPRequestShareLib::getFollowToken(QString chainID, QString userAdress)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotFollowToken(bool, QString)));
	QString strRequest = tcserver + "/chain/getAllTokenBalanceByChainAndUserAddress?chainID=" + chainID;
	strRequest += "&userAddress=" + userAdress;
	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotFollowToken(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();

		QVariantList varList = map.value("AllTokenBalance").toList();
		QList<TokenInfo> tokenList;
		TokenInfo mainToken;
		mainToken.tokenIcon = ":/ewallet/Resources/ewallet/eth.png";
		mainToken.tokenName = "ETH";
		mainToken.balance = map.value("AddressBalance").toString();
		tokenList.append(mainToken);

		for (int i = 0; i < varList.count(); i++)
		{
			QVariantMap tokenVar = varList.at(i).toMap();
			TokenInfo token;
			token.balance = tokenVar.value("balance").toString();
			token.blockChainID = tokenVar.value("blockChainID").toString();
			token.tokenAddress = tokenVar.value("tokenAddress").toString();
			token.tokenDecimals = tokenVar.value("tokenDecimals").toInt();
			token.tokenFullName = tokenVar.value("tokenFullName").toString();
			token.tokenIcon = tokenVar.value("tokenIcon").toString();
			token.tokenID = tokenVar.value("tokenID").toString();
			token.tokenName = tokenVar.value("tokenName").toString();
			tokenList.append(token);
		}

		emit sigFollowToken(tokenList);
	}
}

void OPRequestShareLib::getAllToken(QString chainID, QString userAdress)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotAllToken(bool, QString)));
	QString strRequest = tcserver + "/chain/getAllTokenByChain?chainID=" + chainID;
	strRequest += "&userAddress=" + userAdress;
	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotAllToken(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();

		QVariantList varList = map.value("AllTokenByChain").toList();
		QList<TokenInfo> tokenList;
		TokenInfo mainToken;
		mainToken.tokenIcon = ":/ewallet/Resources/ewallet/eth.png";
		mainToken.tokenName = "ETH";
		mainToken.tokenFullName = "ETH";
		tokenList.append(mainToken);

		for (int i = 0; i < varList.count(); i++)
		{
			QVariantMap tokenVar = varList.at(i).toMap();
			TokenInfo token;
			token.blockChainID = tokenVar.value("blockChainID").toString();
			token.tokenAddress = tokenVar.value("tokenAddress").toString();
			token.tokenDecimals = tokenVar.value("tokenDecimals").toInt();
			token.tokenFullName = tokenVar.value("tokenFullName").toString();
			token.tokenIcon = tokenVar.value("tokenIcon").toString();
			token.tokenID = tokenVar.value("tokenID").toString();
			token.tokenName = tokenVar.value("tokenName").toString();
			token.ifOwner = tokenVar.value("ifOwner").toInt();
			tokenList.append(token);
		}

		emit sigAllToken(tokenList);
	}
}

void OPRequestShareLib::setFollowToken(QString chainID, QString tokenID, QString userAddress, bool followed)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotSetFollowResult(bool, QString)));
	QString strRequest = tcserver + "/chain/updateUserToken?chainID=" + chainID;
	strRequest += "&tokenID=" + tokenID;
	strRequest += "&userAddress=" + userAddress;
	QString type = followed ? "1" : "0";  //关注为1，取关为0.
	strRequest += "&type=" + type;
	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotSetFollowResult(bool success, QString result)
{
	if (success)
	{
		
	}
}

void OPRequestShareLib::setWalletInfo(QString accountAddress, QString mainAccountAddress, QString accountName, QString accountIcon, QString blockChainName)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotSetWalletInfo(bool, QString)));
	QString strRequest = tcserver + "/account/updateChildrenWallet";
	strRequest += "?accountAddress=" + accountAddress;
	strRequest += "&mainAccountAddress=" + mainAccountAddress;
	strRequest += "&blockChainName=" + blockChainName;
	if (!accountName.isEmpty())
	{
		strRequest += "&accountName=" + accountName;
	}
	if (!accountIcon.isEmpty())
	{
		strRequest += "&accountIcon=" + accountIcon;
	}
	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotSetWalletInfo(bool success, QString result)
{
	if (success)
	{
		
	}
}

void OPRequestShareLib::deleteChildrenWallet(QString accountAddress, QString mainAccountAddress, QString blockChainName)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotDeleteChildrenWallet(bool, QString)));
	QString strRequest = tcserver + "/account/delChildrenWallet";
	strRequest += "?accountAddress=" + accountAddress;
	strRequest += "&mainAccountAddress=" + mainAccountAddress;
	strRequest += "&blockChainName=" + blockChainName;
	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotDeleteChildrenWallet(bool success, QString result)
{
	if (success)
	{
		qDebug() << result;
	}
}

void OPRequestShareLib::getRecords(QString walletAddress, QString chainId, int page, int pagesize, int type, QString tokenID)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotRecords(bool, QString)));
	QString strRequest = tcserver + "/chain/txlist_accounts";
	strRequest += "?walletAddress=" + walletAddress;
	strRequest += "&chainId=" + chainId;
	strRequest += "&page=" + QString::number(page);
	strRequest += "&pagesize=" + QString::number(pagesize);
	strRequest += "&type=" + QString::number(type);
	if (!tokenID.isEmpty())
		strRequest += "&tokenId=" + tokenID;
	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotRecords(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();

		if (map.value("result").toString() == "success")
		{
			QVariantList list = map.value("txlist_accounts").toList();
			QList<RecordInfo> recordsList;
			for (int i = 0; i < list.count(); i++)
			{
				QVariantMap variant = list.at(i).toMap();
				RecordInfo record;
				record.age = variant.value("age").toString();
				record.block = variant.value("block").toInt();
				record.from = variant.value("from").toString();
				record.status = variant.value("status").toString();
				record.to = variant.value("to").toString();
				record.txFee = variant.value("txFee").toString();
				record.txHash = variant.value("txHash").toString();
				record.type = variant.value("type").toInt();
				record.value = variant.value("value").toString();
				record.sendId = variant.value("sendIMUserId").toString();
				record.receiveId = variant.value("receiveIMUserId").toString();

				recordsList.append(record);
			}

			emit sigRecords(recordsList);
		}
	}
}

void OPRequestShareLib::getBtcUtxo(QString address)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotBtcUtxo(bool, QString)));
	QString strRequest = openServer + "/btc/btcGetUtxo";
	strRequest += "?BtcAddress=" + address;

#ifdef QT_NO_DEBUG 
	strRequest += "&type=formal";
#else 
	strRequest += "&type=test";
#endif
	
	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotBtcUtxo(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();

		if (map.value("result") == "success")
		{
			QVariantList list = map.value("data").toList();
			QByteArray bytes = QJsonDocument::fromVariant(list).toJson();

			emit sigBtcUtxo(QString(bytes));
		}
		else
		{
			emit sigBtcUtxo("failed");
		}
	}
	else
	{
		emit sigBtcUtxo("failed");
		//IMessageBox::tip(this, tr("注意"), tr("交易失败！"));
	}
}

void OPRequestShareLib::publicBtcTrans(QString rawhex, QString txhash, QString fromID, QString toID /*= ""*/)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotPublicBtcTrans(bool, QString)));
	QString strRequest = openServer + "/btc/publish";
	strRequest += "?rawhex=" + rawhex;
	strRequest += "&txhash=" + txhash;

#ifdef QT_NO_DEBUG 
	strRequest += "&type=formal";
#else 
	strRequest += "&type=test";
#endif

	strRequest += "&fromUserId=" + fromID;
	if (!toID.isEmpty())
		strRequest += "&toUserId=" + toID;

	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotPublicBtcTrans(bool success, QString result)
{
	if (success)
	{
		if (result.contains("success"))
		{
			emit sigBtcTransResult(true);
		}
		else
		{
			emit sigBtcTransResult(false);
		}
	}
}

void OPRequestShareLib::getBtcBalance(QString address)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotBtcBalance(bool, QString)));
	QString strRequest = openServer + "/btc/btcGetBalance";
	strRequest += "?BtcAddress=" + address;

#ifdef QT_NO_DEBUG 
	strRequest += "&type=formal";
#else 
	strRequest += "&type=test";
#endif

	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotBtcBalance(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();

		if (map.value("result") == "success")
		{
			QVariantList list = map.value("data").toList();
			QVariantMap data = list.first().toMap();
			QString strBalance = data.value("finalBalance").toString();

			if (!strBalance.isEmpty())
			{
				double balance = strBalance.toDouble() / 100000000;
				strBalance = QString::number(balance, 'f', 2);

				emit sigBtcBalance(strBalance);
				return;
			}
		}
	}

	emit sigBtcBalance("failed");
}

void OPRequestShareLib::getBtcTxs(QString address)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotBtcTxs(bool, QString)));
	QString strRequest = openServer + "/btc/getAddressTxs";
	strRequest += "?address=" + address;
	strRequest += "&pagesize=1000&page=1&status=1";

#ifdef QT_NO_DEBUG 
	strRequest += "&type=formal";
#else 
	strRequest += "&type=test";
#endif

	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotBtcTxs(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();

		if (map.value("result").toString() == "success")
		{
			QList<RecordInfo> recordsList;

			//获取json的数据部分，该部分是一个列表，每一条代表一条交易记录。
			QVariantList dataList = map.value("data").toList();
            foreach (QVariant data , dataList)
			{
				//获取到一条交易记录，转换为map。
				QVariantMap itemMap = data.toMap();

				//在交易记录的第一层可以获取除转入转出地之外的所有数据。

				//交易类型 1 全部  2 支出 3收入 4 失败
				int type = itemMap.value("type").toInt();
				QString status;
				if (type == 2)
					status = "sender";
				else
				{
					if (type == 3)
						status = "receiver";
					else
						continue;
						//status = "sender";				
				}
				QDateTime date = QDateTime::fromTime_t(itemMap.value("time").toInt());
				QString time = date.toString("yyyy-MM-dd hh:mm:ss");  //交易时间。
				double value = itemMap.value("value").toString().toDouble();
				value = value / 100000000;
				QString strValue = QString::number(value, 'f', 2);    //交易金额。
				QString txHash = itemMap.value("txHash").toString();  //交易hash。
				int height = itemMap.value("height").toInt();         //交易区块。
				double fee = (double)itemMap.value("fee").toInt();
				fee = fee / 100000000;
				QString strFee = QString::number(fee, 'f');                //矿工费用。
				
				//向内层遍历，获取转入地址和转出地址。
				QString strFrom;
				QString strTo;

				//inputs项目为该条交易记录的转入部分，一条交易记录可能有多次转入，所以是一个列表。
				QVariantList inputsList = itemMap.value("inputs").toList();
                foreach (QVariant inputItem , inputsList)
				{
					//每一条转入记录都要获取previousOutput部分，这是一层嵌套。
					QVariantMap inputMap = inputItem.toMap();
					QVariantMap preMap = inputMap.value("previousOutput").toMap();
					//获取转入地址。
					QString toAddress = preMap.value("address").toString();
					//累加。
					if (strTo.isEmpty())
						strTo.append(toAddress);
					else
						strTo.append("\n" + toAddress);
				}

				//outputs项目为该条交易的转出部分，一条交易可能有多次转出，所以是一个列表。
				QVariantList outputsList = itemMap.value("outputs").toList();
                foreach (QVariant outputItem , outputsList)
				{
					//转出与转入不同，每个转入条目都可以直接转化为字典，不含嵌套。
					QVariantMap outputMap = outputItem.toMap();
					//获取转出地址。
					QString fromAddress = outputMap.value("address").toString();
					//累加。
					if (strFrom.isEmpty())
						strFrom.append(fromAddress);
					else
						strFrom.append("\n" + fromAddress);
				}

				//一条交易记录获取完毕，开始组织记录结构体。
				RecordInfo record;
				record.age = time;
				record.block = height;
				record.from = strFrom;
				record.status = status;
				record.to = strTo;
				record.txFee = strFee;
				record.txHash = txHash;
				//record.type = variant.value("type").toInt();
				record.value = strValue;
				record.sendId = itemMap.value("sendIMUserId").toString();
				record.receiveId = itemMap.value("receiveIMUserId").toString();

				recordsList.append(record);
			}

			emit sigRecords(recordsList);
		}
	}
}

void OPRequestShareLib::getRecoveryEosWallet(QString pub1, QString pub2)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotRecoveryEOSWallet(bool, QString)));
	QString strRequest = tcserver + "/eos/getKeyAccountsByKey";
	strRequest += "?ownerPublicKey=" + pub1;
	strRequest += "&activePublicKey=" + pub2;

	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotRecoveryEOSWallet(bool success, QString result)
{
	if (success)
	{
		if (result.contains("success"))
		{
			QJsonDocument json = QJsonDocument::fromJson(result.toUtf8());
			QVariantMap map = json.toVariant().toMap();
			QVariantMap data = map.value("data").toMap();
			QVariantList account_names = data.value("account_names").toList();

			QString accounts;
			foreach(QVariant name, account_names)
			{
				QString strName = name.toString();

				if (accounts.isEmpty())
					accounts += strName;
				else
				{
					accounts += "_" + strName;
				}
			}

			emit sigRecoveryEos(accounts);
			return;
		}
	}

	emit sigRecoveryEos("failed");
}

void OPRequestShareLib::getEOSBalance(QString address)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotEOSBalance(bool, QString)));
	QString strRequest = tcserver + "/eos/get_currency_balance";
	strRequest += "?code=eosio.token";
	strRequest += "&accountName=" + address;
	strRequest += "&symbol=EOS";    //正式eos，测试sys。
	strRequest += "&chainId=" + QString(ChainID_EOS);

	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotEOSBalance(bool success, QString result)
{
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();

		if (map.value("result").toString() == "success")
		{
			QVariantList data = map.value("data").toList();
			if (!data.isEmpty())
			{
				QString strBalance = data.first().toString();
				QString balance = strBalance.split(" ").first();

				emit sigEOSBalance(balance);
				return;
			}
		}
	}

	emit sigEOSBalance("failed");
}

void OPRequestShareLib::addEosWallet(QString address, QString pubKey, QString priKey, QString mainAddress, QString name, QString icon, QString blockChain, int type, QString activePubKey, QString activePriKey, QString accounts)
{
	//发送请求。
	HttpNetWork::HttpNetWorkShareLib *request = new HttpNetWork::HttpNetWorkShareLib;
	connect(request, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotAddChildrenWallet(bool, QString)));
	QString url = QString(tcserver + "/account/addWallet");
	url += QString("?accountAddress=") + address;
	url += QString("&accountPublicKey=") + pubKey;
	url += QString("&accountPrivateKey=") + priKey;
	url += QString("&mainAccountAddress=") + mainAddress;
	url += QString("&accountIcon=") + icon;
	url += QString("&accountName=") + name;
	url += QString("&blockChainName=" + blockChain);
	url += QString("&accountType=") + QString::number(type);

	url += QString("&eosActivePublicKey=" + activePubKey);
	url += QString("&eosActivePrivateKey=" + activePriKey);
	url += QString("&eosAccountNames=" + accounts);

	request->getHttpRequest(url);
}

void OPRequestShareLib::getEOSRecords(QString account, int page, int pageSize, int type)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotEOSRecords(bool, QString)));
	QString strRequest = tcserver + "/eos/getTxsDataByAccount";
	strRequest += "?account=" + account;
	strRequest += "&page=" + QString::number(page);
	strRequest += "&pagesize=" + QString::number(pageSize);
	strRequest += "&type=" + QString::number(type);

	http->setData(account);
	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotEOSRecords(bool success, QString result)
{
	HttpNetWork::HttpNetWorkShareLib *http = qobject_cast<HttpNetWork::HttpNetWorkShareLib *>(sender());
	QString account = http->getData().toString();
	if (success)
	{
		QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
		QVariantMap map = doc.toVariant().toMap();

		if (map.value("result").toString() == "success")
		{
			QList<RecordInfo> records;

			QVariantMap data = map.value("data").toMap();
			QVariantList dataList = data.value("data").toList();
			foreach(QVariant var, dataList)
			{
				RecordInfo record;

				QVariantMap transMap = var.toMap();
				
				record.age = transMap.value("expiration").toString();
				record.from = transMap.value("from").toString();
				record.to = transMap.value("to").toString();
				
				if (record.from == account)
					record.status = "sender";
				else
					record.status = "receiver";
				
				record.txHash = transMap.value("trx_id").toString();
				record.value = transMap.value("quantity").toString().split(" ").first();
				record.block = 0;
				record.sendId = transMap.value("sendIMUserId").toString();
				record.receiveId = transMap.value("receiveIMUserId").toString();

				records.append(record);
			}

			emit sigRecords(records);
		}
	}
}

void OPRequestShareLib::getDataParam(QString from, QString to, QString value, QString chainID)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotEOSDataParam(bool, QString)));
	QString strRequest = tcserver + "/eos/parseDataParam";
	strRequest += "?parseDataParamType=parseTransferData";
	strRequest += "&from=" + from;
	strRequest += "&to=" + to;
	strRequest += "&quantity" + value;
	strRequest += "&chainId=" + chainID;

	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotEOSDataParam(bool success, QString result)
{
	if (success)
	{
		auto ParamObj = QJsonDocument::fromJson(result.toUtf8()).object();
		QString DataParam = ParamObj.value("data").toString();
		emit sigEOSDataParam(DataParam);
	}
}

void OPRequestShareLib::getEOSInfo()
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotEOSInfo(bool, QString)));
	QString strRequest = tcserver + "/eos/get_info";

	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotEOSInfo(bool success, QString result)
{
	if (success)
	{
		emit sigEOSInfo(result);
	}
}

void OPRequestShareLib::getBlockNum(QString headBlockNum)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotEOSBlockNum(bool, QString)));
	QString strRequest = tcserver + "/eos/get_block";
	strRequest += "?headBlockNum=" + headBlockNum;

	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotEOSBlockNum(bool success, QString result)
{
	if (success)
	{
		auto infoblock = QJsonDocument::fromJson(result.toUtf8()).object();
		auto dataBlock = infoblock.value("data").toObject();
		int block_num = dataBlock.value("block_num").toInt();
		int ref_block_prefix = dataBlock.value("ref_block_prefix").toInt();

		emit sigBlockNum(block_num, ref_block_prefix);
	}
}

void OPRequestShareLib::eosPushTransactionBody(QString json)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotPushTransactionBody(bool, QString)));
	QString strRequest = tcserver + "/eos/push_transaction_body";

	http->postHttpJsonRequest(strRequest, json.toUtf8());
}

void OPRequestShareLib::getRedBag(QString userId, QString imUserId, QString trusteeshipCoinId, QString packetId)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotGetPacketResult(bool, QString)));
	QString strRequest = tcserver + "/redpacket/grabRedPacket";
	strRequest += QString("?userId=%1&trusteeshipCoinId=%2&packetId=%3&imUserId=%4")
		.arg(userId).arg(trusteeshipCoinId).arg(packetId).arg(imUserId);

	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::readRedBagStatus(QString userId, QString packetId)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotPacketStatus(bool, QString)));
	QString strRequest = tcserver + "/redpacket/redPacketStatus";
	strRequest += QString("?userId=%1&packetId=%2")
		.arg(userId).arg(packetId);

	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::getRedBagRecordsIn(QString userID)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotPacketRecordsIn(bool, QString)));
	QString strRequest = tcserver + "/redpacket/redPacketRecord";
	strRequest += QString("?userId=%1&page=%2&pagesize=%3&type=1")
		.arg(userID).arg(1).arg(1000);

	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::getRedBagRecordsOut(QString userID)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotPacketRecordsOut(bool, QString)));
	QString strRequest = tcserver + "/redpacket/redPacketRecord";
	strRequest += QString("?userId=%1&page=%2&pagesize=%3&type=2")
		.arg(userID).arg(1).arg(1000);

	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::createRedPacket(QString userId, int trusteeshipCoinId, QString coinName, int type, 
	double totalMoney, int totalSize, QString remarks, QString getPacketUser, double singleMoney)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotCreateRedPacket(bool, QString)));
	QString strRequest = tcserver + "/redpacket/createRedPacket";

	if (getPacketUser.isEmpty())  //接收人为空，是群组红包。只有个人红包有接收人。
	{
		if (type == 0)   //随机红包，没有singleMoney。
		{
			strRequest += QString("?userId=%1&trusteeshipCoinId=%2&coinName=%3&type=%4&totalMoney=%5&totalSize=%6&remarks=%7")
				.arg(userId).arg(trusteeshipCoinId).arg(coinName).arg(type).arg(totalMoney)
				.arg(totalSize).arg(remarks);
		}
		if (type == 1)   //固定金额的红包，有singleMoney。
		{
			strRequest += QString("?userId=%1&trusteeshipCoinId=%2&coinName=%3&type=%4&totalMoney=%5&totalSize=%6&remarks=%7&singleMoney=%8")
				.arg(userId).arg(trusteeshipCoinId).arg(coinName).arg(type).arg(totalMoney)
				.arg(totalSize).arg(remarks).arg(singleMoney);
		}
	}
	else   //个人红包。
	{
		strRequest += QString("?userId=%1&trusteeshipCoinId=%2&coinName=%3&type=%4&totalMoney=%5&totalSize=%6&remarks=%7&getPacketUser=%8&singleMoney=%9")
			.arg(userId).arg(trusteeshipCoinId).arg(coinName).arg(type).arg(totalMoney)
			.arg(totalSize).arg(remarks).arg(getPacketUser).arg(singleMoney);
	}
	

	/*
	QVariantMap map;
	map.insert("userId", userId);
	map.insert("trusteeshipCoinId", trusteeshipCoinId);
	map.insert("coinName", coinName);
	map.insert("type", type);
	map.insert("totalMoney", totalMoney);
	map.insert("totalSize", totalSize);
	map.insert("remarks", remarks);
	map.insert("getPacketUser", getPacketUser);
	map.insert("singleMoney", singleMoney);
	QJsonDocument doc = QJsonDocument::fromVariant(map);

	http->postHttpRequest(strRequest, doc.toJson());
	*/
	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::getCardList(QString phoneNumber, QString address)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotGetCardResult(bool, QString)));
	QString strRequest = tcserver + "/mallCard/getCardListDouble";
	strRequest += QString("?phoneNumber=%1&address=%2")
		.arg(phoneNumber).arg(address);

	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotPushTransactionBody(bool success, QString result)
{
	if (success)
	{
		if (result.contains("success"))
		{
			emit sigTransFinished(true);
			return;
		}
	}

	emit sigTransFinished(false);
}

void OPRequestShareLib::slotGetPacketResult(bool success, QString result)
{
	if (success)
	{
		if (result.contains("redPacket"))
		{
			emit sigGetPacketResult(result);
			return;
		}
	}

	emit sigGetPacketResult("");
}

void OPRequestShareLib::slotPacketStatus(bool success, QString result)
{
	if (success)
	{
		if (result.contains("success"))
		{
			QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
			QVariantMap map = doc.toVariant().toMap();
			QVariantMap redPacketStatus = map.value("redPacketStatus").toMap();
			int isGrabRedPacket = redPacketStatus.value("isGrabRedPacket").toInt();
			int isOutOfDate = redPacketStatus.value("isOutOfDate").toInt();
			int isOutOfSize = redPacketStatus.value("isOutOfSize").toInt();

			if (isGrabRedPacket == 2 && isOutOfDate == 2 && isOutOfSize == 2)
			{
				emit sigPacketStatus(true);
			}
			else
			{
				emit sigPacketStatus(false);
			}
		}
	}
}

void OPRequestShareLib::slotCreateRedPacket(bool success, QString result)
{
	if (success)
	{
		if (result.contains("success"))
		{
			QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
			QVariantMap map = doc.toVariant().toMap();
			QVariantMap data = map.value("data").toMap();
			QString packetID = data.value("packetId").toString();
			if (!packetID.isEmpty())
			{
				emit sigCreatePacketID(packetID);
				return;
			}
		}
	}

	emit sigCreatePacketID("");
}

void OPRequestShareLib::slotGetCardResult(bool success, QString result)
{
	if (success)
	{
		if (result.contains("success"))
		{
			QList<CardInfo> cards;
			QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
			QVariantMap map = doc.toVariant().toMap();
			QVariantList cardList = map.value("data").toList();
			foreach(QVariant varCard, cardList)
			{
				QVariantMap cardMap = varCard.toMap();
				CardInfo card;
				card.bgImg = cardMap.value("bgImg").toString();
				card.cardImg = cardMap.value("cardImg").toString();
				card.coinNumber = cardMap.value("coinNumber").toString();
				card.contractAddress = cardMap.value("contractAddress").toString();
				cards.append(card);
			}

			emit sigGetCard(cards);
		}
	}
}

void OPRequestShareLib::slotPacketRecordsOut(bool success, QString result)
{
	if (success)
	{
		if (result.contains("success"))
		{
			QList<RecordInfo> records;
			QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
			QVariantMap map = doc.toVariant().toMap();
			QVariantList getRedPacketRecordList = map.value("getRedPacketRecordList").toList();
			foreach(QVariant var, getRedPacketRecordList)
			{
				QVariantMap recordMap = var.toMap();
				RecordInfo record;
				int type = recordMap.value("type").toInt();
				record.to = type == 0 ? tr("random red packet") : tr("identical red packet");
				record.age = recordMap.value("timestamp").toString();
				record.unit = recordMap.value("coinName").toString();
				record.value = QString::number(recordMap.value("totalMoney").toDouble(),'f',5);
				records.append(record);
			}
			emit sigRecords(records);
		}
	}
}

void OPRequestShareLib::slotPacketRecordsIn(bool success, QString result)
{
	if (success)
	{
		if (result.contains("success"))
		{
			QList<RecordInfo> records;
			QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
			QVariantMap map = doc.toVariant().toMap();
			QVariantList getRedPacketRecordList = map.value("getRedPacketRecordList").toList();
			foreach (QVariant var, getRedPacketRecordList)
			{
				QVariantMap recordMap = var.toMap();
				RecordInfo record;
				record.from = recordMap.value("userName").toString();
				record.age = recordMap.value("timestamp").toString();
				record.unit = recordMap.value("coinName").toString();
				record.value = QString::number(recordMap.value("totalMoney").toDouble(),'f',5);
				records.append(record);
			}
			emit sigRecords(records);
		}
	}
}

void OPRequestShareLib::getAssets(QString phoneNumber)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotGetAssets(bool, QString)));
	QString strRequest = tcserver + "/trusteeship/allAccountFollowTrusteeshipCoinList";
	strRequest += "?userId=" + phoneNumber;

	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::getHostingAccount(QString userID)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotHostingAccount(bool, QString)));
	QString strRequest = tcserver + "/trusteeship/assets";
	strRequest += "?userId=" + userID;

	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::getHostingRecords(QString userID, double trusteeshipCoinId)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotHostingRecords(bool, QString)));
	QString strRequest = tcserver + "/trusteeship/txlist";
	strRequest += "?userId=" + userID;
	strRequest += QString("&trusteeshipCoinId=%1").arg(trusteeshipCoinId);
	strRequest += "&type=1";
	strRequest += "&page=1";
	strRequest += "&pagesize=1000";

	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::HostingAccountCoin(QString userID, QString coinID, QString value, QString address)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotHostingCoinResult(bool, QString)));
	QString strRequest = tcserver + "/trusteeship/reflect";
	strRequest += "?userId=" + userID;
	strRequest += "&trusteeshipCoinId=" + coinID;
	strRequest += "&value=" + value;
	strRequest += "&txTo=" + address;

	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::HostingCharge(QString userID, QString trusteeshipId, QString signedTransactionData, QString value, QString txFrom, QString txTo)
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotHostingCoinResult(bool, QString)));
	QString strRequest = tcserver + "/trusteeship/recharge";
	strRequest += "?userId=" + userID;
	strRequest += "&trusteeshipId=" + trusteeshipId;
	strRequest += "&signedTransactionData=" + signedTransactionData;
	strRequest += "&value=" + value;
	strRequest += "&txFrom=" + txFrom;
	strRequest += "&from=" + txTo;
	strRequest += "&txTo=" + txTo;

	http->getHttpRequest(strRequest);
}

void OPRequestShareLib::slotGetAssets(bool success, QString result)
{
	if (success)
	{
		if (result.contains("success"))
		{
			QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
			QVariantMap map = doc.toVariant().toMap();
			QVariantList list = map.value("userFollowTrusteeshipCoin").toList();

			QList<AssetInfo> assetsList;
			foreach (QVariant var, list)
			{
				QVariantMap data = var.toMap();
				AssetInfo asset;
				asset.availableCoin = data.value("availableCoin").toDouble();
				asset.coinIcon = data.value("coinIcon").toString();
				asset.coinName = data.value("coinName").toString();
				asset.trusteeshipId = data.value("trusteeshipId").toDouble();
				asset.coinMarketPrice = data.value("coinMarketPrice").toDouble();
				asset.userCoinMarketNum = data.value("userCoinMarketNum").toDouble();

				assetsList.append(asset);
			}

			emit sigAssetsInfo(assetsList);
		}
	}
	else
	{
		emit sigAssetsInfo(QList<AssetInfo>());
	}
}

void OPRequestShareLib::slotHostingAccount(bool success, QString result)
{
	if (success)
	{
		if (result.contains("success"))
		{
			QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
			QVariantMap map = doc.toVariant().toMap();
			QVariantList list = map.value("assets").toList();

			QList<AssetInfo> assetsList;
			foreach(QVariant var, list)
			{
				QVariantMap data = var.toMap();
				AssetInfo asset;
				
				asset.coinName = data.value("coinName").toString();
				asset.coinFullName = data.value("coinFullName").toString();
				asset.coinIcon = data.value("coinIcon").toString();
				asset.chain = data.value("chain").toString();
				asset.blockchainId = data.value("blockchainId").toString();
				asset.sysAddress = data.value("sysAddress").toString();
				asset.userId = data.value("userId").toString();

				asset.availableCoin = data.value("availableCoin").toDouble();
				asset.freezingCoin = data.value("freezingCoin").toDouble();
				asset.userCoinMarketNum = data.value("userCoinMarketNum").toDouble();
				asset.coinMarketPrice = data.value("coinMarketPrice").toDouble();
				asset.coinDecimals = data.value("coinDecimals").toDouble();
				asset.trusteeshipId = data.value("trusteeshipId").toDouble();

				assetsList.append(asset);
			}

			emit sigAssetsInfo(assetsList);
		}
	}
	else
	{
		emit sigAssetsInfo(QList<AssetInfo>());
	}
}

void OPRequestShareLib::getOnlineMessage()
{
	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib();
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotGetAssets(bool, QString)));
	QString strRequest = tcserver + "/user/onLineMessage";
	strRequest += "?access_token=" + gOPDataManager->getAccessToken();

	http->getHttpRequest(strRequest);
}





