﻿#include "imvideoplayer.h"
#include "ui_imvideoplayer.h"

using namespace QtAV;
IMVideoPlayer::IMVideoPlayer(QWidget *parent)
	: QWidget(parent), m_vo(NULL),m_player(NULL)
{
	ui = new Ui::IMVideoPlayer();
	ui->setupUi(this);

	setWindowFlags(Qt::FramelessWindowHint | Qt::Tool);
	setAttribute(Qt::WA_DeleteOnClose);
	mouse.setX(-1);
	isPlaying = false;

	closeImg.load(":/video/Resources/video/close.png");
	playImg.load(":/video/Resources/video/play.png");
	pauseImg.load(":/video/Resources/video/pause.png");
	ui->quitButton->setIcon(QIcon(closeImg));
	ui->playButton->setIcon(QIcon(pauseImg));

	connect(ui->quitButton, SIGNAL(clicked()), this, SIGNAL(sigClose()));
	connect(ui->playButton, SIGNAL(clicked()), this, SLOT(playPause()));
	connect(ui->slider, SIGNAL(valueChanged(int)), this, SLOT(setSliderSlot(int)));

	QFile file(":/QSS/Resources/QSS/VideoPlayShareLib/imvideoplayer.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	m_unit = 1000;
	m_player = new AVPlayer(this);
	m_vo = new VideoOutput(this);
	if (!m_vo->widget()) {
		return;
	}
	m_player->setRenderer(m_vo);

	m_vo->widget()->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	m_firstPicLabel = new QLabel(m_vo->widget());


	ui->verticalLayout_2->addWidget(m_vo->widget());

	connect(ui->slider, SIGNAL(sliderMoved(int)), SLOT(seekBySlider(int)));
	connect(ui->slider, SIGNAL(sliderPressed()), SLOT(seekBySlider()));
	connect(m_player, SIGNAL(positionChanged(qint64)), SLOT(updateSlider(qint64)));
	connect(m_player, SIGNAL(started()), SLOT(updateSlider()));
	connect(m_player, SIGNAL(stopped()), SLOT(stopPlayer()));
	connect(m_player, SIGNAL(notifyIntervalChanged()), SLOT(updateSliderUnit()));

	sizeInitSlot();
}

IMVideoPlayer::~IMVideoPlayer()
{
	//ui->videoWidget->stop();
	if (m_firstPicLabel)
		delete m_firstPicLabel;

	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

//鼠标事件的处理。
void IMVideoPlayer::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();
	return QWidget::mousePressEvent(event);
}
void IMVideoPlayer::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void IMVideoPlayer::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}
void IMVideoPlayer::mouseDoubleClickEvent(QMouseEvent *event)
{
	playPause();
}

//载入视频并播放的方法。
void IMVideoPlayer::videoPlay(QString mediaPath)
{
	mediaPath.replace("%20", " ");
#ifdef Q_OS_WIN
	mediaPath.replace("file:///", "");
#else
    mediaPath.replace("file://", "");
#endif
	address = mediaPath;
	if (address.isEmpty())
		return;
	QString strPicPath = address.left(address.indexOf("."));
	strPicPath += ".png";
	QPixmap pTmp(strPicPath);
// 	QMatrix matrixTmp;
// 	matrixTmp.rotate(-90);
// 	pTmp = pTmp.transformed(matrixTmp, Qt::SmoothTransformation);
	QMatrix matrix;
	matrix.rotate(0);//旋转改到vedioFrame 此处暂时保留
	QSize wsize = m_vo->widget()->size();
	QSize psize = pTmp.size();
	QSize nsize;
	if (wsize.width() / wsize.height() > psize.width() / psize.height())
	{
		nsize.setHeight(wsize.height());
		nsize.setWidth(wsize.height()*psize.width() / psize.height());
	}
	else
	{
		nsize.setWidth(wsize.width());
		nsize.setHeight(wsize.width()*psize.height() / psize.width());
	}
	m_firstPicLabel->resize(nsize);
	pTmp = pTmp.scaled(nsize.width(),nsize.height());
	psize = pTmp.size();
	m_firstPicLabel->setPixmap(pTmp.transformed(matrix, Qt::SmoothTransformation));
	m_firstPicLabel->move((wsize.width() - nsize.width()) / 2, (wsize.height() - nsize.height()) / 2);
	m_player->play(address);
}

void IMVideoPlayer::sizeInitSlot()
{
	QSize screen = QApplication::desktop()->size();
	this->resize(m_vo->widget()->width(), m_vo->widget()->height()+80);

	this->move((screen.width() - this->width()) / 2,
		(screen.height() - this->height()) / 2);

	this->show();
}

void IMVideoPlayer::seekBySlider(int value)
{
	if (!m_player->isPlaying())
		return;
	m_player->seek(qint64(value*m_unit));
}

void IMVideoPlayer::seekBySlider()
{
	seekBySlider(ui->slider->value());
}

void IMVideoPlayer::playPause()
{
	if (!m_player->isPlaying()) 
	{
		ui->playButton->setIcon(QIcon(pauseImg));
		m_player->play();
		return;
	}
	else if (m_player->isPaused())
	{
		ui->playButton->setIcon(QIcon(pauseImg));
		m_player->pause(!m_player->isPaused());
	}
	else
	{
		ui->playButton->setIcon(QIcon(playImg));
		m_player->pause(!m_player->isPaused());
	}
}

void IMVideoPlayer::stopPlayer()
{
	m_firstPicLabel->show();
	ui->playButton->setIcon(QIcon(playImg));
	ui->slider->setValue(0);
}

void IMVideoPlayer::updateSlider(qint64 value)
{
	if(value>350)
		m_firstPicLabel->hide();
	ui->slider->setRange(0, int(m_player->duration() / m_unit));
	ui->slider->setValue(int(value / m_unit));
}

void IMVideoPlayer::updateSlider()
{
	isPlaying = true;
	updateSlider(m_player->position());
}

void IMVideoPlayer::updateSliderUnit()
{
	m_unit = m_player->notifyInterval();
	updateSlider();
}