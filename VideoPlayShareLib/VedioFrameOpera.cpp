#include "VedioFrameOpera.h"
#include "QtAV/FrameReader.h"
#include <QFile>
#include <QPixmap>
using namespace QtAV;

VedioFrameOpera::VedioFrameOpera(QObject *parent)
	: QObject(parent)
{
}

VedioFrameOpera::~VedioFrameOpera()
{
}

void VedioFrameOpera::CreateVedioPicture(QString vedioPath, QString imgPath)
{
	QFile file(vedioPath);
	if (file.exists() && file.size() > 0)
	{
		FrameReader r;
		r.setMedia(vedioPath);
		VideoFrame f;
		while (r.readMore())
		{
			f = r.getVideoFrame();
			if (f.isValid())
			{
				break;
			}
		}
		QImage img = f.toImage();
		QPixmap pix = QPixmap::fromImage(img);
		QMatrix matrix;
		matrix.rotate(90);
		pix = pix.transformed(matrix, Qt::SmoothTransformation);
		img = pix.toImage();
		img.save(imgPath);
	}
}