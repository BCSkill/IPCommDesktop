﻿#include "imsocketdatabasesharelib.h"
#include <QDateTime>
#include "QStringLiteralBak.h"
#include <QDir>
#include <QJsonDocument>
#include <QTextStream>
#include <qdebug.h>

#ifndef Q_OS_WIN
#include "inline_mac.h"
#endif


IMSocketDataBaseShareLib::IMSocketDataBaseShareLib()
{
	mSocketDB = NULL;
	mPageNum = 0;
	mSearchPageNum = 0;
	m_strUserPath = "";
}

IMSocketDataBaseShareLib::~IMSocketDataBaseShareLib()
{

}

bool IMSocketDataBaseShareLib::ConnectDB(QString strDBPath, QString strDBName, QString strPsw)
{
	if (mSocketDB == NULL) mSocketDB = new SqlLiteShareLib();
	if (mSocketDB->CreateDataFile(strDBPath))
	{
		bool bErro = false;
//#ifdef _DEBUG
		bErro = mSocketDB->OnConnectDB(strDBPath, strDBName, "");
//#else
	//	bErro = mSocketDB->OnConnectDB(strDBPath, strDBName, strPsw);
//#endif
		if (bErro)
		{
			/*消息*/
			if (!mSocketDB->isExistTable("MESSAGEINFO"))
			{
				//创建消息表
				strSql = "create table MESSAGEINFO(UserID int ,ChatID int,MessageID varchar(50),Version int,FromUserID int,"
					"ToUserID int,ClientTime int,ServerTime int,MsgOrder int,MsgDeliverType int,MessageChildType int,"
					"strMsg text,nUserID int,MessageState int,SendTime int,MsgIntegral int,updateTime varchar(30));";
				mSocketDB->ExecuSql(strSql);
			}
			else
			{
				QString strSql = QString::fromLocal8Bit("select * from sqlite_master where name='MESSAGEINFO' and sql like '%MsgIntegral%'");
				QSqlQuery query = mSocketDB->ExecuQuery(strSql);
				if (!query.next())
				{
					strSql = QString("ALTER TABLE MESSAGEINFO ADD COLUMN MsgIntegral int");
					mSocketDB->ExecuSql(strSql);
				}
			}
		}
		else
		{
			return false;
		}
	}
	return false;
}

void IMSocketDataBaseShareLib::DisConnectDB()
{
	if (mSocketDB != NULL)
	{
		mSocketDB->CloseDataBase();
		delete mSocketDB;
		mSocketDB = NULL;
	}
}

void IMSocketDataBaseShareLib::InserMessageInfo(int nSelfID, int nUserID, MessageInfo messageInfo)
{
	if (messageInfo.MsgDeliverType == 1)
		nUserID = messageInfo.nToUserID;

	/*首先删除MSGID相同的*/
	QString strSql = QString("delete from MESSAGEINFO where UserID = %1 and MessageID = '%2'").arg(nSelfID).arg(QString(messageInfo.msgID));
	mSocketDB->ExecuSql(strSql);

	//截取路径后半段
	if ((messageInfo.MessageChildType == Message_PIC ||
		messageInfo.MessageChildType == Message_AUDIO ||
		messageInfo.MessageChildType == Message_VEDIO ||
		//messageInfo.MessageChildType == Message_FILE ||
		messageInfo.MessageChildType == Message_VOICE)&&
		messageInfo.nFromUserID !=0)	
	{
		messageInfo.strMsg = messageInfo.strMsg.replace(m_strUserPath, "");
	}

	QDateTime time = QDateTime::currentDateTime();//获取系统现在的时间
	QString str = time.toString("yyyy-MM-dd hh:mm:ss"); //设置显示格式
	

	strSql = "";
	QTextStream(&strSql) << "insert into MESSAGEINFO(UserID, ChatID, MessageID, Version, FromUserID, ToUserID, ClientTime, "
		"ServerTime,MsgOrder,MsgDeliverType,MessageChildType,strMsg,"
		"nUserID,MessageState,SendTime,MsgIntegral,updateTime) values("
		<< nSelfID <<","
		<< nUserID << ","
		<< "\'"+QString(messageInfo.msgID)+"\'" << ","
		<< messageInfo.version << ","
		<< messageInfo.nFromUserID << ","
		<< messageInfo.nToUserID << ","
		<< messageInfo.ClientTime << ","
		<< messageInfo.ServerTime << ","
		<< messageInfo.nMsgOrder << ","
		<< messageInfo.MsgDeliverType << ","
		<< messageInfo.MessageChildType << ","
		<< "\'" + QString(messageInfo.strMsg).replace("\'", "\'\'") + "\'" << ","
		<< messageInfo.nUserID << ","
		<< messageInfo.MessageState << ","
		<< messageInfo.SendTime << ","
		<< messageInfo.integral << ","
		<< "\'" + str + "\'"
		<< ")";

	/*strSql = QString("insert into MESSAGEINFO(UserID,ChatID,MessageID,Version,FromUserID,ToUserID,ClientTime,"
		"ServerTime,MsgOrder,MsgDeliverType,MessageChildType,strMsg,"
		"nUserID,MessageState,SendTime,MsgIntegral,updateTime) values(%1,%2,'%3',%4,%5,%6,%7,"
		"%8,%9,'%10',%11,'%12',%13,%14,%15,%16,'%17')").arg(nSelfID).arg(nUserID).arg(QString(messageInfo.msgID))
		.arg(messageInfo.version).arg(messageInfo.nFromUserID).arg(messageInfo.nToUserID).arg(messageInfo.ClientTime)
		.arg(messageInfo.ServerTime).arg(messageInfo.nMsgOrder).arg(messageInfo.MsgDeliverType)
		.arg(messageInfo.MessageChildType).arg(QString(messageInfo.strMsg).replace("\'","\'\'")).arg(messageInfo.nUserID).arg(messageInfo.MessageState)
		.arg(messageInfo.SendTime).arg(messageInfo.integral).arg(str);*/

	mSocketDB->ExecuSql(strSql);
}

MessageInfo IMSocketDataBaseShareLib::GetDBMessageInfoWithMsgID(int nIMUserID, QString strMsgID)
{
	MessageInfo messageInfo;
	messageInfo.msgID = "";
	QString strSql = QString("select * from MESSAGEINFO where UserID = %1 and MessageID = \"%2\"").arg(nIMUserID).arg(strMsgID);
	if (mSocketDB != NULL && mSocketDB->IsOpen())
	{
		QSqlQuery resultQuery = mSocketDB->ExecuQuery(strSql);
		if (resultQuery.first())
		{
			messageInfo.nFromUserID = resultQuery.value("FromUserID").toInt();
			messageInfo.nToUserID = resultQuery.value("ToUserID").toInt();
			messageInfo.ClientTime = resultQuery.value("ClientTime").toLongLong();
			messageInfo.MsgDeliverType = (unsigned char)resultQuery.value("MsgDeliverType").toInt();
			messageInfo.MessageChildType = (short)resultQuery.value("MessageChildType").toInt();
			messageInfo.strMsg = resultQuery.value("strMsg").toString();
			messageInfo.msgID = resultQuery.value("MessageID").toByteArray();
			messageInfo.MessageState = resultQuery.value("MessageState").toInt();
			messageInfo.integral = resultQuery.value("MsgIntegral").toInt();
			messageInfo.ServerTime = resultQuery.value("ServerTime").toLongLong();
			messageInfo.nUserID = resultQuery.value("nUserID").toInt();
			messageInfo.SendTime = resultQuery.value("SendTime").toLongLong();
			messageInfo.version = (short)resultQuery.value("version").toInt();
			//增加路径前半段
			if ((messageInfo.MessageChildType == Message_PIC ||
				messageInfo.MessageChildType == Message_AUDIO ||
				messageInfo.MessageChildType == Message_VEDIO ||
				//messageInfo.MessageChildType == Message_FILE ||
				messageInfo.MessageChildType == Message_VOICE))
			{
				DealWithJsonMsg(messageInfo);
			}
		}

	}
	return messageInfo;
}

//获取所有的消息
QMap<QString, QList<MessageInfo> > IMSocketDataBaseShareLib::GetAllMessage(int nSelfID)
{
	QMap<QString, QList<MessageInfo> > mapTemp;
	QString strSql = QString("select * from MESSAGEINFO where UserID = %1").arg(nSelfID);
	if (mSocketDB != NULL && mSocketDB->IsOpen())
	{
		QSqlQuery resultQuery = mSocketDB->ExecuQuery(strSql);
		while (resultQuery.next())
		{
			MessageInfo messageInfo;
			messageInfo.nFromUserID = resultQuery.value("FromUserID").toInt();
			messageInfo.nToUserID = resultQuery.value("ToUserID").toInt();
			messageInfo.ClientTime = resultQuery.value("ClientTime").toLongLong();
			messageInfo.MsgDeliverType = (unsigned char)resultQuery.value("MsgDeliverType").toInt();
			messageInfo.MessageChildType = (short)resultQuery.value("MessageChildType").toInt();
			messageInfo.strMsg = resultQuery.value("strMsg").toString();
			messageInfo.msgID = resultQuery.value("MessageID").toByteArray();
			messageInfo.MessageState = resultQuery.value("MessageState").toInt();
			messageInfo.integral = resultQuery.value("MsgIntegral").toInt();
			QString ChatID = resultQuery.value("ChatID").toString();
			//增加路径前半段
			if (messageInfo.MessageChildType == Message_PIC ||
				messageInfo.MessageChildType == Message_AUDIO ||
				messageInfo.MessageChildType == Message_VEDIO ||
				//messageInfo.MessageChildType == Message_FILE ||
				messageInfo.MessageChildType == Message_VOICE)
			{
				DealWithJsonMsg(messageInfo);
			}

			int nTemp = 0;
			QMap<QString, QList<MessageInfo> >::iterator itor = mapTemp.begin();
			for (; itor != mapTemp.end(); ++itor)
			{
				if (ChatID == itor.key())
				{
					nTemp = 1;
					itor.value().append(messageInfo);
					break;
				}
			}
			if (nTemp == 0)
			{
				QList<MessageInfo> tempList;
				tempList.append(messageInfo);
				mapTemp.insert(mapTemp.end(), ChatID, tempList);
			}
		}
	}
	return mapTemp;
}

//获取历史50条消息。
QMap<QString, QList<MessageInfo> > IMSocketDataBaseShareLib::GetDBRecentMessage(int nSelfID, int chatID,QString strEndRowId/* = ""*/)
{
	QMap<QString, QList<MessageInfo> > mapTemp;
	//QString strSql = QString("select * from MESSAGEINFO where UserID = %1 and ChatID=%2 and FromUserID!=0").arg(nSelfID).arg(chatID);

	//限制SQL语句只获取最后的50条数据
	QString strSql = "";
	if (strEndRowId.isEmpty())
	{
		strSql = QString("select * from (select rowid,* from MESSAGEINFO where UserID = %1 and ChatID=%2 and FromUserID!=0 order by rowid desc limit %3) order by rowid").arg(nSelfID).arg(chatID).arg(DB_MESSAGEINFO_INIT_MAX_NUM);
	}
	else
	{
		strSql = QString("select * from (select rowid,* from MESSAGEINFO where UserID = %1 and ChatID=%2 and FromUserID!=0 and rowid<%3 order by rowid desc limit %4) order by rowid").arg(nSelfID).arg(chatID).arg(strEndRowId).arg(DB_MESSAGEINFO_FETCH_MAX_NUM);
	}
	
	if (mSocketDB != NULL && mSocketDB->IsOpen())
	{
		QSqlQuery resultQuery = mSocketDB->ExecuQuery(strSql);
		while (resultQuery.next())
		{
			MessageInfo messageInfo;
			messageInfo.nFromUserID = resultQuery.value("FromUserID").toInt();
			messageInfo.nToUserID = resultQuery.value("ToUserID").toInt();
			messageInfo.ClientTime = resultQuery.value("ClientTime").toLongLong();
			messageInfo.ServerTime = resultQuery.value("ServerTime").toLongLong();
			messageInfo.MsgDeliverType = (unsigned char)resultQuery.value("MsgDeliverType").toInt();
			messageInfo.MessageChildType = (short)resultQuery.value("MessageChildType").toInt();
			messageInfo.strMsg = resultQuery.value("strMsg").toString();
			messageInfo.msgID = resultQuery.value("MessageID").toByteArray();
			messageInfo.MessageState = resultQuery.value("MessageState").toInt();
			messageInfo.integral = resultQuery.value("MsgIntegral").toInt();

			messageInfo.strRowId = resultQuery.value("rowid").toString();

			QString ChatID = resultQuery.value("ChatID").toString();

			//增加路径前半段
			if (messageInfo.MessageChildType == Message_PIC ||
				messageInfo.MessageChildType == Message_AUDIO ||
				messageInfo.MessageChildType == Message_VEDIO ||
				//messageInfo.MessageChildType == Message_FILE ||
				messageInfo.MessageChildType == Message_VOICE)
			{
				DealWithJsonMsg(messageInfo);
			}

			int nTemp = 0;
			QMap<QString, QList<MessageInfo> >::iterator itor = mapTemp.begin();
			for (; itor != mapTemp.end(); ++itor)
			{
				if (ChatID == itor.key())
				{
					nTemp = 1;
					itor.value().append(messageInfo);
					break;
				}
			}
			if (nTemp == 0)
			{
				QList<MessageInfo> tempList;
				tempList.append(messageInfo);
				mapTemp.insert(mapTemp.end(), ChatID, tempList);
			}
		}
	}
	return mapTemp;
}

//获取当天所有的消息
QMap<QString, QList<MessageInfo> > IMSocketDataBaseShareLib::GetDBCurrentDayMessage(int nSelfID)
{
	QDateTime time = QDateTime::currentDateTime();//获取系统现在的时间
	QString str = time.toString("yyyy-MM-dd hh:mm:ss"); //设置显示格式

	QMap<QString, QList<MessageInfo> > mapTemp;
	QString strSql = QString("select * from MESSAGEINFO where UserID = %1 and FromUserID!=0 and date(ClientTime/1000,'unixepoch', 'localtime') == date('%2')").arg(nSelfID).arg(str);
	if (mSocketDB != NULL && mSocketDB->IsOpen())
	{
		QSqlQuery resultQuery = mSocketDB->ExecuQuery(strSql);
		while (resultQuery.next())
		{
			MessageInfo messageInfo;
			messageInfo.nFromUserID = resultQuery.value("FromUserID").toInt();
			messageInfo.nToUserID = resultQuery.value("ToUserID").toInt();
			messageInfo.ClientTime = resultQuery.value("ClientTime").toLongLong();
			messageInfo.MsgDeliverType = (unsigned char)resultQuery.value("MsgDeliverType").toInt();
			messageInfo.MessageChildType = (short)resultQuery.value("MessageChildType").toInt();
			messageInfo.strMsg = resultQuery.value("strMsg").toString();
			messageInfo.msgID = resultQuery.value("MessageID").toByteArray();
			messageInfo.MessageState = resultQuery.value("MessageState").toInt();
			messageInfo.integral = resultQuery.value("MsgIntegral").toInt();
			QString ChatID = resultQuery.value("ChatID").toString();

			//增加路径前半段
			if (messageInfo.MessageChildType == Message_PIC ||
				messageInfo.MessageChildType == Message_AUDIO ||
				messageInfo.MessageChildType == Message_VEDIO ||
				//messageInfo.MessageChildType == Message_FILE ||
				messageInfo.MessageChildType == Message_VOICE)
			{
				DealWithJsonMsg(messageInfo);
			}

			int nTemp = 0;
			QMap<QString, QList<MessageInfo> >::iterator itor = mapTemp.begin();
			for (; itor != mapTemp.end(); ++itor)
			{
				if (ChatID == itor.key())
				{
					nTemp = 1;
					itor.value().append(messageInfo);
					break;
				}
			}
			if (nTemp == 0)
			{
				QList<MessageInfo> tempList;
				tempList.append(messageInfo);
				mapTemp.insert(mapTemp.end(), ChatID, tempList);
			}
		}
	}
	return mapTemp;
}

//根据联信ID 获取当天所有的消息
QMap<QString, QList<MessageInfo> > IMSocketDataBaseShareLib::GetDBCurrentDayMessageWithIMUserID(int nIMUserID, int nChatID)
{
	QDateTime time = QDateTime::currentDateTime();//获取系统现在的时间
	QString str = time.toString("yyyy-MM-dd hh:mm:ss"); //设置显示格式

	QMap<QString, QList<MessageInfo> > mapTemp;
	QString strSql = QString("select * from MESSAGEINFO where UserID = %1 and date(ClientTime/1000,'unixepoch', 'localtime') == date('%2') and ChatID=%3").arg(nIMUserID).arg(str).arg(nChatID);
	if (mSocketDB != NULL && mSocketDB->IsOpen())
	{
		QSqlQuery resultQuery = mSocketDB->ExecuQuery(strSql);
		while (resultQuery.next())
		{
			MessageInfo messageInfo;
			messageInfo.nFromUserID = resultQuery.value("FromUserID").toInt();
			messageInfo.nToUserID = resultQuery.value("ToUserID").toInt();
			messageInfo.ClientTime = resultQuery.value("ClientTime").toLongLong();
			messageInfo.MsgDeliverType = (unsigned char)resultQuery.value("MsgDeliverType").toInt();
			messageInfo.MessageChildType = (short)resultQuery.value("MessageChildType").toInt();
			messageInfo.strMsg = resultQuery.value("strMsg").toString();
			messageInfo.msgID = resultQuery.value("MessageID").toByteArray();
			messageInfo.MessageState = resultQuery.value("MessageState").toInt();
			messageInfo.integral = resultQuery.value("MsgIntegral").toInt();
			QString ChatID = resultQuery.value("ChatID").toString();

			//增加路径前半段
			if (messageInfo.MessageChildType == Message_PIC ||
				messageInfo.MessageChildType == Message_AUDIO ||
				messageInfo.MessageChildType == Message_VEDIO ||
				//messageInfo.MessageChildType == Message_FILE ||
				messageInfo.MessageChildType == Message_VOICE)
			{
				DealWithJsonMsg(messageInfo);
			}

			int nTemp = 0;
			QMap<QString, QList<MessageInfo> >::iterator itor = mapTemp.begin();
			for (; itor != mapTemp.end(); ++itor)
			{
				if (ChatID == itor.key())
				{
					nTemp = 1;
					itor.value().append(messageInfo);
					break;
				}
			}
			if (nTemp == 0)
			{
				QList<MessageInfo> tempList;
				tempList.append(messageInfo);
				mapTemp.insert(mapTemp.end(), ChatID, tempList);
			}
		}
	}
	return mapTemp;
}

void IMSocketDataBaseShareLib::GetDBUnSendMessageInfo(int nSelfID)
{
	QString strSql = QString("select * from MESSAGEINFO where UserID = %1 ").arg(nSelfID);
	if (mSocketDB != NULL && mSocketDB->IsOpen())
	{
		QSqlQuery resultQuery = mSocketDB->ExecuQuery(strSql);
		while (resultQuery.next())
		{
			//首先判断消息类型 只存 预发送 待发送 发送中 未读 消息
			int nState = resultQuery.value("MessageState").toInt();
			if (nState == MESSAGE_STATE_PRESEND || nState == MESSAGE_STATE_WAITSEND || nState == MESSAGE_STATE_SENDING || nState == MESSAGE_STATE_UNREAD)
			{
				int ChatID = resultQuery.value("ChatID").toInt();
				MessageInfo messageInfo;
				messageInfo.version = (short)resultQuery.value("Version").toInt();
				messageInfo.nFromUserID = resultQuery.value("FromUserID").toInt();
				messageInfo.nToUserID = resultQuery.value("ToUserID").toInt();
				messageInfo.ClientTime = resultQuery.value("ClientTime").toLongLong();
				messageInfo.ServerTime = resultQuery.value("ServerTime").toLongLong();
				messageInfo.nMsgOrder = resultQuery.value("MsgOrder").toInt();
				messageInfo.MsgDeliverType = (unsigned char)resultQuery.value("MsgDeliverType").toInt();
				messageInfo.MessageChildType = (short)resultQuery.value("MessageChildType").toInt();
				messageInfo.strMsg = resultQuery.value("strMsg").toString();
				messageInfo.nUserID = (short)resultQuery.value("nUserID").toInt();
				messageInfo.MessageState = (short)resultQuery.value("MessageState").toInt();
				messageInfo.SendTime = (short)resultQuery.value("SendTime").toLongLong();
				messageInfo.integral = resultQuery.value("MsgIntegral").toInt();
				messageInfo.isHavePMsgId = (unsigned char)0;                 //是否有MsgID

																			 //增加路径前半段
				if (messageInfo.MessageChildType == Message_PIC ||
					messageInfo.MessageChildType == Message_AUDIO ||
					messageInfo.MessageChildType == Message_VEDIO ||
					//messageInfo.MessageChildType == Message_FILE ||
					messageInfo.MessageChildType == Message_VOICE)
				{
					DealWithJsonMsg(messageInfo);
				}

				for (int i = 0; i < 10; i++)
				{
					messageInfo.PerByte[i] = (unsigned char)0;
				}
				messageInfo.msgID = resultQuery.value("MessageID").toByteArray();
				//gDataManager->OnInsertMessage(messageInfo, ChatID);
			}
		}
	}
}

//更新消息状态
void IMSocketDataBaseShareLib::UpdataMessageStateInfo(QByteArray msgID, int nState, int integral)
{
	QString strSql = QString("update MESSAGEINFO set MessageState= %1, MsgIntegral=%2 where MessageID = '%3'").arg(nState).arg(integral).arg(QString(msgID));
	if (mSocketDB != NULL && mSocketDB->IsOpen())
	{
		mSocketDB->ExecuSql(strSql);
	}
}

void IMSocketDataBaseShareLib::UpdataMessageContent(QByteArray msgID, QString content)
{
	QString strSql = QString("update MESSAGEINFO set strMsg= '%1' where MessageID = '%2'").arg(content).arg(QString(msgID));
	if (mSocketDB != NULL && mSocketDB->IsOpen())
	{
		mSocketDB->ExecuSql(strSql);
	}
}

//获取分页数量
int IMSocketDataBaseShareLib::GetDBMessageRecordPageNum(int nChatID, int nPageNum)
{
	mPageNum = nPageNum;
	strSql = QString("select count(*) from MESSAGEINFO where ChatID = %1  and MessageChildType <80 ").arg(nChatID);
	if (mSocketDB != NULL && mSocketDB->IsOpen())
	{
		QSqlQuery resultDetQuery = mSocketDB->ExecuQuery(strSql);
		int nIndex = 0;

		while (resultDetQuery.next())
		{
			nIndex = resultDetQuery.value("count(*)").toInt();
		}
		if (nIndex == 0)
		{
			return 0;
		}
		if (nPageNum != 0)
		{
			if (nIndex <= nPageNum)
			{
				return 1;
			}
			else
			{
				if (nIndex%nPageNum != 0)
				{
					return nIndex / nPageNum + 1;
				}
				else
					return nIndex / nPageNum;
			}
		}
	}
	return 0;
}

//根据页数获取消息记录
QList<MessageInfo> IMSocketDataBaseShareLib::GetDBMessageRecordByPage(int nChatID, int nPage, int messageType)
{
	if (mPageNum == 0)
	{
		mPageNum = 20;
	}
	if (messageType == MessageRecordAll)
	{
		strSql = QString("select * from MESSAGEINFO where ChatID=%1  and MessageChildType <80 ORDER BY date(ClientTime/1000,'unixepoch', 'localtime') DESC LIMIT %2 OFFSET %3").arg(nChatID).arg(mPageNum).arg(mPageNum*nPage);
	}
	else
	{
		strSql = QString("select * from MESSAGEINFO where MessageChildType = %1 and ChatID=%2 ORDER BY date(ClientTime/1000,'unixepoch', 'localtime') DESC LIMIT %3 OFFSET %4").arg(messageType).arg(nChatID).arg(mPageNum).arg(mPageNum*nPage);
	}
	QList<MessageInfo> listMessageInfo;
	if (mSocketDB != NULL && mSocketDB->IsOpen())
	{
		QSqlQuery resultQuery = mSocketDB->ExecuQuery(strSql);
		while (resultQuery.next())
		{
			MessageInfo messageInfo;
			messageInfo.nFromUserID = resultQuery.value("FromUserID").toInt();
			messageInfo.nToUserID = resultQuery.value("ToUserID").toInt();
			messageInfo.ClientTime = resultQuery.value("ClientTime").toLongLong();
			messageInfo.ServerTime = resultQuery.value("ServerTime").toLongLong();
			messageInfo.MsgDeliverType = (unsigned char)resultQuery.value("MsgDeliverType").toInt();
			messageInfo.MessageChildType = (short)resultQuery.value("MessageChildType").toInt();
			messageInfo.strMsg = resultQuery.value("strMsg").toString();
			messageInfo.msgID = resultQuery.value("MessageID").toByteArray();
			messageInfo.MessageState = resultQuery.value("MessageState").toInt();
			messageInfo.integral = resultQuery.value("MsgIntegral").toInt();

			//增加路径前半段
			if (messageInfo.MessageChildType == Message_PIC ||
				messageInfo.MessageChildType == Message_AUDIO ||
				messageInfo.MessageChildType == Message_VEDIO ||
				//messageInfo.MessageChildType == Message_FILE ||
				messageInfo.MessageChildType == Message_VOICE)
			{
				DealWithJsonMsg(messageInfo);
			}

			listMessageInfo.append(messageInfo);
		}
	}
	return listMessageInfo;
}

//获取搜索的分页页数
int IMSocketDataBaseShareLib::GetSearchMessagePageNum(int nChatID, QString strLikeMsg, int nPageNum)
{
	mSearchPageNum = nPageNum;
	mSearchMsg = strLikeMsg;
	//改为只搜索纯文本消息
	strSql = QString("select count(*) from MESSAGEINFO where ChatID = %1 and MessageChildType = %2 and strMsg like \"%%3%\"").arg(QString::number(nChatID),QString::number(MessageType::Message_TEXT),strLikeMsg);
	if (mSocketDB != NULL && mSocketDB->IsOpen())
	{
		QSqlQuery resultDetQuery = mSocketDB->ExecuQuery(strSql);
		int nIndex = 0;

		while (resultDetQuery.next())
		{
			nIndex = resultDetQuery.value("count(*)").toInt();
		}

		if (nIndex == 0)
		{
			return 0;
		}

		if (nPageNum != 0)
		{
			if (nIndex <= nPageNum)
			{
				return 1;
			}
			else
			{
				if (nIndex%nPageNum != 0)
				{
					return nIndex / nPageNum + 1;
				}
				else
					return nIndex / nPageNum;
			}
		}
	}
	return 0;
}

//根据页数获取搜索的结果
QList<MessageInfo> IMSocketDataBaseShareLib::GetDBSearchMessageRecordByPage(int nChatID, int nPage)
{
	if (mSearchPageNum == 0)
	{
		mSearchPageNum = 20;
	}

	//只搜索文本消息
	strSql = QString("select * from MESSAGEINFO where MessageChildType = %1 and ChatID = %2 and strMsg like \"%%3%\" ORDER BY date(ClientTime/1000,'unixepoch', 'localtime') DESC LIMIT %4 OFFSET %5").arg(QString::number(MessageType::Message_TEXT), QString::number(nChatID), mSearchMsg, QString::number(mPageNum), QString::number(mPageNum*nPage));

	QList<MessageInfo> listMessageInfo;
	if (mSocketDB != NULL && mSocketDB->IsOpen())
	{
		QSqlQuery resultQuery = mSocketDB->ExecuQuery(strSql);

		while (resultQuery.next())
		{
			MessageInfo messageInfo;
			messageInfo.nFromUserID = resultQuery.value("FromUserID").toInt();
			messageInfo.nToUserID = resultQuery.value("ToUserID").toInt();
			messageInfo.ClientTime = resultQuery.value("ClientTime").toLongLong();
			messageInfo.MsgDeliverType = (unsigned char)resultQuery.value("MsgDeliverType").toInt();
			messageInfo.MessageChildType = (short)resultQuery.value("MessageChildType").toInt();
			messageInfo.strMsg = resultQuery.value("strMsg").toString();
			messageInfo.msgID = resultQuery.value("MessageID").toByteArray();
			messageInfo.MessageState = resultQuery.value("MessageState").toInt();
			messageInfo.integral = resultQuery.value("MsgIntegral").toInt();

			//增加路径前半段
			if (messageInfo.MessageChildType == Message_PIC ||
				messageInfo.MessageChildType == Message_AUDIO ||
				messageInfo.MessageChildType == Message_VEDIO ||
				//messageInfo.MessageChildType == Message_FILE ||
				messageInfo.MessageChildType == Message_VOICE)
			{
				DealWithJsonMsg(messageInfo);
			}

			listMessageInfo.append(messageInfo);
		}
	}
	return listMessageInfo;
}

//获取最后一条消息
QString IMSocketDataBaseShareLib::DBGetMessageLastTime(int nIMUserID)
{
	QString strTemp;
	if (mSocketDB != NULL && mSocketDB->IsOpen())
	{
		//strSql = QString("select max(ServerTime) from MESSAGEINFO where FromUserID <> %1").arg(nIMUserID);
		QString strSql = QString("select max(ServerTime) from MESSAGEINFO");
		QSqlQuery resultDetQuery = mSocketDB->ExecuQuery(strSql);
		while (resultDetQuery.next())
		{
			strTemp = resultDetQuery.value("max(ServerTime)").toString();
		}
	}
	return strTemp;
}

void IMSocketDataBaseShareLib::ChangeOldPath()//替换固定路径
{
#ifdef Q_OS_WIN
	QString strExePath = QDir::currentPath();
#else
	QString strExePath = getResourcePath();
#endif

	QString strSql1 = QString(QString("update MESSAGEINFO set  strMsg =replace(strMsg,'%1','') where ServerTime != 0 and MessageChildType in (1,2,4,5)" ).arg(strExePath));
	if (mSocketDB != NULL && mSocketDB->IsOpen())
	{
		mSocketDB->ExecuQuery(strSql1);
	}
}

void IMSocketDataBaseShareLib::ClearUserData()
{
	QString strSql1 = QString(QString("delete from MESSAGEINFO"));
	if (mSocketDB != NULL && mSocketDB->IsOpen())
	{
		mSocketDB->ExecuQuery(strSql1);
	}
}

void IMSocketDataBaseShareLib::DeleteByMsgId(QString strMsgId)
{
	QString strSql1 = QString(QString("delete from MESSAGEINFO where MessageID = '%1'").arg(strMsgId));
	if (mSocketDB != NULL && mSocketDB->IsOpen())
	{
		mSocketDB->ExecuQuery(strSql1);
	}
}

void IMSocketDataBaseShareLib::DealWithJsonMsg(MessageInfo &megInfo)
{   
	QJsonDocument doc = QJsonDocument::fromJson(megInfo.strMsg.toUtf8());
	QVariantMap map = doc.toVariant().toMap();
	QString path = map.value("path").toString();
	if (!path.isEmpty())
	{
		megInfo.strMsg = path;
		if (path.left(1) == "/")
        {
            if(!megInfo.strMsg.contains(QDir::homePath()))
            {
                megInfo.strMsg = m_strUserPath + megInfo.strMsg;
            }
        }
	}
	else
	{
		if (megInfo.strMsg.left(1) == "/")
        {
            if(!megInfo.strMsg.contains(QDir::homePath()))
                megInfo.strMsg = m_strUserPath + megInfo.strMsg;
        }
	}
}

void IMSocketDataBaseShareLib::setUserPath(QString strPath)
{
	m_strUserPath = strPath;
}


bool IMSocketDataBaseShareLib::IsLastMsgIdFailure(QString myUserId, QString chatBuddyOrGroupId)
{
	QString strSql = QString("select * from MESSAGEINFO where UserID = %1 and ChatID=%2 and FromUserID!=0 order by rowid desc limit 1 ").arg(myUserId).arg(chatBuddyOrGroupId);
	if (mSocketDB != NULL && mSocketDB->IsOpen())
	{
		QSqlQuery resultQuery = mSocketDB->ExecuQuery(strSql);
		if (resultQuery.next())
		{
			int messageState = resultQuery.value("MessageState").toInt();
			if (messageState == MESSAGE_STATE_FAILURE)
			{
				return true;
			}
		}
	}

	return false;
}


QString IMSocketDataBaseShareLib::RestoreLocalPathInMessageInfo(QByteArray msgID, QString localPath)
{
	bool isFailed;
	QString strSql = QString("update MESSAGEINFO set strMsg = '%1' where MessageID = '%2'").arg(localPath).arg(QString(msgID));
	if (mSocketDB != NULL && mSocketDB->IsOpen())
	{
		isFailed = mSocketDB->ExecuSql(strSql);
	}
	return NULL;
}

QString IMSocketDataBaseShareLib::GetLastSysMsgInMessageList(int msgID, QString &fromUser, QString &toUser, int msgType)
{
	QString strSql = QString("SELECT * FROM MESSAGEINFO WHERE ChatID = '%1' AND MessageChildType = %2 ORDER BY ROWID DESC LIMIT 1")
		.arg(QString::number(msgID)).arg(msgType);
	if (mSocketDB != NULL && mSocketDB->IsOpen())
	{
		QSqlQuery resultQuery = mSocketDB->ExecuQuery(strSql);
		while(resultQuery.next())
		{
			QString strMsg = resultQuery.value("strMsg").toString();
			fromUser = resultQuery.value("FromUserID").toString();
			toUser = resultQuery.value("ToUserID").toString();
			return strMsg;
			break;
		}
	}
	return  NULL;
}

int IMSocketDataBaseShareLib::GetFromUserOfLastMsgInMessageList(int chatID, int messageType)
{
	QString strSql = QString("SELECT * FROM MESSAGEINFO WHERE ChatID = '%1' AND MessageChildType = %2 ORDER BY ROWID DESC LIMIT 1")
		.arg(QString::number(chatID)).arg(messageType);

	if (mSocketDB != NULL && mSocketDB->IsOpen())
	{
		QSqlQuery resultQuery = mSocketDB->ExecuQuery(strSql);
		while (resultQuery.next())
		{
			int fromUser = resultQuery.value("FromUserID").toInt();
			return fromUser;
		}
	}
	return 0;
}
