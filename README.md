<p align="center">
<img src="./assets/ipcomm_icon.png" width="150" />
</p>
<h1 align="center">IPComm星际通讯</h1>
<p align="center">
    <a href="https://ipcom.io">
        <img src="https://img.shields.io/badge/Licence-GPL3.0-green.svg?style=flat" />
    </a>
    </p>
<p align="center">    
    <b>如果对您有帮助，您可以点右上角 "Star" 支持一下 谢谢！</b>
</p>

### 项目介绍
星际通讯是基于区块链的价值共享互联网即时通讯应用平台，是一个去中心化的任何人都可以使用的通讯网络，是一款基于区块链的价值共享互联网即时通讯APP。星际通讯系统为人与设备、人与人、人与服务、服务与设备等提供高效、稳定、即时的网络通讯服务。通过区块链加密存储技术帮助您管理数字资产，支持即时消息通讯、离线消息；并支持文字、图片、语音、视频、表单及自定义消息类型。
完全由原生代码实现了单聊、群聊、公众号等聊天功能。聊天格式支持文字、表情、图片、视频、文件等常规内容，更拓展支持加密文本、加密图片等加密内容。

### 下载使用
* 下载使用时，需要先安装移动端星际通信APP，进行扫码登录。移动版星际通信下载地址： 
  [下载](https://gitee.com/IPComm/IPCommAndroid/attach_files/344175/download) ，或直接访问相关[开源项目](https://gitee.com/IPComm)
* Windows下载地址: [下载](https://ipcomefile.openserver.cn/ipcom/windows/ipcom1.1.48.exe) 
* MacOS下载地址：  [下载](https://ipcomefile.openserver.cn/ipcom/mac/ipcom.dmg) 
* Centos下载地址： [下载](https://ipcomefile.openserver.cn/ipcom/linux/ipcom.AppImage) 
* Ubuntu下载地址:  [下载](https://ipcomefile.openserver.cn/ipcom/linux/ipcom_ubuntu.AppImage) 

### 主要特点
#### 聊天
- 支持单聊、群聊、公众号等聊天功能；
- 支持文字、表情、图片、视频、文件等普通聊天格式；
- 支持加密文字、加密图片等加密内容；
- 支持语音实时输入发送（“光速短信”）；
- 数字货币红包；
- 支持公众号消息的推送服务；
#### 其他
- 数字货币的资产管理、转账功能；
- 支持多语言
- 支持换肤

### 运行环境
- VS2015、Qt5.10.1、QtCreator


### 界面展示

<img src="./assets/chat.png" width="300" />

<img src="./assets/buddy.png" width="300" />

<img src="./assets/zc.png" width="300" />

<img src="./assets/pc1.png" width="300" />

<img src="./assets/pc2.png" width="300" />

<img src="./assets/pc3.png" width="300" />


### 版权声明
本软件使用 GPL3.0 协议，请严格遵照协议内容!

### 合作及联系
- QQ交流群: 976048137

- 联系邮箱：app@turbochain.ai
<img src="./assets/qq_ipcomm.jpg" width="300" />

### End