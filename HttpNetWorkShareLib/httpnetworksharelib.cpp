﻿#include "httpnetworksharelib.h"
#include <QTextCodec>
#include <QDir>
#include <QHttpMultiPart>
#include "QStringLiteralBak.h"
#include "imusermessage.h"

//#define HTTP_MESSAGE_DEBUG_ENABLE  //是否记录HTTP内容

namespace HttpNetWork
{
	HttpNetWorkShareLib::HttpNetWorkShareLib(QObject *parent)
	: QObject(parent)
	{
		mReply = NULL;
		m_pTimer = NULL;
	}

	HttpNetWorkShareLib::~HttpNetWorkShareLib()
	{
		if (mReply != NULL)
		{
			if (!mReply->isFinished())
			{
				mReply->abort();
			}
			mReply->deleteLater();
		}
		if (m_pTimer)
		{
			if (m_pTimer->isActive())
			{
				m_pTimer->stop();
			}
			m_pTimer->deleteLater();
		}
	}

	void HttpNetWorkShareLib::getHttpRequest(QString strUrl, int time_out)
	{
#ifdef HTTP_MESSAGE_DEBUG_ENABLE
		qDebug().noquote() << "========getHttpRequest=======>>" << strUrl;
#endif

		QUrl url(strUrl);
		QNetworkRequest request;
		request.setHeader(QNetworkRequest::ContentTypeHeader, tr("application/x-www-form-urlencoded"));
		request.setUrl(url); //地址信息
		if (strUrl.toLower().startsWith(tr("https")))//https请求，需ssl支持(下载openssl拷贝libeay32.dll和ssleay32.dll文件至Qt bin目录或程序运行目录)
		{
			QSslConfiguration sslConfig;
			sslConfig.setPeerVerifyMode(QSslSocket::VerifyNone);
			sslConfig.setProtocol(QSsl::TlsV1_1);
			request.setSslConfiguration(sslConfig);
		}
		QNetworkAccessManager *accessManager = new QNetworkAccessManager(this);
		m_pTimer = new QTimer(this);
		connect(m_pTimer, SIGNAL(timeout()), this, SLOT(slotRequestTimeout()));//超时信号
		mReply = accessManager->get(request);
		connect(accessManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(slotReplyFinished(QNetworkReply*)));
		m_pTimer->start(time_out);
	}

	void HttpNetWorkShareLib::postHttpRequest(QString strUrl, QByteArray data, int time_out)
	{
#ifdef HTTP_MESSAGE_DEBUG_ENABLE
		qDebug().noquote() << "========postHttpRequest=======>>" << strUrl<<" , data="<<data;
#endif

		QUrl url(strUrl);
		QNetworkRequest request;
		request.setHeader(QNetworkRequest::ContentTypeHeader, tr("application/x-www-form-urlencoded"));
		request.setUrl(url); //地址信息
		if (strUrl.toLower().startsWith(tr("https")))//https请求，需ssl支持(下载openssl拷贝libeay32.dll和ssleay32.dll文件至Qt bin目录或程序运行目录)
		{
			QSslConfiguration sslConfig;
			sslConfig.setPeerVerifyMode(QSslSocket::VerifyNone);
			sslConfig.setProtocol(QSsl::TlsV1_1);
			request.setSslConfiguration(sslConfig);
		}
		QNetworkAccessManager *accessManager = new QNetworkAccessManager(this);
		m_pTimer = new QTimer(this);
		connect(m_pTimer, SIGNAL(timeout()), this, SLOT(slotRequestTimeout()));//超时信号
		mReply = accessManager->post(request, data);
		connect(accessManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(slotReplyFinished(QNetworkReply*)));
		m_pTimer->start(time_out);
	}

	void HttpNetWorkShareLib::postHttpJsonRequest(QString strUrl, QByteArray data, int time_out /*= 20000*/)
	{
		QUrl url(strUrl);
		QNetworkRequest request;
		request.setHeader(QNetworkRequest::ContentTypeHeader, tr("application/json"));
		request.setUrl(url); //地址信息
		if (strUrl.toLower().startsWith(tr("https")))//https请求，需ssl支持(下载openssl拷贝libeay32.dll和ssleay32.dll文件至Qt bin目录或程序运行目录)
		{
			QSslConfiguration sslConfig;
			sslConfig.setPeerVerifyMode(QSslSocket::VerifyNone);
			sslConfig.setProtocol(QSsl::TlsV1_1);
			request.setSslConfiguration(sslConfig);
		}
		QNetworkAccessManager *accessManager = new QNetworkAccessManager(this);
		m_pTimer = new QTimer(this);
		connect(m_pTimer, SIGNAL(timeout()), this, SLOT(slotRequestTimeout()));//超时信号
		mReply = accessManager->post(request, data);
		connect(accessManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(slotReplyFinished(QNetworkReply*)));
		m_pTimer->start(time_out);
	}

	void HttpNetWorkShareLib::slotReplyFinished(QNetworkReply*reply)
	{
		if (m_pTimer)
		{
			m_pTimer->stop();
		}
		if (reply->error() == QNetworkReply::NoError)
		{
			QByteArray resultContent = mReply->readAll();
			QTextCodec* pCodec = QTextCodec::codecForName("UTF-8");
			QString strResult = pCodec->toUnicode(resultContent);
			emit sigReplyFinished(true, strResult);

#ifdef HTTP_MESSAGE_DEBUG_ENABLE
			qDebug().noquote() << "========http reply=======>>" << strResult;
#endif
		}
		else
		{
			emit sigReplyFinished(false, tr("relpay erro!"));
		}
		mReply->deleteLater();
		mReply = NULL;

		this->deleteLater();
	}
	//请求超时
	void HttpNetWorkShareLib::slotRequestTimeout()
	{
		m_pTimer->stop();//关闭定时器
		mReply->disconnect();
		mReply->abort();
		mReply->deleteLater();
		mReply = NULL;
		emit sigReplyFinished(false, tr("timeout"));
		this->deleteLater();
	}

	/***************************************************文件下载类***************************************/
#include <QFileInfo>
#include <QDateTime>
	HttpDownLoadFile::HttpDownLoadFile(QObject *parent)
		: QObject(parent)
	{
		mReply = NULL;
		m_timer = NULL;
	}

	HttpDownLoadFile::~HttpDownLoadFile()
	{
		if (m_timer)
		{
			if (m_timer->isActive())
			{
				m_timer->stop();
			}
			m_timer->deleteLater();
		}
	}

	bool HttpDownLoadFile::StartDownLoadFile(QString strUrl, QString strFilePath)
	{
		if (strUrl.isEmpty())
		{
			emit sigDownFinished(false);
			return false;
		}
		QUrl url(strUrl);

		if (strFilePath.contains("/"))
		{
			int nLocal = strFilePath.lastIndexOf("/");
			QString strPath = strFilePath.mid(0, nLocal);
			QDir *dir = new QDir;
			if (!dir->exists(strPath))
			{
				if (!dir->mkpath(strPath))
				{
					qDebug() << tr("图片文件夹创建失败!");
				}
			}
		}
		
		file = new QFile(strFilePath);
		if (file->open(QIODevice::WriteOnly))
		{
			accessManager = new QNetworkAccessManager(this);
			mReply = accessManager->get(QNetworkRequest(url));
			connect(accessManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));
			connect(mReply, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
			connect(mReply, SIGNAL(downloadProgress(qint64, qint64)), this, SLOT(onDownloadProgress(qint64, qint64)));
			m_timer = new QTimer();
			connect(m_timer, SIGNAL(timeout()), this, SLOT(slotRequestTimeout()));//超时信号
			m_timer->start(30000);
		}
		else
		{
			return false;
		}
		return true;
	}
	void HttpDownLoadFile::slotRequestTimeout()
	{
		m_timer->stop();//关闭定时器
		mReply->disconnect();
		mReply->abort();
		mReply->deleteLater();
		mReply = NULL;
		emit sigDownFailed();
		this->deleteLater();
	}

	void HttpDownLoadFile::replyFinished(QNetworkReply* reply)
	{
		if (m_timer)
		{
			m_timer->stop();
		}
		if (file)
		{
			file->flush();
			file->close();
			file = NULL;
			delete file;
		}
		if (reply->error() == QNetworkReply::NoError)
		{
			mReply->deleteLater();
			mReply = NULL;
			emit sigDownFinished(true);
		}
		else
		{
			reply->deleteLater();
			reply = NULL;
			emit sigDownFinished(false);
		}

		this->deleteLater();
	}

	void HttpDownLoadFile::onDownloadProgress(qint64 bytesSent, qint64 bytesTotal)
	{
		m_timer->stop();
		m_timer->start(30000);
		emit sigDownloadProgress(bytesSent, bytesTotal);
	}

	void HttpDownLoadFile::slotLoadorDownLoadCancle(QString MsgId)
	{

		QVariant var = this->getData();
		MessageInfo msgInfo = var.value<MessageInfo>();
		if (msgInfo.msgID != MsgId)
		{
			return;
		}

		if (mReply != NULL)
		{
			if (!mReply->isFinished())
			{
				mReply->abort();
			}
			mReply->deleteLater();
		}
		if (file)
		{
			file->flush();
			file->close();
			file = NULL;
			delete file;
		}
		emit sigDownloadProgress(0, 0); 

		this->deleteLater();
	}

	void HttpDownLoadFile::onReadyRead()
	{
		if (file)
		{
			file->write(mReply->readAll());
			file->flush();
		}
	}

	/*************************************************文件上传类***************************************/
	HttpUpLoadFile::HttpUpLoadFile(QObject *parent)
		: QObject(parent)
	{
		mReply = NULL;
	}
	HttpUpLoadFile::~HttpUpLoadFile()
	{
		if (mReply != NULL)
		{
			if (!mReply->isFinished())
			{
				mReply->abort();
			}
			mReply->deleteLater();
		}
	}

	void HttpUpLoadFile::StartUpLoadFile(QString strUrl, QString filePath, QVariantMap paramMap)
	{
		QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);
		QHttpPart textPart;
		QVariantMap::iterator it; //遍历map
		for (it = paramMap.begin(); it != paramMap.end(); ++it)
		{
			QString keys = QString("form-data; name=\"%1\"").arg(it.key());

			textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant(keys));
			textPart.setBody(it.value().toString().toUtf8());
		}
		QFile *upFile = new QFile(filePath);

		QHttpPart filePart;
		QString strFileInfo = QString("form-data; name=\"file\"; filename=\"" + upFile->fileName() + "\"").toLatin1();
		filePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant(strFileInfo));
		if (upFile->open(QIODevice::ReadOnly))
		{
			filePart.setBodyDevice(upFile);
			upFile->setParent(multiPart);

			multiPart->append(textPart);
			multiPart->append(filePart);

			QNetworkRequest request;
			strUrl.replace(":80", "");
			request.setUrl(strUrl); //地址信息
			if (strUrl.toLower().startsWith("https"))//https请求，需ssl支持(下载openssl拷贝libeay32.dll和ssleay32.dll文件至Qt bin目录或程序运行目录)
			{
				QSslConfiguration sslConfig;
				sslConfig.setPeerVerifyMode(QSslSocket::VerifyNone);
				sslConfig.setProtocol(QSsl::TlsV1_1);
				request.setSslConfiguration(sslConfig);
			}
			QString b = QVariant(qrand()).toString() + QVariant(qrand()).toString() + QVariant(qrand()).toString();
			QString boundary = "---------------------------" + b;
			multiPart->setBoundary(boundary.toLatin1());
			QString contentType = "multipart/form-data; boundary=" + boundary;
			request.setHeader(QNetworkRequest::ContentTypeHeader, contentType.toLatin1());
			request.setRawHeader("UploadType", "COMM");

			QNetworkAccessManager *accessManager = new QNetworkAccessManager(this);
			mReply = accessManager->post(request, multiPart);
			multiPart->setParent(mReply);
			connect(accessManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));
			connect(mReply, SIGNAL(uploadProgress(qint64, qint64)), this, SLOT(OnUploadProgress(qint64, qint64)));
		}
	}

	void HttpUpLoadFile::replyFinished(QNetworkReply* reply)
	{
		if (mReply->error() == QNetworkReply::NoError)
		{
			QByteArray resultContent = reply->readAll();
			emit sigUpLoadFinished(true,resultContent);
		}
		else
		{
			qDebug() << "严重!!! 服务器传来消息...设置发送失败字段...";
			emit sigUpLoadFinished(false, "");
		}
		mReply->deleteLater();
		mReply = NULL;
	}

	void HttpUpLoadFile::OnUploadProgress(qint64 bytesSent, qint64 bytesTotal)
	{
		emit sigUpLoadProgress(bytesSent, bytesTotal);
	}

	void HttpUpLoadFile::slotLoadorDownLoadCancle(QString MsgId)
	{
		QVariant var = this->getData();
		MessageInfo msgInfo = var.value<MessageInfo>();
		if (msgInfo.msgID != MsgId)
		{
			return;
		}

		if (mReply != NULL)
		{
			if (!mReply->isFinished())
			{
				mReply->abort();
			}
			mReply->deleteLater();
		}
		emit sigUpLoadProgress(0, 0);
	}
}

