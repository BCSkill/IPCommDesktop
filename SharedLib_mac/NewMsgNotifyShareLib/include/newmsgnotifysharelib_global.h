﻿#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(NEWMSGNOTIFYSHARELIB_LIB)
#  define NEWMSGNOTIFYSHARELIB_EXPORT Q_DECL_EXPORT
# else
#  define NEWMSGNOTIFYSHARELIB_EXPORT Q_DECL_IMPORT
# endif
#else
# define NEWMSGNOTIFYSHARELIB_EXPORT
#endif
