﻿#include <QtQml>
#include <QQuickItem>
#include <QToolTip>
#include <QtWidgets/QSplitter>
#include "contactlist.h"
#include "QStringLiteralBak.h"

#ifdef Q_OS_MAC
#include "mycommonstyle.h"
#endif

#include <QGuiApplication>
#include "httpnetworksharelib.h"
#include "messageWidget/messagelistdispatcher.h"

extern QString gThemeStyle;

ContactList::ContactList(QWidget *parent)
	: QQuickWidget(parent), m_pCntlistModel(new ContactListModel())
{
	this->setResizeMode(QQuickWidget::SizeRootObjectToView);

	ImgProvider *imageProvider = new ImgProvider(QQmlImageProviderBase::Image);
	this->engine()->addImageProvider(QLatin1String("imageProvider"), imageProvider);

	this->rootContext()->setContextProperty("contactList", this);
	this->rootContext()->setContextProperty("contactListModel", m_pCntlistModel);
	this->setSource(QUrl("qrc:/ContactList/Resources/ContactList/ContactList.qml"));

	//切换主题样式
	if (gThemeStyle == "White")
	{
		this->changeStyle("dayStyle");
	}
	else if (gThemeStyle == "Blue")
	{
		this->changeStyle("nightStyle");
	}

	QString strColor = this->rootObject()->property("clearColor").toString();
	this->setClearColor(QColor(strColor));

	this->setMouseTracking(true);

	m_menuTimer = new QTimer(this);
	setCurrIndexFlag = true;
	updateIndexFlag = true;
#ifdef Q_OS_MAC
	this->setStyle(new MyProxyStyle);
#endif
}

ContactList::~ContactList()
{
}

void ContactList::changeStyle(QString styleName)
{
	QObject* rootItem = rootObject();
	if (rootItem)
	{
		QVariant param = styleName;
		QMetaObject::invokeMethod(rootItem, "changeStyle", Q_ARG(QVariant, param));
	}
}


void ContactList::mouseReleaseEvent(QMouseEvent *event)
{
#ifdef Q_OS_MAC
	//MAC下触摸板经常拖动偶尔导致无法正常点击，所以发射信号使其正常
	if (event->button() == Qt::LeftButton)
	{
		emit qGuiApp->applicationStateChanged(Qt::ApplicationInactive);
		emit qGuiApp->applicationStateChanged(Qt::ApplicationActive);
	}
#endif
	QQuickWidget::mouseReleaseEvent(event);
}

void ContactList::doUpDownKeyClick(bool isUp)
{
	QObject* rootItem = rootObject();
	if (rootItem)
	{
		QVariant param = isUp;
		QMetaObject::invokeMethod(rootItem, "doUpDownKeyClick", Q_ARG(QVariant, param));
	}
}

void ContactList::doClickItem(QString userId)
{
	emit sigProfile(userId);
	emit sigChangeFoucs();
}
void ContactList::doDoubleClickItem(QString userId)
{
	emit sigPerChat(OpenPer, QVariant(userId));
	emit sigChangeFoucs();
}
void ContactList::doRightClickItem(QString userId)
{
	QPoint point = QCursor::pos();
	bool isMenuFlag = false;
	int thisLine;
	QString strGroupID = userId;


	for (int i = 0; i < m_pCntlistModel->rowCount(); i++)
	{
		QString user_id = m_pCntlistModel->data(m_pCntlistModel->index(i), ContactListModel::UserIdRole).toString();
		if (user_id == userId)
		{
			isMenuFlag = true;
			thisLine = i;
			break;
		}
	}

	m_menuTimer->disconnect();
	m_menuTimer->stop();
	m_menuTimer->setSingleShot(true);

	connect(m_menuTimer, &QTimer::timeout, this, [this, thisLine, isMenuFlag, strGroupID]() {
		QCoreApplication::processEvents();
		if (isMenuFlag)
		{
			QMenu* listMenu = new QMenu(this);
			QFile style(":/QSS/Resources/QSS/ContactsWidgetShareLib/contactlist.qss");
			style.open(QFile::ReadOnly);
			QString sheet = QLatin1String(style.readAll());
			listMenu->setStyleSheet(sheet);
			style.close();
			//listMenu->setStyleSheet("QMenu{background-color: #72a4d6;color: #042439;border:none;}QMenu::item{padding:4px;padding-left:10px;padding-right:10px;}QMenu::item:selected{color: white;border:none;padding:4px;padding-left:10px;padding-right:10px;}");
			QAction *DeleteBuddy = new QAction(tr("Delete Friend"), this);
			connect(DeleteBuddy, SIGNAL(triggered()), this, SLOT(slotDeleteBuddy()));
			listMenu->addAction(DeleteBuddy);

			if (listMenu && thisLine >= 0)
			{
				listMenu->exec(QCursor::pos());
			}
		}
		//TODO 这里弹菜单
	});
	m_menuTimer->start(0);
	emit sigChangeFoucs();
}

bool ContactList::OnJudgeIsListTitle(QString strTitle)
{
	if (strTitle == "A" || strTitle == "B" || strTitle == "C" || strTitle == "D" ||
		strTitle == "E" || strTitle == "F" || strTitle == "G" || strTitle == "H" ||
		strTitle == "I" || strTitle == "J" || strTitle == "K" || strTitle == "L" ||
		strTitle == "M" || strTitle == "N" || strTitle == "O" || strTitle == "P" ||
		strTitle == "Q" || strTitle == "R" || strTitle == "S" || strTitle == "T" ||
		strTitle == "U" || strTitle == "V" || strTitle == "W" || strTitle == "X" ||
		strTitle == "Y " || strTitle == "Z")
	{
		return true;
	}
	return false;
}

void ContactList::OnInsertContactsInfo(BuddyInfo buddyInfo)
{
	//非好友直接退出
	if (buddyInfo.BuddyType == 0)
		return;

	//增加对已有的检测
	for (int i = 0; i < m_pCntlistModel->rowCount(); i++)
	{
		QString userId = m_pCntlistModel->data(m_pCntlistModel->index(i), ContactListModel::UserIdRole).toString();
		int iUserId = userId.toInt();
		if (iUserId == buddyInfo.nUserId)
		{
			return;
		}
	}


	bool has = false;  //判断联系人列表是否已有新联系人字母的标志变量。
	int letter = 0;
	if (buddyInfo.strPingYin.isEmpty())
	{
		letter = 35;//'#'
	}
	else
	{
		int c = (int)buddyInfo.strPingYin.at(0).toLatin1();
		if (65 <= c&&c <= 90)
		{
			letter = (int)buddyInfo.strPingYin.at(0).toLatin1();  //新联系人的ascii码值，用于比较字母顺序。
		}
		else
		{
			letter = 35;
		}
	}


	int place = -1;    //如果当前列表没有新联系人的字母，使用该变量保存新联系人字母要插入的位置。
	bool poundSignFlag = true;

	if (letter == 35)
	{
		for (int i = 0; i < m_pCntlistModel->rowCount(); i++)
		{
			QString titleChrct = m_pCntlistModel->data(m_pCntlistModel->index(i), ContactListModel::TitleCharacterRole).toString();
			if (titleChrct == 35)
			{
				place = i;
				poundSignFlag = false;
				break;
			}
		}
		if (poundSignFlag && m_pCntlistModel->rowCount() > 0)
		{
			place = m_pCntlistModel->rowCount()-1;
		}
	}


	for (int i = 0; i < m_pCntlistModel->rowCount(); i++)
	{
		QString titleChrct = m_pCntlistModel->data(m_pCntlistModel->index(i), ContactListModel::TitleCharacterRole).toString();

		if (titleChrct == letter)//buddyInfo.strPingYin)
		{
			QString strBuddyID = QString("%1").arg(buddyInfo.nUserId);

			if (!buddyInfo.strNote.isEmpty())
			{
				buddyInfo.strNickName = buddyInfo.strNote;
			}

			this->OnInsertContactsofIndex(buddyInfo, (i + 1));
			has = true;
			break;
		}
		else
		{
			QString strTemp = m_pCntlistModel->data(m_pCntlistModel->index(i), ContactListModel::TitleCharacterRole).toString();
			int curLetter = 0;
			if (!strTemp.isEmpty())
			{
				curLetter = strTemp.at(0).toLatin1();
			}
			if (curLetter == 35)
				break;
			if (letter > curLetter)
				place = i;
		}
	}


	if (has == false)
	{
		//插入新字母，新联系人。
		char cletter = letter;
		QString strLetter;
		strLetter.append(cletter);
		ContactListItem item(HEAD, HEAD, HEAD, HEAD, strLetter);// buddyInfo.strPingYin);
		m_pCntlistModel->insertItem(place + 1, &item);

		//再插入联系人。
		QString strBuddyID = QString("%1").arg(buddyInfo.nUserId);
		if (!buddyInfo.strNote.isEmpty())
		{
			buddyInfo.strNickName = buddyInfo.strNote;
		}
		this->OnInsertContactsofIndex(buddyInfo, (place + 2));
	}
}

void ContactList::OnInsertContactsofIndex(BuddyInfo info, int nIndex, int nHeight /*= 62*/)
{
	QString strPicPath = info.strLocalAvatar;
	if (!IsValidImage(info.strLocalAvatar))
	{
		strPicPath = QStringLiteral(":/PerChat/Resources/person/temp.png");
	}
	QString headPicPath = QStringLiteral("image://imageProvider/") + strPicPath;


	ContactListItem item(QString::number(info.nUserId),
		headPicPath,
		info.strNickName,
		ITEM,
		info.strPingYin);
	m_pCntlistModel->insertItem(nIndex, &item);
	m_pCntlistModel->setData(m_pCntlistModel->index(nIndex), headPicPath, ContactListModel::HeadUrlRole);

	if (setCurrIndexFlag)
	{
		currIndex = nIndex;
	}
	if (updateIndexFlag)
	{
		QString currUserId = m_pCntlistModel->data(m_pCntlistModel->index(currIndex), ContactListModel::UserIdRole).toString();
		emit sigDefaultSelectedId(currUserId);
		setCurrIndexFlag = false;
		this->setCurrentIndex(currIndex);
	}
}

void ContactList::UpdateBuddyImage(QString userID, QString imgPath)
{
	for (int i = 0; i < m_pCntlistModel->rowCount(); i++)
	{
		QString userId = m_pCntlistModel->data(m_pCntlistModel->index(i), ContactListModel::UserIdRole).toString();
		if (userID == userId)
		{
			QString headPicPath = QStringLiteral("image://imageProvider/") + imgPath;
			m_pCntlistModel->setData(m_pCntlistModel->index(i), headPicPath, ContactListModel::HeadUrlRole);
		}
	}
}

void ContactList::OnUpdateBuddyInfo(BuddyInfo buddyInfo)
{
	for (int i = 0; i < m_pCntlistModel->rowCount(); i++)
	{
		QString userId = m_pCntlistModel->data(m_pCntlistModel->index(i), ContactListModel::UserIdRole).toString();
		int iUserId = userId.toInt();
		if (iUserId == buddyInfo.nUserId)
		{
			//先删除后增加
			m_pCntlistModel->takeItem(i);

			if (i == m_pCntlistModel->rowCount())
			{
				if (m_pCntlistModel->data(m_pCntlistModel->index(i - 1), ContactListModel::MsgTypeRole).toString() == HEAD)
					m_pCntlistModel->takeItem(i - 1);
			}
			else
			{

				if (m_pCntlistModel->data(m_pCntlistModel->index(i - 1), ContactListModel::MsgTypeRole).toString() == HEAD
					&&m_pCntlistModel->data(m_pCntlistModel->index(i), ContactListModel::MsgTypeRole).toString() == HEAD)
					m_pCntlistModel->takeItem(i - 1);
			}
			updateIndexFlag = false;
			OnInsertContactsInfo(buddyInfo);


			//刷新头像
			for (int i = 0; i < m_pCntlistModel->rowCount(); i++)
			{
				QString refreshId = m_pCntlistModel->data(m_pCntlistModel->index(i), ContactListModel::UserIdRole).toString();
				int iRefreshId = refreshId.toInt();
				if (iRefreshId == buddyInfo.nUserId)
				{
					QString strPicPath = buddyInfo.strLocalAvatar;
					if (!IsValidImage(buddyInfo.strLocalAvatar))
					{
						strPicPath = QStringLiteral(":/PerChat/Resources/person/temp.png");
					}
					QString headPicPath = QStringLiteral("image://imageProvider/") + strPicPath;
					m_pCntlistModel->setData(m_pCntlistModel->index(i), headPicPath, ContactListModel::HeadUrlRole);
				}
			}
			updateIndexFlag = true;
			break;
		}
	}
}

void ContactList::deleteFriend(QString userID)
{
	for (int i = 0; i < m_pCntlistModel->rowCount(); i++)
	{
		QString userId = m_pCntlistModel->data(m_pCntlistModel->index(i), ContactListModel::UserIdRole).toString();
		if (userId == userID)
		{
			//先删除后增加
			m_pCntlistModel->takeItem(i);

			if (i == m_pCntlistModel->rowCount())
			{
				if (m_pCntlistModel->data(m_pCntlistModel->index(i - 1), ContactListModel::MsgTypeRole).toString() == HEAD)
					m_pCntlistModel->takeItem(i - 1);
			}
			else
			{

				if (m_pCntlistModel->data(m_pCntlistModel->index(i - 1), ContactListModel::MsgTypeRole).toString() == HEAD
					&&m_pCntlistModel->data(m_pCntlistModel->index(i), ContactListModel::MsgTypeRole).toString() == HEAD)
					m_pCntlistModel->takeItem(i - 1);
			}
			setCurrentIndex(1);
			QString userId = m_pCntlistModel->data(m_pCntlistModel->index(1), ContactListModel::UserIdRole).toString();
			emit sigProfile(userId);
			break;
		}
	}
}


void ContactList::slotUpdateBuddyInfo(BuddyInfo buddyInfo)
{
	OnUpdateBuddyInfo(buddyInfo);
}

void ContactList::slotDeleteBuddy()
{
	IMessageBox* pBox = new IMessageBox(this);
	pBox->initAsk(tr("Notice"), tr("Are you sure to delete this contact?"));
	connect(pBox, SIGNAL(sigOK()), SLOT(slotDoDeleteBuddy()));
}

void ContactList::slotDoDeleteBuddy()
{
	//获取个人ID
	UserInfo info = gDataManager->getUserInfo();
	QString strID;
	//获取部落ID
	int rowNum = this->getCurrentIndex();

	for (int i = 0; i < m_pCntlistModel->rowCount(); i++)
	{
		if (i == rowNum)
			strID = m_pCntlistModel->data(m_pCntlistModel->index(i), ContactListModel::UserIdRole).toString();
	}


	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
	http->setData(QVariant(strID));
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotCheckHttp(bool, QString)));
	QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/user/deleteFriend"
		+ QString("?userId=%1&passWord=%2&friendUserId=%3").arg(info.nUserID).arg(info.strUserPWD).arg(strID);

	http->getHttpRequest(url);

	//同步消息列表
	emit MessageListDispatcher::instance()->sigCloseChat(strID);
}

void ContactList::slotCheckHttp(bool bSuccess, QString strValue)
{
	HttpNetWork::HttpNetWorkShareLib *http = qobject_cast<HttpNetWork::HttpNetWorkShareLib *>(sender());
	if (http)
	{
		if (bSuccess&&strValue.contains("success"))
		{
			//成功后手动删除本地数据库和list 防止收不到服务器的删除消息
			QString strBuddyId = http->getData().toString();
			deleteFriend(strBuddyId);
			gDataBaseOpera->DBDeleteBuddyInfoByID(strBuddyId);
		}
		else
		{
			//提示失败
			IMessageBox::tip(this, tr("Warning"), tr("Network request failed!"));
		}
		http->deleteLater();
	}
	return;
}

void ContactList::slotConChangeFoucs()
{
	//将currentIndex设为-1以跳过悬浮色显示
	QObject* listViewGroupList = this->rootObject()->findChild<QObject*>("listViewContactList");
	listViewGroupList->setProperty("currentIndex", -1);
}
void ContactList::slotNewFriendItemClicked()
{
	QObject* listViewGroupList = this->rootObject()->findChild<QObject*>("listViewContactList");
	listViewGroupList->setProperty("currentIndex", -1);
	emit sigCanclePerProfileWid();
}


int ContactList::getCurrentIndex()
{
	QObject* listViewContactList = this->rootObject()->findChild<QObject*>("listViewContactList");
	return listViewContactList->property("currentIndex").toInt();
}

bool ContactList::IsValidImage(QString imgPath)
{
	QFile file(imgPath);
	if (file.open(QIODevice::ReadOnly))
	{
		QByteArray byteArray = file.readAll();
		file.close();
		QImage image = QImage::fromData(byteArray);

		if (image.isNull())
		{
			return false;
		}
	}
	else
	{
		return false;
	}

	return true;
}
void ContactList::setCurrentIndex(int index)
{
	QVariant qvIndex(index);
	QObject* listViewContactList = this->rootObject()->findChild<QObject*>("listViewContactList");
	listViewContactList->setProperty("currentIndex", qvIndex);
}

void ContactList::showEvent(QShowEvent *event)
{
	//获取焦点以捕获键盘事件
	this->setFocus();
}


void ContactList::keyPressEvent(QKeyEvent * event)
{
	int currIndex;
	QObject* listViewContactList = this->rootObject()->findChild<QObject*>("listViewContactList");
	currIndex = listViewContactList->property("currentIndex").toInt();
	if (event->key() == Qt::Key_Up)
	{
		if (currIndex <= 0)
			return;
		else
		{
			for (int i = currIndex - 1; i >= 0; i--)
			{

				QString msgType = m_pCntlistModel->data(m_pCntlistModel->index(i), ContactListModel::MsgTypeRole).toString();
				if (msgType == HEAD)
				{
					continue;
				}
				else if (msgType == ITEM)
				{
					listViewContactList->setProperty("currentIndex", i);
					QString currUserId = m_pCntlistModel->data(m_pCntlistModel->index(i), ContactListModel::UserIdRole).toString();
					emit sigDefaultSelectedId(currUserId);
					break;
				}
			}
		}
	}
	if (event->key() == Qt::Key_Down)
	{
		if (currIndex <= 0)
			return;
		else
		{
			for (int i = currIndex + 1; i < m_pCntlistModel->rowCount(); i++)
			{
				QString msgType = m_pCntlistModel->data(m_pCntlistModel->index(i), ContactListModel::MsgTypeRole).toString();
				if (msgType == HEAD)
					continue;
				else if (msgType == ITEM)
				{
					listViewContactList->setProperty("currentIndex", i);
					QString currUserId = m_pCntlistModel->data(m_pCntlistModel->index(i), ContactListModel::UserIdRole).toString();
					emit sigDefaultSelectedId(currUserId);
					break;
				}
			}
		}
	}

	if (event->key() == Qt::Key_Return)
	{
		QString currUserId = m_pCntlistModel->data(m_pCntlistModel->index(currIndex), ContactListModel::UserIdRole).toString();
		emit sigPerChat(OpenPer, QVariant(currUserId));
	}

	return QWidget::keyPressEvent(event);
}
void ContactList::setUpdateIndexFlag(bool bVal)
{
	updateIndexFlag = bVal;
}

