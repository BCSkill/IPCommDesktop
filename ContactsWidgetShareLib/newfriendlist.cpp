﻿#include "newfriendlist.h"
#include "httpnetworksharelib.h"
#include "interplanetnoticewidget.h"

#ifdef Q_OS_MAC
#include "mycommonstyle.h"
#endif

NewFriendList::NewFriendList(QWidget *parent)
: QListWidget(parent)
{
	connect(this, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(doDoubleClickedItem(QListWidgetItem *)));
	connect(this, SIGNAL(itemClicked(QListWidgetItem *)), this, SLOT(doClickedItem(QListWidgetItem *)));

	this->setContextMenuPolicy(Qt::CustomContextMenu);
	QFile scroolbar_style_qss(":/QSS/Resources/QSS/scrollbarStyle.qss");
	scroolbar_style_qss.open(QFile::ReadOnly);
	this->verticalScrollBar()->setStyleSheet(scroolbar_style_qss.readAll());
	this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	this->setMouseTracking(true);

	//加载样式
	QFile file(":/QSS/Resources/QSS/listWidgetStyle.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

#ifdef Q_OS_MAC
	this->setStyle(new MyProxyStyle);
#endif
	int iparW = parent->width();
	this->setFixedWidth(iparW);
 	//buddy = new CFrientItemMessage(this);
	buddy = new InterPlanetNoticeWidget(this);
	//buddy->resize(this->width(), 58);
	//buddy->OnInitMessage(tr("InterPlanet Notice"));
	buddy->OnSetPicPath(":/GroupChat/Resources/groupchat/newfriend.png");
	buddy->OnSetNickNameText(tr("OpenPlanet Notice"));
	buddy->OnSetMessageNum("");

	QListWidgetItem *newItem = new QListWidgetItem(); //创建一个newItem
	newItem->setData(Qt::UserRole, QVariant(QString::number(1)));
	newItem->setSizeHint(QSize(0, 58));
	this->insertItem(0, newItem); //将该newItem插入到后面
	this->setItemWidget(newItem, buddy); //将buddy赋给该newItem
	//this->insertItem(1, newItem); //将该newItem插入到后面
	//this->setItemWidget(newItem, buddy); //将buddy赋给该newItem
	this->setFixedHeight(58);
}

NewFriendList::~NewFriendList()
{
	//if (listMenu)
	//	delete listMenu;
}

void NewFriendList::keyPressEvent(QKeyEvent * event)
{
	if (event->key() == Qt::Key_Up)
	{
		if (this->count() > 0)
		{
			QListWidgetItem *item = NULL;
			for (int i = this->count() - 1; i >= 0; i--)
			{
				if (this->currentRow() < 0 && this->item(i)->flags() != Qt::NoItemFlags)
				{
					item = this->item(i);
					break;
				}

				if (this->currentRow() > i && this->item(i)->flags() != Qt::NoItemFlags)
				{
					item = this->item(i);
					break;
				}
			}
			if (item)
			{
				item->setSelected(true);
				this->setCurrentItem(item);
				this->doClickedItem(item);
			}
		}
	}
	if (event->key() == Qt::Key_Down)
	{
		if (this->count() > 0)
		{
			QListWidgetItem *item = NULL;
			for (int i = 0; i < this->count(); i++)
			{
				if (this->currentRow() < 0 && this->item(i)->flags() != Qt::NoItemFlags)
				{
					item = this->item(i);
					break;
				}

				if (this->currentRow() < i && this->item(i)->flags() != Qt::NoItemFlags)
				{
					item = this->item(i);
					break;
				}
			}
			if (item)
			{
				item->setSelected(true);
				this->setCurrentItem(item);
				this->doClickedItem(item);
			}
		}
	}

	if (event->key() == Qt::Key_Return)
	{
		QListWidgetItem *item = this->currentItem();
		if (item)
			this->doDoubleClickedItem(item);
	}

	return QWidget::keyPressEvent(event);
}

void NewFriendList::enterEvent(QEvent * event)
{
	this->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
}

void NewFriendList::leaveEvent(QEvent * event)
{
	this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

void NewFriendList::doClickedItem(QListWidgetItem *item)
{
	InterPlanetNoticeWidget *buddy = (InterPlanetNoticeWidget*)this->itemWidget(item);
	if (buddy != NULL)
	{
		QString strUserID = buddy->getNickName();
		emit sigProfile(strUserID);
	}
	QString strUserID = buddy->getNickName();
	emit sigChangeFoucs();
	emit sigNewFirendFile();
	emit sigNewFriendItemClicked();
}

void NewFriendList::doDoubleClickedItem(QListWidgetItem *item)
{
	InterPlanetNoticeWidget *buddy = (InterPlanetNoticeWidget*)this->itemWidget(item);
	if (buddy != NULL)
	{
		QString strUserID = buddy->getNickName();
		emit sigPerChat(OpenPer, QVariant(strUserID));
	}
}

void NewFriendList::setLableNum(QString strNum)
{
	buddy->OnSetMessageNum(strNum);
}