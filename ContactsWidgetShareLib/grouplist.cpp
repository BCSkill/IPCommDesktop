﻿#include "grouplist.h"
#ifdef Q_OS_MAC
#include "mycommonstyle.h"
#endif
#include "QStringLiteralBak.h"

#include <QtQml>
#include <QQuickItem>
#include "ImgProvider.h"
#include <QToolTip>
#include <QtWidgets/QSplitter>

#include <QGuiApplication>
#include "immainwidget.h"
#include "httpnetworksharelib.h"
#include "messageWidget/messagelistdispatcher.h"

//其它地方不好改，暂时取消修复该QML问题
//#define QML_LISTVIEW_HOVER_BUGFIX

#ifdef QML_LISTVIEW_HOVER_BUGFIX
static QPoint s_offset_pos = QPoint(0,0);
#endif

extern QString gThemeStyle;

GroupList::GroupList(QWidget *parent)
	: QQuickWidget(parent), m_pGrplistModel(new GroupListModel())
{
	this->setResizeMode(QQuickWidget::SizeRootObjectToView);

	ImgProvider *imageProvider = new ImgProvider(QQmlImageProviderBase::Image);
	this->engine()->addImageProvider(QLatin1String("ImgProvider"), imageProvider);

	this->rootContext()->setContextProperty("groupList", this);
	this->rootContext()->setContextProperty("groupListModel", m_pGrplistModel);
	this->setSource(QUrl("qrc:/GroupList/Resources/GroupList/GroupList.qml"));

	//切换主题样式
	if (gThemeStyle == "White")
	{
		this->changeStyle("dayStyle");
	}
	else if (gThemeStyle == "Blue")
	{
		this->changeStyle("nightStyle");
	}

	QString strColor = this->rootObject()->property("clearColor").toString();
	this->setClearColor(QColor(strColor));

	setCurrIndexFlag = true;
	m_menuTimer = new QTimer(this);
}

GroupList::~GroupList()
{
}

void GroupList::changeStyle(QString styleName)
{
	QObject* rootItem = rootObject();
	if (rootItem)
	{
		QVariant param = styleName;
		QMetaObject::invokeMethod(rootItem, "changeStyle", Q_ARG(QVariant, param));
	}
}

void GroupList::mouseReleaseEvent(QMouseEvent *event)
{
#ifdef Q_OS_MAC
	//MAC下触摸板经常拖动偶尔导致无法正常点击，所以发射信号使其正常
	if (event->button() == Qt::LeftButton)
	{
		emit qGuiApp->applicationStateChanged(Qt::ApplicationInactive);
		emit qGuiApp->applicationStateChanged(Qt::ApplicationActive);
	}
#endif
	QQuickWidget::mouseReleaseEvent(event);
}

void GroupList::slotDoubleClickedItem(QListWidgetItem *item)
{
}

void GroupList::doUpDownKeyClick(bool isUp)
{
	QObject* rootItem = rootObject();
	if (rootItem)
	{
		QVariant param = isUp;
		QMetaObject::invokeMethod(rootItem, "doUpDownKeyClick", Q_ARG(QVariant, param));
	}
}

void GroupList::doQmlListViewHoverBugfix(int x,int y)
{
	QObject* rootItem = rootObject();
	if (rootItem)
	{
		QMetaObject::invokeMethod(rootItem, "doQmlListViewHoverBugfix", Q_ARG(QVariant, x), Q_ARG(QVariant, y));
	}
}

void GroupList::doClickItem(QString userId)
{
	emit sigProfile(userId);
}
void GroupList::doDoubleClickItem(QString userId)
{
	emit sigGroupChat(OpenGroup, QVariant(userId));
}

void GroupList::doRightClickItem(QString userId)
{
	point = QCursor::pos();
	UserInfo info = gDataManager->getUserInfo();
	bool isMenuFlag = false;
	int thisLine;
	QString strGroupID = userId;


	for (int i = 0; i < m_pGrplistModel->rowCount(); i++)
	{
		QString user_id = m_pGrplistModel->data(m_pGrplistModel->index(i), GroupListModel::UserIdRole).toString();
		if (user_id == userId)
		{
			isMenuFlag = true;
			thisLine = i;
			break;
		}
	}

	m_menuTimer->disconnect();
	m_menuTimer->stop();
	m_menuTimer->setSingleShot(true);

	connect(m_menuTimer, &QTimer::timeout, this, [this, thisLine, isMenuFlag, strGroupID, info]() {
		QCoreApplication::processEvents();

		if (isMenuFlag)
		{
			listMenu = new QMenu(this);
			QMenu* listMenu = new QMenu(this);
			QFile style(":/QSS/Resources/QSS/ContactsWidgetShareLib/grouplistmenu.qss");
			style.open(QFile::ReadOnly);
			QString sheet = QLatin1String(style.readAll());
			listMenu->setStyleSheet(sheet);
			style.close();
			//listMenu->setStyleSheet("QMenu{border:none;background-color: #72a4d6;color: #042439;}QMenu::item{padding:4px;padding-left:10px;padding-right:10px;}QMenu::item:selected{color: white;border:none;padding:4px;padding-left:10px;padding-right:10px;}");
			//判断管理员权限
			GroupInfo gInfo = gDataBaseOpera->DBGetGroupFromID(strGroupID);
			if (gInfo.createUserId.toInt() == info.nUserID)
			{
				//管理员显示解散群
				QAction *dissolveGroup = new QAction(tr("Dissolve the group"), this);
				connect(dissolveGroup, SIGNAL(triggered()), this, SLOT(slotDissolveGroup()));
				listMenu->addAction(dissolveGroup);
			}
			else
			{
				//成员显示退群
				QAction *userquitgroup = new QAction(tr("Exit the group"), this);
				connect(userquitgroup, SIGNAL(triggered()), this, SLOT(slotUserQuiteGroup()));
				listMenu->addAction(userquitgroup);
			}	
			if (listMenu && thisLine >= 0)
			{
				listMenu->exec(point);
			}
		}
		//TODO 这里弹菜单
	});
	m_menuTimer->start(0);
	
}

void GroupList::OnInsertGroupInfo(GroupInfo groupInfo)
{
	this->OnInsertGroup(groupInfo);
}

void GroupList::OnDeleteGroupInfo(QString groupID)
{
	for (int i = 0; i < m_pGrplistModel->rowCount(); i++)
	{
		QString userId = m_pGrplistModel->data(m_pGrplistModel->index(i), GroupListModel::UserIdRole).toString();
		if (groupID == userId)
		{
			m_pGrplistModel->takeItem(i);
			if (m_pGrplistModel->rowCount() > 0)
			{
				doClickIndex(0);
			}
		}
	}
}

void GroupList::OnInsertGroup(GroupInfo groupInfo,int nHeight)
{
	for (int i = 0; i < m_pGrplistModel->rowCount(); i++)
	{
		QString userId = m_pGrplistModel->data(m_pGrplistModel->index(i), GroupListModel::UserIdRole).toString();
		if (userId == groupInfo.groupId)
		{
			return;
		}
	}
	this->OnInsertMessage(groupInfo.groupId,
		groupInfo.groupLoacalHeadImage,
		groupInfo.groupName,
		1);


	QString currUserId = m_pGrplistModel->data(m_pGrplistModel->index(0), GroupListModel::UserIdRole).toString();
	emit sigDefaultSelectedId(currUserId);
	//setCurrIndexFlag = false;
	this->setCurrentIndex();
}

void GroupList::UpdateHeaderImage(QString userID, QString imgPath, bool  flag)
{
	for (int i = 0; i < m_pGrplistModel->rowCount(); i++)
	{
		QString userId = m_pGrplistModel->data(m_pGrplistModel->index(i), GroupListModel::UserIdRole).toString();
		if (userID == userId)
		{
			if(!IsValidImage(imgPath))
				imgPath = QStringLiteral(":/GroupChat/Resources/groupchat/group.png");
			QString headPicPath = QStringLiteral("image://ImgProvider/") + imgPath;
			m_pGrplistModel->setData(m_pGrplistModel->index(i), headPicPath, GroupListModel::HeadUrlRole);
		}
	}
}

void GroupList::OnUpdateGroupInfo(GroupInfo groupInfo)
{
	QString headPicPath;
	for (int i = 0; i < m_pGrplistModel->rowCount(); i++)
	{
		QString userId = m_pGrplistModel->data(m_pGrplistModel->index(i), GroupListModel::UserIdRole).toString();
		if (userId == groupInfo.groupId)
		{
			if (!IsValidImage(groupInfo.groupLoacalHeadImage))
			{
				QString strPicPath = QStringLiteral(":/GroupChat/Resources/groupchat/group.png");
				headPicPath = QStringLiteral("image://ImgProvider/") + strPicPath;
			}
			else
				headPicPath = QStringLiteral("image://ImgProvider/") + groupInfo.groupLoacalHeadImage;

			m_pGrplistModel->setData(m_pGrplistModel->index(i), headPicPath, GroupListModel::HeadUrlRole);
			m_pGrplistModel->setData(m_pGrplistModel->index(i), groupInfo.groupName, GroupListModel::NickNameRole);
			return;
		}
	}
}
void GroupList::slotDissolveGroup()
{
	IMessageBox* pBox = new IMessageBox(this);
	pBox->initAsk(tr("Notice"), tr("Are you sure to dissolve this group?"));
	connect(pBox, SIGNAL(sigOK()), SLOT(slotDoDissolveGroup()));
}
void GroupList::slotDoDissolveGroup()
{
	UserInfo info = gDataManager->getUserInfo();
	QString strGroupID;
	int rowNum = this->getCurrentIndex();

	for (int i = 0; i < m_pGrplistModel->rowCount(); i++)
	{
		if(i == rowNum)
			strGroupID = m_pGrplistModel->data(m_pGrplistModel->index(i), GroupListModel::UserIdRole).toString();
	}


	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
	http->setData(QVariant(strGroupID));
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotCheckHttp(bool, QString)));
	QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/dissolveGroup"
		+ QString("?userId=%1&passWord=%2&groupId=%3").arg(info.nUserID).arg(info.strUserPWD).arg(strGroupID);
	http->getHttpRequest(url);

	//同步消息列表
	emit MessageListDispatcher::instance()->sigCloseChat(strGroupID);
}
void GroupList::slotUserQuiteGroup()
{
	IMessageBox* pBox = new IMessageBox(this);
	pBox->initAsk(tr("Notice"), tr("Are you sure to quit this group?"));
	connect(pBox, SIGNAL(sigOK()), SLOT(slotDoUserQuiteGroup()));
}
void GroupList::slotDoUserQuiteGroup()
{
	UserInfo info = gDataManager->getUserInfo();
	QString strGroupID;	
	int rowNum = this->getCurrentIndex();

	for (int i = 0; i < m_pGrplistModel->rowCount(); i++)
	{
		if (i == rowNum)
			strGroupID = m_pGrplistModel->data(m_pGrplistModel->index(i), GroupListModel::UserIdRole).toString();
	}


	HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
	http->setData(QVariant(strGroupID));
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotCheckHttp(bool, QString)));
	QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/group/userQuitGroup"
		+ QString("?userId=%1&passWord=%2&groupId=%3").arg(info.nUserID).arg(info.strUserPWD).arg(strGroupID);
	http->getHttpRequest(url);
	
	emit MessageListDispatcher::instance()->sigCloseChat(strGroupID);
}

void GroupList::slotCheckHttp(bool bSuccess, QString strValue)
{
	HttpNetWork::HttpNetWorkShareLib *http = qobject_cast<HttpNetWork::HttpNetWorkShareLib *>(sender());
	if (http)
	{
		if (bSuccess&&strValue.contains("success"))
		{
			//成功后手动删除本地数据库和list 防止收不到服务器的删除消息
			QString strGroupID = http->getData().toString();
			OnDeleteGroupInfo(strGroupID);
			gDataBaseOpera->DBDeleteGroupInfoByID(strGroupID);
		}
		else
		{
			//提示失败
			IMessageBox::tip(this, tr("Warning"), tr("Network request failed!"));
		}
		http->deleteLater();
	}
	return;
}


void GroupList::OnInsertMessage(QString strUserID,
	QString strPicPath,
	QString strNickName,
	int nHeight)
{
	//先对列表中是否存在进行判断。
	int refreshFlag;
	for (int i = 0; i < m_pGrplistModel->rowCount(); i++)
	{
		QString userId = m_pGrplistModel->data(m_pGrplistModel->index(i), GroupListModel::UserIdRole).toString();
		if (strUserID == userId)
		{
			//如果列表中存在，直接退出。
			return;
		}
	}
	//判断strPicPath是否有效的图片，无效的话用默认图片
	if (!IsValidImage(strPicPath))
	{
		strPicPath = QStringLiteral(":/GroupChat/Resources/groupchat/group.png");
	}
	QString headPicPath = QStringLiteral("image://ImgProvider/") + strPicPath;
	GroupListItem item(strUserID, headPicPath, strNickName);
	m_pGrplistModel->pushFront(&item);
	m_pGrplistModel->setData(m_pGrplistModel->index(0), headPicPath, GroupListModel::HeadUrlRole);
}


bool GroupList::IsValidImage(QString imgPath)
{
	QFile file(imgPath);
	if (file.open(QIODevice::ReadOnly))
	{
		QByteArray byteArray = file.readAll();
		file.close();
		QImage image = QImage::fromData(byteArray);

		if (image.isNull())
		{
			return false;
		}
	}
	else
	{
		return false;
	}

	return true;
}

void GroupList::doClickIndex(int index)
{
	QObject* rootItem = rootObject();
	if (rootItem)
	{
		QVariant param_index = index;
		QMetaObject::invokeMethod(rootItem, "doClickIndex", Q_ARG(QVariant, param_index));
	}
}


int GroupList::getCurrentIndex()
{
	QObject* listViewGroupList = this->rootObject()->findChild<QObject*>("listViewGroupList");
	return listViewGroupList->property("currentIndex").toInt();
}

void GroupList::setCurrentIndex()
{
	QVariant qvIndex(0);
	QObject* listViewGroupList = this->rootObject()->findChild<QObject*>("listViewGroupList");
	listViewGroupList->setProperty("currentIndex", qvIndex);
}


void GroupList::showEvent(QShowEvent *event)
{
	this->setFocus();

#ifdef QML_LISTVIEW_HOVER_BUGFIX
	if (s_offset_pos.x() == 0 && s_offset_pos.y() == 0)
	{
		s_offset_pos = this->mapTo(IMMainWidget::self(), QPoint(0, 0));
		doQmlListViewHoverBugfix(s_offset_pos.x(), s_offset_pos.y());

		if (s_offset_pos.x() != 0 || s_offset_pos.y() != 0)
		{
			this->move(-s_offset_pos.x(), -s_offset_pos.y());

			this->resize(this->width() + s_offset_pos.x(), this->height() + s_offset_pos.y());
		}
	}

	this->updateGeometry();
#endif

	
}

void GroupList::moveEvent(QMoveEvent *event)
{
	QQuickWidget::moveEvent(event);

#ifdef QML_LISTVIEW_HOVER_BUGFIX
	qDebug()<<"event->oldPos():"<<event->oldPos();
	qDebug()<<"event->pos():" << event->pos();

	if (!(s_offset_pos.x() == 0 && s_offset_pos.y() == 0) 
		&& event->oldPos().x() == -s_offset_pos.x() && event->oldPos().y() == -s_offset_pos.y()
		)
	{
		s_offset_pos = this->mapTo(IMMainWidget::self(), QPoint(0, 0));
		doQmlListViewHoverBugfix(s_offset_pos.x(), s_offset_pos.y());

		if (s_offset_pos.x() != 0 || s_offset_pos.y() != 0)
		{
			this->move(-s_offset_pos.x(), -s_offset_pos.y());

			this->resize(this->width() + s_offset_pos.x(), this->height() + s_offset_pos.y());
		}
	}
#endif
}

void GroupList::resizeEvent(QResizeEvent *event)
{
	QQuickWidget::resizeEvent(event);

#ifdef QML_LISTVIEW_HOVER_BUGFIX
	if (s_offset_pos.x() != 0 || s_offset_pos.y() != 0)
	{
		this->move(-s_offset_pos.x(), -s_offset_pos.y());

		if (event->oldSize().width() + s_offset_pos.x() == event->size().width() && event->oldSize().height() + s_offset_pos.y() == event->size().height())
		{
			return;
		}

		this->resize(event->size().width() + s_offset_pos.x(), event->size().height() + s_offset_pos.y());
	}
#endif
	
}


void GroupList::keyPressEvent(QKeyEvent * event)
{
	int currIndex;
	QObject* listViewGroupList = this->rootObject()->findChild<QObject*>("listViewGroupList");
	currIndex = listViewGroupList->property("currentIndex").toInt();
	if (event->key() == Qt::Key_Up)
	{
		if (currIndex == 0)
			return;
		else
		{
			listViewGroupList->setProperty("currentIndex", currIndex-1);
			QString currGrpId = m_pGrplistModel->data(m_pGrplistModel->index(currIndex-1), GroupListModel::UserIdRole).toString();
			emit sigDefaultSelectedId(currGrpId);
		}
	}
	if (event->key() == Qt::Key_Down)
	{
		if (currIndex == m_pGrplistModel->rowCount()-1)
			return;
		else
		{

			listViewGroupList->setProperty("currentIndex", currIndex+1);
			QString currGrpId = m_pGrplistModel->data(m_pGrplistModel->index(currIndex+1), GroupListModel::UserIdRole).toString();
			emit sigDefaultSelectedId(currGrpId);
		}
	}

	if (event->key() == Qt::Key_Return)
	{
		QString currGrpId = m_pGrplistModel->data(m_pGrplistModel->index(currIndex), GroupListModel::UserIdRole).toString();
		doDoubleClickItem(currGrpId);
	}
	return QWidget::keyPressEvent(event);
}

