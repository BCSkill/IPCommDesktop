#include "ContactListModel.h"
#include <qdebug.h>

ContactListItem::ContactListItem(const QString &userId, const QString &imgUrl, const QString &nickName, const QString &msgType, const QString &titleCharacter)
	:m_userId(userId), m_headUrl(imgUrl), m_nickName(nickName), m_msgType(msgType), m_titleCharacter(titleCharacter)
{
}

int ContactListItem::itemType() const
{
	return m_itemType;
}

QString ContactListItem::userId() const
{
	return m_userId;
}

QString ContactListItem::headUrl() const
{
	return m_headUrl;
}

QString ContactListItem::nickName() const
{
	return m_nickName;
}

QString ContactListItem::msg() const
{
	return m_msg;
}

QString ContactListItem::msgTime() const
{
	return m_msgTime;
}

int ContactListItem::unreadMsgCount() const
{
	return m_unreadMsgCount;
}

QString ContactListItem::sendStatusUrl() const
{
	return m_sendStatusUrl;
}
QString ContactListItem::msgType() const
{
	return m_msgType;
}
QString ContactListItem::titleCharacter() const
{
	return m_titleCharacter;
}


void ContactListItem::setItemType(QVariant val)
{
	m_itemType = val.toInt();
}

void ContactListItem::setUserId(QVariant val)
{
	m_userId = val.toString();
}

void ContactListItem::setHeadUrl(QVariant val)
{
	m_headUrl = val.toString();
}

void ContactListItem::setNickName(QVariant val)
{
	m_nickName = val.toString();
}

void ContactListItem::setMsg(QVariant val)
{
	m_msg = val.toString();
}

void ContactListItem::setMsgTime(QVariant val)
{
	m_msgTime = val.toString();
}

void ContactListItem::setUnreadMsgCount(QVariant val)
{
	m_unreadMsgCount = val.toInt();
}

void ContactListItem::setSendStatusUrl(QVariant val)
{
	m_sendStatusUrl = val.toString();
}
void ContactListItem::setMsgType(QVariant val)
{
	m_msgType = val.toString();
}
void ContactListItem::setTitleCharacter(QVariant val)
{
	m_titleCharacter = val.toString();
}






ContactListModel::ContactListModel(QObject *parent)
	: QAbstractListModel(parent)
{
}

void ContactListModel::pushFront(const ContactListItem *item)
{
	beginInsertRows(QModelIndex(), 0, 0);
	m_items.push_front(*item);
	endInsertRows();
}

void ContactListModel::pushBack(const ContactListItem *item)
{
	beginInsertRows(QModelIndex(), rowCount(), rowCount());
	m_items.push_back(*item);
	endInsertRows();
}

void ContactListModel::insertItem(const int pos, const ContactListItem *item)
{
	beginInsertRows(QModelIndex(), pos, pos);
	m_items.insert(pos, *item);
	endInsertRows();
}


int ContactListModel::rowCount(const QModelIndex & parent) const {
	Q_UNUSED(parent);
	return m_items.count();
}


QVariant ContactListModel::data(const QModelIndex & index, int role) const {
	if (index.row() < 0 || index.row() >= m_items.count())
		return QVariant();

	const ContactListItem &item = m_items[index.row()];

	if (role == ItemTypeRole)
	{
		return item.itemType();
	}
	else if (role == UserIdRole)
	{
		return item.userId();
	}
	else if (role == HeadUrlRole)
	{
		return item.headUrl();
	}
	else if (role == NickNameRole)
	{
		return item.nickName();
	}
	else if (role == MsgRole)
	{
		return item.msg();
	}
	else if (role == MsgTimeRole)
	{
		return item.msgTime();
	}
	else if (role == UnreadMsgCountRole)
	{
		return item.unreadMsgCount();
	}
	else if (role == SendStatusUrlRole)
	{
		return item.sendStatusUrl();
	}
	else if (role == MsgTypeRole)
	{
		return item.msgType();
	}
	else if (role == TitleCharacterRole)
	{
		return item.titleCharacter();
	}
	return QVariant();
}

bool ContactListModel::setData(const QModelIndex &index, const QVariant &value, int role/* = Qt::EditRole*/)
{
	if (index.row() < 0 || index.row() >= m_items.count())
		return false;

	ContactListItem &item = m_items[index.row()];
	if (role == ItemTypeRole)
	{
		item.setItemType(value);
	}
	else if (role == UserIdRole)
	{
		item.setUserId(value);
	}
	else if (role == HeadUrlRole)
	{
		//item.setHeadUrl(value);
		item.setHeadUrl(value.toString() + "?" + QString::number(QDateTime::currentDateTime().toMSecsSinceEpoch()));
	}
	else if (role == NickNameRole)
	{
		item.setNickName(value);
	}
	else if (role == MsgRole)
	{
		item.setMsg(value);
	}
	else if (role == MsgTimeRole)
	{
		item.setMsgTime(value);
	}
	else if (role == UnreadMsgCountRole)
	{
		item.setUnreadMsgCount(value);
	}
	else if (role == SendStatusUrlRole)
	{
		item.setSendStatusUrl(value);
	}
	else if (role == MsgTypeRole)
	{
		item.setMsgType(value);
	}
	else if (role == TitleCharacterRole)
	{
		item.setTitleCharacter(value);
	}

	emit dataChanged(index, index);
	return true;
}

//![0]
QHash<int, QByteArray> ContactListModel::roleNames() const {
	QHash<int, QByteArray> roles;
	roles[ItemTypeRole] = "itemType";
	roles[UserIdRole] = "userId";
	roles[HeadUrlRole] = "headUrl";
	roles[NickNameRole] = "nickName";
	roles[MsgRole] = "msg";
	roles[MsgTimeRole] = "msgTime";
	roles[UnreadMsgCountRole] = "unreadMsgCount";
	roles[SendStatusUrlRole] = "sendStatusUrl";
	roles[MsgTypeRole] = "msgType";
	roles[TitleCharacterRole] = "titleCharacter";

	return roles;
}

void ContactListModel::clear()
{
	if (m_items.count() > 0)
	{
		beginRemoveRows(QModelIndex(), 0, m_items.count() - 1);
		m_items.clear();
		endRemoveRows();
	}

}

void ContactListModel::takeItem(int row)
{
	beginRemoveRows(QModelIndex(), row, row);
	m_items.removeAt(row);
	endRemoveRows();
}

QString ContactListModel::currentSelectedUserId()
{
	return  m_currentSelectedUserId;
}

void ContactListModel::setCurrentSelectedUserId(QString userId)
{
	m_currentSelectedUserId = userId;
}