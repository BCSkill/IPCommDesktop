<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>ContactList</name>
    <message>
        <source>Delete contact</source>
        <translation type="vanished">删除好友</translation>
    </message>
    <message>
        <location filename="contactlist.cpp" line="135"/>
        <source>Delete Friend</source>
        <translation>删除好友</translation>
    </message>
    <message>
        <location filename="contactlist.cpp" line="409"/>
        <source>Notice</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="contactlist.cpp" line="409"/>
        <source>Are you sure to delete this contact?</source>
        <translation>确定删除此好友？</translation>
    </message>
    <message>
        <source>Are you sure to delete this contact？</source>
        <translation type="vanished">确定删除此好友？</translation>
    </message>
    <message>
        <location filename="contactlist.cpp" line="455"/>
        <source>Warning</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="contactlist.cpp" line="455"/>
        <source>Network request failed!</source>
        <translation>网络请求失败！</translation>
    </message>
</context>
<context>
    <name>ContactMenuWidget</name>
    <message>
        <location filename="contactmenuwidget.ui" line="26"/>
        <location filename="GeneratedFiles/ui_contactmenuwidget.h" line="113"/>
        <source>ContactMenuWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="contactmenuwidget.ui" line="83"/>
        <location filename="GeneratedFiles/ui_contactmenuwidget.h" line="114"/>
        <source>Create group</source>
        <oldsource>Create tribe</oldsource>
        <translation>创建群组</translation>
    </message>
    <message>
        <location filename="contactmenuwidget.ui" line="129"/>
        <location filename="GeneratedFiles/ui_contactmenuwidget.h" line="115"/>
        <source>Add friend</source>
        <translation>添加好友</translation>
    </message>
</context>
<context>
    <name>ContactsDataManager</name>
    <message>
        <location filename="contactsdatamanager.cpp" line="503"/>
        <location filename="contactsdatamanager.cpp" line="506"/>
        <source>Friend requests</source>
        <translation>好友申请</translation>
    </message>
    <message>
        <location filename="contactsdatamanager.cpp" line="539"/>
        <location filename="contactsdatamanager.cpp" line="542"/>
        <source>Group application</source>
        <oldsource>Tribal application</oldsource>
        <translation>群组申请</translation>
    </message>
    <message>
        <location filename="contactsdatamanager.cpp" line="555"/>
        <location filename="contactsdatamanager.cpp" line="593"/>
        <source> Apply to join</source>
        <translation> 申请加入</translation>
    </message>
    <message>
        <location filename="contactsdatamanager.cpp" line="575"/>
        <location filename="contactsdatamanager.cpp" line="578"/>
        <source>Processed</source>
        <translation>已处理</translation>
    </message>
    <message>
        <location filename="contactsdatamanager.cpp" line="603"/>
        <source>approved</source>
        <translation>已同意</translation>
    </message>
    <message>
        <location filename="contactsdatamanager.cpp" line="607"/>
        <source>rejected</source>
        <translation>已拒绝</translation>
    </message>
    <message>
        <location filename="contactsdatamanager.cpp" line="629"/>
        <source>Please enter a remark name:</source>
        <translation>请输入备注名称：</translation>
    </message>
    <message>
        <location filename="contactsdatamanager.cpp" line="680"/>
        <source>We have become friends and chat together!</source>
        <translation>我们已经成为好友，一起聊天吧！</translation>
    </message>
    <message>
        <location filename="contactsdatamanager.cpp" line="726"/>
        <source>Warning</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="contactsdatamanager.cpp" line="726"/>
        <source>network anomaly!</source>
        <translation>网络异常！</translation>
    </message>
    <message>
        <location filename="contactsdatamanager.cpp" line="712"/>
        <source> joined the group</source>
        <oldsource> joined the tribe</oldsource>
        <translation> 加入了群组</translation>
    </message>
</context>
<context>
    <name>ContactsSearchList</name>
    <message>
        <location filename="contactssearchlist.ui" line="16"/>
        <location filename="GeneratedFiles/ui_contactssearchlist.h" line="38"/>
        <source>ContactsSearchList</source>
        <translation></translation>
    </message>
    <message>
        <location filename="contactssearchlist.cpp" line="123"/>
        <location filename="contactssearchlist.cpp" line="150"/>
        <location filename="contactssearchlist.cpp" line="153"/>
        <location filename="contactssearchlist.cpp" line="170"/>
        <location filename="contactssearchlist.cpp" line="230"/>
        <location filename="contactssearchlist.cpp" line="250"/>
        <source>friends</source>
        <translation>好友</translation>
    </message>
    <message>
        <location filename="contactssearchlist.cpp" line="132"/>
        <location filename="contactssearchlist.cpp" line="187"/>
        <location filename="contactssearchlist.cpp" line="190"/>
        <location filename="contactssearchlist.cpp" line="203"/>
        <source>groups</source>
        <translation>群组</translation>
    </message>
    <message>
        <source>tribes</source>
        <translation type="vanished">群组</translation>
    </message>
    <message>
        <location filename="contactssearchlist.cpp" line="227"/>
        <source>other friends</source>
        <translation>其他朋友</translation>
    </message>
</context>
<context>
    <name>ContactsWidget</name>
    <message>
        <location filename="contactswidget.ui" line="14"/>
        <location filename="GeneratedFiles/ui_contactswidget.h" line="213"/>
        <source>ContactsWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="contactswidget.ui" line="125"/>
        <location filename="contactswidget.ui" line="173"/>
        <location filename="GeneratedFiles/ui_contactswidget.h" line="215"/>
        <location filename="GeneratedFiles/ui_contactswidget.h" line="219"/>
        <source>Search</source>
        <oldsource>search</oldsource>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="contactswidget.cpp" line="44"/>
        <source>Create group</source>
        <oldsource>Create tribe</oldsource>
        <translation>创建群组</translation>
    </message>
    <message>
        <location filename="contactswidget.cpp" line="48"/>
        <source>Add friends</source>
        <translation>添加好友</translation>
    </message>
</context>
<context>
    <name>GroupList</name>
    <message>
        <location filename="grouplist.cpp" line="157"/>
        <source>Dissolve the group</source>
        <oldsource>Dissolve the tribe</oldsource>
        <translation>解散群组</translation>
    </message>
    <message>
        <location filename="grouplist.cpp" line="164"/>
        <source>Exit the group</source>
        <oldsource>Exit the tribe</oldsource>
        <translation>退出群组</translation>
    </message>
    <message>
        <location filename="grouplist.cpp" line="262"/>
        <location filename="grouplist.cpp" line="291"/>
        <source>Notice</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="grouplist.cpp" line="262"/>
        <source>Are you sure to dissolve this group?</source>
        <translation>确定要解散此群组？</translation>
    </message>
    <message>
        <location filename="grouplist.cpp" line="291"/>
        <source>Are you sure to quit this group?</source>
        <translation>确定要退出此群组？</translation>
    </message>
    <message>
        <source>Are you sure to dissolve this tribe?</source>
        <translation type="vanished">确定要解散此部落？</translation>
    </message>
    <message>
        <source>Are you sure to quit this tribe?</source>
        <translation type="vanished">确定要退出此部落？</translation>
    </message>
    <message>
        <location filename="grouplist.cpp" line="332"/>
        <source>Warning</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="grouplist.cpp" line="332"/>
        <source>Network request failed!</source>
        <translation>网络请求失败！</translation>
    </message>
</context>
<context>
    <name>GroupProfileWidget</name>
    <message>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="492"/>
        <source>Tribal details</source>
        <translation>部落详情</translation>
    </message>
    <message>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="496"/>
        <source>Tribal QR code</source>
        <translation>部落二维码</translation>
    </message>
    <message>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="499"/>
        <source>Tribal name</source>
        <translation>部落名称</translation>
    </message>
    <message>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="502"/>
        <source>English name</source>
        <translation>英文名称</translation>
    </message>
    <message>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="503"/>
        <source>creator</source>
        <translation>创建者</translation>
    </message>
    <message>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="505"/>
        <source>Creation time</source>
        <translation>创建时间</translation>
    </message>
    <message>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="507"/>
        <source>Tribal
introduction</source>
        <translation>部落介绍</translation>
    </message>
    <message>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="510"/>
        <source>Group owner</source>
        <translation>部落拥有者</translation>
    </message>
    <message>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="513"/>
        <source>Add member</source>
        <translation>增加成员</translation>
    </message>
    <message>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="516"/>
        <source>My nickname</source>
        <translation>部落昵称</translation>
    </message>
    <message>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="517"/>
        <source>Free to join</source>
        <translation>自由加入</translation>
    </message>
    <message>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="520"/>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="522"/>
        <source>Join the tribe</source>
        <translation>加入群组</translation>
    </message>
    <message>
        <location filename="GeneratedFiles/ui_groupprofilewidget.h" line="523"/>
        <source>Apply for tribal success</source>
        <translation>申请加群组成功</translation>
    </message>
</context>
<context>
    <name>NewFriendList</name>
    <message>
        <source>Telecomm Notice</source>
        <oldsource>InterPlanet Notice</oldsource>
        <translation type="vanished">通知</translation>
    </message>
    <message>
        <location filename="newfriendlist.cpp" line="39"/>
        <source>OpenPlanet Notice</source>
        <translation>星际通知</translation>
    </message>
</context>
<context>
    <name>perProfileWidget</name>
    <message>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="434"/>
        <source>Member details</source>
        <translation>成员详情</translation>
    </message>
    <message>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="436"/>
        <source>View avatar</source>
        <translation>查看头像</translation>
    </message>
    <message>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="444"/>
        <source>Modify note</source>
        <translation>修改备注</translation>
    </message>
    <message>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="448"/>
        <source>Recommend to a friend</source>
        <translation>分享</translation>
    </message>
    <message>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="452"/>
        <source>Interstellar ID</source>
        <translation>星际ID</translation>
    </message>
    <message>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="454"/>
        <source>Base ID</source>
        <translation>基地ID</translation>
    </message>
    <message>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="457"/>
        <source>copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="460"/>
        <source>Success</source>
        <translation>成功</translation>
    </message>
    <message>
        <location filename="GeneratedFiles/ui_perprofilewidget.h" line="462"/>
        <source>Send message</source>
        <translation>发送消息</translation>
    </message>
</context>
</TS>
