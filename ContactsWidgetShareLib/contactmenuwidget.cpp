﻿#include "contactmenuwidget.h"
#include "ui_contactmenuwidget.h"
#include "qdebug.h"
#include <qfile.h>
ContactMenuWidget::ContactMenuWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::ContactMenuWidget();
	ui->setupUi(this);

	setWindowFlags(Qt::FramelessWindowHint|Qt::Dialog);
	mPix.load(":/addPerson/Resources/add/mask.png");
	QSize sss = mPix.size();
	resize(mPix.size()); 
	setAutoFillBackground(true);
	setMask(mPix.mask());


	QFile file(":/QSS/Resources/QSS/ContactsWidgetShareLib/contactmenuwidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();	

	connect(ui->CreateBtn, SIGNAL(clicked()), this, SIGNAL(sigAddCreateGroup()));
	connect(ui->AddBtn, SIGNAL(clicked()), this, SIGNAL(sigAddPerson()));
	connect(ui->picbtn1, SIGNAL(clicked()), this, SIGNAL(sigAddCreateGroup()));
	connect(ui->picbtn2, SIGNAL(clicked()), this, SIGNAL(sigAddPerson()));
	
}

ContactMenuWidget::~ContactMenuWidget()
{
	delete ui;
}

void ContactMenuWidget::paintEvent(QPaintEvent *)
{
	QPalette palette = this->palette();
	palette.setBrush(QPalette::Background, mPix);
	setPalette(palette);
}

bool ContactMenuWidget::event(QEvent* event)
{
	//qDebug() << event->type();
	if (event->type() == QEvent::WindowDeactivate)
	{
		this->deleteLater();
	}
	return QWidget::event(event);
}