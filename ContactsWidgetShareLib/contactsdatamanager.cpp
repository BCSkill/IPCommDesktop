﻿#include "contactsdatamanager.h"
#include "cfrientstylewidget.h"
#include "inputbox.h"
#ifndef Q_OS_WIN
#include "mycommonstyle.h"
#endif
#include "QStringLiteralBak.h"
#include "opdatamanager.h"
#include "opdatebasesharelib.h"
#include "oprequestsharelib.h"
#include "imdownloadheaderimg.h"
#include "groupprofilewidget.h"
#include "perprofilewidget.h"
#include "pinyin.h"
#include "interplanetnoticewidget.h"

extern QString gThemeStyle;

extern OPDatebaseShareLib *gOPDataBaseOpera;
extern OPDataManager  *gOPDataManager;

ContactsDataManager* ContactsDataManager::s_instance = NULL;

ContactsDataManager::ContactsDataManager(QObject *parent)
	: QObject(parent)
{
	s_instance = this;

	widget = new ContactsWidget();
	searchList = new ContactsSearchList();
	contactsList = new ContactList();
	newfriendList = new NewFriendList(widget->getTabsWidgetProfile());
	groupList = new GroupList();
	BigWidget = new QWidget();
	QVBoxLayout *grid = new QVBoxLayout;
	m_iMessageNum = 0;
	grid->addWidget(newfriendList, 0, 0);
	grid->addWidget(contactsList, 1, 0);
	BigWidget->setLayout(grid);
	grid->setMargin(0);
	grid->setSpacing(0);
	BigWidget->setStyleSheet("border:none;");
	perWidget = widget->getPerProfile();
	groupWidget = widget->getGroupProfile();
	newFriendWidget = widget->getNewFriendProfile();
	newFriendWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	newFriendWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	//UpdateNewFriendWidget();
	if (gThemeStyle == "Blue")
	{
		m_style1 = "color:#000000";
		m_style2 = "color:#999999";
	}
	else
	{
		m_style1 = "color:#108de9"; 
		m_style2 = "color:#28557c";
	}
	//向窗口列表中添加搜索窗口、好友窗口和部落窗口。

	widget->addWidget(searchList);
	widget->addWidget(BigWidget);
	widget->addWidget(groupList);

	//设置好友窗口为默认窗口。
	widget->setWidget(BigWidget);

	connect(searchList, SIGNAL(sigOpenChat(int, QVariant)), this, SLOT(slotSendChat(int, QVariant)));
	connect(searchList, SIGNAL(sigOpenChat(int, QVariant)), widget, SLOT(slotClickCancelSearch()));
	connect(widget, SIGNAL(sigSearchText(QString)), this, SLOT(slotSearchText(QString)));
	connect(widget, SIGNAL(sigAddCreateGroup()), this, SIGNAL(sigAddCreateGroup()));
	connect(widget, SIGNAL(sigAddPerson()), this, SIGNAL(sigAddPerson()));
	connect(widget, SIGNAL(sigFilter(int)), this, SLOT(slotFilter(int)));
	connect(widget, SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget*)), this, SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget*)));//图片浏览


	connect(this, SIGNAL(sigOpenPerOrGroupChat(QString)), this, SLOT(slotOpenPerOrGroupChat(QString)));

	connect(perWidget, SIGNAL(sigPerChat(int, QVariant)), this, SLOT(slotSendChat(int, QVariant)));
	connect(perWidget, SIGNAL(sigShareID(int, QString)), this, SIGNAL(sigShareID(int, QString)));
	connect(contactsList, SIGNAL(sigPerChat(int, QVariant)), this, SLOT(slotSendChat(int, QVariant)));
	connect(contactsList, SIGNAL(sigProfile(QString)), this, SLOT(slotPerProfile(QString)));
	connect(perWidget, SIGNAL(sigUpdateBuddyInfo(BuddyInfo)), contactsList, SLOT(slotUpdateBuddyInfo(BuddyInfo)));
	connect(perWidget, SIGNAL(sigUpdateBuddyInfo(BuddyInfo)), this, SLOT(slotUpdateBuddyInfo(BuddyInfo)));
	connect(this, SIGNAL(sigUpdateBuddyInfo(BuddyInfo)), contactsList, SLOT(slotUpdateBuddyInfo(BuddyInfo)));

	connect(groupWidget, SIGNAL(sigGroupChat(int, QVariant)), this, SLOT(slotSendChat(int, QVariant)));
	connect(groupWidget, SIGNAL(sigShareID(int, QString)), this, SIGNAL(sigShareID(int, QString)));
	connect(groupWidget, SIGNAL(sigTipMessage(int, QString, QString)), this, SIGNAL(sigTipMessage(int, QString, QString)));
	connect(groupList, SIGNAL(sigGroupChat(int, QVariant)), this, SLOT(slotSendChat(int, QVariant)));
	connect(groupList, SIGNAL(sigProfile(QString)), this, SLOT(slotGroupProfile(QString)));

	connect(newfriendList, SIGNAL(sigChangeFoucs()), contactsList, SLOT(slotConChangeFoucs()));
	connect(contactsList, SIGNAL(sigChangeFoucs()), this, SLOT(slotNewChangeFoucs()));
	connect(newfriendList, SIGNAL(sigNewFirendFile()), this, SLOT(slotNewFirendFile()));
	connect(newfriendList, SIGNAL(sigNewFriendItemClicked()), contactsList, SLOT(slotNewFriendItemClicked()));
	connect(contactsList, SIGNAL(sigCanclePerProfileWid()), widget, SLOT(slotCanclePerProfileWid()));
	connect(contactsList, SIGNAL(sigChangeFoucs()), widget, SLOT(slotPerProfileWid()));

	//connect(this, SIGNAL(sigAddFriendSuccess(BuddyInfo)), this, SLOT(slotAddFriendSuccess(BuddyInfo)));
	//connect(contactsList, SIGNAL(sigChangeFoucs()), newfriendList, SLOT(doChangeFocus()));
	connect(contactsList, SIGNAL(sigDefaultSelectedId(QString)), this, SLOT(slotPerProfile(QString)), Qt::QueuedConnection);
	connect(groupList, SIGNAL(sigDefaultSelectedId(QString)), this, SLOT(slotGroupProfileNoShow(QString)), Qt::QueuedConnection);
	connect(widget, SIGNAL(sigSendPerOrGroupFlag(int)), this, SLOT(slotSendPerOrGroupFlag(int)));

	OPRequestShareLib *request = new OPRequestShareLib;
	connect(request, SIGNAL(sigBaseBuddyInfo(QString)), this, SLOT(slotAddressInfo(QString)));
	connect(request, SIGNAL(sigBaseBuddyInfo(QString)), request, SLOT(deleteLater()));
	request->getBaseBuddyInfo(gOPDataManager->getAccessToken());


#ifdef Q_OS_MAC
	newFriendWidget->setStyle(new MyProxyStyle);
#endif
}

ContactsDataManager::~ContactsDataManager()
{

}

ContactsDataManager* ContactsDataManager::instance()
{
	return s_instance;
}

ContactsWidget * ContactsDataManager::getContactsWidget()
{
	return this->widget;
}

void ContactsDataManager::slotAddressInfo(QString result)
{
	QJsonDocument json = QJsonDocument::fromJson(result.toUtf8());
	QVariantMap map = json.toVariant().toMap();

	if (map.value("result").toString() == "success")
	{
		QList<QVariant> list = map.value("addressList").toList();
		foreach(QVariant var, list)
		{
			QMap<QString, QVariant> buddy = var.toMap();
			AddressInfo info;
			info.userID = QString::number(buddy.value("imUserId").toInt());
			info.comPublicKey = buddy.value("comPublicKey").toString();
			info.ethAddress = buddy.value("ethAddress").toString();
			info.ethPublicKey = buddy.value("ethPublicKey").toString();
			info.planet = buddy.value("planet").toString();

			gOPDataBaseOpera->DBInsertAddressInfo(info);
		}
	}
}

void ContactsDataManager::recvBuddyData(int type, QVariant data)
{
	switch (type)
	{
	case BuddyInsert:
	{
		BuddyInfo buddy = data.value<BuddyInfo>();
		contactsList->OnInsertContactsInfo(buddy);
		break;
	}
	case BuddyUpdate:
	{
		BuddyInfo buddy = data.value<BuddyInfo>();
		contactsList->OnUpdateBuddyInfo(buddy);
	}
	case BuddyDelete:
	{
		QString userID = data.toString();
		contactsList->deleteFriend(userID);
	}
	default:
		break;
	}
}

void ContactsDataManager::recvGroupData(int type, QVariant data)
{
	switch (type)
	{
	case GroupInsert:
	{
		GroupInfo group = data.value<GroupInfo>();
		groupList->OnInsertGroupInfo(group);
		break;
	}
	case GroupUpdate:
	{
		GroupInfo group = data.value<GroupInfo>();
		groupList->OnUpdateGroupInfo(group);

		if (groupWidget->getGroupID() == group.groupId)
		{
			//如果当前部落资料页面是要更新的部落，就刷新。
			groupWidget->setGroup(group.groupId);
		}

		break;
	}
	case GroupDelete:
	{
		QString groupID = data.value<QString>();
		groupList->OnDeleteGroupInfo(groupID);
		break;
	}
	default:
		break;
	}
}

void ContactsDataManager::updateImage(int type, int nIMuserID, QString imgPath)
{
	switch (type)
	{
	case BuddyUpdate:
	{
		if (contactsList)
		{
			contactsList->UpdateBuddyImage(QString::number(nIMuserID), imgPath);
		}
	}
	break;
	case GroupUpdate:
	{
		if (groupList)
		{
			groupList->UpdateHeaderImage(QString::number(nIMuserID), imgPath, false);
		}
	}
	break;
	default:
		break;
	}
}

void ContactsDataManager::slotPerProfile(QString userID)
{
	groupWidget->hide();
	newFriendWidget->hide();
	perWidget->show();

	BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(userID);
	AddressInfo wallet = gOPDataBaseOpera->DBGetAddressInfo(userID);
	perWidget->setBuddy(buddy, wallet);


	QString style = widget->styleSheet();
	if (style.contains(":/robotsWidget/Resources/mainWidget/theme_background.png"))
	{
		style.remove("background-image: url(:/robotsWidget/Resources/mainWidget/theme_background.png);");
		widget->setStyleSheet(style);
	}
}

void ContactsDataManager::slotGroupProfile(QString groupID)
{
	if (currentProfileFlag == false)
	{
		currentProfileFlag = true;
		return;
	}
		
	perWidget->hide();
	newFriendWidget->hide();
	groupWidget->show();

	groupWidget->setGroup(groupID);
	QString style = widget->styleSheet();
	if (style.contains(":/robotsWidget/Resources/mainWidget/theme_background.png"))
	{
		style.remove("background-image: url(:/robotsWidget/Resources/mainWidget/theme_background.png);");
		widget->setStyleSheet(style);
	}
}

void ContactsDataManager::slotGroupProfileNoShow(QString groupID)
{
	newFriendWidget->hide();

	if (perOrGroupFlag == 1)
	{
		perWidget->show();
		groupWidget->hide();
	}
	if (perOrGroupFlag == 2)
	{
		perWidget->hide();
		groupWidget->show();
	}


	groupWidget->setGroup(groupID);
	QString style = widget->styleSheet();
	if (style.contains(":/robotsWidget/Resources/mainWidget/theme_background.png"))
	{
		style.remove("background-image: url(:/robotsWidget/Resources/mainWidget/theme_background.png);");
		widget->setStyleSheet(style);
	}
}

void ContactsDataManager::slotSendPerOrGroupFlag(int id)
{
	perOrGroupFlag = id;
}

void ContactsDataManager::slotNewFirendFile()
{
	perWidget->hide();
	groupWidget->hide();
	newFriendWidget->show();

	InterPlanetNoticeWidget *buddy = (InterPlanetNoticeWidget*)newfriendList->itemWidget(newfriendList->item(0));
	if (buddy)
	{
		buddy->OnSetMessageNum("");
		m_iMessageNum = 0;
		sigMessageNum(m_iMessageNum);
		gDataBaseOpera->DBUpdateAddApplyRead();
	}
}

void ContactsDataManager::slotSetCurrentProfile(bool flag)
{
	currentProfileFlag = false;
}


void ContactsDataManager::slotSearchText(QString text)
{
	if (!text.isEmpty())
	{
		//将输入小写化处理。
		text = text.toLower();

		QList<BuddyInfo> AllBuddyList = gDataBaseOpera->DBGetBuddyInfo();
		QList<GroupInfo> AllGroupList = gDataBaseOpera->DBGetAllGroupInfo();
		QList<BuddyInfo> AllGroupUserLiist = gDataBaseOpera->DBGetAllGroupUserInfo();

		QList<BuddyInfo> buddyList;
		foreach(BuddyInfo buddy, AllBuddyList)
		{
			if (buddy.BuddyType != 1)
				continue;

			PinYin pinyin;
			if (!buddy.strNickName.isEmpty())
			{
				pinyin = getPinYin(buddy.strNickName);
				if (buddy.strNickName.toLower().contains(text) || pinyin.fullPinYin.contains(text) || pinyin.easyPinYin.contains(text))
				{
					buddyList.append(buddy);
					continue;
				}
			}

			if (!buddy.strNote.isEmpty())
			{
				pinyin = getPinYin(buddy.strNote);
				if (buddy.strNote.toLower().contains(text) || pinyin.fullPinYin.contains(text) || pinyin.easyPinYin.contains(text))
				{
					buddyList.append(buddy);
					continue;
				}
			}
		}

		QList<GroupInfo> groupList;
		foreach(GroupInfo group, AllGroupList)
		{
			PinYin pinyin;
			if (!group.groupName.isEmpty())
			{
				PinYin pinyin = getPinYin(group.groupName);
				if (group.groupName.toLower().contains(text) || pinyin.fullPinYin.contains(text) || pinyin.easyPinYin.contains(text))
				{
					groupList.append(group);
					continue;
				}
			}
		}

		QList<BuddyInfo> GroupbuddyList;
		foreach(BuddyInfo buddy, AllGroupUserLiist)
		{
			UserInfo userInfo = gDataManager->getUserInfo();
			if (buddy.nUserId == userInfo.nUserID)
				continue;

			PinYin pinyin;
			if (!buddy.strNickName.isEmpty())
			{
				pinyin = getPinYin(buddy.strNickName);
				if (buddy.strNickName.toLower().contains(text) || pinyin.fullPinYin.contains(text) || pinyin.easyPinYin.contains(text))
				{
					GroupbuddyList.append(buddy);
					continue;
				}
			}

			// 			if (!buddy.strNote.isEmpty())
			// 			{
			// 				pinyin = getPinYin(buddy.strNote);
			// 				if (buddy.strNote.toLower().contains(text) || pinyin.fullPinYin.contains(text) || pinyin.easyPinYin.contains(text))
			// 				{
			// 					buddyList.append(buddy);
			// 					continue;
			// 				}
			// 			}
		}

		searchList->onInsertContacts(buddyList, groupList, GroupbuddyList);
	}
}

void ContactsDataManager::slotSendChat(int type, QVariant ID)
{
	emit sigOpenChat(type, ID);

	if (type == OpenPer)
	{
		BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(ID.toString());
		IMDownLoadHeaderImg *down = new IMDownLoadHeaderImg;
		connect(down, SIGNAL(sigUpdateBuddyHeaderImagePath(int, QString)), this, SLOT(slotDownloadImageFinished(int, QString)));
		down->setObjectName("per");
		down->StartDownLoadBuddyeHeaderImage(buddy);
	}
	if (type == OpenGroup)
	{
		GroupInfo group = gDataBaseOpera->DBGetGroupFromID(ID.toString());
		IMDownLoadHeaderImg *down = new IMDownLoadHeaderImg;
		connect(down, SIGNAL(sigUpdateBuddyHeaderImagePath(int, QString)), this, SLOT(slotDownloadImageFinished(int, QString)));
		down->setObjectName("group");
		down->StartDownLoadGroupHeaderImage(group);
	}
}

void ContactsDataManager::slotDownloadImageFinished(int ID, QString imgPath)
{
	IMDownLoadHeaderImg *down = qobject_cast<IMDownLoadHeaderImg*>(sender());
	if (down)
	{
		if (down->objectName() == "per")
		{
			BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(QString::number(ID));
			contactsList->UpdateBuddyImage(QString::number(ID), imgPath);
			emit sigUpdateImage(BuddyUpdate, QVariant::fromValue(buddy));
		}
		if (down->objectName() == "group")
		{
			GroupInfo group = gDataBaseOpera->DBGetGroupFromID(QString::number(ID));
			groupList->UpdateHeaderImage(QString::number(ID), imgPath, true);
			emit sigUpdateImage(GroupUpdate, QVariant::fromValue(group));
		}
	}
}


void ContactsDataManager::slotConChangeFoucs()
{
	//QList<QListWidgetItem*> list = contactsList->selectedItems();
	//for (int i = 0; i < list.size(); i++)
	//{
	//	list.at(i)->setSelected(false);
	//}
	//return;
}

void ContactsDataManager::slotNewChangeFoucs()
{
	QList<QListWidgetItem*> list = newfriendList->selectedItems();
	for (int i = 0; i < list.size(); i++)
	{
		list.at(i)->setSelected(false);
	}
	return;
}

void ContactsDataManager::UpdateNewFriendWidget()
{
	newFriendWidget->clear();
	QList<AddApplyMessage> applyMessageList = gDataBaseOpera->DBFindALLAddApplyMessage();
	int iNum = 0;
	int iHasBuddy = 0;
	int iHasGroup = 0;
	int iHasDealed = 0;
	int iUnRead = 0;
	//遍历未接受的好友
	for (int i = 0; i < applyMessageList.size(); i++)
	{
		AddApplyMessage stAdd = applyMessageList.at(i);
		if (stAdd.iType == 0 && stAdd.iState == 0)//是未处理的好友申请消息
		{
			if (stAdd.iRead == 0)
			{
				iUnRead++;
			}
			if (iHasBuddy == 0)	//加入标题
			{
				QListWidgetItem *newItem = new QListWidgetItem(tr("Friend requests"));    //创建一个Item
				newItem->setSizeHint(QSize(newFriendWidget->width(), 30));//设置宽度、高度
				newItem->setFlags(Qt::NoItemFlags);
				newItem->setData(Qt::UserRole, tr("Friend requests"));
				newItem->setFont(QFont("Microsoft YaHei", 12, QFont::Normal));
				newFriendWidget->insertItem(newFriendWidget->count(), newItem);         //加到QListWidget中
			}
			iHasBuddy = 1;
			CFrientStyleWidget *buddy = new CFrientStyleWidget();
			buddy->OnInitNewFriendList(QString::number(stAdd.iId));
			//从好友表中查询头像
			BuddyInfo stBuddy = gDataBaseOpera->DBGetBuddyInfoByID(QString::number(stAdd.iId));
			buddy->OnSetPicPath(stBuddy.strLocalAvatar);
			buddy->OnSetNickNameText(stBuddy.strNickName, "font: 75 16pt 微软雅黑;font-size:16px;font-weight:bold;"+m_style1);
			buddy->OnSetAutoGrapthText(stAdd.strMessage, "font: 75 16pt 微软雅黑;font-size:12px;"+m_style2);
			QListWidgetItem *newItem = new QListWidgetItem(); //创建一个newItem
			newItem->setSizeHint(QSize(newFriendWidget->width(), 60));
			newFriendWidget->insertItem(newFriendWidget->count(), newItem); //将该newItem插入到后面
			newFriendWidget->setItemWidget(newItem, buddy); //将buddy赋给该newItem
			connect(buddy, SIGNAL(sigApplyFriend(QString)), this, SLOT(slotApplyFriend(QString)));
			connect(buddy, SIGNAL(sigRejectFriend(QString)), this, SLOT(slotRejectFriend(QString)));
			buddy->mMessageType->hide();
		}
	}
	//遍历未接受的部落
	for (int i = 0; i < applyMessageList.size(); i++)
	{
		AddApplyMessage stAdd = applyMessageList.at(i);
		if (stAdd.iType == 1 && stAdd.iState == 0)//是未处理的好友申请消息
		{
			if (stAdd.iRead == 0)
			{
				iUnRead++;
			}
			if (iHasGroup == 0)	//加入标题
			{
				QListWidgetItem *newItem = new QListWidgetItem(tr("Group application"));    //创建一个Item
				newItem->setSizeHint(QSize(newFriendWidget->width(), 30));//设置宽度、高度
				newItem->setFlags(Qt::NoItemFlags);
				newItem->setData(Qt::UserRole, tr("Group application"));
				newItem->setFont(QFont("Microsoft YaHei", 12, QFont::Normal));
				newFriendWidget->insertItem(newFriendWidget->count(), newItem);         //加到QListWidget中
			}
			iHasGroup = 1;
			CFrientStyleWidget *buddy = new CFrientStyleWidget();
			buddy->OnInitNewFriendList(stAdd.strMegId);
			//从好友表中查询头像
			BuddyInfo stBuddy = gDataBaseOpera->DBGetBuddyInfoByID(QString::number(stAdd.iId));
			GroupInfo stGroup = gDataBaseOpera->DBGetGroupFromID(stAdd.strGroupId);
			buddy->OnSetPicPath(stBuddy.strLocalAvatar);

			buddy->OnSetAutoGrapthText(stAdd.strMessage, "font: 75 16pt 微软雅黑;font-size:12px;"+m_style2);
			buddy->OnSetNickNameText(stBuddy.strNickName + tr(" Apply to join") + stGroup.groupName, "font: 75 16pt 微软雅黑;font-size:16px;font-weight:bold;"+m_style1);

			QListWidgetItem *newItem = new QListWidgetItem(); //创建一个newItem
			newItem->setSizeHint(QSize(newFriendWidget->width(), 60));
			newFriendWidget->insertItem(newFriendWidget->count(), newItem); //将该newItem插入到后面
			newFriendWidget->setItemWidget(newItem, buddy); //将buddy赋给该newItem
			connect(buddy, SIGNAL(sigApplyFriend(QString)), this, SLOT(slotApplyGroup(QString)));
			connect(buddy, SIGNAL(sigRejectFriend(QString)), this, SLOT(slotRejectGroup(QString)));
			buddy->mMessageType->hide();
		}
	}
	QList<AddApplyMessage> applyMessageDealtList = gDataBaseOpera->DBFindALLAddApplyMessageDealt();
	//遍历已处理
	for (int i = 0; i < applyMessageDealtList.size(); i++)
	{
		AddApplyMessage stAdd = applyMessageDealtList.at(i);
		if (stAdd.iState != 0)//是未处理的好友申请消息
		{
			if (iHasDealed == 0)	//加入标题
			{
				QListWidgetItem *newItem = new QListWidgetItem(tr("Processed"));    //创建一个Item
				newItem->setSizeHint(QSize(newFriendWidget->width(), 30));//设置宽度、高度
				newItem->setFlags(Qt::NoItemFlags);
				newItem->setData(Qt::UserRole, tr("Processed"));
				newItem->setFont(QFont("Microsoft YaHei", 12, QFont::Normal));
				newFriendWidget->insertItem(newFriendWidget->count(), newItem);         //加到QListWidget中
			}
			iHasDealed++;
			CFrientStyleWidget *buddy = new CFrientStyleWidget();
			buddy->OnInitNewFriendList(stAdd.strMegId);
			//从好友表中查询头像
			BuddyInfo stBuddy = gDataBaseOpera->DBGetBuddyInfoByID(QString::number(stAdd.iId));

			buddy->OnSetPicPath(stBuddy.strLocalAvatar);
			buddy->OnSetAutoGrapthText(stAdd.strMessage, "font: 75 16pt 微软雅黑;font-size:12px;"+m_style2);
			if (stAdd.iType == 1)
			{
				GroupInfo stGroup = gDataBaseOpera->DBGetGroupFromID(stAdd.strGroupId);
				QString strNick = stBuddy.strNickName + tr(" Apply to join") + stGroup.groupName;
				buddy->OnSetNickNameText(strNick, "font: 75 16pt 微软雅黑;font-size:14px;font-weight:bold;"+m_style1);
			}
			else
			{
				QString strNick = stBuddy.strNickName;
				buddy->OnSetNickNameText(strNick, "font: 75 16pt 微软雅黑;font-size:14px;font-weight:bold;"+m_style1);
			}
			if (stAdd.iState == 1)
			{
				buddy->OnSetMessageTypeText(tr("approved"), "font: 75 16pt 微软雅黑;font-size:16px;"+m_style2);
			}
			else
			{
				buddy->OnSetMessageTypeText(tr("rejected"), "font: 75 16pt 微软雅黑;font-size:16px;" + m_style2);
			}
			QListWidgetItem *newItem = new QListWidgetItem(); //创建一个newItem
			newItem->setSizeHint(QSize(newFriendWidget->width(), 60));
			newFriendWidget->insertItem(newFriendWidget->count(), newItem); //将该newItem插入到后面
			newFriendWidget->setItemWidget(newItem, buddy); //将buddy赋给该newItem
			buddy->mAcceptBtn->hide();
			buddy->mAddPerson->hide();
		}
	}
	SetMessageNumUp(iUnRead);
}
void ContactsDataManager::slotApplyFriend(QString strValue)
{
	//m_TmpId = strValue;
	BuddyInfo stTmp = gDataBaseOpera->DBGetBuddyInfoByID(strValue);
	InputBox *box = new InputBox(newFriendWidget);
	box->setLineText(stTmp.strNote, stTmp.strNickName);
	box->setEmpty(true);
	box->setProperty("buddyId", strValue);
	connect(box, SIGNAL(sigEnter(QString)), this, SLOT(slotSetNote(QString)));
	//connect(box, SIGNAL(sigClose(QString)), this, SLOT(slotSetNote(QString)));
	box->init(tr("Please enter a remark name:"));
}

void ContactsDataManager::slotRejectFriend(QString strValue)
{
	HttpNetWork::HttpNetWorkShareLib *PersonInfo = new HttpNetWork::HttpNetWorkShareLib(this);
	UserInfo userInfo = gDataManager->getUserInfo();
	AppConfig appConf = gDataManager->getAppConfigInfo();
	QString strResult = appConf.MessageServerAddress + "/IMServer/user/rejectAddFriend" + QString("?userId=") + QString::number(userInfo.nUserID) + "&passWord=" + userInfo.strUserPWD + "&friendUserId=" + strValue;
	PersonInfo->getHttpRequest(strResult);
	AddApplyMessage stInfo;
	bool bHas = gDataBaseOpera->DBFindAddApplyMessage(0, strValue.toInt(), "", stInfo);
	if (bHas)
	{
		stInfo.iState = 2;//状态改为已拒绝
		gDataBaseOpera->DBUpdateAddApplyMessage(stInfo);
	}
	UpdateNewFriendWidget();
	QListWidgetItem *item = newfriendList->item(0);
	if (item)
	{
		InterPlanetNoticeWidget *buddy = (InterPlanetNoticeWidget*)newfriendList->itemWidget(item);
		if (buddy)
		{
			buddy->OnSetMessageNum("");
			m_iMessageNum = 0;
			sigMessageNum(m_iMessageNum);
		}
	}
}
void ContactsDataManager::slotAddFriendUrl(bool bSuc, QString strSuc)
{
	HttpNetWork::HttpNetWorkShareLib *act = qobject_cast<HttpNetWork::HttpNetWorkShareLib*>(sender());
	if (act)
	{
		QJsonParseError jsonError;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(strSuc.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QJsonObject obj = jsonDocument.object();
				if (obj["result"].toString() == "success")
				{
					QVariant var = act->property("IsAddFriend");
					bool bfriend = var.value<bool>();
					if (bfriend)
					{
						QVariant var = act->property("buddyId");
						QString buddyId = var.value<QString>();
						UserInfo userInfo = gDataManager->getUserInfo();
						MessageInfo msgInfo = gSocketMessage->SendTextMessage(userInfo.nUserID, buddyId.toInt(), 0, (short)0, tr("We have become friends and chat together!"));
						BuddyInfo buddy = gDataBaseOpera->DBGetBuddyInfoByID(buddyId);

						MessageListInfo messageListInfo;
						messageListInfo.nBudyyID = buddyId.toInt();
						messageListInfo.strLastMessage = msgInfo.strMsg;
						messageListInfo.nLastTime = msgInfo.ClientTime;
						messageListInfo.strBuddyName = buddy.strUserName;
						messageListInfo.strBuddyHeaderImage = buddy.strLocalAvatar;
						messageListInfo.messageType = msgInfo.MessageChildType;
						messageListInfo.nUnReadNum = 0;
						messageListInfo.isGroup = 0;
						gDataBaseOpera->DBInsertPerMessageListInfo(messageListInfo);

						//发送请求通知服务器
						HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
						QString url = gDataManager->getAppConfigInfo().MessageServerAddress + "/IMServer/user/updateFriend"
							+ QString("?userId=%1&passWord=%2&friendUserId=%3&note=%4").arg(userInfo.nUserID).arg(userInfo.strUserPWD).arg(buddy.nUserId).arg(buddy.strNote);

						http->getHttpRequest(url);
						QMap<QString, QVariant> params;

						emit sigUpdateSelfMessage(false, QVariant::fromValue(buddy), msgInfo.strMsg, false, params);
					}
					else
					{
						//发送加人通知。
						QVariant var = act->property("GroupId");
						QString GroupID = var.value<QString>();
						var = act->property("BuddyId");
						QString BuddyId = var.value<QString>();
						BuddyInfo buddyInfo = gDataBaseOpera->DBGetBuddyInfoByID(BuddyId);
						QString content = buddyInfo.strNickName + tr(" joined the group");
						QVariantMap map;
						map.insert("type", "notification");
						map.insert("content", content);
						map.insert("inviterID", "");
						map.insert("invitedID", buddyInfo.strNickName);
						map.insert("CMD", "addUserToGroup");

						emit sigTipMessage(GroupMessage, GroupID, QJsonDocument::fromVariant(map).toJson());
					}
					return;
				}
			}
		}
		IMessageBox::tip(newFriendWidget, tr("Warning"), tr("network anomaly!"));
		act->deleteLater();
	}
}
void ContactsDataManager::slotApplyGroup(QString strValue)
{
	AddApplyMessage stInfo;
	UserInfo userInfo;
	bool bHas = gDataBaseOpera->DBFindAddApplyMessageByMegId(strValue, stInfo);
	if (bHas)
	{
		HttpNetWork::HttpNetWorkShareLib *PersonInfo = new HttpNetWork::HttpNetWorkShareLib();
		userInfo = gDataManager->getUserInfo();
		AppConfig appConf = gDataManager->getAppConfigInfo();
		PersonInfo->setProperty("IsAddFriend", false);
		PersonInfo->setProperty("GroupId", stInfo.strGroupId);
		PersonInfo->setProperty("BuddyId", stInfo.iId);
		connect(PersonInfo, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotAddFriendUrl(bool, QString)));
		QString strResult = appConf.MessageServerAddress + "/IMServer/group/addUserToGroup" + QString("?userId=") + QString::number(userInfo.nUserID) + "&passWord=" + userInfo.strUserPWD + "&groupId=" + stInfo.strGroupId + "&addUserId=" + QString::number(stInfo.iId);
		PersonInfo->getHttpRequest(strResult);
	}
	InterPlanetNoticeWidget *buddy = (InterPlanetNoticeWidget*)newfriendList->itemWidget(newfriendList->item(0));
	buddy->OnSetMessageNum("");
	m_iMessageNum = 0;
	sigMessageNum(m_iMessageNum);
}
void ContactsDataManager::slotRejectGroup(QString strValue)
{
	bool bHas;
	AddApplyMessage stInfo;
	bHas = gDataBaseOpera->DBFindAddApplyMessageByMegId(strValue, stInfo);
	if (bHas)
	{
		HttpNetWork::HttpNetWorkShareLib *PersonInfo = new HttpNetWork::HttpNetWorkShareLib();
		UserInfo userInfo = gDataManager->getUserInfo();
		AppConfig appConf = gDataManager->getAppConfigInfo();
		PersonInfo->setProperty("IsAddFriend", false);
		connect(PersonInfo, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotAddFriendUrl(bool, QString)));
		QString strResult = appConf.MessageServerAddress + "/IMServer/group/rejectAddUserToGroup" + QString("?userId=") + QString::number(userInfo.nUserID) + "&passWord=" + userInfo.strUserPWD + "&groupId=" + stInfo.strGroupId + "&applyUserId=" + QString::number(stInfo.iId) + "&leaveMessage=";
		PersonInfo->getHttpRequest(strResult);

		stInfo.iState = 2;//状态改为已拒绝
		gDataBaseOpera->DBUpdateAddApplyMessage(stInfo);
		UpdateNewFriendWidget();
	}
	InterPlanetNoticeWidget *buddy = (InterPlanetNoticeWidget*)newfriendList->itemWidget(newfriendList->item(0));
	buddy->OnSetMessageNum("");
	m_iMessageNum = 0;
	sigMessageNum(m_iMessageNum);
}
void ContactsDataManager::SetMessageNumUp(int iNum)
{
	InterPlanetNoticeWidget *buddy = (InterPlanetNoticeWidget*)newfriendList->itemWidget(newfriendList->item(0));
	m_iMessageNum = iNum;
	if (iNum > 0)
	{
		buddy->OnSetMessageNum(QString::number(m_iMessageNum));
	}
	else
	{
		buddy->OnSetMessageNum("");
	}
	sigMessageNum(m_iMessageNum);
}
void ContactsDataManager::slotNewApply()
{
	slotNewFirendFile();
	return;
}
void ContactsDataManager::slotFilter(int x)
{
	newfriendList->setFixedWidth(x);
}

void ContactsDataManager::slotSetNote(QString strValue)
{
	InputBox *act = qobject_cast<InputBox*>(sender());
	QVariant var = act->property("buddyId");
	QString buddyId = var.value<QString>();
	BuddyInfo stTmp = gDataBaseOpera->DBGetBuddyInfoByID(buddyId);
	stTmp.strNote = strValue;
	gDataBaseOpera->DBInsertBuddyInfo(stTmp);
	sigUpdateBuddyInfo(stTmp);

	HttpNetWork::HttpNetWorkShareLib *PersonInfo = new HttpNetWork::HttpNetWorkShareLib();
	PersonInfo->setProperty("buddyId", buddyId);
	UserInfo userInfo = gDataManager->getUserInfo();
	AppConfig appConf = gDataManager->getAppConfigInfo();
	PersonInfo->setProperty("IsAddFriend", true);
	connect(PersonInfo, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotAddFriendUrl(bool, QString)));
	QString strResult = appConf.MessageServerAddress + "/IMServer/user/addFriend" + QString("?userId=") + QString::number(userInfo.nUserID) + "&passWord=" + userInfo.strUserPWD + "&friendUserId=" + buddyId;
	PersonInfo->getHttpRequest(strResult);
	QListWidgetItem *item = newfriendList->item(0);
	if (item)
	{
		InterPlanetNoticeWidget *buddy = (InterPlanetNoticeWidget*)newfriendList->itemWidget(item);
		if (buddy)
		{
			buddy->OnSetMessageNum("");
			m_iMessageNum = 0;
			sigMessageNum(m_iMessageNum);
		}
	}
}

void ContactsDataManager::slotUpdateBuddyInfo(BuddyInfo eInfo)
{
	emit sigUpdateInfo(BuddyUpdate, QVariant::fromValue(eInfo));
}

bool ContactsDataManager::currentProfileFlag = true;

void ContactsDataManager::setUpdateIndexFlag(bool bVal)
{
	contactsList->setUpdateIndexFlag(bVal);
}


void ContactsDataManager::slotOpenPerOrGroupChat(QString strUserId)
{
	if (gDataBaseOpera->DBJudgeGroupIsHaveByID(strUserId))  //是部落
	{
		slotSendChat(OpenGroup, strUserId);
	}
	else
	{
		slotSendChat(OpenPer, strUserId);
	}
}