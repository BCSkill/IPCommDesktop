#include "GroupListModel.h"
#include <qdebug.h>

GroupListItem::GroupListItem(const QString &userId, const QString &headUrl, const QString &nickName)
	:m_userId(userId),m_headUrl(headUrl), m_nickName(nickName)
{
}

int GroupListItem::itemType() const
{
	return m_itemType;
}

QString GroupListItem::userId() const
{
	return m_userId;
}

QString GroupListItem::headUrl() const
{
	return m_headUrl;
}

QString GroupListItem::nickName() const
{
	return m_nickName;
}

QString GroupListItem::msg() const
{
	return m_msg;
}

QString GroupListItem::msgTime() const
{
	return m_msgTime;
}

int GroupListItem::unreadMsgCount() const
{
	return m_unreadMsgCount;
}

QString GroupListItem::sendStatusUrl() const
{
	return m_sendStatusUrl;
}



void GroupListItem::setItemType(QVariant val)
{
	m_itemType = val.toInt();
}

void GroupListItem::setUserId(QVariant val)
{
	m_userId = val.toString();
}

void GroupListItem::setHeadUrl(QVariant val)
{
	m_headUrl = val.toString();
}

void GroupListItem::setNickName(QVariant val)
{
	m_nickName = val.toString();
}

void GroupListItem::setMsg(QVariant val)
{
	m_msg = val.toString();
}

void GroupListItem::setMsgTime(QVariant val)
{
	m_msgTime = val.toString();
}

void GroupListItem::setUnreadMsgCount(QVariant val)
{
	m_unreadMsgCount = val.toInt();
}

void GroupListItem::setSendStatusUrl(QVariant val)
{
	m_sendStatusUrl = val.toString();
}










GroupListModel::GroupListModel(QObject *parent)
	: QAbstractListModel(parent)
{
}

void GroupListModel::pushFront(const GroupListItem *item)
{
	beginInsertRows(QModelIndex(), 0, 0);
	m_items.push_front(*item);
	endInsertRows();
}

void GroupListModel::pushBack(const GroupListItem *item)
{
	beginInsertRows(QModelIndex(), rowCount(), rowCount());
	m_items.push_back(*item);
	endInsertRows();
}

int GroupListModel::rowCount(const QModelIndex & parent) const {
	Q_UNUSED(parent);
	return m_items.count();
}


QVariant GroupListModel::data(const QModelIndex & index, int role) const {
	if (index.row() < 0 || index.row() >= m_items.count())
		return QVariant();

	const GroupListItem &item = m_items[index.row()];

	if (role == ItemTypeRole)
	{
		return item.itemType();
	}
	else if (role == UserIdRole)
	{
		return item.userId();
	}
	else if (role == HeadUrlRole)
	{
		return item.headUrl();
	}
	else if (role == NickNameRole)
	{
		return item.nickName();
	}
	else if (role == MsgRole)
	{
		return item.msg();
	}
	else if (role == MsgTimeRole)
	{
		return item.msgTime();
	}
	else if (role == UnreadMsgCountRole)
	{
		return item.unreadMsgCount();
	}
	else if (role == SendStatusUrlRole)
	{
		return item.sendStatusUrl();
	}

	return QVariant();
}

bool GroupListModel::setData(const QModelIndex &index, const QVariant &value, int role/* = Qt::EditRole*/)
{
	if (index.row() < 0 || index.row() >= m_items.count())
		return false;

	GroupListItem &item = m_items[index.row()];
	if (role == ItemTypeRole)
	{
		item.setItemType(value);
	}
	else if (role == UserIdRole)
	{
		item.setUserId(value);
	}
	else if (role == HeadUrlRole)
	{
		//item.setHeadUrl(value);
		item.setHeadUrl(value.toString() + "?" + QString::number(QDateTime::currentDateTime().toMSecsSinceEpoch()));
	}
	else if (role == NickNameRole)
	{
		item.setNickName(value);
	}
	else if (role == MsgRole)
	{
		item.setMsg(value);
	}
	else if (role == MsgTimeRole)
	{
		item.setMsgTime(value);
	}
	else if (role == UnreadMsgCountRole)
	{
		item.setUnreadMsgCount(value);
	}
	else if (role == SendStatusUrlRole)
	{
		item.setSendStatusUrl(value);
	}

	emit dataChanged(index, index);
	return true;
}

//![0]
QHash<int, QByteArray> GroupListModel::roleNames() const {
	QHash<int, QByteArray> roles;
	roles[ItemTypeRole] = "itemType";
	roles[UserIdRole] = "userId";
	roles[HeadUrlRole] = "headUrl";
	roles[NickNameRole] = "nickName";
	roles[MsgRole] = "msg";
	roles[MsgTimeRole] = "msgTime";
	roles[UnreadMsgCountRole] = "unreadMsgCount";
	roles[SendStatusUrlRole] = "sendStatusUrl";
	return roles;
}

void GroupListModel::clear()
{
	if (m_items.count() > 0)
	{
		beginRemoveRows(QModelIndex(), 0, m_items.count() - 1);
		m_items.clear();
		endRemoveRows();
	}
	
}

void GroupListModel::takeItem(int row)
{
	beginRemoveRows(QModelIndex(), row, row);
	m_items.removeAt(row);
	endRemoveRows();
}

QString GroupListModel::currentSelectedUserId()
{
	return  m_currentSelectedUserId;
}

void GroupListModel::setCurrentSelectedUserId(QString userId)
{
	m_currentSelectedUserId = userId;
}

void GroupListModel::reLoadBegin()
{
	beginResetModel();
}
void GroupListModel::reLoadEnd()
{
	endResetModel();
}