#include "ImgProvider.h"
#include <QFile>
#include <QDebug>

ImgProvider::ImgProvider(ImageType type, Flags flags) :
	QQuickImageProvider(type, flags)
{

}

ImgProvider::~ImgProvider()
{

}

QImage ImgProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
	QImageReader reader;
	reader.setDecideFormatFromContent(true);
	QStringList id_list = id.split('?');
	QString file;

	if (id_list.count() > 1)
	{
		file = id_list[0];
	}
	else
	{
		file = id_list[0];
	}
	reader.setFileName(file);

	size->setWidth(reader.size().width());
	size->setHeight(reader.size().height());

	reader.setScaledSize(requestedSize);

	QImage img = reader.read();
	if (img.isNull())
	{
		img.load(":/PerChat/Resources/person/temp.png");
	}
	return img;
}