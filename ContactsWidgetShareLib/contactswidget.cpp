﻿#include "contactswidget.h"
#include "ui_contactswidget.h"
#ifdef Q_OS_MAC
#include "mycommonstyle.h"
#endif
#include "QStringLiteralBak.h"
#include "contactmenuwidget.h"
#include "perprofilewidget.h"
#include "groupprofilewidget.h"
#include "contactssearchlist.h"

ContactsWidget::ContactsWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::ContactsWidget();

	ui->setupUi(this);
	buttons = new QButtonGroup(this);

	QPushButton *buddyButtn = new QPushButton();
	buddyButtn->setObjectName("buddy");
	buddyButtn->setCheckable(true);
	buddyButtn->setFixedHeight(40);
	buddyButtn->setAutoExclusive(true);

	QPushButton *groupButton = new QPushButton();
	groupButton->setObjectName("group");
	groupButton->setCheckable(true);
	groupButton->setFixedHeight(40);
	groupButton->setAutoExclusive(true);
	QPushButton *addButton = new QPushButton;
	addButton->setObjectName("add");
	addButton->setCursor(Qt::PointingHandCursor);
	addButton->setFixedSize(40, 40);

	ui->buttonsLayout->addWidget(buddyButtn);
	ui->buttonsLayout->addWidget(groupButton);
	ui->buttonsLayout->addWidget(addButton);

	buttons->addButton(buddyButtn, 1);
	buttons->addButton(groupButton, 2);
	buttons->addButton(addButton, 3);

	mCreateGroup = new QAction(tr("Create group"), this);
	mCreateGroup->setIcon(QIcon(":/GroupChat/Resources/groupchat/group_chat.png"));
	connect(mCreateGroup, SIGNAL(triggered()), this, SIGNAL(sigAddCreateGroup()));

	mAddPerson = new QAction(tr("Add friends"), this);
	mAddPerson->setIcon(QIcon(":/Login/Resources/login/pclogin_title_user.png"));
	connect(mAddPerson, SIGNAL(triggered()), this, SIGNAL(sigAddPerson()));

	mAddMenu = new QMenu((QWidget*)QApplication::desktop());
	QMenu* listMenu = new QMenu(this);
	QFile style(":/QSS/Resources/QSS/ContactsWidgetShareLib/contactswidgetmenu.qss");
	style.open(QFile::ReadOnly);
	QString sheet = QLatin1String(style.readAll());
	mAddMenu->setStyleSheet(sheet);
	style.close();
	//mAddMenu->setStyleSheet("QMenu{background-color: #72a4d6;color: #042439;}QMenu::item{padding:4px;padding-left:20px;padding-right:30px;}QMenu::item:selected{color: white;border:none;padding:4px;padding-left:20px;padding-right:30px;}");
	mAddMenu->addAction(mCreateGroup);
	mAddMenu->addAction(mAddPerson);

	connect(buttons, SIGNAL(buttonToggled(int, bool)), this, SLOT(slotSwitchTabs(int, bool)));
	connect(addButton, SIGNAL(clicked(bool)), this, SLOT(slotAddBtnClicked()));
	
	connect(ui->searchLineEdit, SIGNAL(textEdited(QString)), this, SLOT(slotEditSearchLine(QString)));
	connect(ui->searchBtn, SIGNAL(clicked(bool)), this, SLOT(slotClickCancelSearch()));
	connect(ui->perWidget, SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget*)), this, SIGNAL(sigOpenPic(QString, QList<QString>*, QWidget*)));//图片浏览

	ui->separateLine->installEventFilter(this);
	ui->searchLineEdit->installEventFilter(this);

#ifdef Q_OS_MAC
    ui->searchLineEdit->setStyle(new MyProxyStyle);
#endif
	ui->perWidget->hide();
	ui->groupWidget->hide();
	ui->NewFriendWidget->hide();

	this->setStyleFunc();
	buddyButtn->setChecked(true);
	pmenuWidget = NULL;
	flag = true;
}

ContactsWidget::~ContactsWidget()
{
	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}


void ContactsWidget::slotSwitchTabs(int id, bool checked)
{
	if (checked)
	{
		if (id == 3)
		{
			return;
		}
		ui->contactsStackedWidget->setCurrentIndex(id);
		int currIndex = ui->contactsStackedWidget->currentIndex();

		if (id == 2 && id == currIndex)
		{
			ui->perWidget->hide();
			ui->NewFriendWidget->hide();
			ui->groupWidget->show();
		}
		else if (id == 1 && id == currIndex)
		{
			if (flag)
			{
				ui->groupWidget->hide();
				ui->perWidget->show();
			}
			else
			{
				ui->groupWidget->hide();
				ui->NewFriendWidget->show();
				ui->perWidget->hide();
			}
		}
		emit sigSendPerOrGroupFlag(id);
	}
}

void ContactsWidget::addWidget(QWidget *widget)
{
	widget->resize(ui->contactsStackedWidget->size());
	ui->contactsStackedWidget->addWidget(widget);
}

void ContactsWidget::setWidget(QWidget *widget)
{
	ui->contactsStackedWidget->setCurrentWidget(widget);
}

perProfileWidget * ContactsWidget::getPerProfile()
{
	return ui->perWidget;
}

GroupProfileWidget * ContactsWidget::getGroupProfile()
{
	return ui->groupWidget;
}

QListWidget * ContactsWidget::getNewFriendProfile()
{
	return ui->NewFriendWidget;
}

QWidget * ContactsWidget::getTabsWidgetProfile()
{
	return ui->tabsWidget;
}

bool ContactsWidget::eventFilter(QObject *obj, QEvent *e)
{
	if (obj == ui->separateLine)
	{
		if (e->type() == QEvent::MouseMove)
		{
			QMouseEvent *event = (QMouseEvent *)e;
			int w = ui->tabsWidget->width() + event->x();
			if (w > 350)
				w = 350;
			if (w < 150)
				w = 150;
			ui->tabsWidget->setFixedWidth(w);
			emit sigFilter(w);
		}
	}

	if (obj == ui->searchLineEdit && e->type() == QEvent::KeyPress)
	{
		QKeyEvent *keyEvent = (QKeyEvent *)e;
		if (keyEvent->key() == Qt::Key_Up || keyEvent->key() == Qt::Key_Down || keyEvent->key() == Qt::Key_Return)
		{
			QWidget *widget = ui->contactsStackedWidget->widget(0);
			if (widget)
			{
				ContactsSearchList *searchList = (ContactsSearchList *)widget;
				searchList->onChangeItem(keyEvent);
			}
		}
	}

	if (obj == ui->searchLineEdit && e->type() == QEvent::MouseButtonPress)
	{
		ui->searchLineEdit->selectAll();
		return true;
	}

	return QWidget::eventFilter(obj, e);
}

void ContactsWidget::slotEditSearchLine(QString text)
{
	if (text.isEmpty())
	{
		ui->contactsStackedWidget->setCurrentIndex(buttons->checkedId());
		ui->searchLineEdit->setFocus();
	}
	else
	{
		//0位是搜索页面。
		if (ui->contactsStackedWidget->currentIndex() != 0)
			ui->contactsStackedWidget->setCurrentIndex(0);
		emit sigSearchText(text);
	}
}

void ContactsWidget::slotClickCancelSearch()
{
	ui->searchLineEdit->clear();

	ui->contactsStackedWidget->setCurrentIndex(buttons->checkedId());
}

void ContactsWidget::slotAddBtnClicked()
{
//     if (mAddMenu != NULL)
//     {
// 		QPoint point = QCursor::pos();
// 		double dV = mAddMenu->sizeHint().height();
// 		point.setY(point.y() - dV);
// 		mAddMenu->exec(point);
 		QPoint posAdd = buttons->button(3)->pos();
		posAdd = this->mapToGlobal(posAdd);
		//addButton->pos();
		pmenuWidget = new ContactMenuWidget(this);
		pmenuWidget->installEventFilter(this);
		connect(pmenuWidget, SIGNAL(sigAddCreateGroup()), this, SIGNAL(sigAddCreateGroup()));
		connect(pmenuWidget, SIGNAL(sigAddPerson()), this, SIGNAL(sigAddPerson()));
		pmenuWidget->move(posAdd.rx(), posAdd.ry()- pmenuWidget->size().height()-1);
		pmenuWidget->show();
		pmenuWidget->setFocus();
//    }
}

void ContactsWidget::setStyleFunc()
{
	QFile file(":/QSS/Resources/QSS/ContactsWidgetShareLib/contactswidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();
}

void ContactsWidget::slotCanclePerProfileWid()
{
	flag = false;
}

void ContactsWidget::slotPerProfileWid()
{
	flag = true;
}
