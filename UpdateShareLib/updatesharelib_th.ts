<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="th_TH">
<context>
    <name>UpdateShareLib</name>
    <message>
        <location filename="updatesharelib.cpp" line="100"/>
        <source>下载完成,开始解压!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="updatesharelib.cpp" line="114"/>
        <source>Warning</source>
        <translation>การเตือน</translation>
    </message>
    <message>
        <location filename="updatesharelib.cpp" line="114"/>
        <source>Upgrade package download failed, the program could not be started</source>
        <translation>การดาวน์โหลดแพ็คเกจอัพเกรดล้มเหลวโปรแกรมไม่สามารถเริ่มต้นได้</translation>
    </message>
</context>
</TS>
