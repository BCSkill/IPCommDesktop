﻿#include "updatesharelib.h"
#ifndef Q_OS_WIN
#include <QDesktopServices>
#endif
#include "QStringLiteralBak.h"
#include "globalmanager.h"

UpdateShareLib::UpdateShareLib()
{

}

UpdateShareLib::~UpdateShareLib()
{

}

void UpdateShareLib::onUpdate()
{
    HttpNetWork::HttpNetWorkShareLib *http = new HttpNetWork::HttpNetWorkShareLib;
	connect(http, SIGNAL(sigReplyFinished(bool, QString)), this, SLOT(slotUpdateApp(bool, QString)));
	OPRequestShareLib *config = new OPRequestShareLib;
	QString url = config->getUpdateUrl();
	delete config;
    http->getHttpRequest(url);
}

void UpdateShareLib::slotUpdateApp(bool success, QString result)
{
	if (success)
	{
		QJsonParseError jsonError;
		QJsonDocument jsonDocument = QJsonDocument::fromJson(result.toUtf8(), &jsonError);
		if (jsonError.error == QJsonParseError::NoError)
		{
			if (jsonDocument.isObject())
			{
				QVariantMap result = jsonDocument.toVariant().toMap();
				if (result["result"].toString() == "success")
				{
#ifdef Q_OS_WIN
                    //判断下当前版本号及更新文件路径
                    QVariantMap appUpgrade = result["appUpgrade"].toMap();
                    QVariantMap win = appUpgrade["win"].toMap();
                    if (!win.isEmpty())
                    {
                        appMap = win["appFile"].toMap();
                        updateMap = win["appResource"].toMap();
                        //取出版本号
                        QString strServerVersion = updateMap["version"].toString();

                        std::string v1 = gDataManager->getAppConfigInfo().appVersion.updateVersion.toStdString();
                        int nResult = VersionCompare(v1, strServerVersion.toStdString());
                        if (nResult == -1 || v1 == "")
                        {
                            //需要升级升级程序。
                            QString updateURL = updateMap["path"].toString();
                            HttpNetWork::HttpDownLoadFile *http = new HttpNetWork::HttpDownLoadFile;
                            connect(http, SIGNAL(sigDownFinished(bool)), this, SLOT(slotDownloadUpdate(bool)));
                            QString file = "./updateApp.zip";
                            http->setData(QVariant(file));
                            http->StartDownLoadFile(updateURL, file);
                        }
                        else
                        {
                            //处理看看是否需要升级主程序。
                            slotUpdateMain();
                        }
                    }
#else
                    OnDealMacUpdate(result);
#endif
				}
			}
		}
	}
	else
	{
		//即使连接网络失败，也要显示主界面。
		emit sigAlreadyLatest();
	}
}

void UpdateShareLib::slotDownloadUpdate(bool success)
{
#ifdef Q_OS_WIN
	if (success)
	{
		static wchar_t szbuf[260];
		::GetModuleFileNameW(NULL, szbuf, 260);
		::PathRemoveFileSpecW(szbuf);

		std::wstring path;
		path.append(szbuf);
		if (path.at(path.size() - 1) != L'\\')
		{
			path.append(L"\\");
		}

		qDebug() << tr("下载完成,开始解压!");
		HttpNetWork::HttpDownLoadFile *download = qobject_cast<HttpNetWork::HttpDownLoadFile *>(sender());
		QString fileName = download->getData().toString();
		QString strDir = QString::fromStdWString(path);

		if (ZipUtil::UnzipFile(fileName.toStdWString(), strDir.toStdWString()))
		{
			QFile::remove(fileName);//刪除文件
			//处理主程序是否需要升级。
			slotUpdateMain();
		}
	}
	else
	{
		IMessageBox::tip(NULL, tr("Warning"), tr("Upgrade package download failed, the program could not be started"));
		qApp->exit();
	}
#endif
}

void UpdateShareLib::slotUpdateMain()
{
#ifdef Q_OS_WIN
	/*取出版本号*/
	QString strServerVersion = appMap["version"].toString();

	std::string v1 = gDataManager->getAppConfigInfo().appVersion.versionID.toStdString();
	int nResult = VersionCompare(v1, strServerVersion.toStdString());
	if (nResult == -1)  //需要升级。
	{
		QStringList arguments;
		arguments.append(strServerVersion);
		arguments.append(appMap["path"].toString());
		arguments.append(appMap["des"].toString());
		arguments.append("OpenPlanet.exe");

		LoginDatabaseOperaShareLib *database = new LoginDatabaseOperaShareLib;
		QString strUserPath = gSettingsManager->getUserPath();
		database->ConnectLoginDB(strUserPath+"/database/common.db", "Login");
		NetWorkProxyInfo proxy = database->GetNetWorkProxyInfoDB();
		arguments.append(QString::number(proxy.proxyModel));
		arguments.append(proxy.strUserName);
		arguments.append(proxy.strUserPwd);
		arguments.append(proxy.strAddress);
		arguments.append(proxy.strPort);
		delete database;
		//传入程序名。
		arguments.append(gDataManager->getAppConfigInfo().appVersion.appName);
		//传入程序语言。
		arguments.append(gSettingsManager->getLanguage());
		QString strUpdatePath = QApplication::applicationDirPath() + "/" + "update.exe";

		LPCWSTR path = reinterpret_cast<const wchar_t *>(strUpdatePath.utf16());
		QString arg;
		for (int i = 0; i < arguments.count(); i++)
		{
			QString argument;
			if (arguments[i].isEmpty())
				argument = "\"\"";
			else
				argument = arguments[i];

			if (i == 0)
				arg += argument;
			else
				arg += " " + argument;
		}

		LPCWSTR str = reinterpret_cast<const wchar_t *>(arg.utf16());
		ShellExecute(0, L"open", path, str, NULL, SW_SHOW);
	}
	else    //不需要升级。
	{
		emit sigAlreadyLatest();
	}
#endif
}

/*提供版本比对*/
int UpdateShareLib::VersionCompare(std::string v1, std::string v2)
{
	int vnum1 = 0, vnum2 = 0;
	for (int i = 0, j = 0; (i < v1.length() || j < v2.length());)
	{
		while (i < v1.length() && v1[i] != '.')
		{
			vnum1 = vnum1 * 10 + (v1[i] - '0');
			i++;
		}
		while (j < v2.length() && v2[j] != '.')
		{
			vnum2 = vnum2 * 10 + (v2[j] - '0');
			j++;
		}
		if (vnum1 > vnum2)
			return 1;
		if (vnum2 > vnum1)
			return -1;      //代表需要升级。
		vnum1 = vnum2 = 0;
		i++;
		j++;
	}
	return 0;
}

//处理mac升级
void UpdateShareLib::OnDealMacUpdate(QVariantMap result)
{
#ifndef Q_OS_WIN
    //判断下当前版本号及更新文件路径
    QVariantMap appUpgrade = result["appUpgrade"].toMap();
    QVariantMap win = appUpgrade["mac"].toMap();
    if (!win.isEmpty())
    {
        appMap = win["appFile"].toMap();
        QString strServerVersion = appMap["version"].toString();

        std::string v1 = gDataManager->getAppConfigInfo().appVersion.versionID.toStdString();
        int nResult = VersionCompare(v1, strServerVersion.toStdString());
        if (nResult == -1)  //需要升级。
        {
            m_pWidget = new updateWidget;
            connect(m_pWidget,SIGNAL(sigUpdate()),this,SLOT(slotQuit()));
            connect(m_pWidget,SIGNAL(sigExit()),this,SIGNAL(sigAlreadyLatest()));
            m_pWidget->SetUpdateDescribeInfo(appMap["des"].toString());
            m_pWidget->setObjectName(appMap["path"].toString());
            m_pWidget->show();
        }
        else
        {
            emit sigAlreadyLatest();
        }
    }
#endif
}

void UpdateShareLib::slotQuit()
{
#ifndef Q_OS_WIN
    updateWidget *act=qobject_cast<updateWidget*>(sender());
    if(act)
    {
        QDesktopServices::openUrl(QUrl(act->objectName()));
        qApp->quit();
    }
#endif
}
