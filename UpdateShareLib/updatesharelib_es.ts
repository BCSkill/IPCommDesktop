<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>UpdateShareLib</name>
    <message>
        <location filename="updatesharelib.cpp" line="100"/>
        <source>下载完成,开始解压!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="updatesharelib.cpp" line="114"/>
        <source>Warning</source>
        <translation>Advertencia</translation>
    </message>
    <message>
        <location filename="updatesharelib.cpp" line="114"/>
        <source>Upgrade package download failed, the program could not be started</source>
        <translation>Error al descargar el paquete de actualización, no se pudo iniciar el programa</translation>
    </message>
</context>
</TS>
