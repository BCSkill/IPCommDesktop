﻿#include "recoverywidget.h"
#include "ui_recoverywidget.h"
#include "QStringLiteralBak.h"
#include <QMenu>
#include <qclipboard.h>
#ifdef Q_OS_LINUX
    #include <qdesktopwidget.h>
#endif

RecoveryWidget::RecoveryWidget(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::RecoveryWidget();
	ui->setupUi(this);
	
	//加载样式
	QFile file(":/QSS/Resources/QSS/OPRecoveryWalletShareLib/recoverywidget.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.close();

	this->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
	this->setAttribute(Qt::WA_TranslucentBackground);    // 添加阴影时使用
	setWindowIcon(QIcon(":/Login/Resources/login/system.ico"));

	connect(ui->closeBtn, SIGNAL(clicked()), this, SLOT(close()));
	connect(ui->closeBtn, SIGNAL(clicked()), this, SIGNAL(sigClose()));
	connect(ui->wordBtn, SIGNAL(toggled(bool)), this, SLOT(slotSwitchStackedWidget()));
	connect(ui->privateKeyBtn, SIGNAL(toggled(bool)), this, SLOT(slotSwitchStackedWidget()));
	connect(ui->enterBtn, SIGNAL(clicked()), this, SLOT(slotClickedBtn()));

	ui->prikeyEdit->installEventFilter(this);

	m_Menu = new QMenu(this);
	m_Menu->setObjectName("m_Menu");
	//m_Menu->setStyleSheet("QMenu{background-color: #72a4d6;color: #042439;}QMenu::item{padding:4px;padding-left:20px;padding-right:30px;}QMenu::item:selected{color: white;border:none;padding:4px;padding-left:20px;padding-right:30px;}");
	m_ActPaste = new QAction(tr("Paste"), this);
	//ActCopy->setIcon(QIcon(":/zooming/Resources/zooming/zomming_copy_black.png"));
	connect(m_ActPaste, SIGNAL(triggered()), this, SLOT(slotPaste()));
	m_Menu->addAction(m_ActPaste);

#ifdef Q_OS_LINUX
	setLinuxCenter();
#endif
}

RecoveryWidget::~RecoveryWidget()
{
	if (m_Menu)
		delete m_Menu;
	if (m_ActPaste)
		delete m_ActPaste;
	if (ui)
	{
		delete ui;
		ui = NULL;
	}
}

void RecoveryWidget::slotSwitchStackedWidget()
{
	if (ui->wordBtn->isChecked())
		ui->stackedWidget->setCurrentWidget(ui->wordPage);
	if (ui->privateKeyBtn->isChecked())
		ui->stackedWidget->setCurrentWidget(ui->prikeyPage);
}

void RecoveryWidget::slotClickedBtn()
{
	if (ui->wordBtn->isChecked())
	{
		QStringList wordList;
		wordList.append(ui->lineEdit_1->text());
		wordList.append(ui->lineEdit_2->text());
		wordList.append(ui->lineEdit_3->text());
		wordList.append(ui->lineEdit_4->text());
		wordList.append(ui->lineEdit_5->text());
		wordList.append(ui->lineEdit_6->text());
		wordList.append(ui->lineEdit_7->text());
		wordList.append(ui->lineEdit_8->text());
		wordList.append(ui->lineEdit_9->text());
		wordList.append(ui->lineEdit_10->text());
		wordList.append(ui->lineEdit_11->text());
		wordList.append(ui->lineEdit_12->text());

		QString arg;

        foreach (QString word , wordList)
		{
			//只要有一个助记词不合法，直接报错结束处理。
			if (word.isEmpty() || word.contains(" "))
			{
				IMessageBox::tip(this, tr("Warning"), tr("The mnemonic entered is not legal!"));
				return;
			}

			if (arg.isEmpty())  //arg为空，说明是首个助记词，因此直接赋值，不加空格。
				arg = word;
			else
				arg += " " + word;
		}

		emit sigRecoveryWord(arg);
	}

	if (ui->privateKeyBtn->isChecked())
	{
		QString key = ui->prikeyEdit->toPlainText();
		key.remove(" ");
		key.remove("\n");

		if (key.isEmpty())
			IMessageBox::tip(this, tr("Warning"), tr("The private key entered cannot be empty!"));
		else
		    emit sigPrivateKey(key);
	}
}

//鼠标事件的处理。
void RecoveryWidget::mousePressEvent(QMouseEvent *event)
{
	mouse = event->pos();   //设置移动的原始位置。
	return QWidget::mousePressEvent(event);
}
void RecoveryWidget::mouseReleaseEvent(QMouseEvent *event)
{
	mouse.setX(-1);
	return QWidget::mouseReleaseEvent(event);
}
void RecoveryWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (mouse.x() < 0)
		return;
	//首先通过做差值，获得鼠标位移的距离。
	int x = event->pos().x() - mouse.x();
	int y = event->pos().y() - mouse.y();
	//移动本窗体。
	this->move(this->x() + x, this->y() + y);
	return QWidget::mouseMoveEvent(event);
}

void RecoveryWidget::setBTCMode()
{
	ui->privateKeyBtn->hide();
}

#ifdef Q_OS_LINUX
void RecoveryWidget::setLinuxCenter()
{
    window()->setGeometry
    (
            QStyle::alignedRect
            (
                    Qt::LeftToRight,
                    Qt::AlignCenter,
                    window()->size(),
                    qApp->desktop()->availableGeometry()
            )
    );
}
#endif

bool RecoveryWidget::eventFilter(QObject *obj, QEvent *e)
{
	if (obj == ui->prikeyEdit && e->type() == QEvent::ContextMenu)
	{
		QContextMenuEvent *event = (QContextMenuEvent *)e;
		QPoint eventPos1 = event->globalPos();
		m_Menu->exec(event->globalPos());
		return true;
	}
	return QWidget::eventFilter(obj, e);
}

void RecoveryWidget::slotPaste()
{
	QClipboard *board = QApplication::clipboard();
	ui->prikeyEdit->insertPlainText(board->text());
}

void RecoveryWidget::closeWindow()
{
	this->close();
}