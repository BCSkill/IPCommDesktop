<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>RecoveryWidget</name>
    <message>
        <location filename="recoverywidget.ui" line="26"/>
        <source>Recovery Base</source>
        <oldsource>Recovery Wallet</oldsource>
        <translation>База восстановления</translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="59"/>
        <source>OK</source>
        <translation>Хорошо</translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="62"/>
        <source>Return</source>
        <translation>Вернуть</translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="160"/>
        <source>Mnemonic</source>
        <translation>мнемонический</translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="203"/>
        <source>Private key</source>
        <translation>Закрытый ключ</translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="239"/>
        <source>OpenPlanet</source>
        <translation>OpenPlanet</translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="315"/>
        <source>1.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="337"/>
        <source>2.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="366"/>
        <source>3.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="388"/>
        <source>4.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="417"/>
        <source>5.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="439"/>
        <source>6.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="468"/>
        <source>7.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="490"/>
        <source>8.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="519"/>
        <source>9.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="541"/>
        <source>10.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="570"/>
        <source>11.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="592"/>
        <source>12.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="633"/>
        <source>Please enter the private key:</source>
        <translation>Пожалуйста, введите закрытый ключ:</translation>
    </message>
    <message>
        <location filename="recoverywidget.cpp" line="38"/>
        <source>Paste</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <location filename="recoverywidget.cpp" line="94"/>
        <location filename="recoverywidget.cpp" line="114"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="recoverywidget.cpp" line="94"/>
        <source>The mnemonic entered is not legal!</source>
        <translation>Введенная мнемоника не является законной!</translation>
    </message>
    <message>
        <location filename="recoverywidget.cpp" line="114"/>
        <source>The private key entered cannot be empty!</source>
        <translation>Введенный закрытый ключ не может быть пустым!</translation>
    </message>
</context>
</TS>
