<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>RecoveryWidget</name>
    <message>
        <location filename="recoverywidget.ui" line="26"/>
        <source>Recovery Base</source>
        <oldsource>Recovery Wallet</oldsource>
        <translation>恢复基地</translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="62"/>
        <source>Return</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="59"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="160"/>
        <source>Mnemonic</source>
        <translation>助记词</translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="203"/>
        <source>Private key</source>
        <translation>私钥</translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="239"/>
        <source>OpenPlanet</source>
        <translation>星际通讯</translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="315"/>
        <source>1.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="337"/>
        <source>2.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="366"/>
        <source>3.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="388"/>
        <source>4.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="417"/>
        <source>5.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="439"/>
        <source>6.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="468"/>
        <source>7.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="490"/>
        <source>8.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="519"/>
        <source>9.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="541"/>
        <source>10.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="570"/>
        <source>11.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="592"/>
        <source>12.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="recoverywidget.ui" line="633"/>
        <source>Please enter the private key:</source>
        <translation>请输入私钥：</translation>
    </message>
    <message>
        <location filename="recoverywidget.cpp" line="38"/>
        <source>Paste</source>
        <translation>粘贴</translation>
    </message>
    <message>
        <location filename="recoverywidget.cpp" line="94"/>
        <location filename="recoverywidget.cpp" line="114"/>
        <source>Warning</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="recoverywidget.cpp" line="94"/>
        <source>The mnemonic entered is not legal!</source>
        <translation>助记词输入不合法！</translation>
    </message>
    <message>
        <location filename="recoverywidget.cpp" line="114"/>
        <source>The private key entered cannot be empty!</source>
        <translation>私钥不能为空！</translation>
    </message>
</context>
</TS>
